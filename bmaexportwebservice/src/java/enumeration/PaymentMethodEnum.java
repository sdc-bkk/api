/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Prapaporn
 */
public enum PaymentMethodEnum {

    Cash(1, "เงินสด"),
    CreditCard(2, "บัตรเครดิต"),
    DebitCard(3, "บัตรเดบิต"),
    Order(4,"ธนาณัติ"),
    CashCheck(5, "เช็ค"),
    QRCodeEDC(6, "QR Code ผ่านเครื่อง EDC");

    private final int value;

    private final String displayName;

    PaymentMethodEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int value() {
        return value;
    }

    public String display() {
        return displayName;
    }
    
    
    public static PaymentMethodEnum valueOf(int value) throws IllegalArgumentException {
        for (PaymentMethodEnum newsResourceType : PaymentMethodEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<PaymentMethodEnum> list() {

        List<PaymentMethodEnum> list = Arrays.asList(PaymentMethodEnum.values());

        return list;
    }
    
    public static String getDisplayName(int value) throws IllegalArgumentException {
        for (PaymentMethodEnum newsResourceType : PaymentMethodEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayName;
            }
        }
        throw new IllegalArgumentException();
    }
    
}
