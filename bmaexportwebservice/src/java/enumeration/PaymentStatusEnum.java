/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Prapaporn
 */
public enum PaymentStatusEnum {

    NoPay(0, "ยังไม่ชำระ"),
    Pay(1,"ชำระแล้ว"),
    ChequeNotComplete(2, "เช็คขัดข้อง"),
    ChequeReplace(3, "ชดใช้เช็ค"),
    WaitingBalanceCheck(99, "รอตรวจสอบยอดเงิน");

    private final int value;

    private final String displayName;

    PaymentStatusEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int value() {
        return value;
    }

    public String display() {
        return displayName;
    }
    
    
    public static PaymentStatusEnum valueOf(int value) throws IllegalArgumentException {
        for (PaymentStatusEnum newsResourceType : PaymentStatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<PaymentStatusEnum> list() {

        List<PaymentStatusEnum> list = Arrays.asList(PaymentStatusEnum.values());

        return list;
    }
    
    public static String getDisplayName(int value) throws IllegalArgumentException {
        for (PaymentStatusEnum newsResourceType : PaymentStatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayName;
            }
        }
        throw new IllegalArgumentException();
    }
    
}
