/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.chrono.ThaiBuddhistChronology;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 * @author User
 */
public class DateUtil {

    public static String toFormatDateLongThai(String strDate) {
        String result = "-";
        String Months[] = {
            "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
            "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
            "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        int year = 0, month = 0, day = 0;
        try {
            if (strDate != "") {
                Date date = df.parse(strDate);
                Calendar c = Calendar.getInstance();
                c.setTime(date);

                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DATE);

                if (year < 2400) {//ถ้าเป็น คศ ให้ลบออกเพื่อเป็น พ.ศ.
                    year = year + 543;
                }
                result = String.format("%s %s %s", day, Months[month], year);
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    public static String toDateYearThai(LocalDate date) {
//    LocalDate date = LocalDate.now();
        DateTimeFormatter toThai = DateTimeFormatter.ofPattern("dd/MM/yyyy").withChronology(ThaiBuddhistChronology.INSTANCE);
        String dateStr = toThai.format(date);
        return dateStr;
    }

    public static String toDateYearThai(String strDate) {
        String dateStr = "";
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        int year = 0, month = 0, day = 0;
        try {
            Date date = df.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);

            if (year < 2400) {//ถ้าเป็น คศ ให้ลบออกเพื่อเป็น พ.ศ.
                year = year + 543;
            }

            LocalDate localDate = LocalDate.of(year, month, day);
            dateStr = toDateYearThai(localDate);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return dateStr;
    }

    public static String toDateYearThai(Date date) {
        String dateStr = "";
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        int year = 0, month = 0, day = 0;

        Calendar c = Calendar.getInstance();
        c.setTime(date);

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DATE);

        if (year > 2400) {//ถ้าเป็น พ.ศ. ให้ลบออกเพื่อเป็น คศ
            year = year - 543;
        }

        LocalDate localDate = LocalDate.of(year, month, day);
        dateStr = toDateYearThai(localDate);

        return dateStr;
    }

    public static int diffTwoDate(Date startdate, Date enddate, String typeDiff) {
        int diff = 0;
        LocalDate startLocaldate = convertToLocalDateViaInstant(startdate);
        LocalDate endLocaldate = convertToLocalDateViaInstant(enddate);

        if ("D".equalsIgnoreCase(typeDiff)) {
            long diffDay = ChronoUnit.DAYS.between(startLocaldate, endLocaldate);
            diff = (int) diffDay;
        } else if ("M".equalsIgnoreCase(typeDiff)) {
            long diffMonth = ChronoUnit.MONTHS.between(startLocaldate, endLocaldate);
            diff = (int) diffMonth;
        } else if ("Y".equalsIgnoreCase(typeDiff)) {
            long diffYear = ChronoUnit.YEARS.between(startLocaldate, endLocaldate);
            diff = (int) diffYear;
        } else if ("MI".equalsIgnoreCase(typeDiff)) {//นาที
            long diffMinute = ChronoUnit.MINUTES.between(startLocaldate, endLocaldate);
            diff = (int) diffMinute;
        }
        Period period = Period.between(startLocaldate, endLocaldate);
//    int months = Months.monthsBetween(startLocaldate, endLocaldate).getMonths();
        int days = period.getMonths();
        return diff;
    }

    public static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
