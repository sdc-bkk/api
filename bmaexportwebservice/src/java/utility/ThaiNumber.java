/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

/**
 *
 * @author User
 */
public class ThaiNumber {
    private static final String[] DIGIT_TH = { "0", "๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙" };
    private String valueText;  
  
    // ···········Methods·············· 
    public String getText(double amount) {
        this.valueText = getThaiNumber(String.valueOf(amount));  
        return this.valueText;  
    }  
  
    public String getText(float amount) {
        this.valueText = getThaiNumber(String.valueOf(amount));  
        return this.valueText;  
    }  
  
    public String getText(int amount) {  
        this.valueText = getThaiNumber(String.valueOf(amount));  
        return this.valueText;  
    }  
  
    public String getText(long amount) {
        this.valueText = getThaiNumber(String.valueOf(amount));  
        return this.valueText;  
    }  
  
    public String getText(String amount) {  
        this.valueText = getThaiNumber(amount.trim());  
        return this.valueText;  
    }  
  
    public String getText(Number amount) {  
        this.valueText = getThaiNumber(String.valueOf(amount));  
        return this.valueText;  
    }  
  
    private static String getThaiNumber(String amount) {  
        if(amount == null || amount.isEmpty()) return "";

        StringBuilder sb = new StringBuilder();
        for(char c : amount.toCharArray()){
            if(Character.isDigit(c)){
                String index = DIGIT_TH[Character.getNumericValue(c)].toString();
                sb.append(index);
            } else {
                sb.append(c);
            }
        }
        return sb.toString();  
    }  
}
