/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.lang3.CharUtils;
import th.co.swf.jk.utilities.DateUtils;

public class AppUtil {

    public static String getValueConfig(String key) {
        try {
            Properties properties = getPropConfig("th.co.swf.rest.config.config");
            return properties.getProperty(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String getValueConfig(String key, Object... params) {
        try {
            ResourceBundle labels = ResourceBundle.getBundle("th.co.swf.rest.config.config");

            return MessageFormat.format(labels.getString(key), params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static Properties getPropConfig(String configPath) {
        Properties properties = new Properties();
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();

            configPath = configPath.replaceAll("\\.", "/");
            configPath = configPath + ".properties";//configPath.replaceAll("/properties", ".properties");

            properties.load(loader.getResourceAsStream(configPath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }

    public static byte[] InputStreamToByteArray(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int reads = is.read();

        while (reads != -1) {
            baos.write(reads);
            reads = is.read();
        }

        return baos.toByteArray();

    }

    public static String getFileType(String filename) {
        try {
            return filename.substring(filename.lastIndexOf("."), filename.length()).toLowerCase();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String getFileName(String filename) {
        try {
            String fileType = filename.substring(filename.lastIndexOf("."), filename.length()).toLowerCase();

            return filename.toLowerCase().replace(fileType, "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //Convert \r\n to br
    public static String nl2br(String text) {
        try {
            return text == null ? "" : text.replaceAll("(\r\n|\n)", "<br />");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //Convert br to \n
    public static String nl2BackslashN(String text) {
        try {
            return text == null ? "" : text.replaceAll("<br />", "\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String space2nbsp(String text) {
        try {
            return text == null ? "" : text.replaceAll(" ", "&nbsp;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<String> findDuplicateList(List<String> list) {
        try {

            final List<String> setToReturn = new ArrayList<String>();
            final Set<String> set1 = new HashSet<String>();

            for (String yourInt : list) {
                if (!set1.add(yourInt)) {
                    setToReturn.add(yourInt);
                }
            }
            return setToReturn;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static boolean checkIdentificationCard(String id) {
        try {

            Pattern pattern = Pattern.compile("\\d{13}");

            if (!pattern.matcher(id).matches()) {
                return false;
            }

            if (id.length() != 13) {
                return false;
            }

            int sum = 0;
            for (int i = 0; i < 12; i++) {
                sum += Integer.parseInt(String.valueOf(id.charAt(i))) * (13 - i);
            }

            if ((11 - sum % 11) % 10 != Integer.parseInt(String.valueOf(id.charAt(12)))) {
                return false;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean checkDateFormat(String date) {
        try {

            Pattern pattern = Pattern.compile("\\d{2}/\\d{2}/\\d{4}");

            if (!pattern.matcher(date).matches()) {
                return false;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public static String convertEnNuberToThNumber(int number) {
        String[] thNumber = {"๐", "๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙"};
        String enNumber = String.valueOf(number);
        String result = "";
        try {
            for (int i = 0; i < enNumber.length(); i++) {
                int en = Integer.parseInt(String.valueOf(enNumber.charAt(i)));
                result += thNumber[en];
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static String encryptId(int id) {
        String result = "";
        try {
            AES aes = new AES();
            result = aes.encryptURLSafe(String.valueOf(id));
        } catch (Exception e) {
        }
        return result;
    }

    public static int decryptId(String id) {

        int result = 0;

        try {

            AES aes = new AES();

            id = id == null ? "0" : id;
            result = Integer.parseInt(!id.equals("0") ? aes.decrypt(id) : "0");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String encrypt(String str) {
        String result = "";
        try {
            AES aes = new AES();
            result = aes.encryptURLSafe(str);
        } catch (Exception e) {
        }
        return result;
    }

    public static String decrypt(String str) {
        String result = "";
        try {
            AES aes = new AES();

            str = str.equals(null) ? "" : str;
            result = !str.equals("") ? aes.decrypt(str) : "";
        } catch (Exception e) {
        }
        return result;
    }

    public static String randomPassword(int length) {
        SecureRandom random = new SecureRandom();
        String randomPassword = new BigInteger(130, random).toString(32);

        String password = "";

        try {
            if (length <= 0) {
                length = 8;
            }

            for (int i = 0; i < length; i++) {
                password += randomPassword.charAt(i);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return password;
    }

    public static String randomPassword() {

        return randomPassword(0);
    }

    public static String encryptPassword(String password, String username, String key) {
        String result = "";
        try {
            result = new AES().encrypt(key + "$" + username.toLowerCase() + "$" + password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static String decryptPassword(String password) {
        String result = "";
        String[] resultSplit = null;
        try {
            result = new AES().decrypt(password);

            resultSplit = result.split("\\$");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultSplit[2];
    }

    public static String numberFormatComma(String number) {

        try {
            BigDecimal value = new BigDecimal(number.trim());
            DecimalFormat formatter = new DecimalFormat("#,###.z");
            number = formatter.format(value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return number;
    }
    public static String numberFormatCommaTwoDigit(String number) {

        try {
            BigDecimal value = new BigDecimal(number.trim());
            DecimalFormat formatter = new DecimalFormat("#,##0.00");
            number = formatter.format(value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return number;
    }

    public static String toThaiNumber(String number) {
        String outputString = "";
        if (number != null) {
            Locale locale = new Locale("th", "TH", "TH");
            NumberFormat nf = NumberFormat.getNumberInstance(locale);

            for (int i = 0; i < number.trim().toCharArray().length; i++) {
                char c = number.trim().charAt(i);
                if (CharUtils.isAsciiNumeric(c)) {
                    outputString += nf.format(Integer.parseInt(String.valueOf(c)));
                } else {
                    outputString += String.valueOf(c);
                }
            }
        }
        return outputString;

    }

    public static String toThaiNumber(int number) {
        String outputString = "";
        Locale locale = new Locale("th", "TH", "TH");
        NumberFormat nf = NumberFormat.getNumberInstance(locale);
        outputString = nf.format(number);
        return outputString;
    }

    public static String toDateENString(String date) {
        String dateString = null;

        try {
            Integer year = Integer.parseInt(date.substring(6, 10));
            if (year > 2550) {//พ.ศ.
                year = year - 543;
            }
            dateString = date.substring(0, 6) + year.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static java.sql.Date toDateSql(String date) {
        java.sql.Date sqlDocDate = null;
        try {

            Integer year = Integer.parseInt(date.substring(6, 10));
            if (year > 2550) {//พ.ศ.
                year = year - 543;
            }
            String dateString = date.substring(0, 6) + year.toString();

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            Date docDate = formatter.parse(dateString);
            sqlDocDate = new java.sql.Date(docDate.getTime());

//            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//            Date docDate = formatter.parse(date);
//            sqlDocDate = new java.sql.Date(docDate.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlDocDate;
    }

    public static boolean isNull(String data) {

        return data == null;
    }

    public static boolean isNull(Integer data) {

        return data == null;
    }

    public static boolean isNull(Boolean data) {

        return data == null;
    }

    public static boolean isNullAndZero(Integer data) {

        return data == null ? true : data == 0;
    }

    public static boolean isNullAndSpace(String data) {

        return data == null ? true : data.equalsIgnoreCase("");
    }

    public static String trim(String data) {

        return data != null ? data.trim() : data;

    }

    public static String convertDoubleToStringFormat(Double data) {

        return (data != null && data != 0) ? new DecimalFormat("###,###,###,###,###,##0.00").format(data) : "-";

    }

    public static String convertDoubleTwoDigit(Double data) {

        return (data != null && data != 0) ? new DecimalFormat("#################0.00").format(data) : "-";

    }

    public static String toStringTwoDigitSatang(int data) {
        if (data != 0 && data < 10) {
            data = data * 10;
        }

        return (data != 0) ? Integer.toString(data) : "-";

    }

    public static String convertMonthToThMonth(int number) {
        String[] thNumber = {"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};
        String result = "";
        try {
            result = thNumber[number - 1];
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc=" Check null data">   
    public static String checkNullDataNoDash(String data) {

        return data != null ? data.trim() : "";

    } 
    public static String checkNullDouble0(String data) {

        return data != "0.00" ? data.trim() : "";

    } 
    public static String checkNullData(String data) {

        return data != null ? data.trim() : "-";

    }

    public static String checkNullData(java.sql.Date data) {

        return data != null ? DateUtils.toThai(data) : "-";

    }

    public static String checkNullData(Integer data) {

        return (data != null && data > 0) ? data.toString() : "-";

    }
    
    public static Integer checkNullData1(Integer data) {

        return data != null ? data : 0;

    }

    public static double checkNullData(Double data) {

        return data != null ? data : 0;

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" Date">  
    public static String dateThai(String strDate) {
        String Months[] = {
            "ม.ค", "ก.พ", "มี.ค", "เม.ย",
            "พ.ค", "มิ.ย", "ก.ค", "ส.ค",
            "ก.ย", "ต.ค", "พ.ย", "ธ.ค"};

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", new Locale("th", "TH"));

        int year = 0, month = 0, day = 0;
        try {
            Date date = df.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return String.format("%s %s %s", day, Months[month], year + 543);
    }
    // </editor-fold> 

}
