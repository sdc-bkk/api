/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class InvoicePaymentModel extends InvoiceModel{
    
    private String paymentId;//เลขที่รายการ
    private String paymentStatus;//สถานะการชำระเงิน (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
    private String paymentMethod;//วิธีการชำระ (1 =เงินสด, 2 = บัตรเครดิต, 3 = บัตรเดบิต, 4 = ธนาณัติ, 5 =  เช็ค , 6 = QR Code ผ่านเครื่อง EDC)
    private String paymentDate;//วันที่รับชำระ รูปแบบจะเป็น dd/mm/yyyy 
    private String paymentTime;//เวลาที่รับชำระ รูปแบบจะเป็น hh:mm 
    private String paymentAmount;//จำนวนที่รับชำระ
    private String amount;//ยอดที่ชำระ 
    private String receiptNo;//เลขที่ใบเสร็จรับเงิน
    private String chequeDate;//วันที่เช็ค กรณีชำระด้วยเช็ค
    private String chequeNo;//เลขที่เช็ค กรณีชำระด้วยเช็ค
    private String chequeBank;//ธนาคาร กรณีชำระด้วยเช็ค
    private String chequeBranch;//สาขา กรณีชำระด้วยเช็ค
    private String invoiceDate;//วันที่ใบแจ้งชำระ
    private String invoiceNo;//เลขที่ใบเสร็จรับเงิน
    private String paymentChannel;
    private String paymentAt;
    private String paymentAtCode;
    private double totalAmount;//ยอดที่ชำระ 
    private String cusAddress;//ที่อยู่ลูกค้า
    private String officeAddress;//ที่อยู่สำนักงานเขต 

    public String getCusAddress() {
        return cusAddress;
    }

    public void setCusAddress(String cusAddress) {
        this.cusAddress = cusAddress;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }
    
    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentAt() {
        return paymentAt;
    }

    public void setPaymentAt(String paymentAt) {
        this.paymentAt = paymentAt;
    }

    public String getPaymentAtCode() {
        return paymentAtCode;
    }

    public void setPaymentAtCode(String paymentAtCode) {
        this.paymentAtCode = paymentAtCode;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
    
    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeBank() {
        return chequeBank;
    }

    public void setChequeBank(String chequeBank) {
        this.chequeBank = chequeBank;
    }

    public String getChequeBranch() {
        return chequeBranch;
    }

    public void setChequeBranch(String chequeBranch) {
        this.chequeBranch = chequeBranch;
    }    
    
}
