/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import service.FormExportService;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("formExport")
public class FormExportWebservice {

    @Context
    private UriInfo context;
    FormExportService service = new FormExportService();
    
    
    //<editor-fold defaultstate="collapsed" desc="ใบแจ้งชำระ">  
    @GET
    @Path("getInvPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getInvPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getInvoicePDF(request, response, "pdf", id, null);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ใบเสร็จรับเงิน">  
    @GET
    @Path("getReceiptPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getReceiptPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getReceiptPDF(request, response, "pdf", id, null);
    }
    //</editor-fold>
}
