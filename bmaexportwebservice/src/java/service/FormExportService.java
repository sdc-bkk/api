/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.InvoicePaymentModel;
import model.ResultModel;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import repository.FormExportBKKRepo;
import utility.AppConfig;
import utility.AppUtil;
import utility.DateUtil;
import viewModel.FileDataViewModel;
import viewModel.InvoiceViewModel;

/**
 *
 * @author User
 */
public class FormExportService {

    FormExportBKKRepo repo = new FormExportBKKRepo();

    public void getInvoicePDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        InvoiceViewModel data = new InvoiceViewModel();
        //           int id = Integer.parseInt(idStr);
    int id = AppUtil.decryptId(idStr);
        data = repo.getDataInvoice(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo.bkk");
        String bankList = appConfig.value("banklist");
        Map params = new HashMap();

        params.put("Logo", pathLogo); //logo
        params.put("BankList", bankList); //bankList
        params.put("OwnerName", data.getCusName()); // ชื่อผู้ประกอบการ
        params.put("CusAddress", data.getCusAddress()); // ชื่อผู้ประกอบการ
        params.put("BillNo", data.getBillNo()); // ชื่อผู้ประกอบการ
        params.put("Ref1", data.getRef1()); // อำเภอ
        params.put("Ref2", data.getRef2()); // อำเภอ
        params.put("DueDate", DateUtil.toFormatDateLongThai(data.getDueDate())); // จังหวัด
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getActionDate())); // จังหวัด
        params.put("OrgName", data.getOffice()); // จังหวัด

        params.put("Order1", "1"); // จังหวัด
        params.put("Detail1", data.getDetail1()); // จังหวัด
        params.put("Amount1", data.getAmount1()); // จังหวัด

        params.put("Order2", !"".equals(data.getDetail2()) ? "2" : ""); // จังหวัด
        params.put("Detail2", data.getDetail2()); // จังหวัด
        params.put("Amount2", !"0.00".equals(data.getAmount2()) ? data.getAmount2() : ""); // จังหวัด

        params.put("Order3", !"".equals(data.getDetail3()) ? "3" : ""); // จังหวัด
        params.put("Detail3", data.getDetail3()); // จังหวัด
        params.put("Amount3", !"0.00".equals(data.getAmount3()) ? data.getAmount3() : ""); // จังหวัด

        params.put("Order4", !"".equals(data.getDetail4()) ? "4" : ""); // จังหวัด
        params.put("Detail4", data.getDetail4()); // จังหวัด
        params.put("Amount4", !"0.00".equals(data.getAmount4()) ? data.getAmount4() : ""); // จังหวัด

        params.put("Order5", !"".equals(data.getDetail5()) ? "5" : ""); // จังหวัด
        params.put("Detail5", data.getDetail5()); // จังหวัด
        params.put("Amount5", !"0.00".equals(data.getAmount5()) ? data.getAmount5() : ""); // จังหวัด
        
        params.put("Amt", data.getAmount()); // จังหวัด
        params.put("TotalAmount", data.getTotalAmount()); // จังหวัด
        
        params.put("Barcode", data.getBarcode()); // จังหวัด        
        params.put("QRCode", data.getQrcode()); // จังหวัด

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        String pathReport = appConfig.value("export.invoice");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxrefund_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void getReceiptPDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        InvoicePaymentModel data = new InvoicePaymentModel();
//        int id = Integer.parseInt(idStr);
        int id = AppUtil.decryptId(idStr);
        data = repo.getReceiptPDF(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo.bkk");
        Map params = new HashMap();

        params.put("Logo", pathLogo); //logo
        params.put("ReportName", "เอกสารสำเนา (Copy)"); //สำเนา ต้นฉบับ
        params.put("ReceiptNo", data.getReceiptNo()); // ชื่อผู้ประกอบการ
        params.put("BillNo", data.getBillNo()); // ชื่อผู้ประกอบการ
        params.put("OwnerName", data.getCusName()); // ชื่อผู้ประกอบการ
        params.put("CusAddress", data.getCusAddress()==null?"":data.getCusAddress()); // ที่อยู่ผู้ประกอบการ
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getPaymentDate())); // อำเภอ
        params.put("OrgName", data.getOffice().contains("สำนักงานเขต") == true ? data.getOffice() : "สำนักงานเขต" + data.getOffice()); // ชื่อหน่วยงาน
        params.put("OrgAddress", data.getOfficeAddress()==null?"-":data.getOfficeAddress()); // ที่อยู่เขต

        params.put("Order1", "1"); // จังหวัด
        params.put("Detail1", data.getDetail1()); // จังหวัด
        params.put("Amount1", data.getAmount1()); // จังหวัด

        params.put("Order2", !"".equals(data.getDetail2()) ? "2" : ""); // จังหวัด
        params.put("Detail2", data.getDetail2()); // จังหวัด
        params.put("Amount2", !"0.00".equals(data.getAmount2()) ? data.getAmount2() : ""); // จังหวัด

        params.put("Order3", !"".equals(data.getDetail3()) ? "3" : ""); // จังหวัด
        params.put("Detail3", data.getDetail3()); // จังหวัด
        params.put("Amount3", !"0.00".equals(data.getAmount3()) ? data.getAmount3() : ""); // จังหวัด

        params.put("Order4", !"".equals(data.getDetail4()) ? "4" : ""); // จังหวัด
        params.put("Detail4", data.getDetail4()); // จังหวัด
        params.put("Amount4", !"0.00".equals(data.getAmount4()) ? data.getAmount4() : ""); // จังหวัด

        params.put("Order5", !"".equals(data.getDetail5()) ? "5" : ""); // จังหวัด
        params.put("Detail5", data.getDetail5()); // จังหวัด
        params.put("Amount5", !"0.00".equals(data.getAmount5()) ? data.getAmount5() : ""); // จังหวัด
        
        params.put("Amt", data.getAmount()); // จังหวัด
        params.put("TotalAmount", data.getTotalAmount()); // จังหวัด

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  

        String pathReport = appConfig.value("export.receipt");

        InputStream resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxrefund_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

}
