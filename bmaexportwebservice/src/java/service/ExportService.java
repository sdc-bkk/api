/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import viewModel.FileDataViewModel;

/**
 *
 * @author User
 */
public class ExportService {

    //<editor-fold defaultstate="collapsed" desc="exportTo....">  
    public static void exportPDF(JasperPrint jprint, HttpServletResponse response, String name) throws IOException {
        ServletOutputStream outputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(jprint, byteArrayOutputStream);

            String header = "inline;filename=\"" + name + ".pdf\"";
            header = new String(header.getBytes("UTF8"), "ISO8859-1");
            response.setHeader("Content-disposition", header);
            response.setContentType("application/pdf;charset=UTF-8");

            response.getOutputStream().write(byteArrayOutputStream.toByteArray());
            response.getOutputStream().flush();
            response.getOutputStream().close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    public static void exportPDF(List<JasperPrint> jprintList, HttpServletResponse response, String name) throws IOException {
        ServletOutputStream outputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            //Add the list as a Parameter
            exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jprintList);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
            exporter.exportReport();

            String header = "inline;filename=\"" + name + ".pdf\"";
            header = new String(header.getBytes("UTF8"), "ISO8859-1");
            response.setHeader("Content-disposition", header);
            response.setContentType("application/pdf;charset=UTF-8");

            response.getOutputStream().write(byteArrayOutputStream.toByteArray());
            response.getOutputStream().flush();
            response.getOutputStream().close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
 
    public static void exportWord(JasperPrint jprint, HttpServletResponse response, String name) throws IOException {
        ServletOutputStream outputStream = null;
        try {

            String header = "inline;filename=\"" + name + ".docx\"";
            header = new String(header.getBytes("UTF8"), "ISO8859-1");
            response.setHeader("Content-disposition", header);

            response.setContentType("aapplication/msword;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");

            JRDocxExporter exporter = new JRDocxExporter();

            exporter.setExporterInput(new SimpleExporterInput(jprint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));

            exporter.exportReport();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }

    }

    public static void exportExcel(JasperPrint jprint, HttpServletResponse response, String name) throws IOException {
        ServletOutputStream outputStream = null;
        try {

            String header = "attachment;filename=\"" + name + ".xlsx\"";
            header = new String(header.getBytes("UTF8"), "ISO8859-1");
            response.setHeader("Content-disposition", header);
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("UTF-8");

            outputStream = response.getOutputStream();

            ByteArrayOutputStream os = new ByteArrayOutputStream();

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jprint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setOnePagePerSheet(false);
            configuration.setRemoveEmptySpaceBetweenRows(true);
            configuration.setIgnoreGraphics(Boolean.FALSE);

            exporter.setConfiguration(configuration);

            exporter.exportReport();

            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }

    }
          
    public static void exportBase64(JasperPrint jprint, HttpServletResponse response, String name) throws IOException {
        ServletOutputStream outputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            JasperExportManager.exportReportToPdfStream(jprint, byteArrayOutputStream);

            String encoded = Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());

//            fileData.setFileType("pdf");
//            fileData.setFileContent(encoded);
//            fileData.setFileName(name + AppUtils.toDateTimeString(new Timestamp(System.currentTimeMillis())) + ".pdf");
//            fileData.setCreatedDateStr(AppUtils.toDateTimeString(new Timestamp(System.currentTimeMillis())));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
    
    public static void exportBase64(JasperPrint jprint, HttpServletResponse response, String name, FileDataViewModel fileData) throws IOException {
        ServletOutputStream outputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            JasperExportManager.exportReportToPdfStream(jprint, byteArrayOutputStream);

            String encoded = Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());

            fileData.setFileType("pdf");
            fileData.setFileContent(encoded);
            fileData.setFileName(name + ".pdf");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
    //</editor-fold>
}
