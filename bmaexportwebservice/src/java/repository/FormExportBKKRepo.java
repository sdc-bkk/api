/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.InvoicePaymentModel;
import repository.mapper.InvoiceMapper;
import repository.mapper.InvoicePaymentMapper;
import utility.ConnnectionDB_BKK;
import utility.PreparedStatementDB;
import viewModel.InvoiceViewModel;

/**
 *
 * @author User
 */
public class FormExportBKKRepo {
    
    private final ConnnectionDB_BKK connDB = new ConnnectionDB_BKK();
    
    public InvoiceViewModel getDataInvoice(int id) throws SQLException {

        InvoiceViewModel data = new InvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get invoice ">
            String sql = " SELECT * \n"
                    + "FROM INVOICE \n"
                    + " WHERE INVOICE_ID =?  ";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new InvoiceMapper(rs).mapData();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }
    
    public InvoicePaymentModel getReceiptPDF(int id) throws SQLException {

        InvoicePaymentModel data = new InvoicePaymentModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT * \n"
                    + "FROM INVOICE_PAYMENT \n"
                    + " WHERE TRANSACTION_ID =?  ";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new InvoicePaymentMapper(rs).mapData();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }
}
