/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import utility.AppUtil;
import viewModel.InvoiceViewModel;

/**
 *
 * @author User
 */
public class InvoiceMapper {

    private ResultSet rs = null;

    public InvoiceMapper() {

    }

    public InvoiceMapper(ResultSet rs) {
        this.rs = rs;
    }


    public InvoiceViewModel mapData() {

        InvoiceViewModel data = new  InvoiceViewModel();
        try {

            while (rs.next()) {
                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setCusAddress(AppUtil.checkNullData(rs.getString("CUS_ADDRESS")));
                data.setBillNo(AppUtil.checkNullData(rs.getString("BILL_NO")));

                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
                data.setRef2(AppUtil.checkNullData(rs.getString("REF2")));

                data.setDueDate(AppUtil.checkNullData(rs.getDate("DUE_DATE")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                
                data.setDetail1(AppUtil.checkNullData(rs.getString("DETAIL1")));
                data.setAmount1(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT1")));
                data.setDetail2(AppUtil.checkNullDataNoDash(rs.getString("DETAIL2")));
                data.setAmount2(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT2")));
                data.setDetail3(AppUtil.checkNullDataNoDash(rs.getString("DETAIL3")));
                data.setAmount3(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT3")));
                data.setDetail4(AppUtil.checkNullDataNoDash(rs.getString("DETAIL4")));
                data.setAmount4(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT4")));
                data.setDetail5(AppUtil.checkNullDataNoDash(rs.getString("DETAIL5")));
                data.setAmount5(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT5")));
                data.setAmount(AppUtil.numberFormatCommaTwoDigit(rs.getString("TOTAL_AMOUNT")));
                
                data.setTotalAmount(AppUtil.convertDoubleTwoDigit(rs.getDouble("TOTAL_AMOUNT")));
                
                String orgName =rs.getString("OFFICE")+" " + rs.getString("PART_OF_OFFICE")+" "+ rs.getString("TELEPHONE");
                
                data.setOffice(orgName);                
                
                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                
                data.setBarcode(AppUtil.checkNullData(rs.getString("BARCODE")));                
                data.setQrcode(AppUtil.checkNullData(rs.getString("QRCODE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
}
