/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.PaymentMethodEnum;
import enumeration.PaymentStatusEnum;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.InvoicePaymentModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;

/**
 *
 * @author User
 */
public class InvoicePaymentMapper {

    private ResultSet rs = null;

    public InvoicePaymentMapper() {

    }

    public InvoicePaymentMapper(ResultSet rs) {
        this.rs = rs;
    }

    ///////////// bma-back /////////////////
    public InvoicePaymentModel mapData() {

        InvoicePaymentModel data = new InvoicePaymentModel();

        try {

            while (rs.next()) {
                data.setPaymentId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
                data.setBillNo(AppUtil.checkNullData(rs.getString("BILL_NO")));
                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
                data.setRef2(AppUtil.checkNullData(rs.getString("REF2")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setCusAddress(AppUtil.checkNullData(rs.getString("CUS_ADDRESS")));
                if (rs.getDate("DUE_DATE") != null) {
                    data.setDueDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
                } else {
                    data.setDueDate("");
                }

                data.setDetail1(AppUtil.checkNullData(rs.getString("DETAIL1")));
                data.setAmount1(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT1")));
                data.setDetail2(AppUtil.checkNullDataNoDash(rs.getString("DETAIL2")));
                data.setAmount2(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT2")));
                data.setDetail3(AppUtil.checkNullDataNoDash(rs.getString("DETAIL3")));
                data.setAmount3(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT3")));
                data.setDetail4(AppUtil.checkNullDataNoDash(rs.getString("DETAIL4")));
                data.setAmount4(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT4")));
                data.setDetail5(AppUtil.checkNullDataNoDash(rs.getString("DETAIL5")));
                data.setAmount5(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT5")));
                data.setAmount(AppUtil.numberFormatCommaTwoDigit(rs.getString("TOTAL_AMOUNT")));
                
                data.setTotalAmount(AppUtil.convertDoubleTwoDigit(rs.getDouble("TOTAL_AMOUNT")));

                data.setOffice(AppUtil.checkNullData(rs.getString("OFFICE")));
                data.setPartOfOffice(AppUtil.checkNullData(rs.getString("PART_OF_OFFICE")));
                data.setTelephone(AppUtil.checkNullData(rs.getString("TELEPHONE")));

                // </editor-fold>
//                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
//                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
//                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                
                if (rs.getDate("INVOICE_DATE") != null) {
                    data.setInvoiceDate(DateUtils.toThai(rs.getDate("INVOICE_DATE")));
                } else {
                    data.setInvoiceDate("");
                }
//                data.setTransactionId(AppUtil.encryptId(rs.getInt("TRANSACTION_ID")));
//                data.setBmaBillNo(AppUtil.checkNullData(rs.getString("BMA_BILL_NO")));
//                
//                data.setBarcode(AppUtil.checkNullData(rs.getString("BARCODE")));
//                data.setQrcode(AppUtil.checkNullData(rs.getString("QRCODE")));

//                if (rs.getString("PAYMENT_STATUS") != null) {
//                    data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
//                } else {
//                    data.setPaymentStatus("");
//                }
//                if (rs.getString("PAYMENT_METHOD") != null) {
//                    data.setPaymentMethod(PaymentMethodEnum.getDisplayName(rs.getInt("PAYMENT_METHOD")));
//                } else {
//                    data.setPaymentMethod("");
//                }
                
                if (rs.getDate("PAYMENT_DATE") != null) {
                    data.setPaymentDate(DateUtils.toThai(rs.getDate("PAYMENT_DATE")));
                } else {
                    data.setPaymentDate("");
                }
                data.setPaymentTime(AppUtil.checkNullData(rs.getString("PAYMENT_TIME")));
                data.setPaymentAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("PAYMENT_AMOUNT")));                
                data.setReceiptNo(AppUtil.checkNullData(rs.getString("RECEIPT_NO")));

//                if (rs.getDate("CHEQUE_DATE") != null) {
//                    data.setChequeDate(DateUtils.toThai(rs.getDate("CHEQUE_DATE")));
//                } else {
//                    data.setChequeDate("");
//                }              
//                data.setChequeNo(AppUtil.checkNullData(rs.getString("CHEQUE_NO")));
//                data.setChequeBank(AppUtil.checkNullData(rs.getString("CHEQUE_BANK")));
//                data.setChequeBranch(AppUtil.checkNullData(rs.getString("CHEQUE_BRANCH")));
//                data.setInvoiceNo(AppUtil.checkNullData(rs.getString("INVOICE_NO")));
//                
//                data.setPaymentChannel(AppUtil.checkNullData(rs.getString("PAYMENT_CHANNEL")));
//                data.setPaymentAt(AppUtil.checkNullData(rs.getString("PAYMENT_AT")));
//                data.setPaymentAtCode(AppUtil.checkNullData(rs.getString("PAYMENT_AT_CODE")));
            }

        } catch (SQLException e) {
        }

        return data;

    }

}
