/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.ContentOfficeModel;
import repository.mapper.ContentOfficeMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.ContentOfficeViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class ContentOfficeRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentOfficeViewModel>> getList(ResultPage page, FilterContentModel filter) throws SQLException {

        ResultData<List<ContentOfficeViewModel>> resultData = new ResultData<List<ContentOfficeViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM CONTENT_OFFICE "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNull(filter.getActive())) {
                sql += " AND (IS_ACTIVE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND OFFICE_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                sql += " AND MOBILE LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                sql += " AND EMAIL LIKE ? ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("officeName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                ps.setString(i++, "%" + filter.getMobile() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                ps.setString(i++, "%" + filter.getEmail() + "%");
            }

            rs = ps.executeQuery();

            List<ContentOfficeViewModel> dataList = null;
            dataList = new ContentOfficeMapper(rs).mapList();

            if (dataList != null) {
                resultData.setResult(dataList);

                if (page != null) {
                    resultData.setResultPage(ps.getResultPage());
                }

            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " OFFICE_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("officeName")) {
                str = " OFFICE_NAME ";
            } else if (orderBy.equalsIgnoreCase("mobile")) {
                str = " MOBILE ";
            } else if (orderBy.equalsIgnoreCase("email")) {
                str = " EMAIL ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " OFFICE_NAME ";
            }
        }

        return str;

    }

    public ResultData<ContentOfficeViewModel> getData(int id) throws SQLException {

        ResultData<ContentOfficeViewModel> resultData = new ResultData<ContentOfficeViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM CONTENT_OFFICE "
                       + " WHERE OFFICE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            ContentOfficeViewModel data = new  ContentOfficeMapper(rs).mapData();

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(ContentOfficeModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql">
            if (data.getOfficeId()== 0) {

                sql = "INSERT INTO CONTENT_OFFICE ( "
                        + "  OFFICE_NAME "
                        + ", ADDRESS"
                        + ", MOBILE"
                        + ", EMAIL"
                        + ", FAX"
                        + ", IS_ACTIVE"
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?,?,?, SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"OFFICE_ID"});

            } else {

                sql = "UPDATE CONTENT_OFFICE SET "
                        + "  OFFICE_NAME = ? "
                        + ", ADDRESS = ? "
                        + ", MOBILE = ? "
                        + ", EMAIL = ? "
                        + ", FAX = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE OFFICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getOfficeName());
            ps.setString(i++, data.getAddress());
            ps.setString(i++, data.getMobile());
            ps.setString(i++, data.getEmail());
            ps.setString(i++, data.getFax());
            ps.setBoolean(i++, data.getActive());

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getOfficeId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setOfficeId(id);
            } else {

                ps.setInt(i++, data.getOfficeId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean duplicate(ContentOfficeViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM CONTENT_OFFICE "
                    + " WHERE (? = 0 OR OFFICE_ID <> ?) "
                    + " AND (OFFICE_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(data.getOfficeId()));
            ps.setInt(i++, AppUtil.decryptId(data.getOfficeId()));
            ps.setString(i++, data.getOfficeName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public ResultData deleteData(List<Integer> idList, FilterContentModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete ContentOffice by id list">
            sql = " DELETE FROM CONTENT_OFFICE "
                + " WHERE OFFICE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

}
