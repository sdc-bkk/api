/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.ContentFAQModel;
import repository.mapper.ContentFAQMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.ContentFAQViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class ContentFAQRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentFAQViewModel>> getList(ResultPage page, FilterContentModel filter) throws SQLException {

        ResultData<List<ContentFAQViewModel>> resultData = new ResultData<List<ContentFAQViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM CONTENT_FAQ "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNull(filter.getActive())) {
                sql += " AND (IS_ACTIVE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND FAQ_NAME LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("faqName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            rs = ps.executeQuery();

            List<ContentFAQViewModel> dataList = null;
            dataList = new ContentFAQMapper(rs).mapList();

            if (dataList != null) {
                resultData.setResult(dataList);

                if (page != null) {
                    resultData.setResultPage(ps.getResultPage());
                }

            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " FAQ_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("faqName")) {
                str = " FAQ_NAME ";
            } else if (orderBy.equalsIgnoreCase("faqQuestion")) {
                str = " FAQ_QUESTION ";
            } else if (orderBy.equalsIgnoreCase("faqAnswer")) {
                str = " FAQ_ANSWER ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " FAQ_NAME ";
            }
        }

        return str;

    }

    public ResultData<ContentFAQViewModel> getData(int id) throws SQLException {

        ResultData<ContentFAQViewModel> resultData = new ResultData<ContentFAQViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM CONTENT_FAQ "
                    + " WHERE FAQ_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            ContentFAQViewModel data = new ContentFAQMapper(rs).mapData();

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(ContentFAQModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql">
            if (data.getFaqId()== 0) {

                sql = "INSERT INTO CONTENT_FAQ ( "
                        + "  FAQ_NAME "
                        + ", FAQ_QUESTION"
                        + ", FAQ_ANSWER"
                        + ", IS_ACTIVE"
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?, SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"FAQ_ID"});

            } else {

                sql = "UPDATE CONTENT_FAQ SET "
                        + "  FAQ_NAME = ? "
                        + ", FAQ_QUESTION = ? "
                        + ", FAQ_ANSWER = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE FAQ_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getFaqName());
            ps.setString(i++, data.getFaqQuestion());
            ps.setString(i++, data.getFaqAnswer());
            ps.setBoolean(i++, data.getActive());

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getFaqId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setFaqId(id);
            } else {

                ps.setInt(i++, data.getFaqId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean duplicate(ContentFAQViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM CONTENT_FAQ "
                    + " WHERE (? = 0 OR FAQ_ID <> ?) "
                    + " AND (FAQ_QUESTION LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(data.getFaqId()));
            ps.setInt(i++, AppUtil.decryptId(data.getFaqId()));
            ps.setString(i++, data.getFaqQuestion());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public ResultData deleteData(List<Integer> idList, FilterContentModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete ContentFAQ by id list">
            sql = " DELETE FROM CONTENT_FAQ "
                    + " WHERE FAQ_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

}
