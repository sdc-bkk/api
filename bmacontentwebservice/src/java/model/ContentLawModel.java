/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author User
 */
public class ContentLawModel extends BaseClassModel{    
    private int lawId;
    private String lawName;
    private List<ContentLawFileModel> lawCoverList;
    private List<ContentLawFileModel> lawFileList;
    private List<ContentLawFileModel> lawImageList;

    public int getLawId() {
        return lawId;
    }

    public void setLawId(int lawId) {
        this.lawId = lawId;
    }

    public String getLawName() {
        return lawName;
    }

    public void setLawName(String lawName) {
        this.lawName = lawName;
    }

    public List<ContentLawFileModel> getLawCoverList() {
        return lawCoverList;
    }

    public void setLawCoverList(List<ContentLawFileModel> lawCoverList) {
        this.lawCoverList = lawCoverList;
    }

    public List<ContentLawFileModel> getLawFileList() {
        return lawFileList;
    }

    public void setLawFileList(List<ContentLawFileModel> lawFileList) {
        this.lawFileList = lawFileList;
    }

    public List<ContentLawFileModel> getLawImageList() {
        return lawImageList;
    }

    public void setLawImageList(List<ContentLawFileModel> lawImageList) {
        this.lawImageList = lawImageList;
    }

}
