/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import model.ContentOfficeModel;
import repository.ContentOfficeRepo;
import repository.mapper.ContentOfficeMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.ContentOfficeViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class ContentOfficeService {
    
    ContentOfficeRepo repo = new ContentOfficeRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentOfficeViewModel>> getList(FilterContentModel filter) {

        ResultData<List<ContentOfficeViewModel>> resultData = new ResultData<List<ContentOfficeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ContentOfficeViewModel> getData(FilterContentModel filter) {

        ResultData<ContentOfficeViewModel> resultData = new ResultData<ContentOfficeViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = repo.getData(id);
            
            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getOfficeId())) {
                resultData.setResult(new ContentOfficeViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(ContentOfficeViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        ContentOfficeModel dataSave = new ContentOfficeModel();

        try {
            boolean resultDuplicate = repo.duplicate(data);

            if (!resultDuplicate) {
                ContentOfficeMapper mapper = new ContentOfficeMapper();
                dataSave = mapper.mapSaveData(data);
                resultData = repo.save(dataSave);
            } else {
                resultData.setResult(false);
                resultData.setStatus("false");
                resultData.setResultMessage(message.getMessage("message.contentOffice.duplicate"));
            }

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteData(FilterContentModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            resultData = repo.deleteData(idDeleteList, filter);           

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }
}
