/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import model.ContentTypeModel;
import repository.ContentTypeRepo;
import repository.mapper.ContentTypeMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.ContentTypeViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class ContentTypeService {

    ContentTypeRepo repo = new ContentTypeRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentTypeViewModel>> getList(FilterContentModel filter) {

        ResultData<List<ContentTypeViewModel>> resultData = new ResultData<List<ContentTypeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ContentTypeViewModel> getData(FilterContentModel filter) {

        ResultData<ContentTypeViewModel> resultData = new ResultData<ContentTypeViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = repo.getData(id);
            
            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getContentTypeId())) {
                resultData.setResult(new ContentTypeViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(ContentTypeViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        ContentTypeModel dataSave = new ContentTypeModel();

        try {
            boolean resultDuplicate = repo.duplicate(data);

            if (!resultDuplicate) {
                ContentTypeMapper mapper = new ContentTypeMapper();
                dataSave = mapper.mapSaveData(data);
                resultData = repo.save(dataSave);
            } else {
                resultData.setResult(false);
                resultData.setStatus("false");
                resultData.setResultMessage(message.getMessage("message.contentType.duplicate"));
            }

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteData(FilterContentModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            resultData = repo.deleteData(idDeleteList, filter);           

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<ContentTypeViewModel>> getDrpList() {     
        ResultData<List<ContentTypeViewModel>> resultData = new ResultData<List<ContentTypeViewModel>>();
        ResultPage resultPage = null;

        try {

            FilterContentModel filter = new FilterContentModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);
            
            resultData = repo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

}
