/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.ContentLawFileModel;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import viewModel.ContentLawFileViewModel;

/**
 *
 * @author Sirichai
 */
public class FileUtil {

    private final static String filePathRoot = new AppConfig().value("file.path.root");
    private final static String filePathContent = new AppConfig().value("file.path.content");

    public FileUtil() {

    }

    public static void main(String[] args) throws Exception {
        
        FileUtil fileUtil = new FileUtil();
        String fileName = "saveDefineAuditDoc.pdf";
        String pathFile = "14158953-f5fe-4a66-a680-14aa099da5a5.pdf";
        List<ContentLawFileViewModel> list = fileUtil.convertPdfToImage(fileName, pathFile);
        String temp = "";
    }
    
    public List<ContentLawFileViewModel> convertPdfToImage(String fileName, String pathFile) {
        List<ContentLawFileViewModel> resultImageList = new ArrayList<>();
        Pattern pattern = Pattern.compile(".*.pdf$");
        Matcher matcher = pattern.matcher(pathFile);

        if (matcher.find()) {

            //cutPage from PDF file
            File temp = new File(filePathRoot + "\\" + filePathContent + "\\" + pathFile);
            
            try (final PDDocument document = PDDocument.load(temp)) {
                PDFRenderer pdfRenderer = new PDFRenderer(document);
                int numPage = document.getPages().getCount();
                
                for (int page = 0; page < numPage; ++page) {
                    
                    ContentLawFileViewModel data = new ContentLawFileViewModel();
                    BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 100, ImageType.RGB);
                    String uuid = UUID.randomUUID().toString();
                    String pathFileImge = filePathRoot + "\\" + filePathContent + "\\" + uuid + ".jpg";
                    String fileNameNew = (page+1)+"-|-"+ fileName + ".jpg";
                    File chkFile = new File(pathFileImge);
                    if (chkFile.exists()) {
                        chkFile.delete();
                    }
                    ImageIOUtil.writeImage(bim, pathFileImge, 100);
                    
                    data.setFileName(fileNameNew);
                    data.setFileType("I"); //I=imge
                    data.setPathFile(uuid + ".jpg");
                    resultImageList.add(data);
                    
                }
                
                document.close();
            } catch (IOException e) {
                System.err.println("Exception while trying to create pdf document - " + e);
            }
            
        }
        
        return resultImageList;
    }

}
