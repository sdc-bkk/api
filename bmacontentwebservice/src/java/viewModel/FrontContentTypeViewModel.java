/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class FrontContentTypeViewModel {
    
    private String contentTypeId;
    private String contentTypeName;
    private String contentFormat;
    private List<ContentViewModel> contentList;

    public String getContentTypeId() {
        return contentTypeId;
    }

    public void setContentTypeId(String contentTypeId) {
        this.contentTypeId = contentTypeId;
    }

    public String getContentTypeName() {
        return contentTypeName;
    }

    public void setContentTypeName(String contentTypeName) {
        this.contentTypeName = contentTypeName;
    }

    public String getContentFormat() {
        return contentFormat;
    }

    public void setContentFormat(String contentFormat) {
        this.contentFormat = contentFormat;
    }

    public List<ContentViewModel> getContentList() {
        return contentList;
    }

    public void setContentList(List<ContentViewModel> contentList) {
        this.contentList = contentList;
    }
    
    
}
