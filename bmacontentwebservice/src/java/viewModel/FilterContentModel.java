/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import utility.AppConfig;

/**
 *
 * @author User
 */
public class FilterContentModel extends BaseClassFilterModel {
    private String id;  
    private String name;    
    private String contentTypeId;  
    private String contentFormat;  
    private String timeDisplay;    
    
    private String mobile;
    private String email;   
    
    private String startDate;    
    private String endDate;    
    
    private int itemPerPageMain = Integer.parseInt(new AppConfig().value("config.itemPerPage"));
    private int itemPerPageSub = Integer.parseInt(new AppConfig().value("config.itemPerPage"));

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    

    public String getContentTypeId() {
        return contentTypeId;
    }

    public void setContentTypeId(String contentTypeId) {
        this.contentTypeId = contentTypeId;
    }

    public String getContentFormat() {
        return contentFormat;
    }

    public void setContentFormat(String contentFormat) {
        this.contentFormat = contentFormat;
    }

    public String getTimeDisplay() {
        return timeDisplay;
    }

    public void setTimeDisplay(String timeDisplay) {
        this.timeDisplay = timeDisplay;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getItemPerPageMain() {
        return itemPerPageMain;
    }

    public void setItemPerPageMain(int itemPerPageMain) {
        this.itemPerPageMain = itemPerPageMain;
    }

    public int getItemPerPageSub() {
        return itemPerPageSub;
    }

    public void setItemPerPageSub(int itemPerPageSub) {
        this.itemPerPageSub = itemPerPageSub;
    }
    
}
