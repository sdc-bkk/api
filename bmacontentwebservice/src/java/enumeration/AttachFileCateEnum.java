/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

/**
 *
 * @author Prapaporn
 */
public enum AttachFileCateEnum {

    FILE(1, "F"),
    IMAGE(2,"I"),
    COVER(3, "C");

    private final int value;

    private final String displayName;

    AttachFileCateEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int value() {
        return value;
    }

    public String display() {
        return displayName;
    }
}
