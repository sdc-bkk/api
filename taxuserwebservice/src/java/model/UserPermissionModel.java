/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class UserPermissionModel {

    private int roleId;
    private UserRoleModel role; //กลุ่มผู้ใช้งาน
    private int menuId;
    private MenuModel menu; //เมนู
    
    private boolean view; //ได้รับสิทธิ์การดูข้อมูล(0=ไม่,1=ใช่)
    private boolean add; //ได้รับสิทธิ์การเพิ่มข้อมูล(0=ไม่,1=ใช่)
    private boolean edit; //ได้รับสิทธิ์การแก้ไขข้อมูล(0=ไม่,1=ใช่)
    private boolean delete; //ได้รับสิทธิ์การลบข้อมูล(0=ไม่,1=ใช่)
    private boolean approve; //ได้รับสิทธิ์การอนุมัติข้อมูล(0=ไม่,1=ใช่)

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public UserRoleModel getRole() {
        return role;
    }

    public void setRole(UserRoleModel role) {
        this.role = role;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public MenuModel getMenu() {
        return menu;
    }

    public void setMenu(MenuModel menu) {
        this.menu = menu;
    }

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

}
