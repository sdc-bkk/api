/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.UserRoleModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.UserPermissionFilterModel;
import model.filter.UsersFilterModel;
import repository.UserPermissionRepo;
import repository.mapper.UserPermissionMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.UserPermissionViewModel;
import viewModel.UserRoleViewModel;

/**
 *
 * @author Sirichai
 */
public class UserPermissionService {

    UserPermissionRepo userPermissionRepo = new UserPermissionRepo();

    public ResultData<List<UserRoleViewModel>> getListAll(UserPermissionFilterModel filter) {

        ResultData<List<UserRoleViewModel>> data = new ResultData<List<UserRoleViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = userPermissionRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<UserRoleViewModel> getData(UserPermissionFilterModel filter) {

        ResultData<UserRoleViewModel> data = new ResultData<UserRoleViewModel>();

        try {

            if(!AppUtil.isNullAndSpace(filter.getRoleId())){
                
                //มี RoleId ส่งเข้ามา
                int id = AppUtil.decryptId(filter.getRoleId());
                if (userPermissionRepo.hasData(id)) {

                    data = userPermissionRepo.getData(id);

                    UserRoleViewModel temp = data.getResult();
                    List<UserPermissionViewModel> menuList = getListMenuByRoleId("0", temp.getRoleId());
                    temp.setUserPermissionList(menuList);
                    data.setResult(temp);

                } else {

                    data.setResult(null);
                    MessageBundleUtil message = new MessageBundleUtil();
                    data.setMessage(message.getMessage("message.userPermission.hasData"));

                }
                
            } else {

                //ไม่มี RoleId ส่งเข้ามา
                UserRoleViewModel temp = new UserRoleViewModel();
                List<UserPermissionViewModel> menuList = getListMenuByRoleId("0", "0");
                temp.setUserPermissionList(menuList);
                data.setResult(temp);

            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(UserRoleViewModel userRoleView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        UserRoleModel userRole = new UserRoleModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            UserPermissionFilterModel filter = new UserPermissionFilterModel();
            filter.setRoleId(userRoleView.getRoleId());
            filter.setRoleName(userRoleView.getRoleName());

            boolean resultDuplicate = userPermissionRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                UserPermissionMapper mapper = new UserPermissionMapper();

                userRole = mapper.mapFull(userRoleView);

                boolean resultSave = userPermissionRepo.save(userRole);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.userPermission.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(UserPermissionFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = userPermissionRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<UserRoleViewModel>> getDrpList() {

        ResultData<List<UserRoleViewModel>> data = new ResultData<List<UserRoleViewModel>>();

        try {

            ResultPage resultPage = null;
            
            UserPermissionFilterModel filter = new UserPermissionFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = userPermissionRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<List<UserRoleViewModel>> getListPermission(UserPermissionFilterModel filter) {

        ResultData<List<UserRoleViewModel>> data = new ResultData<List<UserRoleViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = userPermissionRepo.getListAll(resultPage, filter);
            
            List<UserRoleViewModel> tempList = data.getResult();
            for (UserRoleViewModel role : tempList) {
                List<UserPermissionViewModel> menuList = userPermissionRepo.getListPermission(role.getRoleId());
                role.setUserPermissionList(menuList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
    public ResultData<List<UserPermissionViewModel>> getListMenu(UserPermissionFilterModel filter) {

        ResultData<List<UserPermissionViewModel>> data = new ResultData<List<UserPermissionViewModel>>();

        try {

            //List<UserPermissionViewModel> menuList = userPermissionRepo.getListMenu("0", filter.getRoleId());
            List<UserPermissionViewModel> menuList = getListMenuByRoleId("0", filter.getRoleId());
            data.setResult(menuList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
    private List<UserPermissionViewModel> getListMenuByRoleId(String refMenuId, String roleId) {

        List<UserPermissionViewModel> menuList = new ArrayList<UserPermissionViewModel>();

        try {
            
            menuList = userPermissionRepo.getListMenu(refMenuId, roleId);

            if (menuList != null && !menuList.isEmpty()) {

                for (UserPermissionViewModel menu : menuList) {
                    menu.setSubMenu(getListMenuByRoleId(menu.getMenuId(), roleId));
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return menuList;
    }
        
    public  ResultData<List<UserPermissionViewModel>> getMenuAndPermissionListByUserId(UsersFilterModel filter) {

        ResultData<List<UserPermissionViewModel>> data = new ResultData<List<UserPermissionViewModel>>();

        try {

            filter.setRefMenuId("0");
            filter.setIsActive(true);
            List<UserPermissionViewModel> menuList = getChildMenuAndPermissionList(filter);

            data.setResult(menuList);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return data;

    }
    
    private List<UserPermissionViewModel> getChildMenuAndPermissionList(UsersFilterModel model) {

        List<UserPermissionViewModel> menuList = new ArrayList<UserPermissionViewModel>();

        try {

             menuList = userPermissionRepo.getMenuAndPermissionListByLevel(model);

            if (!menuList.isEmpty()) {

                for (UserPermissionViewModel menu : menuList) {
                    model.setRefMenuId(menu.getMenuId());
                    menu.setSubMenu(getChildMenuAndPermissionList(model));
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return menuList;
    }
    
}
