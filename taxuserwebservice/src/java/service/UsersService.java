/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.UsersModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.UsersFilterModel;
import repository.UserPermissionRepo;
import repository.UsersRepo;
import repository.mapper.UsersMapper;
import utility.AES256;
import utility.AppConfig;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.UserPermissionViewModel;
import viewModel.UserRoleViewModel;
import viewModel.UsersViewModel;

/**
 *
 * @author Sirichai
 */
public class UsersService {

    UsersRepo usersRepo = new UsersRepo();

    public ResultData<List<UsersViewModel>> getListAll(UsersFilterModel filter) {

        ResultData<List<UsersViewModel>> data = new ResultData<List<UsersViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = usersRepo.getListAll(resultPage, filter);

            List<UsersViewModel> tempList = data.getResult();
            for (UsersViewModel users : tempList) {
                List<UserRoleViewModel> menuList = usersRepo.getListUserInRole(users.getUserId());
                users.setUserRoleList(menuList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<UsersViewModel> getData(UsersFilterModel filter) {

        ResultData<UsersViewModel> data = new ResultData<UsersViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getUserId());
            if (usersRepo.hasData(id)) {

                data = usersRepo.getData(id);

                UsersViewModel users = data.getResult();
                List<UserRoleViewModel> roleList = usersRepo.getListUserInRole(users.getUserId());

                UserPermissionRepo userPermissionRepo = new UserPermissionRepo();
                for (UserRoleViewModel role : roleList) {
                    List<UserPermissionViewModel> menuList = userPermissionRepo.getListPermission(role.getRoleId());
                    role.setUserPermissionList(menuList);
                }

                users.setUserRoleList(roleList);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.users.hasData"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public UsersViewModel getDataUser(UsersFilterModel filter) {

        ResultData<UsersViewModel> data = new ResultData<UsersViewModel>();

        try {
            data = usersRepo.getData(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data.getResult();
    }

    public ResultData<Boolean> saveData(UsersViewModel usersView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        UsersModel users = new UsersModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            UsersFilterModel filter = new UsersFilterModel();
            filter.setUserId(usersView.getUserId());
            filter.setUserName(usersView.getUserName());

            boolean resultDuplicate = usersRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                UsersMapper mapper = new UsersMapper();

                users = mapper.mapFull(usersView);

                boolean resultSave = usersRepo.save(users);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.users.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(UsersFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = usersRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);

            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<UsersViewModel>> getDrpList() {

        ResultData<List<UsersViewModel>> data = new ResultData<List<UsersViewModel>>();

        try {

            ResultPage resultPage = null;

            UsersFilterModel filter = new UsersFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = usersRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<UsersViewModel> getDataSSO(UsersFilterModel filter) {

        ResultData<UsersViewModel> data = new ResultData<UsersViewModel>();

        try {

            String userName = filter.getUserName();
            data = usersRepo.getDataSSO(userName);

            String temp = "";
            if (data != null && data.getResult() != null && data.getResult().getUserName() != null) {
                temp = data.getResult().getUserName();
            }

            if (AppUtil.isNullAndSpace(temp)) {
                data.setResult(null);
                data.setStatus("false");
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.usersSSO.hasData"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> authenUser(UsersFilterModel filter) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            String password = filter.getPassword();
            if (usersRepo.hasDataByUserNamePassword(userName.trim(), password.trim())) {
                AppConfig appConfig = new AppConfig();
                String secretKey = appConfig.value("secretKey");
                String encryptedString = AES256.encrypt(secretKey, userName.trim());

                resultData.setResult(true);
                resultData.setToken(encryptedString);

            } else {
                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.authenNoSuccess"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }
}
