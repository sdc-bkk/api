/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.UserPermissionFilterModel;
import service.UserPermissionService;
import viewModel.UserRoleViewModel;
import viewModel.UserPermissionViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("userPermission")
public class UserPermissionWebservice {

    @Context
    private UriInfo context;

    UserPermissionService userPermissionService = new UserPermissionService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(UserPermissionFilterModel filter) {

        try {

            ResultData<List<UserRoleViewModel>> data = userPermissionService.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(UserPermissionFilterModel filter) {

        try {

            ResultData<UserRoleViewModel> data = userPermissionService.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(UserRoleViewModel userRoleView) {

        try {

            ResultData<Boolean> data = userPermissionService.saveData(userRoleView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(UserPermissionFilterModel filter) {

        try {

            return new Gson().toJson(userPermissionService.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpList")
    public String getDrpList() {

        try {

            ResultData<List<UserRoleViewModel>> data = userPermissionService.getDrpList();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListPermission")
    public String getListPermission(UserPermissionFilterModel filter) {

        try {

            ResultData<List<UserRoleViewModel>> data = userPermissionService.getListPermission(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListMenu")
    public String getListMenu(UserPermissionFilterModel filter) {

        try {

            ResultData<List<UserPermissionViewModel>> data = userPermissionService.getListMenu(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

}
