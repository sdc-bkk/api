/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.UserPermissionModel;
import model.UserRoleModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.UserPermissionViewModel;
import viewModel.UserRoleViewModel;

/**
 *
 * @author Sirichai
 */
public class UserPermissionMapper {

    private ResultSet rs = null;

    public UserPermissionMapper() {

    }

    public UserPermissionMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<UserRoleViewModel> mapFullList() {

        List<UserRoleViewModel> dataList = new ArrayList<UserRoleViewModel>();

        try {

            while (rs.next()) {

                UserRoleViewModel data = new UserRoleViewModel();

                data.setRoleId(AppUtil.encryptId(rs.getInt("ROLE_ID")));
                data.setRoleName(rs.getString("ROLE_NAME"));
                data.setShortName(rs.getString("SHORT_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<UserRoleViewModel> mapFullDrpList() {

        List<UserRoleViewModel> dataList = new ArrayList<UserRoleViewModel>();

        try {

            while (rs.next()) {

                UserRoleViewModel data = new UserRoleViewModel();

                data.setRoleId(AppUtil.encryptId(rs.getInt("ROLE_ID")));
                data.setRoleName(rs.getString("ROLE_NAME"));
                data.setShortName(rs.getString("SHORT_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public UserRoleViewModel mapFull() {

        UserRoleViewModel data = new UserRoleViewModel();

        try {

            while (rs.next()) {

                data.setRoleId(AppUtil.encryptId(rs.getInt("ROLE_ID")));
                data.setRoleName(rs.getString("ROLE_NAME"));
                data.setShortName(rs.getString("SHORT_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public UserRoleModel mapFull(UserRoleViewModel dataView) {

        UserRoleModel data = new UserRoleModel();

        try {
            data.setRoleId(AppUtil.decryptId(dataView.getRoleId()));
            data.setRoleName(dataView.getRoleName());
            data.setShortName(dataView.getShortName());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

            if (dataView.getUserPermissionList() != null) {

                if (!dataView.getUserPermissionList().isEmpty()) {

                    List<UserPermissionViewModel> userPermissionView = dataView.getUserPermissionList();
                    List<UserPermissionModel> userPermissionList = new ArrayList<UserPermissionModel>();
                    userPermissionList = mapFullListUserPermission(userPermissionView, userPermissionList);

                    data.setUserPermissionList(userPermissionList);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    private List<UserPermissionModel> mapFullListUserPermission(List<UserPermissionViewModel> dataViewList, List<UserPermissionModel> dataList) {

        try {

            for (UserPermissionViewModel dataView : dataViewList) {

                UserPermissionModel data = new UserPermissionModel();

                data.setMenuId(AppUtil.decryptId(dataView.getMenuId()));
                //data.setRoleId(AppUtil.decryptId(dataView.getRoleId()));

                data.setView(dataView.getView());
                data.setAdd(dataView.getAdd());
                data.setEdit(dataView.getEdit());
                data.setDelete(dataView.getDelete());
                data.setApprove(dataView.getApprove());

                dataList.add(data);
                
                if(dataView.getSubMenu() != null && !dataView.getSubMenu().isEmpty()){
                    dataList = mapFullListUserPermission(dataView.getSubMenu(), dataList);
                }
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public List<UserPermissionViewModel> mapFullListPermission() {

        List<UserPermissionViewModel> dataList = new ArrayList<UserPermissionViewModel>();

        try {

            while (rs.next()) {

                UserPermissionViewModel data = new UserPermissionViewModel();

                data.setMenuId(AppUtil.encryptId(rs.getInt("MENU_ID")));
                data.setMenuName(rs.getString("MENU_NAME"));
                
                data.setView(rs.getBoolean("IS_VIEW"));
                data.setAdd(rs.getBoolean("IS_ADD"));
                data.setEdit(rs.getBoolean("IS_EDIT"));
                data.setDelete(rs.getBoolean("IS_DELETE"));
                data.setApprove(rs.getBoolean("IS_APPROVE"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    
    public List<UserPermissionViewModel> mapFullListMenu() {

        List<UserPermissionViewModel> dataList = new ArrayList<UserPermissionViewModel>();

        try {

            while (rs.next()) {

                UserPermissionViewModel data = new UserPermissionViewModel();

                data.setMenuId(AppUtil.encryptId(rs.getInt("MENU_ID")));
                data.setMenuName(rs.getString("MENU_NAME"));
                data.setIcon(rs.getString("ICON"));
                data.setPath(rs.getString("PATH"));
                
                data.setRefMenuId(AppUtil.encryptId(rs.getInt("REF_MENU_ID")));
                data.setLevelMenu(rs.getInt("LEVEL_MENU"));
                data.setOrderMenu(rs.getInt("ORDER_MENU"));
                data.setHasChild(rs.getBoolean("HAS_CHILD"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCanView(rs.getBoolean("CAN_VIEW"));
                data.setCanAdd(rs.getBoolean("CAN_ADD"));
                data.setCanEdit(rs.getBoolean("CAN_EDIT"));
                data.setCanDelete(rs.getBoolean("CAN_DELETE"));
                data.setCanApprove(rs.getBoolean("CAN_APPROVE"));
               
                data.setView(rs.getBoolean("IS_VIEW"));
                data.setAdd(rs.getBoolean("IS_ADD"));
                data.setEdit(rs.getBoolean("IS_EDIT"));
                data.setDelete(rs.getBoolean("IS_DELETE"));
                data.setApprove(rs.getBoolean("IS_APPROVE"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

}
