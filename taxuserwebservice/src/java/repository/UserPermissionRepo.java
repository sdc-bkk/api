/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.UserPermissionModel;
import model.UserRoleModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.UserPermissionFilterModel;
import model.filter.UsersFilterModel;
import repository.mapper.UserPermissionMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.UserPermissionViewModel;
import viewModel.UserRoleViewModel;

/**
 *
 * @author Sirichai
 */
public class UserPermissionRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<UserRoleViewModel>> getListAll(ResultPage page, UserPermissionFilterModel filter) throws SQLException {

        ResultData<List<UserRoleViewModel>> resultData = new ResultData<List<UserRoleViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM USER_ROLE "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getRoleName())) {
                sql += " AND ROLE_NAME LIKE ? ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("roleName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getRoleName())) {
                ps.setString(i++, "%" + filter.getRoleName() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<UserRoleViewModel> dataList = new UserPermissionMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<UserRoleViewModel> dataList = new UserPermissionMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " ROLE_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("roleName")) {
                str = " ROLE_NAME ";
            } else if (orderBy.equalsIgnoreCase("shortName")) {
                str = " SHORT_NAME ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM USER_ROLE "
                        + " WHERE ROLE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<UserRoleViewModel> getData(int id) throws SQLException {

        ResultData<UserRoleViewModel> resultData = new ResultData<UserRoleViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM USER_ROLE "
                       + " WHERE ROLE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            UserRoleViewModel data = new UserPermissionMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(UserPermissionFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM USER_ROLE "
                        + " WHERE (? = 0 OR ROLE_ID <> ?) "
                        + " AND (ROLE_NAME = ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getRoleId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getRoleId()));
            ps.setString(i++, filter.getRoleName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(UserRoleModel userRole) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="UserRole">
            if (userRole.getRoleId() == 0) {

                sql = "INSERT INTO USER_ROLE ( "
                        + "  ROLE_NAME "
                        + ", SHORT_NAME "
                        + ", IS_ACTIVE "
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "ROLE_ID" });
                
            } else {

                sql = "UPDATE USER_ROLE SET "
                        + "  ROLE_NAME = ? "
                        + ", SHORT_NAME = ? "
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE ROLE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, userRole.getRoleName());
            ps.setString(i++, userRole.getShortName());
            ps.setBoolean(i++, userRole.isActive());

            if (userRole.getRoleId() == 0) {

                if (!AppUtil.isNullAndSpace(userRole.getUpdatedBy())) {
                    ps.setString(i++, userRole.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                userRole.setRoleId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(userRole.getUpdatedBy())) {
                    ps.setString(i++, userRole.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, userRole.getRoleId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="UserPermission">
            if (userRole.getUserPermissionList() != null) {

                if (!userRole.getUserPermissionList().isEmpty()) {

                    //<editor-fold defaultstate="collapsed" desc="MenuId is use NOT DELETE">
                    String delId = "";
                    int size = userRole.getUserPermissionList().size();
                    List empty = new ArrayList();

                    for (int j = 0; j < size; j++) {

                        UserPermissionModel userPermission = userRole.getUserPermissionList().get(j);

                        if (!AppUtil.isNullAndZero(userPermission.getMenuId())) {
                            empty.add(userPermission.getMenuId());
                        }
                    }

                    delId = empty.isEmpty() ? "" : empty.toString().replace("[", "").replace("]", "");
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="DELETE UserPermission by MenuId is not use">
                    i = 1;

                    sql = " DELETE FROM USER_PERMISSION WHERE ROLE_ID = ? ";

                    if (!delId.equals("")) {
                        sql += " AND MENU_ID NOT IN (" + delId + ") ";
                    }

                    ps = conn.prepareStatement(sql);

                    ps.setInt(i++, userRole.getRoleId());

                    ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Insert or Update UserPermission">
                    for (UserPermissionModel userPermission : userRole.getUserPermissionList()) {

                        if (!AppUtil.isNullAndZero(userPermission.getMenuId())) {

                            boolean resultUpdate = false;
                            //<editor-fold defaultstate="collapsed" desc="Update UserPermission by RoleId and MenuId">
                            i = 1;

                            sql = " UPDATE USER_PERMISSION SET "
                                + "   IS_VIEW = ?, IS_ADD = ?, IS_EDIT = ?, "
                                + "   IS_DELETE = ?, IS_APPROVE = ? "
                                + " WHERE ROLE_ID = ? AND MENU_ID = ? ";

                            ps = conn.prepareStatement(sql);

                            ps.setBoolean(i++, userPermission.isView());
                            ps.setBoolean(i++, userPermission.isAdd());
                            ps.setBoolean(i++, userPermission.isEdit());
                            ps.setBoolean(i++, userPermission.isDelete());
                            ps.setBoolean(i++, userPermission.isApprove());

                            ps.setInt(i++, userRole.getRoleId());
                            ps.setInt(i++, userPermission.getMenuId());

                            resultUpdate = ps.executeUpdate() > 0;
                            //</editor-fold>
                            
                            if(resultUpdate == false){

                                //Update = false = data not found
                                //<editor-fold defaultstate="collapsed" desc="Insert UserPermission at RoleId and MenuId">
                                i = 1;

                                sql = " INSERT INTO USER_PERMISSION ( "
                                    + "    ROLE_ID, MENU_ID, IS_VIEW, "
                                    + "    IS_ADD, IS_EDIT, IS_DELETE, IS_APPROVE "
                                    + " ) VALUES (?,?,?, ?,?,?,?) ";

                                ps = conn.prepareStatement(sql);

                                ps.setInt(i++, userRole.getRoleId());
                                ps.setInt(i++, userPermission.getMenuId());

                                ps.setBoolean(i++, userPermission.isView());
                                ps.setBoolean(i++, userPermission.isAdd());
                                ps.setBoolean(i++, userPermission.isEdit());
                                ps.setBoolean(i++, userPermission.isDelete());
                                ps.setBoolean(i++, userPermission.isApprove());

                                boolean resultInsert = ps.executeUpdate() != 0;
                                //</editor-fold>

                            }
                            
                        }
                    }
                    //</editor-fold>

                } else {

                    //<editor-fold defaultstate="collapsed" desc="DELETE ALL UserPermission by RoleId">
                    i = 1;

                    sql = " DELETE FROM USER_PERMISSION WHERE ROLE_ID = ? ";

                    ps = conn.prepareStatement(sql);

                    ps.setInt(i++, userRole.getRoleId());

                    ps.executeUpdate();
                    //</editor-fold>

                }
            }
            //</editor-fold>
            
            conn.commit();
            result = true;

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, UserPermissionFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sqlRole = "";
            String sqlUserRole = "";
            String sqlUserPermission = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete UserRole and UserPermission by id list">
            sqlRole = " DELETE FROM USER_ROLE "
                + " WHERE ROLE_ID = ? ";
            sqlUserPermission = " DELETE FROM USER_PERMISSION "
                              + " WHERE ROLE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete UserPermission by id ">
                i = 1;
                ps.setSql(sqlUserPermission);

                ps.setInt(i, id); //RoleId

                boolean resultDeletePermission = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Delete UserRole by id ">
                i = 1;
                ps.setSql(sqlRole);

                ps.setInt(i, id); //RoleId

                boolean resultDeleteRole = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();
            result = true;

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    
    public List<UserPermissionViewModel> getListPermission(String roleId) throws SQLException {

        List<UserPermissionViewModel> resultData = new ArrayList<UserPermissionViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT R.*, P.MENU_ID, M.MENU_NAME, "
                       + "    P.IS_VIEW, P.IS_ADD, P.IS_EDIT, P.IS_DELETE, P.IS_APPROVE "
                       + " FROM USER_ROLE R "
                       + "    LEFT JOIN USER_PERMISSION P ON R.ROLE_ID = P.ROLE_ID "
                       + "    LEFT JOIN MENU M ON P.MENU_ID = M.MENU_ID "
                       + " WHERE R.ROLE_ID = ? AND M.IS_ACTIVE = ? "
                       + "    AND (M.LEVEL_MENU = ?) "
                       + "    AND ((P.IS_VIEW+P.IS_ADD+P.IS_EDIT+P.IS_DELETE+P.IS_APPROVE) > 0) ";

            if(true){
            
            }
/*
 SELECT R.*, P.MENU_ID, M.MENU_NAME, 
    P.IS_VIEW, P.IS_ADD, P.IS_EDIT, P.IS_DELETE, P.IS_APPROVE
 FROM USER_ROLE R 
    LEFT JOIN USER_PERMISSION P ON R.ROLE_ID = P.ROLE_ID 
    LEFT JOIN MENU M ON  P.MENU_ID = M.MENU_ID 
 WHERE R.ROLE_ID = 1 AND M.IS_ACTIVE = 1 
    AND (M.LEVEL_MENU = 3) 
    AND ((P.IS_VIEW+P.IS_ADD+P.IS_EDIT+P.IS_DELETE+P.IS_APPROVE) > 0) 
*/
            ps.setSql(sql);

            ps.setOrderBy(" M.ORDER_MENU ASC ");

            ps.setInt(i++, AppUtil.decryptId(roleId)); //ROLE_ID
            ps.setBoolean(i++, true);
            ps.setInt(i++, 3); //LEVEL_MENU

            rs = ps.executeQuery();

            resultData = new UserPermissionMapper(rs).mapFullListPermission();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public List<UserPermissionViewModel> getListMenu(String refMenuId, String roleId) throws SQLException {

        List<UserPermissionViewModel> resultData = new ArrayList<UserPermissionViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT M.*, P.ROLE_ID, NVL(P.IS_VIEW,0) IS_VIEW, NVL(P.IS_ADD,0) IS_ADD, "
                       + "    NVL(P.IS_EDIT,0) IS_EDIT, NVL(P.IS_DELETE,0) IS_DELETE, NVL(P.IS_APPROVE,0) IS_APPROVE "
                       + " FROM MENU M "
                       + "    LEFT JOIN USER_PERMISSION P ON M.MENU_ID = P.MENU_ID AND P.ROLE_ID = ? "
                       + " WHERE M.REF_MENU_ID = ? AND M.IS_ACTIVE = ? ";

            if(true){
            
            }
/*
 SELECT M.*, P.ROLE_ID, NVL(P.IS_VIEW,0) IS_VIEW, NVL(P.IS_ADD,0) IS_ADD, 
    NVL(P.IS_EDIT,0) IS_EDIT, NVL(P.IS_DELETE,0) IS_DELETE, NVL(P.IS_APPROVE,0) IS_APPROVE 
 FROM MENU M  
 LEFT JOIN USER_PERMISSION P ON M.MENU_ID = P.MENU_ID AND P.ROLE_ID = NULL 
 WHERE M.REF_MENU_ID = 1 AND M.IS_ACTIVE = 1 
*/
            ps.setSql(sql);

            ps.setOrderBy(" M.ORDER_MENU ASC ");

            if (!AppUtil.isNullAndSpace(roleId) && AppUtil.decryptId(roleId) > 0) {
                ps.setInt(i++, AppUtil.decryptId(roleId));
            } else {
                ps.setInt(i++, 0);
            }

            if (!AppUtil.isNullAndSpace(refMenuId) && AppUtil.decryptId(refMenuId) > 0) {
                ps.setInt(i++, AppUtil.decryptId(refMenuId));
            } else {
                ps.setInt(i++, 0);
            }
            
            ps.setBoolean(i++, true);

            rs = ps.executeQuery();

            resultData = new UserPermissionMapper(rs).mapFullListMenu();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public List<UserPermissionViewModel> getMenuAndPermissionListByLevel(UsersFilterModel model) throws SQLException {

        List<UserPermissionViewModel> menuList = new ArrayList<UserPermissionViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT R.MENU_ID,R.MENU_NAME,R.ICON,R.PATH,R.REF_MENU_ID"
                    + ",R.LEVEL_MENU,R.ORDER_MENU,R.HAS_CHILD,R.IS_ACTIVE\n"
                    + ", R.CAN_VIEW,R.CAN_ADD,R.CAN_EDIT\n"
                    + ",R.CAN_DELETE,R.CAN_APPROVE\n"
                    + ",CAST(MAX(CAST(R.IS_VIEW as INT)) AS CHAR) IS_VIEW\n"
                    + ",CAST(MAX(CAST(R.IS_ADD as INT)) AS CHAR) IS_ADD\n"
                    + ",CAST(MAX(CAST(R.IS_EDIT as INT)) AS CHAR) IS_EDIT\n"
                    + ",CAST(MAX(CAST(R.IS_DELETE as INT)) AS CHAR) IS_DELETE\n"
                    + ",CAST(MAX(CAST(R.IS_APPROVE as INT)) AS CHAR) IS_APPROVE\n"
                    + "FROM (\n"
                    + "SELECT DISTINCT MENU.*,  USER_PERMISSION.IS_VIEW, USER_PERMISSION.IS_ADD, USER_PERMISSION.IS_EDIT\n"
                    + ",USER_PERMISSION.IS_DELETE ,USER_PERMISSION.IS_APPROVE \n"
                    + " FROM USER_PERMISSION \n"
                    + " LEFT JOIN MENU ON MENU.MENU_ID = USER_PERMISSION.MENU_ID\n"
                    + " AND USER_PERMISSION.ROLE_ID IN (SELECT USER_IN_ROLE.ROLE_ID FROM USER_IN_ROLE \n"
                    + " LEFT JOIN USER_ROLE ON USER_IN_ROLE.ROLE_ID = USER_ROLE.ROLE_ID\n"
                    + " WHERE ( ? IS NULL OR ? = 0 OR USER_IN_ROLE.USER_ID =?) AND (? IS NULL OR USER_ROLE.IS_ACTIVE=?) ) AND USER_PERMISSION.IS_VIEW = 1 \n"
                    + " WHERE (? IS NULL OR MENU.IS_ACTIVE = ?)\n"
                    + " AND (? IS NULL OR MENU.REF_MENU_ID = ?))R\n"
                    + " GROUP BY R.MENU_ID,R.MENU_NAME,R.ICON,R.PATH,R.REF_MENU_ID,R.LEVEL_MENU,R.ORDER_MENU,R.HAS_CHILD,R.IS_ACTIVE "
                    + ", R.CAN_VIEW,R.CAN_ADD,R.CAN_EDIT\n"
                    + ",R.CAN_DELETE,R.CAN_APPROVE \n";

            ps.setSql(sql);
            ps.setOrderBy("R.ORDER_MENU");

            ps.setInt(i++, AppUtil.decryptId(model.getUserId()));
            ps.setInt(i++, AppUtil.decryptId(model.getUserId()));
            ps.setInt(i++, AppUtil.decryptId(model.getUserId()));

            ps.setBoolean(i++, model.isIsActive());
            ps.setBoolean(i++, model.isIsActive());

            ps.setBoolean(i++, model.isIsActive());
            ps.setBoolean(i++, model.isIsActive());

            if (!model.getRefMenuId().equals("0")) {
                ps.setInt(i++, AppUtil.decryptId(model.getRefMenuId()));
                ps.setInt(i++, AppUtil.decryptId(model.getRefMenuId()));
            } else {
                ps.setInt(i++, 0);
                ps.setInt(i++, 0);
            }

            rs = ps.executeQuery();

            menuList = new UserPermissionMapper(rs).mapFullListMenu();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();

        }

        return menuList;

    }

}
