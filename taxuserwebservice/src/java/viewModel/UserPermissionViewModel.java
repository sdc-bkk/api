/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import utility.AppConfig;

/**
 *
 * @author Sirichai
 */
public class UserPermissionViewModel {

    private String menuId;
    private String menuName; //ชื่อเมนู
    private String icon; //รูปสัญลักษณ์เมนู
    private String path; //พาธเมนู
    private String refMenuId; //รหัสอ้างอิงเมนู
    private Integer levelMenu; //ระดับของเมนู
    private Integer orderMenu; //ลำดับของเมนู
    private Boolean hasChild; //สถานะการมีเมนูย่อย(0=ไม่มี,1=มี)

    private Boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private Boolean canView; //สามารถเรียกดูข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private Boolean canAdd; //สามารถเพิ่มข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private Boolean canEdit; //สามารถแก้ไขข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private Boolean canDelete; //สามารถลบข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private Boolean canApprove; //สามารถอนุมัติข้อมูลได้หรือไม่(0=ไม่,1=ใช่)

    private Boolean view; //ได้รับสิทธิ์การดูข้อมูล(0=ไม่,1=ใช่)
    private Boolean add; //ได้รับสิทธิ์การเพิ่มข้อมูล(0=ไม่,1=ใช่)
    private Boolean edit; //ได้รับสิทธิ์การแก้ไขข้อมูล(0=ไม่,1=ใช่)
    private Boolean delete; //ได้รับสิทธิ์การลบข้อมูล(0=ไม่,1=ใช่)
    private Boolean approve; //ได้รับสิทธิ์การอนุมัติข้อมูล(0=ไม่,1=ใช่)
    
    private List<UserPermissionViewModel> subMenu;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRefMenuId() {
        return refMenuId;
    }

    public void setRefMenuId(String refMenuId) {
        this.refMenuId = refMenuId;
    }

    public Integer getLevelMenu() {
        return levelMenu;
    }

    public void setLevelMenu(Integer levelMenu) {
        this.levelMenu = levelMenu;
    }

    public Integer getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(Integer orderMenu) {
        this.orderMenu = orderMenu;
    }

    public Boolean getHasChild() {
        return hasChild;
    }

    public void setHasChild(Boolean hasChild) {
        this.hasChild = hasChild;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getCanView() {
        return canView;
    }

    public void setCanView(Boolean canView) {
        this.canView = canView;
    }

    public Boolean getCanAdd() {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd) {
        this.canAdd = canAdd;
    }

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Boolean getCanApprove() {
        return canApprove;
    }

    public void setCanApprove(Boolean canApprove) {
        this.canApprove = canApprove;
    }

    public Boolean getView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }

    public Boolean getAdd() {
        return add;
    }

    public void setAdd(Boolean add) {
        this.add = add;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    public Boolean getApprove() {
        return approve;
    }

    public void setApprove(Boolean approve) {
        this.approve = approve;
    }

    public List<UserPermissionViewModel> getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(List<UserPermissionViewModel> subMenu) {
        this.subMenu = subMenu;
    }

}
