/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.PaymentFilterModel;
import repository.PaymentRepo;
import service.PaymentService;
import viewModel.PaymentViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("payment")
public class PaymentWebservice {

    @Context
    private UriInfo context;

    PaymentService paymentService = new PaymentService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(PaymentFilterModel filter) {

        try {

            ResultData<List<PaymentViewModel>> data = paymentService.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(PaymentFilterModel filter) {

        try {

            ResultData<PaymentViewModel> data = paymentService.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(PaymentViewModel feeView) {

        try {

            ResultData<Boolean> data = paymentService.saveData(feeView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(PaymentFilterModel filter) {

        try {

            return new Gson().toJson(paymentService.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpList")
    public String getDrpList() {

        try {

            ResultData<List<PaymentViewModel>> data = paymentService.getDrpList();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("updateBarcode")
    public String updatebarcode() {

        try {
            PaymentRepo repo = new PaymentRepo();
            boolean data = repo.updatebarcode();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getBarcode")
    public String getBarcode() {

        try {
            PaymentRepo repo = new PaymentRepo();
            String data = repo.getBarcode();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
