/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.PaymentModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.PaymentFilterModel;
import repository.PaymentRepo;
import repository.mapper.PaymentMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.PaymentViewModel;

/**
 *
 * @author Sirichai
 */
public class PaymentService {

    PaymentRepo paymentRepo = new PaymentRepo();

    public ResultData<List<PaymentViewModel>> getListAll(PaymentFilterModel filter) {

        ResultData<List<PaymentViewModel>> data = new ResultData<List<PaymentViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = paymentRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<PaymentViewModel> getData(PaymentFilterModel filter) {

        ResultData<PaymentViewModel> data = new ResultData<PaymentViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getPaymentId());
            if (paymentRepo.hasData(id)) {
                
                data = paymentRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.payment.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(PaymentViewModel paymentView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        PaymentModel payment = new PaymentModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            PaymentFilterModel filter = new PaymentFilterModel();
            filter.setPaymentId(paymentView.getPaymentId());
            filter.setPaymentName(paymentView.getPaymentName());
            filter.setStartDate(paymentView.getStartDate());
            filter.setEndDate(paymentView.getEndDate());

            boolean resultDuplicate = paymentRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                PaymentMapper mapper = new PaymentMapper();

                payment = mapper.mapFull(paymentView);

                boolean resultSave = paymentRepo.save(payment);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.payment.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(PaymentFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = paymentRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<PaymentViewModel>> getDrpList() {

        ResultData<List<PaymentViewModel>> data = new ResultData<List<PaymentViewModel>>();

        try {

            ResultPage resultPage = null;
            
            PaymentFilterModel filter = new PaymentFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = paymentRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
