/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class PaymentModel {

    private int paymentId;
    private String paymentName; //ชื่อค่าปรับล่าช้า/ค่าเพิ่ม
    private String paymentFormat; //รูปแบบค่าธรรมเนียม (R = ร้อยละ , B = จำนวนเงินบาท)
    private Float paymentValue; //ค่าของค่าปรับล่าช้า/ค่าเพิ่ม
    
    private String startDate; //วันที่เริ่มต้น บังคับใช้
    private String endDate; //วันที่สิ้นสุด บังคับใช้
    
    private boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private String createdBy; //ผู้ที่สร้าง
    private String createdDate; //วันที่สร้าง
    private String updatedBy; //ผู้ที่แก้ไขล่าสุด
    private String updatedDate; //วันที่แก้ไขล่าสุด

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public String getPaymentFormat() {
        return paymentFormat;
    }

    public void setPaymentFormat(String paymentFormat) {
        this.paymentFormat = paymentFormat;
    }

    public Float getPaymentValue() {
        return paymentValue;
    }

    public void setPaymentValue(Float paymentValue) {
        this.paymentValue = paymentValue;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
