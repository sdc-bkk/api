/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.PaymentModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.PaymentViewModel;

/**
 *
 * @author Sirichai
 */
public class PaymentMapper {

    private ResultSet rs = null;

    public PaymentMapper() {

    }

    public PaymentMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<PaymentViewModel> mapFullList() {

        List<PaymentViewModel> dataList = new ArrayList<PaymentViewModel>();

        try {

            while (rs.next()) {

                PaymentViewModel data = new PaymentViewModel();

                data.setPaymentId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                data.setPaymentName(rs.getString("PAYMENT_NAME"));
                data.setPaymentFormat(rs.getString("PAYMENT_FORMAT"));
                data.setPaymentValue(rs.getFloat("PAYMENT_VALUE"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<PaymentViewModel> mapFullDrpList() {

        List<PaymentViewModel> dataList = new ArrayList<PaymentViewModel>();

        try {

            while (rs.next()) {

                PaymentViewModel data = new PaymentViewModel();

                data.setPaymentId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                data.setPaymentName(rs.getString("PAYMENT_NAME"));
                data.setPaymentFormat(rs.getString("PAYMENT_FORMAT"));
                data.setPaymentValue(rs.getFloat("PAYMENT_VALUE"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public PaymentViewModel mapFull() {

        PaymentViewModel data = new PaymentViewModel();

        try {

            while (rs.next()) {

                data.setPaymentId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                data.setPaymentName(rs.getString("PAYMENT_NAME"));
                data.setPaymentFormat(rs.getString("PAYMENT_FORMAT"));
                data.setPaymentValue(rs.getFloat("PAYMENT_VALUE"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public PaymentModel mapFull(PaymentViewModel dataView) {

        PaymentModel data = new PaymentModel();

        try {
            data.setPaymentId(AppUtil.decryptId(dataView.getPaymentId()));
            data.setPaymentName(dataView.getPaymentName());
            data.setPaymentFormat(dataView.getPaymentFormat());
            data.setPaymentValue(dataView.getPaymentValue());

            data.setStartDate(dataView.getStartDate());
            data.setEndDate(dataView.getEndDate());

            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
