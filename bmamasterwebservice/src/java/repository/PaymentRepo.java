/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.PaymentModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.PaymentFilterModel;
import repository.mapper.PaymentMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.PaymentViewModel;

/**
 *
 * @author Sirichai
 */
public class PaymentRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<PaymentViewModel>> getListAll(ResultPage page, PaymentFilterModel filter) throws SQLException {

        ResultData<List<PaymentViewModel>> resultData = new ResultData<List<PaymentViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;
            String tempStartDate = "";
            String tempEndDate = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                tempStartDate = DateUtils.toEng(DateUtils.toEng(filter.getStartDate()), "YYYY-MM-dd");
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "YYYY-MM-dd");
            }

            String sql = "SELECT * FROM M_PAYMENT "
                    + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getPaymentName())) {
                sql += " AND PAYMENT_NAME LIKE ? ";
            }

            if (filter.getPaymentValue() != null && filter.getPaymentValue() > 0) {
                sql += " AND PAYMENT_VALUE = ? ";
            }

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                sql += " AND START_DATE <= TO_DATE(?, 'YYYY-MM-dd') ";
            }

            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                sql += " AND END_DATE >= TO_DATE(?, 'YYYY-MM-dd') ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("paymentName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getPaymentName())) {
                ps.setString(i++, "%" + filter.getPaymentName() + "%");
            }

            if (filter.getPaymentValue() != null && filter.getPaymentValue() > 0) {
                ps.setFloat(i++, filter.getPaymentValue());
            }

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                ps.setString(i++, tempStartDate);
            }

            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                ps.setString(i++, tempEndDate);
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<PaymentViewModel> dataList = new PaymentMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<PaymentViewModel> dataList = new PaymentMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " PAYMENT_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("paymentName")) {
                str = " PAYMENT_NAME ";
            } else if (orderBy.equalsIgnoreCase("paymentValue")) {
                str = " PAYMENT_VALUE ";
            } else if (orderBy.equalsIgnoreCase("startDate")) {
                str = " START_DATE ";
            } else if (orderBy.equalsIgnoreCase("endDate")) {
                str = " END_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM M_PAYMENT "
                    + " WHERE PAYMENT_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<PaymentViewModel> getData(int id) throws SQLException {

        ResultData<PaymentViewModel> resultData = new ResultData<PaymentViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_PAYMENT "
                    + " WHERE PAYMENT_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            PaymentViewModel data = new PaymentMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(PaymentFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;
            String tempStartDate = "";
            String tempEndDate = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                tempStartDate = DateUtils.toEng(DateUtils.toEng(filter.getStartDate()), "yyyy-MM-dd");
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "yyyy-MM-dd");
            }

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM M_PAYMENT "
                    + " WHERE (? = 0 OR PAYMENT_ID <> ?) "
                    + " AND ( "
                    + "    (PAYMENT_NAME = ?) ";

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                sql += "    OR ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                sql += "    OR ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            sql += " ) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getPaymentId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getPaymentId()));
            ps.setString(i++, filter.getPaymentName());

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                ps.setString(i++, tempStartDate);
                ps.setString(i++, tempStartDate);
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                ps.setString(i++, tempEndDate);
                ps.setString(i++, tempEndDate);
            }

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(PaymentModel payment) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Payment">
            if (payment.getPaymentId() == 0) {

                sql = "INSERT INTO M_PAYMENT ( "
                        + "  PAYMENT_NAME "
                        + ", PAYMENT_FORMAT "
                        + ", PAYMENT_VALUE "
                        + ", START_DATE "
                        + ", END_DATE "
                        + ", IS_ACTIVE "
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?,?, ?,SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"PAYMENT_ID"});

            } else {

                sql = "UPDATE M_PAYMENT SET "
                        + "  PAYMENT_NAME = ? "
                        + ", PAYMENT_FORMAT = ? "
                        + ", PAYMENT_VALUE = ? "
                        + ", START_DATE = ? "
                        + ", END_DATE = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE PAYMENT_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, payment.getPaymentName());
            ps.setString(i++, payment.getPaymentFormat());
            ps.setFloat(i++, payment.getPaymentValue());

            if (!AppUtil.isNullAndSpace(payment.getStartDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(payment.getStartDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }
            if (!AppUtil.isNullAndSpace(payment.getEndDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(payment.getEndDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }

            ps.setBoolean(i++, payment.isActive());

            if (payment.getPaymentId() == 0) {

                if (!AppUtil.isNullAndSpace(payment.getUpdatedBy())) {
                    ps.setString(i++, payment.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                payment.setPaymentId(id);

            } else {

                if (!AppUtil.isNullAndSpace(payment.getUpdatedBy())) {
                    ps.setString(i++, payment.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, payment.getPaymentId());

                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>
            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, PaymentFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete Payment by id list">
            sql = " DELETE FROM M_PAYMENT "
                    + " WHERE PAYMENT_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete Payment by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean updatebarcode() throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Payment">
            sql = "UPDATE INVOICE SET "
                    + "  BARCODE = ? "
                    + ", QRCODE = ? "
                    + " WHERE INVOICE_ID = 1 ";

            ps = conn.prepareStatement(sql);

            //Set Parameter
            String barcode = "|" + "0994000160151" + "02" + " " + "500200006220057003" + " " + "020464100202" + " " + "918000".replace(".", "");

            ps.setString(i++, barcode);
            ps.setString(i++, barcode);

            result = ps.executeUpdate() != 0;

            //</editor-fold>
            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }
    
    public String getBarcode() throws SQLException {

        ResultData<PaymentViewModel> resultData = new ResultData<PaymentViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
String result ="";
        try {

            ps.setAutoCommit(false);

            String sql = " SELECT BARCODE FROM INVOICE "
                    + " WHERE INVOICE_ID = 1 ";

            ps.setSql(sql);


            rs = ps.executeQuery();

            
            while (rs.next()) {
                
               result =rs.getString("BARCODE");
            }


        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }
}
