/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import repository.mapper.ContentTypeMapper;
import repository.mapper.FrontContentMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.ContentFileViewModel;
import viewModel.ContentTypeViewModel;
import viewModel.ContentViewModel;
import viewModel.FilterContentModel;
import viewModel.FrontContentTypeViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class FrontContentRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public List<ContentFileViewModel> getListMain(ResultPage page, FilterContentModel filter) throws SQLException {

        List<ContentFileViewModel> resultData = new ArrayList<>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT CONTENT_FILE.*,CONTENT.UPDATED_DATE,CONTENT.CREATED_DATE "
                    + " FROM CONTENT_FILE  "
                    + " LEFT JOIN CONTENT ON CONTENT.CONTENT_ID = CONTENT_FILE.CONTENT_ID "
                    + " WHERE CONTENT.IS_MAIN_CONTENT = 1 AND CONTENT.IS_ACTIVE = 1  "
                    + " AND ((CONTENT.DISPLAY_FORMAT = ? ) OR (TRUNC(CONTENT.START_DATE) <= SYSDATE AND TRUNC(CONTENT.END_DATE) >= SYSDATE )) "
                    + " AND CONTENT_FILE.FILE_CATE =?  ";

            ps.setSql(sql);

            String orderBy = " (CASE WHEN  UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            filter.setSort("DESC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }
            ps.setString(i++, "I");
            ps.setString(i++, "C");

            rs = ps.executeQuery();

            List<ContentFileViewModel> dataList = null;

            dataList = new FrontContentMapper(rs).mapDataFile();
            resultData = dataList;

        } catch (Exception e) {
            e.getMessage();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public List<FrontContentTypeViewModel> getListSub(ResultPage page, FilterContentModel filter) throws SQLException {

        List<FrontContentTypeViewModel> resultData = new ArrayList<>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT CONTENT_TYPE_ID,CONTENT_TYPE_NAME,CONTENT_FORMAT,SEQUENCE "
                    + "FROM CONTENT_TYPE   \n";

            ps.setSql(sql);

            String orderBy = "  SEQUENCE, CONTENT_TYPE_ID  ";
            filter.setSort("DESC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

//            if (page != null) {
//                ps.setResultPage(page);
//            }
            rs = ps.executeQuery();

            List<FrontContentTypeViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
            FrontContentTypeViewModel data = new FrontContentTypeViewModel();
                data.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                data.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));
                data.setContentFormat(AppUtil.checkNullData(rs.getString("CONTENT_FORMAT")));
                dataList.add(data);
            }
            resultData = dataList;

        } catch (Exception e) {
            e.getMessage();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public List<ContentViewModel> getListByContentType(ResultPage page, FilterContentModel filter) throws SQLException {
        List<ContentViewModel> contentList = new ArrayList<>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT CT.CONTENT_TYPE_NAME,CT.CONTENT_FORMAT,CT.SEQUENCE\n"
                    + ",C.*,CF.FILE_NAME, CF.FILE_TYPE, CF.PATH_FILE\n"
                    + "FROM CONTENT C\n"
                    + "LEFT JOIN CONTENT_TYPE CT ON C.CONTENT_TYPE_ID = CT.CONTENT_TYPE_ID\n"
                    + "LEFT JOIN CONTENT_FILE CF ON C.CONTENT_ID = CF.CONTENT_ID AND CF.FILE_CATE ='C' \n"
                    + "WHERE C.IS_ACTIVE = 1 \n"
                    + "AND ((DISPLAY_FORMAT = ? ) OR (TRUNC(START_DATE) <= SYSDATE AND TRUNC(END_DATE) >= SYSDATE )) ";

            if (!AppUtil.isNullAndSpace(filter.getContentTypeId())) {
                sql += " AND (C.CONTENT_TYPE_ID = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "  SEQUENCE,CONTENT_TYPE_ID,(CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            filter.setSort("DESC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            ps.setString(i++, "I");

            if (!AppUtil.isNullAndSpace(filter.getContentTypeId())) {
                ps.setString(i++, AppUtil.decrypt(filter.getContentTypeId()));
            }

            rs = ps.executeQuery();

            while (rs.next()) {
                //Content
                ContentViewModel content = new ContentViewModel();
                content.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                content.setContentName(AppUtil.checkNullData(rs.getString("CONTENT_NAME")));
                content.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                content.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));

                content.setDisplayFormat(AppUtil.checkNullData(rs.getString("DISPLAY_FORMAT")));
                content.setStartDate(AppUtil.checkNullData(rs.getDate("START_DATE")));
                content.setEndDate(AppUtil.checkNullData(rs.getDate("END_DATE")));
                content.setActive(rs.getBoolean("IS_ACTIVE"));

                content.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                content.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                content.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                content.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                //Cover
                if (!AppUtil.isNullAndSpace(rs.getString("PATH_FILE"))) {
                    List<ContentFileViewModel> coverList = new ArrayList<>();
                    ContentFileViewModel cover = new ContentFileViewModel();
                    cover.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                    cover.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                    cover.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                    cover.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                    coverList.add(cover);
                    content.setContentCoverList(coverList);
                }
                contentList.add(content);

            }

        } catch (Exception e) {
            e.getMessage();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return contentList;

    }

    public List<FrontContentTypeViewModel> getListSub_Old(ResultPage page, FilterContentModel filter) throws SQLException {

        List<FrontContentTypeViewModel> resultData = new ArrayList<>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT CT.CONTENT_TYPE_NAME,CT.CONTENT_FORMAT,CT.SEQUENCE\n"
                    + ",C.*,CF.FILE_NAME, CF.FILE_TYPE, CF.PATH_FILE\n"
                    + "FROM CONTENT C\n"
                    + "LEFT JOIN CONTENT_TYPE CT ON C.CONTENT_TYPE_ID = CT.CONTENT_TYPE_ID\n"
                    + "LEFT JOIN CONTENT_FILE CF ON C.CONTENT_ID = CF.CONTENT_ID AND CF.FILE_CATE ='C' \n"
                    + "WHERE C.IS_ACTIVE = 1 \n"
                    + "AND ((DISPLAY_FORMAT = ? ) OR (TRUNC(START_DATE) <= SYSDATE AND TRUNC(END_DATE) >= SYSDATE )) ";

            ps.setSql(sql);

            String orderBy = "  SEQUENCE,CONTENT_TYPE_ID,(CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            filter.setSort("DESC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }
            ps.setString(i++, "I");

            rs = ps.executeQuery();

            List<FrontContentTypeViewModel> dataList = new ArrayList<>();
            FrontContentTypeViewModel data = new FrontContentTypeViewModel();
            List<ContentViewModel> contentList = new ArrayList<>();

            int contentTypeIdCur = 0;
            while (rs.next()) {
                //ContentType
//                contentTypeIdCur = rs.getInt("CONTENT_TYPE_ID");
                if (contentTypeIdCur != rs.getInt("CONTENT_TYPE_ID")) {
                    data = new FrontContentTypeViewModel();
                    data.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                    data.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));
                    data.setContentFormat(AppUtil.checkNullData(rs.getString("CONTENT_FORMAT")));
                    contentList = new ArrayList<>();
                };

                //Content
                ContentViewModel content = new ContentViewModel();
                content.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                content.setContentName(AppUtil.checkNullData(rs.getString("CONTENT_NAME")));
                content.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                content.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));

                content.setDisplayFormat(AppUtil.checkNullData(rs.getString("DISPLAY_FORMAT")));
                content.setStartDate(AppUtil.checkNullData(rs.getDate("START_DATE")));
                content.setEndDate(AppUtil.checkNullData(rs.getDate("END_DATE")));
                content.setActive(rs.getBoolean("IS_ACTIVE"));

                content.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                content.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                content.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                content.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                //Cover
                if (!AppUtil.isNullAndSpace(rs.getString("PATH_FILE"))) {
                    List<ContentFileViewModel> coverList = new ArrayList<>();
                    ContentFileViewModel cover = new ContentFileViewModel();
                    cover.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                    cover.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                    cover.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                    cover.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                    coverList.add(cover);
                    content.setContentCoverList(coverList);
                }
                contentList.add(content);

                if (contentTypeIdCur != rs.getInt("CONTENT_TYPE_ID")) {
                    data.setContentList(contentList);
                    dataList.add(data);
                    contentTypeIdCur = rs.getInt("CONTENT_TYPE_ID");
                }

            }
            resultData = dataList;

        } catch (Exception e) {
            e.getMessage();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

}
