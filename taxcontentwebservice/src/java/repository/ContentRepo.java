/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.ContentFileModel;
import model.ContentModel;
import repository.mapper.ContentMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.ContentFileViewModel;
import viewModel.ContentViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class ContentRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentViewModel>> getList(ResultPage page, FilterContentModel filter) throws SQLException {

        ResultData<List<ContentViewModel>> resultData = new ResultData<List<ContentViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT CONTENT.*,CONTENT_TYPE.CONTENT_TYPE_NAME "
                    + " FROM CONTENT LEFT JOIN CONTENT_TYPE ON CONTENT.CONTENT_TYPE_ID = CONTENT_TYPE.CONTENT_TYPE_ID "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNull(filter.getActive())) {
                sql += " AND (CONTENT.IS_ACTIVE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND (CONTENT.CONTENT_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getContentTypeId())) {
                sql += " AND (CONTENT.CONTENT_TYPE_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ((CONTENT.DISPLAY_FORMAT = ? ) OR ( TRUNC(CONTENT.START_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy')))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ((CONTENT.DISPLAY_FORMAT = ? ) OR ( TRUNC(CONTENT.END_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy')))) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("contentName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getContentTypeId())) {
                ps.setString(i++, AppUtil.decrypt(filter.getContentTypeId()));
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, "I");
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, "I");
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            rs = ps.executeQuery();

            List<ContentViewModel> dataList = null;
            dataList = new ContentMapper(rs).mapList();
            

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " CONTENT_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("contentName")) {
                str = " CONTENT_NAME ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }
        }

        return str;

    }

    public ResultData<ContentViewModel> getData(int id) throws SQLException {

        ResultData<ContentViewModel> resultData = new ResultData<ContentViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get content ">
            String sql = " SELECT * "
                    + " FROM CONTENT  LEFT JOIN CONTENT_TYPE ON CONTENT.CONTENT_TYPE_ID = CONTENT_TYPE.CONTENT_TYPE_ID "
                    + " WHERE CONTENT_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            // </editor-fold>
            ContentViewModel data = new ContentMapper(rs).mapData();

            // <editor-fold defaultstate="collapsed" desc=" get content Cover ">
            sql = " SELECT * "
                    + " FROM CONTENT_FILE  "
                    + " WHERE CONTENT_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.COVER.display());

            rs = ps.executeQuery();

            List<ContentFileViewModel> dataCover = new ContentMapper(rs).mapDataFile();
            data.setContentCoverList(dataCover);
            // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc=" get content File ">
            sql = " SELECT * "
                    + " FROM CONTENT_FILE  "
                    + " WHERE CONTENT_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.FILE.display());

            rs = ps.executeQuery();

            List<ContentFileViewModel> dataFile = new ContentMapper(rs).mapDataFile();
            data.setContentFileList(dataFile);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get content Image ">
            sql = " SELECT * "
                    + " FROM CONTENT_FILE  "
                    + " WHERE CONTENT_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.IMAGE.display());

            rs = ps.executeQuery();

            List<ContentFileViewModel> dataImage = new ContentMapper(rs).mapDataFile();
            data.setContentImageList(dataImage);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(ContentModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql content">
            if (data.getContentId() == 0) {

                sql = "INSERT INTO CONTENT ( "
                        + "  CONTENT_TYPE_ID "
                        + ", CONTENT_NAME "
                        + ", CONTENT_LINK "
                        + ", DISPLAY_FORMAT "
                        + ", START_DATE "
                        + ", END_DATE "
                        + ", DETAIL "
                        + ", IS_MAIN_CONTENT "
                        + ", IS_ACTIVE"
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?  ,?,?,?,?,?,?,   ?,SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"CONTENT_ID"});

            } else {

                sql = "UPDATE CONTENT SET "
                        + "  CONTENT_TYPE_ID = ? "
                        + ", CONTENT_NAME = ? "
                        + ", CONTENT_LINK = ? "
                        + ", DISPLAY_FORMAT = ? "
                        + ", START_DATE = ? "
                        + ", END_DATE = ? "
                        + ", DETAIL = ? "
                        + ", IS_MAIN_CONTENT = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE CONTENT_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setInt(i++, data.getContentTypeId());
            ps.setString(i++, data.getContentName());

            ps.setString(i++, data.getContentLink());
            ps.setString(i++, data.getDisplayFormat());
            ps.setDate(i++, AppUtil.toDateSql(data.getStartDate()));
            ps.setDate(i++, AppUtil.toDateSql(data.getEndDate()));
            ps.setString(i++, data.getDetail());
            ps.setBoolean(i++, data.getIsMainContent());

            ps.setBoolean(i++, data.getActive());

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getContentId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setContentId(id);
            } else {

                ps.setInt(i++, data.getContentId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            if (data.getContentId() != 0) {

                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM CONTENT_FILE WHERE CONTENT_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getContentId());

                ps.executeUpdate();
                //</editor-fold>
                
                //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_COVER">
                if (data.getContentCoverList()!= null && !data.getContentCoverList().isEmpty()) {
                    for (ContentFileModel file : data.getContentCoverList()) {
                        if (data.getContentId() != 0) {
                            i = 1;

                            sql = "INSERT INTO CONTENT_FILE("
                                    + " CONTENT_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getContentId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_IMAGE">
                if (data.getContentImageList() != null && !data.getContentImageList().isEmpty()) {
                    for (ContentFileModel file : data.getContentImageList()) {
                        //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_DETAIL">
                        if (data.getContentId() != 0) {
                            i = 1;

                            sql = "INSERT INTO CONTENT_FILE("
                                    + " CONTENT_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getContentId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                        //</editor-fold>
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_FILE">
                if (data.getContentFileList() != null && !data.getContentFileList().isEmpty()) {
                    for (ContentFileModel file : data.getContentFileList()) {
                        if (data.getContentId() != 0) {
                            i = 1;

                            sql = "INSERT INTO CONTENT_FILE("
                                    + " CONTENT_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getContentId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>
            }

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean duplicate(ContentViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM CONTENT "
                    + " WHERE (? = 0 OR CONTENT_ID <> ?) "
                    + " AND (CONTENT_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(data.getContentId()));
            ps.setInt(i++, AppUtil.decryptId(data.getContentId()));
            ps.setString(i++, data.getContentName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public ResultData deleteData(List<Integer> idList, FilterContentModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            String sqlFile = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete Content by id list">
            sqlFile = " DELETE FROM CONTENT_FILE "
                    + " WHERE CONTENT_ID = ? ";

            sql = " DELETE FROM CONTENT "
                    + " WHERE CONTENT_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete File ">
                i = 1;
                ps.setSql(sqlFile);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Delete by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }
}
