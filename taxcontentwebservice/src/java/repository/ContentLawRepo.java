/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.ContentLawFileModel;
import model.ContentLawModel;
import repository.mapper.ContentLawMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.ContentLawFileViewModel;
import viewModel.ContentLawViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class ContentLawRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentLawViewModel>> getList(ResultPage page, FilterContentModel filter) throws SQLException {

        ResultData<List<ContentLawViewModel>> resultData = new ResultData<List<ContentLawViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT * FROM CONTENT_LAW "
                       + " WHERE (1=1) ";

            if (!AppUtil.isNull(filter.getActive())) {
                sql += " AND (IS_ACTIVE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND LAW_NAME LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("lawName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            rs = ps.executeQuery();

            List<ContentLawViewModel> dataList = null;
            dataList = new ContentLawMapper(rs).mapList();

            if (dataList != null) {
                resultData.setResult(dataList);

                if (page != null) {
                    resultData.setResultPage(ps.getResultPage());
                }

            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " LAW_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("lawName")) {
                str = " LAW_NAME ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " LAW_NAME ";
            }
        }

        return str;

    }

    public ResultData<ContentLawViewModel> getData(int id) throws SQLException {

        ResultData<ContentLawViewModel> resultData = new ResultData<ContentLawViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get ContentLaw ">
            String sql = " SELECT * FROM CONTENT_LAW "
                       + " WHERE LAW_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            // </editor-fold>
            ContentLawViewModel data = new ContentLawMapper(rs).mapData();
            
/*
            // <editor-fold defaultstate="collapsed" desc=" get contentLaw Cover ">
            sql = " SELECT * "
                    + " FROM CONTENT_LAW_FILE  "
                    + " WHERE LAW_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.COVER.display());

            rs = ps.executeQuery();

            List<ContentLawFileViewModel> dataCover = new ContentLawMapper(rs).mapDataFile();
            data.setLawCoverList(dataCover);
            // </editor-fold>
*/
            
            // <editor-fold defaultstate="collapsed" desc=" get contentLaw File ">
            sql = " SELECT * "
                    + " FROM CONTENT_LAW_FILE  "
                    + " WHERE LAW_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.FILE.display());

            rs = ps.executeQuery();

            List<ContentLawFileViewModel> dataFile = new ContentLawMapper(rs).mapDataFile();
            data.setLawFileList(dataFile);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get contentLaw Image ">
            sql = " SELECT * "
                    + " FROM CONTENT_LAW_FILE  "
                    + " WHERE LAW_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.IMAGE.display());

            rs = ps.executeQuery();

            List<ContentLawFileViewModel> dataImage = new ContentLawMapper(rs).mapDataFile();
            data.setLawImageList(dataImage);
            // </editor-fold>


            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(ContentLawModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql">
            if (data.getLawId()== 0) {

                sql = "INSERT INTO CONTENT_LAW ( "
                        + "  LAW_NAME "
                        + ", IS_ACTIVE"
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?, SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"LAW_ID"});

            } else {

                sql = "UPDATE CONTENT_LAW SET "
                        + "  LAW_NAME = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE LAW_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getLawName());
            ps.setBoolean(i++, data.getActive());

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getLawId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setLawId(id);
            } else {

                ps.setInt(i++, data.getLawId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>
            
            if (data.getLawId() != 0) {
                
                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM CONTENT_LAW_FILE WHERE LAW_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getLawId());

                ps.executeUpdate();
                //</editor-fold>
                
/*
                //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_COVER">
                if (data.getLawCoverList()!= null && !data.getLawCoverList().isEmpty()) {
                    for (ContentLawFileModel file : data.getLawCoverList()) {
                        if (data.getLawId() != 0) {
                            i = 1;

                            sql = "INSERT INTO CONTENT_LAW_FILE ( "
                                    + " LAW_ID "
                                    + ",FILE_NAME "
                                    + ",PATH_FILE "
                                    + ",FILE_CATE "
                                    + ",FILE_TYPE "
                                    + ") VALUES(?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getLawId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>
*/
                
                //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_IMAGE">
                if (data.getLawImageList() != null && !data.getLawImageList().isEmpty()) {
                    for (ContentLawFileModel file : data.getLawImageList()) {
                        //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_DETAIL">
                        if (data.getLawId() != 0) {
                            i = 1;

                            sql = "INSERT INTO CONTENT_LAW_FILE ( "
                                    + " LAW_ID "
                                    + ",FILE_NAME "
                                    + ",PATH_FILE "
                                    + ",FILE_CATE "
                                    + ",FILE_TYPE "
                                    + ") VALUES(?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getLawId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                        //</editor-fold>
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT CONTENT_FILE">
                if (data.getLawFileList() != null && !data.getLawFileList().isEmpty()) {
                    for (ContentLawFileModel file : data.getLawFileList()) {
                        if (data.getLawId() != 0) {
                            i = 1;

                            sql = "INSERT INTO CONTENT_LAW_FILE ( "
                                    + " LAW_ID "
                                    + ",FILE_NAME "
                                    + ",PATH_FILE "
                                    + ",FILE_CATE "
                                    + ",FILE_TYPE "
                                    + ") VALUES(?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getLawId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>
                
            }
            
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean duplicate(ContentLawViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM CONTENT_LAW "
                    + " WHERE (? = 0 OR LAW_ID <> ?) "
                    + " AND (LAW_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(data.getLawId()));
            ps.setInt(i++, AppUtil.decryptId(data.getLawId()));
            ps.setString(i++, data.getLawName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public ResultData deleteData(List<Integer> idList, FilterContentModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            String sqlFile = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete ContentLaw by id list">
            sqlFile = " DELETE FROM CONTENT_LAW_FILE "
                    + " WHERE LAW_ID = ? ";
            sql = " DELETE FROM CONTENT_LAW "
                + " WHERE LAW_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete File ">
                i = 1;
                ps.setSql(sqlFile);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Delete by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

}
