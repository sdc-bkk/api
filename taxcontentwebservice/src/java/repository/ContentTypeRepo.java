/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.ContentTypeModel;
import repository.mapper.ContentTypeMapper;
import utility.AppUtil;
import utility.PreparedStatementDB;
import viewModel.ContentTypeViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;

/**
 *
 * @author User
 */
public class ContentTypeRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentTypeViewModel>> getList(ResultPage page, FilterContentModel filter) throws SQLException {

        ResultData<List<ContentTypeViewModel>> resultData = new ResultData<List<ContentTypeViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM CONTENT_TYPE "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNull(filter.getActive())) {
                sql += " AND (IS_ACTIVE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND CONTENT_TYPE_NAME LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getContentFormat())) {
                sql += " AND CONTENT_FORMAT = ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("contentTypeName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getContentFormat())) {
                ps.setString(i++, filter.getContentFormat());
            }

            rs = ps.executeQuery();

            List<ContentTypeViewModel> dataList = null;
            
            if (filter.getDropdown() !=null  && filter.getDropdown() == true) {
                dataList = new ContentTypeMapper(rs).mapDrpList();
            } else {
                dataList = new ContentTypeMapper(rs).mapList();
            }

            if (dataList != null) {
                resultData.setResult(dataList);

                if (page != null) {
                    resultData.setResultPage(ps.getResultPage());
                }

            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " CONTENT_TYPE_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("contentTypeName")) {
                str = " CONTENT_TYPE_NAME ";
            } else if (orderBy.equalsIgnoreCase("contentFormat")) {
                str = " CONTENT_FORMAT ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " CONTENT_TYPE_NAME ";
            }
        }

        return str;

    }

    public ResultData<ContentTypeViewModel> getData(int id) throws SQLException {

        ResultData<ContentTypeViewModel> resultData = new ResultData<ContentTypeViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM CONTENT_TYPE "
                    + " WHERE CONTENT_TYPE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            ContentTypeViewModel data = new ContentTypeMapper(rs).mapData();

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(ContentTypeModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql">
            if (data.getContentTypeId() == 0) {

                sql = "INSERT INTO CONTENT_TYPE ( "
                        + "  CONTENT_TYPE_NAME "
                        + ", CONTENT_FORMAT"
                        + ", IS_ACTIVE"
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?, SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"CONTENT_TYPE_ID"});

            } else {

                sql = "UPDATE CONTENT_TYPE SET "
                        + "  CONTENT_TYPE_NAME = ? "
                        + ", CONTENT_FORMAT = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE CONTENT_TYPE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getContentTypeName());
            ps.setString(i++, data.getContentFormat());
            ps.setBoolean(i++, data.getActive());

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getContentTypeId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setContentTypeId(id);
            } else {

                ps.setInt(i++, data.getContentTypeId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean duplicate(ContentTypeViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM CONTENT_TYPE "
                    + " WHERE (? = 0 OR CONTENT_TYPE_ID <> ?) "
                    + " AND (CONTENT_TYPE_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(data.getContentTypeId()));
            ps.setInt(i++, AppUtil.decryptId(data.getContentTypeId()));
            ps.setString(i++, data.getContentTypeName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public ResultData deleteData(List<Integer> idList, FilterContentModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete ContentType by id list">
            sql = " DELETE FROM CONTENT_TYPE "
                    + " WHERE CONTENT_TYPE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();
            
            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));
            
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

}
