/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ContentFileModel;
import model.ContentModel;
import utility.AppUtil;
import viewModel.ContentFileViewModel;
import viewModel.ContentViewModel;

/**
 *
 * @author User
 */
public class ContentMapper {

    private ResultSet rs = null;

    public ContentMapper() {

    }

    public ContentMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<ContentViewModel> mapList() {

        List<ContentViewModel> dataList = new ArrayList<ContentViewModel>();

        try {

            while (rs.next()) {
                ContentViewModel data = new ContentViewModel();

                data.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                data.setContentName(AppUtil.checkNullData(rs.getString("CONTENT_NAME")));
                data.setContentLink(AppUtil.checkNullData(rs.getString("CONTENT_LINK")));
                data.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                data.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));

                data.setDisplayFormat(AppUtil.checkNullData(rs.getString("DISPLAY_FORMAT")));
                data.setStartDate(AppUtil.checkNullData(rs.getDate("START_DATE")));
                data.setEndDate(AppUtil.checkNullData(rs.getDate("END_DATE")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public ContentViewModel mapData() {

        ContentViewModel data = new ContentViewModel();

        try {

            while (rs.next()) {
                data.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                data.setContentName(AppUtil.checkNullData(rs.getString("CONTENT_NAME")));
                data.setContentLink(AppUtil.checkNullData(rs.getString("CONTENT_LINK")));
                data.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                data.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));

                data.setDetail(AppUtil.checkNullData(rs.getString("DETAIL")));
                data.setIsMainContent(rs.getBoolean("IS_MAIN_CONTENT"));

                data.setDisplayFormat(AppUtil.checkNullData(rs.getString("DISPLAY_FORMAT")));
                data.setStartDate(AppUtil.checkNullData(rs.getDate("START_DATE")));
                data.setEndDate(AppUtil.checkNullData(rs.getDate("END_DATE")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<ContentFileViewModel> mapDataFile() {

        List<ContentFileViewModel> dataList = new ArrayList<ContentFileViewModel>();

        try {

            while (rs.next()) {
                ContentFileViewModel data = new ContentFileViewModel();
                data.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public ContentModel mapSaveData(ContentViewModel dataView) {

        ContentModel data = new ContentModel();

        try {
            data.setContentId(AppUtil.decryptId(dataView.getContentId()));
            data.setContentName(AppUtil.checkNullData(dataView.getContentName()));
                data.setContentLink(AppUtil.checkNullData(dataView.getContentLink()));
            data.setContentTypeId(AppUtil.decryptId(dataView.getContentTypeId()));

            data.setDisplayFormat(AppUtil.checkNullData(dataView.getDisplayFormat()));
            data.setStartDate(AppUtil.checkNullData(dataView.getStartDate()));
            data.setEndDate(AppUtil.checkNullData(dataView.getEndDate()));
            data.setDetail(AppUtil.checkNullData(dataView.getDetail()));
            data.setIsMainContent(dataView.getIsMainContent());

            data.setActive(dataView.getActive());
            data.setCreatedBy(AppUtil.checkNullData(dataView.getCreatedBy()));
            data.setUpdatedBy(AppUtil.checkNullData(dataView.getUpdatedBy()));


            if (!dataView.getContentCoverList().isEmpty()) {
                List<ContentFileModel> contentCoverList = new ArrayList<>();
                for (ContentFileViewModel file : dataView.getContentCoverList()) {
                    ContentFileModel dataFile = new ContentFileModel();
                    dataFile.setContentId(AppUtil.decryptId(dataView.getContentId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.COVER.display());//C Cover
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    contentCoverList.add(dataFile);
                }
                data.setContentCoverList(contentCoverList);
            }
            
            if (!dataView.getContentImageList().isEmpty()) {
                List<ContentFileModel> contentImageList = new ArrayList<>();
                for (ContentFileViewModel file : dataView.getContentImageList()) {
                    ContentFileModel dataFile = new ContentFileModel();
                    dataFile.setContentId(AppUtil.decryptId(dataView.getContentId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.IMAGE.display());//I =image
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    contentImageList.add(dataFile);
                }
                data.setContentImageList(contentImageList);
            }

            if (!dataView.getContentFileList().isEmpty()) {
                List<ContentFileModel> contentFileList = new ArrayList<>();
                for (ContentFileViewModel file : dataView.getContentFileList()) {
                    ContentFileModel dataFile = new ContentFileModel();
                    dataFile.setContentId(AppUtil.decryptId(dataView.getContentId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.FILE.display());//F =File
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    contentFileList.add(dataFile);
                }
                data.setContentFileList(contentFileList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
