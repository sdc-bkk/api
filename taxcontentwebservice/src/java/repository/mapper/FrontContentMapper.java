/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utility.AppUtil;
import viewModel.ContentFileViewModel;

/**
 *
 * @author User
 */
public class FrontContentMapper {

    private ResultSet rs = null;

    public FrontContentMapper() {

    }

    public FrontContentMapper(ResultSet rs) {
        this.rs = rs;
    }
    
    public List<ContentFileViewModel> mapDataFile() {

        List<ContentFileViewModel> dataList = new ArrayList<ContentFileViewModel>();

        try {

            while (rs.next()) {
                ContentFileViewModel data = new ContentFileViewModel();
                data.setContentId(AppUtil.encryptId(rs.getInt("CONTENT_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
}
