/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ContentTypeModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.ContentTypeViewModel;

/**
 *
 * @author User
 */
public class ContentTypeMapper {

    private ResultSet rs = null;

    public ContentTypeMapper() {

    }

    public ContentTypeMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<ContentTypeViewModel> mapList() {

        List<ContentTypeViewModel> dataList = new ArrayList<ContentTypeViewModel>();

        try {

            while (rs.next()) {
                ContentTypeViewModel data = new ContentTypeViewModel();

                data.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                data.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));
                data.setContentFormat(AppUtil.checkNullData(rs.getString("CONTENT_FORMAT")));
                data.setSequence(rs.getInt("SEQUENCE"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<ContentTypeViewModel> mapDrpList() {

        List<ContentTypeViewModel> dataList = new ArrayList<ContentTypeViewModel>();

        try {

            while (rs.next()) {
                ContentTypeViewModel data = new ContentTypeViewModel();

                data.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                data.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public ContentTypeViewModel mapData() {

        ContentTypeViewModel data = new ContentTypeViewModel();

        try {

            while (rs.next()) {

                data.setContentTypeId(AppUtil.encryptId(rs.getInt("CONTENT_TYPE_ID")));
                data.setContentTypeName(AppUtil.checkNullData(rs.getString("CONTENT_TYPE_NAME")));
                data.setContentFormat(AppUtil.checkNullData(rs.getString("CONTENT_FORMAT")));
                data.setSequence(rs.getInt("SEQUENCE"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public ContentTypeModel mapSaveData(ContentTypeViewModel dataView) {

        ContentTypeModel data = new ContentTypeModel();

        try {
            data.setContentTypeId(AppUtil.decryptId(dataView.getContentTypeId()));
            data.setContentTypeName(AppUtil.checkNullData(dataView.getContentTypeName()));
            data.setContentFormat(AppUtil.checkNullData(dataView.getContentFormat()));
            data.setSequence(dataView.getSequence());

            data.setActive(dataView.getActive());
            data.setCreatedBy(AppUtil.checkNullData(dataView.getCreatedBy()));
            data.setUpdatedBy(AppUtil.checkNullData(dataView.getUpdatedBy()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
