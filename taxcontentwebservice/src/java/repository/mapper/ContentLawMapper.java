/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ContentLawFileModel;
import model.ContentLawModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.ContentLawFileViewModel;
import viewModel.ContentLawViewModel;

/**
 *
 * @author User
 */
public class ContentLawMapper {
    private ResultSet rs = null;

    public ContentLawMapper() {

    }

    public ContentLawMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<ContentLawViewModel> mapList() {

        List<ContentLawViewModel> dataList = new ArrayList<ContentLawViewModel>();

        try {

            while (rs.next()) {
                ContentLawViewModel data = new ContentLawViewModel();

                data.setLawId(AppUtil.encryptId(rs.getInt("LAW_ID")));
                data.setLawName(AppUtil.checkNullData(rs.getString("LAW_NAME")));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public ContentLawViewModel mapData() {

        ContentLawViewModel data = new ContentLawViewModel();

        try {

            while (rs.next()) {

                data.setLawId(AppUtil.encryptId(rs.getInt("LAW_ID")));
                data.setLawName(AppUtil.checkNullData(rs.getString("LAW_NAME")));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<ContentLawFileViewModel> mapDataFile() {

        List<ContentLawFileViewModel> dataList = new ArrayList<ContentLawFileViewModel>();

        try {

            while (rs.next()) {
                ContentLawFileViewModel data = new ContentLawFileViewModel();
                data.setLawId(AppUtil.encryptId(rs.getInt("LAW_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public ContentLawModel mapSaveData(ContentLawViewModel dataView) {

        ContentLawModel data = new ContentLawModel();

        try {
            data.setLawId(AppUtil.decryptId(dataView.getLawId()));
            data.setLawName(AppUtil.checkNullData(dataView.getLawName()));
            
            data.setActive(dataView.getActive());
            data.setCreatedBy(AppUtil.checkNullData(dataView.getCreatedBy()));
            data.setUpdatedBy(AppUtil.checkNullData(dataView.getUpdatedBy()));
/*
            if (dataView.getLawCoverList() != null && !dataView.getLawCoverList().isEmpty()) {
                List<ContentLawFileModel> lawCoverList = new ArrayList<>();
                for (ContentLawFileViewModel file : dataView.getLawCoverList()) {
                    ContentLawFileModel dataFile = new ContentLawFileModel();
                    dataFile.setLawId(AppUtil.decryptId(dataView.getLawId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.COVER.display());//C Cover
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    lawCoverList.add(dataFile);
                }
                data.setLawCoverList(lawCoverList);
            }
*/
            
            if (dataView.getLawImageList() != null && !dataView.getLawImageList().isEmpty()) {
                List<ContentLawFileModel>  lawImageList = new ArrayList<>();
                for (ContentLawFileViewModel file : dataView.getLawImageList()) {
                    ContentLawFileModel dataFile = new ContentLawFileModel();
                    dataFile.setLawId(AppUtil.decryptId(dataView.getLawId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.IMAGE.display());//I =image
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    lawImageList.add(dataFile);
                }
                data.setLawImageList(lawImageList);
            }

            if (dataView.getLawFileList() != null && !dataView.getLawFileList().isEmpty()) {
                List<ContentLawFileModel> contentFileList = new ArrayList<>();
                for (ContentLawFileViewModel file : dataView.getLawFileList()) {
                    ContentLawFileModel dataFile = new ContentLawFileModel();
                    dataFile.setLawId(AppUtil.decryptId(dataView.getLawId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.FILE.display());//F =File
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    contentFileList.add(dataFile);
                }
                data.setLawFileList(contentFileList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
