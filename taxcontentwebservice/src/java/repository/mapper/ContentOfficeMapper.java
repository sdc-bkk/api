/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ContentOfficeModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.ContentOfficeViewModel;

/**
 *
 * @author User
 */
public class ContentOfficeMapper {
    private ResultSet rs = null;

    public ContentOfficeMapper() {

    }

    public ContentOfficeMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<ContentOfficeViewModel> mapList() {

        List<ContentOfficeViewModel> dataList = new ArrayList<ContentOfficeViewModel>();

        try {

            while (rs.next()) {
                ContentOfficeViewModel data = new ContentOfficeViewModel();

                data.setOfficeId(AppUtil.encryptId(rs.getInt("OFFICE_ID")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public ContentOfficeViewModel mapData() {

        ContentOfficeViewModel data = new ContentOfficeViewModel();

        try {

            while (rs.next()) {

                data.setOfficeId(AppUtil.encryptId(rs.getInt("OFFICE_ID")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public ContentOfficeModel mapSaveData(ContentOfficeViewModel dataView) {

        ContentOfficeModel data = new ContentOfficeModel();

        try {
            data.setOfficeId(AppUtil.decryptId(dataView.getOfficeId()));
            data.setOfficeName(AppUtil.checkNullData(dataView.getOfficeName()));
            data.setAddress(AppUtil.checkNullData(dataView.getAddress()));
            data.setMobile(AppUtil.checkNullData(dataView.getMobile()));
            data.setEmail(AppUtil.checkNullData(dataView.getEmail()));
            
            data.setActive(dataView.getActive());
            data.setCreatedBy(AppUtil.checkNullData(dataView.getCreatedBy()));
            data.setUpdatedBy(AppUtil.checkNullData(dataView.getUpdatedBy()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
