/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ContentFAQModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.ContentFAQViewModel;

/**
 *
 * @author User
 */
public class ContentFAQMapper {
    private ResultSet rs = null;

    public ContentFAQMapper() {

    }

    public ContentFAQMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<ContentFAQViewModel> mapList() {

        List<ContentFAQViewModel> dataList = new ArrayList<ContentFAQViewModel>();

        try {

            while (rs.next()) {
                ContentFAQViewModel data = new ContentFAQViewModel();

                data.setFaqId(AppUtil.encryptId(rs.getInt("FAQ_ID")));
                data.setFaqName(AppUtil.checkNullData(rs.getString("FAQ_NAME")));
                data.setFaqQuestion(AppUtil.checkNullData(rs.getString("FAQ_QUESTION")));
                data.setFaqAnswer(AppUtil.checkNullData(rs.getString("FAQ_ANSWER")));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public ContentFAQViewModel mapData() {

        ContentFAQViewModel data = new ContentFAQViewModel();

        try {

            while (rs.next()) {

                data.setFaqId(AppUtil.encryptId(rs.getInt("FAQ_ID")));
                data.setFaqName(AppUtil.checkNullData(rs.getString("FAQ_NAME")));
                data.setFaqQuestion(AppUtil.checkNullData(rs.getString("FAQ_QUESTION")));
                data.setFaqAnswer(AppUtil.checkNullData(rs.getString("FAQ_ANSWER")));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public ContentFAQModel mapSaveData(ContentFAQViewModel dataView) {

        ContentFAQModel data = new ContentFAQModel();

        try {
            data.setFaqId(AppUtil.decryptId(dataView.getFaqId()));
            data.setFaqName(AppUtil.checkNullData(dataView.getFaqName()));
            data.setFaqQuestion(AppUtil.checkNullData(dataView.getFaqQuestion()));
            data.setFaqAnswer(AppUtil.checkNullData(dataView.getFaqAnswer()));
            
            data.setActive(dataView.getActive());
            data.setCreatedBy(AppUtil.checkNullData(dataView.getCreatedBy()));
            data.setUpdatedBy(AppUtil.checkNullData(dataView.getUpdatedBy()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
