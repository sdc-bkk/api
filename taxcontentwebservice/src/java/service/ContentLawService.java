/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import model.ContentLawFileModel;
import model.ContentLawModel;
import repository.ContentLawRepo;
import repository.mapper.ContentLawMapper;
import utility.AppUtil;
import utility.FileUtil;
import utility.MessageBundleUtil;
import viewModel.ContentLawFileViewModel;
import viewModel.ContentLawViewModel;
import viewModel.FilterContentModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class ContentLawService {
    
    ContentLawRepo repo = new ContentLawRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<ContentLawViewModel>> getList(FilterContentModel filter) {

        ResultData<List<ContentLawViewModel>> resultData = new ResultData<List<ContentLawViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ContentLawViewModel> getData(FilterContentModel filter) {

        ResultData<ContentLawViewModel> resultData = new ResultData<ContentLawViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = repo.getData(id);
            
            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getLawId())) {
                resultData.setResult(new ContentLawViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(ContentLawViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        ContentLawModel dataSave = new ContentLawModel();

        try {
            boolean resultDuplicate = repo.duplicate(data);

            if (!resultDuplicate) {
                //add LawImageList from LawFileList (pdf file)
                if (data.getLawFileList() != null && !data.getLawFileList().isEmpty()) {
                    List<ContentLawFileViewModel> list = new ArrayList<>();
                    
                    for (ContentLawFileViewModel file : data.getLawFileList()) {

                        FileUtil fileUtil = new FileUtil();
                        String fileName = file.getFileName();//"saveDefineAuditDoc.pdf";
                        String pathFile = file.getPathFile();//"14158953-f5fe-4a66-a680-14aa099da5a5.pdf";
                        list = fileUtil.convertPdfToImage(fileName, pathFile);
                        
                    }
                    
                    data.setLawImageList(list);
                }
                
                ContentLawMapper mapper = new ContentLawMapper();
                dataSave = mapper.mapSaveData(data);
                resultData = repo.save(dataSave);
            } else {
                resultData.setResult(false);
                resultData.setStatus("false");
                resultData.setResultMessage(message.getMessage("message.contentLaw.duplicate"));
            }

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteData(FilterContentModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            resultData = repo.deleteData(idDeleteList, filter);           

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }
}
