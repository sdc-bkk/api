/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import repository.ContentFAQRepo;
import repository.ContentLawRepo;
import repository.ContentOfficeRepo;
import repository.ContentRepo;
import repository.FrontContentRepo;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.ContentFAQViewModel;
import viewModel.ContentFileViewModel;
import viewModel.ContentLawViewModel;
import viewModel.ContentOfficeViewModel;
import viewModel.ContentViewModel;
import viewModel.FilterContentModel;
import viewModel.FrontContentTypeViewModel;
import viewModel.FrontContentViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class FrontContentService {

    FrontContentRepo repo = new FrontContentRepo();
    ContentRepo contentRepo = new ContentRepo();
    ContentFAQRepo contentFaqRepo = new ContentFAQRepo();
    ContentOfficeRepo contentOfficeRepo = new ContentOfficeRepo();
    ContentLawRepo contentLawRepo = new ContentLawRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<FrontContentViewModel> getDataMain(FilterContentModel filter) {

        ResultData<FrontContentViewModel> resultData = new ResultData<FrontContentViewModel>();
        FrontContentViewModel data = new FrontContentViewModel();
        List<ContentFileViewModel> contentMainList = new ArrayList<>();
        List<FrontContentTypeViewModel> contentSubList = new ArrayList<>();
        ResultPage resultPage = null;

        try {

            //<editor-fold defaultstate="collapsed" desc="sql contentMain">
            ResultPage resultPageMain = new ResultPage();
            resultPageMain.setPageNo(1);
            resultPageMain.setItemPerPage(filter.getItemPerPageMain());

            FilterContentModel filterMain = new FilterContentModel();
            filterMain.setItemPerPage(filter.getItemPerPageMain());

            contentMainList = repo.getListMain(resultPageMain, filterMain);
            data.setContentMainList(contentMainList);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="sql contentTypeList">
            ResultPage resultPageSub = new ResultPage();
            resultPageSub.setPageNo(1);
            resultPageSub.setItemPerPage(filter.getItemPerPageSub());

            FilterContentModel filterSub = new FilterContentModel();
            filterSub.setItemPerPage(filter.getItemPerPageMain());

            contentSubList = repo.getListSub(resultPageSub, filterSub);

            if (!contentSubList.isEmpty()) {
                for (FrontContentTypeViewModel content : contentSubList) {
                    List<ContentViewModel> contentList = new ArrayList<>();
                    filterSub.setContentTypeId(content.getContentTypeId());
                    contentList = repo.getListByContentType(resultPageSub, filterSub);
                    content.setContentList(contentList);
                }
            }
            data.setContentSubList(contentSubList);

            //</editor-fold>
//            int id = AppUtil.decryptId(filter.getId());
//            resultData = repo.getData(id);
//            
//            resultData.setStatus("true");
//            resultData.setResultMessage("");
//
//            if (AppUtil.isNullAndSpace(resultData.getResult().getContentId())) {
//                resultData.setResult(new ContentViewModel());
//                resultData.setResultMessage(message.getMessage("message.list.nodata"));
//            }
            resultData.setResult(data);
        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<ContentViewModel>> getList(FilterContentModel filter) {

        ResultData<List<ContentViewModel>> resultData = new ResultData<List<ContentViewModel>>();
        ResultPage resultPage = null;

        try {
            filter.setActive(Boolean.TRUE);

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }
            List<ContentViewModel> contentList = new ArrayList<>();
            contentList = repo.getListByContentType(resultPage, filter);
            resultData.setResult(contentList);
//            resultData = contentRepo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ContentViewModel> getData(FilterContentModel filter) {

        ResultData<ContentViewModel> resultData = new ResultData<ContentViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = contentRepo.getData(id);

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getContentId())) {
                resultData.setResult(new ContentViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<ContentFAQViewModel>> getListFAQ(FilterContentModel filter) {

        ResultData<List<ContentFAQViewModel>> resultData = new ResultData<List<ContentFAQViewModel>>();
        ResultPage resultPage = null;

        try {
            filter.setActive(Boolean.TRUE);
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = contentFaqRepo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ContentFAQViewModel> getDataFAQ(FilterContentModel filter) {

        ResultData<ContentFAQViewModel> resultData = new ResultData<ContentFAQViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = contentFaqRepo.getData(id);

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getFaqId())) {
                resultData.setResult(new ContentFAQViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<ContentOfficeViewModel>> getListOffice(FilterContentModel filter) {

        ResultData<List<ContentOfficeViewModel>> resultData = new ResultData<List<ContentOfficeViewModel>>();
        ResultPage resultPage = null;

        try {
            filter.setActive(Boolean.TRUE);
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = contentOfficeRepo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ContentOfficeViewModel> getDataOffice(FilterContentModel filter) {

        ResultData<ContentOfficeViewModel> resultData = new ResultData<ContentOfficeViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = contentOfficeRepo.getData(id);

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getOfficeId())) {
                resultData.setResult(new ContentOfficeViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<ContentLawViewModel>> getListLaw(FilterContentModel filter) {

        ResultData<List<ContentLawViewModel>> resultData = new ResultData<List<ContentLawViewModel>>();
        ResultPage resultPage = null;

        try {
            filter.setActive(Boolean.TRUE);
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = contentLawRepo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ContentLawViewModel> getDataLaw(FilterContentModel filter) {

        ResultData<ContentLawViewModel> resultData = new ResultData<ContentLawViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = contentLawRepo.getData(id);

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getLawId())) {
                resultData.setResult(new ContentLawViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

}
