/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author User
 */
public class ContentModel extends BaseClassModel{    
    private int contentId;
    private int contentTypeId;
    private String contentName;
    private String contentLink;
    private String displayFormat;
    private String startDate;
    private String endDate;
    private String detail;
    private boolean isMainContent;
    private List<ContentFileModel> contentCoverList;
    private List<ContentFileModel> contentFileList;
    private List<ContentFileModel> contentImageList;

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public int getContentTypeId() {
        return contentTypeId;
    }

    public void setContentTypeId(int contentTypeId) {
        this.contentTypeId = contentTypeId;
    }

    public String getContentName() {
        return contentName;
    }

    public void setContentName(String contentName) {
        this.contentName = contentName;
    }

    public String getContentLink() {
        return contentLink;
    }

    public void setContentLink(String contentLink) {
        this.contentLink = contentLink;
    }

    public String getDisplayFormat() {
        return displayFormat;
    }

    public void setDisplayFormat(String displayFormat) {
        this.displayFormat = displayFormat;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean getIsMainContent() {
        return isMainContent;
    }

    public void setIsMainContent(boolean isMainContent) {
        this.isMainContent = isMainContent;
    }

    public List<ContentFileModel> getContentCoverList() {
        return contentCoverList;
    }

    public void setContentCoverList(List<ContentFileModel> contentCoverList) {
        this.contentCoverList = contentCoverList;
    }    

    public List<ContentFileModel> getContentFileList() {
        return contentFileList;
    }

    public void setContentFileList(List<ContentFileModel> contentFileList) {
        this.contentFileList = contentFileList;
    }

    public List<ContentFileModel> getContentImageList() {
        return contentImageList;
    }

    public void setContentImageList(List<ContentFileModel> contentImageList) {
        this.contentImageList = contentImageList;
    }
    
}
