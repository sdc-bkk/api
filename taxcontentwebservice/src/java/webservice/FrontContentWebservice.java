/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.ContentService;
import service.FrontContentService;
import viewModel.ContentFAQViewModel;
import viewModel.ContentLawViewModel;
import viewModel.ContentOfficeViewModel;
import viewModel.ContentViewModel;
import viewModel.FilterContentModel;
import viewModel.FrontContentViewModel;
import viewModel.ResultData;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("frontcontent")
public class FrontContentWebservice {

    @Context
    private UriInfo context;

    FrontContentService service = new FrontContentService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataMain")
    public String getDataMain(FilterContentModel filter) {

        try {

            ResultData<FrontContentViewModel> data = service.getDataMain(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListByContentType")
    public String getListByContentType(FilterContentModel filter) {

        try {

            ResultData<List<ContentViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterContentModel filter) {

        try {

            ResultData<ContentViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }


    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListFAQ")
    public String getListFAQ(FilterContentModel filter) {

        try {

            ResultData<List<ContentFAQViewModel>> data = service.getListFAQ(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataFAQ")
    public String getDataFAQ(FilterContentModel filter) {

        try {

            ResultData<ContentFAQViewModel> data = service.getDataFAQ(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListOffice")
    public String getListOffice(FilterContentModel filter) {

        try {

            ResultData<List<ContentOfficeViewModel>> data = service.getListOffice(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataOffice")
    public String getDataOffice(FilterContentModel filter) {

        try {

            ResultData<ContentOfficeViewModel> data = service.getDataOffice(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListLaw")
    public String getListLaw(FilterContentModel filter) {

        try {

            ResultData<List<ContentLawViewModel>> data = service.getListLaw(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataLaw")
    public String getDataLaw(FilterContentModel filter) {

        try {

            ResultData<ContentLawViewModel> data = service.getDataLaw(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
}
