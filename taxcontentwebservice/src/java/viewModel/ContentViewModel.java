/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.BaseClassModel;

/**
 *
 * @author User
 */
public class ContentViewModel  extends BaseClassModel{ 
    private String contentId;
    private String contentTypeId;
    private String contentName;
    private String contentLink;
    private String contentTypeName;
    private String displayFormat;
    private String startDate;
    private String endDate;
    private String detail;
    private boolean isMainContent;
    private List<ContentFileViewModel> contentCoverList;
    private List<ContentFileViewModel> contentFileList;
    private List<ContentFileViewModel> contentImageList;

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContentTypeId() {
        return contentTypeId;
    }

    public String getContentName() {
        return contentName;
    }

    public void setContentName(String contentName) {
        this.contentName = contentName;
    }

    public void setContentTypeId(String contentTypeId) {
        this.contentTypeId = contentTypeId;
    }

    public String getContentTypeName() {
        return contentTypeName;
    }

    public void setContentTypeName(String contentTypeName) {
        this.contentTypeName = contentTypeName;
    }

    public String getContentLink() {
        return contentLink;
    }

    public void setContentLink(String contentLink) {
        this.contentLink = contentLink;
    }


    public String getDisplayFormat() {
        return displayFormat;
    }

    public void setDisplayFormat(String displayFormat) {
        this.displayFormat = displayFormat;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean getIsMainContent() {
        return isMainContent;
    }

    public void setIsMainContent(boolean isMainContent) {
        this.isMainContent = isMainContent;
    }

    public List<ContentFileViewModel> getContentCoverList() {
        return contentCoverList;
    }

    public void setContentCoverList(List<ContentFileViewModel> contentCoverList) {
        this.contentCoverList = contentCoverList;
    }

    public List<ContentFileViewModel> getContentFileList() {
        return contentFileList;
    }

    public void setContentFileList(List<ContentFileViewModel> contentFileList) {
        this.contentFileList = contentFileList;
    }

    public List<ContentFileViewModel> getContentImageList() {
        return contentImageList;
    }

    public void setContentImageList(List<ContentFileViewModel> contentImageList) {
        this.contentImageList = contentImageList;
    }

    
    
}
