/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.BaseClassModel;

/**
 *
 * @author User
 */
public class ContentLawViewModel extends BaseClassModel{
    private String lawId;
    private String lawName;
    private List<ContentLawFileViewModel> lawCoverList;
    private List<ContentLawFileViewModel> lawFileList;
    private List<ContentLawFileViewModel> lawImageList;

    public String getLawId() {
        return lawId;
    }

    public void setLawId(String lawId) {
        this.lawId = lawId;
    }

    public String getLawName() {
        return lawName;
    }

    public void setLawName(String lawName) {
        this.lawName = lawName;
    }

    public List<ContentLawFileViewModel> getLawCoverList() {
        return lawCoverList;
    }

    public void setLawCoverList(List<ContentLawFileViewModel> lawCoverList) {
        this.lawCoverList = lawCoverList;
    }

    public List<ContentLawFileViewModel> getLawFileList() {
        return lawFileList;
    }

    public void setLawFileList(List<ContentLawFileViewModel> lawFileList) {
        this.lawFileList = lawFileList;
    }

    public List<ContentLawFileViewModel> getLawImageList() {
        return lawImageList;
    }

    public void setLawImageList(List<ContentLawFileViewModel> lawImageList) {
        this.lawImageList = lawImageList;
    }

}
