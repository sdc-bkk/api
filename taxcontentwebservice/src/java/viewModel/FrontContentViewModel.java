/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class FrontContentViewModel {
    
    private List<ContentFileViewModel> contentMainList;
    private List<FrontContentTypeViewModel> contentSubList;

    public List<ContentFileViewModel> getContentMainList() {
        return contentMainList;
    }

    public void setContentMainList(List<ContentFileViewModel> contentMainList) {
        this.contentMainList = contentMainList;
    }

    public List<FrontContentTypeViewModel> getContentSubList() {
        return contentSubList;
    }

    public void setContentSubList(List<FrontContentTypeViewModel> contentSubList) {
        this.contentSubList = contentSubList;
    }
    
}
