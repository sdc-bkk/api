/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author WeAble
 */
public enum StatusEnum {

    Create(0, "สร้าง", "Create."),
    Wait(1, "รอตรวจสอบ", "Wait."),
    NoPass(2, "ไม่ผ่านการตรวจสอบ", "Pass has condition."),
    Pass(3, "ตรวจสอบแล้ว", "Pass.");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    StatusEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static StatusEnum valueOf(int value) throws IllegalArgumentException {
        for (StatusEnum newsResourceType : StatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<StatusEnum> list() {

//        List<StatusEnum> list = new ArrayList<StatusEnum>();
        List<StatusEnum> list = Arrays.asList(StatusEnum.values());

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (StatusEnum newsResourceType : StatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (StatusEnum newsResourceType : StatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
