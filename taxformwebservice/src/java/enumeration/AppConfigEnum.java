/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Sirichai
 */
public enum AppConfigEnum {

    TaxType(0, "taxtype", "รหัสภาษีน้ำมัน"),
    WsBma(1, "wsbma", "url ws"),
    TaxTypeName(2, "taxtypename", "ชื่อภาษีน้ำมัน"),
    WsBmaInvoice(3, "wsbmainvoice", "url ws ส่ง invoice"),
    WsBmaClientId(4, "wsclientid", "client id bma"),
    WsBmaClientSecret(5, "wsclientsecret", "client secret bma"),
    BankCode(6, "bankcode", "client secret bma"),
    SuffixBank(7, "suffixbank", "client secret bma");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    AppConfigEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static AppConfigEnum valueOf(int value) throws IllegalArgumentException {
        for (AppConfigEnum newsResourceType : AppConfigEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<StatusEnum> list() {

//        List<StatusEnum> list = new ArrayList<StatusEnum>();
        List<StatusEnum> list = Arrays.asList(StatusEnum.values());

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (AppConfigEnum newsResourceType : AppConfigEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (AppConfigEnum newsResourceType : AppConfigEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
