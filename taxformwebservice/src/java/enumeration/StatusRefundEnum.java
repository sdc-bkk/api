/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author WeAble
 */
public enum StatusRefundEnum {

    //Create(0, "สร้าง", "Create."),
    Wait(1, "รอตรวจสอบ", "Wait."),
    Accept(2, "อยู่ระหว่างดำเนินการ", "Processing."),
    NoPass(3, "ไม่เข้าหลักเกณฑ์", "No Pass."),
    Pass(4, "แจ้งรับเงินคืน", "Pass.");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    StatusRefundEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static StatusRefundEnum valueOf(int value) throws IllegalArgumentException {
        for (StatusRefundEnum newsResourceType : StatusRefundEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<StatusRefundEnum> list() {

//        List<StatusEnum> list = new ArrayList<StatusEnum>();
        List<StatusRefundEnum> list = Arrays.asList(StatusRefundEnum.values());

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (StatusRefundEnum newsResourceType : StatusRefundEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (StatusRefundEnum newsResourceType : StatusRefundEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
