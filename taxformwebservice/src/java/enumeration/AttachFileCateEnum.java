/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

/**
 *
 * @author Prapaporn
 */
public enum AttachFileCateEnum {

    House(1, "H", "สำเนาทะเบียนบ้าน สำเนาบัตรประจำตัวประชาชน และสำเนาบัตรประจำตัวผู้เสียภาษีอาการ"),
    IDCard(2,"I", "สำเนาบัตรประจำตัวประชาชน"),
    Tax(3, "T", "สำเนาบัตรประจำตัวผู้เสียภาษีอาการ"),
    Certificate(4, "C", "สำเนาหนังสือรับรองของกระทรวงพาณิชย์"),
    Commerce(5, "E", "สำเนาหนังสือรับรองของกระทรวงพาณิชย์"),
    Authorize(6, "A", "หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนผู้รับมอบอำนาจ"),
    TaxRemain(7, "R", "บัญชีสินค้าน้ำมัน/ก๊าซปิโตรเลียมและบัญชีรายการค้างชำระภาษี"),
    Other(8, "O", "เอกสารอื่น ๆ"),
    Agent(9, "G", "หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ"),
    Map(10, "M", "แผนที่ตั้ง"),
    Account(11, "S", "เอกสารงบเดือนแสดงรายรับ-จ่ายน้ำมัน/ก๊าซปิโตรเลียม"),
    BillPayment(12, "P", "ใบเสร็จรับเงินหรือหลักฐานการชำระเงิน"),
    BookBank(13, "B", "หน้าสมุดบัญชีธนาคาร");
    
    private final int value;

    private final String displayName;
    private final String docName;

    AttachFileCateEnum(int value, String displayName, String docName) {
        this.value = value;
        this.displayName = displayName;
        this.docName = docName;
    }

    public int value() {
        return value;
    }

    public String display() {
        return displayName;
    }

    public String docName() {
        return docName;
    }
    
    public static String getDisplayName(String value) throws IllegalArgumentException {
        for (AttachFileCateEnum name : AttachFileCateEnum.values()) {
            if (name.display().equals(value)) {
                return name.display();
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDocName (String value) throws IllegalArgumentException {
        for (AttachFileCateEnum name : AttachFileCateEnum.values()) {
            if (name.display().equals(value)) {
                return name.docName();
            }
        }
        throw new IllegalArgumentException();
    }
}
