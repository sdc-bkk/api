/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author WeAble
 */
public enum ForTypeEnum {

    Cancel(1, "เลิกกิจการ", "Cancel."),
    Move(2, "ย้ายสถานการค้าปลีก", "Move."),
    ChangeOwner(3, "เปลี่ยนชื่อผู้ประกอบการ", "ChangeOwner."),
    ChangeLocation(4, "เปลี่ยนชื่อสถานการค้าปลีก", "ChangeLocation."),
    Other(5, "อื่นๆ", "Other.");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    ForTypeEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static ForTypeEnum valueOf(int value) throws IllegalArgumentException {
        for (ForTypeEnum newsResourceType : ForTypeEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<ForTypeEnum> list() {

        List<ForTypeEnum> list = new ArrayList<ForTypeEnum>();

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (ForTypeEnum newsResourceType : ForTypeEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (ForTypeEnum newsResourceType : ForTypeEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
