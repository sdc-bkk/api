/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.AddressModel;
import repository.mapper.RetailMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationFileViewModel;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.TaxForm01RetailFileViewModel;

/**
 *
 * @author User
 */
public class RetailRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<RetailViewModel> getData(FilterTaxFormModel filter) throws SQLException {

        ResultData<RetailViewModel> resultData = new ResultData<RetailViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT RETAIL.*,M_TAMBON.TAMBON_THAI,M_TAMBON.DISTRICT_THAI,M_TAMBON.PROVINCE_THAI "
                    + " FROM RETAIL  LEFT JOIN M_TAMBON ON RETAIL.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + " WHERE (RETAIL.IS_ACTIVE = 1) ";

            if (!AppUtil.isNullAndSpace(filter.getId())) {
                sql += " AND (RETAIL.RETAIL_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (RETAIL.TAX_NO = ?) ";
            }

            ps.setSql(sql);

            if (!AppUtil.isNullAndSpace(filter.getId())) {
                ps.setInt(i++, AppUtil.decryptId(filter.getId()));
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            rs = ps.executeQuery();

            RetailViewModel data = new RetailMapper(rs).mapDataRetail();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail houseFile ">
            //เอกสารแนบสำเนาทะเบียนบ้าน
            sql = " SELECT * "
                    + " FROM RETAIL_FILE  "
                    + " WHERE RETAIL_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, AppUtil.decryptId(data.getRetailId()));
            ps.setString(2, AttachFileCateEnum.House.display());

            rs = ps.executeQuery();

            List<TaxForm01RetailFileViewModel> dataHouse = new RetailMapper(rs).mapDataRetailFile();
            data.setHouseFileList(dataHouse);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail CerFile ">
            //เอกสารแนบสำเนารับรอง
            sql = " SELECT * "
                    + " FROM RETAIL_FILE  "
                    + " WHERE RETAIL_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, AppUtil.decryptId(data.getRetailId()));
            ps.setString(2, AttachFileCateEnum.Certificate.display());

            rs = ps.executeQuery();

            List<TaxForm01RetailFileViewModel> dataCer = new RetailMapper(rs).mapDataRetailFile();
            data.setCerFileList(dataCer);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail AgentFile ">
            //เอกสารแนบสำเนาลงอำนาจ
            sql = " SELECT * "
                    + " FROM RETAIL_FILE  "
                    + " WHERE RETAIL_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, AppUtil.decryptId(data.getRetailId()));
            ps.setString(2, AttachFileCateEnum.Agent.display());

            rs = ps.executeQuery();

            List<TaxForm01RetailFileViewModel> dataAgent = new RetailMapper(rs).mapDataRetailFile();
            data.setAgentFileList(dataAgent);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public String getTaxNoByMemberName(String memberName) throws SQLException {

        String taxNo = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT TAX_NO "
                    + " FROM MEMBER_OF_RETAIL  "
                    + " WHERE EMAIL = ? ";

            ps.setSql(sql);

            ps.setString(1, memberName);

            rs = ps.executeQuery();

            while (rs.next()) {
                taxNo = rs.getString("TAX_NO");
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return taxNo;

    }

    public RetailViewModel getRetailByMemberName(String memberName) throws SQLException {
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        RetailViewModel data = new RetailViewModel();

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT DISTINCT RETAIL.TAX_NO ,RETAIL.RETAIL_ID,RETAIL.OWNER_NAME\n"
                    + "FROM MEMBER_OF_RETAIL LEFT JOIN RETAIL ON MEMBER_OF_RETAIL.TAX_NO = RETAIL.TAX_NO\n"
                    + "WHERE MEMBER_OF_RETAIL.EMAIL = ? ";

            ps.setSql(sql);

            ps.setString(1, memberName);

            rs = ps.executeQuery();

            while (rs.next()) {
                data.setRetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }

    public Boolean duplicateTaxNo(FilterTaxFormModel filter) throws SQLException {

        Boolean isDup = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT TAX_NO "
                    + " FROM MEMBER_OF_RETAIL  "
                    + " WHERE EMAIL != ? AND TAX_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, filter.getMemberName());
            ps.setString(2, filter.getTaxNo());

            rs = ps.executeQuery();

            while (rs.next()) {
                isDup = true;
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return isDup;

    }

    public ResultData<List<DrpDownViewModel>> getListStationByTaxNo(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT RETAIL_STATION.RETAIL_STATION_ID, RETAIL_STATION.STATION_NAME \n"
                    + " FROM RETAIL_STATION LEFT JOIN RETAIL ON RETAIL.RETAIL_ID = RETAIL_STATION.RETAIL_ID "
                    + " WHERE (RETAIL_STATION.IS_ACTIVE =1) ";

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (RETAIL.TAX_NO = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            orderBy = " STATION_NAME ";
            filter.setSort("ASC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();

                data.setId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("STATION_NAME")));

                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<RetailStationViewModel>> getListAllStationByTaxNo(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<RetailStationViewModel>> resultData = new ResultData<List<RetailStationViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT RETAIL_STATION.*,M_TAMBON.TAMBON_THAI,M_TAMBON.DISTRICT_THAI,M_TAMBON.PROVINCE_THAI  \n"
                    + " FROM RETAIL_STATION LEFT JOIN RETAIL ON RETAIL.RETAIL_ID = RETAIL_STATION.RETAIL_ID"
                    + " LEFT JOIN M_TAMBON ON RETAIL_STATION.TAMBON_ID = M_TAMBON.TAMBON_ID  "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (RETAIL.TAX_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                sql += " AND (RETAIL_STATION.OWNER_NAME LIKE ?) ";
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (RETAIL_STATION.STATION_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeId())) {
                sql += " AND (RETAIL_STATION.REF_OFFICE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeName())) {
                sql += " AND (RETAIL_STATION.REF_OFFICE_NAME LIKE ?) ";
            }
            
            ps.setSql(sql);

            String orderBy = "";
            orderBy = " created_Date ";
            filter.setSort("ASC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }else{                
                page = new ResultPage();
                page.setPageNo(1);
                page.setItemPerPage(500);
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }
            
            
            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                ps.setString(i++, "%" + filter.getOwnerName() +"%" );
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() +"%" );
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeId())) {
                ps.setInt(i++, AppUtil.decryptId(filter.getOfficeId()));
            }
            if (!AppUtil.isNullAndSpace(filter.getOfficeName())) {
                ps.setString(i++, "%" + filter.getOfficeName() +"%" );
            }

            rs = ps.executeQuery();

            List<RetailStationViewModel> dataList = new RetailMapper(rs).mapDataStationList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<RetailStationViewModel> getDataStation(int id) throws SQLException {

        ResultData<RetailStationViewModel> resultData = new ResultData<RetailStationViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get station ">
            String sql = " SELECT RETAIL_STATION.*,M_TAMBON.TAMBON_THAI,M_TAMBON.DISTRICT_THAI,M_TAMBON.PROVINCE_THAI  "
                    + " FROM RETAIL_STATION LEFT JOIN M_TAMBON ON RETAIL_STATION.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + " WHERE RETAIL_STATION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            RetailStationViewModel data = new RetailMapper(rs).mapDataStation();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail business type ">
            sql = " SELECT * "
                    + " FROM RETAIL_STATION_BUSINESS_TYPE  "
                    + " WHERE RETAIL_STATION_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataBusiness = new RetailMapper(rs).mapDataStationBusiness();
            data.setBusinessTypeList(dataBusiness);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail houseFile ">
            //เอกสารแนบสำเนาทะเบียนบ้าน
            sql = " SELECT * "
                    + " FROM RETAIL_STATION_FILE  "
                    + " WHERE RETAIL_STATION_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.House.display());

            rs = ps.executeQuery();

            List<RetailStationFileViewModel> dataHouse = new RetailMapper(rs).mapDataStationFile();
            data.setHouseFileList(dataHouse);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail mapFile ">
            //เอกสารแนบแผนที่
            sql = " SELECT * "
                    + " FROM RETAIL_STATION_FILE  "
                    + " WHERE RETAIL_STATION_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Map.display());

            rs = ps.executeQuery();

            List<RetailStationFileViewModel> dataMap = new RetailMapper(rs).mapDataStationFile();
            data.setMapFileList(dataMap);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public RetailStationViewModel getOfficeByStation(int stationId) throws SQLException {

        RetailStationViewModel resultData = new RetailStationViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get station ">
            String sql = " SELECT RETAIL_STATION.*,M_TAMBON.TAMBON_THAI,M_TAMBON.DISTRICT_THAI,M_TAMBON.PROVINCE_THAI  "
                    + " FROM RETAIL_STATION LEFT JOIN M_TAMBON ON RETAIL_STATION.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + " WHERE RETAIL_STATION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, stationId);

            rs = ps.executeQuery();

            resultData = new RetailMapper(rs).mapDataStation();
            // </editor-fold>

        } catch (Exception e) {
            resultData.setStatus("false");

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

}
