/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import enumeration.StatusEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import model.AddressModel;
import model.DrpDownModel;
import model.OfficeModel;
import model.TaxForm01RetailFileModel;
import model.TaxForm01RetailModel;
import model.TaxForm01StationFileModel;
import model.TaxForm01StationModel;
import repository.mapper.TaxForm01Mapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailFileViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationFileViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 *
 * @author User
 */
public class TaxForm01Repo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();
    ShareRepo shareRepo = new ShareRepo();

    public ResultData<List<TaxForm01RetailViewModel>> getList(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<TaxForm01RetailViewModel>> resultData = new ResultData<List<TaxForm01RetailViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT TAX_FORM01_RETAIL_ID,DOC_NO,DOC_DATE,TAX_NO, CREATED_DATE , OWNER_NAME"
                    + ", REF_OFFICE , REF_OFFICE_NAME , MAIN_STATUS  "
                    + ", (SELECT COUNT(TAX_FORM01_STATION_ID) FROM TAX_FORM01_STATION S WHERE S.TAX_FORM01_RETAIL_ID = TAX_FORM01_RETAIL.TAX_FORM01_RETAIL_ID ) STATION_NUM\n"
                    + "FROM TAX_FORM01_RETAIL  "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND (MAIN_STATUS = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND (OWNER_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                sql += " AND (REF_OFFICE_NAME = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (DOC_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(CREATED_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(CREATED_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (TAX_NO = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("createdDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                ps.setString(i++, filter.getAmphurName());
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, filter.getDocNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            rs = ps.executeQuery();

            List<TaxForm01RetailViewModel> dataList = null;
            dataList = new TaxForm01Mapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<TaxForm01StationViewModel>> getListForOfficer(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<TaxForm01StationViewModel>> resultData = new ResultData<List<TaxForm01StationViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT TAX_FORM01_STATION.*,TAX_FORM01_RETAIL.DOC_NO,TAX_FORM01_RETAIL.DOC_DATE,TAX_FORM01_RETAIL.TAX_NO \n"
                    + ",TAX_FORM01_RETAIL.OWNER_NAME "
                    + "FROM TAX_FORM01_STATION "
                    + "LEFT JOIN  TAX_FORM01_RETAIL ON TAX_FORM01_STATION.TAX_FORM01_RETAIL_ID=TAX_FORM01_RETAIL.TAX_FORM01_RETAIL_ID "
                    + " WHERE (1=1)  AND STATUS NOT IN( 0 ) "; // 0 สร้าง 1 รอตรวจ 2 ไม่ผ่าน 3 ผ่าน

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND (STATUS = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND (TAX_FORM01_RETAIL.OWNER_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (STATION_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                sql += " AND (AMPHUR_NAME = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (STATION_CODE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (TAX_FORM01_RETAIL.TAX_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(TAX_FORM01_RETAIL.DOC_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(TAX_FORM01_RETAIL.DOC_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("createdDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                ps.setString(i++, filter.getAmphurId());
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, filter.getDocNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            rs = ps.executeQuery();

            List<TaxForm01StationViewModel> dataList = new ArrayList<>();

            while (rs.next()) {
                TaxForm01StationViewModel data = new TaxForm01StationViewModel();
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));

                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBranchName(AppUtil.checkNullData(rs.getString("BRANCH_NAME")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));

                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                // <editor-fold defaultstate="collapsed" desc=" get retail business type ">
                PreparedStatementDB psBusiness = new PreparedStatementDB(conn);
                ResultSet rsBusiness = null;
                sql = " SELECT * "
                        + " FROM TAX_FORM01_STATION_BUSINESS_TYPE  "
                        + " WHERE TAX_FORM01_STATION_ID = ?  ";

                psBusiness.setSql(sql);

                psBusiness.setInt(1, rs.getInt("TAX_FORM01_STATION_ID"));

                rsBusiness = psBusiness.executeQuery();

                List<DrpDownViewModel> dataBusiness = new TaxForm01Mapper(rsBusiness).mapDataStationBusiness();
                data.setBusinessTypeList(dataBusiness);
                // </editor-fold>

                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<DrpDownViewModel>> getDrpListRetail(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT TAX_FORM01_RETAIL_ID, OWNER_NAME \n"
                    + "FROM TAX_FORM01_RETAIL  "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNullAndSpace(filter.getMemberName())) {
                sql += " AND (CREATED_BY = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            orderBy = " OWNER_NAME ";
            filter.setSort("ASC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getMemberName())) {
                ps.setString(i++, filter.getMemberName());
            }

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();

                data.setId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));

                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<DrpDownViewModel>> getDrpListStation(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT TAX_FORM01_STATION_ID, STATION_NAME \n"
                    + " FROM TAX_FORM01_STATION  "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNullAndSpace(filter.getTaxForm01RetailId())) {
                sql += " AND (TAX_FORM01_RETAIL_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getMemberName())) {
                sql += " AND (CREATED_BY = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            orderBy = " STATION_NAME ";
            filter.setSort("ASC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxForm01RetailId())) {
                ps.setInt(i++, AppUtil.decryptId(filter.getTaxForm01RetailId()));
            }

            if (!AppUtil.isNullAndSpace(filter.getMemberName())) {
                ps.setString(i++, filter.getMemberName());
            }

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();

                data.setId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("STATION_NAME")));

                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<RetailStationViewModel>> getListStatusLog(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<RetailStationViewModel>> resultData = new ResultData<List<RetailStationViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT TAX_FORM01_STATION_ID, STATION_NAME, STATION_CODE \n"
                    + " FROM TAX_FORM01_STATION  "
                    + " WHERE (TAX_FORM01_RETAIL_ID = ?) ";

            ps.setSql(sql);

            String orderBy = "";
            orderBy = " STATION_NAME ";
            filter.setSort("ASC");
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            ps.setInt(i++, AppUtil.decryptId(filter.getTaxForm01RetailId()));

            rs = ps.executeQuery();

            List<RetailStationViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
                RetailStationViewModel data = new RetailStationViewModel();

                data.setRetailStationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));

                // <editor-fold defaultstate="collapsed" desc=" get log ">
                PreparedStatementDB psLog = new PreparedStatementDB(conn);
                ResultSet rsLog = null;
                sql = " SELECT * "
                        + " FROM TAX_FORM01_STATUS_LOG  "
                        + " WHERE TAX_FORM01_STATION_ID = ?  ORDER BY ACTION_DATE DESC ";

                psLog.setSql(sql);

                psLog.setInt(1, rs.getInt("TAX_FORM01_STATION_ID"));

                rsLog = psLog.executeQuery();

                List<StatusLogViewModel> dataLog = new TaxForm01Mapper(rsLog).mapDataStatusLog();
                data.setLogList(dataLog);
                // </editor-fold>

                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " TAX_FORM01_RETAIL_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("ownerName")) {
                str = " OWNER_NAME ";
            } else if (orderBy.equalsIgnoreCase("amphurName")) {
                str = " AMPHUR_NAME ";
            } else if (orderBy.equalsIgnoreCase("createdDate")) {
                str = " CREATED_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " CREATED_DATE ";
            }
        }

        return str;

    }

    public ResultData<TaxForm01RetailViewModel> getData(int id) throws SQLException {

        ResultData<TaxForm01RetailViewModel> resultData = new ResultData<TaxForm01RetailViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get TAX_FORM01_RETAIL ">
            String sql = " SELECT TAX_FORM01_RETAIL.*"
                    + ",M_TAMBON.TAMBON_THAI TAMBON_NAME,M_TAMBON.DISTRICT_THAI AMPHUR_NAME,M_TAMBON.PROVINCE_THAI PROVINCE_NAME  "
                    + " FROM TAX_FORM01_RETAIL LEFT JOIN M_TAMBON ON TAX_FORM01_RETAIL.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + " WHERE TAX_FORM01_RETAIL_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            TaxForm01RetailViewModel data = new TaxForm01Mapper(rs).mapDataRetail();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail houseFile ">
            //เอกสารแนบสำเนาทะเบียนบ้าน
            sql = " SELECT * "
                    + " FROM TAX_FORM01_RETAIL_FILE  "
                    + " WHERE TAX_FORM01_RETAIL_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.House.display());

            rs = ps.executeQuery();

            List<TaxForm01RetailFileViewModel> dataHouse = new TaxForm01Mapper(rs).mapDataRetailFile();
            data.setHouseFileList(dataHouse);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail CERTIFICATEFile ">
            //เอกสารแนบ	สำเนาหนังสือรับรองของกระทรวจพาณิชย์
            sql = " SELECT * "
                    + " FROM TAX_FORM01_RETAIL_FILE  "
                    + " WHERE TAX_FORM01_RETAIL_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Certificate.display());

            rs = ps.executeQuery();

            List<TaxForm01RetailFileViewModel> dataCer = new TaxForm01Mapper(rs).mapDataRetailFile();
            data.setCerFileList(dataCer);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail agentFile ">
            //เอกสารแนบ	หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
            sql = " SELECT * "
                    + " FROM TAX_FORM01_RETAIL_FILE  "
                    + " WHERE TAX_FORM01_RETAIL_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Agent.display());

            rs = ps.executeQuery();

            List<TaxForm01RetailFileViewModel> dataAgent = new TaxForm01Mapper(rs).mapDataRetailFile();
            data.setAgentFileList(dataAgent);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get stationList ">
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION  "
                    + " WHERE TAX_FORM01_RETAIL_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<TaxForm01StationViewModel> stationList = new ArrayList<TaxForm01StationViewModel>();

            while (rs.next()) {
                TaxForm01StationViewModel dataStation = new TaxForm01StationViewModel();
                int stationId = rs.getInt("TAX_FORM01_STATION_ID");
                dataStation = _getDataStation(stationId, conn);
                stationList.add(dataStation);
            }
//            stationList = new TaxForm01Mapper(rs).mapListStation();
            data.setStationList(stationList);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public TaxForm01StationViewModel _getDataStation(int id, Connection conn) throws SQLException {

        TaxForm01StationViewModel resultData = new TaxForm01StationViewModel();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get station ">
            String sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION  "
                    + " WHERE TAX_FORM01_STATION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            resultData = new TaxForm01Mapper(rs).mapDataStation();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail business type ">
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION_BUSINESS_TYPE  "
                    + " WHERE TAX_FORM01_STATION_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataBusiness = new TaxForm01Mapper(rs).mapDataStationBusiness();
            resultData.setBusinessTypeList(dataBusiness);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail houseFile ">
            //เอกสารแนบสำเนาทะเบียนบ้าน
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION_FILE  "
                    + " WHERE TAX_FORM01_STATION_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.House.display());

            rs = ps.executeQuery();

            List<TaxForm01StationFileViewModel> dataHouse = new TaxForm01Mapper(rs).mapDataStationFile();
            resultData.setHouseFileList(dataHouse);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail mapFile ">
            //เอกสารแนบแผนที่
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION_FILE  "
                    + " WHERE TAX_FORM01_STATION_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Map.display());

            rs = ps.executeQuery();

            List<TaxForm01StationFileViewModel> dataMap = new TaxForm01Mapper(rs).mapDataStationFile();
            resultData.setMapFileList(dataMap);
            // </editor-fold>

        } catch (Exception e) {

        } finally {

        }

        return resultData;

    }

    public ResultData<TaxForm01StationViewModel> getDataStation(int id) throws SQLException {

        ResultData<TaxForm01StationViewModel> resultData = new ResultData<TaxForm01StationViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get STATION ">
            String sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION  "
                    + " WHERE TAX_FORM01_STATION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            TaxForm01StationViewModel data = new TaxForm01Mapper(rs).mapDataStation();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail business type ">
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION_BUSINESS_TYPE  "
                    + " WHERE TAX_FORM01_STATION_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataBusiness = new TaxForm01Mapper(rs).mapDataStationBusiness();
            data.setBusinessTypeList(dataBusiness);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail houseFile ">
            //เอกสารแนบสำเนาทะเบียนบ้าน
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION_FILE  "
                    + " WHERE TAX_FORM01_STATION_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.House.display());

            rs = ps.executeQuery();

            List<TaxForm01StationFileViewModel> dataHouse = new TaxForm01Mapper(rs).mapDataStationFile();
            data.setHouseFileList(dataHouse);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail mapFile ">
            //เอกสารแนบแผนที่
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION_FILE  "
                    + " WHERE TAX_FORM01_STATION_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Map.display());

            rs = ps.executeQuery();

            List<TaxForm01StationFileViewModel> dataMap = new TaxForm01Mapper(rs).mapDataStationFile();
            data.setMapFileList(dataMap);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(TaxForm01RetailModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";
            String refOffice = ""; //รหัสเขต
            String refOfficeName = "";//ชื่อเขต

            //<editor-fold defaultstate="collapsed" desc="ดึงที่อยู่ของ tax02Retail "> 
            AddressModel addressData = new AddressModel();
            addressData = getAddressByTambonId(data.getTambonId());
            data.setTambonName(addressData.getTambonName());
            data.setAmphurName(addressData.getAmphurName());
            data.setProvinceName(addressData.getProvinceName());

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="ตรวจสอบสำนักงานเขตที่จะเอามาแสดง "> 
            //ดูจาก station รายการแรก            
            OfficeModel officeData = new OfficeModel();
            if (data.getStationList() != null && !data.getStationList().isEmpty()) {
//                refOffice = data.getStationList().get(0).getAmphurId();
//                refOfficeName = data.getStationList().get(0).getAmphurName();
                officeData = shareRepo.getOfficeDataByAmphurId(data.getStationList().get(0).getAmphurId());

            }

            //</editor-fold>
//            if (data.getRefRetailId() == 0) { 
            if (data.getMainStatus().equals(Integer.toString(StatusEnum.Create.value()))) {

                //<editor-fold defaultstate="collapsed" desc="Check has data Member of Retail">  
                ResultSet rs1 = null;
                sql = " SELECT TAX_NO FROM MEMBER_OF_RETAIL WHERE EMAIL = ? AND IS_ACTIVE = 1 ";

                ps = conn.prepareStatement(sql);
                ps.setString(i++, data.getMemberName());

                rs1 = ps.executeQuery();
                Boolean hasDataMemberRetail = false;

                while (rs1.next()) {
                    hasDataMemberRetail = true;
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT Member Of Retail">   
//                if (data.getRefRetailId() == 0) {
                i = 1;
                if (hasDataMemberRetail == false) {
                    sql = "INSERT INTO MEMBER_OF_RETAIL("
                            + " TAX_NO "
                            + ",EMAIL "
                            + ",IS_ACTIVE "
                            + ") VALUES(?,?,1)";

                    ps = conn.prepareStatement(sql);

                    ps.setString(i++, data.getTaxNo());
                    ps.setString(i++, data.getMemberName());

                    result = ps.executeUpdate() != 0;
                    
                } else {
                    sql = "UPDATE MEMBER_OF_RETAIL SET"
                            + " TAX_NO= ? "
                            + " WHERE EMAIL = ? ";

                    ps = conn.prepareStatement(sql);

                    ps.setString(i++, data.getTaxNo());
                    ps.setString(i++, data.getMemberName());

                    result = ps.executeUpdate() != 0;
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Check has data Retail">  
                ResultSet rs2 = null;
                sql = " SELECT RETAIL_ID FROM RETAIL WHERE TAX_NO = ? ";

                ps = conn.prepareStatement(sql);
                ps.setString(1, data.getTaxNo());

                rs2 = ps.executeQuery();

                while (rs2.next()) {
                    data.setRefRetailId(rs2.getInt("RETAIL_ID"));
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="retail"> 
                i = 1;
                if (data.getRefRetailId() == 0) {

                    sql = "INSERT INTO RETAIL ( "
                            + "  OWNER_NAME "
                            + ", MAIN_STATUS "
                            + ", TAX_NO "
                            + ", CUSTOMER_TYPE "
                            + ", ID_CARD "
                            + ", ID_CARD_AT "
                            + ", CORP_NO "
                            + ", CORP_DATE "
                            + ", HOUSE_NO "
                            + ", ADDRESS "
                            + ", SOI "
                            + ", MOO "
                            + ", ROAD "
                            + ", TAMBON_ID "
                            + ", AMPHUR_ID "
                            + ", PROVINCE_ID "
                            + ", POSTCODE "
                            + ", MOBILE "
                            + ", EMAIL "
                            + ", SIGNER_NAME "
                            + ", REF_OFFICE "
                            + ", REF_OFFICE_NAME "
                            + ", REF_OFFICE_CODE "
                            + ", HOUSE_FILE_NUM "
                            + ", CER_FILE_NUM "
                            + ", AGENT_FILE_NUM "
                            + ", IS_ACTIVE"
                            + ", CREATED_DATE "
                            + ", CREATED_BY"
                            + ") VALUES(?,?,?,?  ,?,?,?,?,?  ,?,?,?,?,?  ,?,?,?,?,?,?  ,?,?,?  ,?,?,?  ,?,SYSDATE,?) ";

                    ps = conn.prepareStatement(sql, new String[]{"RETAIL_ID"});
                } else {

                    sql = "UPDATE RETAIL SET "
                            + "  OWNER_NAME = ? "
                            + ", MAIN_STATUS = ? "
                            + ", TAX_NO = ? "
                            + ", CUSTOMER_TYPE = ? "
                            + ", ID_CARD = ? "
                            + ", ID_CARD_AT = ? "
                            + ", CORP_NO = ? "
                            + ", CORP_DATE = ? "
                            + ", HOUSE_NO = ? "
                            + ", ADDRESS = ? "
                            + ", SOI = ? "
                            + ", MOO = ? "
                            + ", ROAD = ? "
                            + ", TAMBON_ID = ? "
                            + ", AMPHUR_ID = ? "
                            + ", PROVINCE_ID = ? "
                            + ", POSTCODE = ? "
                            + ", MOBILE = ? "
                            + ", EMAIL= ? "
                            + ", SIGNER_NAME= ? "
                            + ", REF_OFFICE = ? "
                            + ", REF_OFFICE_NAME= ? "
                            + ", REF_OFFICE_CODE= ? "
                            + ", HOUSE_FILE_NUM = ? "
                            + ", CER_FILE_NUM = ? "
                            + ", AGENT_FILE_NUM = ? "
                            + ", IS_ACTIVE = ? "
                            + ", UPDATED_DATE = SYSDATE "
                            + ", UPDATED_BY = ? "
                            + " WHERE RETAIL_ID = ? ";

                    ps = conn.prepareStatement(sql);

                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="set parameter">
                ps.setString(i++, data.getOwnerName());
                if (data.getRefRetailId() == 0) {
                    ps.setString(i++, String.valueOf(StatusEnum.Create.value()));
                } else {
                    ps.setString(i++, String.valueOf(data.getMainStatus()));
                }
                ps.setString(i++, data.getTaxNo());
                ps.setString(i++, data.getCustomerType());
                ps.setString(i++, data.getIdCard());
                ps.setString(i++, data.getIdCardAt());
                ps.setString(i++, data.getCorpNo());
                ps.setDate(i++, AppUtil.toDateSql(data.getCorpDate()));

                ps.setString(i++, data.getHouseNo());
                ps.setString(i++, data.getAddress());
                ps.setString(i++, data.getSoi());
                ps.setString(i++, data.getMoo());
                ps.setString(i++, data.getRoad());
                ps.setString(i++, data.getTambonId());
                ps.setString(i++, data.getAmphurId());
                ps.setString(i++, data.getProvinceId());
                ps.setString(i++, data.getPostcode());

                ps.setString(i++, data.getMobile());
                ps.setString(i++, data.getEmail());
                ps.setString(i++, data.getSignerName());

                ps.setString(i++, officeData.getOfficeId());
                ps.setString(i++, officeData.getOfficeName());
                ps.setString(i++, officeData.getOfficeCode());

                ps.setInt(i++, data.getHouseFileNum());
                ps.setInt(i++, data.getCerFileNum());
                ps.setInt(i++, data.getAgentFileNum());

                if (data.getRefRetailId() == 0) {
                    ps.setBoolean(i++, true);
                } else {
                    ps.setBoolean(i++, data.getActive());
                }

                if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                    ps.setString(i++, data.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                if (data.getRefRetailId() == 0) {
                    result = ps.executeUpdate() != 0;

                    ResultSet rs = ps.getGeneratedKeys();
                    int id = 0;
                    while (rs.next()) {
                        id = rs.getInt(1);
                    }

                    if (id <= 0) {
                        throw new Exception();
                    }

                    data.setRefRetailId(id);
                } else {

                    ps.setInt(i++, data.getRefRetailId());

                    result = ps.executeUpdate() != 0;
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT ATTACH FILE RETIAL">
                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM RETAIL_FILE WHERE RETAIL_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getRefRetailId());

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT House">
                if (data.getHouseFileList() != null && !data.getHouseFileList().isEmpty()) {
                    for (TaxForm01RetailFileModel file : data.getHouseFileList()) {
                        if (data.getRefRetailId() != 0) {
                            i = 1;

                            sql = "INSERT INTO RETAIL_FILE("
                                    + " RETAIL_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getRefRetailId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT cer">
                if (data.getCerFileList() != null && !data.getCerFileList().isEmpty()) {
                    for (TaxForm01RetailFileModel file : data.getCerFileList()) {
                        if (data.getRefRetailId() != 0) {
                            i = 1;

                            sql = "INSERT INTO RETAIL_FILE("
                                    + " RETAIL_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getRefRetailId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT Agent">
                if (data.getAgentFileList() != null && !data.getAgentFileList().isEmpty()) {
                    for (TaxForm01RetailFileModel file : data.getAgentFileList()) {
                        if (data.getRefRetailId() != 0) {
                            i = 1;

                            sql = "INSERT INTO RETAIL_FILE("
                                    + " RETAIL_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getRefRetailId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //</editor-fold>
            }

            //<editor-fold defaultstate="collapsed" desc="tax_form01 retail">
            i = 1;
            if (data.getTaxForm01RetailId() == 0) {

                sql = "INSERT INTO TAX_FORM01_RETAIL ( "
                        + "  REF_RETAIL_ID "
                        + ", DOC_NO "
                        + ", DOC_DATE "
                        + ", MAIN_STATUS "
                        + ", OWNER_NAME "
                        + ", TAX_NO "
                        + ", CUSTOMER_TYPE "
                        + ", ID_CARD "
                        + ", ID_CARD_AT "
                        + ", CORP_NO "
                        + ", CORP_DATE "
                        + ", HOUSE_NO "
                        + ", ADDRESS "
                        + ", SOI "
                        + ", MOO "
                        + ", ROAD "
                        + ", TAMBON_ID "
                        + ", TAMBON_NAME "
                        + ", AMPHUR_ID "
                        + ", AMPHUR_NAME "
                        + ", PROVINCE_ID "
                        + ", PROVINCE_NAME "
                        + ", POSTCODE "
                        + ", MOBILE "
                        + ", EMAIL "
                        + ", SIGNER_NAME "
                        + ", HOUSE_FILE_NUM "
                        + ", CER_FILE_NUM "
                        + ", AGENT_FILE_NUM "
                        + ", FIRST_RECORD "
                        + ", REF_OFFICE "
                        + ", REF_OFFICE_NAME "
                        + ", REF_OFFICE_CODE "
                        + ", IS_ACTIVE"
                        + ", CREATED_DATE "
                        + ", CREATED_BY "
                        + ") VALUES(?,?,SYSDATE,?,?,?,?  ,?,?,?,? ,? ,?,?,?,?,?  ,?,?,?,?,?,?  ,?,?,?,?,?,?  ,?,?,? ,? ,?,SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"TAX_FORM01_RETAIL_ID"});

            } else {

                sql = "UPDATE TAX_FORM01_RETAIL SET "
                        + "  REF_RETAIL_ID = ? "
                        + ", DOC_NO = ? "
                        //                        + ", DOC_DATE = ? "
                        + ", MAIN_STATUS = ? "
                        + ", OWNER_NAME = ? "
                        + ", TAX_NO = ? "
                        + ", CUSTOMER_TYPE = ? "
                        + ", ID_CARD = ? "
                        + ", ID_CARD_AT = ? "
                        + ", CORP_NO = ? "
                        + ", CORP_DATE = ? "
                        + ", HOUSE_NO = ? "
                        + ", ADDRESS = ? "
                        + ", SOI = ? "
                        + ", MOO = ? "
                        + ", ROAD = ? "
                        + ", TAMBON_ID = ? "
                        + ", TAMBON_NAME = ? "
                        + ", AMPHUR_ID = ? "
                        + ", AMPHUR_NAME = ? "
                        + ", PROVINCE_ID = ? "
                        + ", PROVINCE_NAME = ? "
                        + ", POSTCODE = ? "
                        + ", MOBILE = ? "
                        + ", EMAIL = ? "
                        + ", SIGNER_NAME = ? "
                        + ", HOUSE_FILE_NUM  = ? "
                        + ", CER_FILE_NUM = ? "
                        + ", AGENT_FILE_NUM = ?  "
                        + ", FIRST_RECORD = ? "
                        + ", REF_OFFICE = ? "
                        + ", REF_OFFICE_NAME = ? "
                        + ", REF_OFFICE_CODE = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE TAX_FORM01_RETAIL_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setInt(i++, data.getRefRetailId());
            if (data.getTaxForm01RetailId() == 0) {
                ps.setString(i++, AppUtil.getRunningDoc("taxform01", officeData.getOfficeCode()));
                ps.setString(i++, String.valueOf(StatusEnum.Create.value()));
            } else {
                ps.setString(i++, data.getDocNo());
                ps.setString(i++, String.valueOf(data.getMainStatus()));
            }
            ps.setString(i++, data.getOwnerName());
            ps.setString(i++, data.getTaxNo());
            ps.setString(i++, data.getCustomerType());
            ps.setString(i++, data.getIdCard());
            ps.setString(i++, data.getIdCardAt());
            ps.setString(i++, data.getCorpNo());
            ps.setDate(i++, AppUtil.toDateSql(data.getCorpDate()));

            ps.setString(i++, data.getHouseNo());
            ps.setString(i++, data.getAddress());
            ps.setString(i++, data.getSoi());
            ps.setString(i++, data.getMoo());
            ps.setString(i++, data.getRoad());
            ps.setString(i++, data.getTambonId());
            ps.setString(i++, data.getTambonName());
            ps.setString(i++, data.getAmphurId());
            ps.setString(i++, data.getAmphurName());
            ps.setString(i++, data.getProvinceId());
            ps.setString(i++, data.getProvinceName());
            ps.setString(i++, data.getPostcode());

            ps.setString(i++, data.getMobile());
            ps.setString(i++, data.getEmail());
            ps.setString(i++, data.getSignerName());

            ps.setInt(i++, data.getHouseFileNum());
            ps.setInt(i++, data.getCerFileNum());
            ps.setInt(i++, data.getAgentFileNum());

            ps.setBoolean(i++, data.getFirstRecord());

            ps.setString(i++, officeData.getOfficeId());
            ps.setString(i++, officeData.getOfficeName());
            ps.setString(i++, officeData.getOfficeCode());

            if (data.getTaxForm01RetailId() == 0) {
                ps.setBoolean(i++, true);
            } else {
                ps.setBoolean(i++, data.getActive());
            }

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getTaxForm01RetailId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setTaxForm01RetailId(id);
            } else {

                ps.setInt(i++, data.getTaxForm01RetailId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT TAXFORM01_RETAIL_FILE">
            if (data.getTaxForm01RetailId() != 0) {

                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM TAX_FORM01_RETAIL_FILE WHERE TAX_FORM01_RETAIL_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm01RetailId());

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT House">
                if (data.getHouseFileList() != null && !data.getHouseFileList().isEmpty()) {
                    for (TaxForm01RetailFileModel file : data.getHouseFileList()) {
                        if (data.getTaxForm01RetailId() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM01_RETAIL_FILE("
                                    + " TAX_FORM01_RETAIL_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm01RetailId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT cer">
                if (data.getCerFileList() != null && !data.getCerFileList().isEmpty()) {
                    for (TaxForm01RetailFileModel file : data.getCerFileList()) {
                        if (data.getTaxForm01RetailId() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM01_RETAIL_FILE("
                                    + " TAX_FORM01_RETAIL_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm01RetailId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT Agent">
                if (data.getAgentFileList() != null && !data.getAgentFileList().isEmpty()) {
                    for (TaxForm01RetailFileModel file : data.getAgentFileList()) {
                        if (data.getTaxForm01RetailId() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM01_RETAIL_FILE("
                                    + " TAX_FORM01_RETAIL_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm01RetailId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT STATIONLIST">
            if (data.getStationList() != null && !data.getStationList().isEmpty()) {

                //<editor-fold defaultstate="collapsed" desc="update status child = 0">                
                i = 1;

                sql = "UPDATE RETAIL_STATION SET IS_ACTIVE =0 WHERE TAX_FORM01_RETAIL_ID = ? ";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm01RetailId());

                ps.executeUpdate();

                i = 1;

                sql = "UPDATE TAX_FORM01_STATION SET IS_ACTIVE =0 WHERE TAX_FORM01_RETAIL_ID = ? ";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm01RetailId());

                ps.executeUpdate();
                //</editor-fold>

                for (TaxForm01StationModel station : data.getStationList()) {

                    //<editor-fold defaultstate="collapsed" desc="ตรวจสอบสำนักงานเขตที่จะเอามาแสดง "> 
                    //ดูจาก station รายการแรก            
                    OfficeModel officeChildData = new OfficeModel();
                    officeChildData = shareRepo.getOfficeDataByAmphurId(station.getAmphurId());

                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="INSERT STATION">
//                    if (station.getRefStationId()!= 0) {
                    //<editor-fold defaultstate="collapsed" desc="sql station">
                    i = 1;
                    if (station.getRefStationId() == 0) {

                        sql = "INSERT INTO RETAIL_STATION ( "
                                + "  RETAIL_ID "
                                + ", STATION_NAME "
                                + ", STATION_CODE "
                                + ", STATUS "
                                + ", BRANCH_NAME "
                                + ", HOUSE_NO "
                                + ", ADDRESS "
                                + ", SOI "
                                + ", MOO "
                                + ", ROAD "
                                + ", TAMBON_ID "
                                + ", AMPHUR_ID "
                                + ", PROVINCE_ID "
                                + ", POSTCODE "
                                + ", MOBILE "
                                + ", EMAIL "
                                + ", REF_OFFICE "
                                + ", REF_OFFICE_NAME "
                                + ", REF_OFFICE_CODE "
                                + ", HOUSE_FILE_NUM"
                                + ", MAP_FILE_NUM"
                                + ", TAX_FORM01_RETAIL_ID "
                                + ", IS_ACTIVE"
                                + ", CREATED_DATE "
                                + ", CREATED_BY"
                                + ") VALUES(?,?,?,?,?  ,?,?,?,?,?  ,?,?,?,?,?  ,?,?,?,?,?   ,?,?,?,  SYSDATE,?) ";

                        ps = conn.prepareStatement(sql, new String[]{"RETAIL_STATION_ID"});

                    } else {

                        sql = "UPDATE RETAIL_STATION SET "
                                + "  RETAIL_ID = ? "
                                + ", STATION_NAME = ? "
                                + ", STATION_CODE = ? "
                                + ", STATUS = ? "
                                + ", BRANCH_NAME  = ? "
                                + ", HOUSE_NO = ? "
                                + ", ADDRESS = ? "
                                + ", SOI = ? "
                                + ", MOO = ? "
                                + ", ROAD = ? "
                                + ", TAMBON_ID = ? "
                                + ", AMPHUR_ID = ? "
                                + ", PROVINCE_ID = ? "
                                + ", POSTCODE = ? "
                                + ", MOBILE = ? "
                                + ", EMAIL= ? "
                                + ", REF_OFFICE= ?  "
                                + ", REF_OFFICE_NAME= ?  "
                                + ", REF_OFFICE_CODE= ?  "
                                + ", HOUSE_FILE_NUM= ? "
                                + ", MAP_FILE_NUM= ? "
                                + ", TAX_FORM01_RETAIL_ID= ? "
                                + ", IS_ACTIVE = ? "
                                + ", UPDATED_DATE = SYSDATE "
                                + ", UPDATED_BY = ? "
                                + " WHERE RETAIL_STATION_ID = ? ";

                        ps = conn.prepareStatement(sql);

                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="set parameter">
                    ps.setInt(i++, data.getRefRetailId());
                    ps.setString(i++, station.getStationName());
                    if (station.getRefStationId() == 0) {
                        ps.setString(i++, String.format("%04d", _getCountStation(data.getRefRetailId(), conn) + 1));
                        ps.setString(i++, String.valueOf(StatusEnum.Create.value()));
                    } else {
                        ps.setString(i++, station.getStationCode());
                        ps.setString(i++, String.valueOf(station.getStatus()));
                    }

                    ps.setString(i++, station.getBranchName());

                    ps.setString(i++, station.getHouseNo());
                    ps.setString(i++, station.getAddress());
                    ps.setString(i++, station.getSoi());
                    ps.setString(i++, station.getMoo());
                    ps.setString(i++, station.getRoad());

                    ps.setString(i++, station.getTambonId());
                    ps.setString(i++, station.getAmphurId());
                    ps.setString(i++, station.getProvinceId());

                    ps.setString(i++, station.getPostcode());
                    ps.setString(i++, station.getMobile());
                    ps.setString(i++, station.getEmail());

                    ps.setString(i++, officeChildData.getOfficeId());
                    ps.setString(i++, officeChildData.getOfficeName());
                    ps.setString(i++, officeChildData.getOfficeCode());

                    ps.setInt(i++, station.getHouseFileNum());
                    ps.setInt(i++, station.getMapFileNum());

                    ps.setInt(i++, data.getTaxForm01RetailId());
//                    if (station.getRefStationId() == 0) {
//                        ps.setBoolean(i++, true);
//                    } else {
                    ps.setBoolean(i++, true);
//                    }

                    if (!AppUtil.isNullAndSpace(station.getUpdatedBy())) {
                        ps.setString(i++, station.getUpdatedBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }

                    if (station.getRefStationId() == 0) {
                        result = ps.executeUpdate() != 0;

                        ResultSet rs = ps.getGeneratedKeys();
                        int id = 0;
                        while (rs.next()) {
                            id = rs.getInt(1);
                        }

                        if (id <= 0) {
                            throw new Exception();
                        }

                        station.setRefStationId(id);
                    } else {

                        ps.setInt(i++, station.getRefStationId());

                        result = ps.executeUpdate() != 0;
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="INSERT BUSINESS_TYPE">
                    if (station.getRefStationId() != 0) {

                        //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                        i = 1;

                        sql = "DELETE FROM RETAIL_STATION_BUSINESS_TYPE WHERE RETAIL_STATION_ID = ?";

                        ps = conn.prepareStatement(sql);

                        ps.setInt(i++, station.getRefStationId());

                        ps.executeUpdate();
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="INSERT RETAIL_STATION_BUSINESS_TYPE">
                        if (station.getBusinessTypeList() != null && !station.getBusinessTypeList().isEmpty()) {
                            for (DrpDownModel child : station.getBusinessTypeList()) {
                                if (station.getRefStationId() != 0) {
                                    i = 1;

                                    sql = "INSERT INTO RETAIL_STATION_BUSINESS_TYPE("
                                            + " RETAIL_STATION_ID "
                                            + ",BUSINESS_TYPE_ID"
                                            + ",BUSINESS_TYPE_NAME "
                                            + ") VALUES(?,?,?)";

                                    ps = conn.prepareStatement(sql);

                                    ps.setInt(i++, station.getRefStationId());
                                    ps.setInt(i++, child.getId());
                                    ps.setString(i++, child.getName());

                                    result = ps.executeUpdate() != 0;
                                }
                            }
                        }
                        //</editor-fold>
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="INSERT ATTACH FILE">
                    if (station.getRefStationId() != 0) {

                        //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                        i = 1;

                        sql = "DELETE FROM RETAIL_STATION_FILE WHERE RETAIL_STATION_ID = ?";

                        ps = conn.prepareStatement(sql);

                        ps.setInt(i++, station.getRefStationId());

                        ps.executeUpdate();
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="INSERT House">
                        if (station.getHouseFileList() != null && !station.getHouseFileList().isEmpty()) {
                            for (TaxForm01StationFileModel file : station.getHouseFileList()) {
                                if (station.getRefStationId() != 0) {
                                    i = 1;

                                    sql = "INSERT INTO RETAIL_STATION_FILE("
                                            + " RETAIL_STATION_ID "
                                            + ",FILE_NAME"
                                            + ",PATH_FILE"
                                            + ",FILE_CATE"
                                            + ",FILE_TYPE"
                                            + ") VALUES(?,?,?,?,?)";

                                    ps = conn.prepareStatement(sql);

                                    ps.setInt(i++, station.getRefStationId());
                                    ps.setString(i++, file.getFileName());
                                    ps.setString(i++, file.getPathFile());
                                    ps.setString(i++, file.getFileCate());
                                    ps.setString(i++, file.getFileType());

                                    result = ps.executeUpdate() != 0;
                                }
                            }
                        }
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="INSERT Map">
                        if (station.getMapFileList() != null && !station.getMapFileList().isEmpty()) {
                            for (TaxForm01StationFileModel file : station.getMapFileList()) {
                                if (station.getRefStationId() != 0) {
                                    i = 1;

                                    sql = "INSERT INTO RETAIL_STATION_FILE("
                                            + " RETAIL_STATION_ID "
                                            + ",FILE_NAME"
                                            + ",PATH_FILE"
                                            + ",FILE_CATE"
                                            + ",FILE_TYPE"
                                            + ") VALUES(?,?,?,?,?)";

                                    ps = conn.prepareStatement(sql);

                                    ps.setInt(i++, station.getRefStationId());
                                    ps.setString(i++, file.getFileName());
                                    ps.setString(i++, file.getPathFile());
                                    ps.setString(i++, file.getFileCate());
                                    ps.setString(i++, file.getFileType());

                                    result = ps.executeUpdate() != 0;
                                }
                            }
                        }
                        //</editor-fold>
                    }
                    //</editor-fold>
//                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="INSERT TAX_FORM01_STATION">
                    i = 1;

                    //<editor-fold defaultstate="collapsed" desc="sql station">
                    if (station.getTaxForm01StationId() == 0) {

                        sql = "INSERT INTO TAX_FORM01_STATION ( "
                                + "  TAX_FORM01_RETAIL_ID "
                                + ", REF_STATION_ID "
                                + ", STATION_CODE "
                                + ", STATION_NAME "
                                + ", BRANCH_NAME "
                                
                                + ", STATUS "
                                + ", HOUSE_NO "
                                + ", ADDRESS "
                                + ", MOO "
                                + ", SOI "
                                + ", ROAD "
                                
                                + ", TAMBON_ID "
                                + ", TAMBON_NAME "
                                + ", AMPHUR_ID "
                                + ", AMPHUR_NAME "
                                + ", PROVINCE_ID "
                                
                                + ", PROVINCE_NAME "
                                + ", POSTCODE "
                                + ", MOBILE "
                                + ", EMAIL "
                                + ", REF_OFFICE "
                                
                                + ", REF_OFFICE_NAME "
                                + ", REF_OFFICE_CODE "
                                + ", HOUSE_FILE_NUM "
                                + ", MAP_FILE_NUM "
                                + ", IS_ACTIVE"
                                
                                + ", CREATED_DATE "
                                + ", CREATED_BY"
                                + ") VALUES(?,?,?,?,?  ,?,?,?,?,? ,?   ,?,?,?,?,?,  ?,?,?,?,?   ,?,?,?,?,?,  SYSDATE,?) ";

                        ps = conn.prepareStatement(sql, new String[]{"TAX_FORM01_STATION_ID"});

                    } else {

                        sql = "UPDATE TAX_FORM01_STATION SET "
                                + "  TAX_FORM01_RETAIL_ID = ? "
                                + ", REF_STATION_ID = ? "
                                + ", STATION_CODE = ? "
                                + ", STATION_NAME = ? "
                                + ", BRANCH_NAME = ? "
                                + ", STATUS = ? "
                                + ", HOUSE_NO = ? "
                                + ", ADDRESS = ? "
                                + ", MOO = ? "
                                + ", SOI = ? "
                                + ", ROAD = ? "
                                + ", TAMBON_ID = ? "
                                + ", TAMBON_NAME = ? "
                                + ", AMPHUR_ID = ? "
                                + ", AMPHUR_NAME = ? "
                                + ", PROVINCE_ID = ? "
                                + ", PROVINCE_NAME = ? "
                                + ", POSTCODE = ? "
                                + ", MOBILE = ? "
                                + ", EMAIL= ? "
                                + ", REF_OFFICE= ? "
                                + ", REF_OFFICE_NAME = ? "
                                + ", REF_OFFICE_CODE= ?  "
                                + ", HOUSE_FILE_NUM = ? "
                                + ", MAP_FILE_NUM = ? "
                                + ", IS_ACTIVE = ? "
                                + ", UPDATED_DATE = SYSDATE "
                                + ", UPDATED_BY = ? "
                                + " WHERE TAX_FORM01_STATION_ID = ? ";

                        ps = conn.prepareStatement(sql);

                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="set parameter">
                    ps.setInt(i++, data.getTaxForm01RetailId());
                    ps.setInt(i++, station.getRefStationId());
                    ps.setString(i++, station.getStationCode());
                    ps.setString(i++, station.getStationName());
                    ps.setString(i++, station.getBranchName());
                    if (station.getTaxForm01StationId() == 0) {
                        ps.setString(i++, String.valueOf(StatusEnum.Create.value()));
                    } else {
                        ps.setString(i++, String.valueOf(station.getStatus()));
                    }

                    ps.setString(i++, station.getHouseNo());
                    ps.setString(i++, station.getAddress());
                    ps.setString(i++, station.getMoo());
                    ps.setString(i++, station.getSoi());
                    ps.setString(i++, station.getRoad());
                    ps.setString(i++, station.getTambonId());
                    ps.setString(i++, station.getTambonName());
                    ps.setString(i++, station.getAmphurId());
                    ps.setString(i++, station.getAmphurName());
                    ps.setString(i++, station.getProvinceId());
                    ps.setString(i++, station.getProvinceName());
                    ps.setString(i++, station.getPostcode());

                    ps.setString(i++, station.getMobile());
                    ps.setString(i++, station.getEmail());

                    ps.setString(i++, officeChildData.getOfficeId());
                    ps.setString(i++, officeChildData.getOfficeName());
                    ps.setString(i++, officeChildData.getOfficeCode());

                    ps.setInt(i++, station.getHouseFileNum());
                    ps.setInt(i++, station.getMapFileNum());

//                    ps.setBoolean(i++, station.getActive());
                    ps.setBoolean(i++, true);

                    if (!AppUtil.isNullAndSpace(station.getUpdatedBy())) {
                        ps.setString(i++, station.getUpdatedBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }

                    if (station.getTaxForm01StationId() == 0) {
                        result = ps.executeUpdate() != 0;

                        ResultSet rs = ps.getGeneratedKeys();
                        int id = 0;
                        while (rs.next()) {
                            id = rs.getInt(1);
                        }

                        if (id <= 0) {
                            throw new Exception();
                        }

                        station.setTaxForm01StationId(id);
                    } else {

                        ps.setInt(i++, station.getTaxForm01StationId());

                        result = ps.executeUpdate() != 0;
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="INSERT BUSINESS_TYPE">
                    if (station.getTaxForm01StationId() != 0) {

                        //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                        i = 1;

                        sql = "DELETE FROM TAX_FORM01_STATION_BUSINESS_TYPE WHERE TAX_FORM01_STATION_ID = ?";

                        ps = conn.prepareStatement(sql);

                        ps.setInt(i++, station.getTaxForm01StationId());

                        ps.executeUpdate();
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="INSERT RETAIL_STATION_BUSINESS_TYPE">
                        if (station.getBusinessTypeList() != null && !station.getBusinessTypeList().isEmpty()) {
                            for (DrpDownModel child : station.getBusinessTypeList()) {
                                if (station.getTaxForm01StationId() != 0) {
                                    i = 1;

                                    sql = "INSERT INTO TAX_FORM01_STATION_BUSINESS_TYPE("
                                            + " TAX_FORM01_STATION_ID "
                                            + ",BUSINESS_TYPE_ID"
                                            + ",BUSINESS_TYPE_NAME "
                                            + ") VALUES(?,?,?)";

                                    ps = conn.prepareStatement(sql);

                                    ps.setInt(i++, station.getTaxForm01StationId());
                                    ps.setInt(i++, child.getId());
                                    ps.setString(i++, child.getName());

                                    result = ps.executeUpdate() != 0;
                                }
                            }
                        }
                        //</editor-fold>
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="INSERT ATTACH FILE">
                    if (station.getTaxForm01StationId() != 0) {

                        //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                        i = 1;

                        sql = "DELETE FROM TAX_FORM01_STATION_FILE WHERE TAX_FORM01_STATION_ID = ?";

                        ps = conn.prepareStatement(sql);

                        ps.setInt(i++, station.getTaxForm01StationId());

                        ps.executeUpdate();
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="INSERT House">
                        if (station.getHouseFileList() != null && !station.getHouseFileList().isEmpty()) {
                            for (TaxForm01StationFileModel file : station.getHouseFileList()) {
                                if (station.getTaxForm01StationId() != 0) {
                                    i = 1;

                                    sql = "INSERT INTO TAX_FORM01_STATION_FILE("
                                            + " TAX_FORM01_STATION_ID "
                                            + ",FILE_NAME"
                                            + ",PATH_FILE"
                                            + ",FILE_CATE"
                                            + ",FILE_TYPE"
                                            + ") VALUES(?,?,?,?,?)";

                                    ps = conn.prepareStatement(sql);

                                    ps.setInt(i++, station.getTaxForm01StationId());
                                    ps.setString(i++, file.getFileName());
                                    ps.setString(i++, file.getPathFile());
                                    ps.setString(i++, file.getFileCate());
                                    ps.setString(i++, file.getFileType());
                                    result = ps.executeUpdate() != 0;
                                }
                            }
                        }
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="INSERT Map">
                        if (station.getMapFileList() != null && !station.getMapFileList().isEmpty()) {
                            for (TaxForm01StationFileModel file : station.getMapFileList()) {
                                if (station.getTaxForm01StationId() != 0) {
                                    i = 1;

                                    sql = "INSERT INTO TAX_FORM01_STATION_FILE("
                                            + " TAX_FORM01_STATION_ID "
                                            + ",FILE_NAME"
                                            + ",PATH_FILE"
                                            + ",FILE_CATE"
                                            + ",FILE_TYPE"
                                            + ") VALUES(?,?,?,?,?)";

                                    ps = conn.prepareStatement(sql);

                                    ps.setInt(i++, station.getTaxForm01StationId());
                                    ps.setString(i++, file.getFileName());
                                    ps.setString(i++, file.getPathFile());
                                    ps.setString(i++, file.getFileCate());
                                    ps.setString(i++, file.getFileType());

                                    result = ps.executeUpdate() != 0;
                                }
                            }
                        }
                        //</editor-fold>
                    }
                    //</editor-fold>
                    //</editor-fold>
                }

                //<editor-fold defaultstate="collapsed" desc="DELETE child">                
                i = 1;

                sql = "DELETE FROM RETAIL_STATION WHERE TAX_FORM01_RETAIL_ID = ? AND  IS_ACTIVE =0  ";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm01RetailId());

                ps.executeUpdate();

                i = 1;

                sql = "DELETE FROM TAX_FORM01_STATION WHERE TAX_FORM01_RETAIL_ID = ? AND  IS_ACTIVE =0  ";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm01RetailId());

                ps.executeUpdate();
                //</editor-fold>
            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public Integer _getCountStation(int id, Connection conn) throws SQLException {

        Integer countStation = 0;
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_STATION FROM RETAIL_STATION  "
                    + " WHERE RETAIL_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                countStation = rs.getInt("COUNT_STATION");
            }

        } catch (Exception e) {

        }
        return countStation;

    }

    public ResultData deleteData(List<Integer> idList, FilterTaxFormModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id list">
            for (int id : idList) {

                String childList = "";
                String refList = "";

                //<editor-fold defaultstate="collapsed" desc="get list station id ">
                sql = " SELECT distinct LISTAGG(TAX_FORM01_STATION_ID, ',') WITHIN GROUP (ORDER BY tax_form01_retail_id)\n"
                        + "         OVER (PARTITION BY tax_form01_retail_id) as \"CHILD_LIST\"\n         "
                        + ",   LISTAGG(REF_STATION_ID, ',') WITHIN GROUP (ORDER BY tax_form01_retail_id)\n"
                        + "         OVER (PARTITION BY tax_form01_retail_id) as \"REF_LIST\""
                        + "  FROM TAX_FORM01_STATION where tax_form01_retail_id = ? ";

                ps.setSql(sql);

                ps.setInt(i, id);

                rs = ps.executeQuery();

                while (rs.next()) {
                    childList = rs.getString("CHILD_LIST");
                    refList = rs.getString("REF_LIST");
                }
                //</editor-fold>

                if (!"".equals(childList)) {
                    //<editor-fold defaultstate="collapsed" desc="TAX_FORM01_STATION_LOG">
                    sql = " DELETE FROM TAX_FORM01_STATUS_LOG "
                            + " WHERE TAX_FORM01_STATION_ID IN ( " + childList + " )";
                    i = 1;
                    ps.setSql(sql);

//                ps.setString(i, childList);
                    result = ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="TAX_FORM01_STATION_FILE">
                    sql = " DELETE FROM TAX_FORM01_STATION_FILE "
                            + " WHERE TAX_FORM01_STATION_ID IN ( " + childList + " )";
                    i = 1;
                    ps.setSql(sql);

//                ps.setString(i, childList);
                    result = ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="TAX_FORM01_STATION_BUSINESS_TYPE">
                    sql = " DELETE FROM TAX_FORM01_STATION_BUSINESS_TYPE "
                            + " WHERE TAX_FORM01_STATION_ID IN ( " + childList + " )";
                    i = 1;
                    ps.setSql(sql);

//                ps.setString(i, childList);
                    result = ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="TAX_FORM01_STATION">
                    sql = " DELETE FROM TAX_FORM01_STATION "
                            + " WHERE TAX_FORM01_STATION_ID IN ( " + childList + " )";
                    i = 1;
                    ps.setSql(sql);

//                ps.setString(i, childList);
                    result = ps.executeUpdate();
                    //</editor-fold>
                }

                if (!"".equals(refList)) {
                    //<editor-fold defaultstate="collapsed" desc="RETAIL_STATION">
                    sql = " DELETE FROM RETAIL_STATION "
                            + " WHERE RETAIL_STATION_ID IN ( " + refList + " )";
                    i = 1;
                    ps.setSql(sql);

//                ps.setString(i, refList);
                    result = ps.executeUpdate();
                    //</editor-fold>
                }

                int retailId = 0;
                String taxNo = "";

                //<editor-fold defaultstate="collapsed" desc="get retail id ">
                sql = " SELECT REF_RETAIL_ID ,TAX_NO "
                        + "  FROM TAX_FORM01_RETAIL where tax_form01_retail_id = ? ";

                ps.setSql(sql);

                ps.setInt(i, id);

                rs = ps.executeQuery();

                while (rs.next()) {
                    retailId = rs.getInt("REF_RETAIL_ID");
                    taxNo = rs.getString("TAX_NO");
                }
                //</editor-fold>

                int countRetail = 0;

                //<editor-fold defaultstate="collapsed" desc="count retail ">
                sql = " SELECT COUNT(REF_RETAIL_ID ) NUM_RETAIL "
                        + "  FROM TAX_FORM01_RETAIL where REF_RETAIL_ID = ? ";

                ps.setSql(sql);

                ps.setInt(i, retailId);

                rs = ps.executeQuery();

                while (rs.next()) {
                    countRetail = rs.getInt("NUM_RETAIL");
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM01_RETAIL_FILE">
                sql = " DELETE FROM TAX_FORM01_RETAIL_FILE "
                        + " WHERE TAX_FORM01_RETAIL_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM01_RETAIL">
                sql = " DELETE FROM TAX_FORM01_RETAIL "
                        + " WHERE TAX_FORM01_RETAIL_ID  = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="delete RETAIL">
                if (countRetail == 1) {
                    sql = " DELETE FROM RETAIL  "
                            + " WHERE RETAIL_ID = ? ";
                    i = 1;
                    ps.setSql(sql);

                    ps.setInt(i, retailId);

                    result = ps.executeUpdate();

                    sql = " DELETE FROM MEMBER_OF_RETAIL  "
                            + " WHERE TAX_NO = ? ";
                    i = 1;
                    ps.setSql(sql);

                    ps.setString(i, taxNo);

                    result = ps.executeUpdate();
                }
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    public AddressModel getAddressByTambonId(String tambonId) throws SQLException {
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        AddressModel data = new AddressModel();

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get m_tambon ">
            String sql = " SELECT * \n"
                    + "FROM M_TAMBON  \n"
                    + "WHERE TAMBON_ID = ? ";

            ps.setSql(sql);

            ps.setString(1, tambonId);

            rs = ps.executeQuery();

            while (rs.next()) {
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_THAI")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("DISTRICT_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_THAI")));
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }
}
