/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import enumeration.StatusEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import model.DocFormFileModel;
import model.DocFormModel;
import model.DocFormStationFileModel;
import model.DocFormStationModel;
import model.DrpDownModel;
import model.TaxForm01RetailFileModel;
import model.TaxForm01RetailModel;
import model.TaxForm01StationFileModel;
import model.TaxForm01StationModel;
import repository.mapper.DocFormMapper;
import repository.mapper.TaxForm01Mapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.DocFormFileViewModel;
import viewModel.DocFormStationFileViewModel;
import viewModel.DocFormStationViewModel;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01StationFileViewModel;
import viewModel.TaxForm01StationViewModel;
import viewModel.DocFormViewModel;
import viewModel.FilterDocFormModel;

/**
 *
 * @author User
 */
public class DocFormRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    //<editor-fold defaultstate="collapsed" desc="for retail">
    public ResultData<List<DocFormViewModel>> getListForRetail(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<DocFormViewModel>> resultData = new ResultData<List<DocFormViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT DISTINCT RETAIL_ID,TAX_NO,OWNER_NAME,REF_OFFICE_NAME,CREATED_DATE FROM RETAIL "
                    + "  WHERE IS_ACTIVE=1 AND MAIN_STATUS != 0 ";//MAIN_STAUS != 0 คือไม่เอาเคสที่พึ่งสร้าง

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND (OWNER_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (TAX_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                sql += " AND (AMPHUR_ID = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("createdDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, "%" + filter.getTaxNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                ps.setString(i++, filter.getAmphurId());
            }

            rs = ps.executeQuery();

            List<DocFormViewModel> dataList = null;
            dataList = new DocFormMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<DocFormFileViewModel>> getListForRetailFile(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<DocFormFileViewModel>> resultData = new ResultData<List<DocFormFileViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "select * from( "
                    + "select 0 retail_add_file_id,r.ref_retail_id as retail_id,r.created_date,r.doc_no,'เอกสารลงทะเบียน' as doc_type,  "
                    + "f.file_name,f.path_file,f.file_cate,f.file_type, null  file_name_other "
                    + "from tax_form01_retail_file f "
                    + "left join tax_form01_retail r on f.tax_form01_retail_id = r.tax_form01_retail_id "
                    + " "
                    + "union all "
                    + " "
                    + "select af.retail_add_file_id, af.retail_id, af.created_date,null doc_no , 'แนบเพิ่มเติม' as doc_type, "
                    + "af.file_name,af.path_file,af.file_cate ,af.file_type ,af.file_name_other  "
                    + "from retail_add_file af "
                    + ") where retail_id = ? ";
           

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (DOC_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(CREATED_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(CREATED_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getFileCate())) {
                sql += " AND (FILE_CATE = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("createdDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            ps.setInt(i++, AppUtil.decryptId(filter.getId()));

            
            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, "%" + filter.getDocNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getFileCate())) {
                ps.setString(i++, filter.getFileCate());
            }

            rs = ps.executeQuery();

            List<DocFormFileViewModel> dataList = null;
            dataList = new DocFormMapper(rs).mapListRetailFile();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData saveDataRetailAddFile(DocFormFileViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="INSERT retail add file">
            i = 1;

            sql = "INSERT INTO RETAIL_ADD_FILE("
                    + " RETAIL_ID "
                    + ",FILE_NAME"
                    + ",PATH_FILE"
                    + ",FILE_CATE"
                    + ",FILE_TYPE"
                    + ",CREATED_DATE"
                    + ",CREATED_BY"
                    + ") VALUES(?,?,?,?,?,SYSDATE,?)";

            ps = conn.prepareStatement(sql, new String[]{"RETAIL_ADD_FILE_ID"});

            ps.setInt(i++, AppUtil.decryptId(data.getRetailId()));
            ps.setString(i++, data.getFileName());
            ps.setString(i++, data.getPathFile());
            ps.setString(i++, data.getFileCate());
            ps.setString(i++, data.getFileType());
            ps.setString(i++, data.getUpdatedBy());

            result = ps.executeUpdate() != 0;
            //</editor-fold>
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean deleteDataRetailAddFile(List<Integer> idList, FilterTaxFormModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id list">
            sql = " DELETE FROM RETAIL_ADD_FILE "
                    + " WHERE RETAIL_ADD_FILE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete OilType by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="for station">
    public ResultData<List<DocFormViewModel>> getListForStation(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<DocFormViewModel>> resultData = new ResultData<List<DocFormViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM (SELECT DISTINCT s.RETAIL_STATION_ID ,RETAIL.OWNER_NAME,s.STATION_NAME,s.REF_OFFICE_NAME,s.CREATED_DATE "
                    + " FROM RETAIL_STATION s  LEFT JOIN RETAIL ON s.RETAIL_ID = RETAIL.RETAIL_ID "
                    + " WHERE s.IS_ACTIVE=1 AND s.STATUS != 0) WHERE (0=0) ";//STAUS != 0 คือไม่เอาเคสที่พึ่งสร้าง

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                sql += " AND (OWNER_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (STATION_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                sql += " AND (REF_OFFICE = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("createdDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getName())) {
                ps.setString(i++, "%" + filter.getName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                ps.setString(i++, filter.getAmphurId());
            }

            rs = ps.executeQuery();

            List<DocFormViewModel> dataList = null;
            dataList = new DocFormMapper(rs).mapStationList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<DocFormFileViewModel>> getListForStationFile(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<DocFormFileViewModel>> resultData = new ResultData<List<DocFormFileViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "select * from( "
                    + "select * from( "
                    + "select 0 retail_station_add_file_id,s.ref_station_id as retail_station_id,s.created_date,r.doc_no,'เอกสารลงทะเบียน' as doc_type,  "
                    + "f.file_name,f.path_file,f.file_cate,f.file_type, null  file_name_other "
                    + "from tax_form01_station_file f "
                    + "left join tax_form01_station s on f.tax_form01_station_id = s.tax_form01_station_id "
                    + "left join tax_form01_retail r on s.tax_form01_retail_id = r.tax_form01_retail_id "
                    + " "
                    + "union all "
                    + ""
                    + "select 0 retail_station_add_file_id,r.ref_station_id as retail_station_id,r.created_date, r.doc_no"
                    + ",case   when cancel_Date is null then 'เปลี่ยนแปลงข้อมูล' else 'ยกเลิกกิจการ' end as doc_type,  "
                    + "f.file_name,f.path_file,f.file_cate,f.file_type, null  file_name_other "
                    + "from tax_form02_file f "
                    + "left join tax_form02 r on f.tax_form02_id = r.tax_form02_id "
                    + " "
                    + "union all "
                    + " "
                    + "select af.retail_station_add_file_id, af.retail_station_id, af.created_date,null doc_no , 'แนบเพิ่มเติม' as doc_type, "
                    + "af.file_name,af.path_file,af.file_cate ,af.file_type ,af.file_name_other  "
                    + "from retail_station_add_file af "
                    + ") "
                    + ") where retail_station_id = ? ";

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (DOC_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(CREATED_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(CREATED_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getFileCate())) {
                sql += " AND (FILE_CATE = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("createdDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            ps.setInt(i++, AppUtil.decryptId(filter.getId()));

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, "%" + filter.getDocNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getFileCate())) {
                ps.setString(i++, filter.getFileCate());
            }

            rs = ps.executeQuery();

            List<DocFormFileViewModel> dataList = null;
            dataList = new DocFormMapper(rs).mapListStationFile();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData saveDataStationAddFile(DocFormFileViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="INSERT retail station add file">
            i = 1;

            sql = "INSERT INTO RETAIL_STATION_ADD_FILE("
                    + " RETAIL_STATION_ID "
                    + ",FILE_NAME"
                    + ",PATH_FILE"
                    + ",FILE_CATE"
                    + ",FILE_TYPE"
                    + ",CREATED_DATE"
                    + ",CREATED_BY"
                    + ") VALUES(?,?,?,?,?,SYSDATE,?)";

            ps = conn.prepareStatement(sql, new String[]{"RETAIL_STATION_ADD_FILE_ID"});

            ps.setInt(i++, AppUtil.decryptId(data.getRetailStationId()));
            ps.setString(i++, data.getFileName());
            ps.setString(i++, data.getPathFile());
            ps.setString(i++, data.getFileCate());
            ps.setString(i++, data.getFileType());
            ps.setString(i++, data.getUpdatedBy());

            result = ps.executeUpdate() != 0;
            //</editor-fold>
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean deleteDataStationAddFile(List<Integer> idList, FilterTaxFormModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id list">
            sql = " DELETE FROM RETAIL_STATION_ADD_FILE "
                    + " WHERE RETAIL_STATION_ADD_FILE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    //</editor-fold>    
    private String getColumnDB(String orderBy) {

        String str = " TAX_FORM01_RETAIL_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("ownerName")) {
                str = " OWNER_NAME ";
            } else if (orderBy.equalsIgnoreCase("taxNo")) {
                str = " TAX_NO ";
            } else if (orderBy.equalsIgnoreCase("createdDate")) {
                str = " CREATED_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " CREATED_DATE ";
            }
        }

        return str;

    }

}
