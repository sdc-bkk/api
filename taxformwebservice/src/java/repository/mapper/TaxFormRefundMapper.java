/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.StatusRefundEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.TaxFormRefundFileModel;
import model.TaxFormRefundModel;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.TaxFormRefundViewModel;
import viewModel.TaxFormRefundFileViewModel;

/**
 *
 * @author User
 */
public class TaxFormRefundMapper {

    private ResultSet rs = null;

    public TaxFormRefundMapper() {

    }

    public TaxFormRefundMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<TaxFormRefundViewModel> mapList() {

        List<TaxFormRefundViewModel> dataList = new ArrayList<TaxFormRefundViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExport = appConfig.value("export_form_refund");
        String linkExportForm = appConfig.value("export_form_refund_request");

        try {

            while (rs.next()) {
                TaxFormRefundViewModel data = new TaxFormRefundViewModel();
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setAmount(rs.getDouble("AMOUNT"));
                data.setAmountStr(AppUtil.numberFormatComma(String.valueOf(rs.getDouble("AMOUNT"))));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                data.setRefStationId(AppUtil.encryptId(AppUtil.checkNullData(rs.getInt("ref_station_id"))));
                data.setTaxFormRefundId(AppUtil.encryptId(AppUtil.checkNullData(rs.getInt("tax_form_refund_id"))));
                //link export                
                data.setLinkExportTaxformRefund(linkExport + data.getTaxFormRefundId());
                data.setLinkExportTaxformRefundForm(linkExportForm + data.getTaxFormRefundId());
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

    public TaxFormRefundViewModel mapData() {

        TaxFormRefundViewModel data = new TaxFormRefundViewModel();

        try {

            while (rs.next()) {

                data.setTaxFormRefundId(AppUtil.encryptId(rs.getInt("TAX_FORM_REFUND_ID")));
                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));

                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setAmount(rs.getDouble("AMOUNT"));

                data.setYearly(AppUtil.checkNullData(rs.getInt("YEARLY")));
                data.setMonthly(AppUtil.checkNullData(rs.getInt("MONTHLY")));
                data.setMonthName(AppUtil.checkNullData(rs.getString("MONTH_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusRefundEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }
                data.setAcceptBy(AppUtil.checkNullData(rs.getString("ACCEPT_BY")));
                data.setAcceptDate(AppUtil.checkNullData(rs.getDate("ACCEPT_DATE")));
                data.setRefundBy(AppUtil.checkNullData(rs.getString("REFUND_BY")));
                data.setRefundDate(AppUtil.checkNullData(rs.getDate("REFUND_DATE")));
                data.setRejectBy(AppUtil.checkNullData(rs.getString("REJECT_BY")));
                data.setRejectDate(AppUtil.checkNullData(rs.getDate("REJECT_DATE")));
                data.setRejectRemark(AppUtil.checkNullData(rs.getString("REJECT_REMARK")));
                data.setDocNo1(AppUtil.checkNullData(rs.getString("DOC_NO1")));
                data.setDocNo2(AppUtil.checkNullData(rs.getString("DOC_NO2")));
                data.setPlace(AppUtil.checkNullData(rs.getString("PLACE")));
                data.setDocRefundDate(AppUtil.checkNullData(rs.getDate("DOC_REFUND_DATE")));
                data.setRefOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setRefOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                data.setRefOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setReceiptNo(AppUtil.checkNullData(rs.getString("RECEIPT_NO")));
                data.setReceiptDate(AppUtil.checkNullData(rs.getDate("RECEIPT_DATE")));
                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<TaxFormRefundFileViewModel> mapDataFile() {

        List<TaxFormRefundFileViewModel> dataList = new ArrayList<TaxFormRefundFileViewModel>();

        try {

            while (rs.next()) {
                TaxFormRefundFileViewModel data = new TaxFormRefundFileViewModel();
                data.setTaxFormRefundId(AppUtil.encryptId(rs.getInt("TAX_FORM_REFUND_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_NAME")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxFormRefundModel mapSaveData(TaxFormRefundViewModel dataView) {

        TaxFormRefundModel data = new TaxFormRefundModel();

        try {
            //<editor-fold defaultstate="collapsed" desc="data taxform">
            data.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));
            data.setRefRetailId(AppUtil.decryptId(dataView.getRefRetailId()));
            data.setRefStationId(AppUtil.decryptId(dataView.getRefStationId()));

            data.setDocNo(AppUtil.checkNullData(dataView.getDocNo()));
            data.setDocDate(AppUtil.checkNullData(dataView.getDocDate()));

            data.setYearly(dataView.getYearly());
            data.setMonthly(dataView.getMonthly());
            data.setMonthName(AppUtil.convertMonthToThMonth(dataView.getMonthly()));
            data.setAmount(AppUtil.checkNullData(dataView.getAmount()));
            data.setReceiptNo(AppUtil.checkNullData(dataView.getReceiptNo()));
            data.setStatus(AppUtil.checkNullData(String.valueOf(StatusRefundEnum.Wait.value())));
            data.setStatusName(AppUtil.checkNullData(StatusRefundEnum.Wait.displayNameTH()));

            data.setAcceptBy(dataView.getAcceptBy());
            data.setAcceptDate(AppUtil.checkNullData(dataView.getAcceptDate()));
            data.setRefundBy(dataView.getRefundBy());
            data.setRefundDate(AppUtil.checkNullData(dataView.getRefundDate()));
            data.setRejectBy(dataView.getRejectBy());
            data.setRejectDate(AppUtil.checkNullData(dataView.getRejectDate()));
            data.setDocNo1(AppUtil.checkNullData(dataView.getDocNo1()));
            data.setDocNo2(AppUtil.checkNullData(dataView.getDocNo2()));
            data.setPlace(AppUtil.checkNullData(dataView.getPlace()));
            data.setRefOffice(AppUtil.checkNullData(dataView.getRefOffice()));
            data.setRefOfficeName(AppUtil.checkNullData(dataView.getRefOfficeName()));
            data.setDocNo(AppUtil.checkNullData(dataView.getDocNo()));
            data.setReceiptDate(AppUtil.checkNullData(dataView.getReceiptDate()));
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="data BillPaymentFileList">
            if (dataView.getBillPaymentFileList() != null && !dataView.getBillPaymentFileList().isEmpty()) {
                List<TaxFormRefundFileModel> bills = new ArrayList<>();
                for (TaxFormRefundFileViewModel file : dataView.getBillPaymentFileList()) {
                    TaxFormRefundFileModel dataFile = new TaxFormRefundFileModel();
                    dataFile.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.BillPayment.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    bills.add(dataFile);
                }
                data.setBillPaymentFileList(bills);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="data AuthorizeFileList">
            if (dataView.getAuthorizeFileList() != null && !dataView.getAuthorizeFileList().isEmpty()) {
                List<TaxFormRefundFileModel> auths = new ArrayList<>();
                for (TaxFormRefundFileViewModel file : dataView.getAuthorizeFileList()) {
                    TaxFormRefundFileModel dataFile = new TaxFormRefundFileModel();
                    dataFile.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.Authorize.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    auths.add(dataFile);
                }
                data.setAuthorizeFileList(auths);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="data IdCardFileList">
            if (dataView.getIdCardFileList() != null && !dataView.getIdCardFileList().isEmpty()) {
                List<TaxFormRefundFileModel> ids = new ArrayList<>();
                for (TaxFormRefundFileViewModel file : dataView.getIdCardFileList()) {
                    TaxFormRefundFileModel dataFile = new TaxFormRefundFileModel();
                    dataFile.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.IDCard.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    ids.add(dataFile);
                }
                data.setIdCardFileList(ids);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="data BookBankFileList">
            if (dataView.getBookBankFileList() != null && !dataView.getBookBankFileList().isEmpty()) {
                List<TaxFormRefundFileModel> banks = new ArrayList<>();
                for (TaxFormRefundFileViewModel file : dataView.getBookBankFileList()) {
                    TaxFormRefundFileModel dataFile = new TaxFormRefundFileModel();
                    dataFile.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.BookBank.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    banks.add(dataFile);
                }
                data.setBookBankFileList(banks);
            }
            //</editor-fold>

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public TaxFormRefundModel mapAcceptData(TaxFormRefundViewModel dataView) {

        TaxFormRefundModel data = new TaxFormRefundModel();

        try {
            //<editor-fold defaultstate="collapsed" desc="data taxform">
            data.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));

            data.setStatus(AppUtil.checkNullData(String.valueOf(StatusRefundEnum.Accept.value())));
            data.setStatusName(AppUtil.checkNullData(StatusRefundEnum.Accept.displayNameTH()));

            data.setAcceptBy(dataView.getAcceptBy());
            data.setAcceptDate(AppUtil.checkNullData(dataView.getAcceptDate()));

//            data.setRefundBy(dataView.getRefundBy());
//            data.setRefundDate(AppUtil.checkNullData(dataView.getRefundDate()));
//
//            data.setRejectBy(dataView.getRejectBy());
//            data.setRejectDate(AppUtil.checkNullData(dataView.getRejectDate()));
//
//            data.setDocNo1(AppUtil.checkNullData(dataView.getDocNo1()));
//            data.setDocNo2(AppUtil.checkNullData(dataView.getDocNo2()));
//            data.setPlace(AppUtil.checkNullData(dataView.getPlace()));

            //</editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public TaxFormRefundModel mapApproveData(TaxFormRefundViewModel dataView) {

        TaxFormRefundModel data = new TaxFormRefundModel();

        try {
            //<editor-fold defaultstate="collapsed" desc="data taxform">
            data.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));

            data.setStatus(AppUtil.checkNullData(String.valueOf(StatusRefundEnum.Pass.value())));
            data.setStatusName(AppUtil.checkNullData(StatusRefundEnum.Pass.displayNameTH()));

            data.setDocNo1(AppUtil.checkNullData(dataView.getDocNo1()));
            data.setDocNo2(AppUtil.checkNullData(dataView.getDocNo2()));
            data.setPlace(AppUtil.checkNullData(dataView.getPlace()));
            data.setDocRefundDate(AppUtil.checkNullData(dataView.getDocRefundDate()));
            data.setRefundBy(dataView.getRefundBy());

            //</editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public TaxFormRefundModel mapRejectData(TaxFormRefundViewModel dataView) {

        TaxFormRefundModel data = new TaxFormRefundModel();

        try {
            //<editor-fold defaultstate="collapsed" desc="data taxform">
            data.setTaxFormRefundId(AppUtil.decryptId(dataView.getTaxFormRefundId()));

            data.setStatus(AppUtil.checkNullData(String.valueOf(StatusRefundEnum.NoPass.value())));
            data.setStatusName(AppUtil.checkNullData(StatusRefundEnum.NoPass.displayNameTH()));

            data.setRejectBy(dataView.getRejectBy());
            data.setRejectRemark(dataView.getRejectRemark());

            //</editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
