/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import model.DocFormFileModel;
import model.DocFormModel;
import model.DocFormStationFileModel;
import model.DocFormStationModel;
import model.DrpDownModel;
import model.TaxForm01RetailFileModel;
import model.TaxForm01RetailModel;
import model.TaxForm01StationFileModel;
import model.TaxForm01StationModel;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.DocFormFileViewModel;
import viewModel.DocFormStationFileViewModel;
import viewModel.DocFormStationViewModel;
import viewModel.DrpDownViewModel;
import viewModel.DocFormViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationFileViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 *
 * @author DELL
 */
public class DocFormMapper {

    private ResultSet rs = null;

    public DocFormMapper() {

    }

    public DocFormMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<DocFormViewModel> mapList() {

        List<DocFormViewModel> dataList = new ArrayList<DocFormViewModel>();

        try {

            while (rs.next()) {
                DocFormViewModel data = new DocFormViewModel();

                data.setRetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setRefOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }


    public List<DocFormFileViewModel> mapListRetailFile() {

        List<DocFormFileViewModel> dataList = new ArrayList<>();

        try {

            while (rs.next()) {
                DocFormFileViewModel data = new DocFormFileViewModel();
                if (rs.getInt("RETAIL_ADD_FILE_ID") != 0) {
                    data.setRetailAddFileId(AppUtil.encryptId(rs.getInt("RETAIL_ADD_FILE_ID")));
                } else {
                    data.setRetailAddFileId("");
                }
                data.setRetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setDocName(AttachFileCateEnum.getDocName(rs.getString("FILE_CATE")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setRemark(rs.getString("DOC_TYPE") + ' ' + AppUtil.checkNullData(rs.getString("FILE_NAME_OTHER")));

                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    
    public List<DocFormViewModel> mapStationList() {
       
        List<DocFormViewModel> dataList = new ArrayList<DocFormViewModel>();

        try {

            while (rs.next()) {
                DocFormViewModel data = new DocFormViewModel();

                data.setRetailStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setRefOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    

    public List<DocFormFileViewModel> mapListStationFile() {

        List<DocFormFileViewModel> dataList = new ArrayList<>();

        try {

            while (rs.next()) {
                DocFormFileViewModel data = new DocFormFileViewModel();
                if (rs.getInt("RETAIL_STATION_ADD_FILE_ID") != 0) {
                    data.setRetailStationAddFileId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ADD_FILE_ID")));
                } else {
                    data.setRetailAddFileId("");
                }
                data.setRetailStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));
                data.setDocName(AttachFileCateEnum.getDocName(rs.getString("FILE_CATE")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setRemark(rs.getString("DOC_TYPE") + ' ' + AppUtil.checkNullData(rs.getString("FILE_NAME_OTHER")));

                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public List<DocFormFileViewModel> mapDataRetailFile() {

        List<DocFormFileViewModel> dataList = new ArrayList<>();

        try {

            while (rs.next()) {
                DocFormFileViewModel data = new DocFormFileViewModel();
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));
                if (rs.getDate("UPDATED_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                } else {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                }
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setRemark("เอกสารลงทะเบียน");

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<DocFormStationFileViewModel> mapDataRetailStationFile() {

        List<DocFormStationFileViewModel> dataList = new ArrayList<>();

        try {

            while (rs.next()) {
                DocFormStationFileViewModel data = new DocFormStationFileViewModel();
                //data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));
                if (rs.getDate("UPDATED_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                } else {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                }
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setRemark("เอกสารลงทะเบียน");

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<DocFormStationFileViewModel> mapDataFOrm02File() {

        List<DocFormStationFileViewModel> dataList = new ArrayList<>();

        try {

            while (rs.next()) {
                DocFormStationFileViewModel data = new DocFormStationFileViewModel();
                //data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));
                if (rs.getDate("UPDATED_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                } else {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                }
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setRemark("ยกเลิกหรือเปลี่ยนแปลงข้อมูล");

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<DocFormStationFileViewModel> mapDataStationAddFile() {

        List<DocFormStationFileViewModel> dataList = new ArrayList<>();

        try {

            while (rs.next()) {
                DocFormStationFileViewModel data = new DocFormStationFileViewModel();
                //data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));
                if (rs.getDate("UPDATED_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                } else {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                }
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setRemark("แนบเพิ่มเติม");

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<DocFormFileViewModel> mapDataRetailAddFile() {

        List<DocFormFileViewModel> dataList = new ArrayList<>();

        try {

            while (rs.next()) {
                DocFormFileViewModel data = new DocFormFileViewModel();
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setRemark("แนบเพิ่มเติม");
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxForm01StationViewModel> mapListStation() {

        List<TaxForm01StationViewModel> dataList = new ArrayList<TaxForm01StationViewModel>();

        try {

            while (rs.next()) {
                TaxForm01StationViewModel data = new TaxForm01StationViewModel();
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBranchName(AppUtil.checkNullData(rs.getString("BRANCH_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
//                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));

                String fullAddress = "";
                fullAddress = data.getAddress() + " ซอย" + data.getSoi() + " ถนน" + data.getRoad() + " ตำบล" + data.getTambonName() + " อำเภอ" + data.getAmphurName() + " จังหวัด" + data.getProvinceName();
                data.setFullAddress(fullAddress);

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setMapFileNum(rs.getInt("MAP_FILE_NUM"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public DocFormStationViewModel mapDataStation() {

        DocFormStationViewModel data = new DocFormStationViewModel();
        try {

            while (rs.next()) {

                data.setRetailStationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setRetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBranchName(AppUtil.checkNullData(rs.getString("BRANCH_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
//                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));

                String fullAddress = "";
                fullAddress = data.getAddress() + " ซอย" + data.getSoi() + " ถนน" + data.getRoad() + " ตำบล" + data.getTambonName() + " อำเภอ" + data.getAmphurName() + " จังหวัด" + data.getProvinceName();
                data.setFullAddress(fullAddress);

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setMapFileNum(rs.getInt("MAP_FILE_NUM"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<DrpDownViewModel> mapDataStationBusiness() {

        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();

        try {

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(AppUtil.encryptId(rs.getInt("BUSINESS_TYPE_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("BUSINESS_TYPE_NAME")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxForm01StationFileViewModel> mapDataStationFile() {

        List<TaxForm01StationFileViewModel> dataList = new ArrayList<TaxForm01StationFileViewModel>();

        try {

            while (rs.next()) {
                TaxForm01StationFileViewModel data = new TaxForm01StationFileViewModel();
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<StatusLogViewModel> mapDataStatusLog() {

        List<StatusLogViewModel> dataList = new ArrayList<StatusLogViewModel>();

        try {

            while (rs.next()) {
                StatusLogViewModel data = new StatusLogViewModel();
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setRemark(AppUtil.checkNullData(rs.getString("REMARK")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public DocFormStationModel mapSaveStationData(DocFormStationViewModel stationView) {
        DocFormStationModel dataStation = new DocFormStationModel();
        try {
            //<editor-fold defaultstate="collapsed" desc="data station">
            dataStation.setRetailId(AppUtil.decryptId(stationView.getRetailId()));
            dataStation.setRetailStationId(AppUtil.decryptId(stationView.getRetailStationId()));
            dataStation.setRefStationId(AppUtil.decryptId(stationView.getRefStationId()));
            dataStation.setStationCode(AppUtil.checkNullData(stationView.getStationCode()));
            dataStation.setStationName(AppUtil.checkNullData(stationView.getStationName()));
            dataStation.setBranchName(AppUtil.checkNullData(stationView.getBranchName()));
            dataStation.setStatus(AppUtil.checkNullData(stationView.getStatus()));

            dataStation.setHouseNo(AppUtil.checkNullData(stationView.getHouseNo()));
            dataStation.setAddress(AppUtil.checkNullData(stationView.getAddress()));
            dataStation.setSoi(AppUtil.checkNullData(stationView.getSoi()));
            dataStation.setRoad(AppUtil.checkNullData(stationView.getRoad()));
            dataStation.setTambonId(AppUtil.checkNullData(stationView.getTambonId()));
            dataStation.setTambonName(AppUtil.checkNullData(stationView.getTambonName()));
            dataStation.setAmphurId(AppUtil.checkNullData(stationView.getAmphurId()));
            dataStation.setAmphurName(AppUtil.checkNullData(stationView.getAmphurName()));
            dataStation.setProvinceId(AppUtil.checkNullData(stationView.getProvinceId()));
            dataStation.setProvinceName(AppUtil.checkNullData(stationView.getProvinceName()));
            dataStation.setPostcode(AppUtil.checkNullData(stationView.getPostcode()));

            dataStation.setMobile(AppUtil.checkNullData(stationView.getMobile()));
            dataStation.setEmail(AppUtil.checkNullData(stationView.getEmail()));

            dataStation.setHouseFileNum(AppUtil.checkNullData(stationView.getHouseFileNum()));
            dataStation.setMapFileNum(AppUtil.checkNullData(stationView.getMapFileNum()));

            dataStation.setActive(stationView.getActive());
            dataStation.setCreatedBy(AppUtil.checkNullData(stationView.getCreatedBy()));

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="data retail file">
            List<DocFormStationFileViewModel> filterFiles = stationView.getFiles()
                    .stream()
                    .filter(c -> (c.getRemark().equals("แนบเพิ่มเติม")))
                    .collect(Collectors.toList());
            if (filterFiles != null && !filterFiles.isEmpty()) {
                List<DocFormStationFileModel> files = new ArrayList<>();
                for (DocFormStationFileViewModel file : filterFiles) {
                    DocFormStationFileModel dataFile = new DocFormStationFileModel();
                    dataFile.setTaxForm01RetailId(AppUtil.decryptId(stationView.getRetailId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AppUtil.checkNullData(file.getFileCate()));
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    files.add(dataFile);
                }
                dataStation.setFiles(files);
            }

            //</editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataStation;
    }

    public List<DocFormStationViewModel> mapListForStation() {
        List<DocFormStationViewModel> dataList = new ArrayList<DocFormStationViewModel>();

        try {

            while (rs.next()) {
                DocFormStationViewModel data = new DocFormStationViewModel();
                data.setRetailStationId(AppUtil.encryptId(rs.getInt("retail_station_id")));
                data.setRetailId(AppUtil.encryptId(rs.getInt("retail_id")));
                //data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("station_code")));
                data.setStationName(AppUtil.checkNullData(rs.getString("station_name")));
                data.setBranchName(AppUtil.checkNullData(rs.getString("branch_name")));

                data.setStatus(AppUtil.checkNullData(rs.getString("status")));
//                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                if (rs.getString("status") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("status")));
                }

                data.setHouseNo(AppUtil.checkNullData(rs.getString("house_no")));
                data.setAddress(AppUtil.checkNullData(rs.getString("address")));
                data.setSoi(AppUtil.checkNullData(rs.getString("soi")));
                data.setRoad(AppUtil.checkNullData(rs.getString("road")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("tambon_id")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("district_thai")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("amphur_id")));
                //data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("province_id")));
                //data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("postcode")));

//                String fullAddress = "";
//                fullAddress = data.getAddress() + " ซอย" + data.getSoi() + " ถนน" + data.getRoad() + " ตำบล" + data.getTambonName() + " อำเภอ" + data.getAmphurName() + " จังหวัด" + data.getProvinceName();
//                data.setFullAddress(fullAddress);
                data.setMobile(AppUtil.checkNullData(rs.getString("mobile")));
                data.setEmail(AppUtil.checkNullData(rs.getString("email")));

                data.setHouseFileNum(rs.getInt("house_file_num"));
                data.setMapFileNum(rs.getInt("map_file_num"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("owner_name")));
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

}
