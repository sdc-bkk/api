/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.PaymentMethodEnum;
import enumeration.PaymentStatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.FrontInvoiceViewModel;

/**
 *
 * @author User
 */
public class FrontPaymentMapper {

    private ResultSet rs = null;

    public FrontPaymentMapper() {

    }

    public FrontPaymentMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<FrontInvoiceViewModel> mapList() {

        List<FrontInvoiceViewModel> dataList = new ArrayList<FrontInvoiceViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportFrontInvoice = appConfig.value("export_invoice");

        try {

            while (rs.next()) {
                FrontInvoiceViewModel data = new FrontInvoiceViewModel();

                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                
                data.setYearly(rs.getInt("YEARLY"));
                data.setMonthly(rs.getInt("MONTHLY"));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTaxTotal(AppUtil.convertDoubleToStringFormat(rs.getDouble("TAX_TOTAL")));
                data.setExtraMoney(AppUtil.convertDoubleToStringFormat(rs.getDouble("EXTRA_MONEY")));
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                
                if (rs.getDate("DUE_DATE") != null) {
                    data.setDueDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
                } else {
                    data.setDueDate("");
                }
                
                //data.setPaymentStatus(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
                if (rs.getString("PAYMENT_STATUS") != null) {
                    try {
                        data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                    } catch (Exception e) {
                        data.setPaymentStatus("");
                    }
                } else {
                    data.setPaymentStatus("");
                }
                
                data.setLinkExportFrontInvoice(linkExportFrontInvoice + data.getInvoiceId());
//                data.setLinkExportFrontInvoice(linkExportFrontInvoice);
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

    public List<FrontInvoiceViewModel> mapListHistory() {

        List<FrontInvoiceViewModel> dataList = new ArrayList<FrontInvoiceViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportFrontReceipt = appConfig.value("export_receipt");

        try {

            while (rs.next()) {
                FrontInvoiceViewModel data = new FrontInvoiceViewModel();
/*
 SELECT I.INVOICE_ID, I.TAX_NO, I.PAYMENT_DATE, I.PAYMENT_TIME, I.TOTAL_AMOUNT, 
    I.PAYMENT_STATUS, I.PAYMENT_METHOD, I.PAYMENT_CHANNEL, 
    I.TAX_FORM03_ID, F.MONTHLY, F.YEARLY, F.REF_STATION_ID, S.STATION_NAME 
*/
                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                
                data.setYearly(rs.getInt("YEARLY"));
                data.setMonthly(rs.getInt("MONTHLY"));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                
                if (rs.getDate("PAYMENT_DATE") != null) {
                    data.setPaymentDate(DateUtils.toThai(rs.getDate("PAYMENT_DATE")));
                } else {
                    data.setPaymentDate("");
                }
                //data.setPaymentTime(AppUtil.checkNullData(rs.getString("PAYMENT_TIME")));
                if (!AppUtil.isNullAndSpace(rs.getString("PAYMENT_TIME"))) {
                    data.setPaymentTime(AppUtil.checkNullData(rs.getString("PAYMENT_TIME")));
                } else {
                    data.setPaymentTime("");
                }
                
                //data.setPaymentStatus(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
                if (rs.getString("PAYMENT_STATUS") != null) {
                    try {
                        data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                    } catch (Exception e) {
                        data.setPaymentStatus("");
                    }
                } else {
                    data.setPaymentStatus("");
                }
                
                data.setPaymentChannel(AppUtil.checkNullData(rs.getString("PAYMENT_CHANNEL")));
                //data.setPaymentMethod(AppUtil.checkNullData(rs.getString("PAYMENT_METHOD")));
                if (rs.getString("PAYMENT_METHOD") != null) {
                    try {
                        data.setPaymentMethod(PaymentMethodEnum.getDisplayName(rs.getInt("PAYMENT_METHOD")));
                    } catch (Exception e) {
                        data.setPaymentMethod("");
                    }
                } else {
                    data.setPaymentMethod("");
                }
                
                data.setLinkExportFrontReceipt(linkExportFrontReceipt + data.getInvoiceId());
                //data.setLinkExportFrontReceipt(linkExportFrontReceipt);
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

}
