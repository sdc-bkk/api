/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.RetailStationFileViewModel;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.TaxForm01RetailFileViewModel;
import viewModel.TaxForm01RetailViewModel;

/**
 *
 * @author User
 */
public class RetailMapper {

    private ResultSet rs = null;

    public RetailMapper() {

    }

    public RetailMapper(ResultSet rs) {
        this.rs = rs;
    }

    public RetailViewModel mapDataRetail() {

        RetailViewModel data = new RetailViewModel();
        String fullAddress = "";

        try {

            while (rs.next()) {
                data.setRetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setMainStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setMainStatusName(StatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }

                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCustomerType(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE")));
                data.setIdCard(AppUtil.checkNullData(rs.getString("ID_CARD")));
                data.setIdCardAt(AppUtil.checkNullData(rs.getString("ID_CARD_AT")));
                data.setCorpNo(AppUtil.checkNullData(rs.getString("CORP_NO")));
                data.setCorpDate(AppUtil.checkNullData(rs.getDate("CORP_DATE")));

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));

                fullAddress = AppUtil.checkNullData(rs.getString("ADDRESS"))
                        + (!"".equals(AppUtil.checkNullData(rs.getString("MOO"))) ? " หมู่ที่ " + rs.getString("MOO") : "")
                        + (!"".equals(AppUtil.checkNullData(rs.getString("SOI"))) ? " ตรอก/ซอย" + rs.getString("SOI") : "")
                        + (!"".equals(AppUtil.checkNullData(rs.getString("ROAD"))) ? " ถนน" + rs.getString("ROAD") : "")
                        + (!"".equals(AppUtil.checkNullData(rs.getString("TAMBON_THAI"))) ? " แขวง" + rs.getString("TAMBON_THAI") : "")
                        + (!"".equals(AppUtil.checkNullData(rs.getString("DISTRICT_THAI"))) ? " เขต" + rs.getString("DISTRICT_THAI") : "")
                        + " " + AppUtil.checkNullData(rs.getString("PROVINCE_THAI"));

//                fullAddress = AppUtil.checkNullData(rs.getString("ADDRESS")) + " ตรอก/ซอย " + AppUtil.checkNullData(rs.getString("SOI")) + " ถนน " + AppUtil.checkNullData(rs.getString("ROAD"))
//                        + " แขวง " + AppUtil.checkNullData(rs.getString("TAMBON_THAI")) + " เขต " + AppUtil.checkNullData(rs.getString("DISTRICT_THAI")) + " " + AppUtil.checkNullData(rs.getString("PROVINCE_THAI"));
                data.setFullAddress(fullAddress);
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setCerFileNum(rs.getInt("CER_FILE_NUM"));
                data.setAgentFileNum(rs.getInt("AGENT_FILE_NUM"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<TaxForm01RetailFileViewModel> mapDataRetailFile() {

        List<TaxForm01RetailFileViewModel> dataList = new ArrayList<TaxForm01RetailFileViewModel>();

        try {

            while (rs.next()) {
                TaxForm01RetailFileViewModel data = new TaxForm01RetailFileViewModel();
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public RetailStationViewModel mapDataStation() {

        RetailStationViewModel data = new RetailStationViewModel();
        try {

            while (rs.next()) {
                String fullAddress = "";
                data.setRetailStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));
                data.setRetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_THAI")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_THAI")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));

                fullAddress = AppUtil.checkNullData(rs.getString("ADDRESS")) + " ตรอก/ซอย " + AppUtil.checkNullData(rs.getString("SOI")) + " ถนน " + AppUtil.checkNullData(rs.getString("ROAD"))
                        + " แขวง " + AppUtil.checkNullData(rs.getString("TAMBON_THAI")) + " เขต " + AppUtil.checkNullData(rs.getString("DISTRICT_THAI")) + " " + AppUtil.checkNullData(rs.getString("PROVINCE_THAI"));
                data.setFullAddress(fullAddress);

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setMapFileNum(rs.getInt("MAP_FILE_NUM"));

                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<DrpDownViewModel> mapDataStationBusiness() {

        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();

        try {

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(AppUtil.encryptId(rs.getInt("BUSINESS_TYPE_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("BUSINESS_TYPE_NAME")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<RetailStationFileViewModel> mapDataStationFile() {

        List<RetailStationFileViewModel> dataList = new ArrayList<RetailStationFileViewModel>();

        try {

            while (rs.next()) {
                RetailStationFileViewModel data = new RetailStationFileViewModel();
                data.setRetailStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<RetailStationViewModel> mapDataStationList() {

        List<RetailStationViewModel> dataList = new ArrayList<RetailStationViewModel>();

        try {

            while (rs.next()) {
                RetailStationViewModel data = new RetailStationViewModel();
                String fullAddress = "";
                data.setRetailStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));
                data.setRetailId(AppUtil.encryptId(rs.getInt("RETAIL_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_THAI")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_THAI")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));

                fullAddress = AppUtil.checkNullData(rs.getString("ADDRESS")) + " ตรอก/ซอย " + AppUtil.checkNullData(rs.getString("SOI")) + " ถนน " + AppUtil.checkNullData(rs.getString("ROAD"))
                        + " แขวง " + AppUtil.checkNullData(rs.getString("TAMBON_THAI")) + " เขต " + AppUtil.checkNullData(rs.getString("DISTRICT_THAI")) + " " + AppUtil.checkNullData(rs.getString("PROVINCE_THAI"));
                data.setFullAddress(fullAddress);

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setMapFileNum(rs.getInt("MAP_FILE_NUM"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
}
