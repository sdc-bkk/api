/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.DrpDownModel;
import model.TaxForm01RetailFileModel;
import model.TaxForm01RetailModel;
import model.TaxForm01StationFileModel;
import model.TaxForm01StationModel;
import model.TaxForm03DetailModel;
import model.TaxForm03FileModel;
import model.TaxForm03Model;
import model.TaxForm03SummaryModel;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.MonthlyViewModel;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailFileViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationFileViewModel;
import viewModel.TaxForm01StationViewModel;
import viewModel.TaxForm03DetailViewModel;
import viewModel.TaxForm03FileViewModel;
import viewModel.TaxForm03SummaryViewModel;
import viewModel.TaxForm03ViewModel;

/**
 *
 * @author User
 */
public class TaxForm03Mapper {

    private ResultSet rs = null;

    public TaxForm03Mapper() {

    }

    public TaxForm03Mapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<MonthlyViewModel> mapList() {

        List<MonthlyViewModel> dataList = new ArrayList<MonthlyViewModel>();

        try {

            while (rs.next()) {
                MonthlyViewModel data = new MonthlyViewModel();
                data.setYearly(rs.getInt("YEARLY"));
                data.setMonthly(rs.getInt("MONTHLY"));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setWaitSendNum(rs.getInt("WAIT_SEND_NUM"));
                data.setWaitNum(rs.getInt("WAIT_NUM"));
                data.setNoPassNum(rs.getInt("NOPASS_NUM"));
                data.setPassNum(rs.getInt("PASS_NUM"));
                
                data.setTotalNum(data.getWaitNum() + data.getNoPassNum() + data.getPassNum() + data.getWaitSendNum());

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

    public List<TaxForm03ViewModel> mapListTaxForm03() {

        List<TaxForm03ViewModel> dataList = new ArrayList<TaxForm03ViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExport03 = appConfig.value("export_taxform03");
        String linkExportInvoice = appConfig.value("export_invoice");

        try {

            while (rs.next()) {
                TaxForm03ViewModel data = new TaxForm03ViewModel();
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setYearly(rs.getInt("YEARLY"));
                data.setMonthly(rs.getInt("MONTHLY"));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }
                data.setStatusAcc(AppUtil.checkNullData(rs.getString("STATUS_ACC")));
                data.setStatusAccDesc(AppUtil.checkNullData(rs.getString("STATUS_ACC_DESC")));

                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTaxTotal(rs.getDouble("TAX_TOTAL"));

                //link eaport                
                data.setLinkExportTaxform03(linkExport03 + data.getTaxForm03Id());
                data.setLinkExportInvoice(linkExportInvoice + data.getTaxForm03Id());

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

    public TaxForm03ViewModel mapData() {

        TaxForm03ViewModel data = new TaxForm03ViewModel();

        try {

            while (rs.next()) {
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));

                data.setYearly(rs.getInt("YEARLY"));
                data.setMonthly(rs.getInt("MONTHLY"));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }
                data.setStatusAcc(AppUtil.checkNullData(rs.getString("STATUS_ACC")));
                data.setStatusAccDesc(AppUtil.checkNullData(rs.getString("STATUS_ACC_DESC")));

                data.setAddTimes(rs.getInt("ADD_TIMES"));
                data.setSequence(rs.getInt("SEQUENCE"));
                data.setExtraMonth(rs.getInt("EXTRA_MONTH"));
                data.setExtraMoney(rs.getDouble("EXTRA_MONEY"));
                data.setExtraRate(rs.getDouble("EXTRA_RATE"));
                data.setExtraStartDate(AppUtil.checkNullData(rs.getDate("EXTRA_START_DATE")));
                data.setExtraEndDate(AppUtil.checkNullData(rs.getDate("EXTRA_END_DATE")));
                data.setTaxTotal(rs.getDouble("TAX_TOTAL"));

                data.setDueDate(AppUtil.checkNullData(rs.getDate("DUE_DATE")));
                
                data.setRefOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));                
                data.setRefOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));                
                data.setRefOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<TaxForm03FileViewModel> mapDataFile() {

        List<TaxForm03FileViewModel> dataList = new ArrayList<TaxForm03FileViewModel>();

        try {

            while (rs.next()) {
                TaxForm03FileViewModel data = new TaxForm03FileViewModel();
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxForm03SummaryViewModel> mapDataSummary() {

        List<TaxForm03SummaryViewModel> dataList = new ArrayList<TaxForm03SummaryViewModel>();

        try {

            while (rs.next()) {
                TaxForm03SummaryViewModel data = new TaxForm03SummaryViewModel();
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setSequence(rs.getInt("SEQUENCE"));
                data.setOilTypeId(AppUtil.encryptId(rs.getInt("OIL_TYPE_ID")));
                data.setOilTypeName(AppUtil.checkNullData(rs.getString("OIL_TYPE_NAME")));
                data.setQty(rs.getDouble("QTY"));
                data.setRate(rs.getDouble("RATE"));
                data.setAmount(rs.getDouble("AMOUNT"));
                data.setAmountBaht(rs.getInt("AMOUNT_BAHT"));
                data.setAmountSatang(rs.getInt("AMOUNT_SATANG"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxForm03DetailViewModel> mapDataDetail() {

        List<TaxForm03DetailViewModel> dataList = new ArrayList<TaxForm03DetailViewModel>();

        try {

            while (rs.next()) {
                TaxForm03DetailViewModel data = new TaxForm03DetailViewModel();
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setSequence(rs.getInt("SEQUENCE"));
                data.setOilTypeId(AppUtil.encryptId(rs.getInt("OIL_TYPE_ID")));
                data.setOilTypeName(AppUtil.checkNullData(rs.getString("OIL_TYPE_NAME")));
                data.setBalance(rs.getDouble("BALANCE"));
                data.setInQty(rs.getDouble("IN_QTY"));
                data.setOutQty(rs.getDouble("OUT_QTY"));
                data.setRemain(rs.getDouble("REMAIN"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxForm03Model mapSaveData(TaxForm03ViewModel dataView) {

        TaxForm03Model data = new TaxForm03Model();

        try {
            //<editor-fold defaultstate="collapsed" desc="data taxform">
            data.setTaxForm03Id(AppUtil.decryptId(dataView.getTaxForm03Id()));
            data.setRefRetailId(AppUtil.decryptId(dataView.getRefRetailId()));
            data.setRefStationId(AppUtil.decryptId(dataView.getRefStationId()));

            data.setDocNo(AppUtil.checkNullData(dataView.getDocNo()));
            data.setDocDate(AppUtil.checkNullData(dataView.getDocDate()));

            data.setYearly(dataView.getYearly());
            data.setMonthly(dataView.getMonthly());
            data.setMonthName(AppUtil.checkNullData(dataView.getMonthName()));

            data.setStatus(AppUtil.checkNullData(dataView.getStatus()));
            data.setStatusAcc(AppUtil.checkNullData(dataView.getStatusAcc()));
            data.setStatusAccDesc(AppUtil.checkNullData(dataView.getStatusAccDesc()));
            data.setAddTimes(dataView.getAddTimes());

            data.setSequence(dataView.getSequence());
            data.setExtraMonth(dataView.getExtraMonth());
            data.setExtraMoney(dataView.getExtraMoney());
            data.setExtraRate(dataView.getExtraRate());
            data.setExtraStartDate(dataView.getExtraStartDate());
            data.setExtraEndDate(dataView.getExtraEndDate());
            data.setTaxTotal(dataView.getTaxTotal());

            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="data taxform file">
            if (dataView.getAccountList() != null && !dataView.getAccountList().isEmpty()) {
                List<TaxForm03FileModel> accList = new ArrayList<>();
                for (TaxForm03FileViewModel file : dataView.getAccountList()) {
                    TaxForm03FileModel dataFile = new TaxForm03FileModel();
                    dataFile.setTaxForm03Id(AppUtil.decryptId(dataView.getTaxForm03Id()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.Account.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    accList.add(dataFile);
                }
                data.setAccountList(accList);
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="data summaryList">
            if (dataView.getSummaryList() != null && !dataView.getSummaryList().isEmpty()) {
                List<TaxForm03SummaryModel> summaryList = new ArrayList<>();
                for (TaxForm03SummaryViewModel child : dataView.getSummaryList()) {
                    TaxForm03SummaryModel dataChild = new TaxForm03SummaryModel();
                    dataChild.setTaxForm03Id(AppUtil.decryptId(child.getTaxForm03Id()));
                    dataChild.setSequence(child.getSequence());
                    dataChild.setOilTypeId(AppUtil.decryptId(child.getOilTypeId()));
                    dataChild.setOilTypeName(AppUtil.checkNullData(child.getOilTypeName()));
                    dataChild.setQty(child.getQty());
                    dataChild.setRate(child.getRate());
                    dataChild.setAmount(child.getAmount());
                    dataChild.setAmountBaht(child.getAmountBaht());
                    dataChild.setAmountSatang(child.getAmountSatang());
                    summaryList.add(dataChild);
                }
                data.setSummaryList(summaryList);
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="data detailList">
            if (dataView.getDetailList() != null && !dataView.getDetailList().isEmpty()) {
                List<TaxForm03DetailModel> detailList = new ArrayList<>();
                for (TaxForm03DetailViewModel child : dataView.getDetailList()) {
                    TaxForm03DetailModel dataChild = new TaxForm03DetailModel();
                    dataChild.setTaxForm03Id(AppUtil.decryptId(child.getTaxForm03Id()));
                    dataChild.setSequence(child.getSequence());
                    dataChild.setOilTypeId(AppUtil.decryptId(child.getOilTypeId()));
                    dataChild.setOilTypeName(AppUtil.checkNullData(child.getOilTypeName()));
                    dataChild.setBalance(child.getBalance());
                    dataChild.setInQty(child.getInQty());
                    dataChild.setOutQty(child.getOutQty());
                    dataChild.setRemain(child.getRemain());
                    detailList.add(dataChild);
                }
                data.setDetailList(detailList);
            }
            //</editor-fold>

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
