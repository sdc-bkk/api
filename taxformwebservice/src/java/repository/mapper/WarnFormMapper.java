/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import service.WarnFormService;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.MonthlyViewModel;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class WarnFormMapper {

    private ResultSet rs = null;

    public WarnFormMapper() {

    }

    public WarnFormMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<WarnFormViewModel> mapList() {

        List<WarnFormViewModel> dataList = new ArrayList<WarnFormViewModel>();

        try {

            while (rs.next()) {
                WarnFormViewModel data = new WarnFormViewModel();
                data.setStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));

                data.setYearly(AppUtil.checkNullData(rs.getString("MAX_YEARLY")));
                data.setMonthly(AppUtil.checkNullData(rs.getInt("MAX_MONTHLY")));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MAX_MONTHLY")));

                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI_SHORT")));

                int delayNum = 0;
                if (rs.getString("MAX_MONTHLY") != null && rs.getString("MAX_MONTHLY") != null) {
                    delayNum = WarnFormService.calDelayNum(rs.getInt("MAX_MONTHLY"), rs.getInt("MAX_YEARLY"));
                } else {
                    delayNum = 0;
                }
                data.setDelayNum(AppUtil.checkNullData(delayNum));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

    public List<WarnFormViewModel> mapListLog() {

        List<WarnFormViewModel> dataList = new ArrayList<WarnFormViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportWarn = appConfig.value("export_warnform");
        try {

            while (rs.next()) {
                WarnFormViewModel data = new WarnFormViewModel();
                data.setWarnFormId(AppUtil.encryptId(rs.getInt("WARN_FORM_ID")));
                data.setStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));

                data.setYearly(AppUtil.checkNullData(rs.getString("YEARLY")));
                data.setMonthly(AppUtil.checkNullData(rs.getInt("MONTHLY")));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));

                 data.setLinkExportWarnForm(linkExportWarn + data.getWarnFormId());
                                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }
}
