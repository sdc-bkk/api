/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.ForTypeEnum;
import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.TaxForm02FileModel;
import model.TaxForm02Model;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.TaxForm02FileViewModel;
import viewModel.TaxForm02ViewModel;

/**
 *
 * @author User
 */
public class TaxForm02Mapper {

    private ResultSet rs = null;

    public TaxForm02Mapper() {

    }

    public TaxForm02Mapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<TaxForm02ViewModel> mapList() {

        List<TaxForm02ViewModel> dataList = new ArrayList<TaxForm02ViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExport02 = appConfig.value("export_taxform02");

        try {

            while (rs.next()) {
                TaxForm02ViewModel data = new TaxForm02ViewModel();

                data.setTaxForm02Id(AppUtil.encryptId(rs.getInt("TAX_FORM02_ID")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));

                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME_OLD")));

                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationNameOld(AppUtil.checkNullData(rs.getString("STATION_NAME_OLD")));
                data.setOwnerNameOld(AppUtil.checkNullData(rs.getString("OWNER_NAME_OLD")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                data.setForType(AppUtil.checkNullData(rs.getString("FOR_TYPE")));
                if (rs.getString("FOR_TYPE") != null) {
                    data.setForTypeName(ForTypeEnum.getDisplayNameTH(rs.getInt("FOR_TYPE")));
                }
                //data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                if (rs.getInt("BUSINESS_STATUS_ID") > 0) {
                    data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                } else {
                    data.setBusinessStatusId("");
                }
                data.setBusinessStatusName(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                
                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                //link eaport                
                data.setLinkExportTaxform02(linkExport02 + data.getTaxForm02Id());

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxForm02ViewModel mapData() {

        TaxForm02ViewModel data = new TaxForm02ViewModel();

        try {

            while (rs.next()) {
                data.setTaxForm02Id(AppUtil.encryptId(rs.getInt("TAX_FORM02_ID")));
//                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
//                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                if (rs.getInt("REF_RETAIL_ID") > 0) {
                    data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                } else {
                    data.setRefRetailId("");
                }
                if (rs.getInt("REF_STATION_ID") > 0) {
                    data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                } else {
                    data.setRefStationId("");
                }
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setForType(AppUtil.checkNullData(rs.getString("FOR_TYPE")));
                if (rs.getString("FOR_TYPE") != null) {
                    data.setForTypeName(ForTypeEnum.getDisplayNameTH(rs.getInt("FOR_TYPE")));
                }
                //data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                if (rs.getInt("BUSINESS_STATUS_ID") > 0) {
                    data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                    data.setBusinessStatusCode(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_CODE")));

                } else {
                    data.setBusinessStatusId("");
                    data.setBusinessStatusCode("");
                }
                data.setBusinessStatusName(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_NAME")));
                data.setCancelDate(AppUtil.checkNullData(rs.getDate("CANCEL_DATE")));

                data.setChangeDate(AppUtil.checkNullData(rs.getDate("CHANGE_DATE")));
                data.setAddressOld(AppUtil.checkNullData(rs.getString("ADDRESS_OLD")));
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS_NEW")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));
                data.setMobileOld(AppUtil.checkNullData(rs.getString("MOBILE_OLD")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE_NEW")));

                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationNameOld(AppUtil.checkNullData(rs.getString("STATION_NAME_OLD")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME_NEW")));

                data.setOwnerNameOld(AppUtil.checkNullData(rs.getString("OWNER_NAME_OLD")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME_NEW")));
                data.setTaxNoOld(AppUtil.checkNullData(rs.getString("TAX_NO_OLD")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO_NEW")));
                data.setCustomerTypeOld(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE_OLD")));
                data.setCustomerType(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE_NEW")));
                data.setIdCardOld(AppUtil.checkNullData(rs.getString("ID_CARD_OLD")));
                data.setIdCard(AppUtil.checkNullData(rs.getString("ID_CARD_NEW")));
                data.setIdCardAtOld(AppUtil.checkNullData(rs.getString("ID_CARD_AT_OLD")));
                data.setIdCardAt(AppUtil.checkNullData(rs.getString("ID_CARD_AT_NEW")));
                data.setCorpNoOld(AppUtil.checkNullData(rs.getString("CORP_NO_OLD")));
                data.setCorpNo(AppUtil.checkNullData(rs.getString("CORP_NO_NEW")));
                data.setCorpDateOld(AppUtil.checkNullData(rs.getDate("CORP_DATE_OLD")));
                data.setCorpDate(AppUtil.checkNullData(rs.getDate("CORP_DATE_NEW")));

                data.setAddressRetail(AppUtil.checkNullData(rs.getString("ADDRESS_RETAIL")));
                data.setSoiRetail(AppUtil.checkNullData(rs.getString("SOI_RETAIL")));
                data.setRoadRetail(AppUtil.checkNullData(rs.getString("ROAD_RETAIL")));
                data.setTambonIdRetail(AppUtil.checkNullData(rs.getString("TAMBON_ID_RETAIL")));
                data.setTambonNameRetail(AppUtil.checkNullData(rs.getString("TAMBON_NAME_RETAIL")));
                data.setAmphurIdRetail(AppUtil.checkNullData(rs.getString("AMPHUR_ID_RETAIL")));
                data.setAmphurNameRetail(AppUtil.checkNullData(rs.getString("AMPHUR_NAME_RETAIL")));
                data.setProvinceIdRetail(AppUtil.checkNullData(rs.getString("PROVINCE_ID_RETAIL")));
                data.setProvinceNameRetail(AppUtil.checkNullData(rs.getString("PROVINCE_NAME_RETAIL")));
                data.setPostcodeRetail(AppUtil.checkNullData(rs.getString("POSTCODE_RETAIL")));
                data.setMobileRetail(AppUtil.checkNullData(rs.getString("MOBILE_RETAIL")));
                data.setCause(AppUtil.checkNullData(rs.getString("CAUSE")));
                
                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                
                data.setAddressStation(AppUtil.checkNullData(rs.getString("ADDRESS_STATION")));
                data.setSoiStation(AppUtil.checkNullData(rs.getString("SOI_STATION")));
                data.setRoadStation(AppUtil.checkNullData(rs.getString("ROAD_STATION")));
                data.setTambonIdStation(AppUtil.checkNullData(rs.getString("TAMBON_ID_STATION")));
                data.setTambonNameStation(AppUtil.checkNullData(rs.getString("TAMBON_NAME_STATION")));
                data.setAmphurIdStation(AppUtil.checkNullData(rs.getString("AMPHUR_ID_STATION")));
                data.setAmphurNameStation(AppUtil.checkNullData(rs.getString("AMPHUR_NAME_STATION")));
                data.setProvinceIdStation(AppUtil.checkNullData(rs.getString("PROVINCE_ID_STATION")));
                data.setProvinceNameStation(AppUtil.checkNullData(rs.getString("PROVINCE_NAME_STATION")));
                data.setPostcodeStation(AppUtil.checkNullData(rs.getString("POSTCODE_STATION")));
                data.setMobileStation(AppUtil.checkNullData(rs.getString("MOBILE_STATION")));
                
                data.setOther(AppUtil.checkNullData(rs.getString("OTHER")));
                data.setTaxRemain(rs.getDouble("TAX_REMAIN"));
                data.setTaxOilGas(rs.getDouble("TAX_OIL_GAS"));

                data.setFileNumE(rs.getInt("FILE_NUM_E"));
                data.setFileNumA(rs.getInt("FILE_NUM_A"));
                data.setFileNumR(rs.getInt("FILE_NUM_R"));
                data.setFileNumO(rs.getInt("FILE_NUM_O"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<TaxForm02FileViewModel> mapDataFile() {

        List<TaxForm02FileViewModel> dataList = new ArrayList<TaxForm02FileViewModel>();

        try {

            while (rs.next()) {
                TaxForm02FileViewModel data = new TaxForm02FileViewModel();
                data.setTaxForm02Id(AppUtil.encryptId(rs.getInt("TAX_FORM02_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));
                data.setFileCate(AppUtil.checkNullData(rs.getString("FILE_CATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public List<DrpDownViewModel> mapDataBusinessStatus() {

        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();

        try {

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_NAME")));
                data.setCode(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_CODE")));
                data.setOther(AppUtil.checkNullData(rs.getString("TEMPLATE_CODE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxForm02Model mapSaveData(TaxForm02ViewModel dataView) {

        TaxForm02Model data = new TaxForm02Model();

        try {

            //<editor-fold defaultstate="collapsed" desc="data TaxForm02">
            data.setTaxForm02Id(AppUtil.decryptId(dataView.getTaxForm02Id()));
            data.setRefRetailId(AppUtil.decryptId(dataView.getRefRetailId()));
            data.setRefStationId(AppUtil.decryptId(dataView.getRefStationId()));
            //data.setDocDate(AppUtil.checkNullData(dataView.getDocDate()));
            data.setDocNo(AppUtil.checkNullData(dataView.getDocNo()));
            //data.setStatus(AppUtil.checkNullData(dataView.getStatus()));
            data.setForType(AppUtil.checkNullData(dataView.getForType()));
            data.setBusinessStatusId(AppUtil.decryptId(dataView.getBusinessStatusId()));
            data.setBusinessStatusName(AppUtil.checkNullData(dataView.getBusinessStatusName()));
            data.setCancelDate(AppUtil.checkNullData(dataView.getCancelDate()));

            data.setChangeDate(AppUtil.checkNullData(dataView.getChangeDate()));
            data.setAddressOld(AppUtil.checkNullData(dataView.getAddressOld()));
            data.setHouseNo(AppUtil.checkNullData(dataView.getHouseNo()));
            data.setAddress(AppUtil.checkNullData(dataView.getAddress()));
            data.setSoi(AppUtil.checkNullData(dataView.getSoi()));
            data.setRoad(AppUtil.checkNullData(dataView.getRoad()));
            data.setTambonId(AppUtil.checkNullData(dataView.getTambonId()));
            data.setTambonName(AppUtil.checkNullData(dataView.getTambonName()));
            data.setAmphurId(AppUtil.checkNullData(dataView.getAmphurId()));
            data.setAmphurName(AppUtil.checkNullData(dataView.getAmphurName()));
            data.setProvinceId(AppUtil.checkNullData(dataView.getProvinceId()));
            data.setProvinceName(AppUtil.checkNullData(dataView.getProvinceName()));
            data.setPostcode(AppUtil.checkNullData(dataView.getPostcode()));
            data.setMobileOld(AppUtil.checkNullData(dataView.getMobileOld()));
            data.setMobile(AppUtil.checkNullData(dataView.getMobile()));

            data.setStationCode(AppUtil.checkNullData(dataView.getStationCode()));
            data.setStationNameOld(AppUtil.checkNullData(dataView.getStationNameOld()));
            data.setStationName(AppUtil.checkNullData(dataView.getStationName()));

            data.setOwnerNameOld(AppUtil.checkNullData(dataView.getOwnerNameOld()));
            data.setOwnerName(AppUtil.checkNullData(dataView.getOwnerName()));
            data.setTaxNoOld(AppUtil.checkNullData(dataView.getTaxNo()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
            data.setCustomerTypeOld(AppUtil.checkNullData(dataView.getCustomerTypeOld()));
            data.setCustomerType(AppUtil.checkNullData(dataView.getCustomerType()));
            data.setIdCardOld(AppUtil.checkNullData(dataView.getIdCardOld()));
            data.setIdCard(AppUtil.checkNullData(dataView.getIdCard()));
            data.setIdCardAtOld(AppUtil.checkNullData(dataView.getIdCardAtOld()));
            data.setIdCardAt(AppUtil.checkNullData(dataView.getIdCardAt()));
            data.setCorpNoOld(AppUtil.checkNullData(dataView.getCorpNoOld()));
            data.setCorpNo(AppUtil.checkNullData(dataView.getCorpNo()));
            data.setCorpDateOld(AppUtil.checkNullData(dataView.getCorpDateOld()));
            data.setCorpDate(AppUtil.checkNullData(dataView.getCorpDate()));

            data.setAddressRetail(AppUtil.checkNullData(dataView.getAddressRetail()));
            data.setSoiRetail(AppUtil.checkNullData(dataView.getSoiRetail()));
            data.setRoadRetail(AppUtil.checkNullData(dataView.getRoadRetail()));
            data.setTambonIdRetail(AppUtil.checkNullData(dataView.getTambonIdRetail()));
            data.setTambonNameRetail(AppUtil.checkNullData(dataView.getTambonNameRetail()));
            data.setAmphurIdRetail(AppUtil.checkNullData(dataView.getAmphurIdRetail()));
            data.setAmphurNameRetail(AppUtil.checkNullData(dataView.getAmphurNameRetail()));
            data.setProvinceIdRetail(AppUtil.checkNullData(dataView.getProvinceIdRetail()));
            data.setProvinceNameRetail(AppUtil.checkNullData(dataView.getProvinceNameRetail()));
            data.setPostcodeRetail(AppUtil.checkNullData(dataView.getPostcodeRetail()));
            data.setMobileRetail(AppUtil.checkNullData(dataView.getMobileRetail()));
            
            data.setCause(AppUtil.checkNullData(dataView.getCause()));
            
            data.setAddressStation(AppUtil.checkNullData(dataView.getAddressStation()));
            data.setSoiStation(AppUtil.checkNullData(dataView.getSoiStation()));
            data.setRoadStation(AppUtil.checkNullData(dataView.getRoadStation()));
            data.setTambonIdStation(AppUtil.checkNullData(dataView.getTambonIdStation()));
            data.setTambonNameStation(AppUtil.checkNullData(dataView.getTambonNameStation()));
            data.setAmphurIdStation(AppUtil.checkNullData(dataView.getAmphurIdStation()));
            data.setAmphurNameStation(AppUtil.checkNullData(dataView.getAmphurNameStation()));
            data.setProvinceIdStation(AppUtil.checkNullData(dataView.getProvinceIdStation()));
            data.setProvinceNameStation(AppUtil.checkNullData(dataView.getProvinceNameStation()));
            data.setPostcodeStation(AppUtil.checkNullData(dataView.getPostcodeStation()));
            data.setMobileStation(AppUtil.checkNullData(dataView.getMobileStation()));
            
            data.setOther(AppUtil.checkNullData(dataView.getOther()));
            data.setTaxRemain(dataView.getTaxRemain());
            data.setTaxOilGas(dataView.getTaxOilGas());

            data.setFileNumE(AppUtil.checkNullData(dataView.getFileNumE()));
            data.setFileNumA(AppUtil.checkNullData(dataView.getFileNumA()));
            data.setFileNumR(AppUtil.checkNullData(dataView.getFileNumR()));
            data.setFileNumO(AppUtil.checkNullData(dataView.getFileNumO()));

            data.setCreatedBy(AppUtil.checkNullData(dataView.getCreatedBy()));
            data.setUpdatedBy(AppUtil.checkNullData(dataView.getUpdatedBy()));

            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="data TaxForm02 file">
            if (!dataView.getBusinessStatusList().isEmpty()) {
                data.setBusinessStatusList(dataView.getBusinessStatusList());
            }
            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="data TaxForm02 file">
            if (!dataView.getCommerceFileList().isEmpty()) {
                List<TaxForm02FileModel> commerceList = new ArrayList<>();
                for (TaxForm02FileViewModel file : dataView.getCommerceFileList()) {
                    TaxForm02FileModel dataFile = new TaxForm02FileModel();
                    dataFile.setTaxForm02Id(AppUtil.decryptId(dataView.getTaxForm02Id()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.Commerce.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));

                    commerceList.add(dataFile);
                }
                data.setCommerceFileList(commerceList);
            }

            if (!dataView.getAuthorizeFileList().isEmpty()) {
                List<TaxForm02FileModel> authorizeList = new ArrayList<>();
                for (TaxForm02FileViewModel file : dataView.getAuthorizeFileList()) {
                    TaxForm02FileModel dataFile = new TaxForm02FileModel();
                    dataFile.setTaxForm02Id(AppUtil.decryptId(dataView.getTaxForm02Id()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.Authorize.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));

                    authorizeList.add(dataFile);
                }
                data.setAuthorizeFileList(authorizeList);
            }

            if (!dataView.getTaxRemainFileList().isEmpty()) {
                List<TaxForm02FileModel> taxRemainList = new ArrayList<>();
                for (TaxForm02FileViewModel file : dataView.getTaxRemainFileList()) {
                    TaxForm02FileModel dataFile = new TaxForm02FileModel();
                    dataFile.setTaxForm02Id(AppUtil.decryptId(dataView.getTaxForm02Id()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.TaxRemain.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));

                    taxRemainList.add(dataFile);
                }
                data.setTaxRemainFileList(taxRemainList);
            }

            if (!dataView.getOtherFileList().isEmpty()) {
                List<TaxForm02FileModel> otherList = new ArrayList<>();
                for (TaxForm02FileViewModel file : dataView.getOtherFileList()) {
                    TaxForm02FileModel dataFile = new TaxForm02FileModel();
                    dataFile.setTaxForm02Id(AppUtil.decryptId(dataView.getTaxForm02Id()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.Other.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));

                    otherList.add(dataFile);
                }
                data.setOtherFileList(otherList);
            }

            //</editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
