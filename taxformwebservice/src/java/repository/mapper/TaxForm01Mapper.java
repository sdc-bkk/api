/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.DrpDownModel;
import model.TaxForm01RetailFileModel;
import model.TaxForm01RetailModel;
import model.TaxForm01StationFileModel;
import model.TaxForm01StationModel;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailFileViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationFileViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 *
 * @author User
 */
public class TaxForm01Mapper {

    private ResultSet rs = null;

    public TaxForm01Mapper() {

    }

    public TaxForm01Mapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<TaxForm01RetailViewModel> mapList() {

        List<TaxForm01RetailViewModel> dataList = new ArrayList<TaxForm01RetailViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExport01 = appConfig.value("export_taxform01");
        String linkExportData01 = appConfig.value("export_taxform_data01");

        try {

            while (rs.next()) {
                TaxForm01RetailViewModel data = new TaxForm01RetailViewModel();

                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setMainStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setMainStatusName(StatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));

                data.setAmphurId(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                //จำนวนสถานการค้าปลีก (ราย)
                data.setStationNum(rs.getInt("STATION_NUM"));

                //link export                
                data.setLinkExportTaxform01(linkExport01 + data.getTaxForm01RetailId());
                data.setLinkExportTaxformData01(linkExportData01 + data.getTaxForm01RetailId());
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxForm01RetailViewModel mapDataRetail() {

        TaxForm01RetailViewModel data = new TaxForm01RetailViewModel();

        try {

            while (rs.next()) {
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setMainStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setMainStatusName(StatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }

                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCustomerType(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE")));
                data.setIdCard(AppUtil.checkNullData(rs.getString("ID_CARD")));
                data.setIdCardAt(AppUtil.checkNullData(rs.getString("ID_CARD_AT")));
                data.setCorpNo(AppUtil.checkNullData(rs.getString("CORP_NO")));
                data.setCorpDate(AppUtil.checkNullData(rs.getDate("CORP_DATE")));

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));

                String fullAddress = "";
                fullAddress = data.getAddress() + " ซอย" + data.getSoi() + " ถนน" + data.getRoad() + " ตำบล" + data.getTambonName() + " อำเภอ" + data.getAmphurName() + " จังหวัด" + data.getProvinceName();
                data.setFullAddress(fullAddress);

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                data.setSignerName(AppUtil.checkNullData(rs.getString("SIGNER_NAME")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setCerFileNum(rs.getInt("CER_FILE_NUM"));
                data.setAgentFileNum(rs.getInt("AGENT_FILE_NUM"));
                
                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setFirstRecord(rs.getBoolean("FIRST_RECORD"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<TaxForm01RetailFileViewModel> mapDataRetailFile() {

        List<TaxForm01RetailFileViewModel> dataList = new ArrayList<TaxForm01RetailFileViewModel>();

        try {

            while (rs.next()) {
                TaxForm01RetailFileViewModel data = new TaxForm01RetailFileViewModel();
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxForm01StationViewModel> mapListStation() {

        List<TaxForm01StationViewModel> dataList = new ArrayList<TaxForm01StationViewModel>();

        try {

            while (rs.next()) {
                TaxForm01StationViewModel data = new TaxForm01StationViewModel();
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBranchName(AppUtil.checkNullData(rs.getString("BRANCH_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
//                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setMoo(AppUtil.checkNullData(rs.getString("MOO")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));
                
                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                String fullAddress = "";
                fullAddress = data.getAddress() + " ซอย" + data.getSoi() + " ถนน" + data.getRoad() + " ตำบล" + data.getTambonName() + " อำเภอ" + data.getAmphurName() + " จังหวัด" + data.getProvinceName();
                data.setFullAddress(fullAddress);

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setMapFileNum(rs.getInt("MAP_FILE_NUM"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxForm01StationViewModel mapDataStation() {

        TaxForm01StationViewModel data = new TaxForm01StationViewModel();
        try {

            while (rs.next()) {
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBranchName(AppUtil.checkNullData(rs.getString("BRANCH_NAME")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
//                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setMoo(AppUtil.checkNullData(rs.getString("MOO")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));
                
                data.setOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                String fullAddress = "";
                fullAddress = data.getAddress() + " ซอย" + data.getSoi() + " ถนน" + data.getRoad() + " ตำบล" + data.getTambonName() + " อำเภอ" + data.getAmphurName() + " จังหวัด" + data.getProvinceName();
                data.setFullAddress(fullAddress);

                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));

                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setMapFileNum(rs.getInt("MAP_FILE_NUM"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<DrpDownViewModel> mapDataStationBusiness() {

        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();

        try {

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(AppUtil.encryptId(rs.getInt("BUSINESS_TYPE_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("BUSINESS_TYPE_NAME")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxForm01StationFileViewModel> mapDataStationFile() {

        List<TaxForm01StationFileViewModel> dataList = new ArrayList<TaxForm01StationFileViewModel>();

        try {

            while (rs.next()) {
                TaxForm01StationFileViewModel data = new TaxForm01StationFileViewModel();
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setFileName(AppUtil.checkNullData(rs.getString("FILE_NAME")));
                data.setFileType(AppUtil.checkNullData(rs.getString("FILE_TYPE")));
                data.setPathFile(AppUtil.checkNullData(rs.getString("PATH_FILE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<StatusLogViewModel> mapDataStatusLog() {

        List<StatusLogViewModel> dataList = new ArrayList<StatusLogViewModel>();

        try {

            while (rs.next()) {
                StatusLogViewModel data = new StatusLogViewModel();
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setRemark(AppUtil.checkNullData(rs.getString("REMARK")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxForm01RetailModel mapSaveData(TaxForm01RetailViewModel dataView) {

        TaxForm01RetailModel data = new TaxForm01RetailModel();

        try {
            //<editor-fold defaultstate="collapsed" desc="data retail">
            data.setTaxForm01RetailId(AppUtil.decryptId(dataView.getTaxForm01RetailId()));
            data.setRefRetailId(AppUtil.decryptId(dataView.getRefRetailId()));
            data.setDocNo(AppUtil.checkNullData(dataView.getDocNo()));
            data.setMainStatus(AppUtil.checkNullData(dataView.getMainStatus()));
            data.setOwnerName(AppUtil.checkNullData(dataView.getOwnerName()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
            data.setCustomerType(AppUtil.checkNullData(dataView.getCustomerType()));
            data.setIdCard(AppUtil.checkNullData(dataView.getIdCard()));
            data.setIdCardAt(AppUtil.checkNullData(dataView.getIdCardAt()));
            data.setCorpNo(AppUtil.checkNullData(dataView.getCorpNo()));
            data.setCorpDate(AppUtil.checkNullData(dataView.getCorpDate()));

            data.setHouseNo(AppUtil.checkNullData(dataView.getHouseNo()));
            data.setAddress(AppUtil.checkNullData(dataView.getAddress()));
            data.setSoi(AppUtil.checkNullData(dataView.getSoi()));
            data.setRoad(AppUtil.checkNullData(dataView.getRoad()));
            data.setTambonId(AppUtil.checkNullData(dataView.getTambonId()));
            data.setTambonName(AppUtil.checkNullData(dataView.getTambonName()));
            data.setAmphurId(AppUtil.checkNullData(dataView.getAmphurId()));
            data.setAmphurName(AppUtil.checkNullData(dataView.getAmphurName()));
            data.setProvinceId(AppUtil.checkNullData(dataView.getProvinceId()));
            data.setProvinceName(AppUtil.checkNullData(dataView.getProvinceName()));
            data.setPostcode(AppUtil.checkNullData(dataView.getPostcode()));

            data.setMobile(AppUtil.checkNullData(dataView.getMobile()));
            data.setEmail(AppUtil.checkNullData(dataView.getEmail()));

            data.setHouseFileNum(dataView.getHouseFileNum());
            data.setCerFileNum(dataView.getCerFileNum());
            data.setAgentFileNum(dataView.getAgentFileNum());

            data.setActive(dataView.getActive());
            data.setCreatedBy(AppUtil.checkNullData(dataView.getCreatedBy()));
            data.setUpdatedBy(AppUtil.checkNullData(dataView.getUpdatedBy()));

            data.setMemberName(AppUtil.checkNullData(dataView.getMemberName()));
            data.setFirstRecord(dataView.getFirstRecord());
            data.setSignerName(dataView.getSignerName());

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="data retail file">
            if (dataView.getHouseFileList() != null && !dataView.getHouseFileList().isEmpty()) {
                List<TaxForm01RetailFileModel> houseList = new ArrayList<>();
                for (TaxForm01RetailFileViewModel file : dataView.getHouseFileList()) {
                    TaxForm01RetailFileModel dataFile = new TaxForm01RetailFileModel();
                    dataFile.setTaxForm01RetailId(AppUtil.decryptId(dataView.getTaxForm01RetailId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.House.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    houseList.add(dataFile);
                }
                data.setHouseFileList(houseList);
            }
            if (dataView.getAgentFileList() != null && !dataView.getAgentFileList().isEmpty()) {
                List<TaxForm01RetailFileModel> agentList = new ArrayList<>();
                for (TaxForm01RetailFileViewModel file : dataView.getAgentFileList()) {
                    TaxForm01RetailFileModel dataFile = new TaxForm01RetailFileModel();
                    dataFile.setTaxForm01RetailId(AppUtil.decryptId(dataView.getTaxForm01RetailId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.Agent.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    agentList.add(dataFile);
                }
                data.setAgentFileList(agentList);
            }

            if (dataView.getCerFileList() != null && !dataView.getCerFileList().isEmpty()) {
                List<TaxForm01RetailFileModel> cerList = new ArrayList<>();
                for (TaxForm01RetailFileViewModel file : dataView.getCerFileList()) {
                    TaxForm01RetailFileModel dataFile = new TaxForm01RetailFileModel();
                    dataFile.setTaxForm01RetailId(AppUtil.decryptId(dataView.getTaxForm01RetailId()));
                    dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                    dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                    dataFile.setFileCate(AttachFileCateEnum.Certificate.display());
                    dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                    cerList.add(dataFile);
                }
                data.setCerFileList(cerList);
            }

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="data station">
            if (dataView.getStationList() != null && !dataView.getStationList().isEmpty()) {
                List<TaxForm01StationModel> stationList = new ArrayList<>();
                for (TaxForm01StationViewModel stationView : dataView.getStationList()) {
                    TaxForm01StationModel dataStation = new TaxForm01StationModel();
                    dataStation.setTaxForm01RetailId(AppUtil.decryptId(dataView.getTaxForm01RetailId()));
                    dataStation.setTaxForm01StationId(AppUtil.decryptId(stationView.getTaxForm01StationId()));
                    dataStation.setRefStationId(AppUtil.decryptId(stationView.getRefStationId()));
                    dataStation.setStationCode(AppUtil.checkNullData(stationView.getStationCode()));
                    dataStation.setStationName(AppUtil.checkNullData(stationView.getStationName()));
                    dataStation.setBranchName(AppUtil.checkNullData(stationView.getBranchName()));
                    dataStation.setStatus(AppUtil.checkNullData(stationView.getStatus()));

                    dataStation.setHouseNo(AppUtil.checkNullData(stationView.getHouseNo()));
                    dataStation.setAddress(AppUtil.checkNullData(stationView.getAddress()));
                    dataStation.setMoo(AppUtil.checkNullData(stationView.getMoo()));
                    dataStation.setSoi(AppUtil.checkNullData(stationView.getSoi()));
                    dataStation.setRoad(AppUtil.checkNullData(stationView.getRoad()));
                    dataStation.setTambonId(AppUtil.checkNullData(stationView.getTambonId()));
                    dataStation.setTambonName(AppUtil.checkNullData(stationView.getTambonName()));
                    dataStation.setAmphurId(AppUtil.checkNullData(stationView.getAmphurId()));
                    dataStation.setAmphurName(AppUtil.checkNullData(stationView.getAmphurName()));
                    dataStation.setProvinceId(AppUtil.checkNullData(stationView.getProvinceId()));
                    dataStation.setProvinceName(AppUtil.checkNullData(stationView.getProvinceName()));
                    dataStation.setPostcode(AppUtil.checkNullData(stationView.getPostcode()));

                    dataStation.setMobile(AppUtil.checkNullData(stationView.getMobile()));
                    dataStation.setEmail(AppUtil.checkNullData(stationView.getEmail()));

                    dataStation.setHouseFileNum(AppUtil.checkNullData(stationView.getHouseFileNum()));
                    dataStation.setMapFileNum(AppUtil.checkNullData(stationView.getMapFileNum()));

                    dataStation.setActive(stationView.getActive());
                    dataStation.setCreatedBy(AppUtil.checkNullData(stationView.getCreatedBy()));

                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="data business type">
                    if (stationView.getBusinessTypeList() != null && !stationView.getBusinessTypeList().isEmpty()) {
                        List<DrpDownModel> businessList = new ArrayList<>();
                        for (DrpDownViewModel child : stationView.getBusinessTypeList()) {
                            DrpDownModel dataChild = new DrpDownModel();
                            dataChild.setId(AppUtil.decryptId(child.getId()));
                            dataChild.setName(AppUtil.checkNullData(child.getName()));
                            businessList.add(dataChild);
                        }
                        dataStation.setBusinessTypeList(businessList);
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="data station file">
                    if (stationView.getHouseFileList() != null && !stationView.getHouseFileList().isEmpty()) {
                        List<TaxForm01StationFileModel> houseStaList = new ArrayList<>();
                        for (TaxForm01StationFileViewModel file : stationView.getHouseFileList()) {
                            TaxForm01StationFileModel dataFile = new TaxForm01StationFileModel();
                            dataFile.setTaxForm01StationId(AppUtil.decryptId(stationView.getTaxForm01StationId()));
                            dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                            dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                            dataFile.setFileCate(AttachFileCateEnum.House.display());
                            dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                            houseStaList.add(dataFile);
                        }
                        dataStation.setHouseFileList(houseStaList);
                    }
                    if (stationView.getMapFileList() != null && !stationView.getMapFileList().isEmpty()) {
                        List<TaxForm01StationFileModel> mapList = new ArrayList<>();
                        for (TaxForm01StationFileViewModel file : stationView.getMapFileList()) {
                            TaxForm01StationFileModel dataFile = new TaxForm01StationFileModel();
                            dataFile.setTaxForm01StationId(AppUtil.decryptId(stationView.getTaxForm01StationId()));
                            dataFile.setFileName(AppUtil.checkNullData(file.getFileName()));
                            dataFile.setPathFile(AppUtil.checkNullData(file.getPathFile()));
                            dataFile.setFileCate(AttachFileCateEnum.Map.display());
                            dataFile.setFileType(AppUtil.checkNullData(file.getFileType()));
                            mapList.add(dataFile);
                        }
                        dataStation.setMapFileList(mapList);
                    }
                    //</editor-fold>
                    stationList.add(dataStation);

                }

                data.setStationList(stationList);
            }
            //</editor-fold>

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
