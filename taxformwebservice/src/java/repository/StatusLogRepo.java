/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.StatusEnum;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.ResultData;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm02ViewModel;

/**
 *
 * @author User
 */
public class StatusLogRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData sendApproveTaxForm01(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {
                int i = 1;

                int id = AppUtil.decryptId(idString);
                boolean checkStatusData = false;

                //<editor-fold defaultstate="collapsed" desc="check status data is Create or NoPass only">
                String sql = " SELECT COUNT(1) COUNTSTATUS FROM TAX_FORM01_STATION  "
                        + " WHERE (TAX_FORM01_RETAIL_ID=?) AND (STATUS=? OR STATUS=?) ";

                psSelect.setSql(sql);
                psSelect.setInt(i++, id);
                psSelect.setString(i++, String.valueOf(StatusEnum.Create.value())); //สร้าง
                psSelect.setString(i++, String.valueOf(StatusEnum.NoPass.value())); //ไม่ผ่าน
                rs = psSelect.executeQuery();
                while (rs.next()) {
                    checkStatusData = rs.getInt("COUNTSTATUS") > 0;
                }
                //</editor-fold>

                if (checkStatusData) {
                    i = 1;

                    sql = "SELECT TAX_FORM01_STATION_ID  FROM TAX_FORM01_STATION  "
                            + " WHERE (TAX_FORM01_RETAIL_ID=?) ";

                    psSelect.setSql(sql);
                    psSelect.setInt(i++, id);
                    rs = psSelect.executeQuery();

                    String sqlInsert = "INSERT INTO TAX_FORM01_STATUS_LOG ( "
                            + "  TAX_FORM01_STATION_ID "
                            + ", STATUS "
                            + ", STATUS_NAME "
                            + ", REMARK "
                            + ", ACTION_DATE "
                            + ", ACTION_BY "
                            + ") VALUES(?,?,?,?,SYSDATE,?) ";
                    while (rs.next()) {
                        i = 1;
                        ps = conn.prepareStatement(sqlInsert);
                        ps.setInt(i++, rs.getInt("TAX_FORM01_STATION_ID"));
                        ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));
                        ps.setString(i++, StatusEnum.Wait.displayNameTH());
                        ps.setString(i++, data.getRemark());

                        if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                            ps.setString(i++, data.getActionBy());
                        } else {
                            ps.setNull(i++, java.sql.Types.NVARCHAR);
                        }
                        ps.executeUpdate();
                    }

                    //<editor-fold defaultstate="collapsed" desc="update main status ">
                    String sqlUpdate = " UPDATE TAX_FORM01_RETAIL SET "
                            + " MAIN_STATUS = ? "
                            //                            + " ,MAIN_STATUS_NAME=? "
                            + " WHERE TAX_FORM01_RETAIL_ID = ? ";
                    i = 1;
                    ps = conn.prepareStatement(sqlUpdate);
                    ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));
//                    ps.setString(i++, StatusEnum.Wait.displayNameTH());
                    ps.setInt(i++, id);

                    ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="update status station">
                    sqlUpdate = " UPDATE TAX_FORM01_STATION SET "
                            + " STATUS = ? "
                            //                            + " ,STATUS_NAME=? "
                            + " WHERE TAX_FORM01_RETAIL_ID = ? ";
                    i = 1;
                    ps = conn.prepareStatement(sqlUpdate);
                    ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));
//                    ps.setString(i++, StatusEnum.Wait.displayNameTH());
                    ps.setInt(i++, id);

                    ps.executeUpdate();
                    //</editor-fold>
                } else {
                    //status not update
                }

            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData approveTaxForm01(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {
                int i = 1;

                int id = AppUtil.decryptId(idString); //TAX_FORM01_STATION_ID
                int taxForm01RetailId = 0; //TAX_FORM01_RETAIL_ID
                int refStationId = 0; //REF_STATION_ID
                boolean checkStatusData = false;

                String sql = "SELECT TAX_FORM01_RETAIL_ID ,REF_STATION_ID FROM TAX_FORM01_STATION  "
                        + " WHERE (TAX_FORM01_STATION_ID=? ) ";

                psSelect.setSql(sql);
                psSelect.setInt(i++, id);
                rs = psSelect.executeQuery();

                while (rs.next()) {
                    taxForm01RetailId = rs.getInt("TAX_FORM01_RETAIL_ID");
                    refStationId = rs.getInt("REF_STATION_ID");
                }

                String sqlInsert = "INSERT INTO TAX_FORM01_STATUS_LOG ( "
                        + "  TAX_FORM01_STATION_ID "
                        + ", STATUS "
                        + ", STATUS_NAME "
                        + ", REMARK "
                        + ", ACTION_DATE "
                        + ", ACTION_BY "
                        + ") VALUES(?,?,?,?,SYSDATE,?) ";
//            while (rs.next()) {
                i = 1;
                ps = conn.prepareStatement(sqlInsert);
//                ps.setInt(i++, AppUtil.decryptId(data.getTaxForm01StationId()));
                ps.setInt(i++, id);
                ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                ps.setString(i++, StatusEnum.Pass.displayNameTH());
                ps.setString(i++, data.getRemark());

                if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                    ps.setString(i++, data.getActionBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                ps.executeUpdate();
//            }

                //<editor-fold defaultstate="collapsed" desc="update status TAX_FORM01_STATION">
                String sqlUpdate = " UPDATE TAX_FORM01_STATION SET "
                        + " STATUS = ? "
                        + " WHERE TAX_FORM01_STATION_ID = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                ps.setInt(i++, id);
//                ps.setInt(i, AppUtil.decryptId(data.getTaxForm01StationId()));

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="update status RETAIL_STATION">
                sqlUpdate = " UPDATE RETAIL_STATION SET "
                        + " STATUS = ? "
                        + " WHERE RETAIL_STATION_ID = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                ps.setInt(i++, refStationId);
//                ps.setInt(i, AppUtil.decryptId(data.getTaxForm01StationId()));

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="update main status TAX_FORM01_STATION">
                sql = "SELECT COUNT(*)AS COUNT_NO_PASS  FROM TAX_FORM01_STATION  "
                        + " WHERE (TAX_FORM01_RETAIL_ID=?) AND STATUS != ? ";

                i = 1;
                psSelect.setSql(sql);
                psSelect.setInt(i++, taxForm01RetailId);
                psSelect.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                rs = psSelect.executeQuery();
                int countNoPass = 0;
                while (rs.next()) {
                    countNoPass = rs.getInt("COUNT_NO_PASS");
                }

                if (countNoPass == 0) {
                    sqlUpdate = " UPDATE TAX_FORM01_RETAIL SET "
                            + " MAIN_STATUS = ? "
                            + " WHERE TAX_FORM01_RETAIL_ID = ? ";
                    i = 1;
                    ps = conn.prepareStatement(sqlUpdate);
                    ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                    ps.setInt(i++, taxForm01RetailId);

                    ps.executeUpdate();
                }

                sqlUpdate = " UPDATE RETAIL SET "
                        + " MAIN_STATUS = ? "
                        + " WHERE RETAIL_ID IN (SELECT REF_RETAIL_ID FROM TAX_FORM01_RETAIL WHERE TAX_FORM01_RETAIL_ID = ?) ";
//                        + "AND MAIN_STATUS = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                ps.setInt(i++, taxForm01RetailId);
//                ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));

                ps.executeUpdate();
                //</editor-fold>

            }
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData rejectTaxForm01(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {
                int i = 1;

                int id = AppUtil.decryptId(idString); //TAX_FORM01_STATION_ID
                int taxForm01RetailId = 0; //TAX_FORM01_RETAIL_ID
                int refStationId = 0; //REF_STATION_ID
                boolean checkStatusData = false;

                String sql = "SELECT TAX_FORM01_RETAIL_ID ,REF_STATION_ID FROM TAX_FORM01_STATION  "
                        + " WHERE (TAX_FORM01_STATION_ID=? ) ";

                psSelect.setSql(sql);
                psSelect.setInt(i++, id);
                rs = psSelect.executeQuery();

                while (rs.next()) {
                    taxForm01RetailId = rs.getInt("TAX_FORM01_RETAIL_ID");
                    refStationId = rs.getInt("REF_STATION_ID");
                }

                String sqlInsert = "INSERT INTO TAX_FORM01_STATUS_LOG ( "
                        + "  TAX_FORM01_STATION_ID "
                        + ", STATUS "
                        + ", STATUS_NAME "
                        + ", REMARK "
                        + ", ACTION_DATE "
                        + ", ACTION_BY "
                        + ") VALUES(?,?,?,?,SYSDATE,?) ";
//            while (rs.next()) {
                i = 1;
                ps = conn.prepareStatement(sqlInsert);
//                ps.setInt(i++, AppUtil.decryptId(data.getTaxForm01StationId()));
                ps.setInt(i++, id);
                ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
                ps.setString(i++, StatusEnum.NoPass.displayNameTH());
                ps.setString(i++, data.getRemark());

                if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                    ps.setString(i++, data.getActionBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                ps.executeUpdate();
//            }

                //<editor-fold defaultstate="collapsed" desc="update status TAX_FORM01_STATION">
                String sqlUpdate = " UPDATE TAX_FORM01_STATION SET "
                        + " STATUS = ? "
                        + " WHERE TAX_FORM01_STATION_ID = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
//                ps.setInt(i, AppUtil.decryptId(data.getTaxForm01StationId()));
                ps.setInt(i++, id);

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="update status RETAIL_STATION">
                sqlUpdate = " UPDATE RETAIL_STATION SET "
                        + " STATUS = ? "
                        + " WHERE RETAIL_STATION_ID  = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
//                ps.setInt(i, AppUtil.decryptId(data.getTaxForm01StationId()));
                ps.setInt(i++, refStationId);

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="update main status TAX_FORM01_RETAIL ">
//            String sql = "SELECT COUNT(*)AS COUNT_NO_PASS  FROM TAX_FORM01_STATION  "
//                    + " WHERE (TAX_FORM01_RETAIL_ID=?) AND STATUS != ? ";
//
//            i=1;
//            psSelect.setSql(sql);
//            psSelect.setString(i++, String.valueOf(StatusEnum.Pass.value()));
//            psSelect.setString(i++, AppUtil.decrypt(data.getTaxForm01RetailId()));
//            rs = psSelect.executeQuery();
//            int countNoPass = 0;
//            while (rs.next()) {
//                countNoPass = rs.getInt("COUNT_NO_PASS");
//            }
//
//            if (countNoPass == 0) {
                sqlUpdate = " UPDATE TAX_FORM01_RETAIL SET "
                        + " MAIN_STATUS = ? "
                        + " WHERE TAX_FORM01_RETAIL_ID = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
//                ps.setString(i++, AppUtil.decrypt(data.getTaxForm01RetailId()));
                ps.setInt(i++, taxForm01RetailId);

                ps.executeUpdate();

                sqlUpdate = " UPDATE RETAIL SET "
                        + " MAIN_STATUS = ? "
                        + " WHERE RETAIL_ID IN (SELECT REF_RETAIL_ID FROM TAX_FORM01_RETAIL WHERE TAX_FORM01_RETAIL_ID = ?) ";
//                        + "AND MAIN_STATUS = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
//                ps.setString(i++, AppUtil.decrypt(data.getTaxForm01RetailId()));
                ps.setInt(i++, taxForm01RetailId);
//                ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));

                ps.executeUpdate();
//            }
                //</editor-fold>
            }
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData sendApproveTaxForm02(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {

                int id = AppUtil.decryptId(idString);
                boolean checkStatusData = false;

                //<editor-fold defaultstate="collapsed" desc="check status data is Create or NoPass only">
                sql = " SELECT COUNT(1) COUNTSTATUS FROM TAX_FORM02  "
                        + " WHERE (TAX_FORM02_ID=?) AND (STATUS=? OR STATUS=?) ";

                psSelect.setSql(sql);
                psSelect.setInt(i++, id);
                psSelect.setString(i++, String.valueOf(StatusEnum.Create.value())); //สร้าง
                psSelect.setString(i++, String.valueOf(StatusEnum.NoPass.value())); //ไม่ผ่าน
                rs = psSelect.executeQuery();
                while (rs.next()) {
                    checkStatusData = rs.getInt("COUNTSTATUS") > 0;
                }
                //</editor-fold>

                if (checkStatusData) {

                    //<editor-fold defaultstate="collapsed" desc="insert to TaxForm02 status log ">
                    String sqlInsert = "INSERT INTO TAX_FORM02_STATUS_LOG ( "
                            + "  TAX_FORM02_ID "
                            + ", STATUS "
                            + ", STATUS_NAME "
                            + ", REMARK "
                            + ", ACTION_DATE "
                            + ", ACTION_BY "
                            + ") VALUES(?,?,?,?,SYSDATE,?) ";
                    i = 1;
                    ps = conn.prepareStatement(sqlInsert);
                    ps.setInt(i++, id);
                    ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));
                    ps.setString(i++, StatusEnum.Wait.displayNameTH());
                    ps.setString(i++, data.getRemark());

                    if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                        ps.setString(i++, data.getActionBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }
                    ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="update TaxForm02 status ">
                    String sqlUpdate = " UPDATE TAX_FORM02 SET "
                            + " STATUS = ? "
                            + ", STATUS_NAME = ? "
                            + ", UPDATED_DATE = SYSDATE "
                            + ", UPDATED_BY = ? "
                            + " WHERE TAX_FORM02_ID = ? ";
                    i = 1;
                    ps = conn.prepareStatement(sqlUpdate);
                    ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));
                    ps.setString(i++, StatusEnum.Wait.displayNameTH());
                    if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                        ps.setString(i++, data.getActionBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }
                    ps.setInt(i++, id);

                    ps.executeUpdate();
                    //</editor-fold>

                } else {
                    //status not update
                }

            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData approveTaxForm02(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {

                int id = AppUtil.decryptId(idString);
                boolean checkStatusData = false;

                //<editor-fold defaultstate="collapsed" desc="check status data is Wait only">
                sql = " SELECT COUNT(1) COUNTSTATUS FROM TAX_FORM02  "
                        + " WHERE (TAX_FORM02_ID=?) AND (STATUS=?) ";

                psSelect.setSql(sql);
                psSelect.setInt(i++, id);
                psSelect.setString(i++, String.valueOf(StatusEnum.Wait.value())); //รอตรวจสอบ
                rs = psSelect.executeQuery();
                while (rs.next()) {
                    checkStatusData = rs.getInt("COUNTSTATUS") > 0;
                }
                //</editor-fold>

                if (checkStatusData) {

                    //<editor-fold defaultstate="collapsed" desc="insert to TaxForm02 status log ">
                    String sqlInsert = "INSERT INTO TAX_FORM02_STATUS_LOG ( "
                            + "  TAX_FORM02_ID "
                            + ", STATUS "
                            + ", STATUS_NAME "
                            + ", REMARK "
                            + ", ACTION_DATE "
                            + ", ACTION_BY "
                            + ") VALUES(?,?,?,?,SYSDATE,?) ";
                    i = 1;
                    ps = conn.prepareStatement(sqlInsert);
                    ps.setInt(i++, id);
                    ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                    ps.setString(i++, StatusEnum.Pass.displayNameTH());
                    ps.setString(i++, data.getRemark());

                    if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                        ps.setString(i++, data.getActionBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }
                    ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="update TaxForm02 status ">
                    String sqlUpdate = " UPDATE TAX_FORM02 SET "
                            + " STATUS = ? "
                            + ", STATUS_NAME = ? "
                            + ", UPDATED_DATE = SYSDATE "
                            + ", UPDATED_BY = ? "
                            + " WHERE TAX_FORM02_ID = ? ";
                    i = 1;
                    ps = conn.prepareStatement(sqlUpdate);
                    ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                    ps.setString(i++, StatusEnum.Pass.displayNameTH());
                    if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                        ps.setString(i++, data.getActionBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }
                    ps.setInt(i++, id);

                    ps.executeUpdate();
                    //</editor-fold>

                } else {
                    //status not update
                }

                //<editor-fold defaultstate="collapsed" desc="update retail station ">
                //<editor-fold defaultstate="collapsed" desc="get data taxform02">
                TaxForm02Repo taxForm02Repo = new TaxForm02Repo();
                ResultData<TaxForm02ViewModel> taxform02 = taxForm02Repo.getData(id);
                TaxForm02ViewModel taxform02Data = new TaxForm02ViewModel();
                if (taxform02.getResult() != null) {
                    taxform02Data = taxform02.getResult();
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="ตรวจสอบว่าทำรายการเกี่ยวกับอะไรมา">
                List<String> businessStatusList = new ArrayList<>();

                sql = " SELECT DISTINCT(TEMPLATE_CODE) FROM TAX_FORM02_BUSINESS_STATUS  "
                        + " WHERE (TAX_FORM02_ID=?)";

                psSelect.setSql(sql);
                psSelect.setInt(1, id);
                rs = psSelect.executeQuery();
                while (rs.next()) {
                    businessStatusList.add(rs.getString("TEMPLATE_CODE"));
                }
                //</editor-fold>

                for (String businessStatus : businessStatusList) {

                    if (null != businessStatus) {
                        switch (businessStatus) {
                            case "A":
                                //เลิกกิจการ
                                //<editor-fold defaultstate="collapsed" desc="update retail station ">
                                String sqlUpdateRetailStation = " UPDATE RETAIL_STATION SET "
                                        + " CANCEL_DATE = ? "
                                        + " WHERE RETAIL_STATION_ID = ? ";
                                i = 1;
                                ps = conn.prepareStatement(sqlUpdateRetailStation);
                                ps.setDate(1, AppUtil.toDateSql(taxform02Data.getCancelDate()));
                                ps.setInt(2, AppUtil.decryptId(taxform02Data.getRefStationId()));

                                ps.executeUpdate();
                                //</editor-fold>
                                break;
                            case "B":
                                //ย้ายสถานที่ตั้งสถานการค้าปลีก
                                //<editor-fold defaultstate="collapsed" desc="update retail station ">
                                String sqlUpdateRetailStation2 = " UPDATE RETAIL_STATION SET "
                                        + " ADDRESS = ? "
                                        + " ,SOI = ? "
                                        + " ,ROAD = ? "
                                        + " ,TAMBON_ID = ? "
                                        + " ,AMPHUR_ID = ? "
                                        + " ,PROVINCE_ID = ? "
                                        + " ,POSTCODE = ? "
                                        + " ,MOBILE = ? "
                                        + " WHERE RETAIL_STATION_ID = ? ";
                                i = 1;
                                ps = conn.prepareStatement(sqlUpdateRetailStation2);
                                ps.setString(i++, taxform02Data.getAddress());
                                ps.setString(i++, taxform02Data.getSoi());
                                ps.setString(i++, taxform02Data.getRoad());
                                ps.setString(i++, taxform02Data.getTambonId());
                                ps.setString(i++, taxform02Data.getAmphurId());
                                ps.setString(i++, taxform02Data.getProvinceId());
                                ps.setString(i++, taxform02Data.getPostcode());
                                ps.setString(i++, taxform02Data.getMobile());
                                ps.setInt(i++, AppUtil.decryptId(taxform02Data.getRefStationId()));

                                ps.executeUpdate();
                                //</editor-fold>
                                break;
                            case "C":
                                //เปลี่ยนเจ้าของ
                                
                                break;
                            case "E":
                                //เปลี่ยนชื่อสถานการค้าปลีก
                                //<editor-fold defaultstate="collapsed" desc="update retail station ">
                                String sqlUpdateRetailStation4 = " UPDATE RETAIL_STATION SET  STATION_NAME = ?  WHERE RETAIL_STATION_ID = ? ";
                                i = 1;
                                ps = conn.prepareStatement(sqlUpdateRetailStation4);
                                ps.setString(1, taxform02Data.getStationName());
                                ps.setInt(2, AppUtil.decryptId(taxform02Data.getRefStationId()));

                                ps.executeUpdate();
                                //</editor-fold>
                                break;
                            //อื่นๆ
                            case "F":
                                break;
                            default:
                                break;
                        }
                    }

                }
                //</editor-fold>

            }
            //</editor-fold>

            //TODO ถ้ายกเลิกกิจการให้เช็คด้วยว่ายกเลิกกิจการครบทุกสถานหรือยัง ถ้าครบแล้วให้ retail ยกเลิกกิจการด้วย
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData rejectTaxForm02(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {

                int id = AppUtil.decryptId(idString);
                boolean checkStatusData = false;

                //<editor-fold defaultstate="collapsed" desc="check status data is Wait only">
                sql = " SELECT COUNT(1) COUNTSTATUS FROM TAX_FORM02  "
                        + " WHERE (TAX_FORM02_ID=?) AND (STATUS=?) ";

                psSelect.setSql(sql);
                psSelect.setInt(i++, id);
                psSelect.setString(i++, String.valueOf(StatusEnum.Wait.value())); //รอตรวจสอบ
                rs = psSelect.executeQuery();
                while (rs.next()) {
                    checkStatusData = rs.getInt("COUNTSTATUS") > 0;
                }
                //</editor-fold>

                if (checkStatusData) {

                    //<editor-fold defaultstate="collapsed" desc="insert to TaxForm02 status log ">
                    String sqlInsert = "INSERT INTO TAX_FORM02_STATUS_LOG ( "
                            + "  TAX_FORM02_ID "
                            + ", STATUS "
                            + ", STATUS_NAME "
                            + ", REMARK "
                            + ", ACTION_DATE "
                            + ", ACTION_BY "
                            + ") VALUES(?,?,?,?,SYSDATE,?) ";
                    i = 1;
                    ps = conn.prepareStatement(sqlInsert);
                    ps.setInt(i++, id);
                    ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
                    ps.setString(i++, StatusEnum.NoPass.displayNameTH());
                    ps.setString(i++, data.getRemark());

                    if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                        ps.setString(i++, data.getActionBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }
                    ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="update TaxForm02 status ">
                    String sqlUpdate = " UPDATE TAX_FORM02 SET "
                            + " STATUS = ? "
                            + ", STATUS_NAME = ? "
                            + ", UPDATED_DATE = SYSDATE "
                            + ", UPDATED_BY = ? "
                            + " WHERE TAX_FORM02_ID = ? ";
                    i = 1;
                    ps = conn.prepareStatement(sqlUpdate);
                    ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
                    ps.setString(i++, StatusEnum.NoPass.displayNameTH());
                    if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                        ps.setString(i++, data.getActionBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }
                    ps.setInt(i++, id);

                    ps.executeUpdate();
                    //</editor-fold>

                } else {
                    //status not update
                }

            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData sendApproveTaxForm03(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {
                int i = 1;

                int id = AppUtil.decryptId(idString);

                String sqlInsert = "INSERT INTO TAX_FORM03_STATUS_LOG ( "
                        + "  TAX_FORM03_ID "
                        + ", STATUS "
                        + ", STATUS_NAME "
                        + ", REMARK "
                        + ", ACTION_DATE "
                        + ", ACTION_BY "
                        + ") VALUES(?,?,?,?,SYSDATE,?) ";
                i = 1;
                ps = conn.prepareStatement(sqlInsert);
                ps.setInt(i++, id);
                ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));
                ps.setString(i++, StatusEnum.Wait.displayNameTH());
                ps.setString(i++, data.getRemark());

                if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                    ps.setString(i++, data.getActionBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                ps.executeUpdate();

                //<editor-fold defaultstate="collapsed" desc="update  tatus ">
                String sqlUpdate = " UPDATE TAX_FORM03 SET "
                        + " STATUS = ? "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.Wait.value()));
                ps.setInt(i++, id);

                ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData approveTaxForm03(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {
                int i = 1;

                int id = AppUtil.decryptId(idString);

                String sqlInsert = "INSERT INTO TAX_FORM03_STATUS_LOG ( "
                        + "  TAX_FORM03_ID "
                        + ", STATUS "
                        + ", STATUS_NAME "
                        + ", REMARK "
                        + ", ACTION_DATE "
                        + ", ACTION_BY "
                        + ") VALUES(?,?,?,?,SYSDATE,?) ";
                i = 1;
                ps = conn.prepareStatement(sqlInsert);
                ps.setInt(i++, id);
                ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                ps.setString(i++, StatusEnum.Pass.displayNameTH());
                ps.setString(i++, data.getRemark());

                if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                    ps.setString(i++, data.getActionBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                ps.executeUpdate();

                //<editor-fold defaultstate="collapsed" desc="update  tatus ">
                String sqlUpdate = " UPDATE TAX_FORM03 SET "
                        + " STATUS = ? "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.Pass.value()));
                ps.setInt(i++, id);

                ps.executeUpdate();
                //</editor-fold>
            }
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData rejectTaxForm03(StatusLogViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (String idString : data.getIdList()) {
                int i = 1;

                int id = AppUtil.decryptId(idString);

                String sqlInsert = "INSERT INTO TAX_FORM03_STATUS_LOG ( "
                        + "  TAX_FORM03_ID "
                        + ", STATUS "
                        + ", STATUS_NAME "
                        + ", REMARK "
                        + ", ACTION_DATE "
                        + ", ACTION_BY "
                        + ") VALUES(?,?,?,?,SYSDATE,?) ";
                i = 1;
                ps = conn.prepareStatement(sqlInsert);
                ps.setInt(i++, id);
                ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
                ps.setString(i++, StatusEnum.NoPass.displayNameTH());
                ps.setString(i++, data.getRemark());

                if (!AppUtil.isNullAndSpace(data.getActionBy())) {
                    ps.setString(i++, data.getActionBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                ps.executeUpdate();

                //<editor-fold defaultstate="collapsed" desc="update  tatus ">
                String sqlUpdate = " UPDATE TAX_FORM03 SET "
                        + " STATUS = ? "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps = conn.prepareStatement(sqlUpdate);
                ps.setString(i++, String.valueOf(StatusEnum.NoPass.value()));
                ps.setInt(i++, id);

                ps.executeUpdate();
                //</editor-fold>
            }
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }
}
