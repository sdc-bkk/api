/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import utility.ConnnectionDBBma;

/**
 *
 * @author Sirichai
 */
public class MemberBMARepo {

    private final ConnnectionDBBma connDB = new ConnnectionDBBma();

    public boolean updateTaxNo(String userName, String taxNo) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="set password Member">
            sql = "UPDATE MEMBER SET "
                    + "  TAX_NO = ? "
                    + ", UPDATED_DATE = SYSDATE "
                    + " WHERE (USER_NAME = ?) AND FROM_OILTAX = 1";

            ps = conn.prepareStatement(sql);

            //Set Parameter
            ps.setString(i++, taxNo.trim());
            ps.setString(i++, userName.trim());

            boolean resultUpdate = ps.executeUpdate() != 0;

            if (resultUpdate == false) {
                throw new Exception();
            }

            //</editor-fold>
            conn.commit();
            result = true;

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();

        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }

}
