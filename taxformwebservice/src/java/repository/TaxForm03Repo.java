/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import enumeration.SettingGeneralEnum;
import enumeration.StatusEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.AddressModel;
import model.TaxForm03DetailModel;
import model.TaxForm03FileModel;
import model.TaxForm03Model;
import model.TaxForm03SummaryModel;
import repository.mapper.TaxForm03Mapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.FeeViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.MonthlyViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.StatusAccountViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm03DetailViewModel;
import viewModel.TaxForm03FileViewModel;
import viewModel.TaxForm03SummaryViewModel;
import viewModel.TaxForm03ViewModel;

/**
 *
 * @author User
 */
public class TaxForm03Repo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<MonthlyViewModel>> getList(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<MonthlyViewModel>> resultData = new ResultData<List<MonthlyViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT YEARLY, MONTHLY\n"
                    + ",SUM(WAIT_SEND_NUM) WAIT_SEND_NUM"
                    + ",SUM(WAIT_NUM) WAIT_NUM\n"
                    + ",SUM(NOPASS_NUM) NOPASS_NUM\n"
                    + ",SUM(PASS_NUM) PASS_NUM\n"
                    + "FROM (\n"
                    + "SELECT YEARLY, MONTHLY ,REF_RETAIL_ID\n"
                    + ", CASE WHEN STATUS = 0 THEN 1 ELSE 0 END WAIT_SEND_NUM"
                    + ", CASE WHEN STATUS = 1 THEN 1 ELSE 0 END WAIT_NUM\n"
                    + ", CASE WHEN STATUS = 2 THEN 1 ELSE 0 END NOPASS_NUM\n"
                    + ", CASE WHEN STATUS = 3 THEN 1 ELSE 0 END PASS_NUM\n"
                    + "FROM TAX_FORM03\n"
                    + ")FORM03 LEFT JOIN RETAIL ON FORM03.REF_RETAIL_ID = RETAIL.RETAIL_ID\n"
                    + " WHERE (1=1) ";

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                sql += " AND (YEARLY = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                sql += " AND (MONTHLY = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (RETAIL.TAX_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                sql += " AND (AMPHUR_NAME = ?) ";
            }

            sql += " GROUP BY YEARLY, MONTHLY\n ";

            ps.setSql(sql);

            String orderBy = " YEARLY DESC, MONTHLY DESC  ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                ps.setString(i++, filter.getYearly());
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                ps.setString(i++, filter.getMonthly());
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                ps.setString(i++, filter.getAmphurId());
            }

            rs = ps.executeQuery();

            List<MonthlyViewModel> dataList = null;
            dataList = new TaxForm03Mapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<TaxForm03ViewModel>> getListByMonthly(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<TaxForm03ViewModel>> resultData = new ResultData<List<TaxForm03ViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT TAX_FORM03.* ,RETAIL_STATION.STATION_NAME  "
                    + "   FROM TAX_FORM03 "
                    + "   LEFT JOIN RETAIL ON TAX_FORM03.REF_RETAIL_ID = RETAIL.RETAIL_ID "
                    + "   LEFT JOIN RETAIL_STATION ON RETAIL_STATION.RETAIL_STATION_ID = TAX_FORM03.REF_STATION_ID "
                    //                    + "   LEFT JOIN M_TAMBON ON RETAIL_STATION.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + "   WHERE (1=1) ";

            if ("back".equals(filter.getForType())) {
                sql += " AND TAX_FORM03.STATUS NOT IN( 0 ) ";
            }
            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                sql += " AND (YEARLY = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                sql += " AND (MONTHLY = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (RETAIL.TAX_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(DOC_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(DOC_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (DOC_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (RETAIL_STATION.STATION_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND (TAX_FORM03.STATUS = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStatusAcc())) {
                sql += " AND (TAX_FORM03.STATUS_ACC = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                sql += " AND (M_TAMBON.DISTRICT_THAI = ?) ";
            }

            ps.setSql(sql);
            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("createdDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                ps.setString(i++, filter.getYearly());
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                ps.setString(i++, filter.getMonthly());
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, filter.getDocNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }

            if (!AppUtil.isNullAndSpace(filter.getStatusAcc())) {
                ps.setString(i++, filter.getStatusAcc());
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                ps.setString(i++, filter.getAmphurName());
            }

            rs = ps.executeQuery();

            List<TaxForm03ViewModel> dataList = null;
            dataList = new TaxForm03Mapper(rs).mapListTaxForm03();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " createdDate ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("stationName")) {
                str = " RETAIL_STATION.STATION_NAME ";
            } else if (orderBy.equalsIgnoreCase("createdDate")) {
                str = " CREATED_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " CREATED_DATE ";
            }
        }

        return str;

    }

    public ResultData<TaxForm03ViewModel> getData(int id) throws SQLException {

        ResultData<TaxForm03ViewModel> resultData = new ResultData<TaxForm03ViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get taxform03 ">
            String sql = " SELECT * "
                    + " FROM TAX_FORM03  "
                    + " WHERE TAX_FORM03_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            TaxForm03ViewModel data = new TaxForm03Mapper(rs).mapData();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail accountFile ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM03_FILE  "
                    + " WHERE TAX_FORM03_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Account.display());

            rs = ps.executeQuery();

            List<TaxForm03FileViewModel> dataFile = new TaxForm03Mapper(rs).mapDataFile();
            data.setAccountList(dataFile);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get summaryList ">
            sql = " SELECT * "
                    + " FROM TAX_FORM03_SUMMARY  "
                    + " WHERE TAX_FORM03_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<TaxForm03SummaryViewModel> dataSum = new TaxForm03Mapper(rs).mapDataSummary();
            data.setSummaryList(dataSum);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get detailList ">
            sql = " SELECT * "
                    + " FROM TAX_FORM03_DETAIL  "
                    + " WHERE TAX_FORM03_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<TaxForm03DetailViewModel> dataDetail = new TaxForm03Mapper(rs).mapDataDetail();
            data.setDetailList(dataDetail);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }


    public FeeViewModel getDataFeeCurrent() throws SQLException {

        FeeViewModel data = new FeeViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_FEE "
                    + "WHERE TO_DATE(SYSDATE, 'DD/MM/YYYY')  "
                    + "BETWEEN TO_DATE(START_DATE, 'DD/MM/YYYY') AND "
                    + "TO_DATE(END_DATE, 'DD/MM/YYYY') ";

            ps.setSql(sql);

            rs = ps.executeQuery();

            while (rs.next()) {

                data.setFeeId(AppUtil.encryptId(rs.getInt("FEE_ID")));
                data.setFeeName(rs.getString("FEE_NAME"));
                data.setFeeValue(rs.getFloat("FEE_VALUE"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }
                data.setIsDefault(rs.getBoolean("IS_DEFAULT"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setHasHoliday(rs.getBoolean("HAS_HOLIDAY"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }

    public boolean checkHoliday(String holidayDate) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {
            String tempDate = "";
            if (!AppUtil.isNullAndSpace(holidayDate)) {
                tempDate = DateUtils.toEng(DateUtils.toEng(holidayDate), "YYYY-MM-dd");
            }

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA FROM M_HOLIDAYS "
                    + "WHERE (TRUNC(START_DATE) <= TO_DATE(?, 'YYYY-MM-dd')  "
                    + "AND TRUNC(END_DATE) >= TO_DATE(?, 'YYYY-MM-dd')) "
                    + "OR (TO_CHAR (TO_DATE(?, 'YYYY-MM-dd') , 'DY', 'NLS_DATE_LANGUAGE=ENGLISH') IN ('SAT', 'SUN'))  ";

            ps.setSql(sql);

            int i = 1;
            if (!AppUtil.isNullAndSpace(tempDate)) {
                ps.setString(i++, tempDate);
                ps.setString(i++, tempDate);
                ps.setString(i++, tempDate);
            }

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData save(TaxForm03Model data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="tax_form03">
            i = 1;
            if (data.getTaxForm03Id() == 0) {

                sql = "INSERT INTO TAX_FORM03 ( "
                        + "  REF_RETAIL_ID "
                        + ", REF_STATION_ID "
                        + ", YEARLY "
                        + ", MONTHLY "
                        + ", MONTH_NAME "
                        + ", DOC_NO "
                        + ", DOC_DATE "
                        + ", STATUS "
                        + ", STATUS_ACC "
                        + ", STATUS_ACC_DESC "
                        + ", ADD_TIMES "
                        + ", SEQUENCE "
                        + ", EXTRA_MONTH "
                        + ", EXTRA_MONEY "
                        + ", EXTRA_RATE "
                        + ", EXTRA_START_DATE "
                        + ", EXTRA_END_DATE "
                        + ", TAX_TOTAL "
                        + ", REF_OFFICE "
                        + ", REF_OFFICE_NAME "
                        + ", REF_OFFICE_CODE "
                        + ", DUE_DATE "
                        + ", IS_ACTIVE"
                        + ", IS_PAYMENT"
                        + ", CREATED_DATE "
                        + ", CREATED_BY "
                        + ") VALUES(?,?  ,?,?,?  ,?,SYSDATE   ,?,?,?,?,?,?  ,?  ,?,?,?,?,?,?  ,?,?   ,?,0,SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"TAX_FORM03_ID"});

            } else {

                sql = "UPDATE TAX_FORM03 SET "
                        + "  REF_RETAIL_ID = ? "
                        + ", REF_STATION_ID = ? "
                        + ", YEARLY  = ?"
                        + ", MONTHLY  = ?"
                        + ", MONTH_NAME = ? "
                        + ", DOC_NO = ? "
                        //                        + ", DOC_DATE = ? "
                        + ", STATUS = ? "
                        + ", STATUS_ACC = ? "
                        + ", STATUS_ACC_DESC = ? "
                        + ", ADD_TIMES = ? "
                        + ", SEQUENCE = ? "
                        + ", EXTRA_MONTH = ?  "
                        + ", EXTRA_MONEY = ? "
                        + ", EXTRA_RATE = ? "
                        + ", EXTRA_START_DATE = ? "
                        + ", EXTRA_END_DATE = ? "
                        + ", TAX_TOTAL = ? "
                        + ", REF_OFFICE = ? "
                        + ", REF_OFFICE_NAME = ? "
                        + ", REF_OFFICE_CODE = ? "
                        + ", DUE_DATE  = ?  "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE TAX_FORM03_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setInt(i++, data.getRefRetailId());
            ps.setInt(i++, data.getRefStationId());
            ps.setInt(i++, data.getYearly());
            ps.setInt(i++, data.getMonthly());
            ps.setString(i++, data.getMonthName());
            if (data.getTaxForm03Id() == 0) {
                ps.setString(i++, AppUtil.getRunningDoc("taxform03", data.getRefOfficeCode()));
                ps.setString(i++, String.valueOf(StatusEnum.Create.value()));
            } else {
                ps.setString(i++, data.getDocNo());
                ps.setString(i++, String.valueOf(data.getStatus()));
            }

            ps.setString(i++, data.getStatusAcc());
            ps.setString(i++, data.getStatusAccDesc());
            ps.setInt(i++, data.getAddTimes());
            ps.setInt(i++, data.getSequence());
            ps.setInt(i++, data.getExtraMonth());
            ps.setDouble(i++, data.getExtraMoney());
            ps.setDouble(i++, data.getExtraRate());
            ps.setDate(i++, AppUtil.toDateSql(data.getExtraStartDate()));
            ps.setDate(i++, AppUtil.toDateSql(data.getDueDate()));
            ps.setDouble(i++, data.getTaxTotal());

            ps.setString(i++, data.getRefOffice());
            ps.setString(i++, data.getRefOfficeName());
            ps.setString(i++, data.getRefOfficeCode());
            ps.setDate(i++, AppUtil.toDateSql(data.getDueDate()));

            if (data.getTaxForm03Id() == 0) {
                ps.setBoolean(i++, true);
            } else {
                ps.setBoolean(i++, true);
//                ps.setBoolean(i++, data.getActive());
            }

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getTaxForm03Id() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setTaxForm03Id(id);
            } else {

                ps.setInt(i++, data.getTaxForm03Id());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT TAXFORM03_FILE">
            if (data.getTaxForm03Id() != 0) {

                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM TAX_FORM03_FILE WHERE TAX_FORM03_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm03Id());

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT Account">
                if (data.getAccountList() != null && !data.getAccountList().isEmpty()) {
                    for (TaxForm03FileModel file : data.getAccountList()) {
                        if (data.getTaxForm03Id() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM03_FILE("
                                    + " TAX_FORM03_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_FILE"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm03Id());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT SUMMARYLIST">
            if (data.getTaxForm03Id() != 0) {
                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM TAX_FORM03_SUMMARY WHERE TAX_FORM03_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm03Id());

                ps.executeUpdate();
                //</editor-fold>

                if (data.getSummaryList() != null && !data.getSummaryList().isEmpty()) {
                    for (TaxForm03SummaryModel child : data.getSummaryList()) {
                        i = 1;

                        sql = "INSERT INTO TAX_FORM03_SUMMARY("
                                + " TAX_FORM03_ID"
                                + ",SEQUENCE "
                                + ",OIL_TYPE_ID"
                                + ",OIL_TYPE_NAME"
                                + ",QTY"
                                + ",RATE"
                                + ",AMOUNT"
                                + ",AMOUNT_BAHT"
                                + ",AMOUNT_SATANG"
                                + ") VALUES(?  ,?,?,?  ,?,?  ,?,?,?)";

                        ps = conn.prepareStatement(sql);

                        ps.setInt(i++, data.getTaxForm03Id());
                        ps.setInt(i++, child.getSequence());
                        ps.setInt(i++, child.getOilTypeId());
                        ps.setString(i++, child.getOilTypeName());
                        ps.setDouble(i++, child.getQty());
                        ps.setDouble(i++, child.getRate());
                        ps.setDouble(i++, child.getAmount());
                        ps.setInt(i++, child.getAmountBaht());
                        ps.setInt(i++, child.getAmountSatang());

                        result = ps.executeUpdate() != 0;
                    }
                }
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT DETAILLIST">
            if (data.getTaxForm03Id() != 0) {

                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM TAX_FORM03_DETAIL WHERE TAX_FORM03_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm03Id());

                ps.executeUpdate();
                //</editor-fold>

                if (data.getDetailList() != null && !data.getDetailList().isEmpty()) {
                    for (TaxForm03DetailModel child : data.getDetailList()) {
                        i = 1;

                        sql = "INSERT INTO TAX_FORM03_DETAIL("
                                + " TAX_FORM03_ID"
                                + ",SEQUENCE "
                                + ",OIL_TYPE_ID"
                                + ",OIL_TYPE_NAME"
                                + ",BALANCE"
                                + ",IN_QTY"
                                + ",OUT_QTY"
                                + ",REMAIN"
                                + ") VALUES(?  ,?,?,?  ,?,?,?,?)";

                        ps = conn.prepareStatement(sql);

                        ps.setInt(i++, data.getTaxForm03Id());
                        ps.setInt(i++, child.getSequence());
                        ps.setInt(i++, child.getOilTypeId());
                        ps.setString(i++, child.getOilTypeName());
                        ps.setDouble(i++, child.getBalance());
                        ps.setDouble(i++, child.getInQty());
                        ps.setDouble(i++, child.getOutQty());
                        ps.setDouble(i++, child.getRemain());

                        result = ps.executeUpdate() != 0;
                    }
                }
            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData deleteData(List<Integer> idList, FilterTaxFormModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id list">
            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM03_STATION_LOG">
                sql = " DELETE FROM TAX_FORM03_STATUS_LOG "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM03_FILE">
                sql = " DELETE FROM TAX_FORM03_FILE "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM03_SUMMARY">
                sql = " DELETE FROM TAX_FORM03_SUMMARY "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM03_DETAIL">
                sql = " DELETE FROM TAX_FORM03_DETAIL "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM03">
                sql = " DELETE FROM TAX_FORM03 "
                        + " WHERE TAX_FORM03_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>
            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    public Integer checkCount03(StatusAccountViewModel filter) throws SQLException {

        Integer countData = 0;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT COUNT(TAX_FORM03_ID) COUNTDATA "
                    + " FROM TAX_FORM03  "
                    + " WHERE REF_STATION_ID  =? AND STATUS IN (1,2,3) "//
                    + " AND MONTHLY = ? AND YEARLY= ? ";

            ps.setSql(sql);

            ps.setInt(1, AppUtil.decryptId(filter.getRefRetailStationId()));
            ps.setInt(2, filter.getMonthly());
            ps.setInt(3, filter.getYearly());

            rs = ps.executeQuery();

            while (rs.next()) {
                countData = rs.getInt("COUNTDATA");
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return countData;

    }

    public ResultData<List<StatusLogViewModel>> getListStatusLog(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<StatusLogViewModel>> resultData = new ResultData<List<StatusLogViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * "
                    + " FROM TAX_FORM03_STATUS_LOG  "
                    + " WHERE TAX_FORM03_ID = ?  ORDER BY ACTION_DATE DESC ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getTaxForm03Id()));

            if (page != null) {
                ps.setResultPage(page);
            }

            rs = ps.executeQuery();

            List<StatusLogViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
                StatusLogViewModel data = new StatusLogViewModel();
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setActionBy(AppUtil.checkNullData(rs.getString("ACTION_BY")));
                data.setRemark(AppUtil.checkNullData(rs.getString("REMARK")));

                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

}
