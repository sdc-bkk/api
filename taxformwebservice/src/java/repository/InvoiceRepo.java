/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AppConfigEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.InvoiceModel;
import utility.AppConfig;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;

/**
 *
 * @author User
 */
public class InvoiceRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();
    AppConfigRepo appConfigRepo = new AppConfigRepo();

    public ResultData saveInvoice(InvoiceModel data) throws SQLException {

        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        boolean result = false;

        try {
            //get taxtype
            String taxType = appConfigRepo.GetValue(AppConfigEnum.TaxType.displayNameTH());
            String taxTypeName = appConfigRepo.GetValue(AppConfigEnum.TaxTypeName.displayNameTH());

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Delete by id ">
            sql = " DELETE FROM INVOICE "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_TYPE_CODE = ? AND PAYMENT_STATUS = 0 ";

            i = 1;
            ps = conn.prepareStatement(sql);

            ps.setString(1, data.getRef1());
            ps.setString(2, data.getRef2());
            ps.setString(3, taxType);

            ps.executeUpdate();
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="invoice">
            if (data.getInvoiceId() == 0) {

                sql = "INSERT INTO INVOICE ( "
                        + "  BILL_NO "
                        + ", REF1 "
                        + ", REF2 "
                        + ", TAX_NO "
                        + ", CUS_NAME "
                        + ", CUS_ADDRESS "
                        
                        + ", DUE_DATE "
                        + ", DETAIL1 "
                        + ", AMOUNT1 "
                        + ", DETAIL2 "
                        + ", AMOUNT2 "
                        
                        + ", DETAIL3 "
                        + ", AMOUNT3 "
                        + ", DETAIL4 "
                        + ", AMOUNT4 "
                        + ", DETAIL5 "
                        
                        + ", AMOUNT5 "
                        + ", TOTAL_AMOUNT "
                        + ", OFFICE "
                        + ", PART_OF_OFFICE "
                        + ", TELEPHONE"
                        
                        + ", TAX_TYPE_CODE "
                        + ", TAX_TYPE_NAME "
                        + ", TAX_FORM03_ID "
                        + ", ACTION_DATE "
                        + ", PAYMENT_STATUS "
                        
                        + ", BARCODE "
                        + ", QRCODE "
                        + ", IS_CANCEL "
                        + ", BMA_BILL_NO "
                        + ") VALUES(?,?,?,?,?,?  ,?,?,?,?,?  ,?,?,?,?,?  ,?,?,?,?,?  ,?,?,?,SYSDATE,0  ,?,?,0,?) ";

                ps = conn.prepareStatement(sql, new String[]{"INVOICE_ID"});

            } else {

                sql = "UPDATE INVOICE SET "
                        + "  BILL_NO = ? "
                        + ", REF1 = ? "
                        + ", REF2 = ? "
                        + ", TAX_NO = ? "
                        + ", CUS_NAME = ? "
                        + ", CUS_ADDRESS = ? "
                        + ", DUE_DATE = ?"
                        + ", DETAIL1 = ? "
                        + ", AMOUNT1 = ? "
                        + ", DETAIL2 = ? "
                        + ", AMOUNT2 = ? "
                        + ", DETAIL3 = ? "
                        + ", AMOUNT3 = ? "
                        + ", DETAIL4 = ? "
                        + ", AMOUNT4 = ? "
                        + ", DETAIL5 = ? "
                        + ", AMOUNT5 = ? "
                        + ", TOTAL_AMOUNT = ? "
                        + ", OFFICE = ? "
                        + ", PART_OF_OFFICE = ? "
                        + ", TELEPHONE = ?"
                        + ", TAX_TYPE_CODE = ? "
                        + ", TAX_TYPE_NAME = ? "
                        + ", TAX_FORM03_ID = ?"
                        + ", ACTION_DATE = SYSDATE "
                        + ", BARCODE  = ?"
                        + ", QRCODE  = ?"
                        + " WHERE INVOICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getBillNo());
            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());
            ps.setString(i++, data.getTaxNo());
            ps.setString(i++, data.getCusName());
            ps.setString(i++, data.getCusAddress().trim());
            ps.setDate(i++, AppUtil.toDateSql(data.getDueDate()));

            ps.setString(i++, data.getDetail1());
            ps.setDouble(i++, ("".equals(AppUtil.checkNullData(data.getAmount1()))) ? 0 : Double.parseDouble(data.getAmount1()));
            ps.setString(i++, data.getDetail2());
            ps.setDouble(i++, ("".equals(AppUtil.checkNullData(data.getAmount2()))) ? 0 : Double.parseDouble(data.getAmount2()));
            ps.setString(i++, data.getDetail3());
            ps.setDouble(i++, ("".equals(AppUtil.checkNullData(data.getAmount3()))) ? 0 : Double.parseDouble(data.getAmount3()));
            ps.setString(i++, data.getDetail4());
            ps.setDouble(i++, ("".equals(AppUtil.checkNullData(data.getAmount4()))) ? 0 : Double.parseDouble(data.getAmount4()));
            ps.setString(i++, data.getDetail5());
            ps.setDouble(i++, ("".equals(AppUtil.checkNullData(data.getAmount5()))) ? 0 : Double.parseDouble(data.getAmount5()));
            ps.setDouble(i++, ("".equals(AppUtil.checkNullData(data.getTotalAmount()))) ? 0 : Double.parseDouble(data.getTotalAmount()));

            ps.setString(i++, data.getOffice());
            ps.setString(i++, data.getPartOfOffice());
            ps.setString(i++, data.getTelephone());

            ps.setString(i++, taxType);
            ps.setString(i++, taxTypeName);

            ps.setInt(i++, data.getTaxform03Id());

            ps.setString(i++, data.getBarcode());
            ps.setString(i++, data.getQrcode());

            if (data.getInvoiceId() == 0) {
                ps.setString(i++, data.getBmaBillNo());

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setInvoiceId(id);
            } else {

                ps.setInt(i++, data.getInvoiceId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

}
