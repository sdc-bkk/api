/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.StatusWarnformEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import repository.mapper.TaxForm01Mapper;
import repository.mapper.TaxForm03Mapper;
import repository.mapper.WarnFormMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.DateUtil;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.FilterModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm03ViewModel;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class WarnFormRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<WarnFormViewModel>> getList(ResultPage page, FilterModel filter) throws SQLException {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "select to_char( add_months( start_date, level-1 ), 'mm' ) MONTHNAME\n"
                    + "        ,to_char( add_months( start_date, level-1 ), 'yyyy' ) YEARNAME\n"
                    + "      from (select add_months(sysdate,-36) start_date, sysdate end_date from dual)\n"
                    + "     connect by level <= months_between( trunc(end_date,'MM'), trunc(start_date,'MM') ) * + 1 ";

            ps.setSql(sql);

            String orderBy = " YEARNAME DESC, MONTHNAME DESC  ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            rs = ps.executeQuery();

            List<WarnFormViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
                WarnFormViewModel data = new WarnFormViewModel();
                data.setMonthly(rs.getInt("MONTHNAME"));
                data.setMonthName(DateUtil.convertMonthToThMonth(rs.getInt("MONTHNAME")));
                data.setYearly(DateUtil.toYearTHString(rs.getString("YEARNAME")));
                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<WarnFormViewModel>> getListByMonthly(ResultPage page, FilterModel filter) throws SQLException {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT DISTINCT S.RETAIL_STATION_ID,RETAIL.OWNER_NAME , RETAIL.TAX_NO , S.STATION_NAME, M_TAMBON.DISTRICT_THAI_SHORT \n"
                    + ",MAX(F.YEARLY) MAX_YEARLY,MAX(F.MONTHLY) MAX_MONTHLY\n"
                    + "FROM RETAIL_STATION S\n"
                    + "LEFT JOIN RETAIL ON S.RETAIL_ID = RETAIL.RETAIL_ID\n"
                    + "LEFT JOIN M_TAMBON ON S.TAMBON_ID = M_TAMBON.TAMBON_ID \n"
                    + "LEFT JOIN TAX_FORM03 F ON S.RETAIL_STATION_ID = F.REF_STATION_ID AND (F.MONTHLY || F.YEARLY) = ? \n"
                    + "WHERE S.IS_ACTIVE =1 \n"
                    + "AND (TO_CHAR(S.CREATED_DATE, 'mm' ) <= ? and TO_CHAR(S.CREATED_DATE, 'yyyy' ) <= ?)\n"
                    + "AND (F.TAX_FORM03_ID IS null )\n"
                    + "GROUP BY S.RETAIL_STATION_ID, RETAIL.OWNER_NAME , RETAIL.TAX_NO \n"
                    + ", S.STATION_NAME, M_TAMBON.DISTRICT_THAI_SHORT ";

            ps.setSql(sql);

            String orderBy = " OWNER_NAME  ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            ps.setString(i++, (filter.getMonthly() + filter.getYearly()));
            ps.setInt(i++, Integer.parseInt(filter.getMonthly()));
            ps.setInt(i++, Integer.parseInt(DateUtil.toYearENString(filter.getYearly())));

            rs = ps.executeQuery();

            List<WarnFormViewModel> dataList = new ArrayList<>();
            dataList = new WarnFormMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<WarnFormViewModel>> getListLog(ResultPage page, FilterModel filter) throws SQLException {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "select * FROM WARN_FORM WHERE RETAIL_STATION_ID = ? ";

            ps.setSql(sql);

            int id = AppUtil.decryptId(filter.getStationId());
            ps.setInt(1, id);

            String orderBy = " CREATED_DATE DESC  ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            rs = ps.executeQuery();

            List<WarnFormViewModel> dataList = new ArrayList<>();
            dataList = new WarnFormMapper(rs).mapListLog();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public List<WarnFormViewModel> getDataWarnByStationId(int id) throws SQLException {

        Integer countStation = 0;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        List<WarnFormViewModel> resultList = new ArrayList<>();
        try {

            ps.setAutoCommit(false);

            String sql = " SELECT DISTINCT RETAIL.TAX_NO , RETAIL.OWNER_NAME , RETAIL_STATION.STATION_NAME ,WARN_FORM.YEARLY,WARN_FORM.MONTHLY  \n"
                    + "FROM  WARN_FORM\n"
                    + "LEFT JOIN RETAIL_STATION ON WARN_FORM.RETAIL_STATION_ID = RETAIL_STATION.RETAIL_STATION_ID\n"
                    + "LEFT JOIN RETAIL ON RETAIL_STATION.RETAIL_ID = RETAIL.RETAIL_ID\n"
                    + "WHERE RETAIL.TAX_NO IS NOT NULL AND WARN_FORM_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();
            String taxNo = "";
            String ownerName = "";
            String stationName = "";
            String yearly = "";
            int monthly = 0;
            String monthName = "";
            while (rs.next()) {
                taxNo = rs.getString("TAX_NO");
                ownerName = rs.getString("OWNER_NAME");
                stationName = rs.getString("STATION_NAME");
                yearly = rs.getString("YEARLY");
                monthly = rs.getInt("MONTHLY");
                monthName = (AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));
            }

            sql = " SELECT DISTINCT EMAIL FROM MEMBER_OF_RETAIL\n"
                    + "WHERE TAX_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, taxNo);

            rs = ps.executeQuery();
            while (rs.next()) {
                WarnFormViewModel model = new WarnFormViewModel();
                model.setEmail(rs.getString("EMAIL"));
                model.setMonthly(monthly);
                model.setYearly(yearly);
                model.setMonthName(monthName);
                model.setOwnerName(ownerName);
                model.setStationName(stationName);
                resultList.add(model);
            }

        } catch (Exception e) {

        }
        return resultList;

    }

    public ResultData saveData(WarnFormViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql WARN_FORM">
            sql = "INSERT INTO WARN_FORM ( "
                    + "  RETAIL_STATION_ID "
                    + ", DOC_NO1 "
                    + ", DOC_NO2 "
                    + ", OWNER_NAME "
                    + ", STATION_NAME "
                    + ", YEARLY "
                    + ", MONTHLY "
                    + ", DOC_DATE "
                    + ", STATUS "
                    + ", STATUS_NAME "
                    + ", ORG_NAME "
                    + ", ORG_ADDR "
                    + ", ORG_OWNER_NAME "
                    + ", ORG_OWNER_TEL "
                    + ", ORG_OWNER_FAX "
                    + ", CREATED_DATE "
                    + ", CREATED_BY "
                    + ") VALUES (?,?,?,?,?,?,?  ,SYSDATE,?,?,?,?  ,?,?,?, SYSDATE,?) ";

            ps = conn.prepareStatement(sql, new String[]{"WARN_FORM_ID"});
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setInt(i++, AppUtil.decryptId(data.getStationId()));
            ps.setString(i++, data.getDocNo1());
            ps.setString(i++, data.getDocNo2());
            ps.setString(i++, data.getOwnerName());
            ps.setString(i++, data.getStationName());
            ps.setString(i++, data.getYearly());
            ps.setInt(i++, data.getMonthly());
            ps.setInt(i++, StatusWarnformEnum.Create.value());
            ps.setString(i++, StatusWarnformEnum.Create.displayNameTH());
            
            ps.setString(i++, data.getOrgName());
            ps.setString(i++, data.getOrgAddr());
            ps.setString(i++, data.getOrgOwnerName());
            ps.setString(i++, data.getOrgOwnerTel());
            ps.setString(i++, data.getOrgOwnerFax());

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            result = ps.executeUpdate() != 0;

            ResultSet rs = ps.getGeneratedKeys();
            int id = 0;
            while (rs.next()) {
                id = rs.getInt(1);
            }

            if (id <= 0) {
                throw new Exception();
            }

            data.setWarnFormId(AppUtil.encrypt(Integer.toString(id)));
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData deleteData(List<Integer> idList, FilterModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id list">
            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="delete ">
                sql = " DELETE FROM WARN_FORM  "
                        + " WHERE WARN_FORM_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();

                //</editor-fold>
            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    public ResultData updateStatus(FilterModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="delete ">
            sql = " UPDATE WARN_FORM  SET STATUS = ? ,STATUS_NAME = ? , SEND_MAIL_DATE = SYSDATE "
                    + " WHERE WARN_FORM_ID = ? ";
            i = 1;
            ps.setSql(sql);

            ps.setInt(i++, filter.getStatus());
            ps.setString(i++, filter.getStatusName());
            ps.setInt(i++, AppUtil.decryptId(filter.getWarnFormId()));

            result = ps.executeUpdate();

            //</editor-fold>
            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    private String getColumnDB(String orderBy) {

        String str = " TAX_FORM01_RETAIL_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("ownerName")) {
                str = " OWNER_NAME ";
            } else if (orderBy.equalsIgnoreCase("amphurName")) {
                str = " AMPHUR_NAME ";
            } else if (orderBy.equalsIgnoreCase("createdDate")) {
                str = " CREATED_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " CREATED_DATE ";
            }
        }

        return str;

    }

}
