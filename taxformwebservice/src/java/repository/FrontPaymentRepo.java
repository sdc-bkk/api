/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import repository.mapper.FrontPaymentMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.FilterModel;
import viewModel.FrontInvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class FrontPaymentRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<FrontInvoiceViewModel>> getList(ResultPage page, FilterModel filter) throws SQLException {

        ResultData<List<FrontInvoiceViewModel>> resultData = new ResultData<List<FrontInvoiceViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT I.INVOICE_ID, I.TAX_NO, I.DUE_DATE, I.TOTAL_AMOUNT, I.PAYMENT_STATUS, "
                        + "    I.TAX_FORM03_ID, F.TAX_TOTAL, F.EXTRA_MONEY, F.MONTHLY, F.YEARLY, "
                        + "    F.REF_STATION_ID, S.STATION_NAME "
                        + " FROM INVOICE I "
                        + "    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID "
                        + "    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID "
                        + " WHERE I.PAYMENT_STATUS = 0 ";
/*
 SELECT I.INVOICE_ID, I.TAX_NO, I.DUE_DATE, I.TOTAL_AMOUNT, I.PAYMENT_STATUS, 
    I.TAX_FORM03_ID, F.TAX_TOTAL, F.EXTRA_MONEY, F.MONTHLY, F.YEARLY, 
    F.REF_STATION_ID, S.STATION_NAME 
 FROM INVOICE I 
    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID 
    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID 
 WHERE I.PAYMENT_STATUS = 0 AND I.TAX_NO = ''
*/
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (I.TAX_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (S.STATION_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                sql += " AND (F.YEARLY = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                sql += " AND (F.MONTHLY = ?) ";
            }

            ps.setSql(sql);

            String orderBy = " YEARLY DESC, MONTHLY DESC ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                ps.setString(i++, filter.getYearly());
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                ps.setString(i++, filter.getMonthly());
            }

            rs = ps.executeQuery();

            List<FrontInvoiceViewModel> dataList = null;
            dataList = new FrontPaymentMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<FrontInvoiceViewModel>> getListHistory(ResultPage page, FilterModel filter) throws SQLException {

        ResultData<List<FrontInvoiceViewModel>> resultData = new ResultData<List<FrontInvoiceViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT I.INVOICE_ID, I.TAX_NO, I.PAYMENT_DATE, I.PAYMENT_TIME, I.TOTAL_AMOUNT, "
                        + "    I.PAYMENT_STATUS, I.PAYMENT_METHOD, I.PAYMENT_CHANNEL, "
                        + "    I.TAX_FORM03_ID, F.MONTHLY, F.YEARLY, F.REF_STATION_ID, S.STATION_NAME "
                        + " FROM INVOICE I "
                        + "    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID "
                        + "    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID "
                        + " WHERE I.PAYMENT_STATUS IN (1, 2, 3, 99) ";
/*
 SELECT I.INVOICE_ID, I.TAX_NO, I.PAYMENT_DATE, I.PAYMENT_TIME, I.TOTAL_AMOUNT, 
    I.PAYMENT_STATUS, I.PAYMENT_METHOD, I.PAYMENT_CHANNEL, 
    I.TAX_FORM03_ID, F.MONTHLY, F.YEARLY, F.REF_STATION_ID, S.STATION_NAME 
 FROM INVOICE I 
    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID 
    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID 
 WHERE I.PAYMENT_STATUS IN (1, 2, 3, 99) AND I.TAX_NO = ''
*/
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (I.TAX_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (S.STATION_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                sql += " AND (F.YEARLY = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                sql += " AND (F.MONTHLY = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(I.PAYMENT_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(I.PAYMENT_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            ps.setSql(sql);

            String orderBy = " PAYMENT_DATE DESC, PAYMENT_TIME DESC, YEARLY DESC, MONTHLY DESC ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                ps.setString(i++, filter.getYearly());
            }

            if (!AppUtil.isNullAndSpace(filter.getMonthly())) {
                ps.setString(i++, filter.getMonthly());
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            rs = ps.executeQuery();

            List<FrontInvoiceViewModel> dataList = null;
            dataList = new FrontPaymentMapper(rs).mapListHistory();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

}
