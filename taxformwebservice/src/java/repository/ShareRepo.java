/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.SettingGeneralEnum;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.OfficeModel;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;

/**
 *
 * @author User
 */
public class ShareRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public OfficeModel getOfficeDataByAmphurId(String amphurId) throws SQLException {
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        int pk = 0;
        OfficeModel result = new OfficeModel();

        try {
            int i = 1;

            //Get current value
            String sql = "SELECT AMPHUR_ID,OFFICE_CODE,OFFICE_NAME, MOBILE, FAX  "
                    + "FROM OFFICE WHERE AMPHUR_ID = ? ";
            ps.setSql(sql);

            ps.setString(i++, amphurId);

            rs = ps.executeQuery();

            while (rs.next()) {
                result.setOfficeId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                result.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                result.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                result.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                result.setFax(AppUtil.checkNullData(rs.getString("FAX")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return result;
    }

    public String getDataValue(SettingGeneralEnum name) throws SQLException {

        String result = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT VALUE_TEXT "
                    + " FROM M_SETTING_GENERAL "
                    + " WHERE SETTING_GENERAL_NAME = ? ";

            ps.setSql(sql);

            ps.setString(1, name.value());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString("VALUE_TEXT");
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public Double getTaxRateByDatenow() throws SQLException {

        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        Double taxRate = 1.5;
        try {

            ps.setAutoCommit(false);

            String sql = " SELECT TAX_RATE_VALUE FROM M_TAX_RATE \n"
                    + "WHERE TRUNC(SYSDATE) BETWEEN TRUNC(START_DATE) AND TRUNC(END_DATE)\n"
                    + "AND IS_ACTIVE= 1 ";

            ps.setSql(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                taxRate = rs.getDouble("TAX_RATE_VALUE");
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return taxRate;

    }

    public String getYearlyBudget(String mmyyyy) throws SQLException {
        String year = mmyyyy.substring(mmyyyy.length() - 4, mmyyyy.length());
        String month = mmyyyy.substring(0, mmyyyy.length()-4);
        int yearInt = Integer.parseInt(year);
        if (Integer.parseInt(month) > 9) {
            yearInt = yearInt + 1;
        }
        return Integer.toString(yearInt);
    }
}
