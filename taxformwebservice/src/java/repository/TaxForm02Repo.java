/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import enumeration.StatusEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.OfficeModel;
import model.TaxForm02FileModel;
import model.TaxForm02Model;
import repository.mapper.TaxForm02Mapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxForm02Model;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm02FileViewModel;
import viewModel.TaxForm02ViewModel;

/**
 *
 * @author User
 */
public class TaxForm02Repo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();
    ShareRepo shareRepo = new ShareRepo();

    public ResultData<List<TaxForm02ViewModel>> getList(ResultPage page, FilterTaxForm02Model filter) throws SQLException {

        ResultData<List<TaxForm02ViewModel>> resultData = new ResultData<List<TaxForm02ViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT TAX_FORM02.*,RETAIL_STATION.STATION_NAME "
                    + " FROM TAX_FORM02 LEFT JOIN RETAIL ON TAX_FORM02.REF_RETAIL_ID = RETAIL.RETAIL_ID "
                    + " LEFT JOIN RETAIL_STATION ON TAX_FORM02.REF_STATION_ID = RETAIL_STATION.RETAIL_STATION_ID "
                    + " WHERE (1=1) ";

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND (STATUS = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (DOC_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                sql += " AND (AMPHUR_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                sql += " AND (AMPHUR_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusId())) {
                sql += " AND (BUSINESS_STATUS_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusName())) {
                sql += " AND (BUSINESS_STATUS_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (STATION_NAME_OLD LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                sql += " AND (OWNER_NAME_OLD LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDocDate())) {
                sql += " AND ( TRUNC(DOC_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy')) ) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDocDate())) {
                sql += " AND ( TRUNC(DOC_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy')) ) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (RETAIL.TAX_NO = ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("updatedDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, "%" + filter.getDocNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                ps.setString(i++, filter.getAmphurId());
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                ps.setString(i++, "%" + filter.getAmphurName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusId()) && AppUtil.decryptId(filter.getBusinessStatusId()) > 0) {
                ps.setInt(i++, AppUtil.decryptId(filter.getBusinessStatusId()));
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusName())) {
                ps.setString(i++, "%" + filter.getBusinessStatusName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                ps.setString(i++, "%" + filter.getOwnerName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDocDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDocDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDocDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDocDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            rs = ps.executeQuery();

            List<TaxForm02ViewModel> dataList = null;
            dataList = new TaxForm02Mapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<TaxForm02ViewModel>> getListForOfficer(ResultPage page, FilterTaxForm02Model filter) throws SQLException {

        ResultData<List<TaxForm02ViewModel>> resultData = new ResultData<List<TaxForm02ViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT TAX_FORM02.*,TAX_FORM02.STATION_NAME_OLD STATION_NAME "
                    + " FROM TAX_FORM02 LEFT JOIN RETAIL ON TAX_FORM02.REF_RETAIL_ID = RETAIL.RETAIL_ID "
                    + " LEFT JOIN RETAIL_STATION ON TAX_FORM02.REF_STATION_ID = RETAIL_STATION.RETAIL_STATION_ID "
                    + " WHERE (1=1) AND TAX_FORM02.STATUS NOT IN( 0 ) ";

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND (TAX_FORM02.STATUS = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (TAX_FORM02.DOC_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                sql += " AND (TAX_FORM02.AMPHUR_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                sql += " AND (TAX_FORM02.AMPHUR_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusId())) {
                sql += " AND (TAX_FORM02.BUSINESS_STATUS_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusName())) {
                sql += " AND (TAX_FORM02.BUSINESS_STATUS_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (TAX_FORM02.STATION_NAME_OLD LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                sql += " AND (TAX_FORM02.OWNER_NAME_OLD LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDocDate())) {
                sql += " AND ( TRUNC(TAX_FORM02.DOC_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy')) ) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDocDate())) {
                sql += " AND ( TRUNC(TAX_FORM02.DOC_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy')) ) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("updatedDate");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, "%" + filter.getDocNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurId())) {
                ps.setString(i++, filter.getAmphurId());
            }

            if (!AppUtil.isNullAndSpace(filter.getAmphurName())) {
                ps.setString(i++, "%" + filter.getAmphurName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusId()) && AppUtil.decryptId(filter.getBusinessStatusId()) > 0) {
                ps.setInt(i++, AppUtil.decryptId(filter.getBusinessStatusId()));
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusName())) {
                ps.setString(i++, "%" + filter.getBusinessStatusName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                ps.setString(i++, "%" + filter.getOwnerName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDocDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDocDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDocDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDocDate()));
            }

            rs = ps.executeQuery();

            List<TaxForm02ViewModel> dataList = null;
            dataList = new TaxForm02Mapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<StatusLogViewModel>> getListStatusLog(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<StatusLogViewModel>> resultData = new ResultData<List<StatusLogViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * "
                    + " FROM TAX_FORM02_STATUS_LOG  "
                    + " WHERE TAX_FORM02_ID = ?  ORDER BY ACTION_DATE DESC ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getTaxForm02Id()));

            if (page != null) {
                ps.setResultPage(page);
            }

            rs = ps.executeQuery();

            List<StatusLogViewModel> dataList = new ArrayList<>();
            while (rs.next()) {
                StatusLogViewModel data = new StatusLogViewModel();
                data.setTaxForm02Id(AppUtil.encryptId(rs.getInt("TAX_FORM02_ID")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setActionBy(AppUtil.checkNullData(rs.getString("ACTION_BY")));
                data.setRemark(AppUtil.checkNullData(rs.getString("REMARK")));

                dataList.add(data);
            }

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public List<DrpDownViewModel> getListBusinessStatus(String id) throws SQLException {

        List<DrpDownViewModel> dataList = new ArrayList<>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * "
                    + " FROM TAX_FORM02_BUSINESS_STATUS  "
                    + " WHERE TAX_FORM02_ID = ?  ORDER BY TEMPLATE_CODE ASC ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(id));

            rs = ps.executeQuery();

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_NAME")));
                data.setCode(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_CODE")));
                data.setOther(AppUtil.checkNullData(rs.getString("TEMPLATE_CODE")));

                dataList.add(data);
            }

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return dataList;

    }

    private String getColumnDB(String orderBy) {

        String str = " TAX_FORM02_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("stationCode")) {
                str = " STATION_CODE ";
            } else if (orderBy.equalsIgnoreCase("stationName")) {
                str = " STATION_NAME_OLD ";
            } else if (orderBy.equalsIgnoreCase("amphurName")) {
                str = " AMPHUR_NAME ";
            } else if (orderBy.equalsIgnoreCase("docDate")) {
                str = " DOC_DATE ";
            } else if (orderBy.equalsIgnoreCase("docNo")) {
                str = " DOC_NO ";
            } else if (orderBy.equalsIgnoreCase("forType")) {
                str = " FOR_TYPE ";
            } else if (orderBy.equalsIgnoreCase("businessStatusId")) {
                str = " BUSINESS_STATUS_ID ";
            } else if (orderBy.equalsIgnoreCase("businessStatusName")) {
                str = " BUSINESS_STATUS_NAME ";
            } else if (orderBy.equalsIgnoreCase("status")) {
                str = " TAX_FORM02.STATUS ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }
        }

        return str;

    }

    public ResultData<TaxForm02ViewModel> getData(int id) throws SQLException {

        ResultData<TaxForm02ViewModel> resultData = new ResultData<TaxForm02ViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc="get TaxForm02 ">
            String sql = " SELECT TAX_FORM02.*,M_BUSINESS_STATUS.BUSINESS_STATUS_CODE "
                    + " FROM TAX_FORM02 LEFT JOIN M_BUSINESS_STATUS ON M_BUSINESS_STATUS.BUSINESS_STATUS_ID = TAX_FORM02.BUSINESS_STATUS_ID "
                    + " WHERE TAX_FORM02_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            TaxForm02ViewModel data = new TaxForm02Mapper(rs).mapData();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get TaxForm02 Business Status ">
            // ความประสงค์
            sql = " SELECT * "
                    + " FROM TAX_FORM02_BUSINESS_STATUS  "
                    + " WHERE TAX_FORM02_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataBusiness = new TaxForm02Mapper(rs).mapDataBusinessStatus();
            data.setBusinessStatusList(dataBusiness);
            // </editor-fold>

            // FILE_CATE = หมวดหมู่ไฟล์ (E=หนังสือรับรอง,A=หนังสือมอบอำนาจ,R=บัญชีรายการค้างชำระภาษี,O=เอกสารอื่นๆ)
            // <editor-fold defaultstate="collapsed" desc=" get TaxForm02 Commerce File ">
            // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
            sql = " SELECT * "
                    + " FROM TAX_FORM02_FILE  "
                    + " WHERE TAX_FORM02_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Commerce.display());

            rs = ps.executeQuery();

            List<TaxForm02FileViewModel> dataCommerce = new TaxForm02Mapper(rs).mapDataFile();
            data.setCommerceFileList(dataCommerce);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get TaxForm02 Authorize File ">
            // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนผู้รับมอบอำนาจ
            sql = " SELECT * "
                    + " FROM TAX_FORM02_FILE  "
                    + " WHERE TAX_FORM02_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Authorize.display());

            rs = ps.executeQuery();

            List<TaxForm02FileViewModel> dataAuthorize = new TaxForm02Mapper(rs).mapDataFile();
            data.setAuthorizeFileList(dataAuthorize);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get TaxForm02 TaxRemain File ">
            // บัญชีสินค้าน้ำมัน/ก๊าซปิโตรเลียมและบัญชีรายการค้างชำระภาษี
            sql = " SELECT * "
                    + " FROM TAX_FORM02_FILE  "
                    + " WHERE TAX_FORM02_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.TaxRemain.display());

            rs = ps.executeQuery();

            List<TaxForm02FileViewModel> dataTaxRemain = new TaxForm02Mapper(rs).mapDataFile();
            data.setTaxRemainFileList(dataTaxRemain);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get TaxForm02 Other File ">
            // เอกสารอื่น ๆ
            sql = " SELECT * "
                    + " FROM TAX_FORM02_FILE  "
                    + " WHERE TAX_FORM02_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Other.display());

            rs = ps.executeQuery();

            List<TaxForm02FileViewModel> dataOther = new TaxForm02Mapper(rs).mapDataFile();
            data.setOtherFileList(dataOther);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(TaxForm02Model data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql TaxForm02">
            if (data.getTaxForm02Id() == 0) {

                sql = "INSERT INTO TAX_FORM02 ( "
                        + "  REF_RETAIL_ID "
                        + ", REF_STATION_ID "
                        + ", FOR_TYPE "
                        + ", BUSINESS_STATUS_ID "
                        + ", BUSINESS_STATUS_NAME "
                        + ", CANCEL_DATE "
                        + ", CHANGE_DATE "
                        + ", REF_OFFICE "
                        + ", REF_OFFICE_NAME "
                        + ", REF_OFFICE_CODE "
                        + ", ADDRESS_OLD "
                        + ", HOUSE_NO "
                        + ", ADDRESS_NEW "
                        + ", SOI "
                        + ", ROAD "
                        + ", TAMBON_ID "
                        + ", TAMBON_NAME "
                        + ", AMPHUR_ID "
                        + ", AMPHUR_NAME "
                        + ", PROVINCE_ID "
                        + ", PROVINCE_NAME "
                        + ", POSTCODE "
                        + ", MOBILE_OLD "
                        + ", MOBILE_NEW "
                        + ", OWNER_NAME_OLD "
                        + ", OWNER_NAME_NEW "
                        + ", TAX_NO_OLD "
                        + ", TAX_NO_NEW "
                        + ", CUSTOMER_TYPE_OLD "
                        + ", CUSTOMER_TYPE_NEW "
                        + ", ID_CARD_OLD "
                        + ", ID_CARD_NEW "
                        + ", ID_CARD_AT_OLD "
                        + ", ID_CARD_AT_NEW "
                        + ", CORP_NO_OLD "
                        + ", CORP_NO_NEW "
                        + ", CORP_DATE_OLD "
                        + ", CORP_DATE_NEW "
                        + ", STATION_CODE "
                        + ", STATION_NAME_OLD "
                        + ", STATION_NAME_NEW "
                        + ", ADDRESS_RETAIL "
                        + ", SOI_RETAIL "
                        + ", ROAD_RETAIL "
                        + ", TAMBON_ID_RETAIL "
                        + ", TAMBON_NAME_RETAIL "
                        + ", AMPHUR_ID_RETAIL "
                        + ", AMPHUR_NAME_RETAIL "
                        + ", PROVINCE_ID_RETAIL "
                        + ", PROVINCE_NAME_RETAIL "
                        + ", POSTCODE_RETAIL "
                        + ", MOBILE_RETAIL "
                        + ", CAUSE "
                        + ", ADDRESS_STATION "
                        + ", SOI_STATION "
                        + ", ROAD_STATION "
                        + ", TAMBON_ID_STATION "
                        + ", TAMBON_NAME_STATION "
                        + ", AMPHUR_ID_STATION "
                        + ", AMPHUR_NAME_STATION "
                        + ", PROVINCE_ID_STATION "
                        + ", PROVINCE_NAME_STATION "
                        + ", POSTCODE_STATION "
                        + ", MOBILE_STATION "
                        + ", OTHER "
                        + ", TAX_REMAIN "
                        + ", TAX_OIL_GAS "
                        + ", FILE_NUM_E "
                        + ", FILE_NUM_A "
                        + ", FILE_NUM_R "
                        + ", FILE_NUM_O "
                        + ", IS_ACTIVE "
                        + ", DOC_NO "
                        + ", DOC_DATE "
                        + ", STATUS "
                        + ", STATUS_NAME "
                        + ", CREATED_DATE "
                        + ", CREATED_BY "
                        + ") VALUES (?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?, "
                        + " ?,?,?,?,?,?, ?,?,?,?, ?,?,SYSDATE,?,?, SYSDATE,?) ";

                ps = conn.prepareStatement(sql, new String[]{"TAX_FORM02_ID"});

            } else {

                sql = "UPDATE TAX_FORM02 SET "
                        + "  REF_RETAIL_ID= ? "
                        + ", REF_STATION_ID= ? "
                        + ", FOR_TYPE= ? "
                        + ", BUSINESS_STATUS_ID= ? "
                        + ", BUSINESS_STATUS_NAME= ? "
                        + ", CANCEL_DATE= ? "
                        + ", CHANGE_DATE= ? "
                        + ", REF_OFFICE = ? "
                        + ", REF_OFFICE_NAME= ?  "
                        + ", REF_OFFICE_CODE= ?  "
                        + ", ADDRESS_OLD= ? "
                        + ", HOUSE_NO= ? "
                        + ", ADDRESS_NEW= ? "
                        + ", SOI= ? "
                        + ", ROAD= ? "
                        + ", TAMBON_ID= ? "
                        + ", TAMBON_NAME= ? "
                        + ", AMPHUR_ID= ? "
                        + ", AMPHUR_NAME= ? "
                        + ", PROVINCE_ID= ? "
                        + ", PROVINCE_NAME= ? "
                        + ", POSTCODE= ? "
                        + ", MOBILE_OLD= ? "
                        + ", MOBILE_NEW= ? "
                        + ", OWNER_NAME_OLD= ? "
                        + ", OWNER_NAME_NEW= ? "
                        + ", TAX_NO_OLD= ? "
                        + ", TAX_NO_NEW= ? "
                        + ", CUSTOMER_TYPE_OLD= ? "
                        + ", CUSTOMER_TYPE_NEW= ? "
                        + ", ID_CARD_OLD= ? "
                        + ", ID_CARD_NEW= ? "
                        + ", ID_CARD_AT_OLD= ? "
                        + ", ID_CARD_AT_NEW= ? "
                        + ", CORP_NO_OLD= ? "
                        + ", CORP_NO_NEW= ? "
                        + ", CORP_DATE_OLD= ? "
                        + ", CORP_DATE_NEW= ? "
                        + ", STATION_CODE= ? "
                        + ", STATION_NAME_OLD= ? "
                        + ", STATION_NAME_NEW= ? "
                        + ", ADDRESS_RETAIL= ? "
                        + ", SOI_RETAIL= ? "
                        + ", ROAD_RETAIL= ? "
                        + ", TAMBON_ID_RETAIL= ? "
                        + ", TAMBON_NAME_RETAIL= ? "
                        + ", AMPHUR_ID_RETAIL= ? "
                        + ", AMPHUR_NAME_RETAIL= ? "
                        + ", PROVINCE_ID_RETAIL= ? "
                        + ", PROVINCE_NAME_RETAIL= ? "
                        + ", POSTCODE_RETAIL= ? "
                        + ", MOBILE_RETAIL= ? "
                        + ", CAUSE = ? "
                        + ", ADDRESS_STATION = ? "
                        + ", SOI_STATION= ? "
                        + ", ROAD_STATION= ? "
                        + ", TAMBON_ID_STATION= ? "
                        + ", TAMBON_NAME_STATION= ? "
                        + ", AMPHUR_ID_STATION= ? "
                        + ", AMPHUR_NAME_STATION= ? "
                        + ", PROVINCE_ID_STATION= ? "
                        + ", PROVINCE_NAME_STATION= ? "
                        + ", POSTCODE_STATION= ? "
                        + ", MOBILE_STATION= ? "
                        + ", OTHER= ? "
                        + ", TAX_REMAIN= ? "
                        + ", TAX_OIL_GAS= ? "
                        + ", FILE_NUM_E= ? "
                        + ", FILE_NUM_A= ? "
                        + ", FILE_NUM_R= ? "
                        + ", FILE_NUM_O= ? "
                        + ", IS_ACTIVE = ? "
                        + ", DOC_NO = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE TAX_FORM02_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setInt(i++, data.getRefRetailId());
            ps.setInt(i++, data.getRefStationId());
            ps.setString(i++, data.getForType());
            ps.setInt(i++, data.getBusinessStatusId());
            ps.setString(i++, data.getBusinessStatusName());
            ps.setDate(i++, AppUtil.toDateSql(data.getCancelDate()));
            ps.setDate(i++, AppUtil.toDateSql(data.getChangeDate()));

            ps.setString(i++, data.getOffice());
            ps.setString(i++, data.getOfficeName());
            ps.setString(i++, data.getOfficeCode());
            
            ps.setString(i++, data.getAddressOld());
            ps.setString(i++, data.getHouseNo());
            ps.setString(i++, data.getAddress());
            ps.setString(i++, data.getSoi());
            ps.setString(i++, data.getRoad());
            ps.setString(i++, data.getTambonId());
            ps.setString(i++, data.getTambonName());
            ps.setString(i++, data.getAmphurId());
            ps.setString(i++, data.getAmphurName());
            ps.setString(i++, data.getProvinceId());
            ps.setString(i++, data.getProvinceName());
            ps.setString(i++, data.getPostcode());
            ps.setString(i++, data.getMobileOld());
            ps.setString(i++, data.getMobile());

            ps.setString(i++, data.getOwnerNameOld());
            ps.setString(i++, data.getOwnerName());
            ps.setString(i++, data.getTaxNoOld());
            ps.setString(i++, data.getTaxNo());
            ps.setString(i++, data.getCustomerTypeOld());
            ps.setString(i++, data.getCustomerType());
            ps.setString(i++, data.getIdCardOld());
            ps.setString(i++, data.getIdCard());
            ps.setString(i++, data.getIdCardAtOld());
            ps.setString(i++, data.getIdCardAt());
            ps.setString(i++, data.getCorpNoOld());
            ps.setString(i++, data.getCorpNo());
            ps.setDate(i++, AppUtil.toDateSql(data.getCorpDateOld()));
            ps.setDate(i++, AppUtil.toDateSql(data.getCorpDate()));

            ps.setString(i++, data.getStationCode());
            ps.setString(i++, data.getStationNameOld());
            ps.setString(i++, data.getStationName());

            ps.setString(i++, data.getAddressRetail());
            ps.setString(i++, data.getSoiRetail());
            ps.setString(i++, data.getRoadRetail());
            ps.setString(i++, data.getTambonIdRetail());
            ps.setString(i++, data.getTambonNameRetail());
            ps.setString(i++, data.getAmphurIdRetail());
            ps.setString(i++, data.getAmphurNameRetail());
            ps.setString(i++, data.getProvinceIdRetail());
            ps.setString(i++, data.getProvinceNameRetail());
            ps.setString(i++, data.getPostcodeRetail());
            ps.setString(i++, data.getMobileRetail());
            ps.setString(i++, data.getCause());

            ps.setString(i++, data.getAddressStation());
            ps.setString(i++, data.getSoiStation());
            ps.setString(i++, data.getRoadStation());
            ps.setString(i++, data.getTambonIdStation());
            ps.setString(i++, data.getTambonNameStation());
            ps.setString(i++, data.getAmphurIdStation());
            ps.setString(i++, data.getAmphurNameStation());
            ps.setString(i++, data.getProvinceIdStation());
            ps.setString(i++, data.getProvinceNameStation());
            ps.setString(i++, data.getPostcodeStation());
            ps.setString(i++, data.getMobileStation());

            ps.setString(i++, data.getOther());
            ps.setDouble(i++, data.getTaxRemain());
            ps.setDouble(i++, data.getTaxOilGas());

            ps.setInt(i++, data.getFileNumE());
            ps.setInt(i++, data.getFileNumA());
            ps.setInt(i++, data.getFileNumR());
            ps.setInt(i++, data.getFileNumO());

            ps.setBoolean(i++, data.getActive());

            //DOC_NO
            if (data.getTaxForm02Id() == 0) {
                ps.setString(i++, AppUtil.getRunningDoc("taxform02", data.getOfficeCode()));
            } else {
                ps.setString(i++, data.getDocNo());
            }

            if (data.getTaxForm02Id() == 0) {
                ps.setString(i++, String.valueOf(StatusEnum.Create.value()));
                ps.setString(i++, StatusEnum.Create.displayNameTH());
            }

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            if (data.getTaxForm02Id() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setTaxForm02Id(id);
            } else {

                ps.setInt(i++, data.getTaxForm02Id());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT ATTACH TaxForm02 Business Status">
            if (data.getTaxForm02Id() != 0) {

                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM TAX_FORM02_BUSINESS_STATUS WHERE TAX_FORM02_ID = ? ";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm02Id());

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT TAX_FORM02_BUSINESS_STATUS">
                if (data.getBusinessStatusList() != null && !data.getBusinessStatusList().isEmpty()) {
                    for (DrpDownViewModel business : data.getBusinessStatusList()) {
                        if (data.getTaxForm02Id() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM02_BUSINESS_STATUS ( "
                                    + " TAX_FORM02_ID "
                                    + ",BUSINESS_STATUS_ID "
                                    + ",BUSINESS_STATUS_NAME "
                                    + ",BUSINESS_STATUS_CODE "
                                    + ",TEMPLATE_CODE "
                                    + ")  VALUES (?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm02Id());
                            ps.setInt(i++, AppUtil.decryptId(business.getId()));
                            ps.setString(i++, business.getName());
                            ps.setString(i++, business.getCode());
                            ps.setString(i++, business.getOther());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT ATTACH TaxForm02 FILE">
            if (data.getTaxForm02Id() != 0) {

                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM TAX_FORM02_FILE WHERE TAX_FORM02_ID = ? ";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxForm02Id());

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT Commerce">
                if (data.getCommerceFileList() != null && !data.getCommerceFileList().isEmpty()) {
                    for (TaxForm02FileModel file : data.getCommerceFileList()) {
                        if (data.getTaxForm02Id() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM02_FILE ( "
                                    + " TAX_FORM02_ID "
                                    + ",FILE_NAME "
                                    + ",PATH_FILE "
                                    + ",FILE_CATE "
                                    + ",FILE_TYPE "
                                    + ") VALUES (?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm02Id());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT Authorize">
                if (data.getAuthorizeFileList() != null && !data.getAuthorizeFileList().isEmpty()) {
                    for (TaxForm02FileModel file : data.getAuthorizeFileList()) {
                        if (data.getTaxForm02Id() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM02_FILE ( "
                                    + " TAX_FORM02_ID "
                                    + ",FILE_NAME "
                                    + ",PATH_FILE "
                                    + ",FILE_CATE "
                                    + ",FILE_TYPE "
                                    + ") VALUES (?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm02Id());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT TaxRemain">
                if (data.getTaxRemainFileList() != null && !data.getTaxRemainFileList().isEmpty()) {
                    for (TaxForm02FileModel file : data.getTaxRemainFileList()) {
                        if (data.getTaxForm02Id() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM02_FILE ( "
                                    + " TAX_FORM02_ID "
                                    + ",FILE_NAME "
                                    + ",PATH_FILE "
                                    + ",FILE_CATE "
                                    + ",FILE_TYPE "
                                    + ") VALUES (?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm02Id());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT Other">
                if (data.getOtherFileList() != null && !data.getOtherFileList().isEmpty()) {
                    for (TaxForm02FileModel file : data.getOtherFileList()) {
                        if (data.getTaxForm02Id() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM02_FILE ( "
                                    + " TAX_FORM02_ID "
                                    + ",FILE_NAME "
                                    + ",PATH_FILE "
                                    + ",FILE_CATE "
                                    + ",FILE_TYPE "
                                    + ") VALUES (?,?,?,?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxForm02Id());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData deleteData(List<Integer> idList, FilterTaxForm02Model filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;
            String sql = "";
            String sqlTaxForm02 = " DELETE FROM TAX_FORM02 "
                    + " WHERE TAX_FORM02_ID = ? ";
            String sqlFile = " DELETE FROM TAX_FORM02_FILE "
                    + " WHERE TAX_FORM02_ID = ? ";
            String sqlStatusLog = " DELETE FROM TAX_FORM02_STATUS_LOG "
                    + " WHERE TAX_FORM02_ID = ? ";

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (int id : idList) {

                boolean checkStatusData = false;

                //<editor-fold defaultstate="collapsed" desc="check status data is Create only">
                i = 1;
                sql = " SELECT COUNT(1) COUNTSTATUS FROM TAX_FORM02  "
                        + " WHERE (TAX_FORM02_ID=?) AND (STATUS=?) ";

                psSelect.setSql(sql);
                psSelect.setInt(i++, id);
                psSelect.setString(i++, String.valueOf(StatusEnum.Create.value())); //สร้าง

                rs = psSelect.executeQuery();
                while (rs.next()) {
                    checkStatusData = rs.getInt("COUNTSTATUS") > 0;
                }
                //</editor-fold>

                if (checkStatusData) {

                    //<editor-fold defaultstate="collapsed" desc="Delete TaxForm02StatusLog by id">
                    i = 1;

                    ps.setSql(sqlStatusLog);
                    ps.setInt(i++, id);

                    result = ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Delete TaxForm02File by id">
                    i = 1;

                    ps.setSql(sqlFile);
                    ps.setInt(i++, id);

                    result = ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Delete TaxForm02 by id">
                    i = 1;

                    ps.setSql(sqlTaxForm02);
                    ps.setInt(i++, id);

                    result = ps.executeUpdate();
                    //</editor-fold>

                } else {
                    //status not delete
                }

            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

}
