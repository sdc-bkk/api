/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import enumeration.SettingGeneralEnum;
import enumeration.StatusEnum;
import enumeration.StatusRefundEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.OfficeModel;
import model.TaxForm03DetailModel;
import model.TaxForm03FileModel;
import model.TaxForm03Model;
import model.TaxForm03SummaryModel;
import model.TaxFormRefundFileModel;
import model.TaxFormRefundModel;
import repository.mapper.TaxForm03Mapper;
import repository.mapper.TaxFormRefundMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.FeeViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.MonthlyViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.StatusAccountViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm03DetailViewModel;
import viewModel.TaxForm03FileViewModel;
import viewModel.TaxForm03SummaryViewModel;
import viewModel.TaxForm03ViewModel;
import viewModel.TaxFormRefundViewModel;
import viewModel.TaxFormRefundFileViewModel;

/**
 *
 * @author User
 */
public class TaxFormRefundRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<TaxFormRefundViewModel>> getList(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<TaxFormRefundViewModel>> resultData = new ResultData<List<TaxFormRefundViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "select tr.doc_no,tr.doc_date,st.station_name,tr.amount,tr.status,tr.status_name,tr.ref_station_id,tr.TAX_FORM_REFUND_ID from tax_form_refund tr "
                    + "left join retail_station st "
                    + "on tr.ref_station_id=st.retail_station_id "
                    + "where tr.ref_retail_id=? ";

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (tr.DOC_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(tr.DOC_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(tr.DOC_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND (tr.Status = ?) ";
            }

            ps.setSql(sql);

            String orderBy = " DOC_DATE DESC ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            ps.setInt(i++, AppUtil.decryptId(filter.getRetailId()));

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, filter.getDocNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, filter.getStartDate());
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, filter.getEndDate());
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }

            rs = ps.executeQuery();

            List<TaxFormRefundViewModel> dataList = null;
            dataList = new TaxFormRefundMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<TaxFormRefundViewModel>> getListBackOffice(ResultPage page, FilterTaxFormModel filter) throws SQLException {

        ResultData<List<TaxFormRefundViewModel>> resultData = new ResultData<List<TaxFormRefundViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "select tr.doc_no,tr.doc_date,st.station_name,tr.amount,tr.status,tr.status_name,tr.ref_station_id,tr.TAX_FORM_REFUND_ID from tax_form_refund tr "
                    + "left join retail_station st "
                    + "on tr.ref_station_id=st.retail_station_id "
                    + "where tr.DOC_NO IS NOT NULL ";

            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND (tr.DOC_NO = ?) ";
            }

            //search start date
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(tr.doc_date ) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            //search last date
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(tr.doc_date ) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND (tr.status = ?) ";
            }

            ps.setSql(sql);

            String orderBy = " DOC_DATE DESC ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            //ps.setInt(i++, AppUtil.decryptId(filter.getTaxForm01RetailId()));
            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, filter.getDocNo());
            }

            //date
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }

            rs = ps.executeQuery();

            List<TaxFormRefundViewModel> dataList = null;
            dataList = new TaxFormRefundMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    public ResultData<TaxFormRefundViewModel> getData(int id) throws SQLException {

        ResultData<TaxFormRefundViewModel> resultData = new ResultData<TaxFormRefundViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get taxformrefund ">
            String sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND  "
                    + " WHERE TAX_FORM_REFUND_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            TaxFormRefundViewModel data = new TaxFormRefundMapper(rs).mapData();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get  billFile ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.BillPayment.display());

            rs = ps.executeQuery();

            List<TaxFormRefundFileViewModel> dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setBillPaymentFileList(dataFile);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc=" get IdCardFile ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.IDCard.display());

            rs = ps.executeQuery();

            dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setIdCardFileList(dataFile);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc=" get  Authorize File ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Authorize.display());

            rs = ps.executeQuery();

            dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setAuthorizeFileList(dataFile);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc=" get BookBank ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.BookBank.display());

            rs = ps.executeQuery();
            dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setBookBankFileList(dataFile);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<TaxFormRefundViewModel> getDataBackOffice(int id) throws SQLException {

        ResultData<TaxFormRefundViewModel> resultData = new ResultData<TaxFormRefundViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get taxform03 ">
            String sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND  "
                    + " WHERE TAX_FORM_REFUND_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            TaxFormRefundViewModel data = new TaxFormRefundMapper(rs).mapData();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get  billFile ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.BillPayment.display());

            rs = ps.executeQuery();

            List<TaxFormRefundFileViewModel> dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setBillPaymentFileList(dataFile);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc=" get IdCardFile ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.IDCard.display());

            rs = ps.executeQuery();

            dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setIdCardFileList(dataFile);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc=" get  Authorize File ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.Authorize.display());

            rs = ps.executeQuery();

            dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setAuthorizeFileList(dataFile);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc=" get BookBank ">
            //เอกสารแนบ
            sql = " SELECT * "
                    + " FROM TAX_FORM_REFUND_FILE  "
                    + " WHERE TAX_FORM_REFUND_ID = ? AND FILE_CATE = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);
            ps.setString(2, AttachFileCateEnum.BookBank.display());

            rs = ps.executeQuery();
            dataFile = new TaxFormRefundMapper(rs).mapDataFile();
            data.setBookBankFileList(dataFile);
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData save(TaxFormRefundModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="ตรวจสอบสำนักงานเขตที่จะเอามาแสดง "> 
            //ดูจาก station รายการแรก   
            ShareRepo shareRepo = new ShareRepo();
            OfficeModel officeData = new OfficeModel();
            if (data.getRefOffice() != null && !data.getRefOffice().isEmpty()) {
                officeData = shareRepo.getOfficeDataByAmphurId(data.getRefOffice());

            }

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="tax_form_refund">
            i = 1;
            if (data.getTaxFormRefundId() == 0) {

                sql = "INSERT INTO TAX_FORM_REFUND ( "
                        + "  REF_RETAIL_ID "
                        + ", REF_STATION_ID "
                        + ", MONTHLY "
                        + ", YEARLY "
                        + ", MONTH_NAME "
                        + ", AMOUNT "
                        + ", RECEIPT_NO "
                        + ", STATUS "
                        + ", STATUS_NAME "
                        + ", ACCEPT_BY "
                        + ", ACCEPT_DATE "
                        + ", REFUND_BY "
                        + ", REFUND_DATE "
                        + ", REJECT_BY "
                        + ", REJECT_DATE "
                        + ", DOC_NO1 "
                        + ", DOC_NO2 "
                        + ", PLACE "
                        + ", REF_OFFICE"
                        + ", REF_OFFICE_NAME "
                        + ", REF_OFFICE_CODE "
                        + ", DOC_NO "
                        + ", DOC_DATE "
                        + ", CREATED_DATE "
                        + ", CREATED_BY "
                        + ", RECEIPT_DATE "
                        + ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,SYSDATE,?,?) ";

                ps = conn.prepareStatement(sql, new String[]{"TAX_FORM_REFUND_ID"});

            } else {

                sql = "UPDATE TAX_FORM_REFUND SET "
                        + "  REF_RETAIL_ID = ? "
                        + ", REF_STATION_ID = ? "
                        + ", MONTHLY  = ?"
                        + ", YEARLY  = ?"
                        + ", MONTH_NAME = ? "
                        + ", AMOUNT = ? "
                        + ", RECEIPT_NO = ? "
                        + ", STATUS = ? "
                        + ", STATUS_NAME = ? "
                        + ", ACCEPT_BY = ? "
                        + ", ACCEPT_DATE = ? "
                        + ", REFUND_BY = ? "
                        + ", REFUND_DATE = ? "
                        + ", REJECT_BY = ? "
                        + ", REJECT_DATE = ? "
                        + ", DOC_NO1 = ?  "
                        + ", DOC_NO2 = ? "
                        + ", PLACE = ? "
                        + ", REF_OFFICE = ? "
                        + ", REF_OFFICE_NAME = ? "
                        + ", REF_OFFICE_CODE = ? "
                        //                        + ", DOC_NO = ? "
                        + ", UPDATED_BY = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", RECEIPT_DATE = ? "
                        + " WHERE TAX_FORM_REFUND_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setInt(i++, data.getRefRetailId());
            ps.setInt(i++, data.getRefStationId());
            ps.setInt(i++, data.getMonthly());
            ps.setInt(i++, data.getYearly());
            ps.setString(i++, data.getMonthName());
            ps.setDouble(i++, data.getAmount());
            ps.setString(i++, data.getReceiptNo());
            ps.setString(i++, data.getStatus());
            ps.setString(i++, data.getStatusName());
            ps.setString(i++, data.getAcceptBy());
            ps.setDate(i++, AppUtil.toDateSql(data.getAcceptDate()));
            ps.setString(i++, data.getRefundBy());
            ps.setDate(i++, AppUtil.toDateSql(data.getRefundDate()));
            ps.setString(i++, data.getRejectBy());
            ps.setDate(i++, AppUtil.toDateSql(data.getRejectDate()));
            ps.setString(i++, data.getDocNo1());
            ps.setString(i++, data.getDocNo2());
            ps.setString(i++, data.getPlace());
            ps.setString(i++, data.getRefOffice());
            ps.setString(i++, data.getRefOfficeName());
            ps.setString(i++, officeData.getOfficeCode());
            if (data.getTaxFormRefundId() == 0) {
                ps.setString(i++, AppUtil.getRunningDoc("formfund", officeData.getOfficeCode()));
                if (!AppUtil.isNullAndSpace(data.getCreatedBy())) {
                    ps.setString(i++, data.getCreatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

            } else {
//                ps.setString(i++, data.getDocNo());
                if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                    ps.setString(i++, data.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
//                ps.setBoolean(i++, data.getActive());
            }

            ps.setDate(i++, AppUtil.toDateSql(data.getReceiptDate()));

            if (data.getTaxFormRefundId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setTaxFormRefundId(id);
            } else {

                ps.setInt(i++, data.getTaxFormRefundId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="INSERT TAXFORM_REFUND_FILE">
            if (data.getTaxFormRefundId() != 0) {

                //<editor-fold defaultstate="collapsed" desc="DELETE ALL">
                i = 1;

                sql = "DELETE FROM TAX_FORM_REFUND_FILE WHERE TAX_FORM_REFUND_ID = ?";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getTaxFormRefundId());

                ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT BillPaymentFile">
                if (data.getBillPaymentFileList() != null && !data.getBillPaymentFileList().isEmpty()) {
                    for (TaxFormRefundFileModel file : data.getBillPaymentFileList()) {
                        if (data.getTaxFormRefundId() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM_REFUND_FILE("
                                    + " TAX_FORM_REFUND_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_NAME"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxFormRefundId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT AuthorizeFile">
                if (data.getAuthorizeFileList() != null && !data.getAuthorizeFileList().isEmpty()) {
                    for (TaxFormRefundFileModel file : data.getAuthorizeFileList()) {
                        if (data.getTaxFormRefundId() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM_REFUND_FILE("
                                    + " TAX_FORM_REFUND_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_NAME"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxFormRefundId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT CardFileList">
                if (data.getIdCardFileList() != null && !data.getIdCardFileList().isEmpty()) {
                    for (TaxFormRefundFileModel file : data.getIdCardFileList()) {
                        if (data.getTaxFormRefundId() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM_REFUND_FILE("
                                    + " TAX_FORM_REFUND_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_NAME"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxFormRefundId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="INSERT BookBankFileList">
                if (data.getBookBankFileList() != null && !data.getBookBankFileList().isEmpty()) {
                    for (TaxFormRefundFileModel file : data.getBookBankFileList()) {
                        if (data.getTaxFormRefundId() != 0) {
                            i = 1;

                            sql = "INSERT INTO TAX_FORM_REFUND_FILE("
                                    + " TAX_FORM_REFUND_ID "
                                    + ",FILE_NAME"
                                    + ",PATH_NAME"
                                    + ",FILE_CATE"
                                    + ",FILE_TYPE"
                                    + ") VALUES(?,?,?,?,?)";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, data.getTaxFormRefundId());
                            ps.setString(i++, file.getFileName());
                            ps.setString(i++, file.getPathFile());
                            ps.setString(i++, file.getFileCate());
                            ps.setString(i++, file.getFileType());

                            result = ps.executeUpdate() != 0;
                        }
                    }
                }
                //</editor-fold>
            }
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData deleteData(List<Integer> idList, FilterTaxFormModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id list">
            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM03_STATION_LOG">
                sql = " DELETE FROM TAX_FORM_REFUND_LOG "
                        + " WHERE TAX_FORM_REFUND_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM_REFUND_FILE">
                sql = " DELETE FROM TAX_FORM_REFUND_FILE "
                        + " WHERE TAX_FORM_REFUND_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="TAX_FORM_REFUND">
                sql = " DELETE FROM TAX_FORM_REFUND "
                        + " WHERE TAX_FORM_REFUND_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);
                result = ps.executeUpdate();
                //</editor-fold>
            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    public ResultData accept(TaxFormRefundModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="UPDATE TAXREFUND">
            sql = " UPDATE  TAX_FORM_REFUND "
                    + "SET "
                    + "STATUS = ?"
                    + ",STATUS_NAME = ?"
                    + ",ACCEPT_BY = ?"
                    + ",ACCEPT_DATE = SYSDATE "
                    //                    + ",DOC_NO1 = ?"
                    //                    + ",DOC_NO2 = ?"
                    //                    + ",PLACE = ?"
                    //                    + ",DOC_DATE = ? "

                    + "WHERE TAX_FORM_REFUND_ID = ? ";
//            , ACCEPT_BY , ACCEPT_DATE
//    docNo1 , docNo2 , PLACE , DOC_DATE
            i = 1;
            ps.setSql(sql);

            ps.setString(i++, data.getStatus());
            ps.setString(i++, data.getStatusName());
            ps.setString(i++, data.getAcceptBy());
//            ps.setString(i++, data.getDocNo1());
//            ps.setString(i++, data.getDocNo2());
//            ps.setString(i++, data.getPlace());
//            ps.setDate(i++, AppUtil.toDateSql(data.getDocDate()));

            ps.setInt(i++, data.getTaxFormRefundId());
            result = ps.executeUpdate();
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="insert  TAXREFUND LOG">
            sql = " INSERT INTO TAX_FORM_REFUND_LOG "
                    + "(TAX_FORM_REFUND_ID,STATUS,STATUS_NAME,ACTION_DATE,ACTION_BY) "
                    + "VALUES "
                    + "(?,?,?,SYSDATE,?)";
//            , ACCEPT_BY , ACCEPT_DATE
//    docNo1 , docNo2 , PLACE , DOC_DATE
            i = 1;
            ps.setSql(sql);

            ps.setInt(i++, data.getTaxFormRefundId());
            ps.setString(i++, data.getStatus());
            ps.setString(i++, data.getStatusName());
            ps.setString(i++, data.getAcceptBy());

            result = ps.executeUpdate();

//</editor-fold>
            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData approve(TaxFormRefundModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="UPDATE TAXREFUND">
            sql = " UPDATE  TAX_FORM_REFUND "
                    + "SET "
                    + "STATUS = ?"
                    + ",STATUS_NAME = ?"
                    + ",REFUND_BY = ?"
                    + ",REFUND_DATE = SYSDATE "
                    + ",DOC_NO1 = ?"
                    + ",DOC_NO2 = ?"
                    + ",PLACE = ?"
                    + ",DOC_REFUND_DATE = ? "
                    + "WHERE TAX_FORM_REFUND_ID = ? ";
//            , ACCEPT_BY , ACCEPT_DATE
//    docNo1 , docNo2 , PLACE , DOC_DATE
            i = 1;
            ps.setSql(sql);

            ps.setString(i++, data.getStatus());
            ps.setString(i++, data.getStatusName());
            ps.setString(i++, data.getRefundBy());

            ps.setString(i++, data.getDocNo1());
            ps.setString(i++, data.getDocNo2());
            ps.setString(i++, data.getPlace());
            ps.setDate(i++, AppUtil.toDateSql(data.getDocRefundDate()));

            ps.setInt(i++, data.getTaxFormRefundId());
            result = ps.executeUpdate();
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="insert  TAXREFUND LOG">
            sql = " INSERT INTO TAX_FORM_REFUND_LOG "
                    + "(TAX_FORM_REFUND_ID,STATUS,STATUS_NAME,ACTION_DATE,ACTION_BY) "
                    + "VALUES "
                    + "(?,?,?,SYSDATE,?)";
//            , ACCEPT_BY , ACCEPT_DATE
//    docNo1 , docNo2 , PLACE , DOC_DATE
            i = 1;
            ps.setSql(sql);

            ps.setInt(i++, data.getTaxFormRefundId());
            ps.setString(i++, data.getStatus());
            ps.setString(i++, data.getStatusName());
            ps.setString(i++, data.getRefundBy());

            result = ps.executeUpdate();

//</editor-fold>
            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData reject(TaxFormRefundModel data) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="UPDATE TAXREFUND">
            sql = " UPDATE  TAX_FORM_REFUND "
                    + "SET "
                    + "STATUS = ?"
                    + ",STATUS_NAME = ?"
                    + ",REJECT_REMARK = ?"
                    + ",REJECT_BY = ?"
                    + ",REJECT_DATE = SYSDATE "
                    + "WHERE TAX_FORM_REFUND_ID = ? ";
//            , ACCEPT_BY , ACCEPT_DATE
//    docNo1 , docNo2 , PLACE , DOC_DATE
            i = 1;
            ps.setSql(sql);

            ps.setString(i++, data.getStatus());
            ps.setString(i++, data.getStatusName());
            ps.setString(i++, data.getRejectRemark());
            ps.setString(i++, data.getRejectBy());

            ps.setInt(i++, data.getTaxFormRefundId());
            result = ps.executeUpdate();
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="insert  TAXREFUND LOG">
            sql = " INSERT INTO TAX_FORM_REFUND_LOG "
                    + "(TAX_FORM_REFUND_ID,STATUS,STATUS_NAME,REMARK,ACTION_DATE,ACTION_BY) "
                    + "VALUES "
                    + "(?,?,?,?,SYSDATE,?)";
//            , ACCEPT_BY , ACCEPT_DATE
//    docNo1 , docNo2 , PLACE , DOC_DATE
            i = 1;
            ps.setSql(sql);

            ps.setInt(i++, data.getTaxFormRefundId());
            ps.setString(i++, data.getStatus());
            ps.setString(i++, data.getStatusName());
            ps.setString(i++, data.getRejectRemark());
            ps.setString(i++, data.getRejectBy());

            result = ps.executeUpdate();

//</editor-fold>
            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

}
