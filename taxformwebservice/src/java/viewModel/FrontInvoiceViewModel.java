/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.*;

/**
 *
 * @author User
 */
public class FrontInvoiceViewModel {
    private String invoiceId;
    private String taxNo;//เลขประจำตัวผู้เสียภาษี
    private String taxFrom03Id;//รหัส TAX_FROM03_ID
    private String stationId; //รหัสสถานประกอบการค้าปลีก
    private String stationName; //ชื่อสถานประกอบการค้าปลีก
    
    private String dueDate;//วันที่ครบกำหนดชำระ 
    private String taxTotal;//ยอดเงินภาษี
    private String extraMoney;//เงินเพิ่ม
    private String totalAmount;//จำนวนเงินรวมที่ต้องชำระ (บาท)
    
    private int yearly; //ประจำปี
    private int monthly; //ประจำเดือน
    private String monthName; //ชื่อเดือน
    
    private String paymentStatus;//สถานะการชำระเงิน (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
    private String paymentMethod;//วิธีการชำระ (1 =เงินสด, 2 = บัตรเครดิต, 3 = บัตรเดบิต, 4 = ธนาณัติ, 5 =  เช็ค , 6 = QR Code ผ่านเครื่อง EDC)
    private String paymentChannel;//ชื่อช่องทางการชำระ
    private String paymentDate;//วันที่รับชำระ รูปแบบจะเป็น dd/mm/yyyy 
    private String paymentTime;//เวลาที่รับชำระ รูปแบบจะเป็น hh:mm 
    
    private String linkExportFrontInvoice;//ลิงค์สำหรับแสดงใบแจ้งขำระ
    private String linkExportFrontReceipt;//ลิงค์สำหรับแสดงใบเสร็จรับเงิน

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getTaxFrom03Id() {
        return taxFrom03Id;
    }

    public void setTaxFrom03Id(String taxFrom03Id) {
        this.taxFrom03Id = taxFrom03Id;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(String taxTotal) {
        this.taxTotal = taxTotal;
    }

    public String getExtraMoney() {
        return extraMoney;
    }

    public void setExtraMoney(String extraMoney) {
        this.extraMoney = extraMoney;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getYearly() {
        return yearly;
    }

    public void setYearly(int yearly) {
        this.yearly = yearly;
    }

    public int getMonthly() {
        return monthly;
    }

    public void setMonthly(int monthly) {
        this.monthly = monthly;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getLinkExportFrontInvoice() {
        return linkExportFrontInvoice;
    }

    public void setLinkExportFrontInvoice(String linkExportFrontInvoice) {
        this.linkExportFrontInvoice = linkExportFrontInvoice;
    }

    public String getLinkExportFrontReceipt() {
        return linkExportFrontReceipt;
    }

    public void setLinkExportFrontReceipt(String linkExportFrontReceipt) {
        this.linkExportFrontReceipt = linkExportFrontReceipt;
    }
    
}
