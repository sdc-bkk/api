/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class FilterTaxFormModel extends BaseClassFilterModel {
    private String id;  
    private String retailId;  
    private String stationId;  
    private String taxForm01StationId;
    private String taxForm01RetailId;
    private String taxForm02Id;
    private String taxForm03Id;
    private String taxFormRefundId;
    private String name;    
    private String docNo;    
    private String amphurId;    
    private String amphurName;    
    private String status;      
    private String statusAcc;    
    private String memberName;  
    private String taxNo;  
    private String stationName; 
    private String fileCate;       
    
    private String startDate;    
    private String endDate;        
    
    private String yearly; //ประจำปี
    private String monthly; //ประจำเดือน
    
    private String forType;//สำหรับการจำแนก back front
        
    private String ownerName;   
    private String officeId;   
    private String officeName;   
    
    private List<String> idList;

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getRetailId() {
        return retailId;
    }

    public void setRetailId(String retailId) {
        this.retailId = retailId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getForType() {
        return forType;
    }

    public void setForType(String forType) {
        this.forType = forType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaxForm01StationId() {
        return taxForm01StationId;
    }

    public void setTaxForm01StationId(String taxForm01StationId) {
        this.taxForm01StationId = taxForm01StationId;
    }

    public String getTaxForm01RetailId() {
        return taxForm01RetailId;
    }

    public void setTaxForm01RetailId(String taxForm01RetailId) {
        this.taxForm01RetailId = taxForm01RetailId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getYearly() {
        return yearly;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getStatusAcc() {
        return statusAcc;
    }

    public void setStatusAcc(String statusAcc) {
        this.statusAcc = statusAcc;
    }

    public String getTaxForm03Id() {
        return taxForm03Id;
    }

    public void setTaxForm03Id(String taxForm03Id) {
        this.taxForm03Id = taxForm03Id;
    }

    public String getTaxForm02Id() {
        return taxForm02Id;
    }

    public void setTaxForm02Id(String taxForm02Id) {
        this.taxForm02Id = taxForm02Id;
    }

    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(List<String> idList) {
        this.idList = idList;
    }

    public String getFileCate() {
        return fileCate;
    }

    public void setFileCate(String fileCate) {
        this.fileCate = fileCate;
    }
    public String getTaxFormRefundId() {
        return taxFormRefundId;
    }

    public void setTaxFormRefundId(String taxFormRefundId) {
        this.taxFormRefundId = taxFormRefundId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }
    
    
}
