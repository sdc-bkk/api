/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.BaseClassModel;
import utility.AppConfig;

/**
 *
 * @author User
 */
public class TaxForm02ViewModel  extends BaseClassModel{
    private String taxForm02Id;
    private String refRetailId;
    private String refStationId;
    private String refTaxNo;
    private String docNo; //เลขที่ทะเบียน
    private String docDate; //วันทีเลขที่ทะเบียน
    private String forType;//1=เลิกกิจการ,2=ย้ายสถาน , 3= เปลี่ยนเจ้าของ , 4 =เปลี่ยนชื่อสถาน ,5= อื่นๆ
    private String forTypeName;
    private String businessStatusId; //รหัสความประสงค์/สถานประกอบการค้าปลีก
    private String businessStatusName; //ความประสงค์/ชื่อสถานะสถานการค้าปลีก
    private String businessStatusCode; //ความประสงค์/ชื่อสถานะสถานการค้าปลีก
    private String cancelDate; //วันทีเลิกกิจการ
    
    private String changeDate; //วันทีเปลี่ยนแปลงที่ตั้ง
    private String addressOld; //ที่อยู่/ที่ตั้งสำนักงานเลขที่ เดิม
    private String houseNo; //เลขที่ประจำบ้าน
    private String address;
    private String soi;
    private String road;
    private String tambonId;
    private String tambonName;
    private String amphurId;
    private String amphurName;
    private String provinceId;
    private String provinceName;
    private String postcode;
    private String mobileOld; //เบอร์โทรศัพท์ เดิม
    private String mobile;
    
    private String stationCode; //ชื่อรหัสสถานประกอบการค้าปลีก
    private String stationNameOld; //ชื่อสถานประกอบการค้าปลีก เดิม
    private String stationName;
    
    private String ownerNameOld; //ชื่อผู้ประกอบการค้าปลีก เดิม
    private String ownerName;
    private String taxNoOld; //เลขประจำตัวผู้เสียภาษีอากร เดิม
    private String taxNo;
    private String customerTypeOld; //ประเภทผู้เสียภาษี เดิม
    private String customerType;
    private String idCardOld; //เลขที่บัตรประจำตัวประชาชน เดิม
    private String idCard;
    private String idCardAtOld; //ออกให้ ณ ที่ว่าการ เดิม
    private String idCardAt;
    private String corpNoOld; //เลขทะเบียนนิติบุคคล เดิม
    private String corpNo;
    private String corpDateOld; //วันทีออกเลขทะเบียนนิติบุคคล เดิม
    private String corpDate;    
    
    private String addressRetail;
    private String soiRetail;
    private String roadRetail;
    private String tambonIdRetail;
    private String tambonNameRetail;
    private String amphurIdRetail;
    private String amphurNameRetail;
    private String provinceIdRetail;
    private String provinceNameRetail;
    private String postcodeRetail;
    private String mobileRetail; //เบอร์โทรศัพท์ เดิม
    
    private String addressStation;
    private String soiStation;
    private String roadStation;
    private String tambonIdStation;
    private String tambonNameStation;
    private String amphurIdStation;
    private String amphurNameStation;
    private String provinceIdStation;
    private String provinceNameStation;
    private String postcodeStation;
    private String mobileStation; //เบอร์โทรศัพท์ 
    
    private String cause; //เนื่องจาก
    private String other; //ระบุอื่นๆ
    private Double taxRemain; //ภาษีค้างชำระตามบัญชีแนบ
    private Double taxOilGas; //น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษี
    
    private String status; //สถานะ(0=สร้าง,1=รอตรวจสอบ,2=ไม่ผ่าน,3=อนุมัติ)
    private String statusName;
    
    private Integer fileNumE;//จำนวนหน้า หนังสือรับรอง
    private Integer fileNumA;//จำนวนหน้า หนังสือมอบอำนาจ
    private Integer fileNumR;//จำนวนหน้า บัญชีรายการค้างชำระภาษี
    private Integer fileNumO;//จำนวนหน้า เอกสารอื่นๆ
    
    private List<TaxForm02FileViewModel> commerceFileList; //สำเนากระทรวงพาณิชย์
    private List<TaxForm02FileViewModel> authorizeFileList;//สำเนามอบอำนาจ
    private List<TaxForm02FileViewModel> taxRemainFileList;//สำเนาบัญชีสินค้า
    private List<TaxForm02FileViewModel> otherFileList;//สำเนาอื่นๆ
    
    private List<DrpDownViewModel> businessStatusList;//รายการความประสงค์
    private RetailViewModel retail;

    private List<String> idList;
    
    private String linkExportTaxform02;
    
    private String office;
    private String officeName;
    private String officeCode;

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getAddressStation() {
        return addressStation;
    }

    public void setAddressStation(String addressStation) {
        this.addressStation = addressStation;
    }

    public String getSoiStation() {
        return soiStation;
    }

    public void setSoiStation(String soiStation) {
        this.soiStation = soiStation;
    }

    public String getRoadStation() {
        return roadStation;
    }

    public void setRoadStation(String roadStation) {
        this.roadStation = roadStation;
    }

    public String getTambonIdStation() {
        return tambonIdStation;
    }

    public void setTambonIdStation(String tambonIdStation) {
        this.tambonIdStation = tambonIdStation;
    }

    public String getTambonNameStation() {
        return tambonNameStation;
    }

    public void setTambonNameStation(String tambonNameStation) {
        this.tambonNameStation = tambonNameStation;
    }

    public String getAmphurIdStation() {
        return amphurIdStation;
    }

    public void setAmphurIdStation(String amphurIdStation) {
        this.amphurIdStation = amphurIdStation;
    }

    public String getAmphurNameStation() {
        return amphurNameStation;
    }

    public void setAmphurNameStation(String amphurNameStation) {
        this.amphurNameStation = amphurNameStation;
    }

    public String getProvinceIdStation() {
        return provinceIdStation;
    }

    public void setProvinceIdStation(String provinceIdStation) {
        this.provinceIdStation = provinceIdStation;
    }

    public String getProvinceNameStation() {
        return provinceNameStation;
    }

    public void setProvinceNameStation(String provinceNameStation) {
        this.provinceNameStation = provinceNameStation;
    }

    public String getPostcodeStation() {
        return postcodeStation;
    }

    public void setPostcodeStation(String postcodeStation) {
        this.postcodeStation = postcodeStation;
    }

    public String getMobileStation() {
        return mobileStation;
    }

    public void setMobileStation(String mobileStation) {
        this.mobileStation = mobileStation;
    }

    public String getRefTaxNo() {
        return refTaxNo;
    }

    public String getAddressRetail() {
        return addressRetail;
    }

    public void setAddressRetail(String addressRetail) {
        this.addressRetail = addressRetail;
    }

    public String getSoiRetail() {
        return soiRetail;
    }

    public void setSoiRetail(String soiRetail) {
        this.soiRetail = soiRetail;
    }

    public String getRoadRetail() {
        return roadRetail;
    }

    public void setRoadRetail(String roadRetail) {
        this.roadRetail = roadRetail;
    }

    public String getTambonIdRetail() {
        return tambonIdRetail;
    }

    public void setTambonIdRetail(String tambonIdRetail) {
        this.tambonIdRetail = tambonIdRetail;
    }

    public String getTambonNameRetail() {
        return tambonNameRetail;
    }

    public void setTambonNameRetail(String tambonNameRetail) {
        this.tambonNameRetail = tambonNameRetail;
    }

    public String getAmphurIdRetail() {
        return amphurIdRetail;
    }

    public void setAmphurIdRetail(String amphurIdRetail) {
        this.amphurIdRetail = amphurIdRetail;
    }

    public String getAmphurNameRetail() {
        return amphurNameRetail;
    }

    public void setAmphurNameRetail(String amphurNameRetail) {
        this.amphurNameRetail = amphurNameRetail;
    }

    public String getProvinceIdRetail() {
        return provinceIdRetail;
    }

    public void setProvinceIdRetail(String provinceIdRetail) {
        this.provinceIdRetail = provinceIdRetail;
    }

    public String getProvinceNameRetail() {
        return provinceNameRetail;
    }

    public void setProvinceNameRetail(String provinceNameRetail) {
        this.provinceNameRetail = provinceNameRetail;
    }

    public String getPostcodeRetail() {
        return postcodeRetail;
    }

    public void setPostcodeRetail(String postcodeRetail) {
        this.postcodeRetail = postcodeRetail;
    }

    public String getMobileRetail() {
        return mobileRetail;
    }

    public void setMobileRetail(String mobileRetail) {
        this.mobileRetail = mobileRetail;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public void setRefTaxNo(String refTaxNo) {
        this.refTaxNo = refTaxNo;
    }

    public String getTaxForm02Id() {
        return taxForm02Id;
    }

    public void setTaxForm02Id(String taxForm02Id) {
        this.taxForm02Id = taxForm02Id;
    }

    public String getRefRetailId() {
        return refRetailId;
    }

    public void setRefRetailId(String refRetailId) {
        this.refRetailId = refRetailId;
    }

    public String getRefStationId() {
        return refStationId;
    }

    public void setRefStationId(String refStationId) {
        this.refStationId = refStationId;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getForType() {
        return forType;
    }

    public void setForType(String forType) {
        this.forType = forType;
    }

    public String getForTypeName() {
        return forTypeName;
    }

    public void setForTypeName(String forTypeName) {
        this.forTypeName = forTypeName;
    }

    public String getBusinessStatusId() {
        return businessStatusId;
    }

    public void setBusinessStatusId(String businessStatusId) {
        this.businessStatusId = businessStatusId;
    }

    public String getBusinessStatusName() {
        return businessStatusName;
    }

    public void setBusinessStatusName(String businessStatusName) {
        this.businessStatusName = businessStatusName;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getAddressOld() {
        return addressOld;
    }

    public void setAddressOld(String addressOld) {
        this.addressOld = addressOld;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getTambonName() {
        return tambonName;
    }

    public void setTambonName(String tambonName) {
        this.tambonName = tambonName;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getMobileOld() {
        return mobileOld;
    }

    public void setMobileOld(String mobileOld) {
        this.mobileOld = mobileOld;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getStationNameOld() {
        return stationNameOld;
    }

    public void setStationNameOld(String stationNameOld) {
        this.stationNameOld = stationNameOld;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getOwnerNameOld() {
        return ownerNameOld;
    }

    public void setOwnerNameOld(String ownerNameOld) {
        this.ownerNameOld = ownerNameOld;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTaxNoOld() {
        return taxNoOld;
    }

    public void setTaxNoOld(String taxNoOld) {
        this.taxNoOld = taxNoOld;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getCustomerTypeOld() {
        return customerTypeOld;
    }

    public void setCustomerTypeOld(String customerTypeOld) {
        this.customerTypeOld = customerTypeOld;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getIdCardOld() {
        return idCardOld;
    }

    public void setIdCardOld(String idCardOld) {
        this.idCardOld = idCardOld;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIdCardAtOld() {
        return idCardAtOld;
    }

    public void setIdCardAtOld(String idCardAtOld) {
        this.idCardAtOld = idCardAtOld;
    }

    public String getIdCardAt() {
        return idCardAt;
    }

    public void setIdCardAt(String idCardAt) {
        this.idCardAt = idCardAt;
    }

    public String getCorpNoOld() {
        return corpNoOld;
    }

    public void setCorpNoOld(String corpNoOld) {
        this.corpNoOld = corpNoOld;
    }

    public String getCorpNo() {
        return corpNo;
    }

    public void setCorpNo(String corpNo) {
        this.corpNo = corpNo;
    }

    public String getCorpDateOld() {
        return corpDateOld;
    }

    public void setCorpDateOld(String corpDateOld) {
        this.corpDateOld = corpDateOld;
    }

    public String getCorpDate() {
        return corpDate;
    }

    public void setCorpDate(String corpDate) {
        this.corpDate = corpDate;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Double getTaxRemain() {
        return taxRemain;
    }

    public void setTaxRemain(Double taxRemain) {
        this.taxRemain = taxRemain;
    }

    public Double getTaxOilGas() {
        return taxOilGas;
    }

    public void setTaxOilGas(Double taxOilGas) {
        this.taxOilGas = taxOilGas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Integer getFileNumE() {
        return fileNumE;
    }

    public void setFileNumE(Integer fileNumE) {
        this.fileNumE = fileNumE;
    }

    public Integer getFileNumA() {
        return fileNumA;
    }

    public void setFileNumA(Integer fileNumA) {
        this.fileNumA = fileNumA;
    }

    public Integer getFileNumR() {
        return fileNumR;
    }

    public void setFileNumR(Integer fileNumR) {
        this.fileNumR = fileNumR;
    }

    public Integer getFileNumO() {
        return fileNumO;
    }

    public void setFileNumO(Integer fileNumO) {
        this.fileNumO = fileNumO;
    }

    public List<TaxForm02FileViewModel> getCommerceFileList() {
        return commerceFileList;
    }

    public void setCommerceFileList(List<TaxForm02FileViewModel> commerceFileList) {
        this.commerceFileList = commerceFileList;
    }

    public List<TaxForm02FileViewModel> getAuthorizeFileList() {
        return authorizeFileList;
    }

    public void setAuthorizeFileList(List<TaxForm02FileViewModel> authorizeFileList) {
        this.authorizeFileList = authorizeFileList;
    }

    public List<TaxForm02FileViewModel> getTaxRemainFileList() {
        return taxRemainFileList;
    }

    public void setTaxRemainFileList(List<TaxForm02FileViewModel> taxRemainFileList) {
        this.taxRemainFileList = taxRemainFileList;
    }

    public List<TaxForm02FileViewModel> getOtherFileList() {
        return otherFileList;
    }

    public void setOtherFileList(List<TaxForm02FileViewModel> otherFileList) {
        this.otherFileList = otherFileList;
    }

    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(List<String> idList) {
        this.idList = idList;
    }

    public String getBusinessStatusCode() {
        return businessStatusCode;
    }

    public void setBusinessStatusCode(String businessStatusCode) {
        this.businessStatusCode = businessStatusCode;
    }

    public RetailViewModel getRetail() {
        return retail;
    }

    public void setRetail(RetailViewModel retail) {
        this.retail = retail;
    }

    public String getLinkExportTaxform02() {
        return linkExportTaxform02;
    }

    public void setLinkExportTaxform02(String linkExportTaxform02) {
        this.linkExportTaxform02 = linkExportTaxform02;
    }

    public List<DrpDownViewModel> getBusinessStatusList() {
        return businessStatusList;
    }

    public void setBusinessStatusList(List<DrpDownViewModel> businessStatusList) {
        this.businessStatusList = businessStatusList;
    }

}
