/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.*;

/**
 *
 * @author User
 */
public class RetailStationFileViewModel  extends BaseFileModel {
    private String retailStationId;

    public String getRetailStationId() {
        return retailStationId;
    }

    public void setRetailStationId(String retailStationId) {
        this.retailStationId = retailStationId;
    }
    
}
