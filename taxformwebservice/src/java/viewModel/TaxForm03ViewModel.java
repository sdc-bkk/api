/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.*;

/**
 *
 * @author User
 */
public class TaxForm03ViewModel extends BaseClassModel{
    private String taxForm03Id;
    private String refRetailId; //รหัสผู้ประกอบการค้าปลีก
    private String refStationId; //รหัสสถานประกอบการค้าปลีก
    
    private String docNo; //เลขที่ทะเบียน
    private String docDate; //วันทีเลขที่ทะเบียน
    
    private String dueDate; //วันทีเลขที่ทะเบียน
    
    private int yearly; //ประจำปี
    private int monthly; //ประจำเดือน
    private String monthName; //ชื่อเดือน
    
    private String statusAcc;//สถานะการชำระภาษี 1 = ภายในกำหนดเวลา , 2 = เกินกำหนดเวลา , 3 = ยื่นเพิ่มเติมครั้งที่...
    private String statusAccDesc;//ชื่อสถานะการชำระภาษี
    
    private String status;//สถานะของฟอร์ม(0="สร้าง",1="รอตรวจสอบ",2="ไม่ผ่าน" ,3, ="อนุมัติ"); 
    private String statusName;
    private int addTimes; // ครั้งที่ในการยื่นเพิ่มเติม
    private int sequence; // ลำดับที่ในการยื่นแบบในแต่ละเดือน
    
    private int extraMonth; //ประจำปี
    private double extraMoney; // จำนวนเงินเพิ่ม
    private double extraRate; // จำนวนอัตราภาษีเพิ่ม
    private String extraStartDate; // วันที่เริ่มต้นในการคิดเงินเพิ่ม
    private String extraEndDate; // วันที่สิ้นสุดในการคิดเงินพิ่ม
    private double taxTotal; // จำนวนเงินภาษีรวม
    private List<TaxForm03FileViewModel> accountList; //สำเนางบเดือน
    
    private List<TaxForm03SummaryViewModel> summaryList; //รายการงบเดือน
    private List<TaxForm03DetailViewModel> detailList; //รายการน้ำมัน่จำหน่ายได้
    
    private RetailViewModel refRetail;
    private RetailStationViewModel refStation;
    
    
    private String stationName; //ชื่อปั๊ม
    private String amphurName; //ชื่อเขต
    
    private String linkExportTaxform03;
    private String linkExportInvoice;
    
    private String refOffice; //รหัสเขต
    private String refOfficeName; //ชื่อเขต
    private String refOfficeCode; //ชื่อเขต

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    
    public String getRefOfficeCode() {
        return refOfficeCode;
    }

    public void setRefOfficeCode(String refOfficeCode) {
        this.refOfficeCode = refOfficeCode;
    }

    public int getExtraMonth() {
        return extraMonth;
    }

    public void setExtraMonth(int extraMonth) {
        this.extraMonth = extraMonth;
    }

    public String getTaxForm03Id() {
        return taxForm03Id;
    }

    public void setTaxForm03Id(String taxForm03Id) {
        this.taxForm03Id = taxForm03Id;
    }

    public String getRefRetailId() {
        return refRetailId;
    }

    public void setRefRetailId(String refRetailId) {
        this.refRetailId = refRetailId;
    }

    public String getRefStationId() {
        return refStationId;
    }

    public void setRefStationId(String refStationId) {
        this.refStationId = refStationId;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getStatusAcc() {
        return statusAcc;
    }

    public void setStatusAcc(String statusAcc) {
        this.statusAcc = statusAcc;
    }

    public String getStatusAccDesc() {
        return statusAccDesc;
    }

    public void setStatusAccDesc(String statusAccDesc) {
        this.statusAccDesc = statusAccDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public int getAddTimes() {
        return addTimes;
    }

    public void setAddTimes(int addTimes) {
        this.addTimes = addTimes;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public double getExtraMoney() {
        return extraMoney;
    }

    public void setExtraMoney(double extraMoney) {
        this.extraMoney = extraMoney;
    }

    public double getExtraRate() {
        return extraRate;
    }

    public void setExtraRate(double extraRate) {
        this.extraRate = extraRate;
    }

    public String getExtraStartDate() {
        return extraStartDate;
    }

    public void setExtraStartDate(String extraStartDate) {
        this.extraStartDate = extraStartDate;
    }

    public String getExtraEndDate() {
        return extraEndDate;
    }

    public void setExtraEndDate(String extraEndDate) {
        this.extraEndDate = extraEndDate;
    }

    public double getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(double taxTotal) {
        this.taxTotal = taxTotal;
    }

    public List<TaxForm03FileViewModel> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<TaxForm03FileViewModel> accountList) {
        this.accountList = accountList;
    }

    public List<TaxForm03SummaryViewModel> getSummaryList() {
        return summaryList;
    }

    public void setSummaryList(List<TaxForm03SummaryViewModel> summaryList) {
        this.summaryList = summaryList;
    }

    public List<TaxForm03DetailViewModel> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<TaxForm03DetailViewModel> detailList) {
        this.detailList = detailList;
    }

    public RetailViewModel getRefRetail() {
        return refRetail;
    }

    public void setRefRetail(RetailViewModel refRetail) {
        this.refRetail = refRetail;
    }

    public RetailStationViewModel getRefStation() {
        return refStation;
    }

    public void setRefStation(RetailStationViewModel refStation) {
        this.refStation = refStation;
    }

    public int getYearly() {
        return yearly;
    }

    public void setYearly(int yearly) {
        this.yearly = yearly;
    }

    public int getMonthly() {
        return monthly;
    }

    public void setMonthly(int monthly) {
        this.monthly = monthly;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getLinkExportTaxform03() {
        return linkExportTaxform03;
    }

    public void setLinkExportTaxform03(String linkExportTaxform03) {
        this.linkExportTaxform03 = linkExportTaxform03;
    }

    public String getLinkExportInvoice() {
        return linkExportInvoice;
    }

    public void setLinkExportInvoice(String linkExportInvoice) {
        this.linkExportInvoice = linkExportInvoice;
    }

    public String getRefOffice() {
        return refOffice;
    }

    public void setRefOffice(String refOffice) {
        this.refOffice = refOffice;
    }

    public String getRefOfficeName() {
        return refOfficeName;
    }

    public void setRefOfficeName(String refOfficeName) {
        this.refOfficeName = refOfficeName;
    }
    
    
}
