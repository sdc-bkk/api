/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.BaseFileModel;

/**
 *
 * @author User
 */
public class TaxForm02FileViewModel extends BaseFileModel {
    private String taxForm02Id;

    public String getTaxForm02Id() {
        return taxForm02Id;
    }

    public void setTaxForm02Id(String taxForm02Id) {
        this.taxForm02Id = taxForm02Id;
    }
    
}
