/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.BaseClassModel;

/**
 *
 * @author DELL
 */
public class DocFormViewModel extends BaseClassModel {

    private String retailId;
    private String retailStationId;
    private String refRetailId;
    private String docNo;//เลขทำเบียนรับ
    private String docDate;//วันที่รับ
    private String ownerName;//ชื่อผู้ประกอบการ
    private String stationName;//ชื่อสถานค้าปลีก
    private String taxNo;//เลขประจำตัวผู้เสียภาษีอากร
    private String refOfficeName;

    public String getRetailId() {
        return retailId;
    }

    public void setRetailId(String retailId) {
        this.retailId = retailId;
    }

    public String getRetailStationId() {
        return retailStationId;
    }

    public void setRetailStationId(String retailStationId) {
        this.retailStationId = retailStationId;
    }

    public String getRefRetailId() {
        return refRetailId;
    }

    public void setRefRetailId(String refRetailId) {
        this.refRetailId = refRetailId;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getRefOfficeName() {
        return refOfficeName;
    }

    public void setRefOfficeName(String refOfficeName) {
        this.refOfficeName = refOfficeName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }


}
