/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.*;

/**
 *
 * @author User
 */
public class RetailFileViewModel extends BaseFileModel {
    private int retailId;

    public int getRetailId() {
        return retailId;
    }

    public void setRetailId(int retailId) {
        this.retailId = retailId;
    }
    
}
