/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

/**
 *
 * @author User
 */
public class FilterTaxForm02Model extends BaseClassFilterModel {
    
    private String taxForm02Id;
    private String docNo; //เลขที่ทะเบียน
    private String startDocDate; //วันทีเลขที่ทะเบียน
    private String endDocDate; 
    
    private String amphurId;
    private String amphurName; //เขต
    private String businessStatusId; //รหัสความประสงค์/สถานประกอบการค้าปลีก
    private String businessStatusName; //ความประสงค์/ชื่อสถานะสถานการค้าปลีก
    
    private String ownerName; //ชื่อผู้ประกอบการค้าปลีก
    private String stationCode; //ชื่อรหัสสถานประกอบการค้าปลีก
    private String stationName; //ชื่อสถานประกอบการค้าปลีก เดิม
    private String status;     
    private String username;  
    
    private String memberName;  
    private String taxNo;  

    public String getTaxForm02Id() {
        return taxForm02Id;
    }

    public void setTaxForm02Id(String taxForm02Id) {
        this.taxForm02Id = taxForm02Id;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getStartDocDate() {
        return startDocDate;
    }

    public void setStartDocDate(String startDocDate) {
        this.startDocDate = startDocDate;
    }

    public String getEndDocDate() {
        return endDocDate;
    }

    public void setEndDocDate(String endDocDate) {
        this.endDocDate = endDocDate;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getBusinessStatusId() {
        return businessStatusId;
    }

    public void setBusinessStatusId(String businessStatusId) {
        this.businessStatusId = businessStatusId;
    }

    public String getBusinessStatusName() {
        return businessStatusName;
    }

    public void setBusinessStatusName(String businessStatusName) {
        this.businessStatusName = businessStatusName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }
    
}
