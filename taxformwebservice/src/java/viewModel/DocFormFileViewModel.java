/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.BaseFileModel;

/**
 *
 * @author DELL
 */
public class DocFormFileViewModel extends BaseFileModel {

    private String taxForm01RetailId;
    private String retailAddFileId;
    private String retailId;
    private String createdDate;
    private String updatedBy;
    private String docNo;
    private String docName;
    private String remark;
    private String fileNameOther;
    private String retailStationAddFileId;
    private String retailStationId;

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getTaxForm01RetailId() {
        return taxForm01RetailId;
    }

    public void setTaxForm01RetailId(String taxForm01RetailId) {
        this.taxForm01RetailId = taxForm01RetailId;
    }

    public String getRetailId() {
        return retailId;
    }

    public void setRetailId(String retailId) {
        this.retailId = retailId;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getRetailAddFileId() {
        return retailAddFileId;
    }

    public void setRetailAddFileId(String retailAddFileId) {
        this.retailAddFileId = retailAddFileId;
    }

    public String getFileNameOther() {
        return fileNameOther;
    }

    public void setFileNameOther(String fileNameOther) {
        this.fileNameOther = fileNameOther;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getRetailStationAddFileId() {
        return retailStationAddFileId;
    }

    public void setRetailStationAddFileId(String retailStationAddFileId) {
        this.retailStationAddFileId = retailStationAddFileId;
    }

    public String getRetailStationId() {
        return retailStationId;
    }

    public void setRetailStationId(String retailStationId) {
        this.retailStationId = retailStationId;
    }

}
