/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

/**
 *
 * @author DELL
 */
public class FilterDocFormModel {

    private String retailId;
    private String retailStationId;
    private String filePath;

    public String getRetailId() {
        return retailId;
    }

    public void setRetailId(String retailId) {
        this.retailId = retailId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    

    public String getRetailStationId() {
        return retailStationId;
    }

    public void setRetailStationId(String retailStationId) {
        this.retailStationId = retailStationId;
    }
}
