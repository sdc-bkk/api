/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

/**
 *
 * @author User
 */
public class MonthlyViewModel {
    private int yearly; //ประจำปี
    private int monthly; //ประจำเดือน
    private String monthName; //ชื่อเดือน
    
    private int waitSendNum; //จำนวนรายการที่รอส่งแบบ
    private int waitNum; //จำนวนรายการที่รออนุมัติ
    private int passNum; //จำนวนรายการที่อนุมัติ
    private int noPassNum; //จำนวนรายการที่ไม่อนุมัติ
    private int totalNum; //รายการทั้งหมด

    public int getWaitSendNum() {
        return waitSendNum;
    }

    public void setWaitSendNum(int waitSendNum) {
        this.waitSendNum = waitSendNum;
    }

    public int getYearly() {
        return yearly;
    }

    public void setYearly(int yearly) {
        this.yearly = yearly;
    }

    public int getMonthly() {
        return monthly;
    }

    public void setMonthly(int monthly) {
        this.monthly = monthly;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public int getWaitNum() {
        return waitNum;
    }

    public void setWaitNum(int waitNum) {
        this.waitNum = waitNum;
    }

    public int getPassNum() {
        return passNum;
    }

    public void setPassNum(int passNum) {
        this.passNum = passNum;
    }

    public int getNoPassNum() {
        return noPassNum;
    }

    public void setNoPassNum(int noPassNum) {
        this.noPassNum = noPassNum;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }
    
    
    
}
