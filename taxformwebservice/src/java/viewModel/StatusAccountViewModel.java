/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

/**
 *
 * @author User
 */
public class StatusAccountViewModel {
    
    private String refRetailStationId;
    private Integer yearly; //ประจำปี
    private Integer monthly; //ประจำเดือน
    private String statusAcc;
    private String statusAccDesc;
    private Integer addTimes;
    private double finesPerMonth;	//ค่าปรับ (ต่อเเดือน)
    private double finesTotal;	//ค่าปรับ (ต่อเเดือน)
    private String finesStartDate;	//วันที่เริ่มปรับ
    private String finesEndDate;	//สิ้นสุดปรับ
    private Integer finesMonth;	//วันที่เริ่มปรับ
    private String dueDatePayment;//วันครบกำหนดชำระ

    public String getDueDatePayment() {
        return dueDatePayment;
    }

    public void setDueDatePayment(String dueDatePayment) {
        this.dueDatePayment = dueDatePayment;
    }

    public String getStatusAcc() {
        return statusAcc;
    }

    public void setStatusAcc(String statusAcc) {
        this.statusAcc = statusAcc;
    }

    public String getStatusAccDesc() {
        return statusAccDesc;
    }

    public void setStatusAccDesc(String statusAccDesc) {
        this.statusAccDesc = statusAccDesc;
    }

    public Integer getAddTimes() {
        return addTimes;
    }

    public void setAddTimes(Integer addTimes) {
        this.addTimes = addTimes;
    }

    public String getRefRetailStationId() {
        return refRetailStationId;
    }

    public void setRefRetailStationId(String refRetailStationId) {
        this.refRetailStationId = refRetailStationId;
    }

    public Integer getYearly() {
        return yearly;
    }

    public void setYearly(Integer yearly) {
        this.yearly = yearly;
    }

    public Integer getMonthly() {
        return monthly;
    }

    public void setMonthly(Integer monthly) {
        this.monthly = monthly;
    }   

    public double getFinesPerMonth() {
        return finesPerMonth;
    }

    public void setFinesPerMonth(double finesPerMonth) {
        this.finesPerMonth = finesPerMonth;
    }

    public double getFinesTotal() {
        return finesTotal;
    }

    public void setFinesTotal(double finesTotal) {
        this.finesTotal = finesTotal;
    }

    public String getFinesStartDate() {
        return finesStartDate;
    }

    public void setFinesStartDate(String finesStartDate) {
        this.finesStartDate = finesStartDate;
    }

    public String getFinesEndDate() {
        return finesEndDate;
    }

    public void setFinesEndDate(String finesEndDate) {
        this.finesEndDate = finesEndDate;
    }

    public Integer getFinesMonth() {
        return finesMonth;
    }

    public void setFinesMonth(Integer finesMonth) {
        this.finesMonth = finesMonth;
    }
    
}
