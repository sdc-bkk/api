/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class StatusLogViewModel {
    private String taxForm01RetailId;
    private String taxForm02Id;
    private String taxForm03Id;
    private String taxForm01StationId;
    private String status;
    private String statusName;
    private String remark;
    private String actionBy;
    private String actionDate;
    
    private List<String> idList;

    public String getTaxForm03Id() {
        return taxForm03Id;
    }

    public void setTaxForm03Id(String taxForm03Id) {
        this.taxForm03Id = taxForm03Id;
    }

    public String getTaxForm01RetailId() {
        return taxForm01RetailId;
    }

    public void setTaxForm01RetailId(String taxForm01RetailId) {
        this.taxForm01RetailId = taxForm01RetailId;
    }

    public String getTaxForm01StationId() {
        return taxForm01StationId;
    }

    public void setTaxForm01StationId(String taxForm01StationId) {
        this.taxForm01StationId = taxForm01StationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getActionBy() {
        return actionBy;
    }

    public void setActionBy(String actionBy) {
        this.actionBy = actionBy;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(List<String> idList) {
        this.idList = idList;
    }

    public String getTaxForm02Id() {
        return taxForm02Id;
    }

    public void setTaxForm02Id(String taxForm02Id) {
        this.taxForm02Id = taxForm02Id;
    }
    
}
