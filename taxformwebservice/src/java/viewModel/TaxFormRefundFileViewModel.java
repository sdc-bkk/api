/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.BaseFileModel;

/**
 *
 * @author DELL
 */
public class TaxFormRefundFileViewModel extends BaseFileModel {
    private String taxFormRefundId;

    public String getTaxFormRefundId() {
        return taxFormRefundId;
    }

    public void setTaxFormRefundId(String taxFormRefundId) {
        this.taxFormRefundId = taxFormRefundId;
    }

}
