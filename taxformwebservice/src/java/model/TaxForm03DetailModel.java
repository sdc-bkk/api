/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class TaxForm03DetailModel {
    private int taxForm03Id;
    private int sequence; // ลำดับที่
    private int oilTypeId; // รหัสรายการน้ำมัน
    private String oilTypeName; // ชื่อรายการน้ำมัน
    
    private double balance; // จำนวนลิตร
    private double inQty; // จำนวนลิตร
    private double outQty; // จำนวนลิตร
    private double remain; // จำนวนลิตร

    public int getTaxForm03Id() {
        return taxForm03Id;
    }

    public void setTaxForm03Id(int taxForm03Id) {
        this.taxForm03Id = taxForm03Id;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getOilTypeId() {
        return oilTypeId;
    }

    public void setOilTypeId(int oilTypeId) {
        this.oilTypeId = oilTypeId;
    }

    public String getOilTypeName() {
        return oilTypeName;
    }

    public void setOilTypeName(String oilTypeName) {
        this.oilTypeName = oilTypeName;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getInQty() {
        return inQty;
    }

    public void setInQty(double inQty) {
        this.inQty = inQty;
    }

    public double getOutQty() {
        return outQty;
    }

    public void setOutQty(double outQty) {
        this.outQty = outQty;
    }

    public double getRemain() {
        return remain;
    }

    public void setRemain(double remain) {
        this.remain = remain;
    }
    
    
}
