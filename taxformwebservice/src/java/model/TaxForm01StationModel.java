/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author User
 */
public class TaxForm01StationModel extends BaseClassModel{
    private int taxForm01StationId;
    private int taxForm01RetailId;
    
    private int refStationId;
    private String stationCode;//รหัส generate auto
    private String stationName;//ชื่อสถานค้าปลีก
    private String branchName;//สาขา
    private List<DrpDownModel> businessTypeList;
    private String houseNo;//เลขประจำบ้าน
    private String address;
    private String soi;
    private String moo;
    private String road;
    private String tambonId;
    private String tambonName;
    private String amphurId;
    private String amphurName;
    private String provinceId;
    private String provinceName;
    private String postcode;
    private String mobile;
    private String email;
    private String status;
    
    private Integer houseFileNum;
    private Integer mapFileNum;
    
    private List<TaxForm01StationFileModel> houseFileList;
    private List<TaxForm01StationFileModel> mapFileList;

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public int getTaxForm01StationId() {
        return taxForm01StationId;
    }

    public void setTaxForm01StationId(int taxForm01StationId) {
        this.taxForm01StationId = taxForm01StationId;
    }

    public int getTaxForm01RetailId() {
        return taxForm01RetailId;
    }

    public void setTaxForm01RetailId(int taxForm01RetailId) {
        this.taxForm01RetailId = taxForm01RetailId;
    }

    public int getRefStationId() {
        return refStationId;
    }

    public void setRefStationId(int refStationId) {
        this.refStationId = refStationId;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getTambonName() {
        return tambonName;
    }

    public void setTambonName(String tambonName) {
        this.tambonName = tambonName;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TaxForm01StationFileModel> getHouseFileList() {
        return houseFileList;
    }

    public void setHouseFileList(List<TaxForm01StationFileModel> houseFileList) {
        this.houseFileList = houseFileList;
    }

    public List<DrpDownModel> getBusinessTypeList() {
        return businessTypeList;
    }

    public void setBusinessTypeList(List<DrpDownModel> businessTypeList) {
        this.businessTypeList = businessTypeList;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public Integer getHouseFileNum() {
        return houseFileNum;
    }

    public void setHouseFileNum(Integer houseFileNum) {
        this.houseFileNum = houseFileNum;
    }

    public Integer getMapFileNum() {
        return mapFileNum;
    }

    public void setMapFileNum(Integer mapFileNum) {
        this.mapFileNum = mapFileNum;
    }

    public List<TaxForm01StationFileModel> getMapFileList() {
        return mapFileList;
    }

    public void setMapFileList(List<TaxForm01StationFileModel> mapFileList) {
        this.mapFileList = mapFileList;
    }

    
}
