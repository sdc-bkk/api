/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ws;

/**
 *
 * @author User
 */
public class ResponseInvoiceDataModel {
    private String transactionId;
    private String bmaBillNo;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBmaBillNo() {
        return bmaBillNo;
    }

    public void setBmaBillNo(String bmaBillNo) {
        this.bmaBillNo = bmaBillNo;
    }
    
    
}
