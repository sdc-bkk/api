/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ws;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Prapaporn
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseInvoiceBeanModel<T> {
    
    private ResponseInvoiceDataModel result;
    private String status;
    private String resultMsg;

    public ResponseInvoiceDataModel getResult() {
        return result;
    }

    public void setResult(ResponseInvoiceDataModel result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    
}
