/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class DocFormStationFileModel extends BaseFileModel {

    private int taxForm01RetailId;
    private String createdDate;
    private String docNo;
    private String remark;

   


    public int getTaxForm01RetailId() {
        return taxForm01RetailId;
    }

    public void setTaxForm01RetailId(int taxForm01RetailId) {
        this.taxForm01RetailId = taxForm01RetailId;
    }
    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    

}
