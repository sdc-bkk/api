/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class RetailStationFileModel  extends BaseFileModel {
    private int retailStationId;

    public int getRetailStationId() {
        return retailStationId;
    }

    public void setRetailStationId(int retailStationId) {
        this.retailStationId = retailStationId;
    }
    
}
