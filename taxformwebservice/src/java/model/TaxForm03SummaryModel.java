/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class TaxForm03SummaryModel {
    private int taxForm03Id;
    private int sequence; // ลำดับที่
    private int oilTypeId; // รหัสรายการน้ำมัน
    private String oilTypeName; // ชื่อรายการน้ำมัน
    private double qty; // จำนวนลิตร
    private double rate; //อัตราภาษี
    private double amount; // จำนวนเงิน
    private int amountBaht; // จำนวนเงินบาท
    private int amountSatang; // จำนวนเงินสตางค์

    public int getTaxForm03Id() {
        return taxForm03Id;
    }

    public void setTaxForm03Id(int taxForm03Id) {
        this.taxForm03Id = taxForm03Id;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getOilTypeId() {
        return oilTypeId;
    }

    public void setOilTypeId(int oilTypeId) {
        this.oilTypeId = oilTypeId;
    }

    public String getOilTypeName() {
        return oilTypeName;
    }

    public void setOilTypeName(String oilTypeName) {
        this.oilTypeName = oilTypeName;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getAmountBaht() {
        return amountBaht;
    }

    public void setAmountBaht(int amountBaht) {
        this.amountBaht = amountBaht;
    }

    public int getAmountSatang() {
        return amountSatang;
    }

    public void setAmountSatang(int amountSatang) {
        this.amountSatang = amountSatang;
    }
    
}
