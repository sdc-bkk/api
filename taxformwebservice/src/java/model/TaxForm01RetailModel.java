/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author User
 */
public class TaxForm01RetailModel extends BaseClassModel {

    private int taxForm01RetailId;
    private int refRetailId;
    private String docNo;//เลขทำเบียนรับ
    private String docDate;//วันที่รับ
    private String mainStatus;//สถานะ (0="สร้าง",1="รอตรวจสอบ",2="ไม่ผ่าน" ,3, ="อนุมัติ"); 
    private String ownerName;//ชื่อผู้ประกอบการ
    private String taxNo;//เลขประจำตัวผู้เสียภาษีอากร
    private String customerType; //ประเภทบุคคล 1=บุคคลธรรมดา , 2= นิติบุคคล
    private String idCard;//บัตรประชาชน
    private String idCardAt;//ออกให้ ณ ที่ว่าการ
    private String corpNo;//เลขทำเบียนนิติ
    private String corpDate;//เมื่อวันที่
    private String houseNo;//เลขประจำบ้าน
    private String address;
    private String soi;
    private String moo;
    private String road;
    private String tambonId;
    private String tambonName;
    private String amphurId;
    private String amphurName;
    private String provinceId;
    private String provinceName;
    private String postcode;
    private String mobile;
    private String email;
    private String branchName;
    private String signerName;

    private Integer houseFileNum;
    private Integer cerFileNum;
    private Integer agentFileNum;
    
    private String fullAddress;
    private String refOffice;
    private String refOfficeName;

    private List<TaxForm01RetailFileModel> houseFileList; //สำเนาทะเบียนบ้าน
    private List<TaxForm01RetailFileModel> cerFileList;//สำเนารับรอง
    private List<TaxForm01RetailFileModel> agentFileList;//หนังสือมอบอำนาจ

    private List<TaxForm01StationModel> stationList;
    private String memberName;
    private Boolean firstRecord;

    public int getTaxForm01RetailId() {
        return taxForm01RetailId;
    }

    public void setTaxForm01RetailId(int taxForm01RetailId) {
        this.taxForm01RetailId = taxForm01RetailId;
    }

    public int getRefRetailId() {
        return refRetailId;
    }

    public void setRefRetailId(int refRetailId) {
        this.refRetailId = refRetailId;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getMainStatus() {
        return mainStatus;
    }

    public void setMainStatus(String mainStatus) {
        this.mainStatus = mainStatus;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIdCardAt() {
        return idCardAt;
    }

    public void setIdCardAt(String idCardAt) {
        this.idCardAt = idCardAt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getTambonName() {
        return tambonName;
    }

    public void setTambonName(String tambonName) {
        this.tambonName = tambonName;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<TaxForm01RetailFileModel> getHouseFileList() {
        return houseFileList;
    }

    public void setHouseFileList(List<TaxForm01RetailFileModel> houseFileList) {
        this.houseFileList = houseFileList;
    }

    public List<TaxForm01RetailFileModel> getCerFileList() {
        return cerFileList;
    }

    public void setCerFileList(List<TaxForm01RetailFileModel> cerFileList) {
        this.cerFileList = cerFileList;
    }

    public List<TaxForm01StationModel> getStationList() {
        return stationList;
    }

    public void setStationList(List<TaxForm01StationModel> stationList) {
        this.stationList = stationList;
    }

    public Integer getHouseFileNum() {
        return houseFileNum;
    }

    public void setHouseFileNum(Integer houseFileNum) {
        this.houseFileNum = houseFileNum;
    }

    public Integer getCerFileNum() {
        return cerFileNum;
    }

    public void setCerFileNum(Integer cerFileNum) {
        this.cerFileNum = cerFileNum;
    }

    public Integer getAgentFileNum() {
        return agentFileNum;
    }

    public void setAgentFileNum(Integer agentFileNum) {
        this.agentFileNum = agentFileNum;
    }

    public List<TaxForm01RetailFileModel> getAgentFileList() {
        return agentFileList;
    }

    public void setAgentFileList(List<TaxForm01RetailFileModel> agentFileList) {
        this.agentFileList = agentFileList;
    }

    public String getCorpNo() {
        return corpNo;
    }

    public void setCorpNo(String corpNo) {
        this.corpNo = corpNo;
    }

    public String getCorpDate() {
        return corpDate;
    }

    public void setCorpDate(String corpDate) {
        this.corpDate = corpDate;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Boolean getFirstRecord() {
        return firstRecord;
    }

    public void setFirstRecord(Boolean firstRecord) {
        this.firstRecord = firstRecord;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getRefOffice() {
        return refOffice;
    }

    public void setRefOffice(String refOffice) {
        this.refOffice = refOffice;
    }

    public String getRefOfficeName() {
        return refOfficeName;
    }

    public void setRefOfficeName(String refOfficeName) {
        this.refOfficeName = refOfficeName;
    }

    public String getSignerName() {
        return signerName;
    }

    public void setSignerName(String signerName) {
        this.signerName = signerName;
    }

}
