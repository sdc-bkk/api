/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class StatusLogModel {
    private int taxForm01RetailId;
    private int taxForm01StationId;
    private String status;
    private String statusName;
    private String remark;
    private String actionBy;
    private String actionDate;

    public int getTaxForm01RetailId() {
        return taxForm01RetailId;
    }

    public void setTaxForm01RetailId(int taxForm01RetailId) {
        this.taxForm01RetailId = taxForm01RetailId;
    }

    public int getTaxForm01StationId() {
        return taxForm01StationId;
    }

    public void setTaxForm01StationId(int taxForm01StationId) {
        this.taxForm01StationId = taxForm01StationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getActionBy() {
        return actionBy;
    }

    public void setActionBy(String actionBy) {
        this.actionBy = actionBy;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }
}
