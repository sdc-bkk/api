/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import viewModel.DocFormStationFileViewModel;
import viewModel.DrpDownViewModel;
import viewModel.TaxForm01RetailViewModel;

/**
 *
 * @author DELL
 */
public class DocFormStationModel extends BaseClassModel{
    private int retailStationId;
    private int retailId;
    private int refStationId;

    
    private String stationCode;//รหัส generate auto
    private String stationName;//ชื่อสถานค้าปลีก
    private String branchName;//ชื่อสาขา

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    private List<DrpDownViewModel> businessTypeList;
    private String houseNo;//เลขประจำบ้าน
    private String address;
    private String soi;
    private String road;
    private String tambonId;
    private String tambonName;
    private String amphurId;
    private String amphurName;
    private String provinceId;
    private String provinceName;
    private String postcode;
    private String mobile;
    private String email;
    private String status;
    private String statusName;
    
    private Integer houseFileNum;
    private Integer mapFileNum;
    
    private String fullAddress;
    
    private List<DocFormStationFileModel> houseFileList; //สำเนาทะเบียนบ้าน
    private List<DocFormStationFileModel> mapFileList; //แผนที่
    private List<DocFormStationFileModel> files; //แผนที่
    private String docNo;
    private String docDate;
    private String taxNo;
    private String ownerName;
    
    private String memberName;
    
    private TaxForm01RetailViewModel taxForm01Retail;

    


    public int getRetailStationId() {
        return retailStationId;
    }

    public void setRetailStationId(int retailStationId) {
        this.retailStationId = retailStationId;
    }

    public int getRetailId() {
        return retailId;
    }

    public void setRetailId(int retailId) {
        this.retailId = retailId;
    }

    public int getRefStationId() {
        return refStationId;
    }

    public void setRefStationId(int refStationId) {
        this.refStationId = refStationId;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getTambonName() {
        return tambonName;
    }

    public void setTambonName(String tambonName) {
        this.tambonName = tambonName;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public List<DrpDownViewModel> getBusinessTypeList() {
        return businessTypeList;
    }

    public void setBusinessTypeList(List<DrpDownViewModel> businessTypeList) {
        this.businessTypeList = businessTypeList;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public Integer getHouseFileNum() {
        return houseFileNum;
    }

    public void setHouseFileNum(Integer houseFileNum) {
        this.houseFileNum = houseFileNum;
    }

    public Integer getMapFileNum() {
        return mapFileNum;
    }

    public void setMapFileNum(Integer mapFileNum) {
        this.mapFileNum = mapFileNum;
    }

   

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public TaxForm01RetailViewModel getTaxForm01Retail() {
        return taxForm01Retail;
    }

    public void setTaxForm01Retail(TaxForm01RetailViewModel taxForm01Retail) {
        this.taxForm01Retail = taxForm01Retail;
    }
    public List<DocFormStationFileModel> getHouseFileList() {
        return houseFileList;
    }

    public void setHouseFileList(List<DocFormStationFileModel> houseFileList) {
        this.houseFileList = houseFileList;
    }

    public List<DocFormStationFileModel> getMapFileList() {
        return mapFileList;
    }

    public void setMapFileList(List<DocFormStationFileModel> mapFileList) {
        this.mapFileList = mapFileList;
    }

    public List<DocFormStationFileModel> getFiles() {
        return files;
    }

    public void setFiles(List<DocFormStationFileModel> files) {
        this.files = files;
    }
    
}
