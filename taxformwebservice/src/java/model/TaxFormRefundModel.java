/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author User
 */
public class TaxFormRefundModel  extends BaseClassModel{
    private int taxFormRefundId;
    private int refRetailId; //รหัสผู้ประกอบการค้าปลีก
    private int refStationId; //รหัสสถานประกอบการค้าปลีก
    
    private int yearly; //ประจำปี
    private int monthly; //ประจำเดือน
    private String monthName; //ชื่อเดือน
    private double amount;

    private String docRefundDate; //วันที่แจ้งรับเงินคืน  
    
    private String receiptNo; //เลขที่ใบเสร็จ
    private String receiptDate; //วันที่ใบเสร็จ
    private String docNo; //เลขที่ทะเบียนออกโดยระบบ
    private String docDate; //วันทีทะเบียน
    
    private String docNo1; //เลขที่กท
    private String docNo2; //เลขที่กท
    private String place; //สถานที่ชำระเงิน
    
    private String status;//สถานะ(1="รอตรวจสอบ",2="อยู่ระหว่างดำเนินการ" ,3 ="ไม่เข้าหลักเกณฑ์",4 ="แจ้งรับเงินคืน"); 
    private String statusName;//สถานะของฟอร์ม StatusRefundEnum.displayNameTH; 
    
    private String acceptBy; //ผู้ที่รับเรื่อง
    private String acceptDate; //วันที่รับเรื่อง
    private String refundBy; //ผู้ที่แจ้งรับเงินคืน
    private String refundDate; //วันที่แจ้งรับเงินคืน
    private String rejectBy; //ผู้ที่แจ้งไม่เข้าหลักเกณฑ์
    private String rejectDate; //วันที่แจ้งไม่เข้าหลักเกณฑ์
    
    private String refOffice; //รหัสสำนักงานเขต
    private String refOfficeName; //ชื่อสำนักงานเขต    
    
    private String rejectRemark; //หมายเหตุกรณีไม่ผ่าน 
    
    private List<TaxFormRefundFileModel> billPaymentFileList;//ใบเสร็จรับเงิน
    private List<TaxFormRefundFileModel> authorizeFileList;//สำเนามอบอำนาจ
    private List<TaxFormRefundFileModel> idCardFileList;//สำเนาบัตรประจำตัวประชาชน
    private List<TaxFormRefundFileModel> bookBankFileList;//หน้าสมุดบัญชีธนาคาร

    public String getDocRefundDate() {
        return docRefundDate;
    }

    public void setDocRefundDate(String docRefundDate) {
        this.docRefundDate = docRefundDate;
    }

    public String getRejectRemark() {
        return rejectRemark;
    }

    public void setRejectRemark(String rejectRemark) {
        this.rejectRemark = rejectRemark;
    }

    public int getTaxFormRefundId() {
        return taxFormRefundId;
    }

    public void setTaxFormRefundId(int taxFormRefundId) {
        this.taxFormRefundId = taxFormRefundId;
    }

    public int getRefRetailId() {
        return refRetailId;
    }

    public void setRefRetailId(int refRetailId) {
        this.refRetailId = refRetailId;
    }

    public int getRefStationId() {
        return refStationId;
    }

    public void setRefStationId(int refStationId) {
        this.refStationId = refStationId;
    }

    public int getYearly() {
        return yearly;
    }

    public void setYearly(int yearly) {
        this.yearly = yearly;
    }

    public int getMonthly() {
        return monthly;
    }

    public void setMonthly(int monthly) {
        this.monthly = monthly;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocNo1() {
        return docNo1;
    }

    public void setDocNo1(String docNo1) {
        this.docNo1 = docNo1;
    }

    public String getDocNo2() {
        return docNo2;
    }

    public void setDocNo2(String docNo2) {
        this.docNo2 = docNo2;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getAcceptBy() {
        return acceptBy;
    }

    public void setAcceptBy(String acceptBy) {
        this.acceptBy = acceptBy;
    }

    public String getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    public String getRefundBy() {
        return refundBy;
    }

    public void setRefundBy(String refundBy) {
        this.refundBy = refundBy;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public String getRejectBy() {
        return rejectBy;
    }

    public void setRejectBy(String rejectBy) {
        this.rejectBy = rejectBy;
    }

    public String getRejectDate() {
        return rejectDate;
    }

    public void setRejectDate(String rejectDate) {
        this.rejectDate = rejectDate;
    }

    public String getRefOffice() {
        return refOffice;
    }

    public void setRefOffice(String refOffice) {
        this.refOffice = refOffice;
    }

    public String getRefOfficeName() {
        return refOfficeName;
    }

    public void setRefOfficeName(String refOfficeName) {
        this.refOfficeName = refOfficeName;
    }

    public List<TaxFormRefundFileModel> getBillPaymentFileList() {
        return billPaymentFileList;
    }

    public void setBillPaymentFileList(List<TaxFormRefundFileModel> billPaymentFileList) {
        this.billPaymentFileList = billPaymentFileList;
    }

    public List<TaxFormRefundFileModel> getAuthorizeFileList() {
        return authorizeFileList;
    }

    public void setAuthorizeFileList(List<TaxFormRefundFileModel> authorizeFileList) {
        this.authorizeFileList = authorizeFileList;
    }

    public List<TaxFormRefundFileModel> getIdCardFileList() {
        return idCardFileList;
    }

    public void setIdCardFileList(List<TaxFormRefundFileModel> idCardFileList) {
        this.idCardFileList = idCardFileList;
    }

    public List<TaxFormRefundFileModel> getBookBankFileList() {
        return bookBankFileList;
    }

    public void setBookBankFileList(List<TaxFormRefundFileModel> bookBankFileList) {
        this.bookBankFileList = bookBankFileList;
    }
     public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
}
