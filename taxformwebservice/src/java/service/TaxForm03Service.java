/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import repository.RetailRepo;
import repository.StatusLogRepo;
import repository.TaxForm01Repo;
import repository.TaxForm03Repo;
import repository.mapper.TaxForm01Mapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.FilterTaxFormModel;
import viewModel.MonthlyViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01StationViewModel;
import viewModel.TaxForm03ViewModel;

/**
 *
 * @author User
 */
public class TaxForm03Service {
    
    TaxForm03Repo repo = new TaxForm03Repo();
    StatusLogRepo statusRepo = new StatusLogRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();
    
    public ResultData<List<MonthlyViewModel>> getList(FilterTaxFormModel filter) {
        
        ResultData<List<MonthlyViewModel>> resultData = new ResultData<List<MonthlyViewModel>>();
        ResultPage resultPage = null;
        
        try {
            
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

//            RetailRepo retailRepo = new RetailRepo();
//            String taxNo = retailRepo.getTaxNoByMemberName(filter.getMemberName());
//            if (!AppUtil.isNullAndSpace(taxNo)) {
//                filter.setTaxNo(taxNo);
            resultData = repo.getList(resultPage, filter);
            resultData.setStatus("true");
            resultData.setResultMessage("");
//            } else {
//                resultData.setStatus("true");
//                resultData.setResultMessage(message.getMessage("message.list.nodata"));
//            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }
        
        return resultData;
    }
    
    public ResultData<List<TaxForm03ViewModel>> getListByMonthly(FilterTaxFormModel filter) {
        
        ResultData<List<TaxForm03ViewModel>> resultData = new ResultData<List<TaxForm03ViewModel>>();
        ResultPage resultPage = null;
        
        try {
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }
            filter.setForType("back");
            
            resultData = repo.getListByMonthly(resultPage, filter);
            resultData.setStatus("true");
            resultData.setResultMessage("");
            
        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }
        
        return resultData;
    }
    
    public ResultData<Boolean> approve(StatusLogViewModel data) {
        
        ResultData<Boolean> resultData = new ResultData<Boolean>();
        
        try {
            resultData = statusRepo.approveTaxForm03(data);
            
        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }
        
        return resultData;
    }
    
    public ResultData<Boolean> reject(StatusLogViewModel data) {
        
        ResultData<Boolean> resultData = new ResultData<Boolean>();
        
        try {
            resultData = statusRepo.rejectTaxForm03(data);
            
        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }
        
        return resultData;
    }
}
