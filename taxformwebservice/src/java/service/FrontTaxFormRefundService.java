/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.StatusRefundEnum;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.TaxFormRefundModel;
import repository.RetailRepo;
import repository.TaxFormRefundRepo;
import repository.mapper.TaxFormRefundMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.TaxFormRefundViewModel;

/**
 *
 * @author User
 */
public class FrontTaxFormRefundService {

    TaxFormRefundRepo repo = new TaxFormRefundRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();
    private static final String DATE_FORMAT = "dd/MM/yyyy";
    private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    private static final DateTimeFormatter dateFormat8 = DateTimeFormatter.ofPattern(DATE_FORMAT);

    public ResultData<List<TaxFormRefundViewModel>> getList(FilterTaxFormModel filter) {

        ResultData<List<TaxFormRefundViewModel>> resultData = new ResultData<List<TaxFormRefundViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            RetailRepo retailRepo = new RetailRepo();
            RetailViewModel dataRetail = new RetailViewModel();
            dataRetail = retailRepo.getRetailByMemberName(filter.getMemberName());

            if (dataRetail.getRetailId() != null) {
                filter.setRetailId(dataRetail.getRetailId());
                resultData = repo.getList(resultPage, filter);
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListStatus() {
        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();

        try {
            List<DrpDownViewModel> dataList = new ArrayList<>();
            List<StatusRefundEnum> list = new ArrayList<StatusRefundEnum>();
            list = StatusRefundEnum.list();
            for (StatusRefundEnum dataEnum : list) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(String.valueOf(dataEnum.value()));
                data.setName(dataEnum.displayNameTH());
                dataList.add(data);
            }
            resultData.setResult(dataList);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }
    
    public ResultData<TaxFormRefundViewModel> getData(FilterTaxFormModel filter) {

        ResultData<TaxFormRefundViewModel> resultData = new ResultData<TaxFormRefundViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getTaxFormRefundId());
            resultData = repo.getData(id);

            if (AppUtil.isNullAndSpace(resultData.getResult().getTaxFormRefundId())) {
                resultData.setResult(new TaxFormRefundViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            } else {
//                //get ref retail
//                RetailRepo repoRetail = new RetailRepo();
//                ResultData<RetailViewModel> resultDataRetail = new ResultData<RetailViewModel>();
//                FilterTaxFormModel filterRetail = new FilterTaxFormModel();
//                filterRetail.setId(resultData.getResult().getRefRetailId());
//                resultDataRetail = repoRetail.getData(filterRetail);
//                resultData.getResult().setRefRetail(resultDataRetail.getResult());
//
//                //get ref station
//                ResultData<RetailStationViewModel> resultDataStation = new ResultData<RetailStationViewModel>();
//                int idStation = AppUtil.decryptId(resultData.getResult().getRefStationId());
//                resultDataStation = repoRetail.getDataStation(idStation);
//                resultData.getResult().setRefStation(resultDataStation.getResult());
            }

            resultData.setStatus("true");
            resultData.setResultMessage("");
        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(TaxFormRefundViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        TaxFormRefundModel dataSave = new TaxFormRefundModel();

        try {
            TaxFormRefundMapper mapper = new TaxFormRefundMapper();
            dataSave = mapper.mapSaveData(data);
            RetailRepo retailRepo = new RetailRepo();
            ResultData<RetailStationViewModel> dataStation = retailRepo.getDataStation(dataSave.getRefStationId());
            dataSave.setRefOffice(AppUtil.checkNullData(dataStation.getResult().getAmphurId()));
            dataSave.setRefOfficeName(AppUtil.checkNullData(dataStation.getResult().getAmphurName()));
            //System.out.print(dataSave);
            resultData = repo.save(dataSave);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteData(FilterTaxFormModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int id = AppUtil.decryptId(i);
                idDeleteList.add(id);
            }

            resultData = repo.deleteData(idDeleteList, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

}
