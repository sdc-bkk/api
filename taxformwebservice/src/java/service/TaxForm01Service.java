/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.StatusEnum;
import java.util.ArrayList;
import java.util.List;
import repository.StatusLogRepo;
import repository.TaxForm01Repo;
import repository.mapper.TaxForm01Mapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 *
 * @author User
 */
public class TaxForm01Service {

    TaxForm01Repo repo = new TaxForm01Repo();
    StatusLogRepo statusRepo = new StatusLogRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<TaxForm01StationViewModel>> getList(FilterTaxFormModel filter) {

        ResultData<List<TaxForm01StationViewModel>> resultData = new ResultData<List<TaxForm01StationViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListForOfficer(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> approve(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            TaxForm01Mapper mapper = new TaxForm01Mapper();
//                dataSave = mapper.mapSaveData(data);
            resultData = statusRepo.approveTaxForm01(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> reject(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            TaxForm01Mapper mapper = new TaxForm01Mapper();
//                dataSave = mapper.mapSaveData(data);
            resultData = statusRepo.rejectTaxForm01(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListStatus() {
        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        ResultPage resultPage = null;

        try {
            List<DrpDownViewModel> dataList = new ArrayList<>();
            List<StatusEnum> list = new ArrayList<StatusEnum>();
            list = StatusEnum.list();
            for (StatusEnum dataEnum : list) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(String.valueOf(dataEnum.value()));
                data.setName(dataEnum.displayNameTH());
                dataList.add(data);
            }
            resultData.setResult(dataList);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<TaxForm01StationViewModel> getData(FilterTaxFormModel filter) {

        ResultData<TaxForm01StationViewModel> resultData = new ResultData<TaxForm01StationViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getTaxForm01StationId());
            resultData = repo.getDataStation(id);

            if (resultData.getResult() != null) {
                int retailId = AppUtil.decryptId(resultData.getResult().getTaxForm01RetailId());
                ResultData<TaxForm01RetailViewModel> resultDataRetail = new ResultData<TaxForm01RetailViewModel>();
                resultDataRetail = repo.getData(retailId);

                if (resultDataRetail.getResult() != null) {
                    resultData.getResult().setTaxForm01Retail(resultDataRetail.getResult());
                }
            }

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getTaxForm01RetailId())) {
                resultData.setResult(new TaxForm01StationViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }
}
