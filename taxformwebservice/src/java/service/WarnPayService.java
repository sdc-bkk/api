/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.StatusWarnformEnum;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import repository.WarnFormRepo;
import repository.WarnPayRepo;
import utility.AppConfig;
import utility.AppUtil;
import utility.DateUtil;
import utility.MailService;
import utility.MessageBundleUtil;
import viewModel.FilterModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.TaxForm01StationViewModel;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class WarnPayService {

    WarnPayRepo repo = new WarnPayRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<WarnFormViewModel>> getList(FilterModel filter) {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<WarnFormViewModel>> getListByMonthly(FilterModel filter) {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        ResultPage resultPage = null;

        try {
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListByMonthly(resultPage, filter);
            resultData.setStatus("true");
            resultData.setResultMessage("");

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(WarnFormViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            resultData = repo.saveData(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<List<WarnFormViewModel>> getListLog(FilterModel filter) {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListLog(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteData(FilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            resultData = repo.deleteData(idDeleteList, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> updateStatusSendMail(FilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            filter.setStatus(StatusWarnformEnum.Send.value());//ส่งเมล์
            filter.setStatusName(StatusWarnformEnum.Send.displayNameTH());//ส่งเมล์

            resultData = repo.updateStatus(filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> updateStatusCancelMail(FilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            filter.setStatus(StatusWarnformEnum.Cancel.value());//0 = ออกหนังสือ , 1= ส่งเมล์ ,2=ตอบรับแล้ว, 9=ยกเลิก
            filter.setStatusName(StatusWarnformEnum.Cancel.displayNameTH());//ส่งเมล์

            resultData = repo.updateStatus(filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> sendMail(FilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            int id = AppUtil.decryptId(filter.getWarnFormId());
            //get email from
            List<WarnFormViewModel> emailList = repo.getDataWarnByStationId(id);

            for (WarnFormViewModel model : emailList) {
                //<editor-fold defaultstate="collapsed" desc="send mail ">
                AppConfig appConfig = new AppConfig();
                String linkExportWarn = appConfig.value("read_warnpay") + "/" + filter.getWarnFormId();

                //send Email
                String mailTo = model.getEmail();
                MailService mail = new MailService();
                mail.setTo(mailTo);
                mail.setSubject("แจ้งเตือนการค้างชำระของระบบชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ");
                String temp = "เรียน " + model.getOwnerName() + " เจ้าของสถานการค้าปลีก " + model.getStationName() + "<br><br>"
                        + "     ด้วยความปรากฏว่า ท่านมิได้ชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ ประจำเดือน " + model.getMonthName() + " " + model.getYearly()
                        + "<br> "
                        + "กรุณาคลิก<a href=\"" + linkExportWarn + "\">ที่นี่</a> เพื่อเรียกดูรายละเอียดการแจ้งเตือนค้างชำระ "
                        + "<br><br>" + "จึงเรียนมาเพื่อโปรดทราบ"
                        + "<br><br>" + "ขอแสดงความนับถือ";
                mail.setMessageText(temp);
                boolean resultSendMail = mail.sendMail();
            }
            resultData.setResult(Boolean.TRUE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> sendMailCancel(FilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            int id = AppUtil.decryptId(filter.getWarnFormId());
            //get email from
            List<WarnFormViewModel> emailList = repo.getDataWarnByStationId(id);

            for (WarnFormViewModel model : emailList) {
                //<editor-fold defaultstate="collapsed" desc="send mail ">
                String mailTo = model.getEmail();
                MailService mail = new MailService();
                mail.setTo(mailTo);
                mail.setSubject("<ยกเลิก>แจ้งเตือนการค้างชำระของระบบชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ");
                String temp = "เรียน " + model.getOwnerName() + " เจ้าของสถานการค้าปลีก " + model.getStationName() + "<br><br>"
                        + "     ด้วยความปรากฏว่า ท่านมิได้ชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ ประจำเดือน " + model.getMonthName() + " " + model.getYearly()
                        + "<br> "
                        + "ทางกรุงเทพมหานครขอยกเลิกหนังสือเตือนค้างชำระ ดังกล่าว "
                        + "<br><br>" + "จึงเรียนมาเพื่อโปรดทราบ"
                        + "<br><br>" + "ขอแสดงความนับถือ";
                mail.setMessageText(temp);
                boolean resultSendMail = mail.sendMail();
            }
            resultData.setResult(Boolean.TRUE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public static int calDelayNum(Integer monthly, Integer yearly) throws ParseException {
        int delayNum = 0;

        //Set date expire
        Integer month = monthly + 1;

        Integer year = yearly;
        if (month > 12) {
            month = 1;
            year = year + 1;
        }
        String dueDateStr = "16" + "/" + month.toString() + "/" + AppUtil.toYearENString(yearly.toString());

        SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        Date dueDate = formatter1.parse(dueDateStr);
        // Get current date;
        Date currentDate = new Date();

        if (currentDate.before(dueDate)) {

        } else {

            Calendar startCalendar = new GregorianCalendar();
            startCalendar.setTime(dueDate);
            Calendar endCalendar = new GregorianCalendar();
            endCalendar.setTime(currentDate);
            long num = ChronoUnit.DAYS.between(DateUtil.convertToLocalDateViaInstant(dueDate), DateUtil.convertToLocalDateViaInstant(currentDate));
            delayNum = (int) num;
//            delayNum = endCalendar.get(Calendar.DATE) - startCalendar.get(Calendar.DATE);
        }
        return delayNum;
    }

}
