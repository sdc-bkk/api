/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import repository.RetailRepo;
import repository.TaxForm01Repo;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.TaxForm01RetailViewModel;

/**
 *
 * @author User
 */
public class FrontRetailService {

    RetailRepo repo = new RetailRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<RetailViewModel> getDataByMemberName(FilterTaxFormModel filter) {

        ResultData<RetailViewModel> resultData = new ResultData<RetailViewModel>();

        try {
            String taxNo = repo.getTaxNoByMemberName(filter.getMemberName());

            if (!AppUtil.isNullAndSpace(taxNo)) {
                filter.setTaxNo(taxNo);
                resultData = repo.getData(filter);
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("false");
                resultData.setResult(new RetailViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<RetailViewModel> getDataByTaxNo(FilterTaxFormModel filter) {

        ResultData<RetailViewModel> resultData = new ResultData<RetailViewModel>();

        try {

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                resultData = repo.getData(filter);

                if (resultData != null && !AppUtil.isNullAndSpace(resultData.getResult().getRetailId())) {
                    //กรณีมี TaxNo แล้ว                    
                    resultData.setStatus("true");
                    resultData.setResultMessage(message.getMessage("message.retail.duplicate"));
                } else {
                    resultData.setStatus("false");
                    resultData.setResult(new RetailViewModel());
                    resultData.setResultMessage(message.getMessage("message.list.nodata"));
                }

            } else {
                resultData.setStatus("false");
                resultData.setResult(new RetailViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getListStationByTaxNo(FilterTaxFormModel filter) {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListStationByTaxNo(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<RetailStationViewModel> getDataStation(FilterTaxFormModel filter) {

        ResultData<RetailStationViewModel> resultData = new ResultData<RetailStationViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = repo.getDataStation(id);

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getRetailId())) {
                resultData.setResult(new RetailStationViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<RetailStationViewModel>> getListAllStationByMemberName(FilterTaxFormModel filter) {

        ResultData<List<RetailStationViewModel>> resultData = new ResultData<List<RetailStationViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0 ) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            if (filter.getMemberName() != null && !"".equals(filter.getMemberName())) {
                String taxNo = repo.getTaxNoByMemberName(filter.getMemberName());
                filter.setTaxNo(taxNo);
            }

            resultData = repo.getListAllStationByTaxNo(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }
}
