/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.StatusEnum;
import java.util.ArrayList;
import java.util.List;
import model.DocFormModel;
import model.DocFormStationModel;
import model.TaxForm01RetailModel;
import repository.DocFormRepo;
import repository.RetailRepo;
import repository.StatusLogRepo;
import repository.TaxForm01Repo;
import repository.mapper.DocFormMapper;
import repository.mapper.TaxForm01Mapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.DocFormFileViewModel;
import viewModel.DocFormStationViewModel;
import viewModel.DocFormViewModel;
import viewModel.DrpDownViewModel;
import viewModel.FilterDocFormModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 *
 * @author User
 */
public class DocFormService {

    DocFormRepo repo = new DocFormRepo();
    StatusLogRepo statusRepo = new StatusLogRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    //<editor-fold defaultstate="collapsed" desc="for retail">
    public ResultData<List<DocFormViewModel>> getListForRetail(FilterTaxFormModel filter) {

        ResultData<List<DocFormViewModel>> resultData = new ResultData<List<DocFormViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }
            resultData = repo.getListForRetail(resultPage, filter);
            resultData.setStatus("true");
            resultData.setResultMessage("");

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<DocFormFileViewModel>> getListForRetailFile(FilterTaxFormModel filter) {

        ResultData<List<DocFormFileViewModel>> resultData = new ResultData<List<DocFormFileViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }
            resultData = repo.getListForRetailFile(resultPage, filter);
            resultData.setStatus("true");
            resultData.setResultMessage("");

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveDataRetailAddFile(DocFormFileViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        DocFormModel dataSave = new DocFormModel();

        try {

            resultData = repo.saveDataRetailAddFile(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteDataRetailAddFile(FilterTaxFormModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = repo.deleteDataRetailAddFile(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setResultMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="for retail station">
    
    public ResultData<List<DocFormViewModel>> getListForStation(FilterTaxFormModel filter) {

        ResultData<List<DocFormViewModel>> resultData = new ResultData<List<DocFormViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }
            resultData = repo.getListForStation(resultPage, filter);
            resultData.setStatus("true");
            resultData.setResultMessage("");

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<DocFormFileViewModel>> getListForStationFile(FilterTaxFormModel filter) {

        ResultData<List<DocFormFileViewModel>> resultData = new ResultData<List<DocFormFileViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }
            resultData = repo.getListForStationFile(resultPage, filter);
            resultData.setStatus("true");
            resultData.setResultMessage("");

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveDataStationAddFile(DocFormFileViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        DocFormModel dataSave = new DocFormModel();

        try {

            resultData = repo.saveDataStationAddFile(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteDataStationAddFile(FilterTaxFormModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = repo.deleteDataStationAddFile(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setResultMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }
    
    //</editor-fold>
    
}
