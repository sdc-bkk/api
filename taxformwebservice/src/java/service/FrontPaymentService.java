/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;
import repository.FrontPaymentRepo;
import repository.RetailRepo;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.FilterModel;
import viewModel.FrontInvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class FrontPaymentService {

    FrontPaymentRepo repo = new FrontPaymentRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();
    private static final String DATE_FORMAT = "dd/MM/yyyy";
    private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    private static final DateTimeFormatter dateFormat8 = DateTimeFormatter.ofPattern(DATE_FORMAT);

    public ResultData<List<FrontInvoiceViewModel>> getList(FilterModel filter) {

        ResultData<List<FrontInvoiceViewModel>> resultData = new ResultData<List<FrontInvoiceViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            RetailRepo retailRepo = new RetailRepo();
            String taxNo = retailRepo.getTaxNoByMemberName(filter.getMemberName());

            if (!AppUtil.isNullAndSpace(taxNo)) {
                filter.setTaxNo(taxNo);
                resultData = repo.getList(resultPage, filter);
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<FrontInvoiceViewModel>> getListHistory(FilterModel filter) {

        ResultData<List<FrontInvoiceViewModel>> resultData = new ResultData<List<FrontInvoiceViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            RetailRepo retailRepo = new RetailRepo();
            String taxNo = retailRepo.getTaxNoByMemberName(filter.getMemberName());

            if (!AppUtil.isNullAndSpace(taxNo)) {
                filter.setTaxNo(taxNo);
                resultData = repo.getListHistory(resultPage, filter);
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

}
