/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import model.TaxForm02Model;
import repository.RetailRepo;
import repository.StatusLogRepo;
import repository.TaxForm02Repo;
import repository.mapper.TaxForm02Mapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxForm02Model;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm02ViewModel;

/**
 *
 * @author User
 */
public class FrontTaxForm02Service {

    TaxForm02Repo repo = new TaxForm02Repo();

    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<TaxForm02ViewModel>> getList(FilterTaxForm02Model filter) {

        ResultData<List<TaxForm02ViewModel>> resultData = new ResultData<List<TaxForm02ViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            RetailRepo retailRepo = new RetailRepo();
            String taxNo = retailRepo.getTaxNoByMemberName(filter.getMemberName());

            if (!AppUtil.isNullAndSpace(taxNo)) {
                filter.setTaxNo(taxNo);
                resultData = repo.getList(resultPage, filter);

                if (resultData.getResult() != null) {
                    for (TaxForm02ViewModel child : resultData.getResult()) {
                        List<DrpDownViewModel> businessList = repo.getListBusinessStatus(child.getTaxForm02Id());
                        child.setBusinessStatusList(businessList);
                    }
                }
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<StatusLogViewModel>> getListStatusLog(FilterTaxFormModel filter) {

        ResultData<List<StatusLogViewModel>> resultData = new ResultData<List<StatusLogViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListStatusLog(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<TaxForm02ViewModel> getData(FilterTaxForm02Model filter) {

        ResultData<TaxForm02ViewModel> resultData = new ResultData<TaxForm02ViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getTaxForm02Id());
            resultData = repo.getData(id);

            if (resultData.getResult() != null) {
                if (resultData.getResult().getRefRetailId() != null) {
                    //ดึงข้อมูลผู้ประกอบการ
                    ResultData<RetailViewModel> resultDataRetail = new ResultData<RetailViewModel>();
                    FilterTaxFormModel filterRetail = new FilterTaxFormModel();
                    filterRetail.setId(resultData.getResult().getRefRetailId());
                    RetailRepo retailRepo = new RetailRepo();
                    resultDataRetail = retailRepo.getData(filterRetail);

                    resultData.getResult().setRetail(resultDataRetail.getResult());
                }
            }
            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getTaxForm02Id())) {
                resultData.setResult(new TaxForm02ViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(TaxForm02ViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        TaxForm02Model dataSave = new TaxForm02Model();

        try {
            //get data retail_station
            RetailRepo retailRepo = new RetailRepo();
            ResultData<RetailStationViewModel> resultStationData = new ResultData<RetailStationViewModel>();
            RetailStationViewModel stationData = new RetailStationViewModel();
            int id = AppUtil.decryptId(data.getRefStationId());
            resultStationData = retailRepo.getDataStation(id);
//            ResultData<RetailViewModel> retailData = new ResultData<>();
//            if (data.getRefTaxNo() != null && data.getRefTaxNo() != "") {
//                FrontRetailService frontRetailsService = new FrontRetailService();
//                FilterTaxFormModel filterRetail = new FilterTaxFormModel();
//                filterRetail.setTaxNo(data.getRefTaxNo());
//                retailData = frontRetailsService.getDataByTaxNo(filterRetail);
//            }

            TaxForm02Mapper mapper = new TaxForm02Mapper();
            dataSave = mapper.mapSaveData(data);
            if (resultStationData.getResult() != null) {
                dataSave.setOffice(resultStationData.getResult().getOffice());
                dataSave.setOfficeName(resultStationData.getResult().getOfficeName());
                dataSave.setOfficeCode(resultStationData.getResult().getOfficeCode());
            }
            resultData = repo.save(dataSave);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteData(FilterTaxForm02Model filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            resultData = repo.deleteData(idDeleteList, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> sendApprove(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            StatusLogRepo statusRepo = new StatusLogRepo();
//                TaxForm02Mapper mapper = new TaxForm02Mapper();
//                dataSave = mapper.mapSaveData(data);
            resultData = statusRepo.sendApproveTaxForm02(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

}
