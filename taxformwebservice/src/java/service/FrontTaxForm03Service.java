/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import enumeration.AppConfigEnum;
import enumeration.SettingGeneralEnum;
import enumeration.StatusEnum;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import model.TaxForm03Model;
import model.InvoiceModel;
import model.OfficeModel;
import model.ws.ResponseInvoiceDataModel;
import repository.AppConfigRepo;
import repository.InvoiceRepo;
import repository.RetailRepo;
import repository.ShareRepo;
import repository.StatusLogRepo;
import repository.TaxForm03Repo;
import repository.mapper.TaxForm01Mapper;
import repository.mapper.TaxForm03Mapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.DateUtil;
import utility.MessageBundleUtil;
import utility.WebserviceUtil;
import viewModel.FeeViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.MonthlyViewModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.StatusAccountViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm03ViewModel;

/**
 *
 * @author User
 */
public class FrontTaxForm03Service {

    TaxForm03Repo repo = new TaxForm03Repo();
    RetailRepo retailRepo = new RetailRepo();
    AppConfigRepo appRepo = new AppConfigRepo();
    ShareRepo shareRepo = new ShareRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();
    private static final String DATE_FORMAT = "dd/MM/yyyy";
    private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    private static final DateTimeFormatter dateFormat8 = DateTimeFormatter.ofPattern(DATE_FORMAT);

    public ResultData<List<MonthlyViewModel>> getList(FilterTaxFormModel filter) {

        ResultData<List<MonthlyViewModel>> resultData = new ResultData<List<MonthlyViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            RetailRepo retailRepo = new RetailRepo();
            String taxNo = retailRepo.getTaxNoByMemberName(filter.getMemberName());

            if (!AppUtil.isNullAndSpace(taxNo)) {
                filter.setTaxNo(taxNo);
                resultData = repo.getList(resultPage, filter);
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<TaxForm03ViewModel>> getListByMonthly(FilterTaxFormModel filter) {

        ResultData<List<TaxForm03ViewModel>> resultData = new ResultData<List<TaxForm03ViewModel>>();
        ResultPage resultPage = null;

        try {
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            RetailRepo retailRepo = new RetailRepo();
            String taxNo = retailRepo.getTaxNoByMemberName(filter.getMemberName());

            if (!AppUtil.isNullAndSpace(taxNo)) {
                filter.setTaxNo(taxNo);
                resultData = repo.getListByMonthly(resultPage, filter);
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<TaxForm03ViewModel> getData(FilterTaxFormModel filter) {

        ResultData<TaxForm03ViewModel> resultData = new ResultData<TaxForm03ViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getTaxForm03Id());
            resultData = repo.getData(id);

            if (AppUtil.isNullAndSpace(resultData.getResult().getTaxForm03Id())) {
                resultData.setResult(new TaxForm03ViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            } else {
                //get ref retail
                RetailRepo repoRetail = new RetailRepo();
                ResultData<RetailViewModel> resultDataRetail = new ResultData<RetailViewModel>();
                FilterTaxFormModel filterRetail = new FilterTaxFormModel();
                filterRetail.setId(resultData.getResult().getRefRetailId());
                resultDataRetail = repoRetail.getData(filterRetail);
                resultData.getResult().setRefRetail(resultDataRetail.getResult());

                //get ref station
                ResultData<RetailStationViewModel> resultDataStation = new ResultData<RetailStationViewModel>();
                int idStation = AppUtil.decryptId(resultData.getResult().getRefStationId());
                resultDataStation = repoRetail.getDataStation(idStation);
                resultData.getResult().setRefStation(resultDataStation.getResult());
            }

            resultData.setStatus("true");
            resultData.setResultMessage("");
        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(TaxForm03ViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        TaxForm03Model dataSave = new TaxForm03Model();

        try {
            TaxForm03Mapper mapper = new TaxForm03Mapper();
            dataSave = mapper.mapSaveData(data);

            //<editor-fold defaultstate="collapsed" desc="ดึงสำนักงานเขตจาก station "> 
            RetailStationViewModel stationData = retailRepo.getOfficeByStation(dataSave.getRefStationId());
            ShareRepo shareRepo = new ShareRepo();
            OfficeModel officeData = new OfficeModel();
            officeData = shareRepo.getOfficeDataByAmphurId(stationData.getAmphurId());
            dataSave.setRefOffice(officeData.getOfficeId());
            dataSave.setRefOfficeName(officeData.getOfficeName());
            dataSave.setRefOfficeCode(officeData.getOfficeCode());
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="calculate วันครบกำหนดชำระ ">  
            dataSave.setDueDate(getDueDatePaymentNow());
            //</editor-fold>

            resultData = repo.save(dataSave);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> sendApprove(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            TaxForm01Mapper mapper = new TaxForm01Mapper();
            StatusLogRepo statusRepo = new StatusLogRepo();
            resultData = statusRepo.sendApproveTaxForm03(data);

            if(resultData.getResult() == true) {
                createInvoiceAndSendBma(data);
            }
        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> test(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            TaxForm01Mapper mapper = new TaxForm01Mapper();
            StatusLogRepo statusRepo = new StatusLogRepo();
//            resultData = statusRepo.sendApproveTaxForm03(data);

//            if(resultData.getResult() == true)
//            {
            createInvoiceAndSendBma(data);
//            }

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public Boolean createInvoiceAndSendBma(StatusLogViewModel data) throws SQLException, JsonProcessingException, Exception {
        Boolean result = false;
        String taxType = appRepo.GetValue(AppConfigEnum.TaxType.displayNameTH());
        String taxTypeName = appRepo.GetValue(AppConfigEnum.TaxTypeName.displayNameTH());
        String urlWsBma = appRepo.GetValue(AppConfigEnum.WsBmaInvoice.displayNameTH());
        String bankCode = appRepo.GetValue(AppConfigEnum.BankCode.displayNameTH());
        String suffixBank = appRepo.GetValue(AppConfigEnum.SuffixBank.displayNameTH());

        //<editor-fold defaultstate="collapsed" desc="loop list of Id">
        for (String idString : data.getIdList()) {
            //Get data from taxform03
            FilterTaxFormModel filter = new FilterTaxFormModel();
            filter.setTaxForm03Id(idString);
            ResultData<TaxForm03ViewModel> taxForm03Result = new ResultData<TaxForm03ViewModel>();
            TaxForm03ViewModel taxForm03Data = new TaxForm03ViewModel();
            taxForm03Result = getData(filter);

            if (taxForm03Result.getResult() != null) {
                taxForm03Data = taxForm03Result.getResult();
                //<editor-fold defaultstate="collapsed" desc="map to invoiceModel">
                String dueDate = "";//ddmmyy
                String[] arrOfDuedate = taxForm03Data.getDueDate().split("/");
                for (String str : arrOfDuedate) {
                    dueDate = dueDate + str.substring(str.length()-2, str.length());
                }
                String yearPeriod = shareRepo.getYearlyBudget(Integer.toString(taxForm03Data.getMonthly()) + Integer.toString(taxForm03Data.getYearly()));

                ShareRepo shareRepo = new ShareRepo();
                OfficeModel officeData = new OfficeModel();
                officeData = shareRepo.getOfficeDataByAmphurId(taxForm03Data.getRefOffice());

                InvoiceModel invoice = new InvoiceModel();
                invoice.setTaxform03Id(AppUtil.decryptId(idString));
                invoice.setBillNo(bankCode + suffixBank);
                invoice.setRef1(AppUtil.getRunningDoc("ref1", taxForm03Data.getRefOfficeCode() + yearPeriod));
                invoice.setRef2(AppUtil.getRunningDoc("ref2", dueDate + taxForm03Data.getRefOfficeCode()));
                invoice.setTaxNo(taxForm03Data.getRefRetail().getTaxNo());
                invoice.setCusName(taxForm03Data.getRefRetail().getOwnerName());
                invoice.setCusAddress(taxForm03Data.getRefRetail().getFullAddress());
                invoice.setDueDate(taxForm03Data.getDueDate());

                if (taxForm03Data.getTaxTotal() > 0) {
                    invoice.setAmount1(Double.toString(taxForm03Data.getTaxTotal()).replace(",", ""));
                    invoice.setDetail1("ค่าภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ");
                    if (taxForm03Data.getExtraMoney() > 0) {
                        invoice.setAmount2(Double.toString(taxForm03Data.getExtraMoney()).replace(",", ""));
                        invoice.setDetail2("ค่าเพิ่ม");
                    }
                } else {
                    if (taxForm03Data.getExtraMoney() > 0) {
                        invoice.setAmount1(Double.toString(taxForm03Data.getExtraMoney()).replace(",", ""));
                        invoice.setDetail1("ค่าเพิ่ม");
                    }
                }
                String totalAmt = AppUtil.numberFormatNoCommaAnd2Digit(Double.toString(taxForm03Data.getTaxTotal() + taxForm03Data.getExtraMoney()).replace(",", ""));
                invoice.setTotalAmount(totalAmt);

                invoice.setOffice("สำนักงานเขต" + taxForm03Data.getRefOfficeName());
                invoice.setPartOfOffice("(ฝ่ายรายได้)");
                invoice.setTelephone(officeData.getMobile());

                String barcode = "|" + invoice.getBillNo() + " " + invoice.getRef1() + " " + invoice.getRef2() + " " + totalAmt.replace(".", "");
                invoice.setBarcode(barcode);
                invoice.setQrcode(barcode);

                invoice.setTaxTypeCode(taxType);
                invoice.setTaxTypeName(taxTypeName);

                //</editor-fold>
                String jsonStr = new Gson().toJson(invoice);
                ResponseInvoiceDataModel resultInvoice = new ResponseInvoiceDataModel();
                resultInvoice = WebserviceUtil.actionPost(urlWsBma, jsonStr);
                invoice.setBmaBillNo(resultInvoice.getBmaBillNo());

                if (resultInvoice.getBmaBillNo() != null && resultInvoice.getBmaBillNo() != "") {
                    //insert into invoice
                    InvoiceRepo invoiceRepo = new InvoiceRepo();
                    invoiceRepo.saveInvoice(invoice);
                }
            }
        }
        
        //</editor-fold>
        return true;
    }

    public ResultData<Boolean> deleteData(FilterTaxFormModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int id = AppUtil.decryptId(i);
                idDeleteList.add(id);
            }

            resultData = repo.deleteData(idDeleteList, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<StatusAccountViewModel> getStatusAndFinesByMonth(StatusAccountViewModel filter) {

        ResultData<StatusAccountViewModel> data = new ResultData<>();//service.getData(filter);

        try {
            StatusAccountViewModel result = new StatusAccountViewModel();
            result.setRefRetailStationId(filter.getRefRetailStationId());
            result.setYearly(filter.getYearly());
            result.setMonthly(filter.getMonthly());

            //ดูจากเดือนที่ยื่น เทียบกับวันที่ปัจจุบัน
            //ถ้ายื่นก่อนวันที่ 15 ของเดือนถัดไป ถือว่ายื่นภายในกำหนด
            //    - การยื่นเพิ่มเติมดูจากการยื่นของเดือนนั้นๆ แล้วนับ count ไปเรื่อยๆ
            //ถ้ายื่นหลังวันที่ 15 ของเดือนถัดไป ถือว่ายื่นเกินกำหนด
            //    - การยื่นเพิ่มเติมดูจากการยื่นของเดือนนั้นๆ แล้วนับ count ไปเรื่อยๆ
            String statusAcc = "1";
            String statusAccDesc = "ภายในกำหนดเวลา";
            Integer addTimes = 0;

//            Double finesPerMonth = 1.5;//            
            Double finesPerMonth = shareRepo.getTaxRateByDatenow();//อัตราดอกเบี้ยปัจจุบัน

            Double finesTotal = 0.0;
            String finesStartDate = "-";
            String finesEndDate = "-";
            Integer finesMonth = 0;
            Boolean hasHoliday = false;//วันหยุดถือว่าไม่นับ

            //ดึงค่าปรับปัจจุบัน
            FeeViewModel fee = new FeeViewModel();
            fee = repo.getDataFeeCurrent();

            if (fee.getFeeId() != null) {
                finesPerMonth = fee.getFeeValue().doubleValue();
                hasHoliday = fee.getHasHoliday();
            }

            //Set date expire
            Integer month = filter.getMonthly() + 1;

            Integer year = filter.getYearly();
            if (month > 12) {
                month = 1;
                year = year + 1;
            }
            // get วันที่คิดค่าปรับจาก general_Setting
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            String feeDateStr = "16";
            String dueDateStr = feeDateStr + "/" + month.toString() + "/" + AppUtil.toYearENString(year.toString());
            Date dueDate = formatter.parse(dueDateStr);

            String payDate = shareRepo.getDataValue(SettingGeneralEnum.PayTaxDate);//วันที่ชำระภาษี
            if (!AppUtil.isNullAndSpace(payDate)) {
                feeDateStr = payDate;
                dueDateStr = feeDateStr + "/" + month.toString() + "/" + AppUtil.toYearENString(year.toString());
                dueDate = formatter.parse(dueDateStr);
                dueDateStr = DateUtils.toEng(dueDate, "dd/MM/yyyy");
            }

            //ตรวจสอบว่าวันที่ยื่นแบบเป็นวันหยุดหรือไม่
            if (hasHoliday == false) {
                Boolean isHoliday = false;
                isHoliday = repo.checkHoliday(dueDateStr);
                while (isHoliday == true) {
                    //Cal +1 Day
                    dueDate = formatter.parse(dueDateStr);
                    // convert date to localdatetime
                    LocalDateTime localDateTime = dueDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                    System.out.println("localDateTime : " + dateFormat8.format(localDateTime));

                    // plus one
                    localDateTime = localDateTime.plusDays(1);

                    // convert LocalDateTime to date
                    Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

                    dueDate = currentDatePlusOneDay;
                    dueDateStr = DateUtils.toEng(dueDate, "dd/MM/yyyy");
                    isHoliday = repo.checkHoliday(dueDateStr);
                }
            }

            //Cal +1 Day
            dueDate = formatter.parse(dueDateStr);
            // convert date to localdatetime
            LocalDateTime localDateTime = dueDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            System.out.println("localDateTime : " + dateFormat8.format(localDateTime));

            // plus one
            localDateTime = localDateTime.plusDays(1);

            // convert LocalDateTime to date
            Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

            dueDate = currentDatePlusOneDay;

            // Get current date;
            Date currentDate = new Date();

            //check count
            Integer countData = repo.checkCount03(filter);

            if (currentDate.before(dueDate)) {
                //ถ้ายื่นก่อนวันที่ 15 ของเดือนถัดไป ถือว่ายื่นภายในกำหนด
                statusAcc = "1";
                statusAccDesc = "ภายในกำหนดเวลา";
                if (countData > 0) {
                    //ยื่นเพิ่มเติม
                    countData = countData + 1;
                    statusAcc = "3";
                    statusAccDesc = "ยื่นเพิ่มเติมครั้งที่ " + countData;
                    addTimes = countData;
                }

            } else {
                statusAcc = "2";
                statusAccDesc = "เกินกำหนดเวลา";
                if (countData > 0) {
                    //ยื่นเพิ่มเติม
                    countData = countData + 1;
                    statusAcc = "4";
                    statusAccDesc = "ยื่นเพิ่มเติมครั้งที่ " + countData + " (เกินกำหนดเวลา)";
                    addTimes = countData;
                }

                finesStartDate = DateUtil.toDateYearThai(dueDate);
                finesEndDate = DateUtil.toDateYearThai(currentDate);
                if (!finesStartDate.equals(finesEndDate)) {

                    Calendar startCalendar = new GregorianCalendar();
                    startCalendar.setTime(dueDate);
//                    startCalendar.add(Calendar.DATE, -1);
                    Calendar endCalendar = new GregorianCalendar();
                    endCalendar.setTime(currentDate);

                    int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
                    int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
//                int diffMonth = DateUtil.diffTwoDate(dueDate,currentDate,"M");

                    Period diff = Period.between(DateUtil.convertToLocalDateViaInstant(dueDate), DateUtil.convertToLocalDateViaInstant(currentDate));

                    finesTotal = diffMonth * finesPerMonth;
                    finesMonth = diff.getMonths() + 1;//diffMonth + 1;
                } else {
                    finesMonth = 1;
                }
            }

            result.setStatusAcc(statusAcc);
            result.setStatusAccDesc(statusAccDesc);
            result.setAddTimes(addTimes);
            result.setFinesPerMonth(finesPerMonth);
            result.setFinesTotal(finesTotal);
            result.setFinesMonth(finesMonth);
            result.setFinesStartDate(finesStartDate);
            result.setFinesEndDate(finesEndDate);

            data.setResult(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<List<StatusLogViewModel>> getListStatusLog(FilterTaxFormModel filter) {

        ResultData<List<StatusLogViewModel>> resultData = new ResultData<List<StatusLogViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListStatusLog(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public String getDueDatePaymentNow() {
//หาวันครบกำหนดชำระ ที่สามารถนำไปจ่ายเงินได้
        String dueDateStr = "";
        try {
            // get วันที่คิดค่าปรับจาก general_Setting
            String payDate = shareRepo.getDataValue(SettingGeneralEnum.PayTaxDate);//วันที่ชำระภาษี

            Date today = new Date();
            Locale lc = new Locale("th", "TH");
            Calendar cal = Calendar.getInstance(lc);
            cal.setTime(today);
            int yearly = cal.get(Calendar.YEAR);
            int monthly = cal.get(Calendar.MONTH) + 1;
            int currday = cal.get(Calendar.DATE);

            if (currday > Integer.parseInt(payDate)) {
                //ถ้าวันที่ปัจจุบันมากกว่าวันที่ครบชำระในเบส ให้เดือน +1
                monthly = monthly + 1;

                if (monthly > 12) {
                    monthly = 1;
                    yearly = yearly + 1;
                }
            }
            String month = String.format("%02d", monthly);

            dueDateStr = payDate + "/" + month + "/" + Integer.toString(yearly).substring(0, 4);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dueDateStr;
    }

}
