/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import repository.StatusLogRepo;
import repository.TaxForm02Repo;
import repository.mapper.TaxForm02Mapper;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxForm02Model;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm02ViewModel;

/**
 *
 * @author User
 */
public class TaxForm02Service {
    
    TaxForm02Repo repo = new TaxForm02Repo();
    StatusLogRepo statusRepo = new StatusLogRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();
    
    public ResultData<List<TaxForm02ViewModel>> getList(FilterTaxForm02Model filter) {

        ResultData<List<TaxForm02ViewModel>> resultData = new ResultData<List<TaxForm02ViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListForOfficer(resultPage, filter);
            
            if(resultData.getResult() != null ){
                for (TaxForm02ViewModel child : resultData.getResult()) {
                    List<DrpDownViewModel> businessList = repo.getListBusinessStatus(child.getTaxForm02Id());
                    child.setBusinessStatusList(businessList);
                }
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }
     
    public ResultData<Boolean> approve(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
//                TaxForm02Mapper mapper = new TaxForm02Mapper();
//                dataSave = mapper.mapSaveData(data);
                resultData = statusRepo.approveTaxForm02(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }
     
    public ResultData<Boolean> reject(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
//                TaxForm02Mapper mapper = new TaxForm02Mapper();
//                dataSave = mapper.mapSaveData(data);
                resultData = statusRepo.rejectTaxForm02(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }
    
}
