/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.StatusEnum;
import java.util.ArrayList;
import java.util.List;
import model.TaxForm01RetailModel;
import repository.AppConfigRepo;
import repository.MemberBMARepo;
import repository.RetailRepo;
import repository.StatusLogRepo;
import repository.TaxForm01Repo;
import repository.mapper.TaxForm01Mapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 *
 * @author User
 */
public class FrontTaxForm01Service {

    TaxForm01Repo repo = new TaxForm01Repo();
    StatusLogRepo statusRepo = new StatusLogRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<TaxForm01RetailViewModel>> getList(FilterTaxFormModel filter) {

        ResultData<List<TaxForm01RetailViewModel>> resultData = new ResultData<List<TaxForm01RetailViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            RetailRepo retailRepo = new RetailRepo();
            String taxNo = retailRepo.getTaxNoByMemberName(filter.getMemberName());

            if (!AppUtil.isNullAndSpace(taxNo)) {
                filter.setTaxNo(taxNo);
                resultData = repo.getList(resultPage, filter);
                resultData.setStatus("true");
                resultData.setResultMessage("");
            } else {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListRetail(FilterTaxFormModel filter) {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getDrpListRetail(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListStation(FilterTaxFormModel filter) {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getDrpListStation(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<List<RetailStationViewModel>> getListStatusLog(FilterTaxFormModel filter) {

        ResultData<List<RetailStationViewModel>> resultData = new ResultData<List<RetailStationViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListStatusLog(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<TaxForm01RetailViewModel> getData(FilterTaxFormModel filter) {

        ResultData<TaxForm01RetailViewModel> resultData = new ResultData<TaxForm01RetailViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getTaxForm01RetailId());
            resultData = repo.getData(id);

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getTaxForm01RetailId())) {
                resultData.setResult(new TaxForm01RetailViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<TaxForm01StationViewModel> getDataStation(FilterTaxFormModel filter) {

        ResultData<TaxForm01StationViewModel> resultData = new ResultData<TaxForm01StationViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getId());
            resultData = repo.getDataStation(id);

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getTaxForm01RetailId())) {
                resultData.setResult(new TaxForm01StationViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<Boolean> saveData(TaxForm01RetailViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        TaxForm01RetailModel dataSave = new TaxForm01RetailModel();

        try {

            if (!AppUtil.isNullAndSpace(data.getTaxNo())) {
                //ตรวจสอบว่า taxno นี้เคยมีการสร้างแล้วหรือยัง
                FilterTaxFormModel filter = new FilterTaxFormModel();
                filter.setTaxNo(data.getTaxNo());
                filter.setMemberName(data.getMemberName());
                RetailRepo retailRepo = new RetailRepo();
                Boolean hasTaxNo = retailRepo.duplicateTaxNo(filter);

                if (hasTaxNo == true) {
                    //กรณีมี TaxNo แล้ว
                    resultData.setResult(false);
                    resultData.setStatus("false");
                    resultData.setResultMessage(message.getMessage("message.retail.duplicate"));
                } else {
                    TaxForm01Mapper mapper = new TaxForm01Mapper();
                    if (data.getMainStatus() == null || "".equals(data.getMainStatus())) {
                        data.setMainStatus(Integer.toString(StatusEnum.Create.value()));
                    }
                    dataSave = mapper.mapSaveData(data);
                    resultData = repo.save(dataSave);

                    //ตรวจสอบว่าต้องเชื่อมข้อมูลไปที่ bma หรือไม่
                    AppConfigRepo appConfigRepo = new AppConfigRepo();
                    String linkMember = appConfigRepo.GetValue("appConfigRepo");
                    //ถ้ากำหนดไว้ว่าต้องเชื่อม BMA ด้วย ก็ให้ทำการสร้าง member ที่ BMA ด้วย
                    if ("true".equals(linkMember)) {
                        MemberBMARepo memberBmaRepo = new MemberBMARepo();
                        boolean resultBMA = memberBmaRepo.updateTaxNo(data.getMemberName(), dataSave.getTaxNo());
                    }
                }
            } else {
                resultData.setResult(false);
                resultData.setStatus("false");
                resultData.setResultMessage(message.getMessage("message.retail.taxNoNotEmpty"));
            }

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> sendApprove(StatusLogViewModel data) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            TaxForm01Mapper mapper = new TaxForm01Mapper();
//                dataSave = mapper.mapSaveData(data);
            resultData = statusRepo.sendApproveTaxForm01(data);

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> deleteData(FilterTaxFormModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            resultData = repo.deleteData(idDeleteList, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }
}
