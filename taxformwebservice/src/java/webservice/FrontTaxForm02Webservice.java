/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.FrontTaxForm02Service;
import viewModel.FilterTaxForm02Model;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm02ViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("fronttaxform02")
public class FrontTaxForm02Webservice {

    @Context
    private UriInfo context;

    FrontTaxForm02Service service = new FrontTaxForm02Service();
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterTaxForm02Model filter) {

        try {

            ResultData<List<TaxForm02ViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListStatusLog")
    public String getListStatusLog(FilterTaxFormModel filter) {

        try {

            ResultData<List<StatusLogViewModel>> data = service.getListStatusLog(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterTaxForm02Model filter) {

        try {

            ResultData<TaxForm02ViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(TaxForm02ViewModel data) {

        try {

            ResultData<Boolean> result = service.saveData(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(FilterTaxForm02Model filter) {

        try {

            return new Gson().toJson(service.deleteData(filter));
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("sendApprove")
    public String sendApprove(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.sendApprove(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
}
