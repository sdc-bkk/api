/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.FrontPaymentService;
import viewModel.FilterModel;
import viewModel.FrontInvoiceViewModel;
import viewModel.ResultData;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("frontpayment")
public class FrontPaymentWebservice {

    @Context
    private UriInfo context;
    FrontPaymentService service = new FrontPaymentService();

    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterModel filter) {

        try {

            ResultData<List<FrontInvoiceViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListHistory")
    public String getListHistory(FilterModel filter) {

        try {

            ResultData<List<FrontInvoiceViewModel>> data = service.getListHistory(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

}
