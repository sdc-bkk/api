/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.AttachFileCateEnum;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.DocFormService;
import viewModel.DocFormFileViewModel;
import viewModel.DocFormStationViewModel;
import viewModel.DocFormViewModel;
import viewModel.DrpDownViewModel;
import viewModel.FilterDocFormModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("docform")
public class DocFormWebservice {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of DocFormWebservice
     */
    public DocFormWebservice() {
    }
    DocFormService service = new DocFormService();

    //<editor-fold defaultstate="collapsed" desc="for retail">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListForRetail")
    public String getListForRetail(FilterTaxFormModel filter) {

        try {

            ResultData<List<DocFormViewModel>> data = service.getListForRetail(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListForRetailFile")
    public String getListForRetailFile(FilterTaxFormModel filter) {

        try {

            ResultData<List<DocFormFileViewModel>> data = service.getListForRetailFile(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveDataRetailAddFile")
    public String saveDataRetailAddFile(DocFormFileViewModel data) {

        try {

            ResultData<Boolean> result = service.saveDataRetailAddFile(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteDataRetailAddFile")
    public String deleteDataRetailAddFile(FilterTaxFormModel filter) {

        try {

            return new Gson().toJson(service.deleteDataRetailAddFile(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListFileCateRetail")
    public String getDrpListFileCateRetail() {

        try {

            ResultData<List<DrpDownViewModel>> data = new ResultData<>();
            List<DrpDownViewModel> drps = new ArrayList<>();
            for (AttachFileCateEnum e : AttachFileCateEnum.values()) {
                if (e.display().equals("H") || e.display().equals("C") || e.display().equals("A") || e.display().equals("O")) {
                    DrpDownViewModel drpDownViewModel = new DrpDownViewModel();
                    drpDownViewModel.setId(e.display());
                    drpDownViewModel.setName(e.docName());
                    drps.add(drpDownViewModel);
                }
            }
            data.setResult(drps);
            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="for station">
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListForStation")
    public String getListForStation(FilterTaxFormModel filter) {

        try {

            ResultData<List<DocFormViewModel>> data = service.getListForStation(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListForStationFile")
    public String getListForStationFile(FilterTaxFormModel filter) {

        try {

            ResultData<List<DocFormFileViewModel>> data = service.getListForStationFile(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveDataStationAddFile")
    public String saveDataStationAddFile(DocFormFileViewModel data) {

        try {

            ResultData<Boolean> result = service.saveDataStationAddFile(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteDataStationAddFile")
    public String deleteDataStationAddFile(FilterTaxFormModel filter) {

        try {

            return new Gson().toJson(service.deleteDataStationAddFile(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListFileCateStation")
    public String getDrpListFileCateStation() {

        try {

            ResultData<List<DrpDownViewModel>> data = new ResultData<>();
            List<DrpDownViewModel> drps = new ArrayList<>();
            for (AttachFileCateEnum e : AttachFileCateEnum.values()) {
                if (e.display().equals("A") || e.display().equals("R") || e.display().equals("O")) {
                    DrpDownViewModel drpDownViewModel = new DrpDownViewModel();
                    drpDownViewModel.setId(e.display());
                    drpDownViewModel.setName(e.docName());
                    drps.add(drpDownViewModel);
                }
            }
            data.setResult(drps);
            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    //</editor-fold>
    
}
