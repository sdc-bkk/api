/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.FrontTaxForm01Service;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.RetailStationViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("fronttaxform01")
public class FrontTaxForm01Webservice {

    @Context
    private UriInfo context;

    FrontTaxForm01Service service = new FrontTaxForm01Service();
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterTaxFormModel filter) {

        try {

            ResultData<List<TaxForm01RetailViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListRetail")
    public String getDrpListRetail(FilterTaxFormModel filter) {

        try {

            ResultData<List<DrpDownViewModel>> data = service.getDrpListRetail(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListStation")
    public String getDrpListStation(FilterTaxFormModel filter) {

        try {

            ResultData<List<DrpDownViewModel>> data = service.getDrpListStation(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListStatusLog")
    public String getListStatusLog(FilterTaxFormModel filter) {

        try {

            ResultData<List<RetailStationViewModel>> data = service.getListStatusLog(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterTaxFormModel filter) {

        try {

            ResultData<TaxForm01RetailViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(TaxForm01RetailViewModel data) {

        try {

            ResultData<Boolean> result = service.saveData(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataStation")
    public String getDataStation(FilterTaxFormModel filter) {

        try {

            ResultData<TaxForm01StationViewModel> data = service.getDataStation(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
        
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("sendApprove")
    public String sendApprove(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.sendApprove(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(FilterTaxFormModel filter) {

        try {

            return new Gson().toJson(service.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
