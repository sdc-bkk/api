/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.TaxForm01Service;
import service.WarnFormService;
import service.WarnPayService;
import viewModel.FilterModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.TaxForm01StationViewModel;
import viewModel.WarnFormViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("warnpay")
public class WarnPayWebservice {

    @Context
    private UriInfo context;
    
    WarnPayService service = new WarnPayService();
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterModel filter) {

        try {

            ResultData<List<WarnFormViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListByMonthly")
    public String getListByMonthly(FilterModel filter) {

        try {

            ResultData<List<WarnFormViewModel>> data = service.getListByMonthly(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }  
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(WarnFormViewModel data) {

        try {

            ResultData<Boolean> result = service.saveData(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListLog")
    public String getListLog(FilterModel filter) {

        try {

            ResultData<List<WarnFormViewModel>> data = service.getListLog(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }  
        
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(FilterModel filter) {

        try {

            return new Gson().toJson(service.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("updateStatusSendMail")
    public String updateStatusSendMail(FilterModel filter) {
//สำหรับ update status อ่านแล้ว
        try {

            return new Gson().toJson(service.updateStatusSendMail(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("updateStatusCancelMail")
    public String updateStatusCancelMail(FilterModel filter) {
//สำหรับ update status อ่านแล้ว
        try {

            return new Gson().toJson(service.updateStatusCancelMail(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("sendMail")
    public String sendMail(FilterModel filter) {

        try {

            return new Gson().toJson(service.sendMail(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("sendMailCancel")
    public String sendMailCancel(FilterModel filter) {

        try {

            return new Gson().toJson(service.sendMailCancel(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
