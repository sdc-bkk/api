/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.FrontRetailService;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.TaxForm01RetailViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("frontretail")
public class FrontRetailWebservice {

    @Context
    private UriInfo context;
    FrontRetailService service = new FrontRetailService();

    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataByMemberName")
    public String getDataByMemberName(FilterTaxFormModel filter) {

        try {

            ResultData<RetailViewModel> data = service.getDataByMemberName(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataByTaxNo")
    public String getDataByTaxNo(FilterTaxFormModel filter) {

        try {

            ResultData<RetailViewModel> data = service.getDataByTaxNo(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListStationByTaxNo")
    public String getListStationByTaxNo(FilterTaxFormModel filter) {

        try {

            ResultData<List<DrpDownViewModel>> data = service.getListStationByTaxNo(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataStation")
    public String getDataStation(FilterTaxFormModel filter) {

        try {

            ResultData<RetailStationViewModel> data = service.getDataStation(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListAllStationByMemberName")
    public String getListAllStationByMemberName(FilterTaxFormModel filter) {

        try {

            ResultData<List<RetailStationViewModel>> data = service.getListAllStationByMemberName(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
