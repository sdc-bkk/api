/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.AttachFileCateEnum;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.FrontTaxForm03Service;
import service.FrontTaxFormRefundService;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.MonthlyViewModel;
import viewModel.ResultData;
import viewModel.TaxForm03ViewModel;
import viewModel.TaxFormRefundViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("fronttaxformrefund")
public class FrontTaxFormRefundWebservice {

    @Context
    private UriInfo context;
    FrontTaxFormRefundService service = new FrontTaxFormRefundService();

    /**
     * Creates a new instance of FrontTaxFormRefund
     */
    public FrontTaxFormRefundWebservice() {
    }

    /**
     * Retrieves representation of an instance of
     * webservice.FrontTaxFormRefundWebservice
     *
     * @return an instance of java.lang.String
     */
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterTaxFormModel filter) {

        try {

            ResultData<List<TaxFormRefundViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListStatus")
    public String getDrpListStatus() {

        try {

            ResultData<List<DrpDownViewModel>> data = service.getDrpListStatus();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(TaxFormRefundViewModel data) {

        try {

            ResultData<Boolean> result = service.saveData(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterTaxFormModel filter) {

        try {

            ResultData<TaxFormRefundViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(FilterTaxFormModel filter) {

        try {
            ResultData<Boolean> deleteData = service.deleteData(filter);
            return new Gson().toJson(deleteData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of
     * FrontTaxFormRefundWebservice
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }

    public static void main(String[] args) {
        TaxFormRefundViewModel m = new TaxFormRefundViewModel();
        AppUtil appUtil = new AppUtil();

        String retailId = appUtil.encryptId(21);
        String stationId = appUtil.encryptId(173);

        m.setRefRetailId(retailId);
        m.setRefStationId(stationId);
        m.setStationName("ปั๊มก๊าซบางจาก ทองทวี");
        m.setMonthly(1);
        m.setYearly(2564);
        m.setAmount(5000);
        m.setReceiptNo("adf00001");
        m.setReceiptDate("25/01/2564");

        String toJson = new Gson().toJson(m);

        System.out.println("toJson " + toJson);
    }
}
