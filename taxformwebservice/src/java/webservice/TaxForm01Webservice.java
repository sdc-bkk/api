/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.TaxForm01Service;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("taxform01")
public class TaxForm01Webservice {

    @Context
    private UriInfo context;

    TaxForm01Service service = new TaxForm01Service();
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterTaxFormModel filter) {

        try {

            ResultData<List<TaxForm01StationViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("approve")
    public String approve(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.approve(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("reject")
    public String reject(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.reject(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
   
    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListStatus")
    public String getDrpListStatus() {

        try {

            ResultData<List<DrpDownViewModel>> data = service.getDrpListStatus();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
        
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterTaxFormModel filter) {

        try {

            ResultData<TaxForm01StationViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
