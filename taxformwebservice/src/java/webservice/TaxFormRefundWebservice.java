/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.SelectListModel;
import service.TaxFormRefundService;
import viewModel.FilterTaxFormModel;
import viewModel.ResultData;
import viewModel.TaxFormRefundViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("taxrefund")
public class TaxFormRefundWebservice {

    @Context
    private UriInfo context;
    TaxFormRefundService service = new TaxFormRefundService();

    /**
     * Creates a new instance of TaxFormRefundWebservice
     */
    public TaxFormRefundWebservice() {
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterTaxFormModel filter) {

        try {

            ResultData<List<TaxFormRefundViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterTaxFormModel filter) {

        try {

            ResultData<TaxFormRefundViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("accept")
    public String accept(TaxFormRefundViewModel data) {

        try {
            ResultData<Boolean> accept = service.accept(data);
            return new Gson().toJson(accept);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("approve")
    public String approve(TaxFormRefundViewModel data) {

        try {
            ResultData<Boolean> approve = service.approve(data);
            return new Gson().toJson(approve);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("reject")
    public String reject(TaxFormRefundViewModel data) {

        try {
            ResultData<Boolean> reject = service.reject(data);
            return new Gson().toJson(reject);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

   
    @GET
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getStatusList")
    public String getStatusList() {

        try {

            ResultData<List<SelectListModel>> data = service.getStatusList();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
