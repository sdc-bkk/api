/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.FrontTaxForm03Service;
import viewModel.FilterTaxFormModel;
import viewModel.MonthlyViewModel;
import viewModel.ResultData;
import viewModel.RetailStationViewModel;
import viewModel.StatusAccountViewModel;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm03ViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("fronttaxform03")
public class FrontTaxForm03Webservice {

    @Context
    private UriInfo context;

    FrontTaxForm03Service service = new FrontTaxForm03Service();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterTaxFormModel filter) {

        try {

            ResultData<List<MonthlyViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListByMonthly")
    public String getListByMonthly(FilterTaxFormModel filter) {

        try {

            ResultData<List<TaxForm03ViewModel>> data = service.getListByMonthly(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterTaxFormModel filter) {

        try {

            ResultData<TaxForm03ViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    //ดึงสถานะการยื่นแบบ
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getStatusAndFinesByMonth")
    public String getStatusAndFinesByMonth(StatusAccountViewModel filter) {

        try {

            ResultData<StatusAccountViewModel> data = new ResultData<>();//service.getData(filter);
            data = service.getStatusAndFinesByMonth(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getListStatusLog")
    public String getListStatusLog(FilterTaxFormModel filter) {

        try {

            ResultData<List<StatusLogViewModel>> data = service.getListStatusLog(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(TaxForm03ViewModel data) {

        try {

            ResultData<Boolean> result = service.saveData(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("sendApprove")
    public String sendApprove(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.sendApprove(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("test")
    public String test(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.test(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(FilterTaxFormModel filter) {

        try {

            return new Gson().toJson(service.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
