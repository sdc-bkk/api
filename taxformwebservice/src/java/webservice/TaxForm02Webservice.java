/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.TaxForm02Service;
import viewModel.DrpDownViewModel;
import viewModel.FilterTaxForm02Model;
import viewModel.ResultData;
import viewModel.StatusLogViewModel;
import viewModel.TaxForm02ViewModel;
import viewModel.TaxForm02ViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("taxform02")
public class TaxForm02Webservice {

    @Context
    private UriInfo context;

    TaxForm02Service service = new TaxForm02Service();
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterTaxForm02Model filter) {

        try {

            ResultData<List<TaxForm02ViewModel>> data = service.getList(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("approve")
    public String approve(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.approve(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("reject")
    public String reject(StatusLogViewModel data) {

        try {

            ResultData<Boolean> result = service.reject(data);

            return new Gson().toJson(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
}
