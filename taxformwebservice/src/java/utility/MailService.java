/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import utility.AppConfig;

/**
 *
 * @author Prapaporn
 */
public class MailService {

    AppConfig config = new AppConfig();
    private final String username = config.value("mail.username");
    private final String password = config.value("mail.password");
    private final String host = config.value("mail.smtp.host");
    private final String port = config.value("mail.smtp.port");
    private final String auth = config.value("mail.smtp.auth");
//    private final String socketFactoryPort = config.value("mail.smtp.socketFactory.port");
//    private final String socketFactoryClass = config.value("mail.smtp.socketFactory.class");

    private String form = config.value("mail.form");
    private String displayFormEmail = config.value("mail.displayFormEmail");
    private String to;
    private String cc;
    private String bcc;
    private String subject;
    private String messageText;

    private Properties getProperties() {
        Properties props = new Properties();
        try {
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.auth", auth);
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.socketFactory.port", socketFactoryPort);
//            props.put("mail.smtp.socketFactory.class", socketFactoryClass);
            return props;
        } catch (Exception ex) {

        }
        return props;
    }

    private Session getDefaultInstance() {
        try {
            return Session.getDefaultInstance(getProperties(),
                    new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
//                                            return new PasswordAuthentication(username,new String(decodePassword) );
                    return new PasswordAuthentication(username, password);
                }
            });

        } catch (Exception ex) {

        }
        return null;
    }

    public boolean sendMail() {
        try {
            // System.out.println("Base64 : "+Base64.encodeBase64(password));
            if (form != null && to != null && subject != null && !form.equals("") && !to.equals("") && !subject.equals("")) {

                Session session = getDefaultInstance();

                System.out.println("Senting message....");

                //Message message = new MimeMessage(session);
                MimeMessage message = new MimeMessage(session);

                message.setFrom(new InternetAddress(form, displayFormEmail, "UTF-8"));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
                if (cc != null && !cc.equals("")) {
                    message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
                }

                if (bcc != null && !bcc.equals("")) {
                    message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
                }
                message.setSubject(subject, "UTF-8");
                message.setContent(messageText, "text/html; charset=UTF-8");

                Transport.send(message);
                System.out.println("Sent message successfully....");
                return true;
            }
        } catch (Exception ex) {
            System.out.println("++++" + ex.getMessage());
        }
        return false;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public static void main(String[] args) {
        MailService test = new MailService();
        test.setTo("kingwerewolf@gmail.com");
        test.setSubject("test send mail");
        test.setMessageText("ทดสอบส่ง เมล์ ถึง kingwerewolf@gmail.com");
        boolean ddd = test.sendMail();
        String temp = "";
    }

}
