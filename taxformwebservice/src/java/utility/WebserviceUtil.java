/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import enumeration.AppConfigEnum;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import model.ws.ResponseInvoiceBeanModel;
import model.ws.ResponseInvoiceDataModel;
import model.ws.ResultPaymentModel;
import repository.AppConfigRepo;
import viewModel.ResultData;

/**
 *
 * @author User
 */
public class WebserviceUtil {

//    public static void ActionGet(String uri) throws IOException {
//        URL urlForGetRequest = new URL("https://jsonplaceholder.typicode.com/posts/1");
//        String readLine = null;
//        HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
//        conection.setRequestMethod("GET");
//        conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here
//        int responseCode = conection.getResponseCode();
//        if (responseCode == HttpURLConnection.HTTP_OK) {
//            StringBuilder response;
//            try (BufferedReader in = new BufferedReader(
//                    new InputStreamReader(conection.getInputStream()))) {
//                response = new StringBuilder();
//                while ((readLine = in.readLine()) != null) {
//                    response.append(readLine);
//                }
//            }
//            // print result
//            System.out.println("JSON String Result " + response.toString());
//            //GetAndPost.POSTRequest(response.toString());
//        } else {
//            System.out.println("GET NOT WORKED");
//        }
//    }
    public static ResponseInvoiceDataModel actionPost(String urlWS, String params) throws Exception {

        ResponseInvoiceBeanModel responseBean = new ResponseInvoiceBeanModel();
        ResponseInvoiceDataModel responseData = new ResponseInvoiceDataModel();
        try {

            AppConfigRepo appRepo = new AppConfigRepo();
            String clientId = appRepo.GetValue(AppConfigEnum.WsBmaClientId.displayNameTH());
            String clientSecret = appRepo.GetValue(AppConfigEnum.WsBmaClientSecret.displayNameTH());

            //---from ws
            System.out.println("json : " + params);
            URL url = new URL(urlWS);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf8");
            conn.setRequestProperty("client_id", clientId);
            conn.setRequestProperty("client_secret", clientSecret);

            byte[] outputInBytes = params.getBytes("UTF-8");
            OutputStream os = conn.getOutputStream();
            os.write(outputInBytes);
            os.flush();
            os.close();

            System.out.println("os : " + os.toString());

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn.getInputStream(), "UTF-8");
            //--------------------

            BufferedReader br = new BufferedReader(in);
            StringBuilder sb = new StringBuilder();

            String output = "";
            String inLine;

            while ((inLine = br.readLine()) != null) {
                output = output + inLine;
            }
            Object JsonToObject = JsonUtil.JsonToObject(output, (Class<Object>) (Object) ResponseInvoiceBeanModel.class);
            responseBean = (ResponseInvoiceBeanModel) JsonToObject;

            if (responseBean.getResult() != null) {
                responseData = responseBean.getResult();
            }
        } catch (Exception e) {
            throw e;
        }
        return responseData;
    }

    public static ResultData actionGet(String urlWS, String params) throws Exception {
        urlWS = "http://172.16.1.200:8094/bmaprovidewebservice/api/payment/checkPayment/BMA-6400000002";
        ResultData responseData = new ResultData();
        ResultPaymentModel responseBean = new ResultPaymentModel();
        ResponseInvoiceBeanModel responseData2 = new ResponseInvoiceBeanModel();
        try {
            //---from ws
            System.out.println("json : " + params);
            URL url = new URL(urlWS);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("client_id", "oil_system");
            conn.setRequestProperty("client_secret", "NWB6J1Xx3+vOJu33nV88rQIWcriWKR8k74Ax+IbMcms=");

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn.getInputStream(), "UTF-8");
            //--------------------

            BufferedReader br = new BufferedReader(in);
            StringBuilder sb = new StringBuilder();

            String output = "";
            String inLine;

            while ((inLine = br.readLine()) != null) {
                output = output + inLine;
            }
            Object JsonToObject = JsonUtil.JsonToObject(output, (Class<Object>) (Object) ResponseInvoiceBeanModel.class);
            responseData2 = (ResponseInvoiceBeanModel) JsonToObject;

//            Object s = new ResultPaymentModel();
//            Class<ResultPaymentModel> aa = responseData2.getResult().getClass();
//            if (responseBean.getResponseHeader() != null) {
//                if ("OK".equals(responseBean.getResponseHeader().getResponseCode())) {
//                    responseData = responseBean.getResponseData();
//                }
//            }
        } catch (Exception e) {
            throw e;
        }
        return responseData;
    }
}
