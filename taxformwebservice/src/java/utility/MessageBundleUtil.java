/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Prapaporn
 */
public class MessageBundleUtil {

    private final static String RESOURCE_BUNDLE = "messageBundle.message";

    private ResourceBundle messageSource;

    private Locale locale;

    public MessageBundleUtil() {

        locale = new Locale("th");
        Locale.setDefault(locale);
        messageSource = ResourceBundle.getBundle(RESOURCE_BUNDLE,
                locale, this.getClass().getClassLoader());
    }

    public String getMessage(String key) {
        return messageSource.getString(key);
    }

}
