/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import enumeration.SystemEnum;

/**
 *
 * @author Prapaporn
 */
public class FilePath {

    public static String getFilePath(String location) {
        try {
            String folderName ="";
            AppConfig appConfig = new AppConfig();

            switch (location) {
                case "content"://content
                    folderName = appConfig.value("file.path.content");
                    break;
                case "taxform01"://taxform01
                    folderName = appConfig.value("file.path.taxform01");
                    break;
                case "taxform02"://taxform02
                    folderName = appConfig.value("file.path.taxform02");
                    break;
                case "taxform03"://taxform03
                    folderName = appConfig.value("file.path.taxform03");
                    break;
                default:
                    folderName = appConfig.value("file.path.temp");
                    break;
            }
            return appConfig.value("file.path.root") + "/" + folderName ;
        } catch (Exception e) {
        }
        return null;
    }

    public static String getFilePath(String location,String filename) {
        try {
            String folderName ="";
            AppConfig appConfig = new AppConfig();

            switch (location) {
                case "content"://content
                    folderName = appConfig.value("file.path.content");
                    break;
                case "taxform01"://taxform01
                    folderName = appConfig.value("file.path.taxform01");
                    break;
                case "taxform02"://taxform02
                    folderName = appConfig.value("file.path.taxform02");
                    break;
                case "taxform03"://taxform03
                    folderName = appConfig.value("file.path.taxform03");
                    break;
                default:
                    folderName = appConfig.value("file.path.temp");
                    break;
            }
            return appConfig.value("file.path.root") + "/" + folderName + "/" + filename;
        } catch (Exception e) {
        }
        return null;
    }

    public static String temp() {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.temp");
        } catch (Exception e) {
        }
        return null;
    }

    public static String temp(String filename) {
        try {

            return temp() + "/" + filename;
        } catch (Exception e) {

        }
        return null;
    }

    public static String ebook(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.ebook") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String floor(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.floor") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String room(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.room") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String resource(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.resource") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String ebookLink(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.ebook.link") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String profile(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.profile") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String institution(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.institution") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String map(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.map") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }
    
    public static String building(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.building") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }
    
    public static String buildingFloor(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.building.floor") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String buildingZone(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.building.zone") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }
    
    public static String themeFile(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.theme.file") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }

    public static String content(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.content") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }
    
    public static String user(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.user") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }
    
    public static String station(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.station") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }
    
    public static String coverpage(String fileName) {
        try {
            AppConfig appConfig = new AppConfig();
            return appConfig.value("file.path.root") + "/" + appConfig.value("file.path.coverpage") + "/" + fileName;
        } catch (Exception e) {
        }
        return null;
    }
}
