/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Prapaporn
 */
public class AES {
    
    private static byte[] salt;
    private static boolean staticKey = false;
    String key = "t7w!z%C*F-JaNdRf";//"%^4KiL#iNu@Tdf!Y";//"Bar12345Bar12345"; // 128 bit key
    
    public String encrypt(String plainText) throws Exception {

        try {

            //get salt
            salt = key.getBytes();
            // Create key and cipher
            Key secret = new SecretKeySpec(salt, "AES");
            Cipher cipher = Cipher.getInstance("AES");

            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            byte[] encrypted = cipher.doFinal(plainText.getBytes());

            return Base64.encodeBase64String(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public String encryptURLSafe(String plainText) throws Exception {

        try {

            //get salt
            salt = key.getBytes();
            // Create key and cipher
            Key secret = new SecretKeySpec(salt, "AES");
            Cipher cipher = Cipher.getInstance("AES");

            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            byte[] encrypted = cipher.doFinal(plainText.getBytes());

            return Base64.encodeBase64URLSafeString(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
   

    @SuppressWarnings("static-access")
    public String decrypt(String encryptedText) throws Exception {

        try {
            salt =  key.getBytes();
            Key secret = new SecretKeySpec(salt, "AES");
            Cipher cipher = Cipher.getInstance("AES");

            // decrypt the text
            cipher.init(Cipher.DECRYPT_MODE, secret);

            return new String(cipher.doFinal(Base64.decodeBase64(encryptedText)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    public byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[16];
        random.nextBytes(bytes);
        return bytes;
    }
    
}
