/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.SystemEnum;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.DocumentFileModel;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import service.UploadService;
import utility.AppConfig;
import utility.FilePath;

/**
 * REST Web Service
 *
 * @author Prapaporn
 */
@Path("uploadfilewebservice")
public class UploadFileWebservice {

    @Context
    private UriInfo context;

    UploadService uploadService = new UploadService();

// <editor-fold defaultstate="collapsed" desc=" Manage Temp file ">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("/uploadFileBase64")
    public String uploadFileBase64(DocumentFileModel documentFile, @Context HttpServletRequest request) {

        try {

            AppConfig appConfig = new AppConfig();

            String filePath = appConfig.value("file.path.root") + "/" + appConfig.value("file.path.temp");
            String data = uploadService.uploadFile(documentFile, filePath, null);

//            System.out.println("uploadFileBase64 :: " + new Gson().toJson(data));
            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @POST
    @Consumes("multipart/form-data")
    @Produces("Application/json;charset=utf8")
    @Path("/uploadFileAndConvertPDF")
    public String uploadFileAndConvertPDF(@Context HttpServletRequest request) {

        try {
            AppConfig appConfig = new AppConfig();
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            String filePath = appConfig.value("file.path.root") + "/" + appConfig.value("file.path.temp");

            List<String> dataList = uploadService.uploadFilePDF(items, filePath, null);

            System.out.println("dataList :: " + new Gson().toJson(dataList));

            return new Gson().toJson(dataList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @POST
    @Consumes("multipart/form-data")
    @Produces("Application/json;charset=utf8")
    @Path("/uploadFile")
    public String uploadFile(@Context HttpServletRequest request) {

        try {
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

            String filePath = FilePath.getFilePath("temp");
            
            List<String> dataList = uploadService.uploadFile(items, filePath, null);

            System.out.println("dataList :: " + new Gson().toJson(dataList));

            return new Gson().toJson(dataList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Path("/getFileTemp/{fileName}")
    public Response getFileTemp(@PathParam("fileName") String fileName) throws IOException {

        String filePath = FilePath.getFilePath("temp",fileName);

        return uploadService.getFile(filePath, fileName);
    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("/deleteFileTemp/{fileName}")
    public void deleteFileTemp(@PathParam("fileName") String fileName) {

        String tempFile = FilePath.getFilePath("temp", fileName);;

        uploadService.delete(tempFile);

    }

    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc=" Manage real file ">    
    @GET
    @Produces("Application/json;charset=utf8")
    @Path("/moveFile/{fileName}/{location}")
    public void moveFile(@PathParam("fileName") String fileName, @PathParam("location") String location) {

        String tempFilePath = "";
        String documentFilePath = "";

        tempFilePath = FilePath.getFilePath("temp", fileName);
        documentFilePath = FilePath.getFilePath(location, fileName);

        uploadService.moveFile(tempFilePath, documentFilePath);

    }

    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Path("/getFile/{fileName}/{location}")
    public Response getFile(@PathParam("fileName") String fileName, @PathParam("location") String location) throws IOException {

        String filePath = FilePath.getFilePath(location,fileName);

        return uploadService.getFile(filePath, fileName);
    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("/deleteFile/{fileName}/{location}")
    public void deleteFile(@PathParam("fileName") String fileName, @PathParam("location") String location) {

        String tempFile = "";

        tempFile = FilePath.getFilePath(location, fileName);

        uploadService.delete(tempFile);

    }
    // </editor-fold>

}
