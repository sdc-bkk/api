/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

/**
 *
 * @author Prapaporn
 */
public enum SystemEnum {

    TEMP(0, "temp"),
    EBOOK(1, "ebook"),
    FLOOR(2, "floor"),
    ROOM(3, "room"),
    RESOURCE(4, "resource"),
    EBOOK_LINK(5, "ebookLink"),
    PROFILE(6, "profile"),
    INSTITUTION(7, "institution"),
    MAP(8, "map"),
    BUILDING(9, "building"),
    BUILDING_FLOOR(10, "buildingFloor"),
    BUILDING_ZONE(11, "buildingZone"),
    THEME_FILE(12, "themeFile"),
    CONTENT(13,"content"),
    USER(14,"user"),
    STATION(15,"station"),
    COVERPAGE(16,"coverpage");

    private final int value;

    private final String displayName;

    SystemEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int value() {
        return value;
    }

    public String display() {
        return displayName;
    }

}
