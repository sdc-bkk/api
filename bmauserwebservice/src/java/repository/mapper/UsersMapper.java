/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.UserRoleModel;
import model.UsersModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.UserRoleViewModel;
import viewModel.UsersViewModel;

/**
 *
 * @author Sirichai
 */
public class UsersMapper {

    private ResultSet rs = null;

    public UsersMapper() {

    }

    public UsersMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<UsersViewModel> mapFullList() {

        List<UsersViewModel> dataList = new ArrayList<UsersViewModel>();

        try {

            while (rs.next()) {

                UsersViewModel data = new UsersViewModel();

                data.setUserId(AppUtil.encryptId(rs.getInt("USER_ID")));
                data.setUserCode(rs.getString("USER_CODE"));
                data.setUserName(rs.getString("USER_NAME"));
                data.setFullName(rs.getString("FULL_NAME"));
                data.setFirstName(rs.getString("FIRST_NAME"));
                data.setLastName(rs.getString("LAST_NAME"));
                
                data.setMobile(rs.getString("MOBILE"));
                data.setEmail(rs.getString("EMAIL"));
                data.setOffice(rs.getString("OFFICE"));
                data.setPartOf(rs.getString("PART_OF"));
                data.setDepartment(rs.getString("DEPARTMENT"));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<UsersViewModel> mapFullDrpList() {

        List<UsersViewModel> dataList = new ArrayList<UsersViewModel>();

        try {

            while (rs.next()) {

                UsersViewModel data = new UsersViewModel();

                data.setUserId(AppUtil.encryptId(rs.getInt("USER_ID")));
                data.setUserName(rs.getString("USER_NAME"));
                data.setFullName(rs.getString("FULL_NAME"));
                data.setFirstName(rs.getString("FIRST_NAME"));
                data.setLastName(rs.getString("LAST_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public UsersViewModel mapFull() {

        UsersViewModel data = new UsersViewModel();

        try {

            while (rs.next()) {

                data.setUserId(AppUtil.encryptId(rs.getInt("USER_ID")));
                data.setUserCode(rs.getString("USER_CODE"));
                data.setUserName(rs.getString("USER_NAME"));
                data.setFullName(rs.getString("FULL_NAME"));
                data.setFirstName(rs.getString("FIRST_NAME"));
                data.setLastName(rs.getString("LAST_NAME"));
                
                data.setMobile(rs.getString("MOBILE"));
                data.setEmail(rs.getString("EMAIL"));
                data.setOffice(rs.getString("OFFICE"));
                data.setPartOf(rs.getString("PART_OF"));
                data.setDepartment(rs.getString("DEPARTMENT"));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public UsersModel mapFull(UsersViewModel dataView) {

        UsersModel data = new UsersModel();

        try {
            data.setUserId(AppUtil.decryptId(dataView.getUserId()));
            data.setUserCode(dataView.getUserCode());
            data.setUserName(dataView.getUserName());
            
            data.setFirstName(dataView.getFirstName());
            data.setLastName(dataView.getLastName());
            
            data.setMobile(dataView.getMobile());
            data.setEmail(dataView.getEmail());
            data.setOffice(dataView.getOffice());
            data.setPartOf(dataView.getPartOf());
            data.setDepartment(dataView.getDepartment());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

            if (dataView.getUserRoleList() != null) {

                if (!dataView.getUserRoleList().isEmpty()) {

                    List<UserRoleViewModel> userRoleView = dataView.getUserRoleList();
                    List<UserRoleModel> userRoleList = mapFullListUserRole(userRoleView);

                    data.setUserRoleList(userRoleList);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    private List<UserRoleModel> mapFullListUserRole(List<UserRoleViewModel> dataViewList) {

        List<UserRoleModel> dataList = new ArrayList<UserRoleModel>();

        try {

            for (UserRoleViewModel dataView : dataViewList) {

                UserRoleModel data = new UserRoleModel();

                data.setRoleId(AppUtil.decryptId(dataView.getRoleId()));

                dataList.add(data);
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    
    public List<UserRoleViewModel> mapFullListUserInRole() {

        List<UserRoleViewModel> dataList = new ArrayList<UserRoleViewModel>();

        try {

            while (rs.next()) {

                UserRoleViewModel data = new UserRoleViewModel();

                data.setRoleId(AppUtil.encryptId(rs.getInt("ROLE_ID")));
                data.setRoleName(rs.getString("ROLE_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public UsersViewModel mapFullSSO() {

        UsersViewModel data = new UsersViewModel();

        try {

            while (rs.next()) {

                //data.setUserId(AppUtil.encryptId(rs.getInt("USER_TEMP_ID")));
                data.setUserCode(rs.getString("USER_CODE"));
                data.setUserName(rs.getString("USER_NAME"));
                data.setFullName(rs.getString("FULL_NAME"));
                data.setFirstName(rs.getString("FIRST_NAME"));
                data.setLastName(rs.getString("LAST_NAME"));
                
                data.setMobile(rs.getString("MOBILE"));
                data.setEmail(rs.getString("EMAIL"));
                data.setOffice(rs.getString("OFFICE"));
                data.setPartOf(rs.getString("PART_OF"));
                data.setDepartment(rs.getString("DEPARTMENT"));
                
                data.setPosition(rs.getString("POSITION"));
                data.setUserLevel(rs.getString("USER_LEVEL"));
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
