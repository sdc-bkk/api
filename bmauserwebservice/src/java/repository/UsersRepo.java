/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.UserRoleModel;
import model.UsersModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.UsersFilterModel;
import repository.mapper.UsersMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.UserRoleViewModel;
import viewModel.UsersViewModel;

/**
 *
 * @author Sirichai
 */
public class UsersRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<UsersViewModel>> getListAll(ResultPage page, UsersFilterModel filter) throws SQLException {

        ResultData<List<UsersViewModel>> resultData = new ResultData<List<UsersViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT U.*, TRIM(( TRIM(U.FIRST_NAME)||' '||TRIM(U.LAST_NAME) )) FULL_NAME "
                       + " FROM USERS U "
                       //+ "    LEFT JOIN USER_IN_ROLE UR ON U.USER_ID = UR.USER_ID "
                       //+ "    LEFT JOIN USER_ROLE R ON UR.ROLE_ID = R.ROLE_ID "
                       + " WHERE (? IS NULL OR U.IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getFullName())) {
                sql += " AND TRIM(( TRIM(U.FIRST_NAME)||' '||TRIM(U.LAST_NAME) )) LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getUserName())) {
                sql += " AND U.USER_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOffice())) {
                sql += " AND U.OFFICE LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getPartOf())) {
                sql += " AND U.PART_OF LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDepartment())) {
                sql += " AND U.DEPARTMENT LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getRoleId()) && AppUtil.decryptId(filter.getRoleId()) > 0) {
                sql += " AND USER_ID IN (SELECT USER_ID FROM USER_IN_ROLE WHERE ROLE_ID = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getRoleName())) {
                sql += " AND USER_ID IN (SELECT UR.USER_ID "
                     + "    FROM USER_IN_ROLE UR "
                     + "    LEFT JOIN USER_ROLE R ON UR.ROLE_ID = R.ROLE_ID "
                     + "    WHERE R.ROLE_NAME LIKE ?) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("userName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getFullName())) {
                ps.setString(i++, "%" + filter.getFullName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getUserName())) {
                ps.setString(i++, "%" + filter.getUserName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getOffice())) {
                ps.setString(i++, "%" + filter.getOffice() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getPartOf())) {
                ps.setString(i++, "%" + filter.getPartOf() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getDepartment())) {
                ps.setString(i++, "%" + filter.getDepartment() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getRoleId()) && AppUtil.decryptId(filter.getRoleId()) > 0) {
                ps.setInt(i++, AppUtil.decryptId(filter.getRoleId()));
            }

            if (!AppUtil.isNullAndSpace(filter.getRoleName())) {
                ps.setString(i++, "%" + filter.getRoleName() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<UsersViewModel> dataList = new UsersMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<UsersViewModel> dataList = new UsersMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " USER_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("userName")) {
                str = " USER_NAME ";
            } else if (orderBy.equalsIgnoreCase("userCode")) {
                str = " USER_CODE ";
            } else if (orderBy.equalsIgnoreCase("fullName")) {
                str = " TRIM(( TRIM(FIRST_NAME)||' '||TRIM(LAST_NAME) )) ";
            } else if (orderBy.equalsIgnoreCase("firstName")) {
                str = " FIRST_NAME ";
            } else if (orderBy.equalsIgnoreCase("lastName")) {
                str = " LAST_NAME ";
            } else if (orderBy.equalsIgnoreCase("office")) {
                str = " OFFICE ";
            } else if (orderBy.equalsIgnoreCase("partOf")) {
                str = " PART_OF ";
            } else if (orderBy.equalsIgnoreCase("department")) {
                str = " DEPARTMENT ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM USERS "
                        + " WHERE USER_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<UsersViewModel> getData(int id) throws SQLException {

        ResultData<UsersViewModel> resultData = new ResultData<UsersViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT U.*, TRIM(( TRIM(U.FIRST_NAME)||' '||TRIM(U.LAST_NAME) )) FULL_NAME "
                       + " FROM USERS U "
                       + " WHERE U.USER_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            UsersViewModel data = new UsersMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(UsersFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM USERS "
                        + " WHERE (? = 0 OR USER_ID <> ?) "
                        + " AND (USER_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getUserId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getUserId()));
            ps.setString(i++, filter.getUserName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(UsersModel users) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Users">
            if (users.getUserId() == 0) {

                sql = "INSERT INTO USERS ( "
                        + "  USER_CODE "
                        + ", USER_NAME "
                        + ", FIRST_NAME "
                        + ", LAST_NAME "
                        
                        + ", MOBILE "
                        + ", EMAIL "
                        + ", OFFICE "
                        + ", PART_OF "
                        + ", DEPARTMENT "
                        
                        + ", IS_ACTIVE "
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?, ?,?,?,?,?, ?,SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "USER_ID" });
                
            } else {

                sql = "UPDATE USERS SET "
                        + "  USER_CODE = ? "
                        + ", USER_NAME = ? "
                        + ", FIRST_NAME = ? "
                        + ", LAST_NAME = ? "
                        
                        + ", MOBILE = ? "
                        + ", EMAIL = ? "
                        + ", OFFICE = ? "
                        + ", PART_OF = ? "
                        + ", DEPARTMENT = ? "
                        
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE USER_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, users.getUserCode());
            ps.setString(i++, users.getUserName());
            ps.setString(i++, users.getFirstName());
            ps.setString(i++, users.getLastName());
            
            ps.setString(i++, users.getMobile());
            ps.setString(i++, users.getEmail());
            ps.setString(i++, users.getOffice());
            ps.setString(i++, users.getPartOf());
            ps.setString(i++, users.getDepartment());
            
            ps.setBoolean(i++, users.isActive());

            if (users.getUserId() == 0) {

                if (!AppUtil.isNullAndSpace(users.getUpdatedBy())) {
                    ps.setString(i++, users.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                users.setUserId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(users.getUpdatedBy())) {
                    ps.setString(i++, users.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, users.getUserId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="UserInRole">
            if (users.getUserRoleList() != null) {

                if (!users.getUserRoleList().isEmpty()) {
/*
                    //<editor-fold defaultstate="collapsed" desc="RoleId is use NOT DELETE">
                    String delId = "";
                    int size = users.getUserRoleList().size();
                    List empty = new ArrayList();

                    for (int j = 0; j < size; j++) {

                        UserRoleModel userRole = users.getUserRoleList().get(j);

                        if (!AppUtil.isNullAndZero(userRole.getRoleId())) {
                            empty.add(userRole.getRoleId());
                        }
                    }

                    delId = empty.isEmpty() ? "" : empty.toString().replace("[", "").replace("]", "");
                    //</editor-fold>
*/
                    //<editor-fold defaultstate="collapsed" desc="DELETE UserInRole by UserId is not use">
                    i = 1;

                    sql = " DELETE FROM USER_IN_ROLE WHERE USER_ID = ? ";
/*
                    if (!delId.equals("")) {
                        sql += " AND ROLE_ID NOT IN (" + delId + ") ";
                    }
*/
                    ps = conn.prepareStatement(sql);

                    ps.setInt(i++, users.getUserId());

                    ps.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Insert UserInRole">
                    for (UserRoleModel userRole : users.getUserRoleList()) {

                        if (!AppUtil.isNullAndZero(userRole.getRoleId())) {

                            //<editor-fold defaultstate="collapsed" desc="Insert UserInRole at RoleId">
                            i = 1;

                            sql = " INSERT INTO USER_IN_ROLE ( "
                                + "    USER_ID, ROLE_ID "
                                + " ) VALUES (?,?) ";

                            ps = conn.prepareStatement(sql);

                            ps.setInt(i++, users.getUserId());
                            ps.setInt(i++, userRole.getRoleId());

                            boolean resultInsert = ps.executeUpdate() != 0;
                            //</editor-fold>

                        }
                    }
                    //</editor-fold>

                } else {

                    //<editor-fold defaultstate="collapsed" desc="DELETE ALL UserInRole by UserId">
                    i = 1;

                    sql = " DELETE FROM USER_IN_ROLE WHERE USER_ID = ? ";

                    ps = conn.prepareStatement(sql);

                    ps.setInt(i++, users.getUserId());

                    ps.executeUpdate();
                    //</editor-fold>

                }
            }
            //</editor-fold>
            
            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, UsersFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            String sqlUserPermission = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete Users and UserInRole by id list">
            sql = " DELETE FROM USERS "
                + " WHERE USER_ID = ? ";
            sqlUserPermission = " DELETE FROM USER_IN_ROLE "
                              + " WHERE USER_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete UserInRole by id ">
                i = 1;
                ps.setSql(sqlUserPermission);

                ps.setInt(i, id); //UserId

                boolean resultDelete = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Delete Users by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id); //UserId

                boolean resultDeleteUsers = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();
            result = true;

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    
    public List<UserRoleViewModel> getListUserInRole(String userId) throws SQLException {

        List<UserRoleViewModel> resultData = new ArrayList<UserRoleViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT R.* "
                       + " FROM USER_IN_ROLE UR "
                       + "    LEFT JOIN USER_ROLE R ON UR.ROLE_ID = R.ROLE_ID "
                       + " WHERE UR.USER_ID = ? ";

            if(true){
            
            }
/*
 SELECT R.* 
 FROM USER_IN_ROLE UR 
    LEFT JOIN USER_ROLE R ON UR.ROLE_ID = R.ROLE_ID 
 WHERE UR.USER_ID = 1 
*/
            ps.setSql(sql);

            ps.setOrderBy(" R.ROLE_NAME ASC ");

            ps.setInt(i++, AppUtil.decryptId(userId)); //USER_ID

            rs = ps.executeQuery();

            resultData = new UsersMapper(rs).mapFullListUserInRole();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    
    public ResultData<UsersViewModel> getDataSSO(String userName) throws SQLException {

        ResultData<UsersViewModel> resultData = new ResultData<UsersViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT U.* , TRIM(( TRIM(U.FIRST_NAME)||' '||TRIM(U.LAST_NAME) )) FULL_NAME "
                       + " FROM USERS_TEMP U "
                       + " WHERE TRIM(U.USER_NAME) = ? ";

            ps.setSql(sql);

            ps.setString(1, userName.trim());

            rs = ps.executeQuery();

            UsersViewModel data = new UsersMapper(rs).mapFullSSO();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean hasDataByUserNamePassword(String userName, String password) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM USERS "
                    + " WHERE (USER_NAME LIKE ?) AND (PASSWORD LIKE ?) ";

            ps.setSql(sql);

            ps.setString(1, userName.trim());
            ps.setString(2, AppUtil.encrypt(password.trim()));

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }
    
    public ResultData<UsersViewModel> getData(UsersFilterModel filter) throws SQLException {

        ResultData<UsersViewModel> resultData = new ResultData<UsersViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {
            int i = 1;
            ps.setAutoCommit(false);

            String sql = " SELECT U.*, TRIM(( TRIM(U.FIRST_NAME)||' '||TRIM(U.LAST_NAME) )) FULL_NAME "
                    + " FROM USERS U "
                    + " WHERE 1=1 ";

            if (!AppUtil.isNullAndSpace(filter.getUserName())) {
                sql += " AND U.USER_NAME = ? ";
            }

            ps.setSql(sql);

            if (!AppUtil.isNullAndSpace(filter.getUserName())) {
                ps.setString(i++, filter.getUserName());
            }

            rs = ps.executeQuery();

            UsersViewModel data = new UsersMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }
}
