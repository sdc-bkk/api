/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author Sirichai
 */
public class UsersModel {

    private int userId;
    private String userCode; //รหัสพนักงาน
    private String userName; //ชื่อผู้ใช้งาน
    
    private String firstName; //ชื่อ
    private String lastName; //นามสกุล
    private String mobile; //เบอร์โทรศัพท์
    private String email; //อีเมล์
    private String office; //สำนัก
    private String partOf; //ส่วนงาน
    private String department; //ฝ่าย
    
    private boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private String createdBy; //ผู้ที่สร้าง
    private String createdDate; //วันที่สร้าง
    private String updatedBy; //ผู้ที่แก้ไขล่าสุด
    private String updatedDate; //วันที่แก้ไขล่าสุด

    //Relation
    private List<UserRoleModel> userRoleList;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getPartOf() {
        return partOf;
    }

    public void setPartOf(String partOf) {
        this.partOf = partOf;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public List<UserRoleModel> getUserRoleList() {
        return userRoleList;
    }

    public void setUserRoleList(List<UserRoleModel> userRoleList) {
        this.userRoleList = userRoleList;
    }

}
