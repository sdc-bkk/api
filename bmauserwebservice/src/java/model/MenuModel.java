/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class MenuModel {

    private int menuId;
    private String menuName; //ชื่อเมนู
    private String icon; //รูปสัญลักษณ์เมนู
    private String path; //พาธเมนู
    private int refMenuId; //รหัสอ้างอิงเมนู
    private MenuModel refMenu; //รหัสอ้างอิงเมนู
    private int levelMenu; //ระดับของเมนู
    private int orderMenu; //ลำดับของเมนู
    private boolean hasChild; //สถานะการมีเมนูย่อย(0=ไม่มี,1=มี)

    private boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private boolean canView; //สามารถเรียกดูข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private boolean canAdd; //สามารถเพิ่มข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private boolean canEdit; //สามารถแก้ไขข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private boolean canDelete; //สามารถลบข้อมูลได้หรือไม่(0=ไม่,1=ใช่)
    private boolean canApprove; //สามารถอนุมัติข้อมูลได้หรือไม่(0=ไม่,1=ใช่)

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getRefMenuId() {
        return refMenuId;
    }

    public void setRefMenuId(int refMenuId) {
        this.refMenuId = refMenuId;
    }

    public MenuModel getRefMenu() {
        return refMenu;
    }

    public void setRefMenu(MenuModel refMenu) {
        this.refMenu = refMenu;
    }

    public int getLevelMenu() {
        return levelMenu;
    }

    public void setLevelMenu(int levelMenu) {
        this.levelMenu = levelMenu;
    }

    public int getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(int orderMenu) {
        this.orderMenu = orderMenu;
    }

    public boolean isHasChild() {
        return hasChild;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isCanView() {
        return canView;
    }

    public void setCanView(boolean canView) {
        this.canView = canView;
    }

    public boolean isCanAdd() {
        return canAdd;
    }

    public void setCanAdd(boolean canAdd) {
        this.canAdd = canAdd;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean isCanApprove() {
        return canApprove;
    }

    public void setCanApprove(boolean canApprove) {
        this.canApprove = canApprove;
    }

}
