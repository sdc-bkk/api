/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.data.ResultData;
import model.filter.UsersFilterModel;
import service.UserPermissionService;
import service.UsersService;
import utility.MessageBundleUtil;
import viewModel.UserPermissionViewModel;
import viewModel.UsersViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("front")
public class FrontWebservice {

    @Context
    private UriInfo context;
    UsersService usersService = new UsersService();
    UserPermissionService userPermissionService = new UserPermissionService();

    /**
     * Creates a new instance of FrontWebservice
     */
    public FrontWebservice() {
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("authenUser")
    public String authenUser(UsersFilterModel filter) {

        try {

            ResultData<Boolean> data = usersService.authenUser(filter);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getMenuAndPermissionListByUser")
    public String getMenuAndPermissionListByUser(UsersFilterModel filter) {

        ResultData<List<UserPermissionViewModel>> resultData = new ResultData<List<UserPermissionViewModel>>();

        try {
            //get data user from username
            UsersViewModel data = usersService.getDataUser(filter);
            if (!data.getUserId().isEmpty()) {
                filter.setUserId(data.getUserId());
                resultData = userPermissionService.getMenuAndPermissionListByUserId(filter);
            }
            return new Gson().toJson(resultData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getProfileData")
    public String getProfileData(UsersFilterModel filter) {
        ResultData<UsersViewModel> resultData = new ResultData<>();
        try {
            UsersViewModel data = usersService.getDataUser(filter);
            if (!data.getUserId().isEmpty()) {
                resultData.setResult(data);
            }

            return new Gson().toJson(resultData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
