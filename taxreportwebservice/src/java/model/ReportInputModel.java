/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author User
 */
public class ReportInputModel {

    private JRBeanCollectionDataSource dataDataSource;

    public JRBeanCollectionDataSource getDataDataSource() {
        return dataDataSource;
    }

    public void setDataDataSource(JRBeanCollectionDataSource dataDataSource) {
        this.dataDataSource = dataDataSource;
    }

    public Map<String, Object> getDataSources() {
        Map<String, Object> dataSources = new HashMap<>();
        dataSources.put("dataDataSource", dataDataSource);

        return dataSources;
    }
}
