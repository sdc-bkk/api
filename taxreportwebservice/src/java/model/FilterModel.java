/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import enumeration.ReportEnum;
import java.util.List;
import utility.AppConfig;

/**
 *
 * @author User
 */
public class FilterModel {

    private List<String> officeCodeList; //สำนักงานเขต แบบ List 
    private String officeCode; //สถานะเอกสาร 1-01
    private String docStatus; //สถานะเอกสาร 1-01
    private String status; //สถานะ
    private String taxNo; //เลขประจำตัวผู้เสียภาษี
    private String ownerName; //ชื่อผู้ประกอบการ
    private String stationName; //ชื่อสถานการค้าปลีก
    private String startDate; //วันที่เริ่มต้น
    private String endDate; //วันที่สิ้นสุด
    private String businessStatus; //ความประสงค์
    private String docNo; //เลขทะเบียน
    private String officerName; //ชื่อเจ้าหน้าที่ตรวจสอบ
    private String month; //เดือน
    private String year; //ปี
    private String startMonth; //เดือน
    private String startYear; //ปี
    private String endMonth; //เดือน
    private String endYear;
    private String startMonthYear; //ปี
    private String endMonthYear; //เดือน
    private String businessType; //ประเภทกิจการ
    private String mobile; //ประเภทกิจการ
    private String username; //ชื่อผู้จัดพิมพ์
    private String reportType; //ประเภทรายงาน เช่น ประจำปี ประจำเดือน ประจำวัน
    private ReportEnum report;  
    private String receiptNo; //เลขใบเสร็จ

    private int page;
    private int itemPerPage = Integer.parseInt(new AppConfig().value("config.itemPerPage"));
    private String orderBy;
    private String sort;

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getStartMonthYear() {
        return startMonthYear;
    }

    public void setStartMonthYear(String startMonthYear) {
        this.startMonthYear = startMonthYear;
    }

    public String getEndMonthYear() {
        return endMonthYear;
    }

    public void setEndMonthYear(String endMonthYear) {
        this.endMonthYear = endMonthYear;
    }

    
    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }
    
    

    public List<String> getOfficeCodeList() {
        return officeCodeList;
    }

    public void setOfficeCodeList(List<String> officeCodeList) {
        this.officeCodeList = officeCodeList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItemPerPage() {
        return itemPerPage;
    }

    public void setItemPerPage(int itemPerPage) {
        this.itemPerPage = itemPerPage;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public ReportEnum getReport() {
        return report;
    }

    public void setReport(ReportEnum report) {
        this.report = report;
    }

    public String getDocStatus() {
        return docStatus;
    }

    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }
     public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

}
