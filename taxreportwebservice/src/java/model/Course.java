/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Course {
       private String name;
   private String location;
   private String location2;
   private String qty1;
   private String qty2;
   public String getName() {
       return name;
   }
   public void setName(String name) {
       this.name = name;
   }
   public String getLocation() {
       return location;
   }
   public void setLocation(String location) {
       this.location = location;
   }

    public String getLocation2() {
        return location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    public String getQty1() {
        return qty1;
    }

    public void setQty1(String qty1) {
        this.qty1 = qty1;
    }

    public String getQty2() {
        return qty2;
    }

    public void setQty2(String qty2) {
        this.qty2 = qty2;
    }
   
}
