/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class DetailModel {    
    private String monthName; 
    private String totalAmt; //รวม
    private String gasAmt; //จำนวนลิตรก๊าซ
    private String oilAmt; //จำนวนลิตรน้ำมัน
    private String LPGAmt; //จำนวนลิตร LPG
    private String NGVAmt; //จำนวนลิตร NGV
    
    private String name1;
    private String name2;
    private String name3;
    private String name4;
    private String name5;
    private String name6;
    private String name7;
    private String name8;
    private String name9;
    private String name10;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getGasAmt() {
        return gasAmt;
    }

    public void setGasAmt(String gasAmt) {
        this.gasAmt = gasAmt;
    }

    public String getOilAmt() {
        return oilAmt;
    }

    public void setOilAmt(String oilAmt) {
        this.oilAmt = oilAmt;
    }

    public String getLPGAmt() {
        return LPGAmt;
    }

    public void setLPGAmt(String LPGAmt) {
        this.LPGAmt = LPGAmt;
    }

    public String getNGVAmt() {
        return NGVAmt;
    }

    public void setNGVAmt(String NGVAmt) {
        this.NGVAmt = NGVAmt;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public String getName5() {
        return name5;
    }

    public void setName5(String name5) {
        this.name5 = name5;
    }

    public String getName6() {
        return name6;
    }

    public void setName6(String name6) {
        this.name6 = name6;
    }

    public String getName7() {
        return name7;
    }

    public void setName7(String name7) {
        this.name7 = name7;
    }

    public String getName8() {
        return name8;
    }

    public void setName8(String name8) {
        this.name8 = name8;
    }

    public String getName9() {
        return name9;
    }

    public void setName9(String name9) {
        this.name9 = name9;
    }

    public String getName10() {
        return name10;
    }

    public void setName10(String name10) {
        this.name10 = name10;
    }
    
    
}
