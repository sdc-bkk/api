/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author User
 */
public class DataModel {

    private String officeCode; //สำนักงานเขต
    private String officeName; //สำนักงานเขต
    private String docNo; //เลขทะเบียน
    private String actionDate; //วันที่ลงทะเบียน / วันที่ / วันที่ยื่นแบบ / วันที่ชำระ / วันที่ออกหนังสือ
    private String taxNo; //เลขประจำตัวผู้เสียภาษี
    private String ownerName; //ชื่อผู้ประกอบการ
    private List<String> stationNameList; //ชื่อสถานการค้าปลีก แบบ List
    private String childList; //ชื่อสถานการค้าปลีก
    private int stationNameId; //รหัสสถานการค้าปลีก
    private String stationName; //ชื่อสถานการค้าปลีก
    private String status; //สถานะ
    private String statusName; //สถานะ
    private String statusDoc; //สถานะการยื่นแบบ
    private String businessStatus; //ความประสงค์
    private String monthYear; //เดือน ปี
    private String stationAmt;//จำนวน (ราย)
    private String taxAmt; //ยอดเงินภาษี
    private double taxAmtDbl; //ยอดเงินภาษี
    private String extraAmt; //เงินเพิ่ม
    private double extraAmtDbl; //เงินเพิ่ม
    private String totalAmt; //รวม
    private double totalAmtDbl; //รวม
    private String delay; //ยื่นแบบล่าช้า (วัน)
    private String mobile; //เบอร์โทร
    private String officerName; //ชื่อเจ้าหน้าที่ตรวจสอบ
    private String addressRetail; //address form retail
    private String addressStation; //address form station
    private String businessType;
    private String year;
    private String houseNo;
    
    private String month1; 
    private String month2; 
    private String month3; 
    private String month4; 
    private String month5; 
    private String month6; 
    private String month7; 
    private String month8; 
    private String month9; 
    private String month10; 
    private String month11; 
    private String month12; 
    
    private String remark; 
    private String actionDateStr;
    
    private String oilAmt; //จำนวนลิตรน้ำมัน
    private double oilAmtDbl; //จำนวนลิตรน้ำมัน
    private String LPGAmt; //จำนวนลิตร LPG
    private double LPGAmtDbl; //จำนวนลิตร LPG
    private String NGVAmt; //จำนวนลิตร NGV
    private double NGVAmtDbl; //จำนวนลิตร NGV
    
    private String qty;
    private double qtyDbl;
    private String receiptNo;
    private String paymentMethod;
    private String paymentOffice;

    private String houseNoRetail;
    private String soiRetail;
    private String roadRetail;
    private String tambonRetail;
    private String amphurRetail;
    private String provinceRetail;
    
    private String branchName;
    private String houseNoStation;
    private String soiStation;
    private String roadStation;
    private String tambonStation;
    private String amphurStation;
    private String provinceStation;
    
    private String qty1;
    private String qty2;
    private String qty3;
    private String qty4;
    private String qty5;
    private String qty6;
    private String qty7;
    private String qty8;
    private String qty9;
    private String qty10;
    private String qty11;
    private String qty12;
    
    private String totalAmt1;
    private String totalAmt2;
    private String totalAmt3;
    private String totalAmt4;
    private String totalAmt5;
    private String totalAmt6;
    private String totalAmt7;
    private String totalAmt8;
    private String totalAmt9;
    private String totalAmt10;
    private String totalAmt11;
    private String totalAmt12;
    
   private List<DetailModel> detailList = new ArrayList<>();
   private JRBeanCollectionDataSource detailDataSource;

    public double getExtraAmtDbl() {
        return extraAmtDbl;
    }

    public void setExtraAmtDbl(double extraAmtDbl) {
        this.extraAmtDbl = extraAmtDbl;
    }

    public double getTotalAmtDbl() {
        return totalAmtDbl;
    }

    public void setTotalAmtDbl(double totalAmtDbl) {
        this.totalAmtDbl = totalAmtDbl;
    }

    public double getOilAmtDbl() {
        return oilAmtDbl;
    }

    public void setOilAmtDbl(double oilAmtDbl) {
        this.oilAmtDbl = oilAmtDbl;
    }

    public double getLPGAmtDbl() {
        return LPGAmtDbl;
    }

    public void setLPGAmtDbl(double LPGAmtDbl) {
        this.LPGAmtDbl = LPGAmtDbl;
    }

    public double getNGVAmtDbl() {
        return NGVAmtDbl;
    }

    public void setNGVAmtDbl(double NGVAmtDbl) {
        this.NGVAmtDbl = NGVAmtDbl;
    }

    public double getQtyDbl() {
        return qtyDbl;
    }

    public void setQtyDbl(double qtyDbl) {
        this.qtyDbl = qtyDbl;
    }

    public double getTaxAmtDbl() {
        return taxAmtDbl;
    }

    public void setTaxAmtDbl(double taxAmtDbl) {
        this.taxAmtDbl = taxAmtDbl;
    }

    public int getStationNameId() {
        return stationNameId;
    }

    public void setStationNameId(int stationNameId) {
        this.stationNameId = stationNameId;
    }
    
    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
    
    public String getOilAmt() {
        return oilAmt;
    }

    public void setOilAmt(String oilAmt) {
        this.oilAmt = oilAmt;
    }

    public String getLPGAmt() {
        return LPGAmt;
    }

    public void setLPGAmt(String LPGAmt) {
        this.LPGAmt = LPGAmt;
    }

    public String getNGVAmt() {
        return NGVAmt;
    }

    public void setNGVAmt(String NGVAmt) {
        this.NGVAmt = NGVAmt;
    }

    public String getActionDateStr() {
        return actionDateStr;
    }

    public void setActionDateStr(String actionDateStr) {
        this.actionDateStr = actionDateStr;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getChildList() {
        return childList;
    }

    public void setChildList(String childList) {
        this.childList = childList;
    }
    
    public String getMonth1() {
        return month1;
    }

    public void setMonth1(String month1) {
        this.month1 = month1;
    }

    public String getMonth2() {
        return month2;
    }

    public void setMonth2(String month2) {
        this.month2 = month2;
    }

    public String getMonth3() {
        return month3;
    }

    public void setMonth3(String month3) {
        this.month3 = month3;
    }

    public String getMonth4() {
        return month4;
    }

    public void setMonth4(String month4) {
        this.month4 = month4;
    }

    public String getMonth5() {
        return month5;
    }

    public void setMonth5(String month5) {
        this.month5 = month5;
    }

    public String getMonth6() {
        return month6;
    }

    public void setMonth6(String month6) {
        this.month6 = month6;
    }

    public String getMonth7() {
        return month7;
    }

    public void setMonth7(String month7) {
        this.month7 = month7;
    }

    public String getMonth8() {
        return month8;
    }

    public void setMonth8(String month8) {
        this.month8 = month8;
    }

    public String getMonth9() {
        return month9;
    }

    public void setMonth9(String month9) {
        this.month9 = month9;
    }

    public String getMonth10() {
        return month10;
    }

    public void setMonth10(String month10) {
        this.month10 = month10;
    }

    public String getMonth11() {
        return month11;
    }

    public void setMonth11(String month11) {
        this.month11 = month11;
    }

    public String getMonth12() {
        return month12;
    }

    public void setMonth12(String month12) {
        this.month12 = month12;
    }

    
    public String getStationAmt() {
        return stationAmt;
    }

    public void setStationAmt(String stationAmt) {
        this.stationAmt = stationAmt;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public List<String> getStationNameList() {
        return stationNameList;
    }

    public void setStationNameList(List<String> stationNameList) {
        this.stationNameList = stationNameList;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDoc() {
        return statusDoc;
    }

    public void setStatusDoc(String statusDoc) {
        this.statusDoc = statusDoc;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getMonthYear() {
        return monthYear;
    }

    public void setMonthYear(String monthYear) {
        this.monthYear = monthYear;
    }

    public String getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(String taxAmt) {
        this.taxAmt = taxAmt;
    }

    public String getExtraAmt() {
        return extraAmt;
    }

    public void setExtraAmt(String extraAmt) {
        this.extraAmt = extraAmt;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getDelay() {
        return delay;
    }

    public void setDelay(String delay) {
        this.delay = delay;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public void setAddressRetail(String addressRetail) {
        this.addressRetail = addressRetail;
    }

    public String getAddressRetail() {
        return addressRetail;
    }

    public void setAddressStation(String addressStation) {
        this.addressStation = addressStation;
    }

    public String getAddressStation() {
        return addressStation;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentOffice() {
        return paymentOffice;
    }

    public void setPaymentOffice(String paymentOffice) {
        this.paymentOffice = paymentOffice;
    }

    
    
    public String getHouseNoRetail() {
        return houseNoRetail;
    }

    public void setHouseNoRetail(String houseNoRetail) {
        this.houseNoRetail = houseNoRetail;
    }

    public String getSoiRetail() {
        return soiRetail;
    }

    public void setSoiRetail(String soiRetail) {
        this.soiRetail = soiRetail;
    }

    public String getRoadRetail() {
        return roadRetail;
    }

    public void setRoadRetail(String roadRetail) {
        this.roadRetail = roadRetail;
    }

    public String getTambonRetail() {
        return tambonRetail;
    }

    public void setTambonRetail(String tambonRetail) {
        this.tambonRetail = tambonRetail;
    }

    public String getAmphurRetail() {
        return amphurRetail;
    }

    public void setAmphurRetail(String amphurRetail) {
        this.amphurRetail = amphurRetail;
    }

    public String getProvinceRetail() {
        return provinceRetail;
    }

    public void setProvinceRetail(String provinceRetail) {
        this.provinceRetail = provinceRetail;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getHouseNoStation() {
        return houseNoStation;
    }

    public void setHouseNoStation(String houseNoStation) {
        this.houseNoStation = houseNoStation;
    }

    public String getSoiStation() {
        return soiStation;
    }

    public void setSoiStation(String soiStation) {
        this.soiStation = soiStation;
    }

    public String getRoadStation() {
        return roadStation;
    }

    public void setRoadStation(String roadStation) {
        this.roadStation = roadStation;
    }

    public String getTambonStation() {
        return tambonStation;
    }

    public void setTambonStation(String tambonStation) {
        this.tambonStation = tambonStation;
    }

    public String getAmphurStation() {
        return amphurStation;
    }

    public void setAmphurStation(String amphurStation) {
        this.amphurStation = amphurStation;
    }

    public String getProvinceStation() {
        return provinceStation;
    }

    public void setProvinceStation(String provinceStation) {
        this.provinceStation = provinceStation;
    }

    
    
    
    
    public String getQty1() {
        return qty1;
    }

    public void setQty1(String qty1) {
        this.qty1 = qty1;
    }

    public String getQty2() {
        return qty2;
    }

    public void setQty2(String qty2) {
        this.qty2 = qty2;
    }

    public String getQty3() {
        return qty3;
    }

    public void setQty3(String qty3) {
        this.qty3 = qty3;
    }

    public String getQty4() {
        return qty4;
    }

    public void setQty4(String qty4) {
        this.qty4 = qty4;
    }

    public String getQty5() {
        return qty5;
    }

    public void setQty5(String qty5) {
        this.qty5 = qty5;
    }

    public String getQty6() {
        return qty6;
    }

    public void setQty6(String qty6) {
        this.qty6 = qty6;
    }

    public String getQty7() {
        return qty7;
    }

    public void setQty7(String qty7) {
        this.qty7 = qty7;
    }

    public String getQty8() {
        return qty8;
    }

    public void setQty8(String qty8) {
        this.qty8 = qty8;
    }

    public String getQty9() {
        return qty9;
    }

    public void setQty9(String qty9) {
        this.qty9 = qty9;
    }

    public String getQty10() {
        return qty10;
    }

    public void setQty10(String qty10) {
        this.qty10 = qty10;
    }

    public String getQty11() {
        return qty11;
    }

    public void setQty11(String qty11) {
        this.qty11 = qty11;
    }

    public String getQty12() {
        return qty12;
    }

    public void setQty12(String qty12) {
        this.qty12 = qty12;
    }

    public String getTotalAmt1() {
        return totalAmt1;
    }

    public void setTotalAmt1(String totalAmt1) {
        this.totalAmt1 = totalAmt1;
    }

    public String getTotalAmt2() {
        return totalAmt2;
    }

    public void setTotalAmt2(String totalAmt2) {
        this.totalAmt2 = totalAmt2;
    }

    public String getTotalAmt3() {
        return totalAmt3;
    }

    public void setTotalAmt3(String totalAmt3) {
        this.totalAmt3 = totalAmt3;
    }

    public String getTotalAmt4() {
        return totalAmt4;
    }

    public void setTotalAmt4(String totalAmt4) {
        this.totalAmt4 = totalAmt4;
    }

    public String getTotalAmt5() {
        return totalAmt5;
    }

    public void setTotalAmt5(String totalAmt5) {
        this.totalAmt5 = totalAmt5;
    }

    public String getTotalAmt6() {
        return totalAmt6;
    }

    public void setTotalAmt6(String totalAmt6) {
        this.totalAmt6 = totalAmt6;
    }

    public String getTotalAmt7() {
        return totalAmt7;
    }

    public void setTotalAmt7(String totalAmt7) {
        this.totalAmt7 = totalAmt7;
    }

    public String getTotalAmt8() {
        return totalAmt8;
    }

    public void setTotalAmt8(String totalAmt8) {
        this.totalAmt8 = totalAmt8;
    }

    public String getTotalAmt9() {
        return totalAmt9;
    }

    public void setTotalAmt9(String totalAmt9) {
        this.totalAmt9 = totalAmt9;
    }

    public String getTotalAmt10() {
        return totalAmt10;
    }

    public void setTotalAmt10(String totalAmt10) {
        this.totalAmt10 = totalAmt10;
    }

    public String getTotalAmt11() {
        return totalAmt11;
    }

    public void setTotalAmt11(String totalAmt11) {
        this.totalAmt11 = totalAmt11;
    }

    public String getTotalAmt12() {
        return totalAmt12;
    }

    public void setTotalAmt12(String totalAmt12) {
        this.totalAmt12 = totalAmt12;
    }

    public List<DetailModel> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<DetailModel> detailList) {
        this.detailList = detailList;
    }
    
   public JRBeanCollectionDataSource getDetailDataSource() {
       return new JRBeanCollectionDataSource(detailList,false);
   }
    
}
