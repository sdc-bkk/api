package model;

import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author User
 */
public class StudentReportInput {

    private String reportTitle;
    private String instituteName;
    private JRBeanCollectionDataSource studentDataSource;

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public Map<String, Object> getParameters() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("P_INSTITUTE_NAME", getInstituteName());

        return parameters;
    }

    public JRBeanCollectionDataSource getStudentDataSource() {
        return studentDataSource;
    }

    public void setStudentDataSource(JRBeanCollectionDataSource studentDataSource) {
        this.studentDataSource = studentDataSource;
    }

    public Map<String, Object> getDataSources() {
        Map<String, Object> dataSources = new HashMap<>();
        dataSources.put("studentDataSource", studentDataSource);

        return dataSources;
    }
}
