/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class StudentReportDataAssembler {

    public static StudentReportInput assemble() {
        StudentReportInput studentReportInput = new StudentReportInput();
        studentReportInput.setReportTitle("Student Report");
        studentReportInput.setInstituteName("My Institute");

        List<Student> students = new ArrayList<>();
        //Add Student1
        Student student1 = new Student();
        student1.setName("Mark");
        student1.setEmail("mark1234@gmail.com");
        List<Course> student1Courses = new ArrayList<>();

        Course course1Student1 = new Course();
        course1Student1.setName("History");
        course1Student1.setLocation("L1");
        student1Courses.add(course1Student1);
        student1.setCourseList(student1Courses);
        students.add(student1);

        //Add Student2
        Student student2 = new Student();
        student2.setName("Mai");
        student2.setEmail("masmai9@gmail.com");
        List<Course> student2Courses = new ArrayList<>();

        Course course1Student2 = new Course();
        course1Student2.setName("History Of ...");
        course1Student2.setLocation("L2");
        student2Courses.add(course1Student2);
        student2.setCourseList(student2Courses);
        students.add(student2);
        
        
        //Add Student2
        Student student3 = new Student();
        student3.setName("sssss");
        student3.setEmail("sssss@gmail.com");
        List<Course> student3Courses = new ArrayList<>();

        Course course1Student3 = new Course();
        course1Student3.setName("aaaaa");
        course1Student3.setLocation("L3");
        student3Courses.add(course1Student3);
        student3.setCourseList(student3Courses);
        students.add(student3);
        
        JRBeanCollectionDataSource studentDataSource = new JRBeanCollectionDataSource(students, false);
        studentReportInput.setStudentDataSource(studentDataSource);

        return studentReportInput;
    }
}
