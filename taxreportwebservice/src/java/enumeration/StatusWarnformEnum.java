/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author WeAble
 */
public enum StatusWarnformEnum {

    Create(0, "ออกหนังสือ", "Create form."),
    Send(1, "ส่งอีเมล", "Send email."),
    Read(2, "ตอบรับ", "Read email."),
    Cancel(9, "ยกเลิกหนังสือ", "Cancel form.");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    StatusWarnformEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static StatusWarnformEnum valueOf(int value) throws IllegalArgumentException {
        for (StatusWarnformEnum newsResourceType : StatusWarnformEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<StatusWarnformEnum> list() {

//        List<StatusEnum> list = new ArrayList<StatusEnum>();
        List<StatusWarnformEnum> list = Arrays.asList(StatusWarnformEnum.values());

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (StatusWarnformEnum newsResourceType : StatusWarnformEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (StatusWarnformEnum newsResourceType : StatusWarnformEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
