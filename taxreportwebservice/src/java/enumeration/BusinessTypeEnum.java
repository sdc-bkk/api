/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author WeAble
 */
public enum BusinessTypeEnum {

    Oil(0, "น้ำมัน", "Oil."),
    Gas(1, "ก๊าซปิโตรเลียม", "Gas."),
    OilAndGas(2, "น้ำมันและก๊าซปิโตรเลียม", "OilAndGas.");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    BusinessTypeEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static BusinessTypeEnum valueOf(int value) throws IllegalArgumentException {
        for (BusinessTypeEnum newsResourceType : BusinessTypeEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<BusinessTypeEnum> list() {

//        List<StatusEnum> list = new ArrayList<StatusEnum>();
        List<BusinessTypeEnum> list = Arrays.asList(BusinessTypeEnum.values());

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (BusinessTypeEnum newsResourceType : BusinessTypeEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (BusinessTypeEnum newsResourceType : BusinessTypeEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
