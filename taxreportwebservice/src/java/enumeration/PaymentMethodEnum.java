/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author WeAble
 */
public enum PaymentMethodEnum {

    Cash(1, "เงินสด", "Create form."),
    Credit(2, "บัตรเครดิต", "Send email."),
    Debit(3, "บัตรเดบิต", "Read email."),
    Order(4, "ธนาณัติ", "Cancel form."),
    Cheque(5, "เช็ค", "Cancel form."),
    EDc(6, "QR Code ผ่านเครื่อง EDC", "Cancel form."),
    Internet(7, "Internet Banking", "Cancel form.");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    PaymentMethodEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static PaymentMethodEnum valueOf(int value) throws IllegalArgumentException {
        for (PaymentMethodEnum newsResourceType : PaymentMethodEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<PaymentMethodEnum> list() {

//        List<StatusEnum> list = new ArrayList<StatusEnum>();
        List<PaymentMethodEnum> list = Arrays.asList(PaymentMethodEnum.values());

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (PaymentMethodEnum newsResourceType : PaymentMethodEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (PaymentMethodEnum newsResourceType : PaymentMethodEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
