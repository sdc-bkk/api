/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sirichai
 */
public enum ReportEnum {

    //ข้อความของการตั้งค่าทั่วไปของระบบ
    rpt101_RegisterRetail("101_register_retail", "รายงานผลการลงทะเบียนผู้เสียภาษีน้ำมันประจำวันตามหน่วยงาน", "view_101_register_retail","RPT1-01"),
    rpt102_Retail("102_retail", "รายงานผู้ประกอบการแยกตามรายใหม่/รายเก่า/ยกเลิกประจำวัน", "view_102_retail","RPT1-02"),
    rpt103_ChangeData("103_change_data", "รายงานแบบแจ้งการเลิกกิจการหรือเปลี่ยนแปลงข้อมูลสถานการค้าปลีกประจำวัน ตามหน่วยงาน", "view_103_change_data","RPT1-03"),
    rpt104_01ByOrg("104_01_by_org", "รายงานการยื่นแบบประจำวันตามหน่วยงาน", "view_104_01_by_org","RPT1-04"),
    rpt105_01ByOfficer("105_01_by_officer", "รายงานการยื่นแบบประจำวันตามพนักงานเจ้าหน้าที่", "view_105_01_by_officer","RPT1-05"),
    rpt106_03ByOrg("106_03_by_org", "รายงานการชำระภาษีประจำวันตามหน่วยงาน", "view_106_03_by_org","RPT1-06"),
    rpt201_Nosend03ByOrg("201_nosend_03_by_org", "รายงานการค้างยื่นแบบประจำเดือน ตามหน่วยงาน", "view_201_nosend_03_by_org","RPT2-01"),
    rpt202_Nopay03ByOrg("202_nopay_03_by_org", "รายงานรายการค้างชำระภาษีประจำเดือน ตามหน่วยงาน", "view_202_nopay_03_by_org","RPT2-02"),
    rpt203_Nopay03ByRetail("203_nopay_03_by_retail", "รายงานรายการค้างชำระภาษีประจำเดือน ตามผู้ประกอบการ", "view_203_nopay_03_by_retail","RPT2-03"),
    rpt204_Nosend03Monthly("204_nosend_03_mothly", "รายงานรายการเตือนค้างยื่นแบบประจำเดือน", "view_204_nosend_03_monthly","RPT2-04"),
    rpt205_Nopay03Monthly("205_nopay_03_mothly", "รายงานรายการเตือนค้างชำระภาษีประจำเดือน", "view_205_nopay_03_monthly","RPT2-05"),
    rpt206_Summary03ByOrg("206_summary_03_by_org", "รายงานสรุปจำนวนรายการที่ยื่นแบบและยอดภาษีประจำเดือน ตามหน่วยงาน", "view_206_summary_03_by_org","RPT2-06"),
    rpt207_SummaryNosend03ByOrg("207_summary_nosend_03_by_org", "รายงานสรุปจำนวนรายที่ค้างยื่นแบบและยอดภาษีประจำเดือน ตามหน่วยงาน", "view_207_summary_nosend_03_by_org","RPT2-07"),
    rpt208_SummaryNopay03ByOrg("208_summary_nopay_03_by_org", "รายงานสรุปจำนวนรายการที่ี่ค้างชำระภาษีประจำเดือน ตามหน่วยงาน", "view_208_summary_nopay_03_by_org","RPT2-08"),
    rpt209_SummaryPay03ByOilType("209_summary_pay_03_by_oiltype", "รายงานสรุปยอดชำระภาษีรายสถานประกอบการประจำเดือนตามชนิดน้ำมัน/ก๊าซ", "view_209_summary_pay_03_by_oiltype","RPT2-09"),
    rpt210_StatusRetailByOrg("210_status_retail_by_org", "รายงานสถานะของสถานการค้าปลีกประจำเดือน ตามหน่วยงาน", "view_210_status_retail_by_org","RPT2-10"),
    rpt301_01ByOrgYearly("301_01_by_org", "ทะเบียนผู้เสียภาษีน้ำมัน ประจำปีงบประมาณตามหน่วยงาน (ท.1)", "view_301_01_by_org","RPT3-01"),
//    rpt301_01ByOrgYearlyNew("301_01_by_org_new", "ทะเบียนผู้เสียภาษีน้ำมัน ประจำปีงบประมาณตามหน่วยงาน (ท.1)", "view_301_01_by_org","RPT3-01"),
    rpt302_Pay03Yearly("302_pay_03_yearly", "รายงานยอดชำระภาษีประจำปีงบประมาณ ตามหน่วยงาน", "view_302_pay_03_yearly","RPT3-02"),
    rpt303_StationYearly("303_station_yearly", "รายงานสถานการค้าปลีกตามยอดชำระภาษีที่กำหนดจำแนกตามปีงบประมาณ", "view_303_station_yearly","RPT3-03"),
    rpt304_Pay03StationYearly("304_pay_03_station_yearly", "รายงานยอดชำระภาษีสูงสุดของสถานการค้าปลีก จำแนกตามปีงบประมาณ", "view_304_pay_03_station_yearly","RPT3-04"),
    rpt305_Pay03OfficeYearly("305_pay_03_office_yearly", "รายงานยอดชำระภาษีสูงสุดของสำนักงานเขต จำแนกตามปีงบประมาณ", "view_305_pay_03_office_yearly","RPT3-05"),
    rpt401_Qty03("401_qty_03", "สถิติปริมาณการจัดเก็บภาษีน้ำมัน ฯ", "view_401_qty_03","RPT4-01"),
//    rpt401_Qty03New("401_qty_03_new", "สถิติปริมาณการจัดเก็บภาษีน้ำมัน ฯ", "view_401_qty_03","RPT4-01"),
    rpt402_Pay03("402_pay_03", "สถิติจำนวนเงินภาษีและปริมาณน้ำมัน/ก๊าซที่จำหน่ายแยกตามสถานประกอบการค้าปลีก", "view_402_pay_03","RPT4-02"),
    rpt501_RetailDetail("501_retail_detail", "รายงานรายละเอียดผู้ประกอบการ ตามหน่วยงาน", "view_501_retail_detail","RPT5-01"),
    rpt502_StationDetail("502_station_detail", "รายงานรายละเอียดสถานการค้าปลีก ตามหน่วยงาน", "view_502_station_detail","RPT5-02"),
    rpt503_DocDetail("503_doc_detail", "รายงานการขอเอกสารประกอบการลงทะเบียนผู้เสียภาษีน้ำมัน (ภน.01) เพิ่มเติม", "view_503_doc_detail","RPT5-03"),
    rpt504_PayOther("504_pay_other", "รายงานการชำระภาษีจากหน่วยงานอื่น", "view_504_pay_other","RPT5-04"),
    rpt505_PayHistory("505_pay_history", "รายงานประวัติการชำระภาษี", "view_505_pay_history","RPT5-05");

    private final String reportFile;
    private final String reportName;
    private final String viewName;
    private final String reportCode;

    ReportEnum(String reportFile, String reportName, String viewName, String reportCode) {
        this.reportFile = reportFile;
        this.reportName = reportName;
        this.viewName = viewName;
        this.reportCode = reportCode;
    }

    public String reportFile() {
        return reportFile;
    }

    public String reportName() {
        return reportName;
    }

    public String viewName() {
        return viewName;
    }

    public String reportCode() {
        return reportCode;
    }

    public static ReportEnum valueOfString(String value) throws IllegalArgumentException {
        for (ReportEnum settingGeneral : ReportEnum.values()) {
            if (settingGeneral.reportFile.equalsIgnoreCase(value.trim())) {
                return settingGeneral;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<ReportEnum> list() {

        List<ReportEnum> list = new ArrayList<ReportEnum>();

        return list;
    }

}
