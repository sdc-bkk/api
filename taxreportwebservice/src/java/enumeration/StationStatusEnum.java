/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author WeAble
 */
public enum StationStatusEnum {

    New(1, "รายใหม่", "New station."),
    Old(2, "รายเก่า", "Old station."),
    Cancel(3, "เลิกกิจการ", "Cancel station.");

    private final int value;

    private final String displayNameTH;
    private final String displayNameEN;

    StationStatusEnum(int value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public int value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static StationStatusEnum valueOf(int value) throws IllegalArgumentException {
        for (StationStatusEnum newsResourceType : StationStatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<StationStatusEnum> list() {

//        List<StatusEnum> list = new ArrayList<StatusEnum>();
        List<StationStatusEnum> list = Arrays.asList(StationStatusEnum.values());

        return list;
    }
    
    public static String getDisplayNameTH(int value) throws IllegalArgumentException {
        for (StationStatusEnum newsResourceType : StationStatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameTH;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public static String getDisplayNameEN (int value) throws IllegalArgumentException {
        for (StationStatusEnum newsResourceType : StationStatusEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayNameEN;
            }
        }
        throw new IllegalArgumentException();
    }
}
