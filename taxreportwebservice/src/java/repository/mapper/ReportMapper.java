/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.DocStatusEnum;
import enumeration.PaymentMethodEnum;
import enumeration.StationStatusEnum;
import enumeration.StatusWarnformEnum;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import model.DataModel;
import utility.AppUtil;

/**
 *
 * @author User
 */
public class ReportMapper {

    private ResultSet rs = null;

    public ReportMapper() {
    }

    public ReportMapper(ResultSet rs) {
        this.rs = rs;
    }

    //Daily
    //<editor-fold defaultstate="collapsed" desc="101 map Register">
    public List<DataModel> mapDataRegisterRetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setStatusName(DocStatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
                //station name
                String stationName = AppUtil.checkNullData(rs.getString("STATION_NAME_LIST"));
                List<String> stationList = new ArrayList<>();
                String[] array = stationName.split("\\|", -1);
                stationList = Arrays.asList(array);
                data.setStationNameList(stationList);
                data.setChildList(stationName.replace("|", ", "));
                dataList.add(data);
            }
        } catch (IllegalArgumentException | SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="102 map Retail">
    public List<DataModel> mapDataDailyRetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setStatusName(StationStatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
//                if (rs.getString("CANCEL_DATE") != null) {
//                    data.setStatusName("เลิกกิจการ");
//                } else {
//                    LocalDate date = LocalDate.now().minusDays(30);
//                    Date date2 = rs.getDate("ACTION_DATE");
//                    SimpleDateFormat objSDF = new SimpleDateFormat("dd-MM-yyyy");
//                    //Change LocalDate To Date Type
//                    ZoneId defaultZoneId = ZoneId.systemDefault();
//                    Date date3 = Date.from(date.atStartOfDay(defaultZoneId).toInstant());
//                    //Format Date dd-mm-yyyy And Change To Date Type
//                    Date dt_1 = objSDF.parse(objSDF.format(date3));
//                    Date dt_2 = objSDF.parse(objSDF.format(date2));
//                    //Check Date Less 30 Days
//                    if (dt_1.compareTo(dt_2) > 0) {
//                        data.setStatusName("รายเก่า");
//                        //System.out.println("Old Customer");
//                    } else if (dt_1.compareTo(dt_2) < 0) {
//                        data.setStatusName("รายใหม่");
//                        //System.out.println("New Customer");
//                    } else if (dt_1.compareTo(dt_2) == 0) {
//                        data.setStatusName("รายใหม่");
//                        //System.out.println("New Customer");
//                    }
//                }
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="103 map ChangeData">
    public List<DataModel> mapDataChangeDataRetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBusinessStatus(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_NAME")));
                data.setStatusName(DocStatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="104 map 01ByOrgRetail">
    public List<DataModel> mapData01ByOrgRetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setStatusDoc(AppUtil.checkNullData(rs.getString("STATUS_DOC")));
                data.setStatusName(DocStatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="105 map 01ByOfficerRetail">
    public List<DataModel> mapData01ByOfficerRetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficerName(AppUtil.checkNullData(rs.getString("OFFICER_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setStatusName(DocStatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="106 map 03ByOrgRetail">
    public List<DataModel> mapData03ByOrgRetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_TOTAL"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_MONEY"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(String.valueOf(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY")))));
                data.setTaxAmtDbl(rs.getDouble("TAX_TOTAL"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_MONEY"));
                data.setTotalAmtDbl(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY"));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //Monthly
    //<editor-fold defaultstate="collapsed" desc="201 mapDataNoSend03ByOrg">
    public List<DataModel> mapDataNoSend03ByOrg() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="202 mapDataNoPay03ByOrg">
    public List<DataModel> mapDataNoPay03ByOrg() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_TOTAL"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_MONEY"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(String.valueOf(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY")))));
                data.setTaxAmtDbl(rs.getDouble("TAX_TOTAL"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_MONEY"));
                data.setTotalAmtDbl(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY"));
                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="203 mapDataNoPay03ByRetail">
    public List<DataModel> mapDataNoPay03ByRetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_TOTAL"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_MONEY"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(String.valueOf(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY")))));
                data.setTaxAmtDbl(rs.getDouble("TAX_TOTAL"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_MONEY"));
                data.setTotalAmtDbl(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY"));
                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="204 mapDataNosend03Monthly">
    public List<DataModel> mapDataNosend03Monthly() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setStatusName(StatusWarnformEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="205 mapDataNosend03Monthly">
    public List<DataModel> mapDataNopay03Monthly() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("AMOUNT"))));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setStatusName(StatusWarnformEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="206 mapDataSummary03ByOrg">
    public List<DataModel> mapDataSummary03ByOrg() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                
                data.setStationAmt(AppUtil.checkNullData(rs.getString("STATION_AMT")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_TOTAL"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_MONEY"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(String.valueOf(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY")))));
                data.setTaxAmtDbl(rs.getDouble("TAX_TOTAL"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_MONEY"));
                data.setTotalAmtDbl(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY"));
                                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="207 mapDataSummaryNosend03ByOrg">
    public List<DataModel> mapDataSummaryNosend03ByOrg() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setYear(rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                
                data.setMonth1(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH1"))));
                data.setMonth2(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH2"))));
                data.setMonth3(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH3"))));
                data.setMonth4(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH4"))));
                data.setMonth5(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH5"))));
                data.setMonth6(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH6"))));
                data.setMonth7(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH7"))));
                data.setMonth8(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH8"))));
                data.setMonth9(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH9"))));
                data.setMonth10(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH10"))));
                data.setMonth11(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH11"))));
                data.setMonth12(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH12"))));
                int total =0;
                total = rs.getInt("MONTH1") + rs.getInt("MONTH2")+rs.getInt("MONTH3") + rs.getInt("MONTH4")+rs.getInt("MONTH5") + rs.getInt("MONTH6")+rs.getInt("MONTH7") + rs.getInt("MONTH8")+rs.getInt("MONTH9") + rs.getInt("MONTH10")+rs.getInt("MONTH11") + rs.getInt("MONTH12");
                data.setTotalAmt(AppUtil.numberFormatCommaNoDigit(String.valueOf(total)));
                                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="208 mapDataSummaryNopay03ByOrg">
    public List<DataModel> mapDataSummaryNopay03ByOrg() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setYear(rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                
                data.setMonth1(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH1"))));
                data.setMonth2(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH2"))));
                data.setMonth3(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH3"))));
                data.setMonth4(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH4"))));
                data.setMonth5(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH5"))));
                data.setMonth6(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH6"))));
                data.setMonth7(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH7"))));
                data.setMonth8(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH8"))));
                data.setMonth9(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH9"))));
                data.setMonth10(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH10"))));
                data.setMonth11(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH11"))));
                data.setMonth12(AppUtil.checkNullDataZeroToDash(AppUtil.numberFormatCommaNoDigit(rs.getString("MONTH12"))));
                int total =0;
                total = rs.getInt("MONTH1") + rs.getInt("MONTH2")+rs.getInt("MONTH3") + rs.getInt("MONTH4")+rs.getInt("MONTH5") + rs.getInt("MONTH6")+rs.getInt("MONTH7") + rs.getInt("MONTH8")+rs.getInt("MONTH9") + rs.getInt("MONTH10")+rs.getInt("MONTH11") + rs.getInt("MONTH12");
                data.setTotalAmt(AppUtil.numberFormatCommaNoDigit(String.valueOf(total)));
                                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="209 mapDataSummaryPay03ByOilType">
    public List<DataModel> mapDataSummaryPay03ByOilType() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setYear(rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_TOTAL"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_MONEY"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(String.valueOf(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY")))));
                                
                data.setOilAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setNGVAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("NGV_AMT"))));
                data.setLPGAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("LPG_AMT"))));
                                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //Year
    //<editor-fold defaultstate="collapsed" desc="301 map 01ByOrgYearly">
    public List<DataModel> mapData01ByOrgYearly() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setAddressRetail(AppUtil.checkNullData(rs.getString("ADDRESS_RETAIL")));
                data.setBusinessType(AppUtil.checkNullData(rs.getString("BUSINESS_TYPE")));
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddressStation(AppUtil.checkNullData(rs.getString("ADDRESS_STATION")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setStatusName(rs.getString("MAIN_STATUS"));
                } else {
                    data.setStatusName("ปกติ");
                }

                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="301 map 01ByOrgYearly">
    public List<DataModel> mapData01ByOrgYearlyNew() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setYear(AppUtil.checkNullData(rs.getString("YEAR")));
                
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                
                data.setAddressRetail(AppUtil.checkNullData(rs.getString("ADDRESS_RETAIL")));
                data.setHouseNoRetail(AppUtil.checkNullData(rs.getString("RE_ADDRESS")));
                data.setSoiRetail(AppUtil.checkNullData(rs.getString("RE_SOI")));
                data.setRoadRetail(AppUtil.checkNullData(rs.getString("RE_ROAD")));
                data.setTambonRetail(AppUtil.checkNullData(rs.getString("RE_TAMBON")));
                data.setAmphurRetail(AppUtil.checkNullData(rs.getString("RE_AMPHUR")));
                data.setProvinceRetail(AppUtil.checkNullData(rs.getString("RE_PROVINCE")));
                
                data.setBranchName("");
                data.setBusinessType(AppUtil.checkNullData(rs.getString("BUSINESS_TYPE")));
                
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddressStation(AppUtil.checkNullData(rs.getString("ADDRESS_STATION")));
                data.setHouseNoStation(AppUtil.checkNullData(rs.getString("STA_ADDRESS")));
                data.setSoiStation(AppUtil.checkNullData(rs.getString("STA_SOI")));
                data.setRoadStation(AppUtil.checkNullData(rs.getString("STA_ROAD")));
                data.setTambonStation(AppUtil.checkNullData(rs.getString("STA_TAMBON")));
                data.setAmphurStation(AppUtil.checkNullData(rs.getString("STA_AMPHUR")));
                data.setProvinceStation(AppUtil.checkNullData(rs.getString("STA_PROVINCE")));
                
//                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setStatusName(rs.getString("MAIN_STATUS"));
                } else {
                    data.setStatusName("ปกติ");
                }
                data.setRemark("");

                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="302 map Pay03Yearly">
    public List<DataModel> mapDataPay03Yearly() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setYear(AppUtil.checkNullData(rs.getString("YEAR")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_AMT"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_AMT"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTaxAmtDbl(rs.getDouble("TAX_AMT"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_AMT"));
                data.setTotalAmtDbl(rs.getDouble("TOTAL_AMT"));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="303 map StationYearly">
    public List<DataModel> mapDataStationYearly() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setYear(AppUtil.checkNullData(rs.getString("YEAR")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_AMT"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_AMT"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTaxAmtDbl(rs.getDouble("TAX_AMT"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_AMT"));
                data.setTotalAmtDbl(rs.getDouble("TOTAL_AMT"));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="304 map Pay03StationYearly">
    public List<DataModel> mapDataPay03StationYearly() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setYear(AppUtil.checkNullData(rs.getString("YEAR")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_AMT"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_AMT"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTaxAmtDbl(rs.getDouble("TAX_AMT"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_AMT"));
                data.setTotalAmtDbl(rs.getDouble("TOTAL_AMT"));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="305 map Pay03OfficeYearly">
    public List<DataModel> mapDataPay03OfficeYearly() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setYear(AppUtil.checkNullData(rs.getString("YEAR")));
                data.setTaxAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TAX_AMT"))));
                data.setExtraAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("EXTRA_AMT"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTaxAmtDbl(rs.getDouble("TAX_AMT"));
                data.setExtraAmtDbl(rs.getDouble("EXTRA_AMT"));
                data.setTotalAmtDbl(rs.getDouble("TOTAL_AMT"));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //Manual
    //<editor-fold defaultstate="collapsed" desc="401 map Register">
    public List<DataModel> mapDataQty03() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setYear(AppUtil.checkNullData(rs.getString("YEAR")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBusinessType(AppUtil.checkNullData(rs.getString("BUSINESS_LIST")));
                data.setQty(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setQtyDbl(rs.getDouble("OIL_AMT"));
                data.setTotalAmtDbl(rs.getDouble("TOTAL_AMT"));
                dataList.add(data);
            }
        } catch (IllegalArgumentException | SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="401 map Register">
    public List<DataModel> mapDataQty03New() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setAddressStation(AppUtil.checkNullData(rs.getString("ADDRESS_STATION")));
                data.setBusinessType(AppUtil.checkNullData(rs.getString("BUSINESS_LIST")));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                //data.setYear(AppUtil.checkNullData(rs.getString("YEAR")));
                //data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                //data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                
                data.setQty1(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty2(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty3(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty4(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty5(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty6(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty7(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty8(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty9(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty10(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty11(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setQty12(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                
                data.setTotalAmt1(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt2(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt3(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt4(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt5(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt6(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt7(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt8(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt9(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt10(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt11(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setTotalAmt12(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                
                dataList.add(data);
            }
        } catch (IllegalArgumentException | SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="402 mapDataPay03">
    public List<DataModel> mapDataPay03() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setYear(rs.getString("YEAR"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                
                data.setOilAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setNGVAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("NGV_AMT"))));
                data.setLPGAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("LPG_AMT"))));
                                
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
    
    //Other
    //<editor-fold defaultstate="collapsed" desc="501 map Pay03OfficeYearly">
    public List<DataModel> mapDataRetailDetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setActionDateStr(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setAddressRetail(AppUtil.checkNullData(rs.getString("ADDRESS_RETAIL")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="502 map Pay03OfficeYearly">
    public List<DataModel> mapDataStationDetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setBusinessType(AppUtil.checkNullData(rs.getString("BUSINESS_TYPE")));
                data.setAddressRetail(AppUtil.checkNullData(rs.getString("ADDRESS_RETAIL")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                dataList.add(data);
            }
        } catch (SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="503 mapDataDocDetail">
    public List<DataModel> mapDataDocDetail() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setRemark(AppUtil.checkNullData(rs.getString("REMARK")));
                dataList.add(data);
            }
        } catch (IllegalArgumentException | SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="504 mapDataPayOther">
    public List<DataModel> mapDataPayOther() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setReceiptNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setPaymentOffice(AppUtil.checkNullData(rs.getString("PAYMENT_AT")));
                
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setPaymentMethod(PaymentMethodEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
                
                dataList.add(data);
            }
        } catch (IllegalArgumentException | SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="505 mapDataPayHistory">
    public List<DataModel> mapDataPayHistory() {
        List<DataModel> dataList = new ArrayList<>();
        try {
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setMonthYear(AppUtil.getMonthTH(rs.getInt("MONTH")) + " " + rs.getString("YEAR"));
                data.setActionDate(AppUtil.checkNullData(rs.getDate("ACTION_DATE")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                data.setReceiptNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setPaymentOffice(AppUtil.checkNullData(rs.getString("PAYMENT_AT")));
                
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setPaymentMethod(PaymentMethodEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
                
                dataList.add(data);
            }
        } catch (IllegalArgumentException | SQLException e) {
        }
        return dataList;
    }
    //</editor-fold>
}
