/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.SelectListModel;
import model.data.ResultData;
import model.data.ResultPage;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;

/**
 *
 * @author User
 */
public class SelectListRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<SelectListModel>> getOfficeList() throws SQLException {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            List<SelectListModel> dataList = new ArrayList<SelectListModel>();
            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT DISTINCT DISTRICT_ID ,DISTRICT_THAI FROM M_TAMBON "
                    + " WHERE (IS_ACTIVE = 1) AND PROVINCE_ID = 10 ORDER BY DISTRICT_THAI ";

            ps.setSql(sql);

            rs = ps.executeQuery();

            while (rs.next()) {

                SelectListModel data = new SelectListModel();

                data.setId(rs.getString("DISTRICT_ID"));
                data.setName(rs.getString("DISTRICT_THAI"));

                dataList.add(data);
            }
            resultData.setResult(dataList);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<SelectListModel>> getBusinessStatusList() throws SQLException {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            List<SelectListModel> dataList = new ArrayList<SelectListModel>();
            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_BUSINESS_STATUS WHERE IS_ACTIVE = 1 ORDER BY business_status_name ";

            ps.setSql(sql);

            rs = ps.executeQuery();

            while (rs.next()) {

                SelectListModel data = new SelectListModel();

                data.setId(rs.getString("business_status_id"));
                data.setName(rs.getString("business_status_name"));

                dataList.add(data);
            }
            resultData.setResult(dataList);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<SelectListModel>> getBusinessTypeList() throws SQLException {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            List<SelectListModel> dataList = new ArrayList<SelectListModel>();
            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_BUSINESS_TYPE  WHERE IS_ACTIVE = 1 ORDER BY business_type_id";

            ps.setSql(sql);

            rs = ps.executeQuery();

            while (rs.next()) {

                SelectListModel data = new SelectListModel();

                data.setId(rs.getString("business_type_id"));
                data.setName(rs.getString("business_type_name"));

                dataList.add(data);
            }
            resultData.setResult(dataList);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }
}
