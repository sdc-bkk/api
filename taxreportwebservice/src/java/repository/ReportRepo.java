/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.DataModel;
import model.DetailModel;
import model.FilterModel;
import model.Student;
import model.data.ResultData;
import model.data.ResultPage;
import repository.mapper.ReportMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;

/**
 *
 * @author User
 */
public class ReportRepo {
    
    private final ConnnectionDB connDB = new ConnnectionDB();

    //get SQL from reportRepo
    //<editor-fold defaultstate="collapsed" desc="getData">
    public ResultData<List<DataModel>> getReportList(ResultPage page, FilterModel filter) throws SQLException {
        ResultData<List<DataModel>> resultData = new ResultData<>();
        List<DataModel> resultList = new ArrayList();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            ps.setAutoCommit(false);
            int i = 1;
            String viewName = filter.getReport().viewName();
            String sql = "SELECT * FROM " + viewName
                    + " WHERE (1=1) ";

            //<editor-fold defaultstate="collapsed" desc="filter">
            //search office
            if (filter.getOfficeCodeList() != null) {
                if (!filter.getOfficeCodeList().isEmpty()) {
                    String sqlOffice = " AND ( ";
                    int j = 1;
                    for (String s : filter.getOfficeCodeList()) {
                        if (!AppUtil.isNullAndSpace(s)) {
                            if (j != 1) {
                                sqlOffice += "  OR  ";
                            }
                            sqlOffice += " (OFFICE_CODE = " + s + ") ";
                        }
                        j++;
                    }
                    sqlOffice += " ) ";
                    sql += sqlOffice;
                }
            }
            //name
            //search owner name
            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                sql += " AND OWNER_NAME LIKE ? ";
            }
            //search station name
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND STATION_NAME LIKE ? ";
            }
            //search officer name
            if (!AppUtil.isNullAndSpace(filter.getOfficerName())) {
                sql += " AND OFFICER_NAME LIKE ? ";
            }
            //number
            //search tax number
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND TAX_NO LIKE ? ";
            }
            //search docNumber
            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND DOC_NO LIKE ? ";
            }
            //search mobile
            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                sql += " AND MOBILE LIKE ? ";
            }
            //search mobile
            if (!AppUtil.isNullAndSpace(filter.getReceiptNo())) {
                sql += " AND DOC_NO LIKE ? ";
            }
            //date
            //search start date
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(ACTION_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            //search last date
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(ACTION_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            //search  month
            if (!AppUtil.isNullAndSpace(filter.getMonth())) {
                sql += " AND MONTH = ? ";
            }
            //search  year 
            if (!AppUtil.isNullAndSpace(filter.getYear())) {
                sql += " AND YEAR =  ? ";
            }
            //search start year
            if (!AppUtil.isNullAndSpace(filter.getStartYear())) {
                sql += " AND YEAR >= ?  ";
            }
            //search last year
            if (!AppUtil.isNullAndSpace(filter.getEndYear())) {
                sql += " AND YEAR <= ? ";
            }
            //search start year
            if (!AppUtil.isNullAndSpace(filter.getStartMonthYear())) {
                sql += " AND MONTHYEAR >= ?  ";
            }
            //search last year
            if (!AppUtil.isNullAndSpace(filter.getEndMonthYear())) {
                sql += " AND MONTHYEAR <= ? ";
            }
            //status            
            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                sql += " AND MAIN_STATUS = ? ";
            }            
            //search status
            if (!AppUtil.isNullAndSpace(filter.getDocStatus())) {
                sql += " AND MAIN_STATUS = ? ";
            }
            //search businessStatus
            if (!AppUtil.isNullAndSpace(filter.getBusinessStatus())) {
                sql += " AND BUSINESS_STATUS_NAME LIKE ? ";
            }
            //search businessType
            if (!AppUtil.isNullAndSpace(filter.getBusinessType())) {
                sql += " AND BUSINESS_TYPE LIKE ? ";
            }

            //</editor-fold>
            ps.setSql(sql);

            //<editor-fold defaultstate="collapsed" desc="get orderBy">
            String orderBy = "";
            switch (filter.getReport()) {
                case rpt101_RegisterRetail:
                    orderBy = " ACTION_DATE DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt102_Retail:
                    orderBy = " ACTION_DATE DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt103_ChangeData:
                    orderBy = " ACTION_DATE DESC,OFFICE_NAME,STATION_NAME ";
                    break;
                case rpt104_01ByOrg:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,STATION_NAME  ";
                    break;
                case rpt105_01ByOfficer:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OFFICER_NAME ";
                    break;
                case rpt106_03ByOrg:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,STATION_NAME ";
                    break;
                case rpt201_Nosend03ByOrg:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt202_Nopay03ByOrg:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt203_Nopay03ByRetail:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt204_Nosend03Monthly:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt205_Nopay03Monthly:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt206_Summary03ByOrg:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME ";
                    break;
                case rpt207_SummaryNosend03ByOrg:
                    orderBy = " OFFICE_NAME ";
                    break;
                case rpt208_SummaryNopay03ByOrg:
                    orderBy = " OFFICE_NAME ";
                    break;
                case rpt209_SummaryPay03ByOilType:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME ";
                    break;
                case rpt301_01ByOrgYearly:
                    orderBy = " OWNER_NAME,BUSINESS_TYPE,MAIN_STATUS ";
                    break;
//                case rpt301_01ByOrgYearlyNew:
//                    orderBy = " null ";
//                    break;
                case rpt302_Pay03Yearly:
                    orderBy = " YEAR DESC,OFFICE_NAME ";
                    break;
                case rpt303_StationYearly:
                    orderBy = " YEAR DESC,OFFICE_NAME,STATION_NAME  ";
                    break;
                case rpt304_Pay03StationYearly:
                    orderBy = " YEAR DESC,OFFICE_NAME,STATION_NAME  ";
                    break;
                case rpt305_Pay03OfficeYearly:
                    orderBy = " YEAR DESC ,OFFICE_NAME ";
                    break;
                case rpt401_Qty03:
                    orderBy = " YEAR DESC,OWNER_NAME ";
                    break;
//                case rpt401_Qty03New:
//                    orderBy = " null ";
//                    break;
                case rpt402_Pay03:
                    orderBy = " YEAR DESC,OFFICE_NAME,STATION_NAME  ";
                    break;
                case rpt501_RetailDetail:
                    orderBy = " ACTION_DATE,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt502_StationDetail:
                    orderBy = " OFFICE_NAME, BUSINESS_TYPE,OWNER_NAME ";
                    break;
                case rpt503_DocDetail:
                    orderBy = " ACTION_DATE,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt504_PayOther:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                case rpt505_PayHistory:
                    orderBy = " YEAR DESC, MONTH DESC,OFFICE_NAME,OWNER_NAME ";
                    break;
                default:
                    break;
            }
            ps.setOrderBy(orderBy);
            //</editor-fold>

            if (page != null) {
                ps.setResultPage(page);
            }

            //<editor-fold defaultstate="collapsed" desc="filter data">   
            //name
            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                ps.setString(i++, "%" + filter.getOwnerName() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getOfficerName())) {
                ps.setString(i++, "%" + filter.getOfficerName() + "%");
            }
            //number
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, "%" + filter.getTaxNo() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, "%" + filter.getDocNo() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                ps.setString(i++, "%" + filter.getMobile() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getReceiptNo())) {
                ps.setString(i++, "%" + filter.getReceiptNo() + "%");
            }
            //date
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }
            if (!AppUtil.isNullAndSpace(filter.getYear())) {
                ps.setString(i++, filter.getYear());
            }
            if (!AppUtil.isNullAndSpace(filter.getMonth())) {
                ps.setString(i++, filter.getMonth());
            }
            if (!AppUtil.isNullAndSpace(filter.getStartYear())) {
                int year = Integer.parseInt(filter.getStartYear());
//                if (year > 2550) {
//                    year = year - 543;
//                }
                ps.setInt(i++, year);
            }
            if (!AppUtil.isNullAndSpace(filter.getEndYear())) {
                int year = Integer.parseInt(filter.getEndYear());
//                if (year > 2550) {
//                    year = year - 543;
//                }
                ps.setInt(i++, year);
            }

            //search start year
            if (!AppUtil.isNullAndSpace(filter.getStartMonthYear())) {
                ps.setInt(i++, Integer.parseInt(filter.getStartMonthYear()));
            }
            //search last year
            if (!AppUtil.isNullAndSpace(filter.getEndMonthYear())) {
                ps.setInt(i++, Integer.parseInt(filter.getEndMonthYear()));
            }
            //status
            if (!AppUtil.isNullAndSpace(filter.getStatus())) {
                ps.setString(i++, filter.getStatus());
            }
            if (!AppUtil.isNullAndSpace(filter.getDocStatus())) {
                ps.setString(i++, filter.getDocStatus());
            }
            if (!AppUtil.isNullAndSpace(filter.getBusinessStatus())) {
                ps.setString(i++, filter.getBusinessStatus());
            }
            if (!AppUtil.isNullAndSpace(filter.getBusinessType())) {
                ps.setString(i++, filter.getBusinessType());
            }
            //</editor-fold>

            rs = ps.executeQuery();

            //<editor-fold defaultstate="collapsed" desc="case getMapper">
            if (null != filter.getReport()) {
                switch (filter.getReport()) {
                    case rpt101_RegisterRetail:
                        resultList = new ReportMapper(rs).mapDataRegisterRetail();
                        break;
                    case rpt102_Retail:
                        resultList = new ReportMapper(rs).mapDataDailyRetail();
                        break;
                    case rpt103_ChangeData:
                        resultList = new ReportMapper(rs).mapDataChangeDataRetail();
                        break;
                    case rpt104_01ByOrg:
                        resultList = new ReportMapper(rs).mapData01ByOrgRetail();
                        break;
                    case rpt105_01ByOfficer:
                        resultList = new ReportMapper(rs).mapData01ByOfficerRetail();
                        break;
                    case rpt106_03ByOrg:
                        resultList = new ReportMapper(rs).mapData03ByOrgRetail();
                        break;
                    case rpt201_Nosend03ByOrg:
                        resultList = new ReportMapper(rs).mapDataNoSend03ByOrg();
                        break;
                    case rpt202_Nopay03ByOrg:
                        resultList = new ReportMapper(rs).mapDataNoPay03ByOrg();
                        break;
                    case rpt203_Nopay03ByRetail:
                        resultList = new ReportMapper(rs).mapDataNoPay03ByRetail();
                        break;
                    case rpt204_Nosend03Monthly:
                        resultList = new ReportMapper(rs).mapDataNosend03Monthly();
                        break;
                    case rpt205_Nopay03Monthly:
                        resultList = new ReportMapper(rs).mapDataNopay03Monthly();
                        break;
                    case rpt206_Summary03ByOrg:
                        resultList = new ReportMapper(rs).mapDataSummary03ByOrg();
                        break;
                    case rpt207_SummaryNosend03ByOrg:
                        resultList = new ReportMapper(rs).mapDataSummaryNosend03ByOrg();
                        break;
                    case rpt208_SummaryNopay03ByOrg:
                        resultList = new ReportMapper(rs).mapDataSummaryNopay03ByOrg();
                        break;
                    case rpt209_SummaryPay03ByOilType:
                        resultList = new ReportMapper(rs).mapDataSummaryPay03ByOilType();
                        break;
                    case rpt301_01ByOrgYearly:
                        resultList = new ReportMapper(rs).mapData01ByOrgYearly();
                        break;
//                    case rpt301_01ByOrgYearlyNew:
//                        resultList = new ReportMapper(rs).mapData01ByOrgYearlyNew();
//                        break;
                    case rpt302_Pay03Yearly:
                        resultList = new ReportMapper(rs).mapDataPay03Yearly();
                        break;
                    case rpt303_StationYearly:
                        resultList = new ReportMapper(rs).mapDataStationYearly();
                        break;
                    case rpt304_Pay03StationYearly:
                        resultList = new ReportMapper(rs).mapDataPay03StationYearly();
                        break;
                    case rpt305_Pay03OfficeYearly:
                        resultList = new ReportMapper(rs).mapDataPay03OfficeYearly();
                        break;
                    case rpt401_Qty03:
                        resultList = new ReportMapper(rs).mapDataQty03();
                        break;
//                    case rpt401_Qty03New:
//                        resultList = new ReportMapper(rs).mapDataQty03New();
//                        break;
                    case rpt402_Pay03:
                        resultList = new ReportMapper(rs).mapDataPay03();
                        break;
                    case rpt501_RetailDetail:
                        resultList = new ReportMapper(rs).mapDataRetailDetail();
                        break;
                    case rpt502_StationDetail:
                        resultList = new ReportMapper(rs).mapDataStationDetail();
                        break;
                    case rpt503_DocDetail:
                        resultList = new ReportMapper(rs).mapDataDocDetail();
                        break;
                    case rpt504_PayOther:
                        resultList = new ReportMapper(rs).mapDataPayOther();
                        break;
                    case rpt505_PayHistory:
                        resultList = new ReportMapper(rs).mapDataPayHistory();
                        break;
                    default:
                        break;
                }
            }
            //</editor-fold>

            resultData.setResult(resultList);
            
            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }
            
        } catch (Exception e) {
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return resultData;
        
    }
    //</editor-fold>

    //orderBy
    //<editor-fold defaultstate="collapsed" desc="orderBy">
    private String getColumnDB(String orderBy) {
        String str = " OFFICE_NAME ";

        //<editor-fold defaultstate="collapsed" desc="case orderBy">
        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("officeName")) {
                str = " OFFICE_NAME ";
            } else if (orderBy.equalsIgnoreCase("officeCode")) {
                str = " OFFICE_CODE ";
            } else if (orderBy.equalsIgnoreCase("docNo")) {
                str = " DOC_NO ";
            } else if (orderBy.equalsIgnoreCase("actionDate")) {
                str = " ACTION_DATE ";
            } else if (orderBy.equalsIgnoreCase("taxNo")) {
                str = " TAX_NO ";
            } else if (orderBy.equalsIgnoreCase("ownerName")) {
                str = " OWNER_NAME ";
            } else if (orderBy.equalsIgnoreCase("status")) {
                str = " MAIN_STATUS ";
            } else if (orderBy.equalsIgnoreCase("businessStatus")) {
                str = " BUSINESS_STATUS_NAME ";
            } else if (orderBy.equalsIgnoreCase("stationName")) {
                str = " STAION_NAME ";
            } else if (orderBy.equalsIgnoreCase("month")) {
                str = " MONTH  ";
            } else if (orderBy.equalsIgnoreCase("year")) {
                str = " YEAR  ";
            } else if (orderBy.equalsIgnoreCase("yearly")) {
                str = " YEARLY  ";
            } else if (orderBy.equalsIgnoreCase("mobile")) {
                str = " MOBILE  ";
            } else if (orderBy.equalsIgnoreCase("businessType")) {
                str = " BUSINESS_TYPE  ";
            }

//            else if (orderBy.equalsIgnoreCase("updatedDate")) {
//                str = " (CASE WHEN OFFICE.UPDATED_DATE IS NULL THEN OFFICE.CREATED_DATE ELSE OFFICE.UPDATED_DATE END) ";
//            }
        }
        //</editor-fold>

        return str;
    }
    //</editor-fold>

    public List<DataModel> getRpt402_Pay03(FilterModel filter) throws SQLException {
        List<DataModel> resultList = new ArrayList();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            ps.setAutoCommit(false);
            int i = 1;
            String viewName = filter.getReport().viewName();
            String sql = "SELECT * FROM " + viewName
                    + " WHERE (1=1) ";

            //<editor-fold defaultstate="collapsed" desc="filter">
            //search office
            if (filter.getOfficeCodeList() != null) {
                if (!filter.getOfficeCodeList().isEmpty()) {
                    String sqlOffice = " AND ( ";
                    int j = 1;
                    for (String s : filter.getOfficeCodeList()) {
                        if (!AppUtil.isNullAndSpace(s)) {
                            if (j != 1) {
                                sqlOffice += "  OR  ";
                            }
                            sqlOffice += " (OFFICE_CODE = " + s + ") ";
                        }
                        j++;
                    }
                    sqlOffice += " ) ";
                    sql += sqlOffice;
                }
            }
            //name
            //search owner name
            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                sql += " AND OWNER_NAME LIKE ? ";
            }
            //search station name
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND STAION_NAME LIKE ? ";
            }
            //search officer name
            if (!AppUtil.isNullAndSpace(filter.getOfficerName())) {
                sql += " AND OFFICE_NAME LIKE ? ";
            }
            //number
            //search tax number
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND TAX_NO LIKE ? ";
            }
            //search docNumber
            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                sql += " AND DOC_NO LIKE ? ";
            }
            //search mobile
            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                sql += " AND MOBILE LIKE ? ";
            }
            //date
            //search start date
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(ACTION_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            //search last date
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(ACTION_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            //search  month
            if (!AppUtil.isNullAndSpace(filter.getMonth())) {
                sql += " AND MONTH = ? ";
            }
            //search  year 
            if (!AppUtil.isNullAndSpace(filter.getYear())) {
                sql += " AND YEAR =  ? ";
            }
            //search start year
            if (!AppUtil.isNullAndSpace(filter.getStartYear())) {
                sql += " AND  YEARLY <= ?  ";
            }
            //search last year
            if (!AppUtil.isNullAndSpace(filter.getEndYear())) {
                sql += " AND YEARLY >= ? ";
            }
            //search start year
            if (!AppUtil.isNullAndSpace(filter.getStartMonthYear())) {
                sql += " AND  MONTHYEAR >= ?  ";
            }
            //search last year
            if (!AppUtil.isNullAndSpace(filter.getEndMonthYear())) {
                sql += " AND MONTHYEAR <= ? ";
            }
            //status
            //search status
            if (!AppUtil.isNullAndSpace(filter.getDocStatus())) {
                sql += " AND MAIN_STATUS = ? ";
            }
            //search businessStatus
            if (!AppUtil.isNullAndSpace(filter.getBusinessStatus())) {
                sql += " AND BUSINESS_STATUS_NAME LIKE ? ";
            }
            //search businessType
            if (!AppUtil.isNullAndSpace(filter.getBusinessType())) {
                sql += " AND BUSINESS_TYPE LIKE ? ";
            }

            //</editor-fold>
            ps.setSql(sql);
            
            ps.setOrderBy("1");

            //<editor-fold defaultstate="collapsed" desc="filter data">   
            //name
            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                ps.setString(i++, "%" + filter.getOwnerName() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getOfficerName())) {
                ps.setString(i++, filter.getOfficerName());
            }
            //number
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, "%" + filter.getTaxNo() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getDocNo())) {
                ps.setString(i++, "%" + filter.getDocNo() + "%");
            }
            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                ps.setString(i++, "%" + filter.getMobile() + "%");
            }
            //date
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }
            if (!AppUtil.isNullAndSpace(filter.getYear())) {
                ps.setString(i++, filter.getYear());
            }
            if (!AppUtil.isNullAndSpace(filter.getMonth())) {
                ps.setString(i++, filter.getMonth());
            }
            if (!AppUtil.isNullAndSpace(filter.getStartYear())) {
                int year = Integer.parseInt(filter.getStartYear());
                if (year > 2550) {
                    year = year - 543;
                }
                ps.setInt(i++, year);
            }
            if (!AppUtil.isNullAndSpace(filter.getEndYear())) {
                int year = Integer.parseInt(filter.getEndYear());
                if (year > 2550) {
                    year = year - 543;
                }
                ps.setInt(i++, year);
            }

            //search start year
            if (!AppUtil.isNullAndSpace(filter.getStartMonthYear())) {
                ps.setInt(i++, Integer.parseInt(filter.getStartMonthYear()));
            }
            //search last year
            if (!AppUtil.isNullAndSpace(filter.getEndMonthYear())) {
                ps.setInt(i++, Integer.parseInt(filter.getEndMonthYear()));
            }
            //status
            if (!AppUtil.isNullAndSpace(filter.getDocStatus())) {
                ps.setString(i++, filter.getDocStatus());
            }
            if (!AppUtil.isNullAndSpace(filter.getBusinessStatus())) {
                ps.setString(i++, filter.getBusinessStatus());
            }
            if (!AppUtil.isNullAndSpace(filter.getBusinessType())) {
                ps.setString(i++, filter.getBusinessType());
            }
            //</editor-fold>

            rs = ps.executeQuery();
            
            while (rs.next()) {
                DataModel data = new DataModel();
                data.setYear(rs.getString("YEAR_BUDGET"));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setTotalAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("TOTAL_AMT"))));
                
                data.setOilAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("OIL_AMT"))));
                data.setNGVAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("NGV_AMT"))));
                data.setLPGAmt(AppUtil.checkNullData(AppUtil.numberFormatComma(rs.getString("LPG_AMT"))));
                
                data.setDetailList(_getChildRpt402_Pay03(conn, rs.getInt("RETAIL_STATION_ID"), rs.getString("YEAR_BUDGET")));
                resultList.add(data);
            }
            
        } catch (Exception e) {
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return resultList;
        
    }
    
    public List<DetailModel> _getChildRpt402_Pay03(Connection conn, int stationId, String yearly) throws SQLException {
        List<DetailModel> resultList = new ArrayList();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            ps.setAutoCommit(false);
            int i = 1;
            String sql = "SELECT * FROM RETAIL_STATION WHERE RETAIL_ID = " + stationId;
            
            ps.setSql(sql);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                DetailModel data = new DetailModel();
//                data.setName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
//                data.setLocation(AppUtil.checkNullData(rs.getString("POSTCODE")));
                resultList.add(data);
            }
            
        } catch (Exception e) {
        } finally {
            ps.resultSetClose(rs);
        }
        return resultList;
        
    }
    
    public List<Student> getRetail() throws SQLException {
        List<Student> resultList = new ArrayList();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            ps.setAutoCommit(false);
            int i = 1;
            String sql = "SELECT * FROM RETAIL ";
            
            ps.setSql(sql);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                Student data = new Student();
                data.setName(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setEmail(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setCourseList(_getRetailStation(conn, rs.getInt("RETAIL_ID")));

//            JRBeanCollectionDataSource courseDataSource = new JRBeanCollectionDataSource(data.getCourseList(), false);
//                data.setCoursedataSource(courseDataSource);
                resultList.add(data);
            }
            
        } catch (Exception e) {
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return resultList;
        
    }
    
    public List<Course> _getRetailStation(Connection conn, int id) throws SQLException {
        List<Course> resultList = new ArrayList();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            ps.setAutoCommit(false);
            int i = 1;
            String sql = "SELECT * FROM RETAIL_STATION WHERE RETAIL_ID = " + id;
            
            ps.setSql(sql);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                Course data = new Course();
                data.setName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setLocation(AppUtil.checkNullData(rs.getString("POSTCODE")));
                data.setLocation2(AppUtil.checkNullData(rs.getString("POSTCODE") + "2"));
                data.setQty1("aaa" + rs.getString("POSTCODE"));
                data.setQty2("bbb" + rs.getString("POSTCODE"));
                resultList.add(data);
            }
            
        } catch (Exception e) {
        } finally {
            ps.resultSetClose(rs);
        }
        return resultList;
        
    }
}
