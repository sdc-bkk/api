/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DataModel;
import model.FilterModel;
import model.data.ResultData;
import model.data.ResultPage;
import repository.ReportRepo;
import utility.AppUtil;
import java.util.Map;
import java.util.HashMap;
import java.util.Locale;
import model.ReportInputModel;
import model.Student;
import model.StudentReportInput;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import utility.AppConfig;

/**
 *
 * @author User
 */
public class ReportService {

    ReportRepo repo = new ReportRepo();

    //get Data
    //<editor-fold defaultstate="collapsed" desc="getReportDataList">
    public ResultData<List<DataModel>> getReportDataList(FilterModel filter) {
        ResultData<List<DataModel>> dataResult = new ResultData<>();
        ResultPage resultPage = null;
        try {
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }
            dataResult = repo.getReportList(resultPage, filter);
        } catch (SQLException e) {
        }
        return dataResult;
    }
    //</editor-fold>

    //get PDF & Excel
    //<editor-fold defaultstate="collapsed" desc="getPDFAndExcel">
    public void getPDFAndExcel(HttpServletRequest request, HttpServletResponse response, String fileType, FilterModel filter) throws Exception {

        try {
            List<DataModel> dataList = new ArrayList();
            ResultData<List<DataModel>> resultData = new ResultData<>();

            //getData
            resultData = repo.getReportList(null, filter);
            if (resultData.getResult() != null) {
                dataList = resultData.getResult();
            }

            //map parameter
            Map params = new HashMap();
            //go to filter model and go to reportEnum find reportName Head PDF
            String systemName = "ระบบภาษีน้ำมันฯ";
            String reportName = filter.getReport().reportName();
            String reportCode = filter.getReport().reportCode();
            String userName = filter.getUsername();

            Locale locale = new Locale("th", "TH");
            SimpleDateFormat formatterPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", locale);
            Date date = new Date();
            String datePrint = formatterPrint.format(date);

            String filterValue = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate()) || !AppUtil.isNullAndSpace(filter.getEndDate())) {
                if (filter.getStartDate() == null ? filter.getEndDate() == null : filter.getStartDate().equals(filter.getEndDate())) {
                    filterValue = "ประจำวันที่ " + AppUtil.checkNullData(filter.getStartDate());
                } else {
                    filterValue = "ประจำวันที่ " + AppUtil.checkNullAndEmptyData(filter.getStartDate());
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndDate());
                }
            } else if (!AppUtil.isNullAndSpace(filter.getStartMonth()) || !AppUtil.isNullAndSpace(filter.getEndMonth())) {
                if ((filter.getStartMonth() == null ? filter.getEndMonth() == null : filter.getStartMonth().equals(filter.getEndMonth())) && (filter.getStartYear() == null ? filter.getEndYear() == null : filter.getStartYear().equals(filter.getEndYear()))) {
                    filterValue = "ประจำเดือน " + AppUtil.checkNullData(filter.getStartMonth()) + " " + filter.getStartYear();
                } else {
                    filterValue = "ประจำเดือน " + AppUtil.checkNullAndEmptyData(filter.getStartMonth()) + " " + filter.getStartYear();
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndMonth()) + " " + filter.getEndYear();
                }
            } else if (!AppUtil.isNullAndSpace(filter.getStartYear()) || !AppUtil.isNullAndSpace(filter.getEndYear())) {
                if (filter.getStartYear() == null ? filter.getEndYear() == null : filter.getStartYear().equals(filter.getEndYear())) {
                    filterValue = "ประจำปี " + AppUtil.checkNullData(filter.getStartYear());
                } else {
                    filterValue = "ประจำปี " + AppUtil.checkNullAndEmptyData(filter.getStartYear());
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndYear());
                }
            } else if (!AppUtil.isNullAndSpace(filter.getYear())) {
                filterValue = "ประจำปี " + AppUtil.checkNullData(filter.getYear());
            }

            params.put("systemName", AppUtil.checkNullData(systemName));
            params.put("reportName", AppUtil.checkNullData(reportName));
            params.put("reportCode", AppUtil.checkNullData(reportCode));
            params.put("userName", AppUtil.checkNullData(userName));
            params.put("datePrint", AppUtil.checkNullData(datePrint));
            params.put("filter", filterValue);
            params.put("year", dataList.get(0).getYear());
            params.put("officeName", dataList.get(0).getOfficeName());

            //fill report
            InputStream resourceFile = null;
            AppConfig appConfig = new AppConfig();
            String folderReport = appConfig.value("reportPath.reportAll");

            //filter  use reportEnum by first parameter
            String pathReport = folderReport + filter.getReport().reportFile() + "_" + fileType + ".jrxml";
            resourceFile = request.getServletContext().getResourceAsStream(pathReport);
            JasperReport report = JasperCompileManager.compileReport(resourceFile);

            JRBeanCollectionDataSource beanDataSource = new JRBeanCollectionDataSource(dataList);
            
            params.put("dataList", beanDataSource);
            JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

            //export
            SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
            String nameReport = filter.getReport().reportCode() + " " + filter.getReport().reportName();
            if (fileType.equalsIgnoreCase("WORD")) {
                ExportService.exportWord(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("EXCEL")) {
                ExportService.exportExcel(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("base64")) {
            } else {
                ExportService.exportPDF(jprint, response, nameReport);
            }
        } catch (IOException | SQLException | JRException e) {
        }
    }
    //</editor-fold>

    public void rpt402_Pay03(HttpServletRequest request, HttpServletResponse response, String fileType, FilterModel filter) throws Exception {
        byte[] reportData = null;
        try {

            ReportInputModel reportInput = new ReportInputModel();
//            studentReportInput.setReportTitle("Student Report");
//            studentReportInput.setInstituteName("My Institute");

            List<DataModel> data = new ArrayList<>();
            data = repo.getRpt402_Pay03(filter);

            JRBeanCollectionDataSource dataDataSource = new JRBeanCollectionDataSource(data, false);
            reportInput.setDataDataSource(dataDataSource);

            JRMapArrayDataSource dataSource = new JRMapArrayDataSource(new Object[]{reportInput.getDataSources()});

            //map parameter
            Map params = new HashMap();
            //go to filter model and go to reportEnum find reportName Head PDF
            String reportName = filter.getReport().reportName();
            params.put("reportName", AppUtil.checkNullData(reportName));

            //fill report
            InputStream resourceFile = null;
            AppConfig appConfig = new AppConfig();
            String folderReport = appConfig.value("reportPath.reportAll");

            //filter  use reportEnum by first parameter
            String pathReport = folderReport + "blank.jrxml";
//            String pathReport = folderReport + filter.getReport().reportFile() + "_" + fileType + ".jrxml";
            resourceFile = request.getServletContext().getResourceAsStream(pathReport);
            JasperReport report = JasperCompileManager.compileReport(resourceFile);
            JasperPrint jprint = JasperFillManager.fillReport(report, params, dataSource);

            //export
            SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
            String nameReport = filter.getReport().reportCode() + " " + filter.getReport().reportName();
            if (fileType.equalsIgnoreCase("WORD")) {
                ExportService.exportWord(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("EXCEL")) {
                ExportService.exportExcel(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("base64")) {
            } else {
                ExportService.exportPDF(jprint, response, nameReport);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void test(HttpServletRequest request, HttpServletResponse response, String fileType, FilterModel filter) throws Exception {
        byte[] reportData = null;
        try {

//            StudentReportInput studentReportInput = StudentReportDataAssembler.assemble();
            StudentReportInput studentReportInput = new StudentReportInput();
            studentReportInput.setReportTitle("Student Report");
            studentReportInput.setInstituteName("My Institute");

            List<Student> students = new ArrayList<>();
            students = repo.getRetail();

            JRBeanCollectionDataSource studentDataSource = new JRBeanCollectionDataSource(students, false);
            studentReportInput.setStudentDataSource(studentDataSource);

            JRMapArrayDataSource dataSource = new JRMapArrayDataSource(new Object[]{studentReportInput.getDataSources()});
            //fill report
            InputStream resourceFile = null;
            AppConfig appConfig = new AppConfig();
            String folderReport = appConfig.value("reportPath.reportAll");

            //filter  use reportEnum by first parameter
            String pathReport = folderReport + "blank.jrxml";
            resourceFile = request.getServletContext().getResourceAsStream(pathReport);
            JasperReport report = JasperCompileManager.compileReport(resourceFile);
            JasperPrint jprint = JasperFillManager.fillReport(report, studentReportInput.getParameters(), dataSource);

            //export
            SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
            String nameReport = filter.getReport().reportCode() + " " + filter.getReport().reportName();
            if (fileType.equalsIgnoreCase("WORD")) {
                ExportService.exportWord(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("EXCEL")) {
                ExportService.exportExcel(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("base64")) {
            } else {
                ExportService.exportPDF(jprint, response, nameReport);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //For ท1
    public void getPDFAndExcel_Special(HttpServletRequest request, HttpServletResponse response, String fileType, FilterModel filter) throws Exception {

        try {
            List<DataModel> dataList = new ArrayList();
            ResultData<List<DataModel>> resultData = new ResultData<>();

            //getData
            resultData = repo.getReportList(null, filter);
            if (resultData.getResult() != null) {
                dataList = resultData.getResult();
            }

            //map parameter
            Map params = new HashMap();
            //go to filter model and go to reportEnum find reportName Head PDF
            String systemName = "ระบบภาษีน้ำมันฯ";
            String reportName = filter.getReport().reportName();
            String reportCode = filter.getReport().reportCode();
            String userName = filter.getUsername();

            Locale locale = new Locale("th", "TH");
            SimpleDateFormat formatterPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", locale);
            Date date = new Date();
            String datePrint = formatterPrint.format(date);

            String filterValue = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate()) || !AppUtil.isNullAndSpace(filter.getEndDate())) {
                if (filter.getStartDate() == null ? filter.getEndDate() == null : filter.getStartDate().equals(filter.getEndDate())) {
                    filterValue = "ประจำวันที่ " + AppUtil.checkNullData(filter.getStartDate());
                } else {
                    filterValue = "ประจำวันที่ " + AppUtil.checkNullAndEmptyData(filter.getStartDate());
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndDate());
                }
            } else if (!AppUtil.isNullAndSpace(filter.getStartMonth()) || !AppUtil.isNullAndSpace(filter.getEndMonth())) {
                if ((filter.getStartMonth() == null ? filter.getEndMonth() == null : filter.getStartMonth().equals(filter.getEndMonth())) && (filter.getStartYear() == null ? filter.getEndYear() == null : filter.getStartYear().equals(filter.getEndYear()))) {
                    filterValue = "ประจำเดือน " + AppUtil.checkNullData(filter.getStartMonth()) + " " + filter.getStartYear();
                } else {
                    filterValue = "ประจำเดือน " + AppUtil.checkNullAndEmptyData(filter.getStartMonth()) + " " + filter.getStartYear();
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndMonth()) + " " + filter.getEndYear();
                }
            } else if (!AppUtil.isNullAndSpace(filter.getStartYear()) || !AppUtil.isNullAndSpace(filter.getEndYear())) {
                if (filter.getStartYear() == null ? filter.getEndYear() == null : filter.getStartYear().equals(filter.getEndYear())) {
                    filterValue = "ประจำปี " + AppUtil.checkNullData(filter.getStartYear());
                } else {
                    filterValue = "ประจำปี " + AppUtil.checkNullAndEmptyData(filter.getStartYear());
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndYear());
                }
            } else if (!AppUtil.isNullAndSpace(filter.getYear())) {
                filterValue = "ประจำปี " + AppUtil.checkNullData(filter.getYear());
            }

            params.put("systemName", AppUtil.checkNullData(systemName));
            params.put("reportName", AppUtil.checkNullData(reportName));
            params.put("reportCode", AppUtil.checkNullData(reportCode));
            params.put("userName", AppUtil.checkNullData(userName));
            params.put("datePrint", AppUtil.checkNullData(datePrint));
            params.put("filter", filterValue);
            params.put("year", dataList.get(0).getYear());
            params.put("officeName", dataList.get(0).getOfficeName());

            //fill report
            InputStream resourceFile = null;
            AppConfig appConfig = new AppConfig();
            String folderReport = appConfig.value("reportPath.reportAll");

            //filter  use reportEnum by first parameter
            String pathReport = folderReport + filter.getReport().reportFile() + "_" + fileType + ".jrxml";
            resourceFile = request.getServletContext().getResourceAsStream(pathReport);
            JasperReport report = JasperCompileManager.compileReport(resourceFile);

            JRBeanCollectionDataSource beanDataSource = new JRBeanCollectionDataSource(dataList);
            
            params.put("dataList", beanDataSource);
            JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

            //export
            SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
            String nameReport = filter.getReport().reportCode() + " " + filter.getReport().reportName();
            if (fileType.equalsIgnoreCase("WORD")) {
                ExportService.exportWord(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("EXCEL")) {
                ExportService.exportExcel(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("base64")) {
            } else {
                ExportService.exportPDF(jprint, response, nameReport);
            }
        } catch (IOException | SQLException | JRException e) {
        }
    }
    
    public void test2(HttpServletRequest request, HttpServletResponse response, String fileType, FilterModel filter) throws Exception {

        try {
            List<DataModel> dataList = new ArrayList();
            ResultData<List<DataModel>> resultData = new ResultData<>();

            //getData
            resultData = repo.getReportList(null, filter);
            if (resultData.getResult() != null) {
                dataList = resultData.getResult();
            }

            //map parameter
            Map params = new HashMap();
            //go to filter model and go to reportEnum find reportName Head PDF
            String systemName = "ระบบภาษีน้ำมันฯ";
            String reportName = filter.getReport().reportName();
            String reportCode = filter.getReport().reportCode();
            String userName = filter.getUsername();

            Locale locale = new Locale("th", "TH");
            SimpleDateFormat formatterPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", locale);
            Date date = new Date();
            String datePrint = formatterPrint.format(date);

            String filterValue = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate()) || !AppUtil.isNullAndSpace(filter.getEndDate())) {
                if (filter.getStartDate() == null ? filter.getEndDate() == null : filter.getStartDate().equals(filter.getEndDate())) {
                    filterValue = "ประจำวันที่ " + AppUtil.checkNullData(filter.getStartDate());
                } else {
                    filterValue = "ประจำวันที่ " + AppUtil.checkNullAndEmptyData(filter.getStartDate());
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndDate());
                }
            } else if (!AppUtil.isNullAndSpace(filter.getStartMonth()) || !AppUtil.isNullAndSpace(filter.getEndMonth())) {
                if ((filter.getStartMonth() == null ? filter.getEndMonth() == null : filter.getStartMonth().equals(filter.getEndMonth())) && (filter.getStartYear() == null ? filter.getEndYear() == null : filter.getStartYear().equals(filter.getEndYear()))) {
                    filterValue = "ประจำเดือน " + AppUtil.checkNullData(filter.getStartMonth()) + " " + filter.getStartYear();
                } else {
                    filterValue = "ประจำเดือน " + AppUtil.checkNullAndEmptyData(filter.getStartMonth()) + " " + filter.getStartYear();
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndMonth()) + " " + filter.getEndYear();
                }
            } else if (!AppUtil.isNullAndSpace(filter.getStartYear()) || !AppUtil.isNullAndSpace(filter.getEndYear())) {
                if (filter.getStartYear() == null ? filter.getEndYear() == null : filter.getStartYear().equals(filter.getEndYear())) {
                    filterValue = "ประจำปี " + AppUtil.checkNullData(filter.getStartYear());
                } else {
                    filterValue = "ประจำปี " + AppUtil.checkNullAndEmptyData(filter.getStartYear());
                    filterValue += " ถึง " + AppUtil.checkNullAndEmptyData(filter.getEndYear());
                }
            } else if (!AppUtil.isNullAndSpace(filter.getYear())) {
                filterValue = "ประจำปี " + AppUtil.checkNullData(filter.getYear());
            }

            params.put("systemName", AppUtil.checkNullData(systemName));
            params.put("reportName", AppUtil.checkNullData(reportName));
            params.put("reportCode", AppUtil.checkNullData(reportCode));
            params.put("userName", AppUtil.checkNullData(userName));
            params.put("datePrint", AppUtil.checkNullData(datePrint));
            params.put("filter", filterValue);
            params.put("year", dataList.get(0).getYear());
            params.put("officeName", dataList.get(0).getOfficeName());
            params.put("p1", "testtest");

            //fill report
            InputStream resourceFile = null;
            AppConfig appConfig = new AppConfig();
            String folderReport = appConfig.value("reportPath.reportAll");

            //filter  use reportEnum by first parameter
            String pathReport = folderReport+ "Blank_8.jrxml";;
            resourceFile = request.getServletContext().getResourceAsStream(pathReport);
            JasperReport report = JasperCompileManager.compileReport(resourceFile);

            JRBeanCollectionDataSource beanDataSource = new JRBeanCollectionDataSource(dataList);
            
            params.put("dataList", beanDataSource);
            JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

            //export
            SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
            String nameReport = filter.getReport().reportCode() + " " + filter.getReport().reportName();
            if (fileType.equalsIgnoreCase("WORD")) {
                ExportService.exportWord(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("EXCEL")) {
                ExportService.exportExcel(jprint, response, nameReport);
            } else if (fileType.equalsIgnoreCase("base64")) {
            } else {
                ExportService.exportPDF(jprint, response, nameReport);
            }
        } catch (IOException | SQLException | JRException e) {
        }
    }
    
}
