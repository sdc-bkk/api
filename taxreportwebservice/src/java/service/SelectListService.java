/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.DocStatusEnum;
import enumeration.StationStatusEnum;
import enumeration.StatusWarnformEnum;
import java.util.ArrayList;
import java.util.List;
import model.SelectListModel;
import model.data.ResultData;
import model.data.ResultPage;
import repository.SelectListRepo;

/**
 *
 * @author User
 */
public class SelectListService {

    SelectListRepo repo = new SelectListRepo();

    public ResultData<List<SelectListModel>> getOfficeList() {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();

        try {

            ResultPage resultPage = null;

            resultData = repo.getOfficeList();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<SelectListModel>> getDocStatusList() {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();

        try {

            List<SelectListModel> dataList = new ArrayList<>();
            List<DocStatusEnum> list = new ArrayList<DocStatusEnum>();
            list = DocStatusEnum.list();
            for (DocStatusEnum dataEnum : list) {
                if (dataEnum.value() != 0) {//ไม่เอาสถานะสร้าง
                    SelectListModel data = new SelectListModel();
                    data.setId(String.valueOf(dataEnum.value()));
                    data.setName(dataEnum.displayNameTH());
                    dataList.add(data);
                }
            }
            resultData.setResult(dataList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<SelectListModel>> getStationStatusList() {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();

        try {

            List<SelectListModel> dataList = new ArrayList<>();
            List<StationStatusEnum> list = new ArrayList<StationStatusEnum>();
            list = StationStatusEnum.list();
            for (StationStatusEnum dataEnum : list) {
                if (dataEnum.value() != 0) {//ไม่เอาสถานะสร้าง
                    SelectListModel data = new SelectListModel();
                    data.setId(String.valueOf(dataEnum.value()));
                    data.setName(dataEnum.displayNameTH());
                    dataList.add(data);
                }
            }
            resultData.setResult(dataList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<SelectListModel>> getWarnFormStatusList() {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();

        try {

            List<SelectListModel> dataList = new ArrayList<>();
            List<StatusWarnformEnum> list = new ArrayList<StatusWarnformEnum>();
            list = StatusWarnformEnum.list();
            for (StatusWarnformEnum dataEnum : list) {
//                if (dataEnum.value() != 0) {//ไม่เอาสถานะสร้าง
                    SelectListModel data = new SelectListModel();
                    data.setId(String.valueOf(dataEnum.value()));
                    data.setName(dataEnum.displayNameTH());
                    dataList.add(data);
//                }
            }
            resultData.setResult(dataList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<SelectListModel>> getBusinessStatusList() {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();

        try {

            ResultPage resultPage = null;

            resultData = repo.getBusinessStatusList();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<SelectListModel>> getBusinessTypeList() {

        ResultData<List<SelectListModel>> resultData = new ResultData<List<SelectListModel>>();

        try {

            ResultPage resultPage = null;

            resultData = repo.getBusinessTypeList();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }
}
