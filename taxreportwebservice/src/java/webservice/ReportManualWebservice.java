/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.ReportEnum;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.DataModel;
import model.FilterModel;
import model.data.ResultData;
import service.ReportService;
import utility.AppUtil;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("reportManual")
public class ReportManualWebservice {

    ReportService service = new ReportService();

    //<editor-fold defaultstate="collapsed" desc="401 getQty03">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getQty03")
    public String getQty03(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt401_Qty03);
            ResultData<List<DataModel>> data;

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getQty03PDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getQty03PDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            filter.setReport(ReportEnum.rpt401_Qty03);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getQty03Excel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getQty03Excel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            
            filter.setReport(ReportEnum.rpt401_Qty03);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="402 getPay03">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getPay03")
    public String getPay03(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt402_Pay03);
            ResultData<List<DataModel>> data;
            
            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }
    
    @POST
    @Path("getPay03PDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03PDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            if (filter.getYear() == null || "".equals(filter.getYear())) {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            
            filter.setReport(ReportEnum.rpt402_Pay03);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
    @POST
    @Path("getPay03Excel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03Excel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt402_Pay03);
            
            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    
    @POST
    @Path("test")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void test(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            
            filter.setReport(ReportEnum.rpt402_Pay03);
            service.test(request, response, "pdf", filter);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
