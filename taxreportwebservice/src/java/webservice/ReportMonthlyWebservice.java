/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.ReportEnum;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.DataModel;
import model.FilterModel;
import model.data.ResultData;
import service.ReportService;
import utility.AppUtil;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("reportMonthly")
public class ReportMonthlyWebservice {

    ReportService service = new ReportService();

    //<editor-fold defaultstate="collapsed" desc="201 getNoSend03ByOrg">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getNoSend03ByOrg")
    public String getNoSend03ByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt201_Nosend03ByOrg);
            ResultData<List<DataModel>> data;
            filter = AppUtil.setReortMonthlyFilter(filter);
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getNoSend03ByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoSend03ByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt201_Nosend03ByOrg);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getNoSend03ByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoSend03ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt201_Nosend03ByOrg);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="202 getNoPay03ByOrg">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getNoPay03ByOrg")
    public String getNoPay03ByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt202_Nopay03ByOrg);
            ResultData<List<DataModel>> data;
            filter = AppUtil.setReortMonthlyFilter(filter);
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getNoPay03ByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoPay03ByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt202_Nopay03ByOrg);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getNoPay03ByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoPay03ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt202_Nopay03ByOrg);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="203 getNoPay03ByRetail">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getNoPay03ByRetail")
    public String getNoPay03ByRetail(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt203_Nopay03ByRetail);
            ResultData<List<DataModel>> data;
            filter = AppUtil.setReortMonthlyFilter(filter);
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getNoPay03ByRetailPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoPay03ByRetailPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt203_Nopay03ByRetail);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getNoPay03ByRetailExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoPay03ByRetailExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt203_Nopay03ByRetail);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="204 NoSend03Monthly">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getNoSend03Monthly")
    public String getNoSend03Monthly(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt204_Nosend03Monthly);
            ResultData<List<DataModel>> data;
            filter = AppUtil.setReortMonthlyFilter(filter);
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getNoSend03MonthlyPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoSend03MonthlyPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt204_Nosend03Monthly);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getNoSend03MonthlyExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoSend03MonthlyExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt204_Nosend03Monthly);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="205 getNoPay03Monthly">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getNoPay03Monthly")
    public String getNoPay03Monthly(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt205_Nopay03Monthly);
            ResultData<List<DataModel>> data;
            filter = AppUtil.setReortMonthlyFilter(filter);
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getNoPay03MonthlyPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoPay03MonthlyPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt205_Nopay03Monthly);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getNoPay03MonthlyExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getNoPay03MonthlyExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt205_Nopay03Monthly);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="206 getSummary03ByOrg">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getSummary03ByOrg")
    public String getSummary03ByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt206_Summary03ByOrg);
            ResultData<List<DataModel>> data;
            filter = AppUtil.setReortMonthlyFilter(filter);
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getSummary03ByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummary03ByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt206_Summary03ByOrg);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getSummary03ByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummary03ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt206_Summary03ByOrg);
            filter = AppUtil.setReortMonthlyFilter(filter);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="207 getSummaryNosend03ByOrg">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getSummaryNosend03ByOrg")
    public String getSummaryNosend03ByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt207_SummaryNosend03ByOrg);
            ResultData<List<DataModel>> data;

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }

            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getSummaryNosend03ByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummaryNosend03ByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }

            filter.setReport(ReportEnum.rpt207_SummaryNosend03ByOrg);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getSummaryNosend03ByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummaryNosend03ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt207_SummaryNosend03ByOrg);

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }

            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="208 getSummaryNosend03ByOrg">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getSummaryNopay03ByOrg")
    public String getSummaryNopay03ByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt208_SummaryNopay03ByOrg);
            ResultData<List<DataModel>> data;

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getSummaryNopay03ByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummaryNopay03ByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            filter.setReport(ReportEnum.rpt208_SummaryNopay03ByOrg);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getSummaryNopay03ByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummaryNopay03ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt208_SummaryNopay03ByOrg);

            if (filter.getYear() == null || filter.getYear() == "") {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                filter.setYear(Integer.toString(yearly));
            }
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="209 getSummaryPay03ByOilType">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getSummaryPay03ByOilType")
    public String getSummaryPay03ByOilType(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt209_SummaryPay03ByOilType);
            ResultData<List<DataModel>> data;

            filter = AppUtil.setReortMonthlyFilter(filter);

            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getSummaryPay03ByOilTypePDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummaryPay03ByOilTypePDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter = AppUtil.setReortMonthlyFilter(filter);

            filter.setReport(ReportEnum.rpt209_SummaryPay03ByOilType);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getSummaryPay03ByOilTypeExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummaryPay03ByOilTypeExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt209_SummaryPay03ByOilType);

            filter = AppUtil.setReortMonthlyFilter(filter);

            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="210 getStatusRetailByOrg">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getStatusRetailByOrg")
    public String getStatusRetailByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt210_StatusRetailByOrg);
            ResultData<List<DataModel>> data;

            if (filter.getYear() == null || filter.getYear() == "") {
                filter.setYear("2564");
            }
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getStatusRetailByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getStatusRetailByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            if (filter.getYear() == null || filter.getYear() == "") {
                filter.setYear("2564");
            }
            filter.setReport(ReportEnum.rpt210_StatusRetailByOrg);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getStatusRetailByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getStatusRetailByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt210_StatusRetailByOrg);

            if (filter.getYear() == null || filter.getYear() == "") {
                filter.setYear("2564");
            }
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
}
