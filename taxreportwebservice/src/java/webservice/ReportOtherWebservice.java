/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.ReportEnum;
import java.util.List;
////import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import model.DataModel;
import model.FilterModel;
import model.data.ResultData;
import service.ReportService;
import utility.AppUtil;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("reportOther")
public class ReportOtherWebservice {
    ReportService service = new ReportService();
        
    //<editor-fold defaultstate="collapsed" desc="501 getDocDetail">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getRetailDetail")
    public String getRetailDetail(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt501_RetailDetail);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }
    
    @POST
    @Path("getRetailDetailPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getRetailDetailPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            filter.setReport(ReportEnum.rpt501_RetailDetail);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
    @POST
    @Path("getRetailDetailExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getRetailDetailExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            
            filter.setReport(ReportEnum.rpt501_RetailDetail);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="502 getStationDetail">
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getStationDetail")
    public String getStationDetail(FilterModel filter) {
        try {
//            FilterModel filter = new FilterModel();
            filter.setReport(ReportEnum.rpt502_StationDetail);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
            
        }
        return null;
    }
    
    @POST
    @Path("getStationDetailPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getStationDetailPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            filter.setReport(ReportEnum.rpt502_StationDetail);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
    @POST
    @Path("getStationDetailExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getStationDetailExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            
            filter.setReport(ReportEnum.rpt502_StationDetail);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="503 getDocDetail">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDocDetail")
    public String getDocDetail(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt503_DocDetail);
            ResultData<List<DataModel>> data;   
            
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }
    
    @POST
    @Path("getDocDetailPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getDocDetailPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);            
            filter.setReport(ReportEnum.rpt503_DocDetail);
                        
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
    @POST
    @Path("getDocDetailExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getSummaryNopay03ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt503_DocDetail);
                        
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="504 getPayOther">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getPayOther")
    public String getPayOther(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt504_PayOther);
            ResultData<List<DataModel>> data;  
            
            filter = AppUtil.setReortMonthlyFilter(filter);
            
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }
    
    @POST
    @Path("getPayOtherPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPayOtherPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class); 
            
            filter = AppUtil.setReortMonthlyFilter(filter);
            
            filter.setReport(ReportEnum.rpt504_PayOther);
                        
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
    @POST
    @Path("getPayOtherExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPayOtherExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            filter = AppUtil.setReortMonthlyFilter(filter);
            
            filter.setReport(ReportEnum.rpt504_PayOther);
                        
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="505 getPayHistory">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getPayHistory")
    public String getPayHistory(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt505_PayHistory);
            ResultData<List<DataModel>> data;   
                        
            filter = AppUtil.setReortMonthlyFilter(filter);
            
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }
    
    @POST
    @Path("getPayHistoryPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPayHistoryPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);            
            filter.setReport(ReportEnum.rpt505_PayHistory);
            
            filter = AppUtil.setReortMonthlyFilter(filter);
                        
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
    @POST
    @Path("getPayHistoryExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPayHistoryExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt505_PayHistory);
            
            filter = AppUtil.setReortMonthlyFilter(filter);
                        
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
}
