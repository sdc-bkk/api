/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.ReportEnum;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import model.DataModel;
import model.FilterModel;
import model.data.ResultData;
import service.ReportService;
import utility.AppUtil;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("reportDaily")
public class ReportDailyWebservice {

    ReportService service = new ReportService();

    //<editor-fold defaultstate="collapsed" desc="101 getRegisterRetail">    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getRegisterRetail")
    public String getRegisterRetail(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt101_RegisterRetail);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getRegisterRetailPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getRegisterRetailPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt101_RegisterRetail);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getRegisterRetailExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getRegisterRetailExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
//            FilterModel filter = new FilterModel();
            filter.setReport(ReportEnum.rpt101_RegisterRetail);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="102 getRetail">
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getRetail")
    public String getRetail(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt102_Retail);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getRetailPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getRetailPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt102_Retail);

            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getRetailExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getRetailExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt102_Retail);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="103 getChangeData">   
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getChangeData")
    public String getChangeData(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt103_ChangeData);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }
    
    @POST
    @Path("getChangeDataPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getChangeDataPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt103_ChangeData);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    @POST
    @Path("getChangeDataExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getChangeDataExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt103_ChangeData);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="104 get01ByOrg">    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("get01ByOrg")
    public String get01ByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt104_01ByOrg);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("get01ByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get01ByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt104_01ByOrg);

            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("get01ByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get01ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt104_01ByOrg);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="105 get01ByOfficer">     
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("get01ByOfficer")
    public String get01ByOfficer(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt105_01ByOfficer);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("get01ByOfficerPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get01ByOfficerPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt105_01ByOfficer);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("get01ByOfficerExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get01ByOfficerExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt105_01ByOfficer);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="106 get03ByOrg">    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("get03ByOrg")
    public String get03ByOrg(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt106_03ByOrg);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("get03ByOrgPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get03ByOrgPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt106_03ByOrg);

            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("get03ByOrgExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get03ByOrgExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            filter.setReport(ReportEnum.rpt106_03ByOrg);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
}
