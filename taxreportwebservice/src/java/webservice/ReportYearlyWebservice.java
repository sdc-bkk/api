/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.ReportEnum;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import model.DataModel;
import model.FilterModel;
import model.data.ResultData;
import service.ReportService;
import utility.AppUtil;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("reportYearly")
public class ReportYearlyWebservice {

    ReportService service = new ReportService();
    
    //<editor-fold defaultstate="collapsed" desc="301 get01ByOrgYearly"> 
//    @POST
//    @Consumes("Application/json;charset=utf8")
//    @Produces("Application/json;charset=utf8")
//    @Path("get01ByOrgYearly")
//    public String get01ByOrgYearly(FilterModel filter) {
//        try {
//            filter.setReport(ReportEnum.rpt301_01ByOrgYearly);
//            ResultData<List<DataModel>> data;
//            data = service.getReportDataList(filter);
//            return new Gson().toJson(data);
//        } catch (Exception e) {
//        }
//        return null;
//    }
    
    @POST
    @Path("test")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void test(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            filter.setReport(ReportEnum.rpt301_01ByOrgYearly);
            service.getPDFAndExcel_Special(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
//    @POST
//    @Path("get01ByOrgYearlyExcel")
//    @Produces(MediaType.APPLICATION_OCTET_STREAM)
//    public void get01ByOrgYearlypathExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
//        try {
//            String jsonString = AppUtil.extractPostRequestBody(request);
//            Gson gson = new Gson();
//            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
//
//            filter.setReport(ReportEnum.rpt301_01ByOrgYearly);
//            service.getPDFAndExcel(request, response, "excel", filter);
//        } catch (Exception e) {
//        }
//    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="301 get01ByOrgYearly"> 
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("get01ByOrgYearly")
    public String get01ByOrgYearly(FilterModel filter) {
        try {
            filter.setReport(ReportEnum.rpt301_01ByOrgYearly);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }
    
    @POST
    @Path("get01ByOrgYearlyPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get01ByOrgYearlyPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            filter.setReport(ReportEnum.rpt301_01ByOrgYearly);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    @POST
    @Path("test2")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void test2(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);
            
            filter.setReport(ReportEnum.rpt301_01ByOrgYearly);
            service.test2(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }
    
    @POST
    @Path("get01ByOrgYearlyExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void get01ByOrgYearlypathExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt301_01ByOrgYearly);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="302 getPay03Yearly">    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getPay03Yearly")
    public String getPay03Yearly(FilterModel filter) {
        try {
//            FilterModel filter = new FilterModel();
            filter.setReport(ReportEnum.rpt302_Pay03Yearly);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getPay03YearlyPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03YearlyPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt302_Pay03Yearly);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getPay03YearlyExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03YearlyExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt302_Pay03Yearly);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="303 getStationYearly">    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getStationYearly")
    public String getStationYearly(FilterModel filter) {
        try {
//            FilterModel filter = new FilterModel();
            filter.setReport(ReportEnum.rpt303_StationYearly);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getStationYearlyPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getStationYearlyPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt303_StationYearly);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getStationYearlyExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getStationYearlyExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt303_StationYearly);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="304 getPay03StationYearly">    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getPay03StationYearly")
    public String getPay03StationYearly(FilterModel filter) {
        try {
//            FilterModel filter = new FilterModel();
            filter.setReport(ReportEnum.rpt304_Pay03StationYearly);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getPay03StationYearlyPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03StationYearlyPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt304_Pay03StationYearly);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getPay03StationYearlyExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03StationYearlyExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt304_Pay03StationYearly);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="305 getPay03OfficeYearly">    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getPay03OfficeYearly")
    public String getPay03OfficeYearly(FilterModel filter) {
        try {
//            FilterModel filter = new FilterModel();
            filter.setReport(ReportEnum.rpt305_Pay03OfficeYearly);
            ResultData<List<DataModel>> data;
            data = service.getReportDataList(filter);
            return new Gson().toJson(data);
        } catch (Exception e) {
        }
        return null;
    }

    @POST
    @Path("getPay03OfficeYearlyPDF")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03OfficeYearlyPDF(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt305_Pay03OfficeYearly);
            service.getPDFAndExcel(request, response, "pdf", filter);
        } catch (Exception e) {
        }
    }

    @POST
    @Path("getPay03OfficeYearlyExcel")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getPay03OfficeYearlyExcel(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = AppUtil.extractPostRequestBody(request);
            Gson gson = new Gson();
            FilterModel filter = gson.fromJson(jsonString, FilterModel.class);

            filter.setReport(ReportEnum.rpt305_Pay03OfficeYearly);
            service.getPDFAndExcel(request, response, "excel", filter);
        } catch (Exception e) {
        }
    }
    //</editor-fold>
    
}
