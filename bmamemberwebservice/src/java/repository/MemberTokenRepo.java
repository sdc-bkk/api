/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.MemberModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.MemberFilterModel;
import repository.mapper.MemberMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.MemberViewModel;

/**
 *
 * @author Sirichai
 */
public class MemberTokenRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public boolean save(MemberFilterModel member, int expireTime) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Delete Member">
            sql = " DELETE FROM MEMBER_TOKEN "
                    + " WHERE USER_NAME =? ";

            ps = conn.prepareStatement(sql);
            ps.setString(i++, member.getUserName());

            result = ps.executeUpdate() != 0;

            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="MemberToken">
            sql = "INSERT INTO MEMBER_TOKEN ( "
                    + "  USER_NAME "
                    + ", TOKEN "
                    + ", EXPIRED_DATE "
                    + ") VALUES(?,?,SYSDATE + (1/1440 * " + expireTime + " )) ";

            ps = conn.prepareStatement(sql);
            i = 1;
            //Set Parameter
            ps.setString(i++, member.getUserName());
            ps.setString(i++, member.getToken());
            //ps.setInt(i++, expireTime);

            //</editor-fold>
            result = ps.executeUpdate() != 0;
            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }

    public boolean checkToken(MemberFilterModel member) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete Member">
            String sql = " DELETE FROM MEMBER_TOKEN "
                    + " WHERE EXPIRED_DATE < SYSDATE ";

            ps = conn.prepareStatement(sql);

            result = ps.executeUpdate() != 0;

            //</editor-fold>
            
             sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM MEMBER_TOKEN "
                    + " WHERE (USER_NAME = ?) "
                    + " AND (TOKEN = ?) AND EXPIRED_DATE > SYSDATE  ";

            ps = conn.prepareStatement(sql);

            ps.setString(i++, member.getUserName());
            ps.setString(i++, member.getToken());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;

    }

}
