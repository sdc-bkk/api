/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;

/**
 *
 * @author User
 */
public class AppConfigRepo {
    private final ConnnectionDB connDB = new ConnnectionDB();
        
    public String GetValue(String keyName) throws SQLException {
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        int pk = 0;
        String value = "";

        try {
            ps.setAutoCommit(false);

            int i = 1;

            //Get current value
            String sql = "SELECT KEY_VALUE FROM APP_CONFIG WHERE KEY = ? ";
            ps.setSql(sql);

            ps.setString(i++, keyName);

            rs = ps.executeQuery();

            while (rs.next()) {
                value = rs.getString("KEY_VALUE") == null ? "" : rs.getString("KEY_VALUE");
            }           

            conn.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return value;
    }
    
    public String checkValue(String keyName,String keyValue) throws SQLException {
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        int pk = 0;
        String value = "";

        try {
            ps.setAutoCommit(false);

            int i = 1;

            //Get current value
            String sql = "SELECT KEY_VALUE FROM APP_CONFIG WHERE KEY = ? AND KEY_VALUE=? ";
            ps.setSql(sql);

            ps.setString(i++, keyName);
            ps.setString(i++, keyValue);

            rs = ps.executeQuery();

            while (rs.next()) {
                value = rs.getString("KEY_VALUE") == null ? "" : rs.getString("KEY_VALUE");
            }           

            conn.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return value;
    }
}
