/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.data.ResultData;
import model.filter.MemberFilterModel;
import service.MemberRetailService;
import viewModel.MemberRetailViewModel;
import viewModel.MemberViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("memberRetail")
public class MemberRetailWebservice {

    @Context
    private UriInfo context;

    MemberRetailService memberRetailService = new MemberRetailService();
     
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(MemberFilterModel filter) {

        try {

            ResultData<MemberRetailViewModel> data = memberRetailService.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(MemberRetailViewModel usersView) {

        try {

            ResultData<Boolean> data = memberRetailService.saveData(usersView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("checkEmail")
    public String checkEmail(MemberFilterModel filter) {

        try {

            ResultData<Boolean> data = memberRetailService.checkEmail(filter);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
