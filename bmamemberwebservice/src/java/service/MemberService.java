/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import model.MemberModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.MemberFilterModel;
import repository.AppConfigRepo;
import repository.MemberRepo;
import repository.MemberTokenRepo;
import repository.mapper.MemberMapper;
import utility.AES;
import utility.AES256;
import utility.AppConfig;
import utility.AppUtil;
import utility.MailService;
import utility.MessageBundleUtil;
import viewModel.MemberViewModel;

/**
 *
 * @author Sirichai
 */
public class MemberService {

    MemberRepo memberRepo = new MemberRepo();

    public ResultData<List<MemberViewModel>> getListAll(MemberFilterModel filter) {

        ResultData<List<MemberViewModel>> data = new ResultData<List<MemberViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = memberRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<MemberViewModel> getData(MemberFilterModel filter) {

        ResultData<MemberViewModel> data = new ResultData<MemberViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getMemberId());
            if (memberRepo.hasData(id)) {

                data = memberRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.member.hasData"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<MemberViewModel> getDataByToken(MemberFilterModel filter) {

        ResultData<MemberViewModel> data = new ResultData<MemberViewModel>();

        try {
//            AppConfig appConfig = new AppConfig();
//            String secretKey = appConfig.value("secretKey");
//            String encryptedString = AES256.encrypt(secretKey, filter.getUserName().trim());

            MemberTokenRepo memberTokenRepo = new MemberTokenRepo();
            boolean isFound = memberTokenRepo.checkToken(filter);

            if (isFound) {
                data = memberRepo.getDataByUsername(filter.getUserName().trim());
            } else {
                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.member.noData"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(MemberViewModel memberView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        MemberModel member = new MemberModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            MemberFilterModel filter = new MemberFilterModel();
            filter.setMemberId(memberView.getMemberId());
            filter.setUserName(memberView.getUserName());

            boolean resultDuplicate = memberRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                MemberMapper mapper = new MemberMapper();

                member = mapper.mapFull(memberView);

                boolean resultSave = memberRepo.save(member);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> register(MemberFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            boolean resultDuplicate = memberRepo.duplicateUserName(userName.trim());

            if (!resultDuplicate) {
                //ยังไม่มี user ในระบบ

                boolean resultRegister = memberRepo.register(userName.trim(), filter.getTaxNo());

                resultData.setResult(resultRegister);
                if (resultRegister) {
                    AppConfig appConfig = new AppConfig();
                    String urlPath = appConfig.value("register.link");
                    String urlPathFront = appConfig.value("front.link");

                    //send Email
                    MailService mail = new MailService();
                    mail.setTo(userName.trim());
                    mail.setSubject("ยืนยันตัวตนเข้าใช้งานระบบงานรับเงินกลาง (BMA Payment)");
                    String temp = "เรียน คุณ  " + userName.trim() + "<br><br>"
                            + "กรุณา "
                            + "<a href=\"" + urlPath + "#/sign-in?userName=" + userName.trim() + "\">คลิกที่นี่</a> "
                            + " เพื่อยืนยันการเข้าใช้งานระบบงานรับเงินกลาง (BMA Payment) "
                            + " <br>เมื่อคุณยืนยันตัวตนเรียบร้อยแล้ว คุณจะสามารถเข้าใช้งานระบบงานรับเงินกลาง (BMA Payment) ได้ "
                            + "<a href=\"" + urlPathFront + "\">ที่นี่</a> "
                            + "<br><br>" + "ขอบคุณค่ะ";
                    mail.setMessageText(temp);
                    boolean resultSendMail = mail.sendMail();
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.member.register"));
                }

            } else {
                //มี user อยู่ในระบบแล้ว
                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.duplicateUserName"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> forgetPassword(MemberFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {

                resultData.setResult(true);
                AppConfig appConfig = new AppConfig();
                String urlPath = appConfig.value("register.link");
                //send Email
                MailService mail = new MailService();
                mail.setTo(userName.trim());
                mail.setSubject("เปลี่ยนรหัสผ่านเข้าใช้งานระบบงานรับเงินกลาง (BMA Payment)");
                String temp = "เรียน คุณ " + userName.trim() + "<br><br>"
                        + "กรุณา "
                        + "<a href=\"" + urlPath + "#/sign-in?userName=" + userName.trim() + "\">คลิกที่นี่</a>"
                        + " เพื่อกำหนดรหัสผ่านใหม่ สำหรับเข้าใช้งานระบบงานรับเงินกลาง (BMA Payment)"
                        + "<br><br>" + "ขอบคุณค่ะ";
                mail.setMessageText(temp);
                boolean resultSendMail = mail.sendMail();

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.register"));
            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> setPassword(MemberFilterModel filter) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {

                String password = filter.getPassword();
                boolean resultSave = memberRepo.setPassword(userName.trim(), password.trim());
                resultData.setResult(resultSave);
                AppConfig appConfig = new AppConfig();
                String urlLogin = appConfig.value("front.link");
                resultData.setMessage(urlLogin);

            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> changePassword(MemberFilterModel filter) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {

                String password = filter.getPassword();
                if (memberRepo.hasDataByUserNamePassword(userName.trim(), password.trim())) {

                    String newPassword = filter.getNewPassword();
                    boolean resultSave = memberRepo.setPassword(userName.trim(), newPassword.trim());
                    resultData.setResult(resultSave);

                } else {

                    resultData.setResult(false);
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.member.hasDataUserNamePassword"));

                }

            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> authenMember(MemberFilterModel filter) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {

                String password = filter.getPassword();
                if (memberRepo.hasDataByUserNamePassword(userName.trim(), password.trim())) {
                    //get token
                    UUID uuid = UUID.randomUUID();
                    AppConfig appConfig = new AppConfig();
                    String secretKey = appConfig.value("secretKey");
                    String encryptedString = AES256.encrypt(secretKey + "|" + uuid.toString(), userName.trim());
                    filter.setToken(encryptedString);

                    AppConfigRepo appConfigRepo = new AppConfigRepo();
                    String expireTimeStr = appConfigRepo.GetValue("expiretime");
                    int expireTime = (expireTimeStr == null || expireTimeStr == "") ? 0 : Integer.parseInt(expireTimeStr);

                    //insert token                    
                    MemberTokenRepo memberTokenRepo = new MemberTokenRepo();
                    memberTokenRepo.save(filter, expireTime);

                    resultData.setResult(true);
                    resultData.setToken(encryptedString);

                } else {

                    resultData.setResult(false);
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.member.authenNoSuccess"));

                }

            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> getTokenBma(MemberFilterModel filter, String host) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            AppConfigRepo appConfigRepo = new AppConfigRepo();
            //check url allow
            String allow = appConfigRepo.checkValue("allowurl",host);
            if (allow != null && !"".equals(allow)) {
                String userName = filter.getUserName();
                if (memberRepo.hasDataByUserName(userName.trim())) {

                    //get token
                    UUID uuid = UUID.randomUUID();
                    AppConfig appConfig = new AppConfig();
                    String secretKey = appConfig.value("secretKey");
                    String encryptedString = AES256.encrypt(secretKey + "|" + uuid.toString(), userName.trim());
                    filter.setToken(encryptedString);

                    String expireTimeStr = appConfigRepo.GetValue("expiretime");
                    int expireTime = (expireTimeStr == null || expireTimeStr == "") ? 0 : Integer.parseInt(expireTimeStr);

                    //insert token                    
                    MemberTokenRepo memberTokenRepo = new MemberTokenRepo();
                    memberTokenRepo.save(filter, expireTime);

                    resultData.setResult(true);
                    resultData.setToken(encryptedString);

                } else {

                    resultData.setResult(false);
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

                }
            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.nopermission"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }
}
