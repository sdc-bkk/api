/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import model.MemberModel;
import model.data.ResultData;
import model.filter.MemberFilterModel;
import repository.MemberRepo;
import repository.MemberRetailRepo;
import repository.mapper.MemberMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.MemberRetailViewModel;
import viewModel.MemberViewModel;

/**
 *
 * @author User
 */
public class MemberRetailService {

    MemberRetailRepo memberRetailRepo = new MemberRetailRepo();
    MemberRepo memberRepo = new MemberRepo();

    public ResultData<MemberRetailViewModel> getData(MemberFilterModel filter) {

        ResultData<MemberRetailViewModel> data = new ResultData<MemberRetailViewModel>();

        try {

            data = memberRetailRepo.getData(filter.getEmail());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(MemberRetailViewModel memberView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
                boolean resultSave = memberRetailRepo.save(memberView);

                resultData.setResult(resultSave);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }
    
    public ResultData<Boolean> checkEmail(MemberFilterModel filter) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String email = filter.getEmail();
            if (memberRepo.hasDataByUserName(email.trim())) {

                if (memberRetailRepo.hasDataByEmail(email.trim())) {
                    //กรณีที่มี retail ดูแลอยู่แล้ว
                    resultData.setResult(false);
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.memberRetail.hasRetail"));

                } else {
                    //กรณีที่ไม่มี retail ดูแล สามารถเพิ่มเข้ามาได้
                    resultData.setResult(true);
                }

            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }
}
