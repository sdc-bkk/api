/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import javax.sql.DataSource;

/**
 *
 * @author Prapaporn
 */
public class ConnnectionDB {

    private static String url;
    private static String user;
    private static String pass;

    public ConnnectionDB() {
        getSettingDB();
    }

    public final void getSettingDB() {

        InputStream inputStream = null;
        Properties prop = new Properties();
        try {

            String propFileName = "/config/config.properties";

            inputStream = getClass().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);

                url = prop.getProperty("con_str");
                user = prop.getProperty("username");
                pass = prop.getProperty("password");

            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

        } catch (Exception e) {

        } finally {

            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {

            }
        }

    }

    public Connection getConnection() {

        Connection conn = null;

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(url, user, pass);
            
        } catch (SQLException e) {
            e.printStackTrace();
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return conn;
    }

    
    public static void main(String[] args) {
        Connection conn = new ConnnectionDB().getConnection();
        String temp = "";
    }

    
}
