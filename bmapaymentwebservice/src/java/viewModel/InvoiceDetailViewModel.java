/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.*;

/**
 *
 * @author User
 */
public class InvoiceDetailViewModel {
    private String detail;//รายการรับ
    private String amount;//จำนวนเงิน (บาท) 

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
