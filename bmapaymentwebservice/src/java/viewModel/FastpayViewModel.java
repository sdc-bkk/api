/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

/**
 *
 * @author User
 */
public class FastpayViewModel {
    private String merchanId;
    private String orderRef;
    private String currCode;
    private double amount;
    private String payType;
    private String secureHashKey;
    private String securityKey;

    public String getMerchanId() {
        return merchanId;
    }

    public void setMerchanId(String merchanId) {
        this.merchanId = merchanId;
    }

    public String getOrderRef() {
        return orderRef;
    }

    public void setOrderRef(String orderRef) {
        this.orderRef = orderRef;
    }

    public String getCurrCode() {
        return currCode;
    }

    public void setCurrCode(String currCode) {
        this.currCode = currCode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getSecureHashKey() {
        return secureHashKey;
    }

    public void setSecureHashKey(String secureHashKey) {
        this.secureHashKey = secureHashKey;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }
}
