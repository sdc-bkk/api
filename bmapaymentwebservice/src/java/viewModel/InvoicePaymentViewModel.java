/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

/**
 *
 * @author User
 */
public class InvoicePaymentViewModel extends InvoiceViewModel{
    
    private String paymentId;
    private String invoiceDate;//วันที่ออกใบเสร็จ 
    
    private String paymentMethod;//วิธีการชำระ (1 =เงินสด, 2 = บัตรเครดิต, 3 = บัตรเดบิต, 4 = ธนาณัติ, 5 =  เช็ค , 6 = QR Code ผ่านเครื่อง EDC)
    private String paymentDate;//วันที่รับชำระ รูปแบบจะเป็น dd/mm/yyyy 
    private String paymentTime;//เวลาที่รับชำระ รูปแบบจะเป็น hh:mm 
    private String amount;//ยอดที่ชำระ 
    private String receiptNo;//เลขที่ใบเสร็จรับเงิน
    private String chequeDate;//วันที่เช็ค กรณีชำระด้วยเช็ค
    private String chequeNo;//เลขที่เช็ค กรณีชำระด้วยเช็ค
    private String chequeBank;//ธนาคาร กรณีชำระด้วยเช็ค
    private String chequeBranch;//สาขา กรณีชำระด้วยเช็ค
    private String receiptLink;//ลิงค์สำหรับแสดงใบเสร็จรับเงิน

    private String onlinePayment;
    private String onlinePaymentMethod;//m_payment.payment_name
    private String onlinePaymentDate;//วันที่รับชำระ
    private String onlinePaymentRef;//หมายเลขอ้างอิงที่ออกโดยระบบ format : yymmdd + tax_type 4 หลัก + เลขรันนิ่ง 12 หลัก(onlinepayment)
    private String onlinePaymentAmount;//ยอดตามใบแจ้งชำระ
    private String onlinePaymentBy;//username ของคน login
    
    private String paymentName;//ชื่อช่องทาง
    private String username;//username ของคน login
    private String email;//ชื่อ email ที่ส่ง
    
    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeBank() {
        return chequeBank;
    }

    public void setChequeBank(String chequeBank) {
        this.chequeBank = chequeBank;
    }

    public String getChequeBranch() {
        return chequeBranch;
    }

    public void setChequeBranch(String chequeBranch) {
        this.chequeBranch = chequeBranch;
    }

    public String getReceiptLink() {
        return receiptLink;
    }

    public void setReceiptLink(String receiptLink) {
        this.receiptLink = receiptLink;
    }

    public String getOnlinePayment() {
        return onlinePayment;
    }

    public void setOnlinePayment(String onlinePayment) {
        this.onlinePayment = onlinePayment;
    }

    public String getOnlinePaymentMethod() {
        return onlinePaymentMethod;
    }

    public void setOnlinePaymentMethod(String onlinePaymentMethod) {
        this.onlinePaymentMethod = onlinePaymentMethod;
    }

    public String getOnlinePaymentDate() {
        return onlinePaymentDate;
    }

    public void setOnlinePaymentDate(String onlinePaymentDate) {
        this.onlinePaymentDate = onlinePaymentDate;
    }

    public String getOnlinePaymentRef() {
        return onlinePaymentRef;
    }

    public void setOnlinePaymentRef(String onlinePaymentRef) {
        this.onlinePaymentRef = onlinePaymentRef;
    }

    public String getOnlinePaymentAmount() {
        return onlinePaymentAmount;
    }

    public void setOnlinePaymentAmount(String onlinePaymentAmount) {
        this.onlinePaymentAmount = onlinePaymentAmount;
    }

    public String getOnlinePaymentBy() {
        return onlinePaymentBy;
    }

    public void setOnlinePaymentBy(String onlinePaymentBy) {
        this.onlinePaymentBy = onlinePaymentBy;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
