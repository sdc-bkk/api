/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class FilterPaymentModel extends BaseClassFilterModel {
    private String paymentId;
    private String transactionId; //invoiceId
    private String receiptNo;//เลขที่ใบเสร็จรับเงิน
    private String taxTypeCode;//รหัสภาษี
    private String taxNo;//เลขประจำตัวผู้เสียภาษี
    private String cusName;//ชื่อผู้ชำระ
    private String ref1;//รหัสอ้างอิง 1
    private String ref2;//รหัสอ้างอิง 2

    private String startPaymentDate;//วันที่เริ่มต้นออกใบเสร็จ 
    private String endPaymentDate;//วันที่สิ้นสุดออกใบเสร็จ  

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getRef1() {
        return ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    public String getRef2() {
        return ref2;
    }

    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }

    public String getStartPaymentDate() {
        return startPaymentDate;
    }

    public void setStartPaymentDate(String startPaymentDate) {
        this.startPaymentDate = startPaymentDate;
    }

    public String getEndPaymentDate() {
        return endPaymentDate;
    }

    public void setEndPaymentDate(String endPaymentDate) {
        this.endPaymentDate = endPaymentDate;
    }

}
