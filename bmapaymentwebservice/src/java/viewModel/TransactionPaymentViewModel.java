/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class TransactionPaymentViewModel {
    private String transactionPayNo;//รหัสที่ใช้กับธนาคาร
    private String ref1New;//ref1 ที่สร้างมาใหม่
    private String ref2New;//ref2 ที่สร้างมาใหม่

    public String getTransactionPayNo() {
        return transactionPayNo;
    }

    public void setTransactionPayNo(String transactionPayNo) {
        this.transactionPayNo = transactionPayNo;
    }

    public String getRef1New() {
        return ref1New;
    }

    public void setRef1New(String ref1New) {
        this.ref1New = ref1New;
    }

    public String getRef2New() {
        return ref2New;
    }

    public void setRef2New(String ref2New) {
        this.ref2New = ref2New;
    }

    
}
