/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import repository.mapper.PaymentMapper;
import repository.mapper.ResultPaymentMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.FilterPaymentModel;
import viewModel.InvoicePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.ResultPage;
import viewModel.ResultPaymentViewModel;

/**
 *
 * @author User
 */
public class PaymentRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData saveData(InvoicePaymentModel data) throws SQLException {

        ResultData resultData = new ResultData();
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        boolean result = false;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";
//insert select
            //<editor-fold defaultstate="collapsed" desc="insert into payment from invoice">
            sql = "INSERT INTO INVOICE_PAYMENT ( "
                    + "  BILL_NO "
                    + ", REF1 "
                    + ", REF2 "
                    + ", TAX_NO "
                    + ", CUS_NAME "
                    + ", DUE_DATE "
                    + ", DETAIL1 "
                    + ", AMOUNT1 "
                    + ", DETAIL2 "
                    + ", AMOUNT2 "
                    + ", DETAIL3 "
                    + ", AMOUNT3 "
                    + ", DETAIL4 "
                    + ", AMOUNT4 "
                    + ", DETAIL5 "
                    + ", AMOUNT5 "
                    + ", TOTAL_AMOUNT "
                    + ", OFFICE "
                    + ", PART_OF_OFFICE "
                    + ", TELEPHONE"
                    + ", TAX_TYPE_CODE "
                    + ", TAX_TYPE_NAME "
                    + ", INVOICE_DATE "
                    + ", TRANSACTION_ID "
                    + ", BMA_BILL_NO "
                    + ", BARCODE "
                    + ", QRCODE "
                    + ", PAYMENT_STATUS "
                    + ", PAYMENT_METHOD "
                    + ", PAYMENT_DATE "
                    + ", PAYMENT_TIME "
                    + ", PAYMENT_AMOUNT "
                    + ", RECEIPT_NO"
                    + ", CHEQUE_DATE "
                    + ", CHEQUE_NO "
                    + ", CHEQUE_BANK "
                    + ", CHEQUE_BRANCH "
                    + ") "
                    + " SELECT BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, DUE_DATE"
                    + ", DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5 "
                    + ", TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME "
                    + ", ACTION_DATE, INVOICE_ID, BMA_BILL_NO, BARCODE, QRCODE "
                    + ", ?, ?, ?, ?, ? "
                    + ", ?, ? , ?, ?, ? "
                    + " FROM INVOICE "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_NO = ? ";

            ps = conn.prepareStatement(sql);

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getPaymentStatus());
            ps.setString(i++, data.getPaymentMethod());
            ps.setDate(i++, AppUtil.toDateSql(data.getPaymentDate()));
            ps.setString(i++, data.getPaymentTime());
            ps.setDouble(i++, data.getAmount());

            ps.setString(i++, data.getReceiptNo());
            ps.setDate(i++, AppUtil.toDateSql(data.getChequeDate()));
            ps.setString(i++, data.getChequeNo());
            ps.setString(i++, data.getChequeBank());
            ps.setString(i++, data.getChequeBranch());

            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());
            ps.setString(i++, data.getTaxNo());

            result = ps.executeUpdate() != 0;

//            ResultSet rs = ps.getGeneratedKeys();
//            int id = 0;
//            while (rs.next()) {
//                id = rs.getInt(1);
//            }
//
//            if (id <= 0) {
//                throw new Exception();
//            }
//
//            data.setInvoiceId(id);
            //</editor-fold>
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultPaymentViewModel getData(String bmaBillNo) throws SQLException {

        ResultData<ResultPaymentViewModel> resultData = new ResultData<ResultPaymentViewModel>();
        ResultPaymentViewModel data = null;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM INVOICE_PAYMENT "
                    + " WHERE BMA_BILL_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, bmaBillNo);

            rs = ps.executeQuery();

            data = new ResultPaymentMapper(rs).mapFull();

//            resultData.setResult(data);
//            resultData.setStatus("true");
        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return data;
//        return resultData;

    }
    
    
    ///////////// bma-back /////////////////
    public ResultData<List<InvoicePaymentViewModel>> getList(ResultPage page, FilterPaymentModel filter) throws SQLException {

        ResultData<List<InvoicePaymentViewModel>> resultData = new ResultData<List<InvoicePaymentViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT PAYMENT_ID , TRANSACTION_ID , RECEIPT_NO , TAX_TYPE_CODE  , TAX_TYPE_NAME , TAX_NO , CUS_NAME , "
                        + "    TOTAL_AMOUNT , PAYMENT_DATE , PAYMENT_STATUS , PAYMENT_METHOD "
                        + " FROM INVOICE_PAYMENT P "
                        + " WHERE CONFIRM_PAYMENT = '1' "
                        //+ " WHERE (1=1) "
                        ; 
            //PAYMENT_STATUS (0=สร้าง 1=รอตรวจ 2=ไม่ผ่าน 3=ผ่าน)
/*
 SELECT PAYMENT_ID ,  RECEIPT_NO , TAX_TYPE_CODE  , TAX_TYPE_NAME , TAX_NO , CUS_NAME , 
    TOTAL_AMOUNT , PAYMENT_DATE , PAYMENT_STATUS , PAYMENT_METHOD 
 FROM INVOICE_PAYMENT P 
 WHERE CONFIRM_PAYMENT = '1' 
 ORDER BY PAYMENT_DATE DESC , RECEIPT_NO 
*/
            if (!AppUtil.isNullAndSpace(filter.getReceiptNo())) {
                sql += " AND (RECEIPT_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxTypeCode())) {
                sql += " AND (TAX_TYPE_CODE = ?) ";
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStartPaymentDate())) {
                sql += " AND ( TRUNC(PAYMENT_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndPaymentDate())) {
                sql += " AND ( TRUNC(PAYMENT_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            
            ps.setSql(sql);

            String orderBy = " PAYMENT_DATE DESC , RECEIPT_NO ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getReceiptNo())) {
                ps.setString(i++, "%" + filter.getReceiptNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxTypeCode())) {
                ps.setString(i++, filter.getTaxTypeCode());
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStartPaymentDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartPaymentDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndPaymentDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndPaymentDate()));
            }
            
            rs = ps.executeQuery();

            List<InvoicePaymentViewModel> dataList = null;
            dataList = new PaymentMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMsg(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }
    
    
    ///////////// bma-front /////////////////
    public ResultData<List<InvoicePaymentViewModel>> getListFront(ResultPage page, FilterPaymentModel filter) throws SQLException {

        ResultData<List<InvoicePaymentViewModel>> resultData = new ResultData<List<InvoicePaymentViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT PAYMENT_ID ,TRANSACTION_ID , RECEIPT_NO , TAX_TYPE_CODE , TAX_TYPE_NAME , TAX_NO , CUS_NAME , "
                        + "    REF1 , REF2 , TOTAL_AMOUNT , PAYMENT_DATE , PAYMENT_TIME , PAYMENT_STATUS , PAYMENT_METHOD "
                        + " FROM INVOICE_PAYMENT P "
                        + " WHERE PAYMENT_STATUS IN (1,2,3,99) AND CONFIRM_PAYMENT = '1' "
                        //+ " WHERE PAYMENT_STATUS IN (1,2,3,99) "
                        ; 
            //PAYMENT_STATUS (0=สร้าง 1=รอตรวจ 2=ไม่ผ่าน 3=ผ่าน)
/*
 SELECT PAYMENT_ID , RECEIPT_NO , TAX_TYPE_CODE , TAX_TYPE_NAME , TAX_NO , CUS_NAME , 
    REF1 , REF2 , TOTAL_AMOUNT , PAYMENT_DATE , PAYMENT_TIME , PAYMENT_STATUS , PAYMENT_METHOD 
 FROM INVOICE_PAYMENT P 
 WHERE PAYMENT_STATUS IN (1,2,3,99) AND CONFIRM_PAYMENT = '1' 
    AND TAX_NO = '0103551029112' 
 ORDER BY PAYMENT_DATE DESC , PAYMENT_TIME 
*/
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (TAX_NO = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxTypeCode())) {
                sql += " AND (TAX_TYPE_CODE = ?) ";
            }
            
            if (!AppUtil.isNullAndSpace(filter.getRef1())) {
                sql += " AND (REF1 LIKE ?) ";
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStartPaymentDate())) {
                sql += " AND ( TRUNC(PAYMENT_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndPaymentDate())) {
                sql += " AND ( TRUNC(PAYMENT_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
            
            ps.setSql(sql);

            String orderBy = " PAYMENT_DATE DESC , PAYMENT_TIME ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxTypeCode())) {
                ps.setString(i++, filter.getTaxTypeCode());
            }
            
            if (!AppUtil.isNullAndSpace(filter.getRef1())) {
                ps.setString(i++, "%" + filter.getRef1() + "%");
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStartPaymentDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartPaymentDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndPaymentDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndPaymentDate()));
            }
            
            rs = ps.executeQuery();

            List<InvoicePaymentViewModel> dataList = null;
            dataList = new PaymentMapper(rs).mapListFront();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMsg(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }
    
    public ResultData<InvoicePaymentViewModel> getDataForSendMailByTransactionId(int id) throws SQLException {

        ResultData<InvoicePaymentViewModel> resultData = new ResultData<InvoicePaymentViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get INVOICE_PAYMENT ">
            String sql = " SELECT P.PAYMENT_ID, P.TRANSACTION_ID, P.REF1, P.CUS_NAME, P.TAX_NO, P.ONLINE_PAYMENT_BY EMAIL, " //M.EMAIL, 
                        + "    P.ONLINE_PAYMENT_AMOUNT, P.ONLINE_PAYMENT_DATE, P.ONLINE_PAYMENT_REF, P.ONLINE_PAYMENT_METHOD "
                        + " FROM INVOICE_PAYMENT P "
                        //+ "    LEFT JOIN MEMBER_OF_RETAIL M ON P.TAX_NO = M.TAX_NO "
                        + " WHERE P.TRANSACTION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            InvoicePaymentViewModel data = new PaymentMapper(rs).mapDataForSendMail();
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

}
