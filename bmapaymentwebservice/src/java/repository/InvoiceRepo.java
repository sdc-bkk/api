/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.PaymentStatusEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import model.InvoiceModel;
import model.InvoiceTransactionPayModel;
import repository.mapper.InvoiceMapper;
import repository.mapper.ResultPaymentMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import utility.SHA512;
import viewModel.DrpDownViewModel;
import viewModel.FastpayViewModel;
import viewModel.FilterInvoiceModel;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.ResultPage;
import viewModel.ResultPaymentViewModel;
import viewModel.TransactionPaymentViewModel;

/**
 *
 * @author User
 */
public class InvoiceRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData sendExternalInvoice(InvoiceModel data) throws SQLException {

        ResultData resultData = new ResultData();
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        boolean result = false;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Delete by id ">
            sql = " DELETE FROM INVOICE "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_TYPE_CODE = ? ";

            i = 1;
            ps = conn.prepareStatement(sql);

            ps.setString(1, data.getRef1());
            ps.setString(2, data.getRef2());
            ps.setString(3, data.getTaxTypeCode());

            ps.executeUpdate();
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="invoice">
            if (data.getInvoiceId() == 0) {

                sql = "INSERT INTO INVOICE ( "
                        + "  BILL_NO "
                        + ", REF1 "
                        + ", REF2 "
                        + ", TAX_NO "
                        + ", CUS_NAME "
                        + ", DUE_DATE "
                        + ", DETAIL1 "
                        + ", AMOUNT1 "
                        + ", DETAIL2 "
                        + ", AMOUNT2 "
                        + ", DETAIL3 "
                        + ", AMOUNT3 "
                        + ", DETAIL4 "
                        + ", AMOUNT4 "
                        + ", DETAIL5 "
                        + ", AMOUNT5 "
                        + ", TOTAL_AMOUNT "
                        + ", OFFICE "
                        + ", PART_OF_OFFICE "
                        + ", TELEPHONE"
                        + ", TAX_TYPE_CODE "
                        + ", TAX_TYPE_NAME "
                        + ", ACTION_DATE "
                        + ", PAYMENT_STATUS "
                        + ", BARCODE "
                        + ", QRCODE "
                        + ", BMA_BILL_NO "
                        + ") VALUES(?,?,?,?,?,?  ,?,?,?,?,?,?,?,?,?,?,?  ,?,?,?  ,?,?,SYSDATE   ,0,?,?,?) ";

                ps = conn.prepareStatement(sql, new String[]{"INVOICE_ID"});

            } else {

                sql = "UPDATE INVOICE SET "
                        + "  BILL_NO = ? "
                        + ", REF1 = ? "
                        + ", REF2 = ? "
                        + ", TAX_NO = ? "
                        + ", CUS_NAME = ? "
                        + ", DUE_DATE = ?"
                        + ", DETAIL1 = ? "
                        + ", AMOUNT1 = ? "
                        + ", DETAIL2 = ? "
                        + ", AMOUNT2 = ? "
                        + ", DETAIL3 = ? "
                        + ", AMOUNT3 = ? "
                        + ", DETAIL4 = ? "
                        + ", AMOUNT4 = ? "
                        + ", DETAIL5 = ? "
                        + ", AMOUNT5 = ? "
                        + ", TOTAL_AMOUNT = ? "
                        + ", OFFICE = ? "
                        + ", PART_OF_OFFICE = ? "
                        + ", TELEPHONE = ?"
                        + ", TAX_TYPE_CODE = ? "
                        + ", TAX_TYPE_NAME = ? "
                        + ", ACTION_DATE = SYSDATE "
                        + ", BARCODE  = ?"
                        + ", QRCODE  = ?"
                        + " WHERE INVOICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getBillNo());
            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());
            ps.setString(i++, data.getTaxNo());
            ps.setString(i++, data.getCusName());
            ps.setDate(i++, AppUtil.toDateSql(data.getDueDate()));

            ps.setString(i++, data.getDetail1());
            ps.setDouble(i++, data.getAmount1());
            ps.setString(i++, data.getDetail2());
            ps.setDouble(i++, data.getAmount2());
            ps.setString(i++, data.getDetail3());
            ps.setDouble(i++, data.getAmount3());
            ps.setString(i++, data.getDetail4());
            ps.setDouble(i++, data.getAmount4());
            ps.setString(i++, data.getDetail5());
            ps.setDouble(i++, data.getAmount5());
            ps.setDouble(i++, data.getTotalAmount());

            ps.setString(i++, data.getOffice());
            ps.setString(i++, data.getPartOfOffice());
            ps.setString(i++, data.getTelephone());

            ps.setString(i++, data.getTaxTypeCode());
            ps.setString(i++, data.getTaxTypeName());

            ps.setString(i++, data.getBarcode());
            ps.setString(i++, data.getQrcode());

            if (data.getInvoiceId() == 0) {
                ps.setString(i++, data.getBmaBillNo());

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setInvoiceId(id);
            } else {

                ps.setInt(i++, data.getInvoiceId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            conn.commit();
            dataOut.setBmaBillNo(data.getBmaBillNo());
            dataOut.setTransactionId(AppUtil.encryptId(data.getInvoiceId()));

            resultData.setResult(dataOut);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(dataOut);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public boolean deleteData(InvoiceViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id ">
            sql = " DELETE FROM INVOICE "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_TYPE_CODE = ? ";

            i = 1;
            ps.setSql(sql);

            ps.setString(1, data.getRef1());
            ps.setString(2, data.getRef2());
            ps.setString(3, data.getTaxTypeCode());

            result = ps.executeUpdate();
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public ResultData<InvoiceViewModel> getDataFullByBmaBillNo(String bmaBillNo) throws SQLException {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT REF1,REF2 FROM INVOICET "
                    + " WHERE BMA_BILL_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, bmaBillNo);

            rs = ps.executeQuery();

            InvoiceViewModel data = new InvoiceMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<ResultPaymentViewModel> getDataByBmaBillNo(String bmaBillNo, ResultPaymentViewModel data) throws SQLException {

        ResultData<ResultPaymentViewModel> resultData = new ResultData<ResultPaymentViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT REF1,REF2 FROM INVOICE "
                    + " WHERE BMA_BILL_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, bmaBillNo);

            rs = ps.executeQuery();

            data = new InvoiceMapper(rs).mapInvoicePayment();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public int getInvoiceIdByRef(InvoiceViewModel data) throws SQLException {

        int invoiceId = 0;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get InvoiceId">
            String sql = " SELECT INVOICE_ID "
                    + " FROM INVOICE  "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_TYPE_CODE = ? ";

            ps.setSql(sql);

            ps.setString(1, data.getRef1());
            ps.setString(2, data.getRef2());
            ps.setString(3, data.getTaxTypeCode());

            rs = ps.executeQuery();

            while (rs.next()) {
                invoiceId = rs.getInt("INVOICE_ID");
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return invoiceId;

    }

    ///////////// bma-back /////////////////
    public ResultData<List<InvoiceViewModel>> getList(ResultPage page, FilterInvoiceModel filter) throws SQLException {

        ResultData<List<InvoiceViewModel>> resultData = new ResultData<List<InvoiceViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT INVOICE_ID ,  BILL_NO , TAX_TYPE_CODE  , TAX_TYPE_NAME , TAX_NO , CUS_NAME , "
                    + "    TOTAL_AMOUNT , DUE_DATE , ACTION_DATE , PAYMENT_STATUS , IS_CANCEL "
                    + " FROM INVOICE I "
                    + " WHERE (1=1) ";
            // 0 สร้าง 1 รอตรวจ 2 ไม่ผ่าน 3 ผ่าน
/*
 SELECT INVOICE_ID ,  BILL_NO , TAX_TYPE_CODE  , TAX_TYPE_NAME , TAX_NO , CUS_NAME , 
    TOTAL_AMOUNT , DUE_DATE , ACTION_DATE , PAYMENT_STATUS , IS_CANCEL 
 FROM INVOICE I 
 ORDER BY ACTION_DATE DESC , BILL_NO 
             */
            if (!AppUtil.isNullAndSpace(filter.getBillNo())) {
                sql += " AND (BILL_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxTypeCode())) {
                sql += " AND (TAX_TYPE_CODE = ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (TAX_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getCusName())) {
                sql += " AND (CUS_NAME LIKE ?) ";
            }

            /*
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND ( TRUNC(TAX_FORM01_RETAIL.DOC_DATE) >= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND ( TRUNC(TAX_FORM01_RETAIL.DOC_DATE) <= TRUNC(TO_DATE (?, 'dd/mm/yyyy'))) ";
            }
             */
            ps.setSql(sql);

            String orderBy = " ACTION_DATE DESC , BILL_NO ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getBillNo())) {
                ps.setString(i++, "%" + filter.getBillNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxTypeCode())) {
                ps.setString(i++, filter.getTaxTypeCode());
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, "%" + filter.getTaxNo() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getCusName())) {
                ps.setString(i++, "%" + filter.getCusName() + "%");
            }

            /*
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }
             */
            rs = ps.executeQuery();

            List<InvoiceViewModel> dataList = null;
            dataList = new InvoiceMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMsg(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<InvoiceViewModel> getData(int id) throws SQLException {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get INVOICE ">
            String sql = " SELECT *  "
                    + " FROM INVOICE I "
                    + " WHERE INVOICE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            InvoiceViewModel data = new InvoiceMapper(rs).mapDataInvoice();
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public Boolean duplicateData(FilterInvoiceModel filter) throws SQLException {

        Boolean isDup = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get Invoice ">
            String sql = " SELECT INVOICE_ID "
                    + " FROM INVOICE "
                    + " WHERE TAX_NO = ? AND TAX_TYPE_CODE = ? "
                    + "    AND REF1 = ? AND REF2 = ? ";

            ps.setSql(sql);

            ps.setString(1, filter.getTaxNo());
            ps.setString(2, filter.getTaxTypeCode());
            ps.setString(3, filter.getRef1());
            ps.setString(4, filter.getRef2());

            rs = ps.executeQuery();

            while (rs.next()) {
                isDup = true;
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return isDup;

    }

    public ResultData saveData(InvoiceModel data) throws SQLException {

        boolean result = false;
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql INVOICE">
            if (data.getInvoiceId() == 0) {

                sql = "INSERT INTO INVOICE ( "
                        + "  BILL_NO "
                        + ", REF1 "
                        + ", REF2 "
                        + ", TAX_NO "
                        + ", CUS_NAME "
                        + ", CUS_ADDRESS "
                        + ", DUE_DATE "
                        + ", DETAIL1 "
                        + ", AMOUNT1 "
                        + ", DETAIL2 "
                        + ", AMOUNT2 "
                        + ", DETAIL3 "
                        + ", AMOUNT3 "
                        + ", DETAIL4 "
                        + ", AMOUNT4 "
                        + ", DETAIL5 "
                        + ", AMOUNT5 "
                        + ", TOTAL_AMOUNT "
                        + ", OFFICE "
                        + ", PART_OF_OFFICE "
                        + ", TELEPHONE "
                        + ", TAX_TYPE_CODE "
                        + ", TAX_TYPE_NAME "
                        + ", ACTION_DATE "
                        + ", BARCODE "
                        + ", QRCODE "
                        + ", IS_CANCEL "
                        + ", PAYMENT_STATUS "
                        + ", BMA_BILL_NO "
                        + ") VALUES (?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?, ?,?,?,?, ?,?,SYSDATE,?,?,?, ?,?) ";

                ps = conn.prepareStatement(sql, new String[]{"INVOICE_ID"});

            } else {

                sql = "UPDATE INVOICE SET "
                        + "  BILL_NO = ? "
                        + ", REF1 = ? "
                        + ", REF2 = ? "
                        + ", TAX_NO = ? "
                        + ", CUS_NAME = ? "
                        + ", CUS_ADDRESS = ? "
                        + ", DUE_DATE = ?"
                        + ", DETAIL1 = ? "
                        + ", AMOUNT1 = ? "
                        + ", DETAIL2 = ? "
                        + ", AMOUNT2 = ? "
                        + ", DETAIL3 = ? "
                        + ", AMOUNT3 = ? "
                        + ", DETAIL4 = ? "
                        + ", AMOUNT4 = ? "
                        + ", DETAIL5 = ? "
                        + ", AMOUNT5 = ? "
                        + ", TOTAL_AMOUNT = ? "
                        + ", OFFICE = ? "
                        + ", PART_OF_OFFICE = ? "
                        + ", TELEPHONE = ?"
                        + ", TAX_TYPE_CODE = ? "
                        + ", TAX_TYPE_NAME = ? "
                        + ", ACTION_DATE = SYSDATE "
                        + ", BARCODE  = ?"
                        + ", QRCODE  = ?"
                        + ", IS_CANCEL  = ?"
                        + " WHERE INVOICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getBillNo()); //BILL_NO
            ps.setString(i++, data.getRef1()); //REF1
            ps.setString(i++, data.getRef2()); //REF2
            ps.setString(i++, data.getTaxNo()); //TAX_NO
            ps.setString(i++, data.getCusName()); //CUS_NAME
            ps.setString(i++, data.getCusAddress()); //CUS_ADDRESS
            ps.setDate(i++, AppUtil.toDateSql(data.getDueDate())); //DUE_DATE

            double totalAmount = 0;
            if (!AppUtil.isNullAndSpace(data.getDetail1())) {
                ps.setString(i++, data.getDetail1()); //DETAIL1
                ps.setDouble(i++, data.getAmount1()); //AMOUNT1
                totalAmount = totalAmount + data.getAmount1();
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR); //DETAIL1
                ps.setNull(i++, java.sql.Types.DOUBLE); //AMOUNT1
            }
            if (!AppUtil.isNullAndSpace(data.getDetail2())) {
                ps.setString(i++, data.getDetail2()); //DETAIL2
                ps.setDouble(i++, data.getAmount2()); //AMOUNT2
                totalAmount = totalAmount + data.getAmount2();
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR); //DETAIL2
                ps.setNull(i++, java.sql.Types.DOUBLE); //AMOUNT2
            }
            if (!AppUtil.isNullAndSpace(data.getDetail3())) {
                ps.setString(i++, data.getDetail3()); //DETAIL3
                ps.setDouble(i++, data.getAmount3()); //AMOUNT3
                totalAmount = totalAmount + data.getAmount3();
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR); //DETAIL3
                ps.setNull(i++, java.sql.Types.DOUBLE); //AMOUNT3
            }
            if (!AppUtil.isNullAndSpace(data.getDetail4())) {
                ps.setString(i++, data.getDetail4()); //DETAIL4
                ps.setDouble(i++, data.getAmount4()); //AMOUNT4
                totalAmount = totalAmount + data.getAmount4();
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR); //DETAIL4
                ps.setNull(i++, java.sql.Types.DOUBLE); //AMOUNT4
            }
            if (!AppUtil.isNullAndSpace(data.getDetail5())) {
                ps.setString(i++, data.getDetail5()); //DETAIL5
                ps.setDouble(i++, data.getAmount5()); //AMOUNT5
                totalAmount = totalAmount + data.getAmount5();
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR); //DETAIL5
                ps.setNull(i++, java.sql.Types.DOUBLE); //AMOUNT5
            }
            /*
            for (int j = 0; j < 5; j++) {
                if(data.getDetailList() != null && data.getDetailList().size() == (j=1)){
                    ps.setString(i++, data.getDetailList().get(j).getDetail()); //DETAIL1
                    ps.setDouble(i++, AppUtil.convertStringToDouble(data.getDetailList().get(j).getAmount())); //AMOUNT1
                    totalAmount = totalAmount + AppUtil.convertStringToDouble(data.getDetailList().get(j).getAmount());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR); //DETAIL1
                    ps.setNull(i++, java.sql.Types.DOUBLE); //AMOUNT1
                }
            }
             */

            if (data.getTotalAmount() > 0) {
                ps.setDouble(i++, data.getTotalAmount()); //TOTAL_AMOUNT
            } else {
                ps.setDouble(i++, totalAmount); //TOTAL_AMOUNT
            }
            ps.setString(i++, data.getOffice()); //OFFICE
            ps.setString(i++, data.getPartOfOffice()); //PART_OF_OFFICE
            ps.setString(i++, data.getTelephone()); //TELEPHONE

            ps.setString(i++, data.getTaxTypeCode()); //TAX_TYPE_CODE
            ps.setString(i++, data.getTaxTypeName()); //TAX_TYPE_NAME
            ps.setString(i++, data.getBarcode()); //BARCODE
            ps.setString(i++, data.getQrcode()); //QRCODE
            ps.setString(i++, "0"); //IS_CANCEL (0=ใช้งาน , 1=ยกเลิก)

            if (data.getInvoiceId() == 0) {
                ps.setString(i++, "0"); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
                ps.setString(i++, data.getBmaBillNo()); //BMA_BILL_NO

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setInvoiceId(id);
            } else {
                ps.setInt(i++, data.getInvoiceId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            conn.commit();

            dataOut.setBmaBillNo(data.getBmaBillNo());
            dataOut.setTransactionId(AppUtil.encryptId(data.getInvoiceId()));

            resultData.setResult(dataOut);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(dataOut);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData cancelInvoice(FilterInvoiceModel filter) throws SQLException {
        boolean result = false;
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="update INVOICE cancel=1 ">
            sql = " UPDATE INVOICE SET IS_CANCEL = ? "
                    + " WHERE INVOICE_ID = ? ";

            ps.setSql(sql);

            ps.setString(i++, "1"); //IS_CANCEL (0=ใช้งาน , 1=ยกเลิก)
            ps.setInt(i++, AppUtil.decryptId(filter.getInvoiceId()));

            result = ps.executeUpdate();

            //</editor-fold>
            ps.commit();

            dataOut.setTransactionId(filter.getInvoiceId());
            resultData.setResult(dataOut);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(dataOut);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListTaxType() throws SQLException {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT TAX_TYPE_ID ,  TAX_TYPE_CODE , TAX_TYPE_NAME  , IS_ACTIVE , TAX_TYPE_FULL_NAME "
                    + " FROM M_TAX_TYPE T "
                    + " WHERE IS_ACTIVE = 1 ";
            // 0 สร้าง 1 รอตรวจ 2 ไม่ผ่าน 3 ผ่าน
/*
 SELECT TAX_TYPE_ID ,  TAX_TYPE_CODE , TAX_TYPE_NAME  , IS_ACTIVE , TAX_TYPE_FULL_NAME  
 FROM M_TAX_TYPE T 
 WHERE IS_ACTIVE = 1
 ORDER BY TAX_TYPE_NAME 
             */

            ps.setSql(sql);

            String orderBy = " TAX_TYPE_CODE , TAX_TYPE_NAME ";
            ps.setOrderBy(orderBy);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataList = null;
            dataList = new InvoiceMapper(rs).mapDrpListTaxType();

            resultData.setResult(dataList);

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMsg(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    ///////////// bma-front /////////////////
    public ResultData<List<InvoiceViewModel>> getListFront(ResultPage page, FilterInvoiceModel filter) throws SQLException {

        ResultData<List<InvoiceViewModel>> resultData = new ResultData<List<InvoiceViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT INVOICE_ID ,  BILL_NO , I.TAX_TYPE_CODE  , MT.DISPLAY_NAME TAX_TYPE_NAME , TAX_NO , CUS_NAME , "
                    + "    DETAIL1 , AMOUNT1 , DETAIL2 , AMOUNT2 , DETAIL3 , AMOUNT3 , DETAIL4 , AMOUNT4 , "
                    + "    DETAIL5 , AMOUNT5 , TOTAL_AMOUNT , REF1 , REF2 , BMA_BILL_NO , BARCODE , QRCODE , "
                    + "    DUE_DATE , ACTION_DATE , PAYMENT_STATUS , IS_CANCEL "
                    + " FROM INVOICE I "
                    + " LEFT JOIN M_TAX_TYPE MT ON I.TAX_TYPE_CODE = MT.TAX_TYPE_CODE"
                    + " WHERE PAYMENT_STATUS = '0' AND (IS_CANCEL <> '1' OR IS_CANCEL IS NULL) ";
            // PAYMENT_STATUS (0=สร้าง 1=รอตรวจ 2=ไม่ผ่าน 3=ผ่าน)
/*
 SELECT INVOICE_ID ,  BILL_NO , TAX_TYPE_CODE  , TAX_TYPE_NAME , TAX_NO , CUS_NAME , 
    DETAIL1 , AMOUNT1 , DETAIL2 , AMOUNT2 , DETAIL3 , AMOUNT3 , DETAIL4 , AMOUNT4 , 
    DETAIL5 , AMOUNT5 , TOTAL_AMOUNT , REF1 , REF2 , BMA_BILL_NO , BARCODE , QRCODE , 
    DUE_DATE , ACTION_DATE , PAYMENT_STATUS , IS_CANCEL 
 FROM INVOICE I 
 WHERE PAYMENT_STATUS = '0' AND (IS_CANCEL <> '1' OR IS_CANCEL IS NULL) 
 ORDER BY ACTION_DATE DESC , BILL_NO
             */
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (TAX_NO = ?) ";
            }

            ps.setSql(sql);

            String orderBy = " ACTION_DATE DESC , BILL_NO ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            rs = ps.executeQuery();

            List<InvoiceViewModel> dataList = null;
            dataList = new InvoiceMapper(rs).mapListFront();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMsg(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData saveDataFront(List<InvoiceModel> invoiceIdList, String paymentName, String username) throws SQLException {

        boolean result = false;
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";
            String transactionNo = "";
            List<InvoicePaymentViewModel> invoicePaymentList = new ArrayList<InvoicePaymentViewModel>();

            for (InvoiceModel invoice : invoiceIdList) {

                if (invoice != null && invoice.getInvoiceId() > 0) {

                    InvoicePaymentViewModel invoicePayment = new InvoicePaymentViewModel();
                    //<editor-fold defaultstate="collapsed" desc="sql insert INVOICE_PAYMENT">
                    sql = " SELECT TRANSACTION_PAY_NO FROM INVOICE_TRANS_TEMP "
                            + " WHERE INVOICE_ID = ? ";

                    ps = conn.prepareStatement(sql);
                    ps.setInt(1, invoice.getInvoiceId());

                    ResultSet rs = ps.executeQuery();

                    while (rs.next()) {
                        transactionNo = rs.getString("TRANSACTION_PAY_NO");
                    }
                    //</editor-fold>
                    //update paymentStatus = 99 (รอตรวจสอบยอดเงิน) ที่ตาราง invoice
                    //<editor-fold defaultstate="collapsed" desc="sql update INVOICE">
                    i = 1;
                    sql = " UPDATE INVOICE SET "
                            + "    PAYMENT_STATUS = ? "
                            + " WHERE INVOICE_ID = ? ";

                    ps = conn.prepareStatement(sql);

                    ps.setString(i++, String.valueOf(PaymentStatusEnum.Pending.value())); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค, 99 = รอตรวจสอบยอดเงิน)
                    ps.setInt(i++, invoice.getInvoiceId());

                    result = ps.executeUpdate() != 0;

                    //</editor-fold>
                    //insert into invoice_payment จาก invoice โดยใส่ค่าพวก online เพิ่มมาด้วย
                    //<editor-fold defaultstate="collapsed" desc="sql insert INVOICE_PAYMENT">
                    i = 1;
                    sql = " INSERT INTO INVOICE_PAYMENT ( "
                            + "  BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, CUS_ADDRESS, DUE_DATE, "
                            + "  DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5, "
                            + "  TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME, "
                            + "  INVOICE_DATE, TRANSACTION_ID, BARCODE, QRCODE, BMA_BILL_NO, PAYMENT_STATUS, "
                            + "  PAYMENT_METHOD, PAYMENT_DATE, PAYMENT_AMOUNT, RECEIPT_NO, "
                            + "  CHEQUE_DATE, CHEQUE_NO, CHEQUE_BANK, CHEQUE_BRANCH, IS_ONLINE_PAYMENT, "
                            + "  ONLINE_PAYMENT_METHOD, ONLINE_PAYMENT_DATE, ONLINE_PAYMENT_REF, ONLINE_PAYMENT_AMOUNT, ONLINE_PAYMENT_BY, "
                            + "  PAYMENT_TIME, CONFIRM_PAYMENT, INVOICE_NO ) "
                            + " SELECT BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, CUS_ADDRESS, DUE_DATE, "
                            + "  DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5, "
                            + "  TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME, "
                            + "  ACTION_DATE, INVOICE_ID, BARCODE, QRCODE, BMA_BILL_NO, ?, "
                            + "  NULL, NULL, NULL, NULL, "
                            + "  NULL, NULL, NULL, NULL, ?, "
                            + "  ?, SYSDATE, ?, TOTAL_AMOUNT, ?, "
                            + "  NULL, ?, NULL "
                            + " FROM INVOICE "
                            + " WHERE INVOICE_ID = ? ";

                    ps = conn.prepareStatement(sql); //, new String[]{"PAYMENT_ID"}

                    ps.setString(i++, String.valueOf(PaymentStatusEnum.Pending.value())); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค, 99 = รอตรวจสอบยอดเงิน)
                    ps.setString(i++, "1"); //IS_ONLINE_PAYMENT = 1
                    ps.setString(i++, AppUtil.checkNullData(paymentName)); //ONLINE_PAYMENT_METHOD = m_payment.payment_name
                    ps.setString(i++, transactionNo);

                    //-------------- ของเดิม ที่ออกแบบไว้ -----------------
                    // ('640817' || NVL(TAX_TYPE_CODE,'') || '000000000000') 
//                Locale lc = new Locale("th", "TH");
//                DateFormat df = new SimpleDateFormat("yyMMdd", lc); // Just the year, with 2 digits
//                String yymmdd = df.format(Calendar.getInstance(lc).getTime());
                    //ps.setString(i++, yymmdd); //ONLINE_PAYMENT_REF 1 = หมายเลขอ้างอิงที่ออกโดยระบบ 
                    ////เลขรันนิ่ง 12 หลัก (onlinepayment)
                    //String onlinepaymentRunning = AppUtil.getRunningDoc("onlinepayment", "000000000000");
                    ////format : yymmdd + tax_type 4 หลัก + เลขรันนิ่ง 12 หลัก (onlinepayment)
                    //ps.setString(i++, onlinepaymentRunning); //ONLINE_PAYMENT_REF 2 = หมายเลขอ้างอิงที่ออกโดยระบบ                 
                    //-------------- ของเดิม ที่ออกแบบไว้ -----------------
                    ps.setString(i++, AppUtil.checkNullData(username)); //ONLINE_PAYMENT_BY
                    ps.setString(i++, "0"); //CONFIRM_PAYMENT = false = 0 เพราะต้องรอตรวจสอบจากไฟล์ธนาคารอีกที

                    ps.setInt(i++, invoice.getInvoiceId());

                    result = ps.executeUpdate() != 0;
                    //</editor-fold>
//                    //<editor-fold defaultstate="collapsed" desc="sql insert INVOICE_PAYMENT">
//                    i = 1;
//                    sql = " INSERT INTO INVOICE_PAYMENT ( "
//                            + "  BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, CUS_ADDRESS, DUE_DATE, "
//                            + "  DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5, "
//                            + "  TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME, "
//                            + "  INVOICE_DATE, TRANSACTION_ID, BARCODE, QRCODE, BMA_BILL_NO, PAYMENT_STATUS, "
//                            + "  PAYMENT_METHOD, PAYMENT_DATE, PAYMENT_AMOUNT, RECEIPT_NO, "
//                            + "  CHEQUE_DATE, CHEQUE_NO, CHEQUE_BANK, CHEQUE_BRANCH, IS_ONLINE_PAYMENT, "
//                            + "  ONLINE_PAYMENT_METHOD, ONLINE_PAYMENT_DATE, ONLINE_PAYMENT_REF, ONLINE_PAYMENT_AMOUNT, ONLINE_PAYMENT_BY, "
//                            + "  PAYMENT_TIME, CONFIRM_PAYMENT, INVOICE_NO ) "
//                            + " SELECT BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, CUS_ADDRESS, DUE_DATE, "
//                            + "  DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5, "
//                            + "  TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME, "
//                            + "  ACTION_DATE, INVOICE_ID, BARCODE, QRCODE, BMA_BILL_NO, ?, "
//                            + "  NULL, NULL, NULL, NULL, "
//                            + "  NULL, NULL, NULL, NULL, ?, "
//                            + "  ?, SYSDATE, (? || NVL(TAX_TYPE_CODE,'0000') || ?), TOTAL_AMOUNT, ?, "
//                            + "  NULL, ?, NULL "
//                            + " FROM INVOICE "
//                            + " WHERE INVOICE_ID = ? ";
//
//                    ps = conn.prepareStatement(sql); //, new String[]{"PAYMENT_ID"}
//
//                    ps.setString(i++, "99"); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค, 99 = รอตรวจสอบยอดเงิน)
//                    ps.setString(i++, "1"); //IS_ONLINE_PAYMENT = 1
//                    ps.setString(i++, AppUtil.checkNullData(paymentName)); //ONLINE_PAYMENT_METHOD = m_payment.payment_name
//
//                    // ('640817' || NVL(TAX_TYPE_CODE,'') || '000000000000') 
//                    Locale lc = new Locale("th", "TH");
//                    DateFormat df = new SimpleDateFormat("yyMMdd", lc); // Just the year, with 2 digits
//                    //int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
//                    String yymmdd = df.format(Calendar.getInstance(lc).getTime());
//                    ps.setString(i++, yymmdd); //ONLINE_PAYMENT_REF 1 = หมายเลขอ้างอิงที่ออกโดยระบบ 
//                    //เลขรันนิ่ง 12 หลัก (onlinepayment)
//                    String onlinepaymentRunning = AppUtil.getRunningDoc("onlinepayment", "000000000000");
//                    //format : yymmdd + tax_type 4 หลัก + เลขรันนิ่ง 12 หลัก (onlinepayment)
//                    ps.setString(i++, onlinepaymentRunning); //ONLINE_PAYMENT_REF 2 = หมายเลขอ้างอิงที่ออกโดยระบบ 
//
//                    ps.setString(i++, AppUtil.checkNullData(username)); //ONLINE_PAYMENT_BY
//                    ps.setString(i++, "0"); //CONFIRM_PAYMENT = false = 0
//
//                    ps.setInt(i++, invoice.getInvoiceId());
//
//                    result = ps.executeUpdate() != 0;
//                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="insert INVOICE_TRANS from INVOICE_TRANS_TEMP  ">
                    sql = " INSERT INTO INVOICE_TRANS\n"
                            + "SELECT * FROM INVOICE_TRANS_TEMP\n"
                            + "WHERE INVOICE_ID = ? ";

                    i = 1;
                    ps = conn.prepareStatement(sql);
                    ps.setInt(1, invoice.getInvoiceId());

                    result = ps.executeUpdate() != 0;
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="delete INVOICE_TRANS_TEMP  ">
                    sql = " DELETE FROM INVOICE_TRANS_TEMP "
                            + " WHERE INVOICE_ID = ? ";

                    i = 1;
                    ps = conn.prepareStatement(sql);
                    ps.setInt(1, invoice.getInvoiceId());

                    result = ps.executeUpdate() != 0;
                    //</editor-fold>

                    invoicePayment.setInvoiceId(AppUtil.encryptId(invoice.getInvoiceId()));
                    //invoicePayment.setPaymentId(AppUtil.encryptId(id));
                    invoicePaymentList.add(invoicePayment);

                }

            }

            conn.commit();

            dataOut.setInvoicePaymentList(invoicePaymentList);

            resultData.setResult(dataOut);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(dataOut);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListPayment() throws SQLException {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT PAYMENT_ID ,  PAYMENT_NAME , ICON  , IS_ACTIVE , SEQ , "
                    + "    PAYMENT_FORMAT , PAYMENT_VALUE , START_DATE , END_DATE "
                    + " FROM M_PAYMENT P "
                    + " WHERE IS_ACTIVE = '1' ";
            /*
 SELECT PAYMENT_ID ,  PAYMENT_NAME , ICON  , IS_ACTIVE , SEQ , 
    PAYMENT_FORMAT , PAYMENT_VALUE , START_DATE , END_DATE 
 FROM M_PAYMENT P 
 WHERE IS_ACTIVE = '1' 
    --AND ( TRUNC(START_DATE) <= TRUNC(SYSDATE)) 
    --AND ( TRUNC(END_DATE) >= TRUNC(SYSDATE)) 
 ORDER BY SEQ , PAYMENT_NAME 
             */

            ps.setSql(sql);

            String orderBy = " SEQ , PAYMENT_NAME ";
            ps.setOrderBy(orderBy);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataList = null;
            dataList = new InvoiceMapper(rs).mapDrpListPayment();

            resultData.setResult(dataList);

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMsg(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    //generate transPayNo ของ invoiceid
    public TransactionPaymentViewModel saveDataInvoiceTransTemp(InvoicePaymentViewModel data) throws SQLException {
        String running = "";
        String ref1 = "";
        String ref2 = "";
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        TransactionPaymentViewModel transactionData = new TransactionPaymentViewModel();

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            if ("p2p".equals(data.getPaymentName())) {
                running = AppUtil.getRunningDoc("ktbtransnop2p", "1");
            } else if ("fastpay".equals(data.getPaymentName())) {
                running = AppUtil.getRunningDoc("ktbtransnofastpay", "2");
            }

            //กรณีการรวมบิลเท่านั้นที่จะ gen ref1 ref2 ใหม่
            if (data.getInvoiceIdList().size() > 1) {
                Locale lc = new Locale("th", "TH");
                int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                //รหัสหน่วยงาน เอามาแค่ 2 หลัก + รหัสภาษี  เอามาแค่ 2 หลัก + ปีงบ + เลขรันนิ่ง 12 หลัก 
                ref1 = AppUtil.getRunningDoc("ref1", "");
                ref2 = AppUtil.getRunningDoc("ref2", "");
            } else {

                //<editor-fold defaultstate="collapsed" desc="select ref1 ref2">
                for (InvoiceViewModel model : data.getInvoiceIdList()) {
                    sql = " SELECT REF1,REF2 FROM INVOICE "
                            + " WHERE INVOICE_ID = ? ";

                    i = 1;
                    ps.setSql(sql);
                    ps.setString(1, AppUtil.decrypt(model.getInvoiceId()));

                    rs = ps.executeQuery();
                    while (rs.next()) {
                        ref1 = rs.getString("REF1");
                        ref2 = rs.getString("REF2");
                    }
                }
                //</editor-fold>
            }

            for (InvoiceViewModel model : data.getInvoiceIdList()) {

                //<editor-fold defaultstate="collapsed" desc="delete old data  ">
                sql = " DELETE FROM INVOICE_TRANS_TEMP "
                        + " WHERE INVOICE_ID = ? ";

                i = 1;
                ps.setSql(sql);
                ps.setString(1, AppUtil.decrypt(model.getInvoiceId()));

                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="insert from gen ">
                sql = " INSERT INTO INVOICE_TRANS_TEMP (INVOICE_ID, TRANSACTION_PAY_NO , REF1, REF2 "
                        + " ) VALUES(?,?,?,?) ";

                i = 1;
                ps.setSql(sql);

                ps.setString(1, AppUtil.decrypt(model.getInvoiceId()));
                ps.setString(2, running);
                ps.setString(3, ref1);
                ps.setString(4, ref2);

                result = ps.executeUpdate();

                //</editor-fold>
            }
            ps.commit();

            transactionData.setTransactionPayNo(running);
            transactionData.setRef1New(ref1);
            transactionData.setRef2New(ref2);

        } catch (Exception e) {
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return transactionData;
    }

//ดึงข้อมูล transPayNo ของ invoiceid
    public String getTransactionNoTransTemp(String invoiceId) throws SQLException {

        String transactionNo = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT TRANSACTION_PAY_NO FROM INVOICE_TRANS_TEMP "
                    + " WHERE INVOICE_ID = ? ";

            ps.setSql(sql);
            ps.setString(1, AppUtil.decrypt(invoiceId));

            rs = ps.executeQuery();

            while (rs.next()) {
                transactionNo = rs.getString("TRANSACTION_PAY_NO");
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return transactionNo;

    }

    public FastpayViewModel getFastpayData(FastpayViewModel data) throws SQLException {
        String running = "";
        String ref1 = "";
        String ref2 = "";
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        FastpayViewModel fastpayData = new FastpayViewModel();

        try {

            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="select fastpay data">
            sql = " SELECT * FROM PAY_FASTPAY_CONFIG "
                    + " WHERE IS_ACTIVE = 1 ";

            i = 1;
            ps.setSql(sql);

            rs = ps.executeQuery();
            while (rs.next()) {
                fastpayData.setMerchanId(rs.getString("MERCHANT_ID"));
                fastpayData.setCurrCode(rs.getString("CURR_CODE"));
                fastpayData.setPayType(rs.getString("PAY_TYPE"));
                fastpayData.setSecureHashKey(rs.getString("SECURE_HASH_KEY"));
            }
            //</editor-fold>

            //merchantId|orderRef|currCode|amount|payType|SecureHashKey
            String securityKey = fastpayData.getMerchanId() + "|" + data.getOrderRef() + "|" + fastpayData.getCurrCode() + "|" + String.valueOf(data.getAmount()) + "|" + fastpayData.getPayType() + "|" + fastpayData.getSecureHashKey();
            SHA512 utilSHA = new SHA512();            
            fastpayData.setSecurityKey(utilSHA.getSHA512SecurePassword(securityKey, fastpayData.getSecureHashKey()));
            
            fastpayData.setSecureHashKey("");
            fastpayData.setOrderRef(data.getOrderRef());
            fastpayData.setAmount(data.getAmount());

        } catch (Exception e) {
//            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return fastpayData;
    }

}
