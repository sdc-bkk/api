/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.PaymentStatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultPaymentViewModel;

/**
 *
 * @author User
 */
public class InvoiceMapper {

    private ResultSet rs = null;

    public InvoiceMapper() {

    }

    public InvoiceMapper(ResultSet rs) {
        this.rs = rs;
    }

    public InvoiceModel mapSaveData(InvoiceViewModel dataView) {

        InvoiceModel data = new InvoiceModel();

        try {
            data.setInvoiceId(AppUtil.decryptId(dataView.getInvoiceId()));

            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setBillNo(AppUtil.checkNullData(dataView.getBillNo()));
            data.setRef1(AppUtil.checkNullData(dataView.getRef1()));
            data.setRef2(AppUtil.checkNullData(dataView.getRef2()));
            data.setCusName(AppUtil.checkNullData(dataView.getCusName()));
            data.setDueDate(dataView.getDueDate());

            data.setDetail1(AppUtil.checkNullData(dataView.getDetail1()));
            data.setAmount1(AppUtil.convertStringToDouble(dataView.getAmount1()));
            data.setDetail2(AppUtil.checkNullData(dataView.getDetail2()));
            data.setAmount2(AppUtil.convertStringToDouble(dataView.getAmount2()));
            data.setDetail3(AppUtil.checkNullData(dataView.getDetail3()));
            data.setAmount3(AppUtil.convertStringToDouble(dataView.getAmount3()));
            data.setDetail4(AppUtil.checkNullData(dataView.getDetail4()));
            data.setAmount4(AppUtil.convertStringToDouble(dataView.getAmount4()));
            data.setDetail5(AppUtil.checkNullData(dataView.getDetail5()));
            data.setAmount5(AppUtil.convertStringToDouble(dataView.getAmount5()));
            data.setTotalAmount(AppUtil.convertStringToDouble(dataView.getTotalAmount()));

            data.setOffice(AppUtil.checkNullData(dataView.getOffice()));
            data.setPartOfOffice(AppUtil.checkNullData(dataView.getPartOfOffice()));
            data.setTelephone(AppUtil.checkNullData(dataView.getTelephone()));

            // </editor-fold>
            
            data.setTaxTypeCode(AppUtil.checkNullData(dataView.getTaxTypeCode()));
            data.setTaxTypeName(AppUtil.checkNullData(dataView.getTaxTypeName()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
//            if (!dataView.getInvoiceId().isEmpty()) {
            //BmaBillNo มาจาก running
            data.setBmaBillNo(AppUtil.getRunningDoc("bmabillno", ""));
//            }

            //BARCODE มาจาก BillNo + Ref1 + REf2 + amount
            String totalAmt = AppUtil.numberFormatNoCommaAnd2Digit(dataView.getTotalAmount());
            //String barcode = "|" + dataView.getBillNo() + " " + dataView.getRef1() + " " + dataView.getRef2() + " " + totalAmt.replace(".", "").replace(",", "");
            String barcode = "|" + dataView.getBillNo() + "\\r\\n" + dataView.getRef1() + "\\r\\n" + dataView.getRef2() + "\\r\\n" + totalAmt.replace(".", "").replace(",", "");
                
            data.setBarcode(barcode);            
            data.setQrcode(barcode);//QRCODE มาจาก


            //BARCODE มาจาก BillNo + Ref1 + REf2 + amount
//            data.setBarcode("|" + dataView.getBillNo() + "" + dataView.getRef1() + "" + dataView.getRef2() + "" + dataView.getTotalAmount().replace(".", ""));
            //QRCODE มาจาก
//            data.setQrcode("|" + dataView.getBillNo() + "" + dataView.getRef1() + "" + dataView.getRef2() + "" + dataView.getTotalAmount().replace(".", ""));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public InvoiceViewModel mapFull() {

        InvoiceViewModel data = new InvoiceViewModel();

        try {

            while (rs.next()) {
//                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("BILL_NO")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("REF2")));
//                data.setRef2(AppUtil.checkNullData(rs.getString("CUS_NAME")));
//                if (rs.getDate("DUE_DATE") != null) {
//                    data.setPaymentDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
//                }
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL1")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT1")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL2")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT2")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL3")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT3")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL4")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT4")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL5")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT5")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("TOTAL_AMOUNT")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("OFFICE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("PART_OF_OFFICE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TELEPHONE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TAX_NO")));
//                if (rs.getDate("ACTION_DATE") != null) {
//                    data.setPaymentDate(DateUtils.toThai(rs.getDate("ACTION_DATE")));
//                }
//                data.setRef1(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("BMA_BILL_NO")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("BARCODE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("QRCODE")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public ResultPaymentViewModel mapInvoicePayment() {

        ResultPaymentViewModel data = new ResultPaymentViewModel();

        try {

            while (rs.next()) {
                data.setRef1(rs.getString("REF1"));
                data.setRef2(rs.getString("REF2"));
                data.setPaymentStatus("0");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
    
    
    ///////////// bma-back /////////////////
    public List<InvoiceViewModel> mapList() {

        List<InvoiceViewModel> dataList = new ArrayList<InvoiceViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportInvoice = appConfig.value("export_invoice");

        try {

            while (rs.next()) {
                InvoiceViewModel data = new InvoiceViewModel();

                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                data.setBillNo(AppUtil.checkNullData(rs.getString("BILL_NO")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                if (rs.getDate("DUE_DATE") != null) {
                    data.setDueDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
                } else {
                    data.setDueDate("");
                }
                if (rs.getDate("ACTION_DATE") != null) {
                    data.setActionDate(DateUtils.toThai(rs.getDate("ACTION_DATE")));
                } else {
                    data.setActionDate("");
                }
                //data.setPaymentStatus(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
                if (rs.getString("PAYMENT_STATUS") != null) {
                    data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                } else {
                    data.setPaymentStatus("");
                }
                //data.setCancel(AppUtil.checkNullData(rs.getBoolean("IS_CANCEL")));
                //0=ใช้งาน , 1=ยกเลิก
                if(!AppUtil.isNull(rs.getString("IS_CANCEL")) && "1".contentEquals(rs.getString("IS_CANCEL"))){
                    data.setCancel("ยกเลิก");
                } else {
                    data.setCancel("ใช้งาน");
                }

                //link eaport                
                data.setLinkExportInvoice(linkExportInvoice + data.getInvoiceId());
//                data.setLinkExportInvoice(linkExportInvoice);

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public InvoiceViewModel mapDataInvoice() {

        InvoiceViewModel data = new InvoiceViewModel();
        AppConfig appConfig = new AppConfig();
        String linkExportInvoice = appConfig.value("export_invoice");

        try {

            while (rs.next()) {
                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                
                data.setBillNo(AppUtil.checkNullData(rs.getString("BILL_NO")));
                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
                data.setRef2(AppUtil.checkNullData(rs.getString("REF2")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setCusAddress(AppUtil.checkNullData(rs.getString("CUS_ADDRESS")));
                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                
                if (rs.getDate("DUE_DATE") != null) {
                    data.setDueDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
                } else {
                    data.setDueDate("");
                }
                
                data.setDetail1(AppUtil.checkNullData(rs.getString("DETAIL1")));
                data.setAmount1(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT1")));
                data.setDetail2(AppUtil.checkNullData(rs.getString("DETAIL2")));
                data.setAmount2(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT2")));
                data.setDetail3(AppUtil.checkNullData(rs.getString("DETAIL3")));
                data.setAmount3(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT3")));
                data.setDetail4(AppUtil.checkNullData(rs.getString("DETAIL4")));
                data.setAmount4(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT4")));
                data.setDetail5(AppUtil.checkNullData(rs.getString("DETAIL5")));
                data.setAmount5(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT5")));
                
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                data.setOffice(AppUtil.checkNullData(rs.getString("OFFICE")));
                data.setPartOfOffice(AppUtil.checkNullData(rs.getString("PART_OF_OFFICE")));
                data.setTelephone(AppUtil.checkNullData(rs.getString("TELEPHONE")));
                
                if (rs.getDate("ACTION_DATE") != null) {
                    data.setActionDate(DateUtils.toThai(rs.getDate("ACTION_DATE")));
                } else {
                    data.setActionDate("");
                }
                
                data.setBmaBillNo(AppUtil.checkNullData(rs.getString("BMA_BILL_NO")));
                data.setBarcode(AppUtil.checkNullData(rs.getString("BARCODE")));
                data.setQrcode(AppUtil.checkNullData(rs.getString("QRCODE")));

                //data.setPaymentStatus(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
                if (rs.getString("PAYMENT_STATUS") != null) {
                    data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                } else {
                    data.setPaymentStatus("");
                }
                //data.setCancel(AppUtil.checkNullData(rs.getBoolean("IS_CANCEL")));
                //0=ใช้งาน , 1=ยกเลิก
                if(!AppUtil.isNull(rs.getString("IS_CANCEL")) && "1".contentEquals(rs.getString("IS_CANCEL"))){
                    data.setCancel("ยกเลิก");
                } else {
                    data.setCancel("ใช้งาน");
                }

                //link eaport                
                data.setLinkExportInvoice(linkExportInvoice + data.getInvoiceId());
//                data.setLinkExportInvoice(linkExportInvoice);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public InvoiceModel mapSaveDataInvoice(InvoiceViewModel dataView) {

        InvoiceModel data = new InvoiceModel();

        try {
            data.setInvoiceId(AppUtil.decryptId(dataView.getInvoiceId()));

            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setRef1(AppUtil.checkNullData(dataView.getRef1()));
            data.setRef2(AppUtil.checkNullData(dataView.getRef2()));
            data.setCusName(AppUtil.checkNullData(dataView.getCusName()));
            data.setCusAddress(AppUtil.checkNullData(dataView.getCusAddress()));
            data.setDueDate(dataView.getDueDate());

            data.setDetail1(AppUtil.checkNullData(dataView.getDetail1()));
            data.setAmount1(AppUtil.convertStringToDouble(dataView.getAmount1()));
            data.setDetail2(AppUtil.checkNullData(dataView.getDetail2()));
            data.setAmount2(AppUtil.convertStringToDouble(dataView.getAmount2()));
            data.setDetail3(AppUtil.checkNullData(dataView.getDetail3()));
            data.setAmount3(AppUtil.convertStringToDouble(dataView.getAmount3()));
            data.setDetail4(AppUtil.checkNullData(dataView.getDetail4()));
            data.setAmount4(AppUtil.convertStringToDouble(dataView.getAmount4()));
            data.setDetail5(AppUtil.checkNullData(dataView.getDetail5()));
            data.setAmount5(AppUtil.convertStringToDouble(dataView.getAmount5()));
            data.setTotalAmount(AppUtil.convertStringToDouble(dataView.getTotalAmount()));

            data.setOffice(AppUtil.checkNullData(dataView.getOffice()));
            data.setPartOfOffice(AppUtil.checkNullData(dataView.getPartOfOffice()));
            data.setTelephone(AppUtil.checkNullData(dataView.getTelephone()));

            // </editor-fold>
            
            data.setTaxTypeCode(AppUtil.checkNullData(dataView.getTaxTypeCode()));
            data.setTaxTypeName(AppUtil.checkNullData(dataView.getTaxTypeName()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
            
            //Bill No./  = เลขประจำตัวผู้เสียภำษีของกรุงเทพมหำนคร แล้วต่อด้วย 02
            //data.setBillNo(AppUtil.checkNullData(dataView.getBillNo()));
            data.setBillNo(data.getTaxNo() + "02");
            
            if (data.getInvoiceId() == 0) {
                //BmaBillNo มาจาก running
                data.setBmaBillNo(AppUtil.getRunningDoc("bmabillno", ""));
            }
            
            String totalAmt = AppUtil.numberFormatNoCommaAnd2Digit(dataView.getTotalAmount());
            String barcode = "|" + dataView.getBillNo() + " " + dataView.getRef1() + " " + dataView.getRef2() + " " + totalAmt.replace(".", "").replace(",", "");
                
            data.setBarcode(barcode);            
            data.setQrcode(barcode);
            //BARCODE มาจาก BillNo + Ref1 + REf2 + amount
            //data.setBarcode("|" + dataView.getBillNo() + "" + dataView.getRef1() + "" + dataView.getRef2() + "" + dataView.getTotalAmount().replace(".", ""));
//            data.setBarcode("");
                    
            //QRCODE มาจาก
            //data.setQrcode("|" + dataView.getBillNo() + "" + dataView.getRef1() + "" + dataView.getRef2() + "" + dataView.getTotalAmount().replace(".", ""));
//            data.setQrcode("");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    
    public List<DrpDownViewModel> mapDrpListTaxType() {

        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();

        try {

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();

                data.setId(AppUtil.encryptId(rs.getInt("TAX_TYPE_ID")));
                //data.setId(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                data.setOther(data.getCode() + " - " + data.getName());
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    
    ///////////// bma-front /////////////////
    public List<InvoiceViewModel> mapListFront() {

        List<InvoiceViewModel> dataList = new ArrayList<InvoiceViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportInvoice = appConfig.value("export_invoice");

        try {

            while (rs.next()) {
                InvoiceViewModel data = new InvoiceViewModel();

                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                data.setBillNo(AppUtil.checkNullData(rs.getString("BILL_NO")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                
                data.setDetail1(AppUtil.checkNullData(rs.getString("DETAIL1")));
                data.setAmount1(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT1")));
                data.setDetail2(AppUtil.checkNullData(rs.getString("DETAIL2")));
                data.setAmount2(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT2")));
                data.setDetail3(AppUtil.checkNullData(rs.getString("DETAIL3")));
                data.setAmount3(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT3")));
                data.setDetail4(AppUtil.checkNullData(rs.getString("DETAIL4")));
                data.setAmount4(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT4")));
                data.setDetail5(AppUtil.checkNullData(rs.getString("DETAIL5")));
                data.setAmount5(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT5")));
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                
                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
                data.setRef2(AppUtil.checkNullData(rs.getString("REF2")));
                data.setBmaBillNo(AppUtil.checkNullData(rs.getString("BMA_BILL_NO")));
                data.setBarcode(AppUtil.checkNullData(rs.getString("BARCODE")));
                data.setQrcode(AppUtil.checkNullData(rs.getString("QRCODE")));
                
                if (rs.getDate("DUE_DATE") != null) {
                    data.setDueDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
                } else {
                    data.setDueDate("");
                }
                if (rs.getDate("ACTION_DATE") != null) {
                    data.setActionDate(DateUtils.toThai(rs.getDate("ACTION_DATE")));
                } else {
                    data.setActionDate("");
                }
                //data.setPaymentStatus(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
                if (rs.getString("PAYMENT_STATUS") != null) {
                    data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                } else {
                    data.setPaymentStatus("");
                }
                //data.setCancel(AppUtil.checkNullData(rs.getBoolean("IS_CANCEL")));
                //0=ใช้งาน , 1=ยกเลิก
                if(!AppUtil.isNull(rs.getString("IS_CANCEL")) && "1".contentEquals(rs.getString("IS_CANCEL"))){
                    data.setCancel("ยกเลิก");
                } else {
                    data.setCancel("ใช้งาน");
                }

                //link eaport                
                data.setLinkExportInvoice(linkExportInvoice + data.getInvoiceId());
//                data.setLinkExportInvoice(linkExportInvoice);

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    
    public List<DrpDownViewModel> mapDrpListPayment() {

        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();

        try {

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();

                data.setId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                data.setCode(AppUtil.checkNullData(rs.getString("PAYMENT_FORMAT")));
                data.setName(AppUtil.checkNullData(rs.getString("PAYMENT_NAME")));
                data.setOther(AppUtil.checkNullData(rs.getString("ICON")));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

}
