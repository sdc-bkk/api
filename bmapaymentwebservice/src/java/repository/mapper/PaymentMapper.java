/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.PaymentMethodEnum;
import enumeration.PaymentStatusEnum;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;

/**
 *
 * @author User
 */
public class PaymentMapper {
    private ResultSet rs = null;

    public PaymentMapper() {

    }

    public PaymentMapper(ResultSet rs) {
        this.rs = rs;
    }


    public InvoicePaymentModel mapSaveData(InvoicePaymentViewModel dataView) {

        InvoicePaymentModel data = new InvoicePaymentModel();

        try {
            data.setPaymentStatus(AppUtil.checkNullData(dataView.getPaymentStatus()));
            data.setPaymentMethod(AppUtil.checkNullData(dataView.getPaymentMethod()));
            data.setPaymentDate(dataView.getPaymentDate());
            data.setPaymentTime(AppUtil.checkNullData(dataView.getPaymentTime()));
            data.setAmount(AppUtil.convertStringToDouble(dataView.getAmount()));
            data.setReceiptNo(AppUtil.checkNullData(dataView.getReceiptNo()));
            data.setChequeDate(dataView.getChequeDate());
            data.setChequeNo(AppUtil.checkNullData(dataView.getChequeNo()));
            data.setChequeBank(AppUtil.checkNullData(dataView.getChequeBank()));
            data.setChequeBranch(AppUtil.checkNullData(dataView.getChequeBranch()));
            
            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setBillNo(AppUtil.checkNullData(dataView.getBillNo()));
            data.setRef1(AppUtil.checkNullData(dataView.getRef1()));
            data.setRef2(AppUtil.checkNullData(dataView.getRef2()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
            data.setCusName(AppUtil.checkNullData(dataView.getCusName()));
            data.setDueDate(dataView.getDueDate());

            data.setDetail1(AppUtil.checkNullData(dataView.getDetail1()));
            data.setAmount1(AppUtil.convertStringToDouble(dataView.getAmount1()));
            data.setDetail2(AppUtil.checkNullData(dataView.getDetail2()));
            data.setAmount2(AppUtil.convertStringToDouble(dataView.getAmount2()));
            data.setDetail3(AppUtil.checkNullData(dataView.getDetail3()));
            data.setAmount3(AppUtil.convertStringToDouble(dataView.getAmount3()));
            data.setDetail4(AppUtil.checkNullData(dataView.getDetail4()));
            data.setAmount4(AppUtil.convertStringToDouble(dataView.getAmount4()));
            data.setDetail5(AppUtil.checkNullData(dataView.getDetail5()));
            data.setAmount5(AppUtil.convertStringToDouble(dataView.getAmount5()));
            data.setTotalAmount(AppUtil.convertStringToDouble(dataView.getTotalAmount()));

            data.setOffice(AppUtil.checkNullData(dataView.getOffice()));
            data.setPartOfOffice(AppUtil.checkNullData(dataView.getPartOfOffice()));
            data.setTelephone(AppUtil.checkNullData(dataView.getTelephone()));

            // </editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
    
    
    ///////////// bma-back /////////////////
    public List<InvoicePaymentViewModel> mapList() {

        List<InvoicePaymentViewModel> dataList = new ArrayList<InvoicePaymentViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportInvoicePayment = appConfig.value("export_invoice_payment");

        try {

            while (rs.next()) {
                InvoicePaymentViewModel data = new InvoicePaymentViewModel();

                data.setTransactionId(AppUtil.encryptId(rs.getInt("TRANSACTION_ID")));
                data.setPaymentId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                data.setReceiptNo(AppUtil.checkNullData(rs.getString("RECEIPT_NO")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                if (rs.getDate("PAYMENT_DATE") != null) {
                    data.setPaymentDate(DateUtils.toThai(rs.getDate("PAYMENT_DATE")));
                } else {
                    data.setPaymentDate("");
                }
                //data.setPaymentStatus(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
                if (rs.getString("PAYMENT_STATUS") != null) {
                    data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                } else {
                    data.setPaymentStatus("");
                }
                //data.setPaymentMethod(AppUtil.checkNullData(rs.getString("PAYMENT_METHOD")));
                if (rs.getString("PAYMENT_METHOD") != null) {
                    data.setPaymentMethod(PaymentMethodEnum.getDisplayName(rs.getInt("PAYMENT_METHOD")));
                } else {
                    data.setPaymentMethod("");
                }

                //link Export                
                data.setReceiptLink(linkExportInvoicePayment + data.getTransactionId());
                //data.setReceiptLink(linkExportInvoicePayment);

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    
    ///////////// bma-front /////////////////
    public List<InvoicePaymentViewModel> mapListFront() {

        List<InvoicePaymentViewModel> dataList = new ArrayList<InvoicePaymentViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportInvoicePayment = appConfig.value("export_invoice_payment");

        try {

            while (rs.next()) {
                InvoicePaymentViewModel data = new InvoicePaymentViewModel();

                data.setTransactionId(AppUtil.encryptId(rs.getInt("TRANSACTION_ID")));
                data.setPaymentId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                data.setReceiptNo(AppUtil.checkNullData(rs.getString("RECEIPT_NO")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
                data.setRef2(AppUtil.checkNullData(rs.getString("REF2")));
                
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                if (rs.getDate("PAYMENT_DATE") != null) {
                    data.setPaymentDate(DateUtils.toThai(rs.getDate("PAYMENT_DATE")));
                } else {
                    data.setPaymentDate("");
                }
                if (!AppUtil.isNullAndSpace(rs.getString("PAYMENT_TIME"))) {
                    data.setPaymentTime(AppUtil.checkNullData(rs.getString("PAYMENT_TIME")));
                } else {
                    data.setPaymentTime("");
                }
                //data.setPaymentStatus(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
                if (rs.getString("PAYMENT_STATUS") != null) {
                    data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                } else {
                    data.setPaymentStatus("");
                }
                //data.setPaymentMethod(AppUtil.checkNullData(rs.getString("PAYMENT_METHOD")));
                if (rs.getString("PAYMENT_METHOD") != null) {
                    data.setPaymentMethod(PaymentMethodEnum.getDisplayName(rs.getInt("PAYMENT_METHOD")));
                } else {
                    data.setPaymentMethod("");
                }

                //link Export                
                data.setReceiptLink(linkExportInvoicePayment + data.getTransactionId());
                //data.setReceiptLink(linkExportInvoicePayment);

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public InvoicePaymentViewModel mapDataForSendMail() {

        InvoicePaymentViewModel data = new InvoicePaymentViewModel();

        try {
/*
" SELECT P.PAYMENT_ID, P.REF1, P.CUS_NAME, P.TAX_NO, 'kingwerewolf@gmail.com' EMAIL, " //M.EMAIL, 
"    P.ONLINE_PAYMENT_AMOUNT, P.ONLINE_PAYMENT_DATE, P.ONLINE_PAYMENT_REF, P.ONLINE_PAYMENT_METHOD "            
*/
            while (rs.next()) {
                data.setPaymentId(AppUtil.encryptId(rs.getInt("PAYMENT_ID")));
                
                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                
                data.setOnlinePaymentAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("ONLINE_PAYMENT_AMOUNT")));
                data.setOnlinePaymentRef(AppUtil.checkNullData(rs.getString("ONLINE_PAYMENT_REF")));
                data.setOnlinePaymentMethod(AppUtil.checkNullData(rs.getString("ONLINE_PAYMENT_METHOD")));
                
                if (rs.getTimestamp("ONLINE_PAYMENT_DATE") != null) {
                    //data.setDueDate(DateUtils.toThai(rs.getTimestamp("ONLINE_PAYMENT_DATE")));
                    Locale lc = new Locale("th", "TH");
                    DateFormat df = new SimpleDateFormat("dd MMM yyyy HH.mm น.",lc); // 14 เม.ย. 2564 15.35 น.
                    //int yearly = Calendar.getInstance(lc).get(Calendar.YEAR);
                    //String yymmdd = df.format(Calendar.getInstance(lc).getTime());
                    Timestamp ts = rs.getTimestamp("ONLINE_PAYMENT_DATE");
//                    Date date = new Date();
//                    date.setTime(ts.getTime());
                    Calendar c = Calendar.getInstance(lc);
                    c.setTime(ts);
                    String paymentDate = df.format(c.getInstance(lc).getTime());
                    data.setOnlinePaymentDate(paymentDate);
                } else {
                    data.setOnlinePaymentDate("");
                }
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
