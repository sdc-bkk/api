/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.InvoiceModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultPaymentViewModel;

/**
 *
 * @author User
 */
public class ResultPaymentMapper {

    private ResultSet rs = null;

    public ResultPaymentMapper() {

    }

    public ResultPaymentMapper(ResultSet rs) {
        this.rs = rs;
    }

    public ResultPaymentViewModel mapFull() {

        ResultPaymentViewModel data = new ResultPaymentViewModel();

        try {

            while (rs.next()) {
                data.setRef1(rs.getString("REF1"));
                data.setRef2(rs.getString("REF2"));
                data.setPaymentStatus(rs.getString("PAYMENT_STATUS"));
                data.setPaymentMethod(rs.getString("PAYMENT_METHOD"));
                if (rs.getDate("PAYMENT_DATE") != null) {
                    data.setPaymentDate(DateUtils.toThai(rs.getDate("PAYMENT_DATE")));
                }
                data.setPaymentTime("");
                data.setAmount(rs.getString("PAYMENT_AMOUNT"));
                data.setReceiptNo(rs.getString("RECEIPT_NO"));                
                if (rs.getDate("CHEQUE_DATE") != null) {
                    data.setChequeDate(DateUtils.toThai(rs.getDate("CHEQUE_DATE")));
                }
                data.setChequeNo(rs.getString("CHEQUE_NO"));
                data.setChequeBank(rs.getString("CHEQUE_BANK"));
                data.setChequeBranch(rs.getString("CHEQUE_BRANCH"));
                data.setReceiptLink("");  
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
