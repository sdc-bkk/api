/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class InvoicePaymentModel extends InvoiceModel{
    
    private String paymentStatus;//สถานะการชำระเงิน (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
    private String paymentMethod;//วิธีการชำระ (1 =เงินสด, 2 = บัตรเครดิต, 3 = บัตรเดบิต, 4 = ธนาณัติ, 5 =  เช็ค , 6 = QR Code ผ่านเครื่อง EDC)
    private String paymentDate;//วันที่รับชำระ รูปแบบจะเป็น dd/mm/yyyy 
    private String paymentTime;//เวลาที่รับชำระ รูปแบบจะเป็น hh:mm 
    private double amount;//ยอดที่ชำระ 
    private String receiptNo;//เลขที่ใบเสร็จรับเงิน
    private String chequeDate;//วันที่เช็ค กรณีชำระด้วยเช็ค
    private String chequeNo;//เลขที่เช็ค กรณีชำระด้วยเช็ค
    private String chequeBank;//ธนาคาร กรณีชำระด้วยเช็ค
    private String chequeBranch;//สาขา กรณีชำระด้วยเช็ค

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeBank() {
        return chequeBank;
    }

    public void setChequeBank(String chequeBank) {
        this.chequeBank = chequeBank;
    }

    public String getChequeBranch() {
        return chequeBranch;
    }

    public void setChequeBranch(String chequeBranch) {
        this.chequeBranch = chequeBranch;
    }    
    
}
