/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author User
 */
public class InvoiceTransactionPayModel {
    private List<String> invoiceList;    
    private String channel;
    private String transactionPayNo;

    public List<String> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<String> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTransactionPayNo() {
        return transactionPayNo;
    }

    public void setTransactionPayNo(String transactionPayNo) {
        this.transactionPayNo = transactionPayNo;
    }
    
    
}
