/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import model.InvoiceTransactionPayModel;
import service.InvoiceService;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FastpayViewModel;
import viewModel.FilterInvoiceModel;
import viewModel.FilterPaymentModel;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.TransactionPaymentViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("frontinvoice")
public class FrontInvoiceWebservice {

    @Context
    private UriInfo context;

    InvoiceService service = new InvoiceService();
    private final MessageBundleUtil message = new MessageBundleUtil();

    /**
     * Creates a new instance of InvoiceWebservice
     */
    public FrontInvoiceWebservice() {
    }

    ///////////// bma-front /////////////////
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterInvoiceModel filter) {

        ResultData<List<InvoiceViewModel>> resultData = new ResultData<List<InvoiceViewModel>>();
        try {

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                resultData = service.getListFront(filter);
            } else {
                //ไม่มีเลขประจำตัวผู้เสียภาษี
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.invoice.taxNoNotEmpty"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(InvoicePaymentViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        try {

            if (data.getInvoiceIdList() != null && data.getInvoiceIdList().size() > 0) {
                resultData = service.saveDataFront(data);
            } else {
                //ไม่มีรายการชำระเงิน
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.invoiceFront.invoiceNotEmpty"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("sendMailPayment")
    public String sendMailPayment(InvoicePaymentViewModel data) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();
        try {

            if (data.getInvoiceIdList() != null && data.getInvoiceIdList().size() > 0) {
                for (InvoiceViewModel invoicePayment : data.getInvoiceIdList()) {
                    //ส่งเมล์ โดย get email จาก list
                    FilterPaymentModel filterSendMail = new FilterPaymentModel();
                    filterSendMail.setTransactionId(invoicePayment.getInvoiceId());
                    resultData = service.sendMailPayment(filterSendMail);
                }
            } else {
                //ไม่มีรายการชำระเงิน
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.invoiceFront.invoiceNotEmpty"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListPayment")
    public String getDrpListPayment() {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        try {

            resultData = service.getDrpListPayment();

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }

    @POST
    @Produces("Application/json;charset=utf8")
    @Consumes("Application/json;charset=utf8")
    @Path("getTranPayNo")
    public String getTranPayNo(InvoicePaymentViewModel data) {

        ResultData<TransactionPaymentViewModel> resultData = new ResultData<TransactionPaymentViewModel>();
        try {

            if ("p2p".equals(data.getPaymentName()) || "fastpay".equals(data.getPaymentName())) {
                resultData = service.getTranPayNo(data);

            } else {
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.notsuccess"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }
    
    @POST
    @Produces("Application/json;charset=utf8")
    @Consumes("Application/json;charset=utf8")
    @Path("getFastpayData")
    public String getFastpayData(FastpayViewModel data) {

        ResultData<FastpayViewModel> resultData = new ResultData<FastpayViewModel>();
        try {

                resultData = service.getFastpayData(data);

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }

}
