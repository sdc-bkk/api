/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import service.PaymentService;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.FilterPaymentModel;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultBasePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultPaymentViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("frontpayment")
public class FrontPaymentWebservice {

    PaymentService service = new PaymentService();
    private final MessageBundleUtil message = new MessageBundleUtil();
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PaymentWebservice
     */
    public FrontPaymentWebservice() {
    }

    
    ///////////// bma-front /////////////////
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterPaymentModel filter) {

        ResultData<List<InvoicePaymentViewModel>> resultData = new ResultData<List<InvoicePaymentViewModel>>();
        try {

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                resultData = service.getListFront(filter);
            } else {
                //ไม่มีเลขประจำตัวผู้เสียภาษี
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.list.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }
    
}
