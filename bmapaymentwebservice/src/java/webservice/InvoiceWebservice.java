/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import service.InvoiceService;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FilterInvoiceModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("invoice")
public class InvoiceWebservice {

    @Context
    private UriInfo context;

    InvoiceService service = new InvoiceService();
    private final MessageBundleUtil message = new MessageBundleUtil();

    /**
     * Creates a new instance of InvoiceWebservice
     */
    public InvoiceWebservice() {
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")

    @Path("sendExternalInvoice")
    public String sendExternalInvoice(@Context HttpServletRequest request, InvoiceViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        try {
            String clientId = request.getHeader("client_id");
            String clientSecret = request.getHeader("client_secret");

            resultData = service.sendExternalInvoice(data, clientId, clientSecret);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }
    
    
    ///////////// bma-back /////////////////
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterInvoiceModel filter) {

        ResultData<List<InvoiceViewModel>> resultData = new ResultData<List<InvoiceViewModel>>();
        try {

            resultData = service.getList(filter);

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterInvoiceModel filter) {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();
        try {

            resultData = service.getData(filter);

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(InvoiceViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        try {

            resultData = service.saveData(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("cancelInvoice")
    public String cancelInvoice(FilterInvoiceModel filter) {
    //สำหรับปุ่มยกเลิก
        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        try {

            resultData = service.cancelInvoice(filter);
            
        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }
    
    
    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListTaxType")
    public String getDrpListTaxType() {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();
        try {

            resultData = service.getDrpListTaxType();

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }
        
}
