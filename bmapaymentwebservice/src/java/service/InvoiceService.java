/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import model.InvoiceTransactionPayModel;
import repository.AppConfigRepo;
import repository.InvoiceRepo;
import repository.PaymentRepo;
import repository.TaxTypeRepo;
import repository.mapper.InvoiceMapper;
import utility.AppConfig;
import utility.AppUtil;
import utility.MailService;
import utility.MessageBundleUtil;
import viewModel.DrpDownViewModel;
import viewModel.FastpayViewModel;
import viewModel.FilterInvoiceModel;
import viewModel.FilterPaymentModel;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.ResultPage;
import viewModel.TaxTypeViewModel;
import viewModel.TransactionPaymentViewModel;

/**
 *
 * @author User
 */
public class InvoiceService {

    InvoiceRepo repo = new InvoiceRepo();
    AppConfigRepo appRepo = new AppConfigRepo();
    TaxTypeRepo taxTypeRepo = new TaxTypeRepo();
    PaymentRepo paymentRepo = new PaymentRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<ResultInvoiceViewModel> sendExternalInvoice(InvoiceViewModel data, String clientId, String clientSecret) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        InvoiceModel dataSave = new InvoiceModel();

        try {
            //check client_id
            String taxTypecode = taxTypeRepo.checkClientRight(clientId, clientSecret);//"1013";
            if (!taxTypecode.isEmpty()) { //ถ้าไม่ว่าง
                //check taxtype from client_id
//                ResultData<TaxTypeViewModel> taxTypeData = new ResultData<TaxTypeViewModel>();
//                taxTypeData = taxTypeRepo.getDataByTaxTypeCode(taxTypecode);
////                resultData.setResultMsg(taxTypeData.getResult().getTaxTypeName());
//                if (!taxTypeData.getResult().getTaxTypeCode().isEmpty()) {
                data.setTaxTypeCode(taxTypecode);
//                    data.setTaxTypeCode(taxTypeData.getResult().getTaxTypeCode());
//                    data.setTaxTypeName(taxTypeData.getResult().getTaxTypeName());

                //check data from BMA_BILL_NO ถ้ามีรายการแล้วให้ลบออกและสร้างใหม่
//                boolean resultDel = repo.deleteData(data);
                InvoiceMapper mapper = new InvoiceMapper();
                dataSave = mapper.mapSaveData(data);

                resultData = repo.sendExternalInvoice(dataSave);

//            } else {
//                resultData.setStatus("false");
//                resultData.setResultMsg(message.getMessage("message.notaxtypecode"));
//            }
            } else {
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.noauthen"));
            }

        } catch (SQLException e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    ///////////// bma-back /////////////////
    public ResultData<List<InvoiceViewModel>> getList(FilterInvoiceModel filter) {

        ResultData<List<InvoiceViewModel>> resultData = new ResultData<List<InvoiceViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getList(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<InvoiceViewModel> getData(FilterInvoiceModel filter) {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getInvoiceId());
            resultData = repo.getData(id);

            resultData.setStatus("true");
            resultData.setResultMsg("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getInvoiceId())) {
                resultData.setResult(new InvoiceViewModel());
                resultData.setResultMsg(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ResultInvoiceViewModel> saveData(InvoiceViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        InvoiceModel dataSave = new InvoiceModel();

        try {
            if (!AppUtil.isNullAndSpace(data.getTaxNo())) {

                //ตรวจสอบว่า TaxNo Ref1 Ref2 TaxTypeCode นี้เคยมีการสร้างแล้วหรือยัง
                FilterInvoiceModel filter = new FilterInvoiceModel();
                filter.setTaxNo(data.getTaxNo());
                filter.setTaxTypeCode(data.getTaxTypeCode());
                filter.setRef1(data.getRef1());
                filter.setRef2(data.getRef2());
                Boolean hasDuplicate = repo.duplicateData(filter);

                if (hasDuplicate == true) {
                    //กรณีมีข้อมูลแล้ว (TaxNo Ref1 Ref2 TaxTypeCode)
                    resultData.setStatus("false");
                    resultData.setResultMsg(message.getMessage("message.invoice.duplicate"));

                } else {

                    InvoiceMapper mapper = new InvoiceMapper();
                    dataSave = mapper.mapSaveDataInvoice(data);

                    resultData = repo.saveData(dataSave);

                }

            } else {
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.invoice.taxNoNotEmpty"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<ResultInvoiceViewModel> cancelInvoice(FilterInvoiceModel filter) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();

        try {

            resultData = repo.cancelInvoice(filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListTaxType() {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();

        try {

            resultData = repo.getDrpListTaxType();

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    ///////////// bma-front /////////////////
    public ResultData<List<InvoiceViewModel>> getListFront(FilterInvoiceModel filter) {

        ResultData<List<InvoiceViewModel>> resultData = new ResultData<List<InvoiceViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getListFront(resultPage, filter);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ResultInvoiceViewModel> saveDataFront(InvoicePaymentViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        InvoicePaymentModel dataSave = new InvoicePaymentModel();

        try {

            if (data.getInvoiceIdList() != null && data.getInvoiceIdList().size() > 0) {
                List<InvoiceModel> invoiceIdList = new ArrayList<InvoiceModel>(); //รายการ invoiceId
                for (InvoiceViewModel invoiceIdView : data.getInvoiceIdList()) {

                    if (AppUtil.decryptId(invoiceIdView.getInvoiceId()) > 0) {
                        InvoiceModel invoiceId = new InvoiceModel();
                        invoiceId.setInvoiceId(AppUtil.decryptId(invoiceIdView.getInvoiceId()));
                        invoiceIdList.add(invoiceId);
                    }

                }

                if (invoiceIdList.size() > 0) {

                    String paymentName = AppUtil.checkNullData(data.getPaymentName());//ชื่อช่องทาง
                    String username = AppUtil.checkNullData(data.getUsername());//username ของคน login

                    resultData = repo.saveDataFront(invoiceIdList, paymentName, username);

                    if ("true".equalsIgnoreCase(resultData.getStatus()) && resultData.getResult() != null) {
//                        ResultInvoiceViewModel dataOut = resultData.getResult();
//                        if (dataOut.getInvoicePaymentList() != null && dataOut.getInvoicePaymentList().size() > 0) {
//
//                            for (InvoicePaymentViewModel invoicePayment : dataOut.getInvoicePaymentList()) {
//                                //ส่งเมล์ โดย get email จาก member 
//                                FilterPaymentModel filterSendMail = new FilterPaymentModel();
//                                filterSendMail.setTransactionId(invoicePayment.getInvoiceId());
//                                ResultData<Boolean> resultSendMail = sendMailPayment(filterSendMail);
//                            }
//
//                        }
                        resultData.setStatus("true");
                    }

                } else {
                    //ไม่มีรายการชำระเงิน
                    resultData.setStatus("false");
                    resultData.setResultMsg(message.getMessage("message.invoiceFront.invoiceNotEmpty"));
                }

            } else {
                //ไม่มีรายการชำระเงิน
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.invoiceFront.invoiceNotEmpty"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<Boolean> sendMailPayment(FilterPaymentModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            int id = AppUtil.decryptId(filter.getTransactionId());
            //get email from
            ResultData<InvoicePaymentViewModel> resultEmailData = paymentRepo.getDataForSendMailByTransactionId(id);

            if (resultEmailData != null && resultEmailData.getResult() != null) {
                InvoicePaymentViewModel emailData = resultEmailData.getResult();
                //<editor-fold defaultstate="collapsed" desc="send mail ">
                AppConfig appConfig = new AppConfig();
                String linkInvoicePayment = appConfig.value("link_invoice_payment");

                //send Email
                String mailTo = emailData.getEmail();
                MailService mail = new MailService();
                mail.setTo(mailTo);
                mail.setSubject("ขอบคุณที่ชำระค่าภาษี/ค่าธรรมเนียม/ค่าบริการ ของกรุงเทพมหานคร");
                String temp = "เรียน คุณ " + emailData.getCusName() + "<br><br>"
                        + "     ขอบคุณที่ชำระค่าภาษี/ค่าธรรมเนียม/ค่าบริการ ของกรุงเทพมหานคร "
                        + "<br><br> "
                        + "จำนวนเงิน " + emailData.getOnlinePaymentAmount() + " บาท (" + emailData.getOnlinePaymentDate() + ") <br>"
                        + "เลขอ้างอิง " + emailData.getRef1() + " <br>"
                        + "ชำระโดย " + emailData.getOnlinePaymentMethod() + " "
                        + "<br><br>"
                        + "ท่านสามารถเรียกดูใบเสร็จรับเงิน/ใบกำกับภาษีออนไลน์ของท่านได้ในวันถัดไป กรุณา <a href=\"" + linkInvoicePayment + "\">คลิกที่นี่</a>";
                mail.setMessageText(temp);
                boolean resultSendMail = mail.sendMail();
            }
            resultData.setResult(Boolean.TRUE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<DrpDownViewModel>> getDrpListPayment() {

        ResultData<List<DrpDownViewModel>> resultData = new ResultData<List<DrpDownViewModel>>();

        try {

            resultData = repo.getDrpListPayment();

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<TransactionPaymentViewModel> getTranPayNo(InvoicePaymentViewModel data) {

        ResultData<TransactionPaymentViewModel> resultData = new ResultData<TransactionPaymentViewModel>();

        try {
            TransactionPaymentViewModel transactionData = repo.saveDataInvoiceTransTemp(data);
            resultData.setStatus("true");
            resultData.setResult(transactionData);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<FastpayViewModel> getFastpayData(FastpayViewModel data) {

        ResultData<FastpayViewModel> resultData = new ResultData<FastpayViewModel>();

        try {
            FastpayViewModel fastpayData = repo.getFastpayData(data);
            resultData.setStatus("true");
            resultData.setResult(fastpayData);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

}
