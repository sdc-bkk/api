/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import service.DebtorService;
import utility.MessageBundleUtil;
import viewModel.DebtorDetailViewModel;
import viewModel.DebtorViewModel;
import viewModel.FilterDebtorModel;
import viewModel.ResultData;
import viewModel.ResultDebtorViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("debtor")
public class DebtorWebservice {

    @Context
    private UriInfo context;

    DebtorService service = new DebtorService();
    private final MessageBundleUtil message = new MessageBundleUtil();

    /**
     * Creates a new instance of DebtorWebservice
     */
    public DebtorWebservice() {
    }

    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(FilterDebtorModel filter) {

        ResultData<List<DebtorViewModel>> resultData = new ResultData<List<DebtorViewModel>>();
        try {

            resultData = service.getList(filter);

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }
        
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(FilterDebtorModel filter) {

        ResultData<DebtorViewModel> resultData = new ResultData<DebtorViewModel>();
        try {

            resultData = service.getData(filter);

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(DebtorViewModel data) {

        ResultData<ResultDebtorViewModel> resultData = new ResultData<ResultDebtorViewModel>();
        try {

            resultData = service.saveData(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }    
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getCalculateDebtor")
    public String getCalculateDebtor(FilterDebtorModel filter) {

        ResultData<List<DebtorDetailViewModel>> resultData = new ResultData<List<DebtorDetailViewModel>>();
        try {

            resultData = service.getCalculateDebtor(filter);

        } catch (Exception e) {
            e.printStackTrace();
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return new Gson().toJson(resultData);

    }

}
