/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import model.DebtorModel;
import repository.DebtorRepo;
import repository.mapper.DebtorMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppConfig;
import utility.AppUtil;
import utility.DateUtil;
import utility.MessageBundleUtil;
import viewModel.DebtorDetailViewModel;
import viewModel.DebtorViewModel;
import viewModel.FilterDebtorModel;
import viewModel.FilterModel;
import viewModel.ResultData;
import viewModel.ResultDebtorViewModel;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class DebtorService {

    DebtorRepo repo = new DebtorRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<DebtorViewModel>> getList(FilterDebtorModel filter) {

        ResultData<List<DebtorViewModel>> resultData = new ResultData<List<DebtorViewModel>>();
        ResultPage resultPage = null;

        try {
            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            resultData = repo.getList(resultPage, filter);
            if(resultData != null && resultData.getResult() != null){
                for (DebtorViewModel data : resultData.getResult()) {
                        
                    if(!AppUtil.isNullAndSpace(data.getDebtorId()) && AppUtil.decryptId(data.getDebtorId()) > 0){
                        int debtorId = AppUtil.decryptId(data.getDebtorId());
                        ResultData<List<DebtorDetailViewModel>> resultDebtorDetail = repo.getDebtorDetailList(debtorId);
                        
                        int countPayment = 0;
                        //balance = ยอดเงินคงเหลือ = รวม  - เงินที่ชำระแล้ว
                        double balance = AppUtil.convertStringToDouble(data.getSumTax());
                        if(resultDebtorDetail != null && resultDebtorDetail.getResult() != null){
                            for (DebtorDetailViewModel debtorDetail : resultDebtorDetail.getResult()) {
                                if("ชำระแล้ว".equalsIgnoreCase(debtorDetail.getPaymentStatus())){
                                    double taxTotal = AppUtil.convertStringToDouble(debtorDetail.getTaxTotal()); 
                                    balance = balance - taxTotal;
                                    countPayment = countPayment + 1;
                                }
                            }
                        }
                        
                        data.setStatus(countPayment+"/"+data.getSubPayMonth());
                        data.setBalance(AppUtil.convertDoubleToStringFormat(balance));
                    } else {
                        data.setStatus("ค้างชำระ");
                        data.setBalance(data.getSumTax());
                    }
                    
                }
            }
            
            resultData.setStatus("true");
            resultData.setResultMessage("");

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<DebtorViewModel> getData(FilterDebtorModel filter) {

        ResultData<DebtorViewModel> resultData = new ResultData<DebtorViewModel>();

        try {
            int id = AppUtil.decryptId(filter.getInvoiceId());
            resultData = repo.getData(id);
            if(resultData != null && resultData.getResult() != null){
                DebtorViewModel data = resultData.getResult();
                        
                if(!AppUtil.isNullAndSpace(data.getDebtorId()) && AppUtil.decryptId(data.getDebtorId()) > 0){
                    int debtorId = AppUtil.decryptId(data.getDebtorId());
                    ResultData<List<DebtorDetailViewModel>> resultDebtorDetail = repo.getDebtorDetailList(debtorId);

                    int countPayment = 0;
                    //balance = ยอดเงินคงเหลือ = รวม  - เงินที่ชำระแล้ว
                    double balance = AppUtil.convertStringToDouble(data.getSumTax());
                    if(resultDebtorDetail != null && resultDebtorDetail.getResult() != null){
                        for (DebtorDetailViewModel debtorDetail : resultDebtorDetail.getResult()) {
                            if("ชำระแล้ว".equalsIgnoreCase(debtorDetail.getPaymentStatus())){
                                double taxTotal = AppUtil.convertStringToDouble(debtorDetail.getTaxTotal()); 
                                balance = balance - taxTotal;
                                countPayment = countPayment + 1;
                            }
                        }
                        
                        data.setDetailList(resultDebtorDetail.getResult());
                    } else {
                        data.setDetailList(new ArrayList<DebtorDetailViewModel>());
                    }   

                    data.setStatus(countPayment+"/"+data.getSubPayMonth());
                    data.setBalance(AppUtil.convertDoubleToStringFormat(balance));
                } else {
                    data.setStatus("ค้างชำระ");
                    data.setBalance(data.getSumTax());
                }
                    
            }

            resultData.setStatus("true");
            resultData.setResultMessage("");

            if (AppUtil.isNullAndSpace(resultData.getResult().getInvoiceId())) {
                resultData.setResult(new DebtorViewModel());
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ResultDebtorViewModel> saveData(DebtorViewModel data) {

        ResultData<ResultDebtorViewModel> resultData = new ResultData<ResultDebtorViewModel>();
        DebtorModel dataSave = new DebtorModel();

        try {
            if (!AppUtil.isNullAndSpace(data.getInvoiceId()) && AppUtil.decryptId(data.getInvoiceId()) > 0) {

                DebtorMapper mapper = new DebtorMapper();
                dataSave = mapper.mapSaveData(data);

                resultData = repo.saveData(dataSave);

            } else {
                resultData.setStatus("false");
                resultData.setResultMessage(message.getMessage("message.debtor.invoiceIdNotEmpty"));
            }
            
        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public static void main(String[] args) {

        ResultData<List<DebtorDetailViewModel>> resultData = new ResultData<List<DebtorDetailViewModel>>();
        DebtorService service = new DebtorService();
        FilterDebtorModel filter = new FilterDebtorModel();
        filter.setInvoiceId("kADE1H9gOnL-GqjEMnzntg");
        filter.setSubPayDate("15/08/2564");
        filter.setSubPayMonth("3");

        resultData = service.getCalculateDebtor(filter); 
        System.out.println("\t"+resultData.getResult());
        /*
        double average = Double.valueOf("999444")/7; 
        System.out.println(Double.valueOf("999444")+"\t"+average); 
        System.out.println("\t"+Math.round(average));

        java.math.BigDecimal bd = new java.math.BigDecimal(average);
        System.out.println("\t"+bd.setScale(0, java.math.RoundingMode.DOWN));
        */
        
    }

    public ResultData<List<DebtorDetailViewModel>> getCalculateDebtor(FilterDebtorModel filter) {

        ResultData<List<DebtorDetailViewModel>> resultData = new ResultData<List<DebtorDetailViewModel>>();

        try {
            
            if (!AppUtil.isNullAndSpace(filter.getInvoiceId()) && AppUtil.decryptId(filter.getInvoiceId()) > 0) {
                
                List<DebtorDetailViewModel> dataList = new ArrayList<>();
                int invoiceId = AppUtil.decryptId(filter.getInvoiceId());
                //int debtorId = AppUtil.decryptId(filter.getDebtorId());
                String subPayDate = filter.getSubPayDate(); //วันที่เริ่มผ่อนชำระ
                int subPayMonth = AppUtil.convertStringToInteger(filter.getSubPayMonth()); //จำนวนเดือนที่ผ่อนชำระ
            
                //get debtor by invoiceId
                ResultData<DebtorViewModel> resultDataDebtor = new ResultData<DebtorViewModel>();
                resultDataDebtor = repo.getData(invoiceId);
                
                if(resultDataDebtor != null && resultDataDebtor.getResult() != null){
                    DebtorViewModel dataDebtor = resultDataDebtor.getResult();

                    ResultData<List<DebtorDetailViewModel>> resultDebtorDetail = null;
                    if(!AppUtil.isNullAndSpace(dataDebtor.getDebtorId()) && AppUtil.decryptId(dataDebtor.getDebtorId()) > 0){
                        //get debtorDetail by debtorId
                        int debtorId = AppUtil.decryptId(dataDebtor.getDebtorId());
                        resultDebtorDetail = repo.getDebtorDetailList(debtorId);

                    }

                    //check DebtorDetail list from database
                    if(resultDebtorDetail != null && resultDebtorDetail.getResult() != null){
                        
                        //<editor-fold defaultstate="collapsed" desc="มี DebtorDetail แล้ว">
                        //TaxTotal = ยอดเงินภาษี
                        //SumTax = รวมภาษี = ยอดเงินภาษี + เงินเพิ่ม
                        double balance = AppUtil.convertStringToDouble(dataDebtor.getSumTax());
                        double average = 0; //(subPayMonth > 0) ? (balance / subPayMonth) : (balance);
                        int countPayment = 0;
                        for (DebtorDetailViewModel debtorDetail : resultDebtorDetail.getResult()) {
                            //<editor-fold defaultstate="collapsed" desc="DebtorDetail at ชำระแล้ว">
                            if("ชำระแล้ว".equalsIgnoreCase(debtorDetail.getPaymentStatus())){
                                double taxTotal = AppUtil.convertStringToDouble(debtorDetail.getTaxTotal()); 
                                balance = balance - taxTotal;
                                countPayment = countPayment + 1;
                                
                                //add ชำระแล้ว กลับเข้า list
                                DebtorDetailViewModel data = new DebtorDetailViewModel();

                                data.setDebtorDetailId(debtorDetail.getDebtorDetailId());
                                data.setDueDate(debtorDetail.getDueDate());
                                data.setDebtorDetail(debtorDetail.getDebtorDetail());
                                data.setTaxTotal(debtorDetail.getTaxTotal());
                                data.setSeq(debtorDetail.getSeq());

                                data.setPaymentStatus("ชำระแล้ว");
                                dataList.add(data);
                            }
                            //</editor-fold>
                        }
                        
                        
                        if(subPayMonth > 0) {
                            
                            //<editor-fold defaultstate="collapsed" desc="มี จำนวนเดือนที่ผ่อนชำระ">
                            if(subPayMonth > countPayment){
                                //จำนวนเดือนที่ผ่อนชำระ มากกว่า จำนวนชำระแล้ว
                                
                                int start = countPayment;
                                int countList = resultDebtorDetail.getResult().size();
                                
                                average = balance / (subPayMonth - countPayment);
                                for (int i = start; i < subPayMonth; i++) {

                                    int seq = i+1;
                                    Date date = DateUtils.toEng(subPayDate);
                                    Calendar c = Calendar.getInstance(); 
                                    c.setTime(date); 
                                    c.add(Calendar.MONTH, (seq-1));
                                    date = c.getTime();
                                    String debtorDetailString = String.format("ค่าภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมัน : %02d/%02d", seq, subPayMonth);
                                    DebtorDetailViewModel data = new DebtorDetailViewModel();

                                    String debtorDetailId = "";
                                    if(countList >= seq) {
                                        //ยังเหลือ DebtorDetailId ที่ค้างชำระ
                                        int index = 1;
                                        for (DebtorDetailViewModel debtorDetail : resultDebtorDetail.getResult()) {
                                            if(seq == index && !"ชำระแล้ว".equalsIgnoreCase(debtorDetail.getPaymentStatus()) && "".equalsIgnoreCase(debtorDetailId)){
                                                debtorDetailId = debtorDetail.getDebtorDetailId();
                                            }
                                            index = index + 1;
                                        }
                                    }
                                    
                                    data.setDebtorDetailId(debtorDetailId);
                                    data.setDueDate(DateUtils.toThai(date));
                                    data.setDebtorDetail(debtorDetailString);
                                    data.setTaxTotal(AppUtil.convertDoubleToStringFormat(average));
                                    data.setSeq(String.valueOf(seq));
                                    data.setPaymentStatus("ค้างชำระ");

                                    dataList.add(data);

                                }
 
                            } else {
                                //(ชำระครบหรือเกินจำนวนผ่อนชำระ) จำนวนเดือนที่ผ่อนชำระ น้อยกว่าหรือเท่ากับ จำนวนชำระแล้ว
                                
                                if(balance > 0){
                                    int countList = resultDebtorDetail.getResult().size();
                                    String debtorDetailId = "";
                                    if(countList > countPayment) {
                                        //ยังเหลือ DebtorDetailId ที่ค้างชำระ
                                        for (DebtorDetailViewModel debtorDetail : resultDebtorDetail.getResult()) {
                                            if(!"ชำระแล้ว".equalsIgnoreCase(debtorDetail.getPaymentStatus()) && "".equalsIgnoreCase(debtorDetailId)){
                                                debtorDetailId = debtorDetail.getDebtorDetailId();
                                            }
                                        }
                                    }
                                    
                                    DebtorDetailViewModel data = new DebtorDetailViewModel();
                                    average = balance;
                                    int seq = countPayment + 1;
                                    Date date = DateUtils.toEng(subPayDate);
                                    Calendar c = Calendar.getInstance(); 
                                    c.setTime(date); 
                                    c.add(Calendar.MONTH, (seq-1));
                                    date = c.getTime();
                                    String debtorDetailString = String.format("ค่าภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมัน : %02d/%02d", seq, seq);
                            
                                    data.setDebtorDetailId(debtorDetailId);
                                    data.setDueDate(DateUtils.toThai(date));
                                    data.setDebtorDetail(debtorDetailString);
                                    data.setTaxTotal(AppUtil.convertDoubleToStringFormat(average));
                                    data.setSeq(String.valueOf(seq));

                                    data.setPaymentStatus("ค้างชำระ");
                                    dataList.add(data);
                                }
                               
                            }
                            //</editor-fold>
                            
                        } else {
                            
                            //<editor-fold defaultstate="collapsed" desc="ไม่มี จำนวนเดือนที่ผ่อนชำระ def. 1">
                            
                            if(balance > 0){
                                DebtorDetailViewModel data = new DebtorDetailViewModel();
                                average = balance;
                                int seq = countPayment + 1;
                                Date date = DateUtils.toEng(subPayDate);
                                Calendar c = Calendar.getInstance(); 
                                c.setTime(date); 
                                c.add(Calendar.MONTH, (seq-1));
                                date = c.getTime();
                                String debtorDetailString = String.format("ค่าภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมัน : %02d/%02d", seq, seq);

                                data.setDebtorDetailId("");
                                data.setDueDate(DateUtils.toThai(date));
                                data.setDebtorDetail(debtorDetailString);
                                data.setTaxTotal(AppUtil.convertDoubleToStringFormat(average));
                                data.setSeq(String.valueOf(seq));

                                data.setPaymentStatus("ค้างชำระ");
                                dataList.add(data);
                            }

                            //</editor-fold>

                        }
                        //</editor-fold>
                        
                    } else {
                        
                        //<editor-fold defaultstate="collapsed" desc="ยังไม่มี DebtorDetail">
                        //TaxTotal = ยอดเงินภาษี
                        //SumTax = รวมภาษี = ยอดเงินภาษี + เงินเพิ่ม
                        double balance = AppUtil.convertStringToDouble(dataDebtor.getSumTax());
                        double average = 0; //(subPayMonth > 0) ? (balance / subPayMonth) : (balance);
                        if(subPayMonth > 0) {
                            
                            //<editor-fold defaultstate="collapsed" desc="มี จำนวนเดือนที่ผ่อนชำระ">
                            average = balance / subPayMonth;
                            for (int i = 0; i < subPayMonth; i++) {

                                int seq = i+1;
                                Date date = DateUtils.toEng(subPayDate);
                                Calendar c = Calendar.getInstance(); 
                                c.setTime(date); 
                                c.add(Calendar.MONTH, (seq-1));
                                date = c.getTime();
                                String debtorDetailString = String.format("ค่าภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมัน : %02d/%02d", seq, subPayMonth);
                                DebtorDetailViewModel data = new DebtorDetailViewModel();

                                data.setDebtorDetailId("");
                                data.setDueDate(DateUtils.toThai(date));
                                data.setDebtorDetail(debtorDetailString);
                                data.setTaxTotal(AppUtil.convertDoubleToStringFormat(average));
                                data.setSeq(String.valueOf(seq));
                                data.setPaymentStatus("ค้างชำระ");

                                dataList.add(data);

                            }//End loop
                            //</editor-fold>
                            
                        } else {
                            
                            //<editor-fold defaultstate="collapsed" desc="ไม่มี จำนวนเดือนที่ผ่อนชำระ def. 1">
                            average = balance;
                            int seq = 1;
                            String debtorDetailString = String.format("ค่าภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมัน : %02d/%02d", seq, 1);
                            DebtorDetailViewModel data = new DebtorDetailViewModel();

                            data.setDebtorDetailId("");
                            data.setDueDate(subPayDate);
                            data.setDebtorDetail(debtorDetailString);
                            data.setTaxTotal(AppUtil.convertDoubleToStringFormat(average));
                            data.setSeq(String.valueOf(seq));
                            data.setPaymentStatus("ค้างชำระ");

                            dataList.add(data);
                            //</editor-fold>

                        }
                        //</editor-fold>
                        
                    }
                    
                    resultData.setResult(dataList);
                    resultData.setStatus("true");
                    resultData.setResultMessage("");

                } else {
                    resultData.setStatus("false");
                    resultData.setResultMessage(message.getMessage("message.debtor.invoiceIdNotEmpty"));
                } //End invoiceId from database

            } else {
                resultData.setStatus("false");
                resultData.setResultMessage(message.getMessage("message.debtor.invoiceIdNotEmpty"));
            } //End invoiceId from req.

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));
        }

        return resultData;
    }

}
