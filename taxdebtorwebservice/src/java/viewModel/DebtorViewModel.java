/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.*;

/**
 *
 * @author User
 */
public class DebtorViewModel extends InvoiceViewModel {
    private String debtorId;
    private String invoiceDate;//วันที่ชำระเงิน
    
    private String taxTotal;//ยอดเงินภาษี
    private String extraMoney;//เงินเพิ่ม
    private String status;
    private String statusDesc;
    private String subPayDate; //วันที่เริ่มผ่อนชำระ
    private String subPayMonth; //จำนวนเดือนที่ผ่อนชำระ
    
    private String retailId; //รหัสผู้ประกอบการค้าปลีก
    private String ownerName; //ชื่อผู้ประกอบการค้าปลีก
    private String stationId; //รหัสสถานประกอบการค้าปลีก
    private String stationName;
    private String refOffice; //รหัสเขต
    private String refOfficeName; //ชื่อเขต
    
    private List<DebtorDetailViewModel> detailList; //รายการผ่อนชำระ
    private String sumTax; //รวม = ยอดเงินภาษี + เงินเพิ่ม
    private String balance; //ยอดเงินคงเหลือ = รวม  - เงินที่ชำระแล้ว
    
    private String monthly; //รหัสเขต
    private String yearly; //ชื่อเขต
    
    private String linkExportInvoice;

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getYearly() {
        return yearly;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getLinkExportInvoice() {
        return linkExportInvoice;
    }

    public void setLinkExportInvoice(String linkExportInvoice) {
        this.linkExportInvoice = linkExportInvoice;
    }

    public String getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(String debtorId) {
        this.debtorId = debtorId;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(String taxTotal) {
        this.taxTotal = taxTotal;
    }

    public String getExtraMoney() {
        return extraMoney;
    }

    public void setExtraMoney(String extraMoney) {
        this.extraMoney = extraMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getSubPayDate() {
        return subPayDate;
    }

    public void setSubPayDate(String subPayDate) {
        this.subPayDate = subPayDate;
    }

    public String getSubPayMonth() {
        return subPayMonth;
    }

    public void setSubPayMonth(String subPayMonth) {
        this.subPayMonth = subPayMonth;
    }
    
    public String getRetailId() {
        return retailId;
    }

    public void setRetailId(String retailId) {
        this.retailId = retailId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getRefOffice() {
        return refOffice;
    }

    public void setRefOffice(String refOffice) {
        this.refOffice = refOffice;
    }

    public String getRefOfficeName() {
        return refOfficeName;
    }

    public void setRefOfficeName(String refOfficeName) {
        this.refOfficeName = refOfficeName;
    }

    public List<DebtorDetailViewModel> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<DebtorDetailViewModel> detailList) {
        this.detailList = detailList;
    }

    public String getSumTax() {
        return sumTax;
    }

    public void setSumTax(String sumTax) {
        this.sumTax = sumTax;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

}
