/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class FilterModel extends BaseClassFilterModel {
    private String startDate;    
    private String endDate;   
    
    private String yearly; //ประจำปี
    private String monthly; //ประจำเดือน
    private String ownerName;  //ชื่อผู้ประกอบการ
    private String warnFormId;  
    private String stationId;  
    private String stationName;   //ชื่อสถานการค้าปลีก
    private String taxNo;     
    private int status;   
    private String statusName;     
    private String reason;     
    private String officeId;   //รหัสสำนักงานเขต
    private List<String> officeCodeList; //สำนักงานเขต แบบ List 

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getWarnFormId() {
        return warnFormId;
    }

    public void setWarnFormId(String warnFormId) {
        this.warnFormId = warnFormId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getYearly() {
        return yearly;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public List<String> getOfficeCodeList() {
        return officeCodeList;
    }

    public void setOfficeCodeList(List<String> officeCodeList) {
        this.officeCodeList = officeCodeList;
    }
    
}
