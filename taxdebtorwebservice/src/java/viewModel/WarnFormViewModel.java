/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.BaseClassModel;

/**
 *
 * @author User
 */
public class WarnFormViewModel  extends BaseClassModel{
    private String yearly; //ประจำปี
    private int monthly; //ประจำเดือน
    private int stationNum;//จำนวนผู้ประกอบกร
    private String stationId;
    private String monthName;
    private String docNo;
    private String docDate;
    private String paymentDate;
    private String taxNo;
    private String ownerName;
    private String stationName;
    private String monthLast ="";
    private String amphurId;
    private String amphurName;
    private int delayNum;
    private String status; //0 = ออกหนังสือ , 1= ส่งเมล์ ,2=ตอบรับแล้ว, 9=ยกเลิก
    private String statusName;
    
    private String warnFormId;
    private String docNo1;
    private String docNo2;
    private String orgName;
    private String orgAddr;
    private String orgOwnerName;
    private String orgOwnerTel;
    private String orgOwnerFax;
    
    private String placePayment;
    private String amount ;
    
    private String email;
    
    private String linkExportWarnForm;
    
    private double extraMoney; // จำนวนเงินเพิ่ม
    private double taxTotal; // จำนวนเงินภาษีรวม

    private String reasonCancel;

    public String getReasonCancel() {
        return reasonCancel;
    }

    public void setReasonCancel(String reasonCancel) {
        this.reasonCancel = reasonCancel;
    }
    
    
    
    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }
    
    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }
    
    public int getStationNum() {
        return stationNum;
    }

    public void setStationNum(int stationNum) {
        this.stationNum = stationNum;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLinkExportWarnForm() {
        return linkExportWarnForm;
    }

    public void setLinkExportWarnForm(String linkExportWarnForm) {
        this.linkExportWarnForm = linkExportWarnForm;
    }

    public String getWarnFormId() {
        return warnFormId;
    }

    public void setWarnFormId(String warnFormId) {
        this.warnFormId = warnFormId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getYearly() {
        return yearly;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public int getMonthly() {
        return monthly;
    }

    public void setMonthly(int monthly) {
        this.monthly = monthly;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getMonthLast() {
        return monthLast;
    }

    public void setMonthLast(String monthLast) {
        this.monthLast = monthLast;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public int getDelayNum() {
        return delayNum;
    }

    public void setDelayNum(int delayNum) {
        this.delayNum = delayNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getDocNo1() {
        return docNo1;
    }

    public void setDocNo1(String docNo1) {
        this.docNo1 = docNo1;
    }

    public String getDocNo2() {
        return docNo2;
    }

    public void setDocNo2(String docNo2) {
        this.docNo2 = docNo2;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgAddr() {
        return orgAddr;
    }

    public void setOrgAddr(String orgAddr) {
        this.orgAddr = orgAddr;
    }

    public String getOrgOwnerName() {
        return orgOwnerName;
    }

    public void setOrgOwnerName(String orgOwnerName) {
        this.orgOwnerName = orgOwnerName;
    }

    public String getOrgOwnerTel() {
        return orgOwnerTel;
    }

    public void setOrgOwnerTel(String orgOwnerTel) {
        this.orgOwnerTel = orgOwnerTel;
    }

    public String getOrgOwnerFax() {
        return orgOwnerFax;
    }

    public void setOrgOwnerFax(String orgOwnerFax) {
        this.orgOwnerFax = orgOwnerFax;
    }

    public String getPlacePayment() {
        return placePayment;
    }

    public void setPlacePayment(String placePayment) {
        this.placePayment = placePayment;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public double getExtraMoney() {
        return extraMoney;
    }

    public void setExtraMoney(double extraMoney) {
        this.extraMoney = extraMoney;
    }

    public double getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(double taxTotal) {
        this.taxTotal = taxTotal;
    }
    
}
