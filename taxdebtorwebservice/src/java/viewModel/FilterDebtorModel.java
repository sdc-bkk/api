/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class FilterDebtorModel extends BaseClassFilterModel {
    private String invoiceId;
    private String taxNo;//เลขประจำตัวผู้เสียภาษี
    private String ownerName; //ชื่อผู้ประกอบการค้าปลีก

    private String debtorId;
    private String subPayDate; //วันที่เริ่มผ่อนชำระ
    private String subPayMonth; //จำนวนเดือนที่ผ่อนชำระ
    
    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(String debtorId) {
        this.debtorId = debtorId;
    }

    public String getSubPayDate() {
        return subPayDate;
    }

    public void setSubPayDate(String subPayDate) {
        this.subPayDate = subPayDate;
    }

    public String getSubPayMonth() {
        return subPayMonth;
    }

    public void setSubPayMonth(String subPayMonth) {
        this.subPayMonth = subPayMonth;
    }

}
