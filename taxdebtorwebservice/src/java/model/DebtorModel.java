/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import model.*;

/**
 *
 * @author User
 */
public class DebtorModel {
    private int debtorId;
    private int invoiceId;
    private String invoiceDate;//วันที่ชำระเงิน
    
    private String taxNo;//เลขประจำตัวผู้เสียภาษี
    private String ownerName; //ชื่อผู้ประกอบการค้าปลีก
    private String stationName; //ชื่อสถานประกอบการค้าปลีก
    
    private double taxTotal;//ยอดเงินภาษี
    private double extraMoney;//เงินเพิ่ม
    private String status;
    private String statusDesc;
    private String subPayDate;
    private int subPayMonth;
    
    private List<DebtorDetailModel> detailList; //รายการผ่อนชำระ

    public int getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(int debtorId) {
        this.debtorId = debtorId;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public double getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(double taxTotal) {
        this.taxTotal = taxTotal;
    }

    public double getExtraMoney() {
        return extraMoney;
    }

    public void setExtraMoney(double extraMoney) {
        this.extraMoney = extraMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getSubPayDate() {
        return subPayDate;
    }

    public void setSubPayDate(String subPayDate) {
        this.subPayDate = subPayDate;
    }

    public int getSubPayMonth() {
        return subPayMonth;
    }

    public void setSubPayMonth(int subPayMonth) {
        this.subPayMonth = subPayMonth;
    }

    public List<DebtorDetailModel> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<DebtorDetailModel> detailList) {
        this.detailList = detailList;
    }

}
