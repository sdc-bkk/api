/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.*;

/**
 *
 * @author User
 */
public class DebtorDetailModel {
    private int debtorDetailId;
    private int debtorId;
    private String debtorDetail;//รายการ
    private String dueDate;//วันที่ชำระ
    private double taxTotal;//จำนวนเงิน
    private String status;
    private String statusDesc;
    private int seq;
    private String paymentStatus;//สถานะการชำระเงิน (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)

    public int getDebtorDetailId() {
        return debtorDetailId;
    }

    public void setDebtorDetailId(int debtorDetailId) {
        this.debtorDetailId = debtorDetailId;
    }

    public int getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(int debtorId) {
        this.debtorId = debtorId;
    }

    public String getDebtorDetail() {
        return debtorDetail;
    }

    public void setDebtorDetail(String debtorDetail) {
        this.debtorDetail = debtorDetail;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public double getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(double taxTotal) {
        this.taxTotal = taxTotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

}
