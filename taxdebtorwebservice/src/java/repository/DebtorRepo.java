/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.DebtorDetailModel;
import model.DebtorModel;
import repository.mapper.DebtorMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.DateUtil;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.DebtorDetailViewModel;
import viewModel.DebtorViewModel;
import viewModel.FilterDebtorModel;
import viewModel.FilterModel;
import viewModel.ResultData;
import viewModel.ResultDebtorViewModel;
import viewModel.ResultPage;

/**
 *
 * @author User
 */
public class DebtorRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<DebtorViewModel>> getList(ResultPage page, FilterDebtorModel filter) throws SQLException {

        ResultData<List<DebtorViewModel>> resultData = new ResultData<List<DebtorViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT I.INVOICE_ID, I.PAYMENT_DATE, I.TAX_NO, I.TAX_FORM03_ID, "
                    + "    F.REF_RETAIL_ID, R.OWNER_NAME, F.REF_STATION_ID, S.STATION_NAME, "
                    + "    I.TOTAL_AMOUNT, I.ACTION_DATE, D.DEBTOR_ID, D.EXTRA_MONEY, D.SUB_PAY_MONTH "
                    + "   ,F.YEARLY, F.MONTHLY "
                    + " FROM INVOICE I "
                    + "    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID "
                    + "    LEFT JOIN RETAIL R ON F.REF_RETAIL_ID = R.RETAIL_ID "
                    + "    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID "
                    + "    LEFT JOIN DEBTOR D ON I.INVOICE_ID = D.INVOICE_ID "
                    + " WHERE I.PAYMENT_STATUS IN (0 , 88) ";
            //PAYMENT_STATUS = 0(ยังไม่ชำระ) , 88(ผ่อนชำระ)
/*
 SELECT I.INVOICE_ID, I.PAYMENT_DATE, I.TAX_NO, I.TAX_FORM03_ID, 
    F.REF_RETAIL_ID, R.OWNER_NAME, F.REF_STATION_ID, S.STATION_NAME, 
    I.TOTAL_AMOUNT, I.ACTION_DATE, D.DEBTOR_ID, D.EXTRA_MONEY, D.SUB_PAY_MONTH 
 FROM INVOICE I 
    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID 
    LEFT JOIN RETAIL R ON F.REF_RETAIL_ID = R.RETAIL_ID 
    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID 
    LEFT JOIN DEBTOR D ON I.INVOICE_ID = D.INVOICE_ID 
 WHERE I.PAYMENT_STATUS IN (0 , 88) 
             */
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (I.TAX_NO LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                sql += " AND (R.OWNER_NAME LIKE ?) ";
            }

            ps.setSql(sql);

            String orderBy = " PAYMENT_DATE desc, OWNER_NAME, STATION_NAME ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }

            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                ps.setString(i++, "%" + filter.getOwnerName() + "%");
            }

            rs = ps.executeQuery();

            List<DebtorViewModel> dataList = new ArrayList<>();
            dataList = new DebtorMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<DebtorViewModel> getData(int id) throws SQLException {

        ResultData<DebtorViewModel> resultData = new ResultData<DebtorViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get INVOICE ">
            String sql = " SELECT I.INVOICE_ID, I.PAYMENT_DATE, I.TAX_NO, I.TAX_FORM03_ID, "
                    + "    F.REF_RETAIL_ID, R.OWNER_NAME, F.REF_STATION_ID, S.STATION_NAME, S.REF_OFFICE, S.REF_OFFICE_NAME, "
                    + "    I.TOTAL_AMOUNT, I.ACTION_DATE, D.DEBTOR_ID, D.EXTRA_MONEY, D.SUB_PAY_MONTH, D.SUB_PAY_DATE "
                    + " FROM INVOICE I "
                    + "    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID "
                    + "    LEFT JOIN RETAIL R ON F.REF_RETAIL_ID = R.RETAIL_ID "
                    + "    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID "
                    + "    LEFT JOIN DEBTOR D ON I.INVOICE_ID = D.INVOICE_ID "
                    + " WHERE I.INVOICE_ID = ? ";
            /*
 SELECT I.INVOICE_ID, I.PAYMENT_DATE, I.TAX_NO, I.TAX_FORM03_ID, 
    F.REF_RETAIL_ID, R.OWNER_NAME, F.REF_STATION_ID, S.STATION_NAME, S.REF_OFFICE, S.REF_OFFICE_NAME, 
    I.TOTAL_AMOUNT, I.ACTION_DATE, D.DEBTOR_ID, D.EXTRA_MONEY, D.SUB_PAY_MONTH, D.SUB_PAY_DATE  
 FROM INVOICE I 
    LEFT JOIN TAX_FORM03 F ON I.TAX_FORM03_ID = F.TAX_FORM03_ID 
    LEFT JOIN RETAIL R ON F.REF_RETAIL_ID = R.RETAIL_ID 
    LEFT JOIN RETAIL_STATION S ON F.REF_STATION_ID = S.RETAIL_STATION_ID 
    LEFT JOIN DEBTOR D ON I.INVOICE_ID = D.INVOICE_ID 
 WHERE I.INVOICE_ID = 1 
             */
            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            DebtorViewModel data = new DebtorMapper(rs).mapDataInvoiceDebtor();
            // </editor-fold>

            resultData.setStatus("true");
            resultData.setResult(data);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData saveData(DebtorModel data) throws SQLException {

        boolean result = false;
        ResultDebtorViewModel dataOut = new ResultDebtorViewModel();
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        PreparedStatementDB psSelect = new PreparedStatementDB(conn);
        ResultSet rsSelect = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";
            String actionDate = "";

            //check Debtor by InvoiceId
            // <editor-fold defaultstate="collapsed" desc=" get DEBTOR by INVOICE ">
            i = 1;
            sql = " SELECT I.INVOICE_ID, D.DEBTOR_ID, I.ACTION_DATE "
                    + " FROM INVOICE I "
                    + "    LEFT JOIN DEBTOR D ON I.INVOICE_ID = D.INVOICE_ID "
                    + " WHERE I.INVOICE_ID = ? ";
            /*
SELECT I.INVOICE_ID, D.DEBTOR_ID, I.ACTION_DATE  
FROM INVOICE I 
LEFT JOIN DEBTOR D ON I.INVOICE_ID = D.INVOICE_ID 
WHERE I.INVOICE_ID = 1 
             */
            psSelect = new PreparedStatementDB(conn);
            psSelect.setSql(sql);

            psSelect.setInt(i++, data.getInvoiceId());

            rsSelect = psSelect.executeQuery();

            while (rsSelect.next()) {

                //data.setInvoiceId(rsSelect.getInt("INVOICE_ID"));
                if (data.getDebtorId() <= 0 && data.getInvoiceId() > 0) {
                    data.setDebtorId(rsSelect.getInt("DEBTOR_ID"));
                }

                if (rsSelect.getDate("ACTION_DATE") != null) {
                    actionDate = DateUtils.toThai(rsSelect.getDate("ACTION_DATE"));
                } else {
                    actionDate = "";
                }

            }

            // </editor-fold>
            //<editor-fold defaultstate="collapsed" desc="sql DEBTOR">
            if (data.getDebtorId() == 0) {

                //<editor-fold defaultstate="collapsed" desc="sql Insert DEBTOR">
                i = 1;
                sql = "INSERT INTO DEBTOR ( "
                        + "  INVOICE_DATE "
                        + ", INVOICE_ID "
                        + ", TAX_NO "
                        + ", OWNER_NAME "
                        + ", STATION_NAME "
                        + ", TAX_TOTAL "
                        + ", EXTRA_MONEY "
                        + ", STATUS "
                        + ", STATUS_DESC "
                        + ", SUB_PAY_DATE "
                        + ", SUB_PAY_MONTH "
                        + ") VALUES (?,?, ?,?,?,?,?, NULL,NULL,?,?) ";

                ps = conn.prepareStatement(sql, new String[]{"DEBTOR_ID"});
                //</editor-fold>

            } else {

                //<editor-fold defaultstate="collapsed" desc="sql Update DEBTOR">
                i = 1;
                sql = "UPDATE DEBTOR SET "
                        + "  INVOICE_DATE = ? "
                        + ", INVOICE_ID = ? "
                        + ", TAX_NO = ? "
                        + ", OWNER_NAME = ? "
                        + ", STATION_NAME = ? "
                        + ", TAX_TOTAL = ? "
                        + ", EXTRA_MONEY = ? "
                        + ", SUB_PAY_DATE  = ? "
                        + ", SUB_PAY_MONTH  = ? "
                        + " WHERE DEBTOR_ID = ? ";

                ps = conn.prepareStatement(sql);
                //</editor-fold>

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter DEBTOR">
            ps.setDate(i++, AppUtil.toDateSql(data.getInvoiceDate())); //INVOICE_DATE
            ps.setInt(i++, data.getInvoiceId()); //INVOICE_ID

            ps.setString(i++, data.getTaxNo()); //TAX_NO
            ps.setString(i++, data.getOwnerName()); //OWNER_NAME
            ps.setString(i++, data.getStationName()); //STATION_NAME

            ps.setDouble(i++, data.getTaxTotal()); //TAX_TOTAL
            ps.setDouble(i++, data.getExtraMoney()); //EXTRA_MONEY

            ps.setDate(i++, AppUtil.toDateSql(data.getSubPayDate())); //SUB_PAY_DATE
            ps.setInt(i++, data.getSubPayMonth()); //SUB_PAY_MONTH

            if (data.getDebtorId() == 0) {
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setDebtorId(id);
            } else {
                ps.setInt(i++, data.getDebtorId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            //อัพเดทข้อมูลรายการผ่อนชำระ
            //<editor-fold defaultstate="collapsed" desc="INSERT DEBTOR_DETAIL">
            if (data.getDebtorId() > 0 && data.getDetailList() != null && !data.getDetailList().isEmpty()) {

                //<editor-fold defaultstate="collapsed" desc="DELETE NOT USE">
                //<editor-fold defaultstate="collapsed" desc="ID NOT FOR DELETE">
                String delSeq = "";
                int size = data.getDetailList().size();
                List empty = new ArrayList();

                for (int j = 0; j < size; j++) {

                    DebtorDetailModel detail = data.getDetailList().get(j);

                    if (!AppUtil.isNullAndZero(detail.getSeq())) {
                        empty.add(detail.getSeq());
                    }
                }

                delSeq = empty.isEmpty() ? "" : empty.toString().replace("[", "").replace("]", "");
                //</editor-fold>

                String delStstus = "";
                List emptyStstus = new ArrayList();
                //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
                empty.add("1");
                delStstus = emptyStstus.isEmpty() ? "" : emptyStstus.toString().replace("[", "").replace("]", "");
                i = 1;

                sql = " DELETE FROM DEBTOR_DETAIL "
                        + " WHERE DEBTOR_ID = ? ";

                if (!delStstus.equals("")) {
                    sql += "    AND PAYMENT_STATUS NOT IN (" + delStstus + ") ";
                }

                if (!delSeq.equals("")) {
                    sql += "    AND SEQ NOT IN (" + delSeq + ") ";
                }

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, data.getDebtorId());

                ps.executeUpdate();

                //</editor-fold>
                for (DebtorDetailModel detail : data.getDetailList()) {
                    //PAYMENT_STATUS = (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
                    //อื่นๆ = ค้างชำระ , 1 = ชำระแล้ว
                    if (AppUtil.isNull(detail.getPaymentStatus()) || !"ชำระแล้ว".equalsIgnoreCase(detail.getPaymentStatus())) {

                        boolean resultDetail = false;

                        //check DebtorDetail by DebtorId and seq
                        // <editor-fold defaultstate="collapsed" desc=" get check DEBTOR_DETAIL by INVOICE ">
                        i = 1;
                        sql = " SELECT DD.DEBTOR_DETAIL_ID, DD.DEBTOR_ID, DD.SEQ, DD.PAYMENT_STATUS "
                                + " FROM DEBTOR_DETAIL DD "
                                + " WHERE DD.DEBTOR_ID = ? AND DD.SEQ = ? ";

                        psSelect = new PreparedStatementDB(conn);
                        psSelect.setSql(sql);

                        psSelect.setInt(i++, data.getDebtorId());
                        psSelect.setInt(i++, detail.getSeq()); //SEQ

                        rsSelect = psSelect.executeQuery();

                        while (rsSelect.next()) {

                            //data.setDebtorId(rsSelect.getInt("DEBTOR_ID"));
                            if (detail.getDebtorDetailId() <= 0) {
                                detail.setDebtorDetailId(rsSelect.getInt("DEBTOR_DETAIL_ID"));
                            }

                            if (!AppUtil.isNull(rsSelect.getString("PAYMENT_STATUS")) && "1".equalsIgnoreCase(rsSelect.getString("PAYMENT_STATUS"))) {
                                detail.setPaymentStatus("ชำระแล้ว");
                            } else {
                                detail.setPaymentStatus("ค้างชำระ");
                            }

                        }

                        // </editor-fold>
                        if (AppUtil.isNull(detail.getPaymentStatus()) || !"ชำระแล้ว".equalsIgnoreCase(detail.getPaymentStatus())) {

                            //<editor-fold defaultstate="collapsed" desc="sql DEBTOR_DETAIL">
                            if (detail.getDebtorDetailId() == 0) {
                                i = 1;
                                sql = "INSERT INTO DEBTOR_DETAIL ( "
                                        + "  DEBTOR_ID "
                                        + ", DEBTOR_DETAIL "
                                        + ", DUE_DATE "
                                        + ", TAX_TOTAL "
                                        + ", STATUS "
                                        + ", STATUS_DESC "
                                        + ", SEQ "
                                        + ", PAYMENT_STATUS "
                                        + ") VALUES (?,?,?,?, NULL,NULL,?,?) ";

                                ps = conn.prepareStatement(sql, new String[]{"DEBTOR_DETAIL_ID"});

                            } else {
                                i = 1;
                                sql = "UPDATE DEBTOR_DETAIL SET "
                                        + "  DEBTOR_ID = ? "
                                        + ", DEBTOR_DETAIL = ? "
                                        + ", DUE_DATE = ? "
                                        + ", TAX_TOTAL = ? "
                                        + ", SEQ = ? "
                                        //+ ", PAYMENT_STATUS = ? "
                                        + " WHERE DEBTOR_DETAIL_ID = ? ";

                                ps = conn.prepareStatement(sql);

                            }
                            //</editor-fold>

                            //<editor-fold defaultstate="collapsed" desc="set parameter DEBTOR_DETAIL">
                            ps.setInt(i++, data.getDebtorId()); //DEBTOR_ID

                            ps.setString(i++, detail.getDebtorDetail()); //DEBTOR_DETAIL
                            ps.setDate(i++, AppUtil.toDateSql(detail.getDueDate())); //DUE_DATE
                            ps.setDouble(i++, detail.getTaxTotal()); //TAX_TOTAL
                            ps.setInt(i++, detail.getSeq()); //SEQ

                            if (detail.getDebtorDetailId() == 0) {
                                ps.setString(i++, "0"); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)

                                resultDetail = ps.executeUpdate() != 0;

                                ResultSet rs = ps.getGeneratedKeys();
                                int id = 0;
                                while (rs.next()) {
                                    id = rs.getInt(1);
                                }

                                if (id <= 0) {
                                    throw new Exception();
                                }

                                detail.setDebtorDetailId(id);
                            } else {
                                ps.setInt(i++, detail.getDebtorDetailId());

                                resultDetail = ps.executeUpdate() != 0;
                            }
                            //</editor-fold>

                        }

                    }
                }

            }
            //</editor-fold>

            conn.commit();

            //dataOut.setBmaBillNo(data.getBmaBillNo());
            dataOut.setDebtorId(AppUtil.encryptId(data.getDebtorId()));

            resultData.setResult(dataOut);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(dataOut);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

            psSelect.resultSetClose(rsSelect);
            psSelect.closeConnection();
        }

        return resultData;
    }

    public ResultData<List<DebtorDetailViewModel>> getDebtorDetailList(int debtorId) throws SQLException {

        ResultData<List<DebtorDetailViewModel>> resultData = new ResultData<List<DebtorDetailViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT DEBTOR_DETAIL_ID, DEBTOR_ID, DEBTOR_DETAIL, DUE_DATE, "
                    + "    TAX_TOTAL, SEQ, PAYMENT_STATUS "
                    + " FROM DEBTOR_DETAIL D "
                    + " WHERE DEBTOR_ID = ? ";
            /*
 SELECT DEBTOR_DETAIL_ID, DEBTOR_ID, DEBTOR_DETAIL, DUE_DATE, 
    TAX_TOTAL, SEQ, PAYMENT_STATUS 
 FROM DEBTOR_DETAIL D 
 WHERE DEBTOR_ID = 1  
             */
            ps.setSql(sql);

            String orderBy = " SEQ, DUE_DATE DESC ";
            ps.setOrderBy(orderBy);

            ps.setInt(i++, debtorId);

            rs = ps.executeQuery();

            List<DebtorDetailViewModel> dataList = new ArrayList<>();
            dataList = new DebtorMapper(rs).mapDebtorDetailList();

            resultData.setResult(dataList);

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

}
