/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;

/**
 *
 * @author User
 */
public class AppConfigRepo {
    private final ConnnectionDB connDB = new ConnnectionDB();
    
    public int GetRunning(String keyName, int yearly) throws SQLException {
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        int pk = 0;

        try {
            ps.setAutoCommit(false);

            int i = 1;
//            String keyName = "RUNNING_" + tableName;
            String value = "";

            //Get current value
            String sql = "SELECT VALUE FROM APP_CONFIG WHERE KEY = ? AND YEARLY =? ";
            ps.setSql(sql);

            ps.setString(i++, keyName);
            ps.setInt(i++, yearly);

            rs = ps.executeQuery();

            while (rs.next()) {
                value = rs.getString("VALUE") == null ? "" : rs.getString("VALUE");
            }

            boolean result = false;
            if (!"".equals(value)) {
                //กรณีมีข้อมูลแล้วให้ update next value
                sql = "UPDATE APP_CONFIG SET VALUE = ? WHERE KEY = ? AND YEARLY =? ";
                ps.setSql(sql);

                i = 1;
                ps.setString(i++, Integer.toString(Integer.parseInt(value) + 1));
                ps.setString(i++, keyName);
                ps.setInt(i++, yearly);

                result = ps.executeUpdate();

            } else {
                //กรณีที่ไม่มีช้อมุลใน database ให้ insert
                value = "1";
                sql = "INSERT INTO APP_CONFIG (\"KEY\",\"VALUE\",\"REMARK\",YEARLY) VALUES (?,?,?,?) ";
                ps.setSql(sql);

                i = 1;
                ps.setString(i++, keyName);
                ps.setString(i++, Integer.toString(Integer.parseInt(value) + 1));
                ps.setString(i++, "RUNNING ของ  " + keyName + " ปี " + String.valueOf(yearly));
                ps.setInt(i++, yearly);

                result = ps.executeUpdate();
            }

            pk = Integer.parseInt(value);
            if (result = true) {
//TO DO
            }

            conn.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return pk;
    }
}
