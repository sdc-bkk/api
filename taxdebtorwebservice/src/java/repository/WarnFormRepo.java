/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.StatusWarnformEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import repository.mapper.WarnFormMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.DateUtil;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.FilterModel;
import viewModel.ResultData;
import viewModel.ResultPage;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class WarnFormRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<List<WarnFormViewModel>> getList(ResultPage page, FilterModel filter) throws SQLException {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "select * from(\n"
                    + "select date_all.*,station.* from \n"
                    + "(select to_char( add_months( start_date, level-1 ), 'mm' , 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') MONTHNAME\n"
                    + "        ,to_char( add_months( start_date, level-1 ), 'yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI' ) YEARNAME\n"
                    + "      from (select add_months(sysdate,-36) start_date, sysdate end_date from dual)\n"
                    + "     connect by level <= months_between( trunc(end_date,'MM'), trunc(start_date,'MM') ) * + 1 ) date_all\n"
                    + "cross join\n"
                    + "(select S.RETAIL_STATION_ID,RETAIL.OWNER_NAME , RETAIL.TAX_NO , S.STATION_NAME, s.CREATED_DATE, s.REF_OFFICE, s.REF_OFFICE_NAME\n"
                    + "FROM RETAIL_STATION S LEFT JOIN RETAIL ON s.RETAIL_ID = RETAIL.RETAIL_ID ) station\n"
                    + "where (TO_CHAR(station.CREATED_DATE, 'mm' ) <= date_all.MONTHNAME and TO_CHAR(station.CREATED_DATE, 'yyyy' ) <= date_all.YEARNAME)\n"
                    + ")result_date\n"
                    + "left join \n"
                    + "(select  TAX_FORM03.ref_station_id,TAX_FORM03.tax_form03_id, TAX_FORM03.monthly,TAX_FORM03.yearly from TAX_FORM03 \n"
                    + ")result_form03 on result_date.RETAIL_STATION_ID = result_form03.ref_station_id\n"
                    + "and result_date.MONTHNAME = result_form03.monthly and  result_date.YEARNAME = result_form03.yearly\n"
                    + "where result_form03.tax_form03_id is null ";

            //search office
            if (filter.getOfficeCodeList() != null) {
                if (!filter.getOfficeCodeList().isEmpty()) {
                    String sqlOffice = " AND ( ";
                    int j = 1;
                    for (String s : filter.getOfficeCodeList()) {
                        if (!AppUtil.isNullAndSpace(s)) {
                            if (j != 1) {
                                sqlOffice += "  OR  ";
                            }
                            sqlOffice += " (result_date.REF_OFFICE = " +  AppUtil.decryptId(s) + ") ";
                        }
                        j++;
                    }
                    sqlOffice += " ) ";
                    sql += sqlOffice;
                }
            }
            
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                sql += " AND (result_date.TAX_NO LIKE ?) ";
            }
            
            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                sql += " AND (result_date.OWNER_NAME LIKE ?) ";
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                sql += " AND (result_date.STATION_NAME LIKE ?) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeId())) {
                sql += " AND (result_date.REF_OFFICE = ?) ";
            }
            
            ps.setSql(sql);

            String orderBy = " YEARNAME desc, MONTHNAME desc,OWNER_NAME,STATION_NAME  ";
            ps.setOrderBy(orderBy);

            if (page != null) {
                ps.setResultPage(page);
            }

            
            if (!AppUtil.isNullAndSpace(filter.getTaxNo())) {
                ps.setString(i++, filter.getTaxNo());
            }
            
            if (!AppUtil.isNullAndSpace(filter.getOwnerName())) {
                ps.setString(i++, "%" + filter.getOwnerName() +"%" );
            }
            
            if (!AppUtil.isNullAndSpace(filter.getStationName())) {
                ps.setString(i++, "%" + filter.getStationName() +"%" );
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeId())) {
                ps.setInt(i++, AppUtil.decryptId(filter.getOfficeId()));
            }

            rs = ps.executeQuery();

            List<WarnFormViewModel> dataList = new ArrayList<>();
            dataList = new WarnFormMapper(rs).mapList();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<WarnFormViewModel>> getListLog(ResultPage page, FilterModel filter) throws SQLException {

        ResultData<List<WarnFormViewModel>> resultData = new ResultData<List<WarnFormViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            
            String sql = "select * FROM WARN_FORM "
                    + " WHERE RETAIL_STATION_ID = ? "
                    + " AND YEARLY = ? "
                    + " AND MONTHLY = ?  AND WARN_TYPE ='F' ";

            ps.setSql(sql);

            int id = AppUtil.decryptId(filter.getStationId());
            ps.setInt(i++, id);
            ps.setString(i++, filter.getYearly());
            ps.setString(i++, filter.getMonthly());

            String orderBy = " CREATED_DATE DESC  ";
            ps.setOrderBy(orderBy);
            
            if (page != null) {
                ps.setResultPage(page);
            }

            rs = ps.executeQuery();

            List<WarnFormViewModel> dataList = new ArrayList<>();
            dataList = new WarnFormMapper(rs).mapListLog();

            resultData.setResult(dataList);

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

            if (dataList == null || dataList.isEmpty()) {
                resultData.setStatus("true");
                resultData.setResultMessage(message.getMessage("message.list.nodata"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.list.error"));

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public List<WarnFormViewModel> getDataWarnByStationId(int id) throws SQLException {

        Integer countStation = 0;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        List<WarnFormViewModel> resultList = new ArrayList<>();
        try {

            ps.setAutoCommit(false);

            String sql = " SELECT DISTINCT RETAIL.TAX_NO , RETAIL.OWNER_NAME , RETAIL_STATION.STATION_NAME ,WARN_FORM.YEARLY,WARN_FORM.MONTHLY ,WARN_FORM.CANCEL_REASON  \n"
                    + "FROM  WARN_FORM\n"
                    + "LEFT JOIN RETAIL_STATION ON WARN_FORM.RETAIL_STATION_ID = RETAIL_STATION.RETAIL_STATION_ID\n"
                    + "LEFT JOIN RETAIL ON RETAIL_STATION.RETAIL_ID = RETAIL.RETAIL_ID\n"
                    + "WHERE RETAIL.TAX_NO IS NOT NULL AND WARN_FORM_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();
            String taxNo = "";
            String ownerName = "";
            String stationName = "";
            String yearly = "";
            String reason = "";
            int monthly = 0;
            String monthName = "";
            while (rs.next()) {
                taxNo = rs.getString("TAX_NO");
                ownerName = rs.getString("OWNER_NAME");
                stationName = rs.getString("STATION_NAME");
                yearly = rs.getString("YEARLY");
                monthly = rs.getInt("MONTHLY");
                monthName = (AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));
                reason = rs.getString("CANCEL_REASON");
            }

            sql = " SELECT DISTINCT EMAIL FROM MEMBER_OF_RETAIL\n"
                    + "WHERE TAX_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, taxNo);

            rs = ps.executeQuery();
            while (rs.next()) {
                WarnFormViewModel model = new WarnFormViewModel();
                model.setEmail(rs.getString("EMAIL"));
                model.setMonthly(monthly);
                model.setYearly(yearly);
                model.setMonthName(monthName);
                model.setOwnerName(ownerName);
                model.setStationName(stationName);
                model.setReasonCancel(reason);
                resultList.add(model);
            }

        } catch (Exception e) {

        }
        return resultList;

    }

    public ResultData saveData(WarnFormViewModel data) throws SQLException {

        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="sql WARN_FORM">
            sql = "INSERT INTO WARN_FORM ( "
                    + "  RETAIL_STATION_ID "
                    + ", STATION_NAME "
                    + ", DOC_NO1 "
                    + ", DOC_NO2 "
                    + ", ORG_NAME "
                    + ", ORG_ADDR "
                    + ", DOC_DATE "
                    + ", YEARLY "
                    + ", MONTHLY "
//                    + ", AMOUNT "
                    + ", PLACE_PAYMENT "
                    + ", ORG_OWNER_NAME "
                    + ", ORG_OWNER_TEL "
                    + ", ORG_OWNER_FAX "
                    + ", WARN_TYPE "
                    + ", STATUS "
                    + ", STATUS_NAME "
                    + ", CREATED_DATE "
                    + ", CREATED_BY "
                    + ") VALUES (?,?,?,?,?,?,?  ,?,?  ,?,?,?,?  ,?,?,?, SYSDATE,?) ";

            ps = conn.prepareStatement(sql, new String[]{"WARN_FORM_ID"});
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setInt(i++, AppUtil.decryptId(data.getStationId()));
            ps.setString(i++, data.getStationName());
            ps.setString(i++, data.getDocNo1());
            ps.setString(i++, data.getDocNo2());
            ps.setString(i++, data.getOrgName());
            ps.setString(i++, data.getOrgAddr());
            ps.setDate(i++, AppUtil.toDateSql(data.getDocDate()));

            ps.setString(i++, data.getYearly());
            ps.setInt(i++, data.getMonthly());
//            ps.setDouble(i++, data.getAmount());

            ps.setString(i++, data.getPlacePayment());
            ps.setString(i++, data.getOrgOwnerName());
            ps.setString(i++, data.getOrgOwnerTel());
            ps.setString(i++, data.getOrgOwnerFax());

            ps.setString(i++, "F");
            ps.setInt(i++, StatusWarnformEnum.Create.value());
            ps.setString(i++, StatusWarnformEnum.Create.displayNameTH());

            if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                ps.setString(i++, data.getUpdatedBy());
            } else {
                ps.setNull(i++, java.sql.Types.NVARCHAR);
            }

            result = ps.executeUpdate() != 0;

            ResultSet rs = ps.getGeneratedKeys();
            int id = 0;
            while (rs.next()) {
                id = rs.getInt(1);
            }

            if (id <= 0) {
                throw new Exception();
            }

            data.setWarnFormId(AppUtil.encrypt(Integer.toString(id)));
            //</editor-fold>

            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultData deleteData(List<Integer> idList, FilterModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id list">
            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="delete ">
                sql = " DELETE FROM WARN_FORM  "
                        + " WHERE WARN_FORM_ID = ? ";
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();

                //</editor-fold>
            }
            //</editor-fold>

            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.delete.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.delete.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    public ResultData updateStatus(FilterModel filter) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="delete ">
            sql = " UPDATE WARN_FORM  SET STATUS = ? ,STATUS_NAME = ? ,CANCEL_REASON = ? , SEND_MAIL_DATE = SYSDATE "
                    + " WHERE WARN_FORM_ID = ? ";
            i = 1;
            ps.setSql(sql);

            ps.setInt(i++, filter.getStatus());
            ps.setString(i++, filter.getStatusName());
            ps.setString(i++, filter.getReason());
            ps.setInt(i++, AppUtil.decryptId(filter.getWarnFormId()));

            result = ps.executeUpdate();

            //</editor-fold>
            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMessage(message.getMessage("message.save.success"));

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());
            resultData.setResultMessage(message.getMessage("message.save.notsuccess"));

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }

    private String getColumnDB(String orderBy) {

        String str = " TAX_FORM01_RETAIL_ID ";

        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase("ownerName")) {
                str = " OWNER_NAME ";
            } else if (orderBy.equalsIgnoreCase("amphurName")) {
                str = " AMPHUR_NAME ";
            } else if (orderBy.equalsIgnoreCase("createdDate")) {
                str = " CREATED_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            } else {
                str = " CREATED_DATE ";
            }
        }

        return str;

    }

}
