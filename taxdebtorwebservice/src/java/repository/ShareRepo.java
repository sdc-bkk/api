/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.StationModel;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class ShareRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public StationModel getDataStationById(int id) throws SQLException {

        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        StationModel result = new StationModel();
        try {

            ps.setAutoCommit(false);

            String sql = " SELECT RETAIL_STATION.RETAIL_STATION_ID,  RETAIL_STATION.STATION_NAME, RETAIL.OWNER_NAME\n"
                    + ",RETAIL_STATION.REF_OFFICE,RETAIL_STATION.REF_OFFICE_CODE,RETAIL_STATION.REF_OFFICE_NAME\n"
                    + "FROM RETAIL_STATION LEFT JOIN RETAIL ON RETAIL_STATION.RETAIL_ID = RETAIL.RETAIL_ID\n"
                    + "WHERE RETAIL_STATION.RETAIL_STATION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();
            while (rs.next()) {
                result.setStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));
                result.setStationName(rs.getString("STATION_NAME"));
                result.setOwnerName(rs.getString("OWNER_NAME"));
                result.setOffice(rs.getString("REF_OFFICE"));
                result.setOfficeCode(rs.getString("REF_OFFICE_CODE"));
                result.setOfficeName(rs.getString("REF_OFFICE_NAME"));
            }

        } catch (Exception e) {

        }
        return result;

    }

}
