/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import repository.WarnPayRepo;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class WarnPayMapper {

    private ResultSet rs = null;

    public WarnPayMapper() {

    }

    public WarnPayMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<WarnFormViewModel> mapList() {
        List<WarnFormViewModel> dataList = new ArrayList<WarnFormViewModel>();

        try {

            while (rs.next()) {
                WarnFormViewModel data = new WarnFormViewModel();
                data.setStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));

                data.setYearly(AppUtil.checkNullData(rs.getString("YEARLY")));
                data.setMonthly(AppUtil.checkNullData(rs.getInt("MONTHLY")));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                data.setAmphurId(AppUtil.encryptId(rs.getInt("REF_OFFICE")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));

                data.setTaxTotal(rs.getDouble("TAX_TOTAL"));
                data.setExtraMoney(rs.getDouble("EXTRA_MONEY"));
//                data.setAmount(rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY"));
                data.setAmount(String.valueOf((rs.getDouble("TAX_TOTAL") + rs.getDouble("EXTRA_MONEY"))));
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

    public List<WarnFormViewModel> mapListLog() {

        List<WarnFormViewModel> dataList = new ArrayList<WarnFormViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportWarn = appConfig.value("export_warnpay");
        try {

            while (rs.next()) {
                WarnFormViewModel data = new WarnFormViewModel();
                data.setWarnFormId(AppUtil.encryptId(rs.getInt("WARN_FORM_ID")));
                data.setStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));

                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setYearly(AppUtil.checkNullData(rs.getString("YEARLY")));
                data.setMonthly(AppUtil.checkNullData(rs.getInt("MONTHLY")));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));

                data.setLinkExportWarnForm(linkExportWarn + data.getWarnFormId());

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }
}
