/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.DebtorDetailModel;
import model.DebtorModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.DebtorDetailViewModel;
import viewModel.DebtorViewModel;

/**
 *
 * @author User
 */
public class DebtorMapper {

    private ResultSet rs = null;

    public DebtorMapper() {

    }

    public DebtorMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<DebtorViewModel> mapList() {

        List<DebtorViewModel> dataList = new ArrayList<DebtorViewModel>();
        AppConfig appConfig = new AppConfig();
        String linkExportInvoice = appConfig.value("export_invoice");

        try {
            while (rs.next()) {
                DebtorViewModel data = new DebtorViewModel();
                
                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                data.setDebtorId(AppUtil.encryptId(rs.getInt("DEBTOR_ID")));

                if (rs.getDate("PAYMENT_DATE") != null) {
                    data.setPaymentDate(DateUtils.toThai(rs.getDate("PAYMENT_DATE")));
                } else {
                    data.setPaymentDate("");
                }
                
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                data.setTaxTotal(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                double taxTotal = AppUtil.checkNullData(rs.getDouble("TOTAL_AMOUNT"));
                data.setExtraMoney(AppUtil.convertDoubleToStringFormat(rs.getDouble("EXTRA_MONEY")));
                double extraMoney = AppUtil.checkNullData(rs.getDouble("EXTRA_MONEY"));
                
                //รวมภาษี = ยอดเงินภาษี + เงินเพิ่ม
                data.setSumTax(AppUtil.convertDoubleToStringFormat((taxTotal + extraMoney)));
                
                //แบ่งชำระ
                data.setSubPayMonth(String.valueOf(AppUtil.checkNullData(rs.getInt("SUB_PAY_MONTH"))));
                
                
                data.setStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                data.setMonthly(AppUtil.checkNullData(rs.getString("MONTHLY")));
                data.setYearly(AppUtil.checkNullData(rs.getString("YEARLY")));
                
                if (rs.getDate("ACTION_DATE") != null) {
                    data.setActionDate(DateUtils.toThai(rs.getDate("ACTION_DATE")));
                } else {
                    data.setActionDate("");
                }
                
                data.setLinkExportInvoice(linkExportInvoice + data.getInvoiceId());
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

    public DebtorViewModel mapDataInvoiceDebtor() {

        DebtorViewModel data = new DebtorViewModel();

        try {

            while (rs.next()) {
                
                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                data.setDebtorId(AppUtil.encryptId(rs.getInt("DEBTOR_ID")));

                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                //data.setRefOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setRefOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                
                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                data.setTaxTotal(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                double taxTotal = AppUtil.checkNullData(rs.getDouble("TOTAL_AMOUNT"));
                data.setExtraMoney(AppUtil.convertDoubleToStringFormat(rs.getDouble("EXTRA_MONEY")));
                double extraMoney = AppUtil.checkNullData(rs.getDouble("EXTRA_MONEY"));
                
                //รวมภาษี = ยอดเงินภาษี + เงินเพิ่ม
                data.setSumTax(AppUtil.convertDoubleToStringFormat((taxTotal + extraMoney)));
                
                //แบ่งชำระ
                data.setSubPayMonth(String.valueOf(AppUtil.checkNullData(rs.getInt("SUB_PAY_MONTH"))));
                
                if (rs.getDate("SUB_PAY_DATE") != null) {
                    data.setSubPayDate(DateUtils.toThai(rs.getDate("SUB_PAY_DATE")));
                } else {
                    data.setSubPayDate("");
                }
                 
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public DebtorModel mapSaveData(DebtorViewModel dataView) {

        DebtorModel data = new DebtorModel();

        try {
            
            data.setDebtorId(AppUtil.decryptId(dataView.getDebtorId()));
            data.setInvoiceId(AppUtil.decryptId(dataView.getInvoiceId()));

            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
            data.setOwnerName(AppUtil.checkNullData(dataView.getOwnerName()));
            data.setStationName(AppUtil.checkNullData(dataView.getStationName()));
                
            data.setTaxTotal(AppUtil.convertStringToDouble(dataView.getTaxTotal()));
            data.setExtraMoney(AppUtil.convertStringToDouble(dataView.getExtraMoney()));

            data.setSubPayDate(dataView.getSubPayDate());
            data.setSubPayMonth(AppUtil.convertStringToInteger(dataView.getSubPayMonth()));
            
            // </editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="data DebtorDetail List">
            if (dataView.getDetailList() != null && !dataView.getDetailList().isEmpty()) {
                List<DebtorDetailModel> detailList = new ArrayList<>();
                for (DebtorDetailViewModel detail : dataView.getDetailList()) {
                    DebtorDetailModel dataDetail = new DebtorDetailModel();
                    dataDetail.setDebtorDetailId(AppUtil.decryptId(detail.getDebtorDetailId()));
                    dataDetail.setDebtorId(AppUtil.decryptId(detail.getDebtorId()));
                    
                    dataDetail.setDebtorDetail(AppUtil.checkNullData(detail.getDebtorDetail()));
                    dataDetail.setDueDate(detail.getDueDate());
                    dataDetail.setTaxTotal(AppUtil.convertStringToDouble(detail.getTaxTotal()));
                    dataDetail.setSeq(AppUtil.convertStringToInteger(detail.getSeq()));
                    dataDetail.setPaymentStatus(AppUtil.checkNullData(detail.getPaymentStatus()));
                    
                    detailList.add(dataDetail);
                }
                data.setDetailList(detailList);
            }

            // </editor-fold>
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    
    public List<DebtorDetailViewModel> mapDebtorDetailList() {

        List<DebtorDetailViewModel> dataList = new ArrayList<DebtorDetailViewModel>();

        try {
            while (rs.next()) {
                DebtorDetailViewModel data = new DebtorDetailViewModel();
                
                data.setDebtorDetailId(AppUtil.encryptId(rs.getInt("DEBTOR_DETAIL_ID")));
                data.setDebtorId(AppUtil.encryptId(rs.getInt("DEBTOR_ID")));

                if (rs.getDate("DUE_DATE") != null) {
                    data.setDueDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
                } else {
                    data.setDueDate("");
                }
                
                data.setDebtorDetail(AppUtil.checkNullData(rs.getString("DEBTOR_DETAIL")));
                data.setTaxTotal(AppUtil.convertDoubleToStringFormat(rs.getDouble("TAX_TOTAL")));
                data.setSeq(AppUtil.checkNullData(rs.getString("SEQ")));
                
                //data.setPaymentStatus(AppUtil.checkNullData(rs.getBoolean("PAYMENT_STATUS")));
                //PAYMENT_STATUS = (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
                //อื่นๆ = ค้างชำระ , 1 = ชำระแล้ว
                if(!AppUtil.isNull(rs.getString("PAYMENT_STATUS")) && "1".equalsIgnoreCase(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")))){
                    data.setPaymentStatus("ชำระแล้ว");
                } else {
                    data.setPaymentStatus("ค้างชำระ");
                }
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;
    }

}
