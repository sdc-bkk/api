/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.TitleModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.TitleFilterModel;
import repository.TitleRepo;
import repository.mapper.TitleMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.TitleViewModel;

/**
 *
 * @author Sirichai
 */
public class TitleService {

    TitleRepo titleRepo = new TitleRepo();

    public ResultData<List<TitleViewModel>> getListAll(TitleFilterModel filter) {

        ResultData<List<TitleViewModel>> data = new ResultData<List<TitleViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = titleRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<TitleViewModel> getData(TitleFilterModel filter) {

        ResultData<TitleViewModel> data = new ResultData<TitleViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getTitleId());
            if (titleRepo.hasData(id)) {
                
                data = titleRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.title.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(TitleViewModel titleView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        TitleModel title = new TitleModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            TitleFilterModel filter = new TitleFilterModel();
            filter.setTitleId(titleView.getTitleId());
            filter.setTitleName(titleView.getTitleName());

            boolean resultDuplicate = titleRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                TitleMapper mapper = new TitleMapper();

                title = mapper.mapFull(titleView);

                boolean resultSave = titleRepo.save(title);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.title.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(TitleFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = titleRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<TitleViewModel>> getDrpList() {

        ResultData<List<TitleViewModel>> data = new ResultData<List<TitleViewModel>>();

        try {

            ResultPage resultPage = null;
            
            TitleFilterModel filter = new TitleFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = titleRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
