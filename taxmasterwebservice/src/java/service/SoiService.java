/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.SoiModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.SoiFilterModel;
import repository.SoiRepo;
import repository.mapper.SoiMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.SoiViewModel;

/**
 *
 * @author Sirichai
 */
public class SoiService {

    SoiRepo soiRepo = new SoiRepo();

    public ResultData<List<SoiViewModel>> getListAll(SoiFilterModel filter) {

        ResultData<List<SoiViewModel>> data = new ResultData<List<SoiViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = soiRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<SoiViewModel> getData(SoiFilterModel filter) {

        ResultData<SoiViewModel> data = new ResultData<SoiViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getSoiId());
            if (soiRepo.hasData(id)) {
                
                data = soiRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.soi.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(SoiViewModel soiView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        SoiModel soi = new SoiModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            SoiFilterModel filter = new SoiFilterModel();
            filter.setSoiId(soiView.getSoiId());
            filter.setRoadId(soiView.getRoadId());
            filter.setTambonId(soiView.getTambonId());
            filter.setSoiName(soiView.getSoiName());

            boolean resultDuplicate = soiRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                SoiMapper mapper = new SoiMapper();

                soi = mapper.mapFull(soiView);

                boolean resultSave = soiRepo.save(soi);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.soi.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(SoiFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = soiRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<SoiViewModel>> getDrpList() {

        ResultData<List<SoiViewModel>> data = new ResultData<List<SoiViewModel>>();

        try {

            ResultPage resultPage = null;
            
            SoiFilterModel filter = new SoiFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = soiRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<List<SoiViewModel>> getDrpListByRoad(SoiFilterModel filter) {

        ResultData<List<SoiViewModel>> data = new ResultData<List<SoiViewModel>>();

        try {

            ResultPage resultPage = null;
            
            SoiFilterModel filterData = new SoiFilterModel();
            filterData.setProvinceId(filter.getProvinceId());
            filterData.setDistrictId(filter.getDistrictId());
            filterData.setTambonId(filter.getTambonId());
            filterData.setRoadId(filter.getRoadId());
            filterData.setDropdown(Boolean.TRUE);
            filterData.setActive(Boolean.TRUE);

            data = soiRepo.getListAll(resultPage, filterData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
}
