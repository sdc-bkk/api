/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.SettingGeneralEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.SettingGeneralModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.SettingGeneralFilterModel;
import repository.SettingGeneralRepo;
import repository.mapper.SettingGeneralMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.SettingGeneralViewModel;

/**
 *
 * @author Sirichai
 */
public class SettingGeneralService {

    SettingGeneralRepo settingGeneralRepo = new SettingGeneralRepo();

    public String getDataValue(SettingGeneralEnum name) {

        String data = "";

        try {

            data = settingGeneralRepo.getDataValue(name);
            if (AppUtil.isNullAndSpace(data)) {
                
                //Insert value
                SettingGeneralModel settingGeneral = new SettingGeneralModel();
                settingGeneral.setSettingGeneralName(name.value());
                settingGeneral.setSettingGeneralDesc(name.displayNameTH());

                if(name.value().equalsIgnoreCase(SettingGeneralEnum.Telephone.value()) || name.value().equalsIgnoreCase(SettingGeneralEnum.Email.value())){
                    settingGeneral.setValueText("0");
                } else if(name.value().equalsIgnoreCase(SettingGeneralEnum.FiledDate.value()) || name.value().equalsIgnoreCase(SettingGeneralEnum.PayTaxDate.value())
                        || name.value().equalsIgnoreCase(SettingGeneralEnum.FiledNotiStart.value()) || name.value().equalsIgnoreCase(SettingGeneralEnum.FiledNotiEnd.value())
                        || name.value().equalsIgnoreCase(SettingGeneralEnum.PayTaxNotiStart.value()) || name.value().equalsIgnoreCase(SettingGeneralEnum.PayTaxNotiEnd.value())){
                    settingGeneral.setValueText("0"); 
                } else {
                    settingGeneral.setValueText("");
                }

                try {
                    boolean saveData = settingGeneralRepo.saveData(settingGeneral);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                data = settingGeneral.getValueText();
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public boolean saveDataValue(SettingGeneralEnum name, SettingGeneralViewModel settingGeneralView) {

        boolean resultData = false;

        try {

            SettingGeneralModel settingGeneral = new SettingGeneralModel();
            //<editor-fold defaultstate="collapsed" desc="check data value">
            SettingGeneralModel result = settingGeneralRepo.getDataByName(name.value());
            
            settingGeneral.setSettingGeneralName(name.value());
            settingGeneral.setSettingGeneralDesc(name.displayNameTH());
            settingGeneral.setUpdatedBy(settingGeneralView.getUpdatedBy());
            
            switch (name) {
                case Telephone:
                    settingGeneral.setValueText(settingGeneralView.getTelephone());
                    break;
                case Email:
                    settingGeneral.setValueText(settingGeneralView.getEmail());
                    break;
                case FiledDate:
                    settingGeneral.setValueText(settingGeneralView.getFiledDate());
                    break;
                case FiledNotiStart:
                    settingGeneral.setValueText(settingGeneralView.getFiledNotiStart());
                    break;
                case FiledNotiEnd:
                    settingGeneral.setValueText(settingGeneralView.getFiledNotiEnd());
                    break;
                case PayTaxDate:
                    settingGeneral.setValueText(settingGeneralView.getPayTaxDate());
                    break;
                case PayTaxNotiStart:
                    settingGeneral.setValueText(settingGeneralView.getPayTaxNotiStart());
                    break;
                case PayTaxNotiEnd:
                    settingGeneral.setValueText(settingGeneralView.getPayTaxNotiEnd());
                    break;
                default:    
                    settingGeneral.setValueText("");
            }

            //</editor-fold>

            if (result != null && result.getSettingGeneralId() > 0) {

                settingGeneral.setSettingGeneralId(result.getSettingGeneralId());
                resultData = settingGeneralRepo.saveData(settingGeneral);

            } else {

                resultData = settingGeneralRepo.saveData(settingGeneral);
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

/*    
    public ResultData<SettingGeneralViewModel> getData(SettingGeneralFilterModel filter) {

        ResultData<SettingGeneralViewModel> data = new ResultData<SettingGeneralViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getSettingGeneralId());
            if (settingGeneralRepo.hasData(id)) {
                
                data = settingGeneralRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.station.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(SettingGeneralViewModel settingGeneralView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        SettingGeneralModel settingGeneral = new SettingGeneralModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            SettingGeneralFilterModel filter = new SettingGeneralFilterModel();
            filter.setSettingGeneralId(settingGeneralView.getSettingGeneralId());
            filter.setSettingGeneralName(settingGeneralView.getSettingGeneralName());
            filter.setSettingGeneralDesc(settingGeneralView.getSettingGeneralDesc());

            boolean resultDuplicate = settingGeneralRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                SettingGeneralMapper mapper = new SettingGeneralMapper();

                settingGeneral = mapper.mapFull(settingGeneralView);

                boolean resultSave = settingGeneralRepo.save(settingGeneral);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.station.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }
*/
}
