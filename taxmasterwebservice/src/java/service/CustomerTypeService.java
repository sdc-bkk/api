/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.CustomerTypeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.CustomerTypeFilterModel;
import repository.CustomerTypeRepo;
import repository.mapper.CustomerTypeMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.CustomerTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class CustomerTypeService {

    CustomerTypeRepo customerTypeRepo = new CustomerTypeRepo();

    public ResultData<List<CustomerTypeViewModel>> getListAll(CustomerTypeFilterModel filter) {

        ResultData<List<CustomerTypeViewModel>> data = new ResultData<List<CustomerTypeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = customerTypeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<CustomerTypeViewModel> getData(CustomerTypeFilterModel filter) {

        ResultData<CustomerTypeViewModel> data = new ResultData<CustomerTypeViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getCustomerTypeId());
            if (customerTypeRepo.hasData(id)) {
                
                data = customerTypeRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.customerType.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(CustomerTypeViewModel customerTypeView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        CustomerTypeModel customerType = new CustomerTypeModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            CustomerTypeFilterModel filter = new CustomerTypeFilterModel();
            filter.setCustomerTypeId(customerTypeView.getCustomerTypeId());
            filter.setCustomerTypeName(customerTypeView.getCustomerTypeName());

            boolean resultDuplicate = customerTypeRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                CustomerTypeMapper mapper = new CustomerTypeMapper();

                customerType = mapper.mapFull(customerTypeView);

                boolean resultSave = customerTypeRepo.save(customerType);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.customerType.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(CustomerTypeFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = customerTypeRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<CustomerTypeViewModel>> getDrpList() {

        ResultData<List<CustomerTypeViewModel>> data = new ResultData<List<CustomerTypeViewModel>>();

        try {

            ResultPage resultPage = null;
            
            CustomerTypeFilterModel filter = new CustomerTypeFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = customerTypeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
