/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.BusinessStatusModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.BusinessStatusFilterModel;
import repository.BusinessStatusRepo;
import repository.mapper.BusinessStatusMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.BusinessStatusViewModel;

/**
 *
 * @author Sirichai
 */
public class BusinessStatusService {

    BusinessStatusRepo businessStatusRepo = new BusinessStatusRepo();

    public ResultData<List<BusinessStatusViewModel>> getListAll(BusinessStatusFilterModel filter) {

        ResultData<List<BusinessStatusViewModel>> data = new ResultData<List<BusinessStatusViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = businessStatusRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<BusinessStatusViewModel> getData(BusinessStatusFilterModel filter) {

        ResultData<BusinessStatusViewModel> data = new ResultData<BusinessStatusViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getBusinessStatusId());
            if (businessStatusRepo.hasData(id)) {
                
                data = businessStatusRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.businessStatus.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(BusinessStatusViewModel businessStatusView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        BusinessStatusModel businessStatus = new BusinessStatusModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            BusinessStatusFilterModel filter = new BusinessStatusFilterModel();
            filter.setBusinessStatusId(businessStatusView.getBusinessStatusId());
            filter.setBusinessStatusName(businessStatusView.getBusinessStatusName());
            filter.setBusinessStatusCode(businessStatusView.getBusinessStatusCode());

            boolean resultDuplicate = businessStatusRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                BusinessStatusMapper mapper = new BusinessStatusMapper();

                businessStatus = mapper.mapFull(businessStatusView);

                boolean resultSave = businessStatusRepo.save(businessStatus);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.businessStatus.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(BusinessStatusFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = businessStatusRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<BusinessStatusViewModel>> getDrpList() {

        ResultData<List<BusinessStatusViewModel>> data = new ResultData<List<BusinessStatusViewModel>>();

        try {

            ResultPage resultPage = null;
            
            BusinessStatusFilterModel filter = new BusinessStatusFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = businessStatusRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
