/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.RoadModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.RoadFilterModel;
import repository.RoadRepo;
import repository.mapper.RoadMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.RoadViewModel;

/**
 *
 * @author Sirichai
 */
public class RoadService {

    RoadRepo roadRepo = new RoadRepo();

    public ResultData<List<RoadViewModel>> getListAll(RoadFilterModel filter) {

        ResultData<List<RoadViewModel>> data = new ResultData<List<RoadViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = roadRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<RoadViewModel> getData(RoadFilterModel filter) {

        ResultData<RoadViewModel> data = new ResultData<RoadViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getRoadId());
            if (roadRepo.hasData(id)) {
                
                data = roadRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.road.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(RoadViewModel roadView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        RoadModel road = new RoadModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            RoadFilterModel filter = new RoadFilterModel();
            filter.setRoadId(roadView.getRoadId());
            filter.setTambonId(roadView.getTambonId());
            filter.setRoadName(roadView.getRoadName());

            boolean resultDuplicate = false;//roadRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                RoadMapper mapper = new RoadMapper();

                road = mapper.mapFull(roadView);

                boolean resultSave = roadRepo.save(road);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.road.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(RoadFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = roadRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<RoadViewModel>> getDrpList() {

        ResultData<List<RoadViewModel>> data = new ResultData<List<RoadViewModel>>();

        try {

            ResultPage resultPage = null;
            
            RoadFilterModel filter = new RoadFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = roadRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<List<RoadViewModel>> getDrpListByAddress(RoadFilterModel filter) {

        ResultData<List<RoadViewModel>> data = new ResultData<List<RoadViewModel>>();

        try {

            ResultPage resultPage = null;
            
            RoadFilterModel filterData = new RoadFilterModel();
            filterData.setProvinceId(filter.getProvinceId());
            filterData.setDistrictId(filter.getDistrictId());
            filterData.setTambonId(filter.getTambonId());
            filterData.setDropdown(Boolean.TRUE);
            filterData.setActive(Boolean.TRUE);

            data = roadRepo.getListAll(resultPage, filterData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
}
