/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.StationModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.StationFilterModel;
import repository.StationRepo;
import repository.mapper.StationMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.StationViewModel;

/**
 *
 * @author Sirichai
 */
public class StationService {

    StationRepo stationRepo = new StationRepo();

    public ResultData<List<StationViewModel>> getListAll(StationFilterModel filter) {

        ResultData<List<StationViewModel>> data = new ResultData<List<StationViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = stationRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<StationViewModel> getData(StationFilterModel filter) {

        ResultData<StationViewModel> data = new ResultData<StationViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getStationId());
            if (stationRepo.hasData(id)) {
                
                data = stationRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.station.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(StationViewModel stationView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        StationModel station = new StationModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            StationFilterModel filter = new StationFilterModel();
            filter.setStationId(stationView.getStationId());
            filter.setStationName(stationView.getStationName());
            filter.setStationCode(stationView.getStationCode());

            boolean resultDuplicate = stationRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                StationMapper mapper = new StationMapper();

                station = mapper.mapFull(stationView);

                boolean resultSave = stationRepo.save(station);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.station.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(StationFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = stationRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<StationViewModel>> getDrpList() {

        ResultData<List<StationViewModel>> data = new ResultData<List<StationViewModel>>();

        try {

            ResultPage resultPage = null;
            
            StationFilterModel filter = new StationFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = stationRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
