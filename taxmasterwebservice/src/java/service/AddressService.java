/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.AddressModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.AddressFilterModel;
import repository.AddressRepo;
import repository.mapper.AddressMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.AddressViewModel;

import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Sirichai
 */
public class AddressService {

    AddressRepo addressRepo = new AddressRepo();

    public ResultData<List<AddressViewModel>> getListAll(AddressFilterModel filter) {

        ResultData<List<AddressViewModel>> data = new ResultData<List<AddressViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = addressRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<List<AddressViewModel>> getDrpListByPostCodeMain(AddressFilterModel filter) {

        ResultData<List<AddressViewModel>> data = new ResultData<List<AddressViewModel>>();

        try {

            ResultPage resultPage = null;
            
            AddressFilterModel filterData = new AddressFilterModel();
            filterData.setPostCodeMain(filter.getPostCodeMain());
            filterData.setDropdown(Boolean.TRUE);
            filterData.setActive(Boolean.TRUE);

            data = addressRepo.getListAll(resultPage, filterData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
    public ResultData<List<AddressViewModel>> getDrpListProvince(AddressFilterModel filter) {

        ResultData<List<AddressViewModel>> data = new ResultData<List<AddressViewModel>>();

        try {

            data = addressRepo.getDrpListProvince(filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
    public ResultData<List<AddressViewModel>> getDrpListDistrict(AddressFilterModel filter) {

        ResultData<List<AddressViewModel>> data = new ResultData<List<AddressViewModel>>();

        try {

            if (!AppUtil.isNullAndSpace(filter.getProvinceId())) {
                data = addressRepo.getDrpListDistrict(filter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
    public ResultData<List<AddressViewModel>> getDrpListTambon(AddressFilterModel filter) {

        ResultData<List<AddressViewModel>> data = new ResultData<List<AddressViewModel>>();

        try {

            ResultPage resultPage = null;
            
            AddressFilterModel filterData = new AddressFilterModel();
            filterData.setDistrictId(filter.getDistrictId());
            filterData.setDropdown(Boolean.TRUE);
            filterData.setActive(Boolean.TRUE);

            data = addressRepo.getListAll(resultPage, filterData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
    
    
    public ResultData<Boolean> importData(HttpServletRequest request) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String msg = "";
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

            boolean dataList = readAndImportFile(items, null);
            if (dataList != true) {
                resultData.setResult(dataList);
                //resultData.setMessage("รูปแบบข้อมูลไม่ถูกต้อง");
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.address.importErrorFormat"));
            } else {
                resultData.setResult(dataList);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            
            resultData.setResult(null);
            MessageBundleUtil message = new MessageBundleUtil();
            resultData.setMessage(message.getMessage("message.address.importError"));
        }

        return resultData;
    }

    private boolean readAndImportFile(List<FileItem> items, Integer width) throws IOException {
        boolean isInsert = false;
        List<String> dataList = new ArrayList<String>();
        InputStream filecontent = null;
        FileOutputStream fos = null;
        byte[] file;
        
        try {
            
            Workbook workbook = null;
            int row = 1;
            int startRow = 2;

            for (FileItem item : items) {

                //<editor-fold defaultstate="collapsed" desc="workbook file">
                if (!item.isFormField()) {
                    String typefile = AppUtil.getFileType(item.getName());
                    
                    filecontent = item.getInputStream();
                    if (".xlsx".equals(typefile)) {
                        workbook = new XSSFWorkbook(filecontent);
                    } else {
                        workbook = new HSSFWorkbook(filecontent);
                    }

                    Sheet datatypeSheet;
                    Iterator<Row> iterator;
                    int countSheet = workbook.getNumberOfSheets();
                    
                    for (int i = 0; i < countSheet; i++) {
                        
                        //<editor-fold defaultstate="collapsed" desc="workbook Sheets">
                        datatypeSheet = workbook.getSheetAt(i);
                        //if (!"ตัวอย่างการกรอก".equals(datatypeSheet.getSheetName())) {
                        if ("TambonDatabase".equals(datatypeSheet.getSheetName())) {
                            
                            iterator = datatypeSheet.iterator();
                            
                            while (iterator.hasNext()) {
                                
                                Row currentRow = iterator.next();
                                //Iterator<Cell> cellIterator = currentRow.iterator();

                                if (row >= startRow) {
                                    
                                    //<editor-fold defaultstate="collapsed" desc="get Cell Value">
                                    //String tambonID = (currentRow.getCell(0) != null) ? currentRow.getCell(0).getStringCellValue() : "";
                                    String tambonID = "";
                                    if (currentRow.getCell(0) != null && currentRow.getCell(0).getCellType() == CellType.STRING) {
                                        tambonID = (currentRow.getCell(0) != null) ? currentRow.getCell(0).getStringCellValue() : "";
                                    } else if (currentRow.getCell(0) != null && currentRow.getCell(0).getCellType() == CellType.NUMERIC) {
                                        tambonID = (currentRow.getCell(0) != null) ? (int) currentRow.getCell(0).getNumericCellValue()+"" : "";
                                    }
                                    String tambonThai = (currentRow.getCell(1) != null) ? currentRow.getCell(1).getStringCellValue() : "";
                                    String tambonEng = (currentRow.getCell(2) != null) ? currentRow.getCell(2).getStringCellValue() : "";
                                    String tambonThaiShort = (currentRow.getCell(3) != null) ? currentRow.getCell(3).getStringCellValue() : "";
                                    String tambonEngShort = (currentRow.getCell(4) != null) ? currentRow.getCell(4).getStringCellValue() : "";
                                    
                                    //String postCodeMain = (currentRow.getCell(5) != null) ? currentRow.getCell(5).getStringCellValue() : "";
                                    String postCodeMain = "";
                                    if (currentRow.getCell(5) != null && currentRow.getCell(5).getCellType() == CellType.STRING) {
                                        postCodeMain = (currentRow.getCell(5) != null) ? currentRow.getCell(5).getStringCellValue() : "";
                                    } else if (currentRow.getCell(5) != null && currentRow.getCell(5).getCellType() == CellType.NUMERIC) {
                                        postCodeMain = (currentRow.getCell(5) != null) ? (int) currentRow.getCell(5).getNumericCellValue()+"" : "";
                                    }
                                    //String postCodeAll = (currentRow.getCell(6) != null) ? currentRow.getCell(6).getStringCellValue() : "";
                                    String postCodeAll = "";
                                    if (currentRow.getCell(6) != null && currentRow.getCell(6).getCellType() == CellType.STRING) {
                                        postCodeAll = (currentRow.getCell(6) != null) ? currentRow.getCell(6).getStringCellValue() : "";
                                    } else if (currentRow.getCell(6) != null && currentRow.getCell(6).getCellType() == CellType.NUMERIC) {
                                        postCodeAll = (currentRow.getCell(6) != null) ? (int) currentRow.getCell(6).getNumericCellValue()+"" : "";
                                    }
                                    String postalCodeRemark = (currentRow.getCell(7) != null) ? currentRow.getCell(7).getStringCellValue() : "";
                                    
                                    //String districtID = (currentRow.getCell(8) != null) ? currentRow.getCell(8).getStringCellValue() : "";
                                    String districtID = "";
                                    if (currentRow.getCell(8) != null && currentRow.getCell(8).getCellType() == CellType.STRING) {
                                        districtID = (currentRow.getCell(8) != null) ? currentRow.getCell(8).getStringCellValue() : "";
                                    } else if (currentRow.getCell(8) != null && currentRow.getCell(8).getCellType() == CellType.NUMERIC) {
                                        districtID = (currentRow.getCell(8) != null) ? (int) currentRow.getCell(8).getNumericCellValue()+"" : "";
                                    }
                                    String districtThai = (currentRow.getCell(9) != null) ? currentRow.getCell(9).getStringCellValue() : "";
                                    String districtEng = (currentRow.getCell(10) != null) ? currentRow.getCell(10).getStringCellValue() : "";
                                    String districtThaiShort = (currentRow.getCell(11) != null) ? currentRow.getCell(11).getStringCellValue() : "";
                                    String districtEngShort = (currentRow.getCell(12) != null) ? currentRow.getCell(12).getStringCellValue() : "";
                                    
                                    //String provinceID = (currentRow.getCell(13) != null) ? currentRow.getCell(13).getStringCellValue() : "";
                                    String provinceID = "";
                                    if (currentRow.getCell(13) != null && currentRow.getCell(13).getCellType() == CellType.STRING) {
                                        provinceID = (currentRow.getCell(13) != null) ? currentRow.getCell(13).getStringCellValue() : "";
                                    } else if (currentRow.getCell(13) != null && currentRow.getCell(13).getCellType() == CellType.NUMERIC) {
                                        provinceID = (currentRow.getCell(13) != null) ? (int) currentRow.getCell(13).getNumericCellValue()+"" : "";
                                    }
                                    String provinceThai = (currentRow.getCell(14) != null) ? currentRow.getCell(14).getStringCellValue() : "";
                                    String provinceEng = (currentRow.getCell(15) != null) ? currentRow.getCell(15).getStringCellValue() : "";
                                    
                                    String zoneName = (currentRow.getCell(16) != null) ? currentRow.getCell(16).getStringCellValue() : "";
                                    String fourZoneName = (currentRow.getCell(17) != null) ? currentRow.getCell(17).getStringCellValue() : "";
                                    String touristZoneName = (currentRow.getCell(18) != null) ? currentRow.getCell(18).getStringCellValue() : "";
                                    String bangkokZoneName = (currentRow.getCell(19) != null) ? currentRow.getCell(19).getStringCellValue() : "";
                                    //</editor-fold>
                                    
                                    if(!AppUtil.isNullAndSpace(tambonID)){

                                        AddressModel address = new AddressModel();
                                        //<editor-fold defaultstate="collapsed" desc="set Value to Model">
                                        address.setTambonId(tambonID);
                                        address.setTambonThai(tambonThai);
                                        address.setTambonEng(tambonEng);
                                        address.setTambonThaiShort(tambonThaiShort);
                                        address.setTambonEngShort(tambonEngShort);

                                        address.setDistrictId(districtID);
                                        address.setDistrictThai(districtThai);
                                        address.setDistrictEng(districtEng);
                                        address.setDistrictThaiShort(districtThaiShort);
                                        address.setDistrictEngShort(districtEngShort);

                                        address.setProvinceId(provinceID);
                                        address.setProvinceThai(provinceThai);
                                        address.setProvinceEng(provinceEng);

                                        address.setPostCodeMain(postCodeMain);
                                        address.setPostCodeAll(postCodeAll);
                                        address.setPostalCodeRemark(postalCodeRemark);

                                        address.setZoneName(zoneName);
                                        address.setFourZoneName(fourZoneName);
                                        address.setTouristZoneName(touristZoneName);
                                        address.setBangkokZoneName(bangkokZoneName);
                                        //</editor-fold>
                                        address.setActive(Boolean.TRUE);

                                        boolean resultSave = addressRepo.saveDataFromImport(address);
                                        if(resultSave){
                                            isInsert = true;
                                        }

                                    }
                                    
                                }
                                
                                row++; //next row
                                
                            } //End while row
                            
                        }
                        //</editor-fold>
                        
                    } //End for WorkbookSheet
                    
                }
                //</editor-fold>

            } //End for FileItem
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (filecontent != null) {
                filecontent.close();
            }

            if (fos != null) {
                fos.close();
            }
        }
        
        return isInsert;
    }
    
}
