/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.TaxRateModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.TaxRateFilterModel;
import repository.TaxRateRepo;
import repository.mapper.TaxRateMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.TaxRateViewModel;

/**
 *
 * @author Sirichai
 */
public class TaxRateService {

    TaxRateRepo taxRateRepo = new TaxRateRepo();

    public ResultData<List<TaxRateViewModel>> getListAll(TaxRateFilterModel filter) {

        ResultData<List<TaxRateViewModel>> data = new ResultData<List<TaxRateViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = taxRateRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<TaxRateViewModel> getData(TaxRateFilterModel filter) {

        ResultData<TaxRateViewModel> data = new ResultData<TaxRateViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getTaxRateId());
            if (taxRateRepo.hasData(id)) {
                
                data = taxRateRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.taxRate.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(TaxRateViewModel taxRateView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        TaxRateModel taxRate = new TaxRateModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            TaxRateFilterModel filter = new TaxRateFilterModel();
            filter.setTaxRateId(taxRateView.getTaxRateId());
            filter.setTaxRateName(taxRateView.getTaxRateName());
            filter.setStartDate(taxRateView.getStartDate());
            filter.setEndDate(taxRateView.getEndDate());

            boolean resultDuplicate = taxRateRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                TaxRateMapper mapper = new TaxRateMapper();

                taxRate = mapper.mapFull(taxRateView);

                boolean resultSave = taxRateRepo.save(taxRate);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.taxRate.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(TaxRateFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = taxRateRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<TaxRateViewModel>> getDrpList() {

        ResultData<List<TaxRateViewModel>> data = new ResultData<List<TaxRateViewModel>>();

        try {

            ResultPage resultPage = null;
            
            TaxRateFilterModel filter = new TaxRateFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = taxRateRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
