/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.NationalityModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.NationalityFilterModel;
import repository.NationalityRepo;
import repository.mapper.NationalityMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.NationalityViewModel;

/**
 *
 * @author Sirichai
 */
public class NationalityService {

    NationalityRepo nationalityRepo = new NationalityRepo();

    public ResultData<List<NationalityViewModel>> getListAll(NationalityFilterModel filter) {

        ResultData<List<NationalityViewModel>> data = new ResultData<List<NationalityViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = nationalityRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<NationalityViewModel> getData(NationalityFilterModel filter) {

        ResultData<NationalityViewModel> data = new ResultData<NationalityViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getNationalityId());
            if (nationalityRepo.hasData(id)) {
                
                data = nationalityRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.nationality.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(NationalityViewModel nationalityView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        NationalityModel nationality = new NationalityModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            NationalityFilterModel filter = new NationalityFilterModel();
            filter.setNationalityId(nationalityView.getNationalityId());
            filter.setNationalityName(nationalityView.getNationalityName());

            boolean resultDuplicate = nationalityRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                NationalityMapper mapper = new NationalityMapper();

                nationality = mapper.mapFull(nationalityView);

                boolean resultSave = nationalityRepo.save(nationality);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.nationality.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(NationalityFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = nationalityRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<NationalityViewModel>> getDrpList() {

        ResultData<List<NationalityViewModel>> data = new ResultData<List<NationalityViewModel>>();

        try {

            ResultPage resultPage = null;
            
            NationalityFilterModel filter = new NationalityFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = nationalityRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
