/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.FeeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.FeeFilterModel;
import repository.FeeRepo;
import repository.mapper.FeeMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.FeeViewModel;

/**
 *
 * @author Sirichai
 */
public class FeeService {

    FeeRepo feeRepo = new FeeRepo();

    public ResultData<List<FeeViewModel>> getListAll(FeeFilterModel filter) {

        ResultData<List<FeeViewModel>> data = new ResultData<List<FeeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = feeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<FeeViewModel> getData(FeeFilterModel filter) {

        ResultData<FeeViewModel> data = new ResultData<FeeViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getFeeId());
            if (feeRepo.hasData(id)) {
                
                data = feeRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.fee.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(FeeViewModel feeView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        FeeModel fee = new FeeModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            FeeFilterModel filter = new FeeFilterModel();
            filter.setFeeId(feeView.getFeeId());
            filter.setFeeName(feeView.getFeeName());
            filter.setStartDate(feeView.getStartDate());
            filter.setEndDate(feeView.getEndDate());

            boolean resultDuplicate = feeRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                FeeMapper mapper = new FeeMapper();

                fee = mapper.mapFull(feeView);

                boolean resultSave = feeRepo.save(fee);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.fee.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(FeeFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = feeRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<FeeViewModel>> getDrpList() {

        ResultData<List<FeeViewModel>> data = new ResultData<List<FeeViewModel>>();

        try {

            ResultPage resultPage = null;
            
            FeeFilterModel filter = new FeeFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = feeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
