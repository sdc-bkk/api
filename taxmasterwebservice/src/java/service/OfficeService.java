/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.OfficeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.OfficeFilterModel;
import repository.OfficeRepo;
import repository.mapper.OfficeMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.OfficeViewModel;

/**
 *
 * @author Sirichai
 */
public class OfficeService {

    OfficeRepo officeRepo = new OfficeRepo();

    public ResultData<List<OfficeViewModel>> getList(OfficeFilterModel filter) {

        ResultData<List<OfficeViewModel>> data = new ResultData<List<OfficeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = officeRepo.getList(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<List<OfficeViewModel>> getListAll(OfficeFilterModel filter) {

        ResultData<List<OfficeViewModel>> data = new ResultData<List<OfficeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = officeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<OfficeViewModel> getData(OfficeFilterModel filter) {

        ResultData<OfficeViewModel> data = new ResultData<OfficeViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getOfficeId());
//            if (officeRepo.hasData(id)) {

                data = officeRepo.getData(id);

//            } else {
//
//                data.setResult(null);
//                MessageBundleUtil message = new MessageBundleUtil();
//                data.setMessage(message.getMessage("message.station.hasData"));
//
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(OfficeViewModel dataView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        OfficeModel data = new OfficeModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            OfficeFilterModel filter = new OfficeFilterModel();
            filter.setOfficeId(dataView.getOfficeId());
            filter.setOfficeName(dataView.getOfficeName());
            filter.setOfficeCode(dataView.getOfficeCode());

            boolean resultDuplicate = officeRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                dataView.setAmphurId(filter.getOfficeId());
                //getofficeid เพื่อเช็คว่ามีข้อมูลในตาราง office แล้วหรือยัง
                int id = AppUtil.decryptId(filter.getOfficeId());
                dataView.setOfficeId(officeRepo.getOfficeId(id));

                OfficeMapper mapper = new OfficeMapper();

                data = mapper.mapFull(dataView);

                boolean resultSave = officeRepo.save(data);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.office.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(OfficeFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = officeRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);

            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<OfficeViewModel>> getDrpList() {

        ResultData<List<OfficeViewModel>> data = new ResultData<List<OfficeViewModel>>();

        try {

            ResultPage resultPage = null;

            OfficeFilterModel filter = new OfficeFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = officeRepo.getList(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
