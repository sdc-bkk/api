/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.OilTypeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.OilTypeFilterModel;
import repository.OilTypeRepo;
import repository.mapper.OilTypeMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.OilTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class OilTypeService {

    OilTypeRepo oilTypeRepo = new OilTypeRepo();

    public ResultData<List<OilTypeViewModel>> getListAll(OilTypeFilterModel filter) {

        ResultData<List<OilTypeViewModel>> data = new ResultData<List<OilTypeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = oilTypeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<OilTypeViewModel> getData(OilTypeFilterModel filter) {

        ResultData<OilTypeViewModel> data = new ResultData<OilTypeViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getOilTypeId());
            if (oilTypeRepo.hasData(id)) {
                
                data = oilTypeRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.oilType.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(OilTypeViewModel oilTypeView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        OilTypeModel oilType = new OilTypeModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            OilTypeFilterModel filter = new OilTypeFilterModel();
            filter.setOilTypeId(oilTypeView.getOilTypeId());
            filter.setOilTypeName(oilTypeView.getOilTypeName());
            filter.setOilTypeCode(oilTypeView.getOilTypeCode());

            boolean resultDuplicate = oilTypeRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                OilTypeMapper mapper = new OilTypeMapper();

                oilType = mapper.mapFull(oilTypeView);

                boolean resultSave = oilTypeRepo.save(oilType);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.oilType.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(OilTypeFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = oilTypeRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<OilTypeViewModel>> getDrpList() {

        ResultData<List<OilTypeViewModel>> data = new ResultData<List<OilTypeViewModel>>();

        try {

            ResultPage resultPage = null;
            
            OilTypeFilterModel filter = new OilTypeFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = oilTypeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
