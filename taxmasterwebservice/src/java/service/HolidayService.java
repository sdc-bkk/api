/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.HolidayModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.HolidayFilterModel;
import repository.HolidayRepo;
import repository.mapper.HolidayMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.HolidayViewModel;

/**
 *
 * @author Sirichai
 */
public class HolidayService {

    HolidayRepo repo = new HolidayRepo();

    public ResultData<List<HolidayViewModel>> getListAll(HolidayFilterModel filter) {

        ResultData<List<HolidayViewModel>> data = new ResultData<List<HolidayViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = repo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<HolidayViewModel> getData(HolidayFilterModel filter) {

        ResultData<HolidayViewModel> data = new ResultData<HolidayViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getHolidayId());
            if (repo.hasData(id)) {
                
                data = repo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.fee.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(HolidayViewModel feeView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        HolidayModel fee = new HolidayModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            HolidayFilterModel filter = new HolidayFilterModel();
            filter.setHolidayId(feeView.getHolidayId());
            filter.setHolidayName(feeView.getHolidayName());
            filter.setStartDate(feeView.getStartDate());
            filter.setEndDate(feeView.getEndDate());

            boolean resultDuplicate = repo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                HolidayMapper mapper = new HolidayMapper();

                fee = mapper.mapFull(feeView);

                boolean resultSave = repo.save(fee);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.fee.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(HolidayFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = repo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<HolidayViewModel>> getDrpList() {

        ResultData<List<HolidayViewModel>> data = new ResultData<List<HolidayViewModel>>();

        try {

            ResultPage resultPage = null;
            
            HolidayFilterModel filter = new HolidayFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = repo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
