/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.BusinessTypeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.BusinessTypeFilterModel;
import repository.BusinessTypeRepo;
import repository.mapper.BusinessTypeMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.BusinessTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class BusinessTypeService {

    BusinessTypeRepo businessTypeRepo = new BusinessTypeRepo();

    public ResultData<List<BusinessTypeViewModel>> getListAll(BusinessTypeFilterModel filter) {

        ResultData<List<BusinessTypeViewModel>> data = new ResultData<List<BusinessTypeViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = businessTypeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<BusinessTypeViewModel> getData(BusinessTypeFilterModel filter) {

        ResultData<BusinessTypeViewModel> data = new ResultData<BusinessTypeViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getBusinessTypeId());
            if (businessTypeRepo.hasData(id)) {
                
                data = businessTypeRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.businessType.hasData"));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(BusinessTypeViewModel businessTypeView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        BusinessTypeModel businessType = new BusinessTypeModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            BusinessTypeFilterModel filter = new BusinessTypeFilterModel();
            filter.setBusinessTypeId(businessTypeView.getBusinessTypeId());
            filter.setBusinessTypeName(businessTypeView.getBusinessTypeName());
            filter.setBusinessTypeCode(businessTypeView.getBusinessTypeCode());

            boolean resultDuplicate = businessTypeRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {

                BusinessTypeMapper mapper = new BusinessTypeMapper();

                businessType = mapper.mapFull(businessTypeView);

                boolean resultSave = businessTypeRepo.save(businessType);

                resultData.setResult(resultSave);

            } else {

                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.businessType.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> deleteData(BusinessTypeFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {
            
            List<String> idList = filter.getIdList();
            List<Integer> idDeleteList = new ArrayList<Integer>();
            for (String i : idList) {
                int o = AppUtil.decryptId(i);
                idDeleteList.add(o);
            }

            boolean resultDelete = businessTypeRepo.deleteData(idDeleteList, filter);

            if (resultDelete == true) {

                resultData.setResult(resultDelete);
                
            } else {

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setResult(Boolean.FALSE);
                resultData.setMessage(message.getMessage("message.delete.error"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<List<BusinessTypeViewModel>> getDrpList() {

        ResultData<List<BusinessTypeViewModel>> data = new ResultData<List<BusinessTypeViewModel>>();

        try {

            ResultPage resultPage = null;
            
            BusinessTypeFilterModel filter = new BusinessTypeFilterModel();
            filter.setDropdown(Boolean.TRUE);
            filter.setActive(Boolean.TRUE);

            data = businessTypeRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

}
