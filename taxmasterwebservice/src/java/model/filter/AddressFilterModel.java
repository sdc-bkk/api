/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.filter;

import java.util.List;
import utility.AppConfig;

/**
 *
 * @author Sirichai
 */
public class AddressFilterModel {

    private String tambonId; //รหัสตำบล/แขวง
    private String tambonThai; //ชื่อตำบล/แขวง ภาษาไทย
    
    private String districtId; //รหัสอำเภอ/อำเภอ
    private String districtThai; //ชื่ออำเภอ/อำเภอ ภาษาไทย
    
    private String provinceId; //รหัสจังหวัด
    private String provinceThai; //ชื่อจังหวัด ภาษาไทย
    
    private String postCodeMain; //รหัสไปรษณีย์
    
    private Boolean active;
    private Boolean dropdown;
    private String updatedBy;
    private List<String> idList;

    private int page;
    private int itemPerPage = Integer.parseInt(new AppConfig().value("config.itemPerPage"));
    private String orderBy;
    private String sort;

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getTambonThai() {
        return tambonThai;
    }

    public void setTambonThai(String tambonThai) {
        this.tambonThai = tambonThai;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictThai() {
        return districtThai;
    }

    public void setDistrictThai(String districtThai) {
        this.districtThai = districtThai;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceThai() {
        return provinceThai;
    }

    public void setProvinceThai(String provinceThai) {
        this.provinceThai = provinceThai;
    }

    public String getPostCodeMain() {
        return postCodeMain;
    }

    public void setPostCodeMain(String postCodeMain) {
        this.postCodeMain = postCodeMain;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDropdown() {
        return dropdown;
    }

    public void setDropdown(Boolean dropdown) {
        this.dropdown = dropdown;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(List<String> idList) {
        this.idList = idList;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItemPerPage() {
        return itemPerPage;
    }

    public void setItemPerPage(int itemPerPage) {
        this.itemPerPage = itemPerPage;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

}
