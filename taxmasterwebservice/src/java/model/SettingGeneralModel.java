/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class SettingGeneralModel {

    private int settingGeneralId;
    private String settingGeneralName; //ชื่อการตั้งค่าทั่วไป
    private String settingGeneralDesc; //คำอธิบาย
    private String valueText; //ค่าที่กำนหดของการตั้งค่าทั่วไป
    
    private String updatedBy; //ผู้ที่แก้ไขล่าสุด
    private String updatedDate; //วันที่แก้ไขล่าสุด

    public int getSettingGeneralId() {
        return settingGeneralId;
    }

    public void setSettingGeneralId(int settingGeneralId) {
        this.settingGeneralId = settingGeneralId;
    }

    public String getSettingGeneralName() {
        return settingGeneralName;
    }

    public void setSettingGeneralName(String settingGeneralName) {
        this.settingGeneralName = settingGeneralName;
    }

    public String getSettingGeneralDesc() {
        return settingGeneralDesc;
    }

    public void setSettingGeneralDesc(String settingGeneralDesc) {
        this.settingGeneralDesc = settingGeneralDesc;
    }

    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
