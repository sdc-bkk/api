/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class FeeModel {

    private int feeId;
    private String feeName; //ชื่อค่าปรับล่าช้า/ค่าเพิ่ม
    private Float feeValue; //ค่าของค่าปรับล่าช้า/ค่าเพิ่ม
    
    private String startDate; //วันที่เริ่มต้น บังคับใช้
    private String endDate; //วันที่สิ้นสุด บังคับใช้
    private boolean isDefault; //สถานะของค่าปรับเริ่มต้น(0=ไม่ใช่ค่าปรับเริ่มต้น,1=ค่าปรับเริ่มต้น)
    
    private boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private String createdBy; //ผู้ที่สร้าง
    private String createdDate; //วันที่สร้าง
    private String updatedBy; //ผู้ที่แก้ไขล่าสุด
    private String updatedDate; //วันที่แก้ไขล่าสุด
    
    private Boolean hasHoliday;

    public Boolean getHasHoliday() {
        return hasHoliday;
    }

    public void setHasHoliday(Boolean hasHoliday) {
        this.hasHoliday = hasHoliday;
    }

    public int getFeeId() {
        return feeId;
    }

    public void setFeeId(int feeId) {
        this.feeId = feeId;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public Float getFeeValue() {
        return feeValue;
    }

    public void setFeeValue(Float feeValue) {
        this.feeValue = feeValue;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
