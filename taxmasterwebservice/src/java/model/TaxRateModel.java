/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class TaxRateModel {

    private int taxRateId;
    private String taxRateName; //ชื่ออัตราภาษี
    private Float taxRateValue; //ค่าของอัตราภาษี (ลิตรละ)
    
    private String startDate; //วันที่เริ่มต้น บังคับใช้
    private String endDate; //วันที่สิ้นสุด บังคับใช้
    
    private String yearlyFrom; //ปีเริ่มต้น
    private String yearlyTo; //ปีสิ้นสุด
    
    private boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private String createdBy; //ผู้ที่สร้าง
    private String createdDate; //วันที่สร้าง
    private String updatedBy; //ผู้ที่แก้ไขล่าสุด
    private String updatedDate; //วันที่แก้ไขล่าสุด

    public int getTaxRateId() {
        return taxRateId;
    }

    public void setTaxRateId(int taxRateId) {
        this.taxRateId = taxRateId;
    }

    public String getTaxRateName() {
        return taxRateName;
    }

    public void setTaxRateName(String taxRateName) {
        this.taxRateName = taxRateName;
    }

    public Float getTaxRateValue() {
        return taxRateValue;
    }

    public void setTaxRateValue(Float taxRateValue) {
        this.taxRateValue = taxRateValue;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getYearlyFrom() {
        return yearlyFrom;
    }

    public void setYearlyFrom(String yearlyFrom) {
        this.yearlyFrom = yearlyFrom;
    }

    public String getYearlyTo() {
        return yearlyTo;
    }

    public void setYearlyTo(String yearlyTo) {
        this.yearlyTo = yearlyTo;
    }

}
