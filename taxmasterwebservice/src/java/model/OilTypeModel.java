/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class OilTypeModel {

    private int oilTypeId;
    private String oilTypeName; //ชื่อชนิดน้ำมัน/ก๊าซ
    private String oilTypeCode; //ชื่อย่อ/ชื่อรหัสชนิดน้ำมัน/ก๊าซ
    
    private boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private String createdBy; //ผู้ที่สร้าง
    private String createdDate; //วันที่สร้าง
    private String updatedBy; //ผู้ที่แก้ไขล่าสุด
    private String updatedDate; //วันที่แก้ไขล่าสุด

    public int getOilTypeId() {
        return oilTypeId;
    }

    public void setOilTypeId(int oilTypeId) {
        this.oilTypeId = oilTypeId;
    }

    public String getOilTypeName() {
        return oilTypeName;
    }

    public void setOilTypeName(String oilTypeName) {
        this.oilTypeName = oilTypeName;
    }

    public String getOilTypeCode() {
        return oilTypeCode;
    }

    public void setOilTypeCode(String oilTypeCode) {
        this.oilTypeCode = oilTypeCode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
