/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirichai
 */
public class AddressModel {

    private String tambonId; //รหัสตำบล/แขวง
    private String tambonThai; //ชื่อตำบล/แขวง ภาษาไทย
    private String tambonEng; //ชื่อตำบล/แขวง ภาษาอังกฤษ
    private String tambonThaiShort; //ชื่อตำบล/แขวง ภาษาไทยแบบสั้น
    private String tambonEngShort; //ชื่อตำบล/แขวง ภาษาอังกฤษแบบสั้น
    
    private String districtId; //รหัสอำเภอ/อำเภอ
    private String districtThai; //ชื่ออำเภอ/อำเภอ ภาษาไทย
    private String districtEng; //ชื่ออำเภอ/อำเภอ ภาษาอังกฤษ
    private String districtThaiShort; //ชื่ออำเภอ/อำเภอ ภาษาไทยแบบสั้น
    private String districtEngShort; //ชื่ออำเภอ/อำเภอ ภาษาอังกฤษแบบสั้น
    
    private String provinceId; //รหัสจังหวัด
    private String provinceThai; //ชื่อจังหวัด ภาษาไทย
    private String provinceEng; //ชื่อจังหวัด ภาษาอังกฤษ
    
    private String postCodeMain; //รหัสไปรษณีย์
    private String postCodeAll; //รหัสไปรษณีย์ ทั้งหมด
    private String postalCodeRemark; //รหัสไปรษณีย์ หมายเหตุ
    
    private String zoneName; //ภูมิภาคอย่างเป็นทางการ
    private String fourZoneName; //ภูมิภาคแบบสี่ภูมิภาค
    private String touristZoneName; //ภูมิภาคการท่องเที่ยวแห่งประเทศไทย
    private String bangkokZoneName; //กรุงเทพปริมณฑลต่างจังหวัด
    
    private boolean active; //สถานะการใช้งาน(0=ไม่ใช้งาน,1=ใช้งาน)
    private String createdBy; //ผู้ที่สร้าง
    private String createdDate; //วันที่สร้าง
    private String updatedBy; //ผู้ที่แก้ไขล่าสุด
    private String updatedDate; //วันที่แก้ไขล่าสุด

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getTambonThai() {
        return tambonThai;
    }

    public void setTambonThai(String tambonThai) {
        this.tambonThai = tambonThai;
    }

    public String getTambonEng() {
        return tambonEng;
    }

    public void setTambonEng(String tambonEng) {
        this.tambonEng = tambonEng;
    }

    public String getTambonThaiShort() {
        return tambonThaiShort;
    }

    public void setTambonThaiShort(String tambonThaiShort) {
        this.tambonThaiShort = tambonThaiShort;
    }

    public String getTambonEngShort() {
        return tambonEngShort;
    }

    public void setTambonEngShort(String tambonEngShort) {
        this.tambonEngShort = tambonEngShort;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictThai() {
        return districtThai;
    }

    public void setDistrictThai(String districtThai) {
        this.districtThai = districtThai;
    }

    public String getDistrictEng() {
        return districtEng;
    }

    public void setDistrictEng(String districtEng) {
        this.districtEng = districtEng;
    }

    public String getDistrictThaiShort() {
        return districtThaiShort;
    }

    public void setDistrictThaiShort(String districtThaiShort) {
        this.districtThaiShort = districtThaiShort;
    }

    public String getDistrictEngShort() {
        return districtEngShort;
    }

    public void setDistrictEngShort(String districtEngShort) {
        this.districtEngShort = districtEngShort;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceThai() {
        return provinceThai;
    }

    public void setProvinceThai(String provinceThai) {
        this.provinceThai = provinceThai;
    }

    public String getProvinceEng() {
        return provinceEng;
    }

    public void setProvinceEng(String provinceEng) {
        this.provinceEng = provinceEng;
    }

    public String getPostCodeMain() {
        return postCodeMain;
    }

    public void setPostCodeMain(String postCodeMain) {
        this.postCodeMain = postCodeMain;
    }

    public String getPostCodeAll() {
        return postCodeAll;
    }

    public void setPostCodeAll(String postCodeAll) {
        this.postCodeAll = postCodeAll;
    }

    public String getPostalCodeRemark() {
        return postalCodeRemark;
    }

    public void setPostalCodeRemark(String postalCodeRemark) {
        this.postalCodeRemark = postalCodeRemark;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getFourZoneName() {
        return fourZoneName;
    }

    public void setFourZoneName(String fourZoneName) {
        this.fourZoneName = fourZoneName;
    }

    public String getTouristZoneName() {
        return touristZoneName;
    }

    public void setTouristZoneName(String touristZoneName) {
        this.touristZoneName = touristZoneName;
    }

    public String getBangkokZoneName() {
        return bangkokZoneName;
    }

    public void setBangkokZoneName(String bangkokZoneName) {
        this.bangkokZoneName = bangkokZoneName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
