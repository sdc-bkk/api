/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.RoadModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.RoadFilterModel;
import repository.mapper.RoadMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.RoadViewModel;

/**
 *
 * @author Sirichai
 */
public class RoadRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<RoadViewModel>> getListAll(ResultPage page, RoadFilterModel filter) throws SQLException {

        ResultData<List<RoadViewModel>> resultData = new ResultData<List<RoadViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT R.*, "
                       + "    T.TAMBON_THAI, T.DISTRICT_ID, T.DISTRICT_THAI, T.PROVINCE_ID, T.PROVINCE_THAI "
                       + " FROM M_ROAD R "
                       + "    LEFT JOIN M_TAMBON T ON R.TAMBON_ID = T.TAMBON_ID "
                       + " WHERE (? IS NULL OR R.IS_ACTIVE = ?) ";
            
            if (!AppUtil.isNullAndSpace(filter.getRoadName())) {
                sql += " AND R.ROAD_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonId())) {
                sql += " AND T.TAMBON_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonThai())) {
                sql += " AND T.TAMBON_THAI LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictId())) {
                sql += " AND T.DISTRICT_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictThai())) {
                sql += " AND T.DISTRICT_THAI LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceId())) {
                sql += " AND T.PROVINCE_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceThai())) {
                sql += " AND T.PROVINCE_THAI LIKE ? ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("roadName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getRoadName())) {
                ps.setString(i++, "%" + filter.getRoadName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonId())) {
                ps.setString(i++, filter.getTambonId());
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonThai())) {
                ps.setString(i++, "%" + filter.getTambonThai() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictId())) {
                ps.setString(i++, filter.getDistrictId());
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictThai())) {
                ps.setString(i++, "%" + filter.getDistrictThai() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceId())) {
                ps.setString(i++, filter.getProvinceId());
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceThai())) {
                ps.setString(i++, "%" + filter.getProvinceThai() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<RoadViewModel> dataList = new RoadMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<RoadViewModel> dataList = new RoadMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " ROAD_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("roadName")) {
                str = " ROAD_NAME ";
            } else if (orderBy.equalsIgnoreCase("tambonThai")) {
                str = " PROVINCE_THAI,DISTRICT_THAI,TAMBON_THAI,ROAD_NAME ";
            } else if (orderBy.equalsIgnoreCase("districtThai")) {
                str = " PROVINCE_THAI,DISTRICT_THAI ";
            } else if (orderBy.equalsIgnoreCase("provinceThai")) {
                str = " PROVINCE_THAI ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_ROAD "
                        + " WHERE ROAD_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<RoadViewModel> getData(int id) throws SQLException {

        ResultData<RoadViewModel> resultData = new ResultData<RoadViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT R.*, "
                       + "    T.TAMBON_THAI, T.DISTRICT_ID, T.DISTRICT_THAI, T.PROVINCE_ID, T.PROVINCE_THAI "
                       + " FROM M_ROAD R "
                       + "    LEFT JOIN M_TAMBON T ON R.TAMBON_ID = T.TAMBON_ID "
                       + " WHERE R.ROAD_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            RoadViewModel data = new RoadMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(RoadFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_ROAD "
                        + " WHERE (? = 0 OR ROAD_ID <> ?) "
                        + " AND (TAMBON_ID = ?) "
                        + " AND (ROAD_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getRoadId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getRoadId()));
            ps.setString(i++, filter.getTambonId());
            ps.setString(i++, filter.getRoadName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(RoadModel road) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Road">
            if (road.getRoadId() == 0) {

                sql = "INSERT INTO M_ROAD ( "
                        + "  ROAD_NAME "
                        + ", TAMBON_ID "
                        + ", IS_ACTIVE "
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY "
                        + ") VALUES(?,?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "ROAD_ID" });
                
            } else {

                sql = "UPDATE M_ROAD SET "
                        + "  ROAD_NAME = ? "
                        + ", TAMBON_ID = ? "
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE ROAD_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, road.getRoadName());
            ps.setString(i++, road.getTambonId());
            ps.setBoolean(i++, road.isActive());

            if (road.getRoadId() == 0) {

                if (!AppUtil.isNullAndSpace(road.getUpdatedBy())) {
                    ps.setString(i++, road.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                road.setRoadId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(road.getUpdatedBy())) {
                    ps.setString(i++, road.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, road.getRoadId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, RoadFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete Road by id list">
            sql = " DELETE FROM M_ROAD "
                + " WHERE ROAD_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete Road by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
