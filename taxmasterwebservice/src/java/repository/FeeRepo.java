/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.FeeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.FeeFilterModel;
import repository.mapper.FeeMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.FeeViewModel;

/**
 *
 * @author Sirichai
 */
public class FeeRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<FeeViewModel>> getListAll(ResultPage page, FeeFilterModel filter) throws SQLException {

        ResultData<List<FeeViewModel>> resultData = new ResultData<List<FeeViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;
            String tempStartDate = "";
            String tempEndDate = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                tempStartDate = DateUtils.toEng(DateUtils.toEng(filter.getStartDate()), "YYYY-MM-dd");
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "YYYY-MM-dd");
            }

            String sql = "SELECT * FROM M_FEE "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getFeeName())) {
                sql += " AND FEE_NAME LIKE ? ";
            }

            if (filter.getFeeValue() != null && filter.getFeeValue() > 0) {
                sql += " AND FEE_VALUE = ? ";
            }

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                sql += " AND START_DATE <= TO_DATE(?, 'YYYY-MM-dd') ";
            }

            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                sql += " AND END_DATE >= TO_DATE(?, 'YYYY-MM-dd') ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("feeName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getFeeName())) {
                ps.setString(i++, "%" + filter.getFeeName() + "%");
            }

            if (filter.getFeeValue() != null && filter.getFeeValue() > 0) {
                ps.setFloat(i++, filter.getFeeValue());
            }

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                ps.setString(i++, tempStartDate);
            }
            
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                ps.setString(i++, tempEndDate);
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<FeeViewModel> dataList = new FeeMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<FeeViewModel> dataList = new FeeMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " FEE_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("feeName")) {
                str = " FEE_NAME ";
            } else if (orderBy.equalsIgnoreCase("feeValue")) {
                str = " FEE_VALUE ";
            } else if (orderBy.equalsIgnoreCase("startDate")) {
                str = " START_DATE ";
            } else if (orderBy.equalsIgnoreCase("endDate")) {
                str = " END_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_FEE "
                        + " WHERE FEE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<FeeViewModel> getData(int id) throws SQLException {

        ResultData<FeeViewModel> resultData = new ResultData<FeeViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_FEE "
                       + " WHERE FEE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            FeeViewModel data = new FeeMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(FeeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;
            String tempStartDate = "";
            String tempEndDate = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                tempStartDate = DateUtils.toEng(DateUtils.toEng(filter.getStartDate()), "yyyy-MM-dd");
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "yyyy-MM-dd");
            }

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_FEE "
                        + " WHERE (? = 0 OR FEE_ID <> ?) "
                        + " AND ( "
                        + "    (FEE_NAME = ?) ";
                        //+ " AND (FEE_NAME LIKE ?) "
                        //+ " AND ((? IS NULL OR ? BETWEEN START_DATE AND END_DATE) "
                        //+ "   OR (? IS NULL OR ? BETWEEN START_DATE AND END_DATE)) ";

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                sql += "    OR ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                sql += "    OR ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            sql += " ) ";
            
            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getFeeId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getFeeId()));
            ps.setString(i++, filter.getFeeName());
            
            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                ps.setString(i++, tempStartDate);
                ps.setString(i++, tempStartDate);
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                ps.setString(i++, tempEndDate);
                ps.setString(i++, tempEndDate);
            }
            /*
            if (filter.getStartDate() != null && DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())) != null) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())));
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())));
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }
            if (filter.getEndDate() != null && DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())) != null) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())));
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())));
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }
            */

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(FeeModel fee) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Fee">
            if (fee.getFeeId() == 0) {

                sql = "INSERT INTO M_FEE ( "
                        + "  FEE_NAME "
                        + ", FEE_VALUE "
                        + ", START_DATE "
                        + ", END_DATE "
                        + ", IS_DEFAULT "
                        + ", HAS_HOLIDAY "
                        
                        + ", IS_ACTIVE "
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?,?,?, ?,SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "FEE_ID" });
                
            } else {

                sql = "UPDATE M_FEE SET "
                        + "  FEE_NAME = ? "
                        + ", FEE_VALUE = ? "
                        + ", START_DATE = ? "
                        + ", END_DATE = ? "
                        + ", IS_DEFAULT = ? "
                        + ", HAS_HOLIDAY = ? "
                        
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE FEE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, fee.getFeeName());
            ps.setFloat(i++, fee.getFeeValue());
            
            if (!AppUtil.isNullAndSpace(fee.getStartDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(fee.getStartDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }
            if (!AppUtil.isNullAndSpace(fee.getEndDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(fee.getEndDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }
            ps.setBoolean(i++, fee.isIsDefault());
            ps.setBoolean(i++, fee.getHasHoliday());

            ps.setBoolean(i++, fee.isActive());

            if (fee.getFeeId() == 0) {

                if (!AppUtil.isNullAndSpace(fee.getUpdatedBy())) {
                    ps.setString(i++, fee.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                fee.setFeeId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(fee.getUpdatedBy())) {
                    ps.setString(i++, fee.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, fee.getFeeId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, FeeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete Fee by id list">
            sql = " DELETE FROM M_FEE "
                + " WHERE FEE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete Fee by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
