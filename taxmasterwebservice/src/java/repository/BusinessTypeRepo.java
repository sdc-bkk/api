/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.BusinessTypeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.BusinessTypeFilterModel;
import repository.mapper.BusinessTypeMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.BusinessTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class BusinessTypeRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<BusinessTypeViewModel>> getListAll(ResultPage page, BusinessTypeFilterModel filter) throws SQLException {

        ResultData<List<BusinessTypeViewModel>> resultData = new ResultData<List<BusinessTypeViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_BUSINESS_TYPE "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getBusinessTypeName())) {
                sql += " AND BUSINESS_TYPE_NAME LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessTypeCode())) {
                sql += " AND BUSINESS_TYPE_CODE LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("businessTypeName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessTypeName())) {
                ps.setString(i++, "%" + filter.getBusinessTypeName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessTypeCode())) {
                ps.setString(i++, "%" + filter.getBusinessTypeCode() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<BusinessTypeViewModel> dataList = new BusinessTypeMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<BusinessTypeViewModel> dataList = new BusinessTypeMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " BUSINESS_TYPE_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("businessTypeName")) {
                str = " BUSINESS_TYPE_NAME ";
            } else if (orderBy.equalsIgnoreCase("businessTypeCode")) {
                str = " BUSINESS_TYPE_CODE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_BUSINESS_TYPE "
                        + " WHERE BUSINESS_TYPE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<BusinessTypeViewModel> getData(int id) throws SQLException {

        ResultData<BusinessTypeViewModel> resultData = new ResultData<BusinessTypeViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_BUSINESS_TYPE "
                       + " WHERE BUSINESS_TYPE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            BusinessTypeViewModel data = new BusinessTypeMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(BusinessTypeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_BUSINESS_TYPE "
                        + " WHERE (? = 0 OR BUSINESS_TYPE_ID <> ?) "
                        + " AND (BUSINESS_TYPE_NAME LIKE ?) ";
                        //+ " AND (BUSINESS_TYPE_CODE LIKE ?) "

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getBusinessTypeId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getBusinessTypeId()));
            ps.setString(i++, filter.getBusinessTypeName());
            //ps.setString(i++, filter.getBusinessTypeCode());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(BusinessTypeModel businessType) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="BusinessType">
            if (businessType.getBusinessTypeId() == 0) {

                sql = "INSERT INTO M_BUSINESS_TYPE ( "
                        + "  BUSINESS_TYPE_NAME "
                        + ", BUSINESS_TYPE_CODE"
                        + ", IS_ACTIVE"
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "BUSINESS_TYPE_ID" });
                
            } else {

                sql = "UPDATE M_BUSINESS_TYPE SET "
                        + "  BUSINESS_TYPE_NAME = ? "
                        + ", BUSINESS_TYPE_CODE = ? "
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE BUSINESS_TYPE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, businessType.getBusinessTypeName());
            ps.setString(i++, businessType.getBusinessTypeCode());
            ps.setBoolean(i++, businessType.isActive());

            if (businessType.getBusinessTypeId() == 0) {

                if (!AppUtil.isNullAndSpace(businessType.getUpdatedBy())) {
                    ps.setString(i++, businessType.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                businessType.setBusinessTypeId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(businessType.getUpdatedBy())) {
                    ps.setString(i++, businessType.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, businessType.getBusinessTypeId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, BusinessTypeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete BusinessType by id list">
            sql = " DELETE FROM M_BUSINESS_TYPE "
                + " WHERE BUSINESS_TYPE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete BusinessType by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
