/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.AddressModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.AddressFilterModel;
import repository.mapper.AddressMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.AddressViewModel;

/**
 *
 * @author Sirichai
 */
public class AddressRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<AddressViewModel>> getListAll(ResultPage page, AddressFilterModel filter) throws SQLException {

        ResultData<List<AddressViewModel>> resultData = new ResultData<List<AddressViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_TAMBON "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getTambonId())) {
                sql += " AND TAMBON_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonThai())) {
                sql += " AND TAMBON_THAI LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictId())) {
                sql += " AND DISTRICT_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictThai())) {
                sql += " AND DISTRICT_THAI LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceId())) {
                sql += " AND PROVINCE_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceThai())) {
                sql += " AND PROVINCE_THAI LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getPostCodeMain())) {
                sql += " AND POST_CODE_MAIN LIKE ? ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("tambonThai");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonId())) {
                ps.setString(i++, filter.getTambonId().trim());
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonThai())) {
                ps.setString(i++, "%" + filter.getTambonThai().trim() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictId())) {
                ps.setString(i++, filter.getDistrictId().trim());
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictThai())) {
                ps.setString(i++, "%" + filter.getDistrictThai().trim() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceId())) {
                ps.setString(i++, filter.getProvinceId().trim());
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceThai())) {
                ps.setString(i++, "%" + filter.getProvinceThai().trim() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getPostCodeMain())) {
                ps.setString(i++, "%" + filter.getPostCodeMain().trim() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<AddressViewModel> dataList = new AddressMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<AddressViewModel> dataList = new AddressMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " TAMBON_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("tambonThai")) {
                str = " PROVINCE_THAI,DISTRICT_THAI,TAMBON_THAI ";
            } else if (orderBy.equalsIgnoreCase("districtThai")) {
                str = " PROVINCE_THAI,DISTRICT_THAI ";
            } else if (orderBy.equalsIgnoreCase("provinceThai")) {
                str = " PROVINCE_THAI ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public ResultData<List<AddressViewModel>> getDrpListProvince(AddressFilterModel filter) throws SQLException {

        ResultData<List<AddressViewModel>> resultData = new ResultData<List<AddressViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT DISTINCT PROVINCE_ID, PROVINCE_THAI "
                       + " FROM M_TAMBON "
                       + " WHERE IS_ACTIVE = ? ";

            ps.setSql(sql);

            String orderBy = " PROVINCE_THAI ";
            String sort = " ASC ";
            ps.setOrderBy(orderBy + " " + sort);

            ps.setBoolean(i++, Boolean.TRUE); //IS_ACTIVE

            rs = ps.executeQuery();

            List<AddressViewModel> dataList = new AddressMapper(rs).mapFullDrpListProvince();
            resultData.setResult(dataList);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<AddressViewModel>> getDrpListDistrict(AddressFilterModel filter) throws SQLException {

        ResultData<List<AddressViewModel>> resultData = new ResultData<List<AddressViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT DISTINCT DISTRICT_ID, DISTRICT_THAI "
                       + " FROM M_TAMBON "
                       + " WHERE IS_ACTIVE = ? AND PROVINCE_ID LIKE ? ";

            ps.setSql(sql);

            String orderBy = " DISTRICT_THAI ";
            String sort = " ASC ";
            ps.setOrderBy(orderBy + " " + sort);

            ps.setBoolean(i++, Boolean.TRUE); //IS_ACTIVE
            ps.setString(i++, filter.getProvinceId().trim()); //PROVINCE_ID

            rs = ps.executeQuery();

            List<AddressViewModel> dataList = new AddressMapper(rs).mapFullDrpListDistrict();
            resultData.setResult(dataList);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean saveDataFromImport(AddressModel address) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //check hasData by TambonId
            boolean checkData = hasData(address.getTambonId());
            
            //<editor-fold defaultstate="collapsed" desc="Address">
            if (!checkData) {

                sql = "INSERT INTO M_TAMBON ( "
                        + "  TAMBON_ID "
                        + ", TAMBON_THAI "
                        + ", TAMBON_ENG "
                        + ", TAMBON_THAI_SHORT "
                        + ", TAMBON_ENG_SHORT "
                        
                        + ", DISTRICT_ID "
                        + ", DISTRICT_THAI "
                        + ", DISTRICT_ENG "
                        + ", DISTRICT_THAI_SHORT "
                        + ", DISTRICT_ENG_SHORT "
                        
                        + ", PROVINCE_ID "
                        + ", PROVINCE_THAI "
                        + ", PROVINCE_ENG "
                        
                        + ", POST_CODE_MAIN "
                        + ", POST_CODE_ALL "
                        + ", POST_CODE_REMARK "
                        
                        + ", ZONE_NAME "
                        + ", FOUR_ZONE_NAME "
                        + ", TOURIST_ZONE_NAME "
                        + ", BANGKOK_ZONE_NAME "
                        
                        + ", IS_ACTIVE "
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY "
                        + ") VALUES(?,?,?,?,?, ?,?,?,?,?, ?,?,?, ?,?,?, ?,?,?,?, ?, SYSDATE,?) ";

                ps = conn.prepareStatement(sql);
                
            } else {

                sql = "UPDATE M_TAMBON SET "
                        + "  TAMBON_ID = ? "
                        + ", TAMBON_THAI = ? "
                        + ", TAMBON_ENG = ? "
                        + ", TAMBON_THAI_SHORT = ? "
                        + ", TAMBON_ENG_SHORT = ? "
                        
                        + ", DISTRICT_ID = ? "
                        + ", DISTRICT_THAI = ? "
                        + ", DISTRICT_ENG = ? "
                        + ", DISTRICT_THAI_SHORT = ? "
                        + ", DISTRICT_ENG_SHORT = ? "
                        
                        + ", PROVINCE_ID = ? "
                        + ", PROVINCE_THAI = ? "
                        + ", PROVINCE_ENG = ? "
                        
                        + ", POST_CODE_MAIN = ? "
                        + ", POST_CODE_ALL = ? "
                        + ", POST_CODE_REMARK = ? "
                        
                        + ", ZONE_NAME = ? "
                        + ", FOUR_ZONE_NAME = ? "
                        + ", TOURIST_ZONE_NAME = ? "
                        + ", BANGKOK_ZONE_NAME = ? "
                        
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE TAMBON_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, address.getTambonId());
            ps.setString(i++, address.getTambonThai());
            ps.setString(i++, address.getTambonEng());
            ps.setString(i++, address.getTambonThaiShort());
            ps.setString(i++, address.getTambonEngShort());
            
            ps.setString(i++, address.getDistrictId());
            ps.setString(i++, address.getDistrictThai());
            ps.setString(i++, address.getDistrictEng());
            ps.setString(i++, address.getDistrictThaiShort());
            ps.setString(i++, address.getDistrictEngShort());
            
            ps.setString(i++, address.getProvinceId());
            ps.setString(i++, address.getProvinceThai());
            ps.setString(i++, address.getProvinceEng());
            
            ps.setString(i++, address.getPostCodeMain());
            ps.setString(i++, address.getPostCodeAll());
            ps.setString(i++, address.getPostalCodeRemark());
            
            ps.setString(i++, address.getZoneName());
            ps.setString(i++, address.getFourZoneName());
            ps.setString(i++, address.getTouristZoneName());
            ps.setString(i++, address.getBangkokZoneName());
            
            ps.setBoolean(i++, address.isActive());

            if (!checkData) {

                if (!AppUtil.isNullAndSpace(address.getUpdatedBy())) {
                    ps.setString(i++, address.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

            } else {

                if (!AppUtil.isNullAndSpace(address.getUpdatedBy())) {
                    ps.setString(i++, address.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setString(i++, address.getTambonId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean hasData(String id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_TAMBON "
                        + " WHERE TAMBON_ID = ? ";

            ps.setSql(sql);

            ps.setString(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

}
