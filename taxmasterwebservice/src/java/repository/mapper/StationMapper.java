/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.StationModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.StationViewModel;

/**
 *
 * @author Sirichai
 */
public class StationMapper {

    private ResultSet rs = null;

    public StationMapper() {

    }

    public StationMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<StationViewModel> mapFullList() {

        List<StationViewModel> dataList = new ArrayList<StationViewModel>();

        try {

            while (rs.next()) {

                StationViewModel data = new StationViewModel();

                data.setStationId(AppUtil.encryptId(rs.getInt("STATION_ID")));
                data.setStationName(rs.getString("STATION_NAME"));
                data.setStationCode(rs.getString("STATION_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<StationViewModel> mapFullDrpList() {

        List<StationViewModel> dataList = new ArrayList<StationViewModel>();

        try {

            while (rs.next()) {

                StationViewModel data = new StationViewModel();

                data.setStationId(AppUtil.encryptId(rs.getInt("STATION_ID")));
                data.setStationName(rs.getString("STATION_NAME"));
                data.setStationCode(rs.getString("STATION_CODE"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public StationViewModel mapFull() {

        StationViewModel data = new StationViewModel();

        try {

            while (rs.next()) {

                data.setStationId(AppUtil.encryptId(rs.getInt("STATION_ID")));
                data.setStationName(rs.getString("STATION_NAME"));
                data.setStationCode(rs.getString("STATION_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public StationModel mapFull(StationViewModel dataView) {

        StationModel data = new StationModel();

        try {
            data.setStationId(AppUtil.decryptId(dataView.getStationId()));
            data.setStationName(dataView.getStationName());
            data.setStationCode(dataView.getStationCode());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
