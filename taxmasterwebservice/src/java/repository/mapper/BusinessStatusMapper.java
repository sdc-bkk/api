/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.BusinessStatusModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.BusinessStatusViewModel;

/**
 *
 * @author Sirichai
 */
public class BusinessStatusMapper {

    private ResultSet rs = null;

    public BusinessStatusMapper() {

    }

    public BusinessStatusMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<BusinessStatusViewModel> mapFullList() {

        List<BusinessStatusViewModel> dataList = new ArrayList<BusinessStatusViewModel>();

        try {

            while (rs.next()) {

                BusinessStatusViewModel data = new BusinessStatusViewModel();

                data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                data.setBusinessStatusName(rs.getString("BUSINESS_STATUS_NAME"));
                data.setBusinessStatusCode(rs.getString("BUSINESS_STATUS_CODE"));
                data.setTemplateCode(rs.getString("TEMPLATE_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<BusinessStatusViewModel> mapFullDrpList() {

        List<BusinessStatusViewModel> dataList = new ArrayList<BusinessStatusViewModel>();

        try {

            while (rs.next()) {

                BusinessStatusViewModel data = new BusinessStatusViewModel();

                data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                data.setBusinessStatusName(rs.getString("BUSINESS_STATUS_NAME"));
                data.setBusinessStatusCode(rs.getString("BUSINESS_STATUS_CODE"));
                data.setTemplateCode(rs.getString("TEMPLATE_CODE"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public BusinessStatusViewModel mapFull() {

        BusinessStatusViewModel data = new BusinessStatusViewModel();

        try {

            while (rs.next()) {

                data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                data.setBusinessStatusName(rs.getString("BUSINESS_STATUS_NAME"));
                data.setBusinessStatusCode(rs.getString("BUSINESS_STATUS_CODE"));
                data.setTemplateCode(rs.getString("TEMPLATE_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public BusinessStatusModel mapFull(BusinessStatusViewModel dataView) {

        BusinessStatusModel data = new BusinessStatusModel();

        try {
            data.setBusinessStatusId(AppUtil.decryptId(dataView.getBusinessStatusId()));
            data.setBusinessStatusName(dataView.getBusinessStatusName());
            data.setBusinessStatusCode(dataView.getBusinessStatusCode());
            data.setTemplateCode(dataView.getTemplateCode());

            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
