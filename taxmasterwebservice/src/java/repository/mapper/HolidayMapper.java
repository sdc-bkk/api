/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.HolidayModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.HolidayViewModel;

/**
 *
 * @author Sirichai
 */
public class HolidayMapper {

    private ResultSet rs = null;

    public HolidayMapper() {

    }

    public HolidayMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<HolidayViewModel> mapFullList() {

        List<HolidayViewModel> dataList = new ArrayList<HolidayViewModel>();

        try {

            while (rs.next()) {

                HolidayViewModel data = new HolidayViewModel();

                data.setHolidayId(AppUtil.encryptId(rs.getInt("HOLIDAY_ID")));
                data.setHolidayName(rs.getString("HOLIDAY_NAME"));
                data.setYearly(rs.getString("YEARLY"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<HolidayViewModel> mapFullDrpList() {

        List<HolidayViewModel> dataList = new ArrayList<HolidayViewModel>();

        try {

            while (rs.next()) {

                HolidayViewModel data = new HolidayViewModel();

                data.setHolidayId(AppUtil.encryptId(rs.getInt("HOLIDAY_ID")));
                data.setHolidayName(rs.getString("HOLIDAY_NAME"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public HolidayViewModel mapFull() {

        HolidayViewModel data = new HolidayViewModel();

        try {

            while (rs.next()) {

                data.setHolidayId(AppUtil.encryptId(rs.getInt("HOLIDAY_ID")));
                data.setHolidayName(rs.getString("HOLIDAY_NAME"));
                data.setYearly(rs.getString("YEARLY"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public HolidayModel mapFull(HolidayViewModel dataView) {

        HolidayModel data = new HolidayModel();

        try {
            data.setHolidayId(AppUtil.decryptId(dataView.getHolidayId()));
            data.setHolidayName(dataView.getHolidayName());
            data.setYearly(dataView.getYearly());

            data.setStartDate(dataView.getStartDate());
            data.setEndDate(dataView.getEndDate());

            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
