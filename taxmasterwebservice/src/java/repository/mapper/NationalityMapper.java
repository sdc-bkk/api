/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.NationalityModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.NationalityViewModel;

/**
 *
 * @author Sirichai
 */
public class NationalityMapper {

    private ResultSet rs = null;

    public NationalityMapper() {

    }

    public NationalityMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<NationalityViewModel> mapFullList() {

        List<NationalityViewModel> dataList = new ArrayList<NationalityViewModel>();

        try {

            while (rs.next()) {

                NationalityViewModel data = new NationalityViewModel();

                data.setNationalityId(AppUtil.encryptId(rs.getInt("NATIONALITY_ID")));
                data.setNationalityName(rs.getString("NATIONALITY_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<NationalityViewModel> mapFullDrpList() {

        List<NationalityViewModel> dataList = new ArrayList<NationalityViewModel>();

        try {

            while (rs.next()) {

                NationalityViewModel data = new NationalityViewModel();

                data.setNationalityId(AppUtil.encryptId(rs.getInt("NATIONALITY_ID")));
                data.setNationalityName(rs.getString("NATIONALITY_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public NationalityViewModel mapFull() {

        NationalityViewModel data = new NationalityViewModel();

        try {

            while (rs.next()) {

                data.setNationalityId(AppUtil.encryptId(rs.getInt("NATIONALITY_ID")));
                data.setNationalityName(rs.getString("NATIONALITY_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public NationalityModel mapFull(NationalityViewModel dataView) {

        NationalityModel data = new NationalityModel();

        try {
            data.setNationalityId(AppUtil.decryptId(dataView.getNationalityId()));
            data.setNationalityName(dataView.getNationalityName());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
