/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.OfficeModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.OfficeViewModel;

/**
 *
 * @author Sirichai
 */
public class OfficeMapper {

    private ResultSet rs = null;

    public OfficeMapper() {

    }

    public OfficeMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<OfficeViewModel> mapFullList() {

        List<OfficeViewModel> dataList = new ArrayList<OfficeViewModel>();

        try {

            while (rs.next()) {

                OfficeViewModel data = new OfficeViewModel();

//                if (rs.getInt("OFFICE_ID") > 0) {
//                    data.setOfficeId(AppUtil.encryptId(rs.getInt("OFFICE_ID")));
//                } else {
//                    data.setOfficeId("");
//                }
                data.setOfficeId(AppUtil.encryptId(rs.getInt("DISTRICT_ID")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI")));

                data.setLeader(AppUtil.checkNullData(rs.getString("LEADER")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setFax(AppUtil.checkNullData(rs.getString("FAX")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                data.setLineId(AppUtil.checkNullData(rs.getString("LINE_ID")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<OfficeViewModel> mapFullDrpList() {

        List<OfficeViewModel> dataList = new ArrayList<OfficeViewModel>();

        try {

            while (rs.next()) {

                OfficeViewModel data = new OfficeViewModel();

                data.setOfficeId(AppUtil.encryptId(rs.getInt("DISTRICT_ID")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public OfficeViewModel mapFull() {

        OfficeViewModel data = new OfficeViewModel();

        try {

            while (rs.next()) {
//                if (rs.getInt("OFFICE_ID") > 0) {
//                    data.setOfficeId(AppUtil.encryptId(rs.getInt("OFFICE_ID")));
//                } else {
//                    data.setOfficeId("");
//                }
                data.setOfficeId(AppUtil.encryptId(rs.getInt("DISTRICT_ID")));
                data.setOfficeCode(AppUtil.checkNullData(rs.getString("OFFICE_CODE")));
                data.setOfficeName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI")));

                data.setLeader(AppUtil.checkNullData(rs.getString("LEADER")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setFax(AppUtil.checkNullData(rs.getString("FAX")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                data.setLineId(AppUtil.checkNullData(rs.getString("LINE_ID")));
                data.setAmphurId(AppUtil.encryptId(rs.getInt("AMPHUR_ID")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public OfficeModel mapFull(OfficeViewModel dataView) {

        OfficeModel data = new OfficeModel();

        try {
            data.setOfficeId(AppUtil.decryptId(dataView.getOfficeId()));
            data.setOfficeName(dataView.getOfficeName());
            data.setOfficeCode(dataView.getOfficeCode());

            data.setLeader(dataView.getLeader());
            data.setMobile(dataView.getMobile());
            data.setFax(dataView.getFax());
            data.setEmail(dataView.getEmail());
            data.setLineId(dataView.getLineId());
            data.setAmphurId(AppUtil.decryptId(dataView.getAmphurId()));

            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
