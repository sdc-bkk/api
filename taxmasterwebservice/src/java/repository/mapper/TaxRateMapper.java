/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.TaxRateModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.TaxRateViewModel;

/**
 *
 * @author Sirichai
 */
public class TaxRateMapper {

    private ResultSet rs = null;

    public TaxRateMapper() {

    }

    public TaxRateMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<TaxRateViewModel> mapFullList() {

        List<TaxRateViewModel> dataList = new ArrayList<TaxRateViewModel>();

        try {

            while (rs.next()) {

                TaxRateViewModel data = new TaxRateViewModel();

                data.setTaxRateId(AppUtil.encryptId(rs.getInt("TAX_RATE_ID")));
                data.setTaxRateName(rs.getString("TAX_RATE_NAME"));
                data.setTaxRateValue(rs.getFloat("TAX_RATE_VALUE"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }
                data.setYearlyFrom(AppUtil.checkNullData(rs.getString("YEARLY_FROM")));
                data.setYearlyTo(AppUtil.checkNullData(rs.getString("YEARLY_TO")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxRateViewModel> mapFullDrpList() {

        List<TaxRateViewModel> dataList = new ArrayList<TaxRateViewModel>();

        try {

            while (rs.next()) {

                TaxRateViewModel data = new TaxRateViewModel();

                data.setTaxRateId(AppUtil.encryptId(rs.getInt("TAX_RATE_ID")));
                data.setTaxRateName(rs.getString("TAX_RATE_NAME"));
                data.setTaxRateValue(rs.getFloat("TAX_RATE_VALUE"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public TaxRateViewModel mapFull() {

        TaxRateViewModel data = new TaxRateViewModel();

        try {

            while (rs.next()) {

                data.setTaxRateId(AppUtil.encryptId(rs.getInt("TAX_RATE_ID")));
                data.setTaxRateName(rs.getString("TAX_RATE_NAME"));
                data.setTaxRateValue(rs.getFloat("TAX_RATE_VALUE"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }

                data.setYearlyFrom(AppUtil.checkNullData(rs.getString("YEARLY_FROM")));
                data.setYearlyTo(AppUtil.checkNullData(rs.getString("YEARLY_TO")));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public TaxRateModel mapFull(TaxRateViewModel dataView) {

        TaxRateModel data = new TaxRateModel();

        try {
            data.setTaxRateId(AppUtil.decryptId(dataView.getTaxRateId()));
            data.setTaxRateName(dataView.getTaxRateName());
            data.setTaxRateValue(dataView.getTaxRateValue());

            data.setStartDate(dataView.getStartDate());
            data.setEndDate(dataView.getEndDate());

            data.setYearlyFrom(dataView.getYearlyFrom());
            data.setYearlyTo(dataView.getYearlyTo());

            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
