/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.FeeModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.FeeViewModel;

/**
 *
 * @author Sirichai
 */
public class FeeMapper {

    private ResultSet rs = null;

    public FeeMapper() {

    }

    public FeeMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<FeeViewModel> mapFullList() {

        List<FeeViewModel> dataList = new ArrayList<FeeViewModel>();

        try {

            while (rs.next()) {

                FeeViewModel data = new FeeViewModel();

                data.setFeeId(AppUtil.encryptId(rs.getInt("FEE_ID")));
                data.setFeeName(rs.getString("FEE_NAME"));
                data.setFeeValue(rs.getFloat("FEE_VALUE"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }
                data.setIsDefault(rs.getBoolean("IS_DEFAULT"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setHasHoliday(rs.getBoolean("HAS_HOLIDAY"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<FeeViewModel> mapFullDrpList() {

        List<FeeViewModel> dataList = new ArrayList<FeeViewModel>();

        try {

            while (rs.next()) {

                FeeViewModel data = new FeeViewModel();

                data.setFeeId(AppUtil.encryptId(rs.getInt("FEE_ID")));
                data.setFeeName(rs.getString("FEE_NAME"));
                data.setFeeValue(rs.getFloat("FEE_VALUE"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public FeeViewModel mapFull() {

        FeeViewModel data = new FeeViewModel();

        try {

            while (rs.next()) {

                data.setFeeId(AppUtil.encryptId(rs.getInt("FEE_ID")));
                data.setFeeName(rs.getString("FEE_NAME"));
                data.setFeeValue(rs.getFloat("FEE_VALUE"));

                if (rs.getDate("START_DATE") != null) {
                    data.setStartDate(DateUtils.toThai(rs.getDate("START_DATE")));
                }
                if (rs.getDate("END_DATE") != null) {
                    data.setEndDate(DateUtils.toThai(rs.getDate("END_DATE")));
                }
                data.setIsDefault(rs.getBoolean("IS_DEFAULT"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setHasHoliday(rs.getBoolean("HAS_HOLIDAY"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public FeeModel mapFull(FeeViewModel dataView) {

        FeeModel data = new FeeModel();

        try {
            data.setFeeId(AppUtil.decryptId(dataView.getFeeId()));
            data.setFeeName(dataView.getFeeName());
            data.setFeeValue(dataView.getFeeValue());

            data.setStartDate(dataView.getStartDate());
            data.setEndDate(dataView.getEndDate());
            data.setIsDefault(dataView.getIsDefault());

            data.setActive(dataView.getActive());

            data.setHasHoliday(dataView.getHasHoliday());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
