/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.CustomerTypeModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.CustomerTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class CustomerTypeMapper {

    private ResultSet rs = null;

    public CustomerTypeMapper() {

    }

    public CustomerTypeMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<CustomerTypeViewModel> mapFullList() {

        List<CustomerTypeViewModel> dataList = new ArrayList<CustomerTypeViewModel>();

        try {

            while (rs.next()) {

                CustomerTypeViewModel data = new CustomerTypeViewModel();

                data.setCustomerTypeId(AppUtil.encryptId(rs.getInt("CUSTOMER_TYPE_ID")));
                data.setCustomerTypeName(rs.getString("CUSTOMER_TYPE_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<CustomerTypeViewModel> mapFullDrpList() {

        List<CustomerTypeViewModel> dataList = new ArrayList<CustomerTypeViewModel>();

        try {

            while (rs.next()) {

                CustomerTypeViewModel data = new CustomerTypeViewModel();

                data.setCustomerTypeId(AppUtil.encryptId(rs.getInt("CUSTOMER_TYPE_ID")));
                data.setCustomerTypeName(rs.getString("CUSTOMER_TYPE_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public CustomerTypeViewModel mapFull() {

        CustomerTypeViewModel data = new CustomerTypeViewModel();

        try {

            while (rs.next()) {

                data.setCustomerTypeId(AppUtil.encryptId(rs.getInt("CUSTOMER_TYPE_ID")));
                data.setCustomerTypeName(rs.getString("CUSTOMER_TYPE_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public CustomerTypeModel mapFull(CustomerTypeViewModel dataView) {

        CustomerTypeModel data = new CustomerTypeModel();

        try {
            data.setCustomerTypeId(AppUtil.decryptId(dataView.getCustomerTypeId()));
            data.setCustomerTypeName(dataView.getCustomerTypeName());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
