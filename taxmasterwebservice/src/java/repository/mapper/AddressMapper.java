/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.AddressModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.AddressViewModel;

/**
 *
 * @author Sirichai
 */
public class AddressMapper {

    private ResultSet rs = null;

    public AddressMapper() {

    }

    public AddressMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<AddressViewModel> mapFullList() {

        List<AddressViewModel> dataList = new ArrayList<AddressViewModel>();

        try {

            while (rs.next()) {

                AddressViewModel data = new AddressViewModel();

                data.setTambonId(rs.getString("TAMBON_ID"));
                data.setTambonThai(rs.getString("TAMBON_THAI"));
                data.setTambonEng(rs.getString("TAMBON_ENG"));
                data.setTambonThaiShort(rs.getString("TAMBON_THAI_SHORT"));
                data.setTambonEngShort(rs.getString("TAMBON_ENG_SHORT"));
                
                data.setDistrictId(rs.getString("DISTRICT_ID"));
                data.setDistrictThai(rs.getString("DISTRICT_THAI"));
                data.setDistrictEng(rs.getString("DISTRICT_ENG"));
                data.setDistrictThaiShort(rs.getString("DISTRICT_THAI_SHORT"));
                data.setDistrictEngShort(rs.getString("DISTRICT_ENG_SHORT"));
                
                data.setProvinceId(rs.getString("PROVINCE_ID"));
                data.setProvinceThai(rs.getString("PROVINCE_THAI"));
                data.setProvinceEng(rs.getString("PROVINCE_ENG"));
                
                data.setPostCodeMain(rs.getString("POST_CODE_MAIN"));
                data.setPostCodeAll(rs.getString("POST_CODE_ALL"));
                data.setPostalCodeRemark(rs.getString("POST_CODE_REMARK"));
                
                data.setZoneName(rs.getString("ZONE_NAME"));
                data.setFourZoneName(rs.getString("FOUR_ZONE_NAME"));
                data.setTouristZoneName(rs.getString("TOURIST_ZONE_NAME"));
                data.setBangkokZoneName(rs.getString("BANGKOK_ZONE_NAME"));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<AddressViewModel> mapFullDrpList() {

        List<AddressViewModel> dataList = new ArrayList<AddressViewModel>();

        try {

            while (rs.next()) {

                AddressViewModel data = new AddressViewModel();

                data.setTambonId(rs.getString("TAMBON_ID"));
                data.setTambonThai(rs.getString("TAMBON_THAI"));
                
                data.setDistrictId(rs.getString("DISTRICT_ID"));
                data.setDistrictThai(rs.getString("DISTRICT_THAI"));
                
                data.setProvinceId(rs.getString("PROVINCE_ID"));
                data.setProvinceThai(rs.getString("PROVINCE_THAI"));
                
                data.setPostCodeMain(rs.getString("POST_CODE_MAIN"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public List<AddressViewModel> mapFullDrpListProvince() {

        List<AddressViewModel> dataList = new ArrayList<AddressViewModel>();

        try {

            while (rs.next()) {

                AddressViewModel data = new AddressViewModel();

                data.setProvinceId(rs.getString("PROVINCE_ID"));
                data.setProvinceThai(rs.getString("PROVINCE_THAI"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public List<AddressViewModel> mapFullDrpListDistrict() {

        List<AddressViewModel> dataList = new ArrayList<AddressViewModel>();

        try {

            while (rs.next()) {

                AddressViewModel data = new AddressViewModel();

                data.setDistrictId(rs.getString("DISTRICT_ID"));
                data.setDistrictThai(rs.getString("DISTRICT_THAI"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    /*
    public AddressViewModel mapFull() {

        AddressViewModel data = new AddressViewModel();

        try {

            while (rs.next()) {

                //data.setTitleId(AppUtil.encryptId(rs.getInt("TITLE_ID")));
                //data.setTitleName(rs.getString("TITLE_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public AddressModel mapFull(AddressViewModel dataView) {

        AddressModel data = new AddressModel();

        try {
            //data.setTitleId(AppUtil.decryptId(dataView.getTitleId()));
            //data.setTitleName(dataView.getTitleName());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
    */

}
