/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.OilTypeModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.OilTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class OilTypeMapper {

    private ResultSet rs = null;

    public OilTypeMapper() {

    }

    public OilTypeMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<OilTypeViewModel> mapFullList() {

        List<OilTypeViewModel> dataList = new ArrayList<OilTypeViewModel>();

        try {

            while (rs.next()) {

                OilTypeViewModel data = new OilTypeViewModel();

                data.setOilTypeId(AppUtil.encryptId(rs.getInt("OIL_TYPE_ID")));
                data.setOilTypeName(rs.getString("OIL_TYPE_NAME"));
                data.setOilTypeCode(rs.getString("OIL_TYPE_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<OilTypeViewModel> mapFullDrpList() {

        List<OilTypeViewModel> dataList = new ArrayList<OilTypeViewModel>();

        try {

            while (rs.next()) {

                OilTypeViewModel data = new OilTypeViewModel();

                data.setOilTypeId(AppUtil.encryptId(rs.getInt("OIL_TYPE_ID")));
                data.setOilTypeName(rs.getString("OIL_TYPE_NAME"));
                data.setOilTypeCode(rs.getString("OIL_TYPE_CODE"));
                data.setOilTypeRate(0.05);
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public OilTypeViewModel mapFull() {

        OilTypeViewModel data = new OilTypeViewModel();

        try {

            while (rs.next()) {

                data.setOilTypeId(AppUtil.encryptId(rs.getInt("OIL_TYPE_ID")));
                data.setOilTypeName(rs.getString("OIL_TYPE_NAME"));
                data.setOilTypeCode(rs.getString("OIL_TYPE_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public OilTypeModel mapFull(OilTypeViewModel dataView) {

        OilTypeModel data = new OilTypeModel();

        try {
            data.setOilTypeId(AppUtil.decryptId(dataView.getOilTypeId()));
            data.setOilTypeName(dataView.getOilTypeName());
            data.setOilTypeCode(dataView.getOilTypeCode());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
