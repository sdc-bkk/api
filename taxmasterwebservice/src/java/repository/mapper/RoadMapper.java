/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.RoadModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.RoadViewModel;

/**
 *
 * @author Sirichai
 */
public class RoadMapper {

    private ResultSet rs = null;

    public RoadMapper() {

    }

    public RoadMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<RoadViewModel> mapFullList() {

        List<RoadViewModel> dataList = new ArrayList<RoadViewModel>();

        try {

            while (rs.next()) {

                RoadViewModel data = new RoadViewModel();

                data.setRoadId(AppUtil.encryptId(rs.getInt("ROAD_ID")));
                data.setRoadName(rs.getString("ROAD_NAME"));
                
                data.setTambonId(rs.getString("TAMBON_ID"));
                data.setTambonThai(rs.getString("TAMBON_THAI"));
                data.setDistrictId(rs.getString("DISTRICT_ID"));
                data.setDistrictThai(rs.getString("DISTRICT_THAI"));
                data.setProvinceId(rs.getString("PROVINCE_ID"));
                data.setProvinceThai(rs.getString("PROVINCE_THAI"));
                //private AddressViewModel address;
                //data.setAddress(null);
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<RoadViewModel> mapFullDrpList() {

        List<RoadViewModel> dataList = new ArrayList<RoadViewModel>();

        try {

            while (rs.next()) {

                RoadViewModel data = new RoadViewModel();

                data.setRoadId(AppUtil.encryptId(rs.getInt("ROAD_ID")));
                data.setRoadName(rs.getString("ROAD_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public RoadViewModel mapFull() {

        RoadViewModel data = new RoadViewModel();

        try {

            while (rs.next()) {

                data.setRoadId(AppUtil.encryptId(rs.getInt("ROAD_ID")));
                data.setRoadName(rs.getString("ROAD_NAME"));
                
                data.setTambonId(rs.getString("TAMBON_ID"));
                data.setTambonThai(rs.getString("TAMBON_THAI"));
                data.setDistrictId(rs.getString("DISTRICT_ID"));
                data.setDistrictThai(rs.getString("DISTRICT_THAI"));
                data.setProvinceId(rs.getString("PROVINCE_ID"));
                data.setProvinceThai(rs.getString("PROVINCE_THAI"));
                //private AddressViewModel address;
                //data.setAddress(null);
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public RoadModel mapFull(RoadViewModel dataView) {

        RoadModel data = new RoadModel();

        try {
            data.setRoadId(AppUtil.decryptId(dataView.getRoadId()));
            data.setRoadName(dataView.getRoadName());
            
            data.setTambonId(dataView.getTambonId());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
