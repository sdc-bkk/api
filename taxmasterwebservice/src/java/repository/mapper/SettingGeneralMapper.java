/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.SettingGeneralModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.SettingGeneralViewModel;

/**
 *
 * @author Sirichai
 */
public class SettingGeneralMapper {

    private ResultSet rs = null;

    public SettingGeneralMapper() {

    }

    public SettingGeneralMapper(ResultSet rs) {
        this.rs = rs;
    }

    public SettingGeneralModel mapFull() {

        SettingGeneralModel data = new SettingGeneralModel();

        try {

            while (rs.next()) {

                data.setSettingGeneralId(rs.getInt("SETTING_GENERAL_ID"));
                data.setSettingGeneralName(rs.getString("SETTING_GENERAL_NAME"));
                data.setSettingGeneralDesc(rs.getString("SETTING_GENERAL_DESC"));
                data.setValueText(rs.getString("VALUE_TEXT"));

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
