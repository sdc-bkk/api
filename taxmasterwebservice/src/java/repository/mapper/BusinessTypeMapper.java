/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.BusinessTypeModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.BusinessTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class BusinessTypeMapper {

    private ResultSet rs = null;

    public BusinessTypeMapper() {

    }

    public BusinessTypeMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<BusinessTypeViewModel> mapFullList() {

        List<BusinessTypeViewModel> dataList = new ArrayList<BusinessTypeViewModel>();

        try {

            while (rs.next()) {

                BusinessTypeViewModel data = new BusinessTypeViewModel();

                data.setBusinessTypeId(AppUtil.encryptId(rs.getInt("BUSINESS_TYPE_ID")));
                data.setBusinessTypeName(rs.getString("BUSINESS_TYPE_NAME"));
                data.setBusinessTypeCode(rs.getString("BUSINESS_TYPE_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<BusinessTypeViewModel> mapFullDrpList() {

        List<BusinessTypeViewModel> dataList = new ArrayList<BusinessTypeViewModel>();

        try {

            while (rs.next()) {

                BusinessTypeViewModel data = new BusinessTypeViewModel();

                data.setBusinessTypeId(AppUtil.encryptId(rs.getInt("BUSINESS_TYPE_ID")));
                data.setBusinessTypeName(rs.getString("BUSINESS_TYPE_NAME"));
                data.setBusinessTypeCode(rs.getString("BUSINESS_TYPE_CODE"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public BusinessTypeViewModel mapFull() {

        BusinessTypeViewModel data = new BusinessTypeViewModel();

        try {

            while (rs.next()) {

                data.setBusinessTypeId(AppUtil.encryptId(rs.getInt("BUSINESS_TYPE_ID")));
                data.setBusinessTypeName(rs.getString("BUSINESS_TYPE_NAME"));
                data.setBusinessTypeCode(rs.getString("BUSINESS_TYPE_CODE"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public BusinessTypeModel mapFull(BusinessTypeViewModel dataView) {

        BusinessTypeModel data = new BusinessTypeModel();

        try {
            data.setBusinessTypeId(AppUtil.decryptId(dataView.getBusinessTypeId()));
            data.setBusinessTypeName(dataView.getBusinessTypeName());
            data.setBusinessTypeCode(dataView.getBusinessTypeCode());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
