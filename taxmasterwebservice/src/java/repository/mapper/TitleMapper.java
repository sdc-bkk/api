/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.TitleModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.TitleViewModel;

/**
 *
 * @author Sirichai
 */
public class TitleMapper {

    private ResultSet rs = null;

    public TitleMapper() {

    }

    public TitleMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<TitleViewModel> mapFullList() {

        List<TitleViewModel> dataList = new ArrayList<TitleViewModel>();

        try {

            while (rs.next()) {

                TitleViewModel data = new TitleViewModel();

                data.setTitleId(AppUtil.encryptId(rs.getInt("TITLE_ID")));
                data.setTitleName(rs.getString("TITLE_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TitleViewModel> mapFullDrpList() {

        List<TitleViewModel> dataList = new ArrayList<TitleViewModel>();

        try {

            while (rs.next()) {

                TitleViewModel data = new TitleViewModel();

                data.setTitleId(AppUtil.encryptId(rs.getInt("TITLE_ID")));
                data.setTitleName(rs.getString("TITLE_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public TitleViewModel mapFull() {

        TitleViewModel data = new TitleViewModel();

        try {

            while (rs.next()) {

                data.setTitleId(AppUtil.encryptId(rs.getInt("TITLE_ID")));
                data.setTitleName(rs.getString("TITLE_NAME"));
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(rs.getString("CREATED_BY"));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public TitleModel mapFull(TitleViewModel dataView) {

        TitleModel data = new TitleModel();

        try {
            data.setTitleId(AppUtil.decryptId(dataView.getTitleId()));
            data.setTitleName(dataView.getTitleName());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
