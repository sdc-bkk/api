/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.SoiModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.SoiFilterModel;
import repository.mapper.SoiMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.SoiViewModel;

/**
 *
 * @author Sirichai
 */
public class SoiRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<SoiViewModel>> getListAll(ResultPage page, SoiFilterModel filter) throws SQLException {

        ResultData<List<SoiViewModel>> resultData = new ResultData<List<SoiViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT S.*, R.ROAD_NAME, "
                       + "    T.TAMBON_THAI, T.DISTRICT_ID, T.DISTRICT_THAI, T.PROVINCE_ID, T.PROVINCE_THAI "
                       + " FROM M_SOI S "
                       + "    LEFT JOIN M_ROAD R ON S.ROAD_ID = R.ROAD_ID "
                       + "    LEFT JOIN M_TAMBON T ON S.TAMBON_ID = T.TAMBON_ID "
                       + " WHERE (? IS NULL OR S.IS_ACTIVE = ?) ";
            
            if (!AppUtil.isNullAndSpace(filter.getSoiName())) {
                sql += " AND S.SOI_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getRoadId()) && AppUtil.decryptId(filter.getRoadId()) > 0) {
                sql += " AND R.ROAD_ID = ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getRoadName())) {
                sql += " AND R.ROAD_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonId())) {
                sql += " AND T.TAMBON_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonThai())) {
                sql += " AND T.TAMBON_THAI LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictId())) {
                sql += " AND T.DISTRICT_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictThai())) {
                sql += " AND T.DISTRICT_THAI LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceId())) {
                sql += " AND T.PROVINCE_ID LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceThai())) {
                sql += " AND T.PROVINCE_THAI LIKE ? ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("soiName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getSoiName())) {
                ps.setString(i++, "%" + filter.getSoiName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getRoadId()) && AppUtil.decryptId(filter.getRoadId()) > 0) {
                ps.setInt(i++, AppUtil.decryptId(filter.getRoadId()));
            }

            if (!AppUtil.isNullAndSpace(filter.getRoadName())) {
                ps.setString(i++, "%" + filter.getRoadName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonId())) {
                ps.setString(i++, filter.getTambonId());
            }

            if (!AppUtil.isNullAndSpace(filter.getTambonThai())) {
                ps.setString(i++, "%" + filter.getTambonThai() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictId())) {
                ps.setString(i++, filter.getDistrictId());
            }

            if (!AppUtil.isNullAndSpace(filter.getDistrictThai())) {
                ps.setString(i++, "%" + filter.getDistrictThai() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceId())) {
                ps.setString(i++, filter.getProvinceId());
            }

            if (!AppUtil.isNullAndSpace(filter.getProvinceThai())) {
                ps.setString(i++, "%" + filter.getProvinceThai() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<SoiViewModel> dataList = new SoiMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<SoiViewModel> dataList = new SoiMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " SOI_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("soiName")) {
                str = " SOI_NAME ";
            } else if (orderBy.equalsIgnoreCase("roadName")) {
                str = " PROVINCE_THAI,DISTRICT_THAI,TAMBON_THAI,ROAD_NAME,SOI_NAME ";
            } else if (orderBy.equalsIgnoreCase("tambonThai")) {
                str = " PROVINCE_THAI,DISTRICT_THAI,TAMBON_THAI,ROAD_NAME ";
            } else if (orderBy.equalsIgnoreCase("districtThai")) {
                str = " PROVINCE_THAI,DISTRICT_THAI ";
            } else if (orderBy.equalsIgnoreCase("provinceThai")) {
                str = " PROVINCE_THAI ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_SOI "
                        + " WHERE SOI_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<SoiViewModel> getData(int id) throws SQLException {

        ResultData<SoiViewModel> resultData = new ResultData<SoiViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);


            String sql = " SELECT S.*, R.ROAD_NAME, "
                       + "    T.TAMBON_THAI, T.DISTRICT_ID, T.DISTRICT_THAI, T.PROVINCE_ID, T.PROVINCE_THAI "
                       + " FROM M_SOI S "
                       + "    LEFT JOIN M_ROAD R ON S.ROAD_ID = R.ROAD_ID "
                       + "    LEFT JOIN M_TAMBON T ON S.TAMBON_ID = T.TAMBON_ID "
                       + " WHERE SOI_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            SoiViewModel data = new SoiMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(SoiFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_SOI "
                        + " WHERE (? = 0 OR SOI_ID <> ?) "
//                        + " AND (TAMBON_ID = ?) "
//                        + " AND (ROAD_ID = ?) "
                        + " AND (SOI_NAME = ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getSoiId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getSoiId()));
//            ps.setString(i++, filter.getTambonId());
//            ps.setInt(i++, AppUtil.decryptId(filter.getRoadId()));
            ps.setString(i++, filter.getSoiName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(SoiModel soi) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Soi">
            if (soi.getSoiId() == 0) {

                sql = "INSERT INTO M_SOI ( "
                        + "  SOI_NAME "
                        + ", TAMBON_ID "
                        + ", ROAD_ID "
                        + ", IS_ACTIVE "
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "SOI_ID" });
                
            } else {

                sql = "UPDATE M_SOI SET "
                        + "  SOI_NAME = ? "
                        + ", TAMBON_ID = ? "
                        + ", ROAD_ID = ? "
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE SOI_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, soi.getSoiName());
            ps.setString(i++, soi.getTambonId());
            ps.setInt(i++, soi.getRoadId());
            ps.setBoolean(i++, soi.isActive());

            if (soi.getSoiId() == 0) {

                if (!AppUtil.isNullAndSpace(soi.getUpdatedBy())) {
                    ps.setString(i++, soi.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                soi.setSoiId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(soi.getUpdatedBy())) {
                    ps.setString(i++, soi.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, soi.getSoiId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, SoiFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete Soi by id list">
            sql = " DELETE FROM M_SOI "
                + " WHERE SOI_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete Soi by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
