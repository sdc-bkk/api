/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.SettingGeneralEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.SettingGeneralModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.SettingGeneralFilterModel;
import repository.mapper.SettingGeneralMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.SettingGeneralViewModel;

/**
 *
 * @author Sirichai
 */
public class SettingGeneralRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public String getDataValue(SettingGeneralEnum name) throws SQLException {

        String result = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT VALUE_TEXT "
                       + " FROM M_SETTING_GENERAL "
                       + " WHERE SETTING_GENERAL_NAME = ? ";

            ps.setSql(sql);

            ps.setString(1, name.value());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString("VALUE_TEXT");
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public boolean saveData(SettingGeneralModel settingGeneral) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";
            
            //<editor-fold defaultstate="collapsed" desc="HistorySettingGeneral">
            if (settingGeneral.getSettingGeneralId() > 0) {
                
                sql = " INSERT INTO M_HISTORY_SETTING_GENERAL "
                    + " (HISTORY_DATE, SETTING_GENERAL_ID, SETTING_GENERAL_NAME, SETTING_GENERAL_DESC, "
                    + "   VALUE_TEXT, UPDATED_DATE, UPDATED_BY) "
                    + " SELECT SYSDATE, SETTING_GENERAL_ID, SETTING_GENERAL_NAME, SETTING_GENERAL_DESC, "
                    + "   VALUE_TEXT, UPDATED_DATE, UPDATED_BY "
                    + " FROM M_SETTING_GENERAL "
                    + " WHERE SETTING_GENERAL_ID = ? ";

                ps = conn.prepareStatement(sql);

                ps.setInt(i++, settingGeneral.getSettingGeneralId());
                
                boolean resultHistory = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            i = 1;
            //<editor-fold defaultstate="collapsed" desc="SettingGeneral">
            if (settingGeneral.getSettingGeneralId() == 0) {

                sql = "INSERT INTO M_SETTING_GENERAL ( "
                        + "  SETTING_GENERAL_NAME "
                        + ", SETTING_GENERAL_DESC "
                        + ", VALUE_TEXT "
                        
                        + ", UPDATED_DATE "
                        + ", UPDATED_BY"
                        + ") VALUES(?,?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "SETTING_GENERAL_ID" });
                
            } else {

                sql = "UPDATE M_SETTING_GENERAL SET "
                        + "  SETTING_GENERAL_NAME = ? "
                        + ", SETTING_GENERAL_DESC = ? "
                        + ", VALUE_TEXT = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE SETTING_GENERAL_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, settingGeneral.getSettingGeneralName());
            ps.setString(i++, settingGeneral.getSettingGeneralDesc());
            ps.setString(i++, settingGeneral.getValueText());

            if (settingGeneral.getSettingGeneralId() == 0) {

                if (!AppUtil.isNullAndSpace(settingGeneral.getUpdatedBy())) {
                    ps.setString(i++, settingGeneral.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                settingGeneral.setSettingGeneralId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(settingGeneral.getUpdatedBy())) {
                    ps.setString(i++, settingGeneral.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, settingGeneral.getSettingGeneralId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }
 
    public SettingGeneralModel getDataByName(String name) throws SQLException {

        SettingGeneralModel resultData = new SettingGeneralModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_SETTING_GENERAL "
                       + " WHERE SETTING_GENERAL_NAME = ? ";

            ps.setSql(sql);

            ps.setString(1, name);

            rs = ps.executeQuery();

            resultData = new SettingGeneralMapper(rs).mapFull();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    
/*   
    public boolean duplicate(SettingGeneralFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;


            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_SETTING_GENERAL "
                        + " WHERE (? = 0 OR SETTING_GENERAL_ID <> ?) "
                        + " AND (SETTING_GENERAL_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getSettingGeneralId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getSettingGeneralId()));
            //ps.setString(i++, filter.getSettingGeneralName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }
*/
}
