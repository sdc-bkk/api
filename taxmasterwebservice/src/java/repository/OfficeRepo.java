/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.OfficeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.OfficeFilterModel;
import repository.mapper.OfficeMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.OfficeViewModel;

/**
 *
 * @author Sirichai
 */
public class OfficeRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<OfficeViewModel>> getList(ResultPage page, OfficeFilterModel filter) throws SQLException {

        ResultData<List<OfficeViewModel>> resultData = new ResultData<List<OfficeViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT DISTINCT DISTRICT_ID ,DISTRICT_THAI ,OFFICE.*\n"
                    + "FROM M_TAMBON \n"
                    + "LEFT JOIN OFFICE ON M_TAMBON.DISTRICT_ID = OFFICE.AMPHUR_ID\n"
                    + "WHERE PROVINCE_ID = 10 ";

            if (!AppUtil.isNullAndSpace(filter.getOfficeName())) {
                sql += " AND OFFICE.OFFICE_NAME LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeCode())) {
                sql += " AND OFFICE.OFFICE_CODE LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getLeader())) {
                sql += " AND OFFICE.LEADER LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                sql += " AND OFFICE.MOBILE LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getFax())) {
                sql += " AND OFFICE.FAX LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                sql += " AND OFFICE.EMAIL LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getLineId())) {
                sql += " AND OFFICE.LINE_ID LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("DISTRICT_THAI");
                filter.setSort("ASC ");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

//            if (!AppUtil.isNull(filter.getActive())) {
//                ps.setBoolean(i++, filter.getActive());
//                ps.setBoolean(i++, filter.getActive());
//            } else {
//                ps.setNull(i++);
//                ps.setNull(i++);
//            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeName())) {
                ps.setString(i++, "%" + filter.getOfficeName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeCode())) {
                ps.setString(i++, "%" + filter.getOfficeCode() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getLeader())) {
                ps.setString(i++, "%" + filter.getLeader() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                ps.setString(i++, "%" + filter.getMobile() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getFax())) {
                ps.setString(i++, "%" + filter.getFax() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                ps.setString(i++, "%" + filter.getEmail() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getLineId())) {
                ps.setString(i++, "%" + filter.getLineId() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<OfficeViewModel> dataList = new OfficeMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<OfficeViewModel> dataList = new OfficeMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<List<OfficeViewModel>> getListAll(ResultPage page, OfficeFilterModel filter) throws SQLException {

        ResultData<List<OfficeViewModel>> resultData = new ResultData<List<OfficeViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM OFFICE "
                    + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getOfficeName())) {
                sql += " AND OFFICE_NAME LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeCode())) {
                sql += " AND OFFICE_CODE LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getLeader())) {
                sql += " AND LEADER LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                sql += " AND MOBILE LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getFax())) {
                sql += " AND FAX LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                sql += " AND EMAIL LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getLineId())) {
                sql += " AND LINE_ID LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("officeName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeName())) {
                ps.setString(i++, "%" + filter.getOfficeName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getOfficeCode())) {
                ps.setString(i++, "%" + filter.getOfficeCode() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getLeader())) {
                ps.setString(i++, "%" + filter.getLeader() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                ps.setString(i++, "%" + filter.getMobile() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getFax())) {
                ps.setString(i++, "%" + filter.getFax() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                ps.setString(i++, "%" + filter.getEmail() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getLineId())) {
                ps.setString(i++, "%" + filter.getLineId() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<OfficeViewModel> dataList = new OfficeMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<OfficeViewModel> dataList = new OfficeMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " OFFICE_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("officeName")) {
                str = "  DISTRICT_THAI ";
            } else if (orderBy.equalsIgnoreCase("officeCode")) {
                str = " OFFICE.OFFICE_CODE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN OFFICE.UPDATED_DATE IS NULL THEN OFFICE.CREATED_DATE ELSE OFFICE.UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM OFFICE "
                    + " WHERE AMPHUR_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<OfficeViewModel> getData(int id) throws SQLException {

        ResultData<OfficeViewModel> resultData = new ResultData<OfficeViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT DISTINCT DISTRICT_ID ,DISTRICT_THAI ,OFFICE.*\n"
                    + "FROM M_TAMBON \n"
                    + "LEFT JOIN OFFICE ON M_TAMBON.DISTRICT_ID = OFFICE.AMPHUR_ID\n"
                    + "WHERE PROVINCE_ID = 10 AND DISTRICT_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            OfficeViewModel data = new OfficeMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public String getOfficeId(int id) throws SQLException {

        String result = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT OFFICE_ID "
                    + " FROM OFFICE "
                    + " WHERE AMPHUR_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = AppUtil.encryptId(rs.getInt("OFFICE_ID"));
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public boolean duplicate(OfficeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM OFFICE "
                    + " WHERE (? = 0 OR AMPHUR_ID <> ?) "
                    + " AND (OFFICE_NAME LIKE ?) ";
            //+ " AND (STATION_CODE LIKE ?) "

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getOfficeId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getOfficeId()));
            ps.setString(i++, filter.getOfficeName());
            //ps.setString(i++, filter.getOfficeCode());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(OfficeModel data) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Office">
            if (data.getOfficeId() == 0) {

                sql = "INSERT INTO OFFICE ( "
                        + "  OFFICE_NAME "
                        + ", OFFICE_CODE"
                        + ", LEADER "
                        + ", MOBILE "
                        + ", FAX "
                        + ", EMAIL "
                        + ", LINE_ID "
                        + ", AMPHUR_ID "
                        + ", IS_ACTIVE"
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?, ?,?,?,?,?,?, ?,SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[]{"OFFICE_ID"});

            } else {

                sql = "UPDATE OFFICE SET "
                        + "  OFFICE_NAME = ? "
                        + ", OFFICE_CODE = ? "
                        + ", LEADER = ? "
                        + ", MOBILE = ? "
                        + ", FAX = ? "
                        + ", EMAIL = ? "
                        + ", LINE_ID = ? "
                        + ", AMPHUR_ID = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE OFFICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, data.getOfficeName());
            ps.setString(i++, data.getOfficeCode());

            ps.setString(i++, data.getLeader());
            ps.setString(i++, data.getMobile());
            ps.setString(i++, data.getFax());
            ps.setString(i++, data.getEmail());
            ps.setString(i++, data.getLineId());
            ps.setInt(i++, data.getAmphurId());

            ps.setBoolean(i++, data.isActive());

            if (data.getOfficeId() == 0) {

                if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                    ps.setString(i++, data.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setOfficeId(id);

            } else {

                if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                    ps.setString(i++, data.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, data.getOfficeId());

                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>
            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, OfficeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete Office by id list">
            sql = " DELETE FROM OFFICE "
                    + " WHERE OFFICE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete Office by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
