/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.TaxRateModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.TaxRateFilterModel;
import repository.mapper.TaxRateMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.TaxRateViewModel;

/**
 *
 * @author Sirichai
 */
public class TaxRateRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<TaxRateViewModel>> getListAll(ResultPage page, TaxRateFilterModel filter) throws SQLException {

        ResultData<List<TaxRateViewModel>> resultData = new ResultData<List<TaxRateViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;
            String sql = "SELECT * FROM M_TAX_RATE "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getTaxRateName())) {
                sql += " AND TAX_RATE_NAME LIKE ? ";
            }

            if (filter.getTaxRateValue() != null && filter.getTaxRateValue() > 0) {
                sql += " AND TAX_RATE_VALUE = ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND TRUNC(START_DATE) >= TRUNC(TO_DATE(?, 'dd/mm/yyyy')) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND TRUNC(END_DATE) <= TRUNC(TO_DATE(?, 'dd/mm/yyyy')) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("taxRateName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getTaxRateName())) {
                ps.setString(i++, "%" + filter.getTaxRateName() + "%");
            }

            if (filter.getTaxRateValue() != null && filter.getTaxRateValue() > 0) {
                ps.setFloat(i++, filter.getTaxRateValue());
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }
            
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<TaxRateViewModel> dataList = new TaxRateMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<TaxRateViewModel> dataList = new TaxRateMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " TAX_RATE_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("taxRateName")) {
                str = " TAX_RATE_NAME ";
            } else if (orderBy.equalsIgnoreCase("taxRateValue")) {
                str = " TAX_RATE_VALUE ";
            } else if (orderBy.equalsIgnoreCase("startDate")) {
                str = " START_DATE ";
            } else if (orderBy.equalsIgnoreCase("endDate")) {
                str = " END_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_TAX_RATE "
                        + " WHERE TAX_RATE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<TaxRateViewModel> getData(int id) throws SQLException {

        ResultData<TaxRateViewModel> resultData = new ResultData<TaxRateViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_TAX_RATE "
                       + " WHERE TAX_RATE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            TaxRateViewModel data = new TaxRateMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(TaxRateFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;
            String tempStartDate = "";
            String tempEndDate = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                tempStartDate = DateUtils.toEng(DateUtils.toEng(filter.getStartDate()), "yyyy-MM-dd");
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "yyyy-MM-dd");
//                tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "YYYY-MM-DD");
            }
            
            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_TAX_RATE "
                        + " WHERE (? = 0 OR TAX_RATE_ID <> ?) AND IS_ACTIVE = 1 "
                        + " AND ( ";
//                        + "    (TAX_RATE_NAME LIKE ?) ";
                        //+ " AND ((TO_DATE(?,'YYYY-MM-DD') IS NULL OR TO_DATE(?,'YYYY-MM-DD') BETWEEN START_DATE AND END_DATE) "
                        //+ "   OR (TO_DATE(?,'YYYY-MM-DD') IS NULL OR TO_DATE(?,'YYYY-MM-DD') BETWEEN START_DATE AND END_DATE)) ";

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                sql += "     ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                sql += "   OR  ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            sql += " ) ";
            
            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getTaxRateId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getTaxRateId()));
//            ps.setString(i++, filter.getTaxRateName());
            
            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                ps.setString(i++, tempStartDate);
                ps.setString(i++, tempStartDate);
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                ps.setString(i++, tempEndDate);
                ps.setString(i++, tempEndDate);
            }
            /*
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                //ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())));
                //ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())));
                String tempStartDate = DateUtils.toEng(DateUtils.toEng(filter.getStartDate()), "YYYY-MM-DD");
                if (!AppUtil.isNullAndSpace(tempStartDate)) {
                    ps.setString(i++, tempStartDate);
                    ps.setString(i++, tempStartDate);
                } else {
                    ps.setNull(i++);
                    ps.setNull(i++);
                }
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                //ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())));
                //ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())));
                String tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "YYYY-MM-DD");
                if (!AppUtil.isNullAndSpace(tempEndDate)) {
                    ps.setString(i++, tempEndDate);
                    ps.setString(i++, tempEndDate);
                } else {
                    ps.setNull(i++);
                    ps.setNull(i++);
                }
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }
            */

                    
            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(TaxRateModel taxRate) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="TaxRate">
            if (taxRate.getTaxRateId() == 0) {

                sql = "INSERT INTO M_TAX_RATE ( "
                        + "  TAX_RATE_NAME "
                        + ", TAX_RATE_VALUE "
                        + ", START_DATE "
                        + ", END_DATE "
                        
                        + ", YEARLY_FROM "
                        + ", YEARLY_TO "
                        
                        + ", IS_ACTIVE "
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?, ?,?, ?,SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "TAX_RATE_ID" });
                
            } else {

                sql = "UPDATE M_TAX_RATE SET "
                        + "  TAX_RATE_NAME = ? "
                        + ", TAX_RATE_VALUE = ? "
                        + ", START_DATE = ? "
                        + ", END_DATE = ? "
                        
                        + ", YEARLY_FROM = ? "
                        + ", YEARLY_TO = ? "
                        
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE TAX_RATE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, taxRate.getTaxRateName());
            ps.setFloat(i++, taxRate.getTaxRateValue());
            
            if (!AppUtil.isNullAndSpace(taxRate.getStartDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(taxRate.getStartDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }

            if (!AppUtil.isNullAndSpace(taxRate.getEndDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(taxRate.getEndDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }
            ps.setString(i++, taxRate.getYearlyFrom());
            ps.setString(i++, taxRate.getYearlyTo());

            ps.setBoolean(i++, taxRate.isActive());

            if (taxRate.getTaxRateId() == 0) {

                if (!AppUtil.isNullAndSpace(taxRate.getUpdatedBy())) {
                    ps.setString(i++, taxRate.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                taxRate.setTaxRateId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(taxRate.getUpdatedBy())) {
                    ps.setString(i++, taxRate.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, taxRate.getTaxRateId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, TaxRateFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete TaxRate by id list">
            sql = " DELETE FROM M_TAX_RATE "
                + " WHERE TAX_RATE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete TaxRate by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
