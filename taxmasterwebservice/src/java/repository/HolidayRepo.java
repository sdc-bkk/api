/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.HolidayModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.HolidayFilterModel;
import repository.mapper.HolidayMapper;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.HolidayViewModel;

/**
 *
 * @author Sirichai
 */
public class HolidayRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<HolidayViewModel>> getListAll(ResultPage page, HolidayFilterModel filter) throws SQLException {

        ResultData<List<HolidayViewModel>> resultData = new ResultData<List<HolidayViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_HOLIDAYS "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getHolidayName())) {
                sql += " AND HOLIDAY_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                sql += " AND YEARLY = ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                sql += " AND TRUNC(START_DATE) >= TRUNC(TO_DATE(?, 'dd/mm/yyyy')) ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                sql += " AND TRUNC(END_DATE) <= TRUNC(TO_DATE(?, 'dd/mm/yyyy')) ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("holidayName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getHolidayName())) {
                ps.setString(i++, "%" + filter.getHolidayName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getYearly())) {
                ps.setString(i++, filter.getYearly());
            }

            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getStartDate()));
            }
            
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                ps.setString(i++, AppUtil.toDateENString(filter.getEndDate()));
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<HolidayViewModel> dataList = new HolidayMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<HolidayViewModel> dataList = new HolidayMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " HOLIDAY_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("holidayName")) {
                str = " HOLIDAY_NAME ";
            } else if (orderBy.equalsIgnoreCase("yearly")) {
                str = " YEARLY ";
            } else if (orderBy.equalsIgnoreCase("startDate")) {
                str = " START_DATE ";
            } else if (orderBy.equalsIgnoreCase("endDate")) {
                str = " END_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_HOLIDAYS "
                        + " WHERE HOLIDAY_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<HolidayViewModel> getData(int id) throws SQLException {

        ResultData<HolidayViewModel> resultData = new ResultData<HolidayViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_HOLIDAYS "
                       + " WHERE HOLIDAY_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            HolidayViewModel data = new HolidayMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(HolidayFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;
            String tempStartDate = "";
            String tempEndDate = "";
            if (!AppUtil.isNullAndSpace(filter.getStartDate())) {
                tempStartDate = DateUtils.toEng(DateUtils.toEng(filter.getStartDate()), "yyyy-MM-dd");
            }
            if (!AppUtil.isNullAndSpace(filter.getEndDate())) {
                tempEndDate = DateUtils.toEng(DateUtils.toEng(filter.getEndDate()), "yyyy-MM-dd");
            }

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_HOLIDAYS "
                        + " WHERE (? = 0 OR FEE_ID <> ?) "
                        + " AND ( "
                        + "    (HOLIDAY_NAME = ?) ";
                        //+ " AND (FEE_NAME LIKE ?) "
                        //+ " AND ((? IS NULL OR ? BETWEEN START_DATE AND END_DATE) "
                        //+ "   OR (? IS NULL OR ? BETWEEN START_DATE AND END_DATE)) ";

            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                sql += "    OR ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                sql += "    OR ( START_DATE <= TO_DATE(?, 'yyyy-MM-dd') AND END_DATE >= TO_DATE(?, 'yyyy-MM-dd') ) ";
            }
            sql += " ) ";
            
            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getHolidayId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getHolidayId()));
            ps.setString(i++, filter.getHolidayName());
            
            if (!AppUtil.isNullAndSpace(tempStartDate)) {
                ps.setString(i++, tempStartDate);
                ps.setString(i++, tempStartDate);
            }
            if (!AppUtil.isNullAndSpace(tempEndDate)) {
                ps.setString(i++, tempEndDate);
                ps.setString(i++, tempEndDate);
            }
            /*
            if (filter.getStartDate() != null && DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())) != null) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())));
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getStartDate())));
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }
            if (filter.getEndDate() != null && DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())) != null) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())));
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(filter.getEndDate())));
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }
            */

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(HolidayModel data) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Holiday">
            if (data.getHolidayId() == 0) {

                sql = "INSERT INTO M_HOLIDAYS ( "
                        + "  HOLIDAY_NAME "
                        + ", YEARLY "
                        + ", START_DATE "
                        + ", END_DATE "
                        
                        + ", IS_ACTIVE "
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?,?, ?,SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "HOLIDAY_ID" });
                
            } else {

                sql = "UPDATE M_HOLIDAYS SET "
                        + "  HOLIDAY_NAME = ? "
                        + ", YEARLY = ? "
                        + ", START_DATE = ? "
                        + ", END_DATE = ? "
                        
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE HOLIDAY_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, data.getHolidayName());
            ps.setString(i++, data.getYearly());
            
            if (!AppUtil.isNullAndSpace(data.getStartDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(data.getStartDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }
            if (!AppUtil.isNullAndSpace(data.getEndDate())) {
                ps.setDate(i++, DateUtils.toSqlDate(DateUtils.toEng(data.getEndDate())));
            } else {
                ps.setNull(i++, java.sql.Types.DATE);
            }

            ps.setBoolean(i++, data.getActive());

            if (data.getHolidayId() == 0) {

                if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                    ps.setString(i++, data.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setHolidayId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(data.getUpdatedBy())) {
                    ps.setString(i++, data.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, data.getHolidayId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, HolidayFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete Holiday by id list">
            sql = " DELETE FROM M_HOLIDAYS "
                + " WHERE HOLIDAY_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete Holiday by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
