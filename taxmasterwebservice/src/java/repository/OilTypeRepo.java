/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.OilTypeModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.OilTypeFilterModel;
import repository.mapper.OilTypeMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.OilTypeViewModel;

/**
 *
 * @author Sirichai
 */
public class OilTypeRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<OilTypeViewModel>> getListAll(ResultPage page, OilTypeFilterModel filter) throws SQLException {

        ResultData<List<OilTypeViewModel>> resultData = new ResultData<List<OilTypeViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_OIL_TYPE "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getOilTypeName())) {
                sql += " AND OIL_TYPE_NAME LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getOilTypeCode())) {
                sql += " AND OIL_TYPE_CODE LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("oilTypeName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getOilTypeName())) {
                ps.setString(i++, "%" + filter.getOilTypeName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getOilTypeCode())) {
                ps.setString(i++, "%" + filter.getOilTypeCode() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<OilTypeViewModel> dataList = new OilTypeMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<OilTypeViewModel> dataList = new OilTypeMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " OIL_TYPE_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("oilTypeName")) {
                str = " OIL_TYPE_NAME ";
            } else if (orderBy.equalsIgnoreCase("oilTypeCode")) {
                str = " OIL_TYPE_CODE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_OIL_TYPE "
                        + " WHERE OIL_TYPE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<OilTypeViewModel> getData(int id) throws SQLException {

        ResultData<OilTypeViewModel> resultData = new ResultData<OilTypeViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_OIL_TYPE "
                       + " WHERE OIL_TYPE_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            OilTypeViewModel data = new OilTypeMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(OilTypeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_OIL_TYPE "
                        + " WHERE (? = 0 OR OIL_TYPE_ID <> ?) "
                        + " AND (OIL_TYPE_NAME LIKE ?) ";
                        //+ " AND (OIL_TYPE_CODE LIKE ?) "

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getOilTypeId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getOilTypeId()));
            ps.setString(i++, filter.getOilTypeName());
            //ps.setString(i++, filter.getOilTypeCode());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(OilTypeModel oilType) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="OilType">
            if (oilType.getOilTypeId() == 0) {

                sql = "INSERT INTO M_OIL_TYPE ( "
                        + "  OIL_TYPE_NAME "
                        + ", OIL_TYPE_CODE"
                        + ", IS_ACTIVE"
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "OIL_TYPE_ID" });
                
            } else {

                sql = "UPDATE M_OIL_TYPE SET "
                        + "  OIL_TYPE_NAME = ? "
                        + ", OIL_TYPE_CODE = ? "
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE OIL_TYPE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, oilType.getOilTypeName());
            ps.setString(i++, oilType.getOilTypeCode());
            ps.setBoolean(i++, oilType.isActive());

            if (oilType.getOilTypeId() == 0) {

                if (!AppUtil.isNullAndSpace(oilType.getUpdatedBy())) {
                    ps.setString(i++, oilType.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                oilType.setOilTypeId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(oilType.getUpdatedBy())) {
                    ps.setString(i++, oilType.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, oilType.getOilTypeId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, OilTypeFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete OilType by id list">
            sql = " DELETE FROM M_OIL_TYPE "
                + " WHERE OIL_TYPE_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete OilType by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
