/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.NationalityModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.NationalityFilterModel;
import repository.mapper.NationalityMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.NationalityViewModel;

/**
 *
 * @author Sirichai
 */
public class NationalityRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<NationalityViewModel>> getListAll(ResultPage page, NationalityFilterModel filter) throws SQLException {

        ResultData<List<NationalityViewModel>> resultData = new ResultData<List<NationalityViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_NATIONALITY "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getNationalityName())) {
                sql += " AND NATIONALITY_NAME LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("nationalityName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getNationalityName())) {
                ps.setString(i++, "%" + filter.getNationalityName() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<NationalityViewModel> dataList = new NationalityMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<NationalityViewModel> dataList = new NationalityMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " NATIONALITY_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("nationalityName")) {
                str = " NATIONALITY_NAME ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_NATIONALITY "
                        + " WHERE NATIONALITY_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<NationalityViewModel> getData(int id) throws SQLException {

        ResultData<NationalityViewModel> resultData = new ResultData<NationalityViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_NATIONALITY "
                       + " WHERE NATIONALITY_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            NationalityViewModel data = new NationalityMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(NationalityFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_NATIONALITY "
                        + " WHERE (? = 0 OR NATIONALITY_ID <> ?) "
                        + " AND (NATIONALITY_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getNationalityId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getNationalityId()));
            ps.setString(i++, filter.getNationalityName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(NationalityModel title) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Nationality">
            if (title.getNationalityId() == 0) {

                sql = "INSERT INTO M_NATIONALITY ( "
                        + "  NATIONALITY_NAME "
                        + ", IS_ACTIVE"
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "NATIONALITY_ID" });
                
            } else {

                sql = "UPDATE M_NATIONALITY SET "
                        + "  NATIONALITY_NAME = ? "
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE NATIONALITY_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, title.getNationalityName());
            ps.setBoolean(i++, title.isActive());

            if (title.getNationalityId() == 0) {

                if (!AppUtil.isNullAndSpace(title.getUpdatedBy())) {
                    ps.setString(i++, title.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                title.setNationalityId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(title.getUpdatedBy())) {
                    ps.setString(i++, title.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, title.getNationalityId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, NationalityFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete Nationality by id list">
            sql = " DELETE FROM M_NATIONALITY "
                + " WHERE NATIONALITY_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete Nationality by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
