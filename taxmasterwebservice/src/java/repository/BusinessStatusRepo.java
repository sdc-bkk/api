/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.BusinessStatusModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.BusinessStatusFilterModel;
import repository.mapper.BusinessStatusMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.BusinessStatusViewModel;

/**
 *
 * @author Sirichai
 */
public class BusinessStatusRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<BusinessStatusViewModel>> getListAll(ResultPage page, BusinessStatusFilterModel filter) throws SQLException {

        ResultData<List<BusinessStatusViewModel>> resultData = new ResultData<List<BusinessStatusViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = "SELECT * FROM M_BUSINESS_STATUS "
                       + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusName())) {
                sql += " AND BUSINESS_STATUS_NAME LIKE ?";
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusCode())) {
                sql += " AND BUSINESS_STATUS_CODE LIKE ?";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("TEMPLATE_CODE");
                filter.setSort("ASC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusName())) {
                ps.setString(i++, "%" + filter.getBusinessStatusName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getBusinessStatusCode())) {
                ps.setString(i++, "%" + filter.getBusinessStatusCode() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<BusinessStatusViewModel> dataList = new BusinessStatusMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<BusinessStatusViewModel> dataList = new BusinessStatusMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " BUSINESS_STATUS_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("businessStatusName")) {
                str = " BUSINESS_STATUS_NAME ";
            } else if (orderBy.equalsIgnoreCase("businessStatusCode")) {
                str = " BUSINESS_STATUS_CODE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_BUSINESS_STATUS "
                        + " WHERE BUSINESS_STATUS_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<BusinessStatusViewModel> getData(int id) throws SQLException {

        ResultData<BusinessStatusViewModel> resultData = new ResultData<BusinessStatusViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_BUSINESS_STATUS "
                       + " WHERE BUSINESS_STATUS_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            BusinessStatusViewModel data = new BusinessStatusMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(BusinessStatusFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                        + " FROM M_BUSINESS_STATUS "
                        + " WHERE (? = 0 OR BUSINESS_STATUS_ID <> ?) "
                        + " AND (BUSINESS_STATUS_NAME LIKE ?) ";
                        //+ " AND (BUSINESS_STATUS_CODE LIKE ?) "

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getBusinessStatusId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getBusinessStatusId()));
            ps.setString(i++, filter.getBusinessStatusName());
            //ps.setString(i++, filter.getBusinessStatusCode());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(BusinessStatusModel businessStatus) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="BusinessStatus">
            if (businessStatus.getBusinessStatusId() == 0) {

                sql = "INSERT INTO M_BUSINESS_STATUS ( "
                        + "  BUSINESS_STATUS_NAME "
                        + ", BUSINESS_STATUS_CODE"
                        + ", IS_ACTIVE"
                        
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?,?, SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[] { "BUSINESS_STATUS_ID" });
                
            } else {

                sql = "UPDATE M_BUSINESS_STATUS SET "
                        + "  BUSINESS_STATUS_NAME = ? "
                        + ", BUSINESS_STATUS_CODE = ? "
                        + ", IS_ACTIVE = ? "
                        
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE BUSINESS_STATUS_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, businessStatus.getBusinessStatusName());
            ps.setString(i++, businessStatus.getBusinessStatusCode());
            ps.setBoolean(i++, businessStatus.isActive());

            if (businessStatus.getBusinessStatusId() == 0) {

                if (!AppUtil.isNullAndSpace(businessStatus.getUpdatedBy())) {
                    ps.setString(i++, businessStatus.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }
                
                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                businessStatus.setBusinessStatusId(id);
                
            } else {

                if (!AppUtil.isNullAndSpace(businessStatus.getUpdatedBy())) {
                    ps.setString(i++, businessStatus.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, businessStatus.getBusinessStatusId());
                
                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>

            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
            
        }

        return result;
    }

    public boolean deleteData(List<Integer> idList, BusinessStatusFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;
            
            //<editor-fold defaultstate="collapsed" desc="Delete BusinessStatus by id list">
            sql = " DELETE FROM M_BUSINESS_STATUS "
                + " WHERE BUSINESS_STATUS_ID = ? ";

            for (int id : idList) {

                //<editor-fold defaultstate="collapsed" desc="Delete BusinessStatus by id ">
                i = 1;
                ps.setSql(sql);

                ps.setInt(i, id);

                result = ps.executeUpdate();
                //</editor-fold>

            }
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

}
