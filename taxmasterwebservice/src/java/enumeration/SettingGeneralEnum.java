/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sirichai
 */
public enum SettingGeneralEnum {

    //ข้อความของการตั้งค่าทั่วไปของระบบ
    Telephone("Tel", "เบอร์โทรศัพท์", "Telephone."),
    Email("Email", "อีเมล", "Email."),
    FiledDate("FD", "กำหนดวันที่ยื่นแบบทุกวันที่", "Filed date due."),
    FiledNotiStart("FNS", "กำหนดวันที่แจ้งเตือนทุกวันที่", "Filed notifications start."),
    FiledNotiEnd("FNE", "กำหนดวันที่แจ้งเตือนทุกวันที่", "Filed notifications end."),
    PayTaxDate("PD", "กำหนดวันที่ชำระภาษีทุกวันที่", "Pay tax date due."),
    PayTaxNotiStart("PNS", "กำหนดวันที่แจ้งเตือนการชำระภาษีทุกวันที่", "Pay tax notifications start."),
    PayTaxNotiEnd("PNE", "กำหนดวันที่แจ้งเตือนการชำระภาษีทุกวันที่", "Pay tax notifications end.");

    private final String value;
    private final String displayNameTH;
    private final String displayNameEN;

    SettingGeneralEnum(String value, String displayNameTH, String displayNameEN) {
        this.value = value;
        this.displayNameTH = displayNameTH;
        this.displayNameEN = displayNameEN;
    }

    public String value() {
        return value;
    }

    public String displayNameTH() {
        return displayNameTH;
    }

    public String displayNameEN() {
        return displayNameEN;
    }

    public static SettingGeneralEnum valueOfString(String value) throws IllegalArgumentException {
        for (SettingGeneralEnum settingGeneral : SettingGeneralEnum.values()) {
            if (settingGeneral.value.equalsIgnoreCase(value.trim())) {
                return settingGeneral;
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<SettingGeneralEnum> list() {

        List<SettingGeneralEnum> list = new ArrayList<SettingGeneralEnum>();

        return list;
    }

}
