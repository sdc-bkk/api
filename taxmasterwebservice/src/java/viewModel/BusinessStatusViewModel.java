/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import utility.AppConfig;

/**
 *
 * @author Sirichai
 */
public class BusinessStatusViewModel {

    private String businessStatusId;
    private String businessStatusName;
    private String businessStatusCode;
    private String templateCode; 
    
    private Boolean active;
    private String createdBy;
    private String createdDate;
    private String updatedBy;
    private String updatedDate;

    public String getBusinessStatusId() {
        return businessStatusId;
    }

    public void setBusinessStatusId(String businessStatusId) {
        this.businessStatusId = businessStatusId;
    }

    public String getBusinessStatusName() {
        return businessStatusName;
    }

    public void setBusinessStatusName(String businessStatusName) {
        this.businessStatusName = businessStatusName;
    }

    public String getBusinessStatusCode() {
        return businessStatusCode;
    }

    public void setBusinessStatusCode(String businessStatusCode) {
        this.businessStatusCode = businessStatusCode;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
