/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import utility.AppConfig;

/**
 *
 * @author Sirichai
 */
public class TaxRateViewModel {

    private String taxRateId;
    private String taxRateName;
    private Float taxRateValue;
    
    private String startDate;
    private String endDate;
    
    private String yearlyFrom;
    private String yearlyTo;
    
    private Boolean active;
    private String createdBy;
    private String createdDate;
    private String updatedBy;
    private String updatedDate;

    public String getTaxRateId() {
        return taxRateId;
    }

    public void setTaxRateId(String taxRateId) {
        this.taxRateId = taxRateId;
    }

    public String getTaxRateName() {
        return taxRateName;
    }

    public void setTaxRateName(String taxRateName) {
        this.taxRateName = taxRateName;
    }

    public Float getTaxRateValue() {
        return taxRateValue;
    }

    public void setTaxRateValue(Float taxRateValue) {
        this.taxRateValue = taxRateValue;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getYearlyFrom() {
        return yearlyFrom;
    }

    public void setYearlyFrom(String yearlyFrom) {
        this.yearlyFrom = yearlyFrom;
    }

    public String getYearlyTo() {
        return yearlyTo;
    }

    public void setYearlyTo(String yearlyTo) {
        this.yearlyTo = yearlyTo;
    }

}
