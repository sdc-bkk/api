/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import utility.AppConfig;

/**
 *
 * @author Sirichai
 */
public class SettingGeneralViewModel {

    private String telephone;
    private String email;
    
    private String filedDate;
    private String filedNotiStart;
    private String filedNotiEnd;
    private String payTaxDate;
    private String payTaxNotiStart;
    private String payTaxNotiEnd;
    
    private String updatedBy;
    private String updatedDate;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFiledDate() {
        return filedDate;
    }

    public void setFiledDate(String filedDate) {
        this.filedDate = filedDate;
    }

    public String getFiledNotiStart() {
        return filedNotiStart;
    }

    public void setFiledNotiStart(String filedNotiStart) {
        this.filedNotiStart = filedNotiStart;
    }

    public String getFiledNotiEnd() {
        return filedNotiEnd;
    }

    public void setFiledNotiEnd(String filedNotiEnd) {
        this.filedNotiEnd = filedNotiEnd;
    }

    public String getPayTaxDate() {
        return payTaxDate;
    }

    public void setPayTaxDate(String payTaxDate) {
        this.payTaxDate = payTaxDate;
    }

    public String getPayTaxNotiStart() {
        return payTaxNotiStart;
    }

    public void setPayTaxNotiStart(String payTaxNotiStart) {
        this.payTaxNotiStart = payTaxNotiStart;
    }

    public String getPayTaxNotiEnd() {
        return payTaxNotiEnd;
    }

    public void setPayTaxNotiEnd(String payTaxNotiEnd) {
        this.payTaxNotiEnd = payTaxNotiEnd;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
