/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import utility.AppConfig;

/**
 *
 * @author Sirichai
 */
public class OilTypeViewModel {

    private String oilTypeId;
    private String oilTypeName;
    private String oilTypeCode;
    private double oilTypeRate;
    
    private Boolean active;
    private String createdBy;
    private String createdDate;
    private String updatedBy;
    private String updatedDate;

    public String getOilTypeId() {
        return oilTypeId;
    }

    public void setOilTypeId(String oilTypeId) {
        this.oilTypeId = oilTypeId;
    }

    public String getOilTypeName() {
        return oilTypeName;
    }

    public void setOilTypeName(String oilTypeName) {
        this.oilTypeName = oilTypeName;
    }

    public String getOilTypeCode() {
        return oilTypeCode;
    }

    public void setOilTypeCode(String oilTypeCode) {
        this.oilTypeCode = oilTypeCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public double getOilTypeRate() {
        return oilTypeRate;
    }

    public void setOilTypeRate(double oilTypeRate) {
        this.oilTypeRate = oilTypeRate;
    }

}
