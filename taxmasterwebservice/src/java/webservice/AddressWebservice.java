/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.AddressFilterModel;
import service.AddressService;
import viewModel.AddressViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("address")
public class AddressWebservice {

    @Context
    private UriInfo context;

    AddressService addressService = new AddressService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(AddressFilterModel filter) {

        try {

            ResultData<List<AddressViewModel>> data = addressService.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListByPostCodeMain")
    public String getDrpListByPostCodeMain(AddressFilterModel filter) {

        try {

            ResultData<List<AddressViewModel>> data = addressService.getDrpListByPostCodeMain(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    } 
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListProvince")
    public String getDrpListProvince(AddressFilterModel filter) {

        try {

            ResultData<List<AddressViewModel>> data = addressService.getDrpListProvince(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    } 
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListDistrict")
    public String getDrpListDistrict(AddressFilterModel filter) {

        try {

            ResultData<List<AddressViewModel>> data = addressService.getDrpListDistrict(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    } 
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDrpListTambon")
    public String getDrpListTambon(AddressFilterModel filter) {

        try {

            ResultData<List<AddressViewModel>> data = addressService.getDrpListTambon(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    } 
    
    
    @POST
    @Consumes("multipart/form-data")
    @Produces("Application/json;charset=utf8")
    @Path("/importData")
    public String importData(@Context HttpServletRequest request) {

        try {

            ResultData<Boolean> data = addressService.importData(request);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("exportData")
    public String exportData(AddressFilterModel filter) {

        try {

            //ResultData<AddressViewModel> data = addressService.exportData(filter);

            //return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

}
