/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Sirichai
 */
@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(webservice.AddressWebservice.class);
        resources.add(webservice.BusinessStatusWebservice.class);
        resources.add(webservice.BusinessTypeWebservice.class);
        resources.add(webservice.CustomerTypeWebservice.class);
        resources.add(webservice.FeeWebservice.class);
        resources.add(webservice.HolidayWebservice.class);
        resources.add(webservice.NationalityWebservice.class);
        resources.add(webservice.OfficeWebservice.class);
        resources.add(webservice.OilTypeWebservice.class);
        resources.add(webservice.RoadWebservice.class);
        resources.add(webservice.SettingGeneralWebservice.class);
        resources.add(webservice.SoiWebservice.class);
        resources.add(webservice.StationWebservice.class);
        resources.add(webservice.TaxRateWebservice.class);
        resources.add(webservice.TitleWebservice.class);
    }
    
}
