/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import enumeration.SettingGeneralEnum;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.SettingGeneralFilterModel;
import service.SettingGeneralService;
import viewModel.SettingGeneralViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("settingGeneral")
public class SettingGeneralWebservice {

    @Context
    private UriInfo context;

    SettingGeneralService settingGeneralService = new SettingGeneralService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(SettingGeneralFilterModel filter) {

        try {

            //ResultData<SettingGeneralViewModel> data = settingGeneralService.getData(filter);
            ResultData<SettingGeneralViewModel> data = new ResultData<SettingGeneralViewModel>();
            
            SettingGeneralViewModel settingGeneral = new SettingGeneralViewModel();
            
            String filedDate = settingGeneralService.getDataValue(SettingGeneralEnum.FiledDate);
            settingGeneral.setFiledDate(filedDate);
            
            String filedNotiStart = settingGeneralService.getDataValue(SettingGeneralEnum.FiledNotiStart);
            settingGeneral.setFiledNotiStart(filedNotiStart);
            
            String filedNotiEnd = settingGeneralService.getDataValue(SettingGeneralEnum.FiledNotiEnd);
            settingGeneral.setFiledNotiEnd(filedNotiEnd);
            
            String payTaxDate = settingGeneralService.getDataValue(SettingGeneralEnum.PayTaxDate);
            settingGeneral.setPayTaxDate(payTaxDate);
            
            String payTaxNotiStart = settingGeneralService.getDataValue(SettingGeneralEnum.PayTaxNotiStart);
            settingGeneral.setPayTaxNotiStart(payTaxNotiStart);
            
            String payTaxNotiEnd = settingGeneralService.getDataValue(SettingGeneralEnum.PayTaxNotiEnd);
            settingGeneral.setPayTaxNotiEnd(payTaxNotiEnd);
            
            data.setResult(settingGeneral);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(SettingGeneralViewModel settingGeneralView) {

        try {

            ResultData<Boolean> data = new ResultData<Boolean>();
            
            boolean saveFiledDate = settingGeneralService.saveDataValue(SettingGeneralEnum.FiledDate, settingGeneralView);
            boolean saveFiledNotiStart = settingGeneralService.saveDataValue(SettingGeneralEnum.FiledNotiStart, settingGeneralView);
            boolean saveFiledNotiEnd = settingGeneralService.saveDataValue(SettingGeneralEnum.FiledNotiEnd, settingGeneralView);
            boolean savePayTaxDate = settingGeneralService.saveDataValue(SettingGeneralEnum.PayTaxDate, settingGeneralView);
            boolean savePayTaxNotiStart = settingGeneralService.saveDataValue(SettingGeneralEnum.PayTaxNotiStart, settingGeneralView);
            boolean savePayTaxNotiEnd = settingGeneralService.saveDataValue(SettingGeneralEnum.PayTaxNotiEnd, settingGeneralView);
            
            if(saveFiledDate && saveFiledNotiStart && saveFiledNotiEnd 
                    && savePayTaxDate && savePayTaxNotiStart && savePayTaxNotiEnd){
                data.setResult(true);
            } else {
                data.setResult(false);
            }

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataTelephone")
    public String getDataTelephone(SettingGeneralFilterModel filter) {

        try {

            //ResultData<SettingGeneralViewModel> data = settingGeneralService.getDataTelephone(filter);
            ResultData<SettingGeneralViewModel> data = new ResultData<SettingGeneralViewModel>();
            
            SettingGeneralViewModel tel = new SettingGeneralViewModel();
            String isTelephone = settingGeneralService.getDataValue(SettingGeneralEnum.Telephone);
            tel.setTelephone(isTelephone);
            
            data.setResult(tel);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveDataTelephone")
    public String saveDataTelephone(SettingGeneralViewModel settingGeneralView) {

        try {

            ResultData<Boolean> data = new ResultData<Boolean>();
            boolean saveData = settingGeneralService.saveDataValue(SettingGeneralEnum.Telephone, settingGeneralView);
            data.setResult(saveData);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataEmail")
    public String getDataEmail(SettingGeneralFilterModel filter) {

        try {

            //ResultData<SettingGeneralViewModel> data = settingGeneralService.getDataEmail(filter);
            ResultData<SettingGeneralViewModel> data = new ResultData<SettingGeneralViewModel>();
            
            SettingGeneralViewModel email = new SettingGeneralViewModel();
            String isEmail = settingGeneralService.getDataValue(SettingGeneralEnum.Email);
            email.setEmail(isEmail);
            
            data.setResult(email);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveDataEmail")
    public String saveDataEmail(SettingGeneralViewModel settingGeneralView) {

        try {

            ResultData<Boolean> data = new ResultData<Boolean>();
            boolean saveData = settingGeneralService.saveDataValue(SettingGeneralEnum.Email, settingGeneralView);
            data.setResult(saveData);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

}
