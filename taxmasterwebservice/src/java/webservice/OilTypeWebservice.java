/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.OilTypeFilterModel;
import service.OilTypeService;
import viewModel.OilTypeViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("oilType")
public class OilTypeWebservice {

    @Context
    private UriInfo context;

    OilTypeService oilTypeService = new OilTypeService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(OilTypeFilterModel filter) {

        try {

            ResultData<List<OilTypeViewModel>> data = oilTypeService.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(OilTypeFilterModel filter) {

        try {

            ResultData<OilTypeViewModel> data = oilTypeService.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(OilTypeViewModel activityView) {

        try {

            ResultData<Boolean> data = oilTypeService.saveData(activityView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(OilTypeFilterModel filter) {

        try {

            return new Gson().toJson(oilTypeService.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpList")
    public String getDrpList() {

        try {

            ResultData<List<OilTypeViewModel>> data = oilTypeService.getDrpList();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
}
