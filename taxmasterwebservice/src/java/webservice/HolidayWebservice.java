/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.data.ResultData;
import model.filter.HolidayFilterModel;
import service.HolidayService;
import viewModel.HolidayViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("holiday")
public class HolidayWebservice {

 
    @Context
    private UriInfo context;

    HolidayService service = new HolidayService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(HolidayFilterModel filter) {

        try {

            ResultData<List<HolidayViewModel>> data = service.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(HolidayFilterModel filter) {

        try {

            ResultData<HolidayViewModel> data = service.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(HolidayViewModel feeView) {

        try {

            ResultData<Boolean> data = service.saveData(feeView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(HolidayFilterModel filter) {

        try {

            return new Gson().toJson(service.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpList")
    public String getDrpList() {

        try {

            ResultData<List<HolidayViewModel>> data = service.getDrpList();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
}
