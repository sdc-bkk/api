/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.CustomerTypeFilterModel;
import service.CustomerTypeService;
import viewModel.CustomerTypeViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("customerType")
public class CustomerTypeWebservice {

    @Context
    private UriInfo context;

    CustomerTypeService customerTypeService = new CustomerTypeService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(CustomerTypeFilterModel filter) {

        try {

            ResultData<List<CustomerTypeViewModel>> data = customerTypeService.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(CustomerTypeFilterModel filter) {

        try {

            ResultData<CustomerTypeViewModel> data = customerTypeService.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(CustomerTypeViewModel activityView) {

        try {

            ResultData<Boolean> data = customerTypeService.saveData(activityView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(CustomerTypeFilterModel filter) {

        try {

            return new Gson().toJson(customerTypeService.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpList")
    public String getDrpList() {

        try {

            ResultData<List<CustomerTypeViewModel>> data = customerTypeService.getDrpList();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
}
