/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.TaxRateFilterModel;
import service.TaxRateService;
import viewModel.TaxRateViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("taxRate")
public class TaxRateWebservice {

    @Context
    private UriInfo context;

    TaxRateService taxRateService = new TaxRateService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(TaxRateFilterModel filter) {

        try {

            ResultData<List<TaxRateViewModel>> data = taxRateService.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(TaxRateFilterModel filter) {

        try {

            ResultData<TaxRateViewModel> data = taxRateService.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(TaxRateViewModel stationView) {

        try {

            ResultData<Boolean> data = taxRateService.saveData(stationView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("deleteData")
    public String deleteData(TaxRateFilterModel filter) {

        try {

            return new Gson().toJson(taxRateService.deleteData(filter));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("getDrpList")
    public String getDrpList() {

        try {

            ResultData<List<TaxRateViewModel>> data = taxRateService.getDrpList();

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
}
