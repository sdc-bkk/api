/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.PNGEncodeParam;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.TIFFDecodeParam;
import com.sun.media.jai.codecimpl.PNGImageEncoder;
import enumeration.SystemEnum;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.imageio.ImageIO;

import org.apache.commons.fileupload.FileItem;
import utility.AppUtil;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import model.DocumentFileModel;
import org.apache.commons.codec.binary.Base64;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import utility.AppConfig;
import utility.FilePath;

/**
 *
 * @author Prapaporn
 */
public class UploadService {

    public List<String> uploadFile(List<FileItem> items, String filePath, Integer width) throws IOException {

        List<String> dataList = new ArrayList<String>();
        InputStream filecontent = null;
        FileOutputStream fos = null;
        byte[] file;

        try {

            for (FileItem item : items) {

                if (!item.isFormField()) {
                    String uuid = UUID.randomUUID().toString();
                    String typefile = AppUtil.getFileType(item.getName());
                    String newFilename = uuid + typefile;

                    if ((typefile.equalsIgnoreCase(".tif") || typefile.equalsIgnoreCase(".tiff"))) {
                        newFilename = uuid + ".png";
                    }

                    dataList.add(newFilename);

                    File fileDir = new File(filePath);
                    if (!fileDir.exists()) {
                        fileDir.mkdirs();
                    }

                    filecontent = item.getInputStream();
                    file = AppUtil.InputStreamToByteArray(filecontent);

                    File tempFile = new File(filePath + "/" + newFilename);

                    if (width != null && (typefile.equalsIgnoreCase(".jpg") || typefile.equalsIgnoreCase(".jpeg") || typefile.equalsIgnoreCase(".png") || typefile.equalsIgnoreCase(".gif"))) {
                        BufferedImage bfimg = ImageIO.read(item.getInputStream());

                        int widthImg = width;
                        BufferedImage newResize;
                        if (bfimg.getWidth() > widthImg) {
                            newResize = Scalr.resize(bfimg, Method.SPEED, widthImg);//, OP_ANTIALIAS
                        } else {
                            newResize = bfimg;
                        }

                        ImageIO.write(newResize, typefile.replace(".", ""), tempFile);

                    } else if ((typefile.equalsIgnoreCase(".tif") || typefile.equalsIgnoreCase(".tiff"))) {

                        tempFile = new File(filePath + "/" + newFilename);

                        SeekableStream s = SeekableStream.wrapInputStream(item.getInputStream(), true);
                        TIFFDecodeParam param = null;
                        ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, param);
                        RenderedImage op = dec.decodeAsRenderedImage(0);

                        fos = new FileOutputStream(tempFile);
                        PNGEncodeParam pngParam = PNGEncodeParam
                                .getDefaultEncodeParam(op);
                        PNGImageEncoder pngEncoder = new PNGImageEncoder(fos,
                                pngParam);
                        pngEncoder.encode(op);
                        fos.flush();

                    } else {

                        tempFile.createNewFile();
                        fos = new FileOutputStream(tempFile);
                        fos.write(file);
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (filecontent != null) {
                filecontent.close();
            }

            if (fos != null) {
                fos.close();
            }
        }

        return dataList;
    }

    public Response getFile(String filePath, String fileName) {

        try {

            AppConfig appConfig = new AppConfig();
            String typeFile = AppUtil.getFileType(fileName);
            typeFile = typeFile.replace(".", "");
            
            File tempFile = null;

            if (!filePath.equals("")) {
                tempFile = new File(filePath);
            }

            ResponseBuilder response = Response.ok((Object) tempFile);
            response.header("Content-Length", tempFile.length());

            if (typeFile.equalsIgnoreCase("jpg") || typeFile.equalsIgnoreCase("jpeg")) {
                response.type(appConfig.fileTypeValue("contentType.jpeg"));
            } else if (typeFile.equalsIgnoreCase("gif")) {
                response.type(appConfig.fileTypeValue("contentType.gif"));
            } else if (typeFile.equalsIgnoreCase("png")) {
                response.type(appConfig.fileTypeValue("contentType.png"));
            } else if (typeFile.equalsIgnoreCase("pdf")) {
                response.type(appConfig.fileTypeValue("contentType.pdf"));
            } else if (typeFile.equalsIgnoreCase("doc")) {
                response.type(appConfig.fileTypeValue("contentType.doc"));
            } else if (typeFile.equalsIgnoreCase("docx")) {
                response.type(appConfig.fileTypeValue("contentType.docx"));
            } else if (typeFile.equalsIgnoreCase("xls")) {
                response.type(appConfig.fileTypeValue("contentType.xls"));
            } else if (typeFile.equalsIgnoreCase("xlsx")) {
                response.type(appConfig.fileTypeValue("contentType.xlsx"));
            } else if (typeFile.equalsIgnoreCase("ppt")) {
                response.type(appConfig.fileTypeValue("contentType.ppt"));
            } else if (typeFile.equalsIgnoreCase("pptx")) {
                response.type(appConfig.fileTypeValue("contentType.pptx"));
            } else if (typeFile.equalsIgnoreCase("zip")) {
                response.type(appConfig.fileTypeValue("contentType.zip"));
            } else if (typeFile.equalsIgnoreCase("rar")) {
                response.type(appConfig.fileTypeValue("contentType.rar"));
            } else if (typeFile.equalsIgnoreCase("epub")) {
                response.type(appConfig.fileTypeValue("contentType.epub"));
            } else if (typeFile.equalsIgnoreCase("mp4")) {
                response.type(appConfig.fileTypeValue("contentType.mp4"));
            } else if (typeFile.equalsIgnoreCase("mp3")) {
                response.type(appConfig.fileTypeValue("contentType.mp3"));
            } else {
                response.type(appConfig.fileTypeValue("contentType.txt"));
            }

            if (!typeFile.equalsIgnoreCase("pdf") && !typeFile.equalsIgnoreCase("gif")
                    && !typeFile.equalsIgnoreCase("jpg") && !typeFile.equalsIgnoreCase("jpeg")
                    && !typeFile.equalsIgnoreCase("png") && !typeFile.equalsIgnoreCase("mp4")
                    && !typeFile.equalsIgnoreCase("mp3")) {
                String header = "attachment; filename=\"" + fileName + "\"";
                header = new String(header.getBytes("UTF8"), "ISO8859-1");
                response.header("Content-Disposition", header);

            }

            return response.build();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
    
    public File getDestinationFile(String filePath, String fileName, SystemEnum sysEnum) {

        try {

            int sysEnumId = sysEnum.value();

            switch (sysEnumId) {
                case 1:
                    filePath = FilePath.ebook(fileName);
                    break;
                case 2:
                    filePath = FilePath.floor(fileName);
                    break;
                case 3:
                    filePath = FilePath.room(fileName);
                    break;
                case 4:
                    filePath = FilePath.resource(fileName);
                    break;
                case 5:
                    filePath = FilePath.ebookLink(fileName);
                    break;
                case 6:
                    filePath = FilePath.profile(fileName);
                    break;
                case 7:
                    filePath = FilePath.institution(fileName);
                    break;
                case 8:
                    filePath = FilePath.map(fileName);
                    break;
                case 9:
                    filePath = FilePath.building(fileName);
                    break;
                case 10:
                    filePath = FilePath.buildingFloor(fileName);
                    break;
                case 11:
                    filePath = FilePath.buildingZone(fileName);
                    break;
                case 12:
                    filePath = FilePath.themeFile(fileName);
                    break;
                case 13:
                    filePath = FilePath.content(fileName);
                    break;
                case 14:
                    filePath = FilePath.user(fileName);
                    break;
                case 15:
                    filePath = FilePath.station(fileName);
                    break;
                default:
                    filePath = FilePath.temp(fileName);
                    break;
            }

            File tempFile = null;

            if (!filePath.equals("")) {

                tempFile = new File(filePath);

            }

            return tempFile;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public Response getFile(String filePath, String fileName, SystemEnum sysEnum) {

        try {

            AppConfig appConfig = new AppConfig();
            String typeFile = AppUtil.getFileType(fileName);
            typeFile = typeFile.replace(".", "");

            int sysEnumId = sysEnum.value();

            switch (sysEnumId) {
                case 1:
                    filePath = FilePath.ebook(fileName);
                    break;
                case 2:
                    filePath = FilePath.floor(fileName);
                    break;
                case 3:
                    filePath = FilePath.room(fileName);
                    break;
                case 4:
                    filePath = FilePath.resource(fileName);
                    break;
                case 5:
                    filePath = FilePath.ebookLink(fileName);
                    break;
                case 6:
                    filePath = FilePath.profile(fileName);
                    break;
                case 7:
                    filePath = FilePath.institution(fileName);
                    break;
                case 8:
                    filePath = FilePath.map(fileName);
                    break;
                case 9:
                    filePath = FilePath.building(fileName);
                    break;
                case 10:
                    filePath = FilePath.buildingFloor(fileName);
                    break;
                case 11:
                    filePath = FilePath.buildingZone(fileName);
                    break;
                case 12:
                    filePath = FilePath.themeFile(fileName);
                    break;
                case 13:
                    filePath = FilePath.content(fileName);
                    break;
                case 14:
                    filePath = FilePath.user(fileName);
                    break;
                case 15:
                    filePath = FilePath.station(fileName);
                    break;
                case 16:
                    filePath = FilePath.coverpage(fileName);
                    break;
                default:
                    filePath = FilePath.temp(fileName);
                    break;
            }

            File tempFile = null;

            if (!filePath.equals("")) {

                tempFile = new File(filePath);

            }

            ResponseBuilder response = Response.ok((Object) tempFile);
            response.header("Content-Length", tempFile.length());

            if (typeFile.equalsIgnoreCase("jpg") || typeFile.equalsIgnoreCase("jpeg")) {
                response.type(appConfig.fileTypeValue("contentType.jpeg"));
            } else if (typeFile.equalsIgnoreCase("gif")) {
                response.type(appConfig.fileTypeValue("contentType.gif"));
            } else if (typeFile.equalsIgnoreCase("png")) {
                response.type(appConfig.fileTypeValue("contentType.png"));
            } else if (typeFile.equalsIgnoreCase("pdf")) {
                response.type(appConfig.fileTypeValue("contentType.pdf"));
            } else if (typeFile.equalsIgnoreCase("doc")) {
                response.type(appConfig.fileTypeValue("contentType.doc"));
            } else if (typeFile.equalsIgnoreCase("docx")) {
                response.type(appConfig.fileTypeValue("contentType.docx"));
            } else if (typeFile.equalsIgnoreCase("xls")) {
                response.type(appConfig.fileTypeValue("contentType.xls"));
            } else if (typeFile.equalsIgnoreCase("xlsx")) {
                response.type(appConfig.fileTypeValue("contentType.xlsx"));
            } else if (typeFile.equalsIgnoreCase("ppt")) {
                response.type(appConfig.fileTypeValue("contentType.ppt"));
            } else if (typeFile.equalsIgnoreCase("pptx")) {
                response.type(appConfig.fileTypeValue("contentType.pptx"));
            } else if (typeFile.equalsIgnoreCase("zip")) {
                response.type(appConfig.fileTypeValue("contentType.zip"));
            } else if (typeFile.equalsIgnoreCase("rar")) {
                response.type(appConfig.fileTypeValue("contentType.rar"));
            } else if (typeFile.equalsIgnoreCase("epub")) {
                response.type(appConfig.fileTypeValue("contentType.epub"));
            } else if (typeFile.equalsIgnoreCase("mp4")) {
                response.type(appConfig.fileTypeValue("contentType.mp4"));
            } else if (typeFile.equalsIgnoreCase("mp3")) {
                response.type(appConfig.fileTypeValue("contentType.mp3"));
            } else {
                response.type(appConfig.fileTypeValue("contentType.txt"));
            }

            if (!typeFile.equalsIgnoreCase("pdf") && !typeFile.equalsIgnoreCase("gif")
                    && !typeFile.equalsIgnoreCase("jpg") && !typeFile.equalsIgnoreCase("jpeg")
                    && !typeFile.equalsIgnoreCase("png") && !typeFile.equalsIgnoreCase("mp4")
                    && !typeFile.equalsIgnoreCase("mp3")) {
                String header = "attachment; filename=\"" + fileName + "\"";
                header = new String(header.getBytes("UTF8"), "ISO8859-1");
                response.header("Content-Disposition", header);

            }

            return response.build();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public List<String> uploadFilePDF(List<FileItem> items, String filePath, Integer width) throws IOException {

        List<String> dataList = new ArrayList<String>();
        InputStream filecontent = null;
        FileOutputStream fos = null;
        byte[] file;

        try {

            for (FileItem item : items) {

                if (!item.isFormField()) {
                    String uuid = UUID.randomUUID().toString();
                    String typefile = AppUtil.getFileType(item.getName());
                    String newFilename = uuid + typefile;

                    dataList.add(newFilename);

                    File fileDir = new File(filePath);
                    if (!fileDir.exists()) {
                        fileDir.mkdirs();
                    }

                    filecontent = item.getInputStream();
                    file = AppUtil.InputStreamToByteArray(filecontent);

                    try (PDDocument document = PDDocument.load(file)) {

                        PDFRenderer pdfRenderer = new PDFRenderer(document);
                        for (int page = 0; page < document.getNumberOfPages(); ++page) {
                            String name = uuid + "_" + (page + 1) + ".png";
                            BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 240, ImageType.RGB);
                            boolean convert = ImageIOUtil.writeImage(bim, filePath + "/" + name, 240);
                            if (convert) {
                                dataList.add(name);
                            }

                        }
                        document.close();
                    }

                    File tempFile = new File(filePath + "/" + newFilename);

                    tempFile.createNewFile();
                    fos = new FileOutputStream(tempFile);
                    fos.write(file);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (filecontent != null) {
                filecontent.close();
            }

            if (fos != null) {
                fos.close();
            }
        }

        return dataList;
    }

    public void moveFile(String oldFilePath, String newDirPath) {

        try {
            File oldFile = new File(oldFilePath);

            if (oldFile.exists()) {
                File fileDir = new File(newDirPath);
                if (!fileDir.exists()) {
                    fileDir.mkdirs();
                }
                Path movefrom = FileSystems.getDefault().getPath(oldFilePath);
                Path newPath = FileSystems.getDefault().getPath(newDirPath);
                Files.move(movefrom, newPath, StandardCopyOption.REPLACE_EXISTING);
//                System.out.println("newPath : " + newPath.toString());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void delete(String filePath) {
        try {
            File file = new File(filePath);

            if (file.exists()) {
                file.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String uploadFile(DocumentFileModel documentFile, String filePath, Integer width) {

        AppConfig appConfig = new AppConfig();
        BufferedImage image = null;
        byte[] file;
        String filename = "";

        try {

            if (documentFile != null) {

                if (!documentFile.getEncodeFile().equals("")) {

                    String typeFile = documentFile.getTypeFile();
                    
                    // create new filename
                    String uuid = UUID.randomUUID().toString();
                    filename = uuid + "." + typeFile;

                    file = AppUtil.decodeBase64(documentFile.getEncodeFile());

                    ByteArrayInputStream bis = new ByteArrayInputStream(file);
                    image = ImageIO.read(bis);
                    bis.close();
                    File outputfile = new File(filePath + "/" + filename);
                    ImageIO.write(image, typeFile, outputfile);
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return filename;
    }

}
