/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Prapaporn
 */
public class DocumentFileModel {
    
    private String filename;
    private String typeFile;
    private String phyficalFilename;
    private String encodeFile;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(String typeFile) {
        this.typeFile = typeFile;
    }

    public String getPhyficalFilename() {
        return phyficalFilename;
    }

    public void setPhyficalFilename(String phyficalFilename) {
        this.phyficalFilename = phyficalFilename;
    }

    public String getEncodeFile() {
        return encodeFile;
    }

    public void setEncodeFile(String encodeFile) {
        this.encodeFile = encodeFile;
    }

}
