/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.data.ResultData;
import model.filter.MemberFilterModel;
import service.MemberService;
import viewModel.MemberViewModel;

/**
 * REST Web Service
 *
 * @author Sirichai
 */
@Path("member")
public class MemberWebservice {

    @Context
    private UriInfo context;

    MemberService memberService = new MemberService();

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getList")
    public String getList(MemberFilterModel filter) {

        try {

            ResultData<List<MemberViewModel>> data = memberService.getListAll(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getData")
    public String getData(MemberFilterModel filter) {

        try {

            ResultData<MemberViewModel> data = memberService.getData(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("saveData")
    public String saveData(MemberViewModel usersView) {

        try {

            ResultData<Boolean> data = memberService.saveData(usersView);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }


    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("register")
    public String register(MemberFilterModel filter) {

        try {

            ResultData<Boolean> data = memberService.register(filter);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("forgetPassword")
    public String forgetPassword(MemberFilterModel filter) {

        try {

            ResultData<Boolean> data = memberService.forgetPassword(filter);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("setPassword")
    public String setPassword(MemberFilterModel filter) {

        try {

            ResultData<Boolean> data = memberService.setPassword(filter);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("changePassword")
    public String changePassword(MemberFilterModel filter) {

        try {

            ResultData<Boolean> data = memberService.changePassword(filter);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("authenMember")
    public String authenMember(MemberFilterModel filter) {

        try {

            ResultData<Boolean> data = memberService.authenMember(filter);

            return new Gson().toJson(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
        
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("getDataByToken")
    public String getDataByToken(MemberFilterModel filter) {

        try {

            ResultData<MemberViewModel> data = memberService.getDataByToken(filter);

            return new Gson().toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
