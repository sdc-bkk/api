/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;

/**
 *
 * @author User
 */
public class MemberRetailViewModel {
    
    private String ownerName;
    private String taxNo;
    private String customerType;
    private List<MemberViewModel> memberList; //รายชื่อผู่ที่มีสิทธิ์ดูแล
    
    private String createdBy;
    private String createdDate;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public List<MemberViewModel> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<MemberViewModel> memberList) {
        this.memberList = memberList;
    }   

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    
}
