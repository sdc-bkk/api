/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.data;

/**
 *
 * @author Prapaporn
 */
public class ResultData<T> {
    
    private T result;
    private ResultPage resultPage;
    private String status;
    private String message;
    private String token;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public ResultPage getResultPage() {
        return resultPage;
    }

    public void setResultPage(ResultPage resultPage) {
        this.resultPage = resultPage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
