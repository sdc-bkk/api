/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.MemberModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.MemberFilterModel;
import repository.AppConfigRepo;
import repository.MemberBMARepo;
import repository.MemberRepo;
import repository.mapper.MemberMapper;
import utility.AES;
import utility.AES256;
import utility.AppConfig;
import utility.AppUtil;
import utility.MailService;
import utility.MessageBundleUtil;
import viewModel.MemberViewModel;

/**
 *
 * @author Sirichai
 */
public class MemberService {

    MemberRepo memberRepo = new MemberRepo();
    MemberBMARepo memberBmaRepo = new MemberBMARepo();

    public ResultData<List<MemberViewModel>> getListAll(MemberFilterModel filter) {

        ResultData<List<MemberViewModel>> data = new ResultData<List<MemberViewModel>>();
        ResultPage resultPage = null;

        try {

            if (filter.getPage() != 0) {
                resultPage = new ResultPage();
                resultPage.setPageNo(filter.getPage());
                resultPage.setItemPerPage(filter.getItemPerPage());
            }

            data = memberRepo.getListAll(resultPage, filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<MemberViewModel> getData(MemberFilterModel filter) {

        ResultData<MemberViewModel> data = new ResultData<MemberViewModel>();

        try {

            int id = AppUtil.decryptId(filter.getMemberId());
            if (memberRepo.hasData(id)) {

                data = memberRepo.getData(id);

            } else {

                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.member.hasData"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<MemberViewModel> getDataByToken(MemberFilterModel filter) {

        ResultData<MemberViewModel> data = new ResultData<MemberViewModel>();

        try {
            AppConfig appConfig = new AppConfig();
            String secretKey = appConfig.value("secretKey");
            String encryptedString = AES256.encrypt(secretKey, filter.getUserName().trim());

            if (encryptedString.equals(filter.getToken())) {
                data = memberRepo.getDataAndTaxNoByUsername(filter.getUserName().trim());
            } else {
                data.setResult(null);
                MessageBundleUtil message = new MessageBundleUtil();
                data.setMessage(message.getMessage("message.member.noData"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ResultData<Boolean> saveData(MemberViewModel memberView) {

        ResultData<Boolean> resultData = new ResultData<Boolean>();
        MemberModel member = new MemberModel();

        try {

            //<editor-fold defaultstate="collapsed" desc="Duplicate Data">
            MemberFilterModel filter = new MemberFilterModel();
            filter.setMemberId(memberView.getMemberId());
            filter.setUserName(memberView.getUserName());

            boolean resultDuplicate = memberRepo.duplicate(filter);
            //</editor-fold>

            if (!resultDuplicate) {
                MemberMapper mapper = new MemberMapper();
                member = mapper.mapFull(memberView);
                boolean resultSave = memberRepo.save(member);
                resultData.setResult(resultSave);
            } else {
                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.duplicate"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> register(MemberFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            boolean resultDuplicate = memberRepo.duplicateUserName(userName.trim());

            if (!resultDuplicate) {
                //ยังไม่มี user ในระบบ
                boolean resultRegister = memberRepo.register(userName.trim());

                resultData.setResult(resultRegister);
                if (resultRegister) {
                    AppConfig appConfig = new AppConfig();
                    String urlPath = appConfig.value("register.link");
                    String urlPathFront = appConfig.value("front.link");

                    //send Email
                    MailService mail = new MailService();
                    mail.setTo(userName.trim());
                    mail.setSubject("ยืนยันตัวตนเข้าใช้งานระบบชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ");
                    String temp = "เรียน คุณ  " + userName.trim() + "<br><br>"
                            + "กรุณา "
                            + "<a href=\"" + urlPath + "#/sign-in?userName=" + userName.trim() + "\">คลิกที่นี่</a> "
                            + " เพื่อยืนยันการเข้าใช้งานระบบชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ "
                            + " <br>เมื่อคุณยืนยันตัวตนเรียบร้อยแล้ว คุณจะสามารถเข้าใช้งานระบบชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ ได้ "
                            + "<a href=\"" + urlPathFront + "\">ที่นี่</a> "
                            + "<br><br>" + "ขอบคุณค่ะ";
                    mail.setMessageText(temp);
                    boolean resultSendMail = mail.sendMail();
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.member.register"));

                    //ตรวจสอบว่าต้องเชื่อมข้อมูลไปที่ bma หรือไม่
                    AppConfigRepo appConfigRepo = new AppConfigRepo();
                    String linkMember = appConfigRepo.GetValue("linkmemberbma");

                    //ถ้ากำหนดไว้ว่าต้องเชื่อม BMA ด้วย ก็ให้ทำการสร้าง member ที่ BMA ด้วย
                    if ("true".equals(linkMember)) {
                        MemberFilterModel filterBma = new MemberFilterModel();
                        filterBma.setUserName(userName.trim());

                        boolean resultDupBMA = memberBmaRepo.duplicate(filterBma);
                        if (!resultDupBMA) {
                            boolean resultSave = memberBmaRepo.register(userName.trim());
                        }
                    }
                }

            } else {
                //มี user อยู่ในระบบแล้ว
                resultData.setResult(!resultDuplicate);

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.duplicateUserName"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> forgetPassword(MemberFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {

                resultData.setResult(true);
                AppConfig appConfig = new AppConfig();
                String urlPath = appConfig.value("register.link");
                //send Email
                MailService mail = new MailService();
                mail.setTo(userName.trim());
                mail.setSubject("เปลี่ยนรหัสผ่านเข้าใช้งานระบบชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ");
                String temp = "เรียน คุณ " + userName.trim() + "<br><br>"
                        + "กรุณา "
                        + "<a href=\"" + urlPath + "#/sign-in?userName=" + userName.trim() + "\">คลิกที่นี่</a>"
                        + " เพื่อกำหนดรหัสผ่านใหม่ สำหรับเข้าใช้งานระบบชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ "
                        + "<br><br>" + "ขอบคุณค่ะ";
                mail.setMessageText(temp);
                boolean resultSendMail = mail.sendMail();

                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.register"));
            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> setPassword(MemberFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();
        try {

            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {
                String password = filter.getPassword();
                boolean resultSave = memberRepo.setPassword(userName.trim(), password.trim());

                //ตรวจสอบว่าต้องเชื่อมข้อมูลไปที่ bma หรือไม่
                AppConfigRepo appConfigRepo = new AppConfigRepo();
                String linkMember = appConfigRepo.GetValue("linkmemberbma");
                //ถ้ากำหนดไว้ว่าต้องเชื่อม BMA ด้วย ก็ให้ทำการสร้าง member ที่ BMA ด้วย
                if ("true".equals(linkMember)) {
                    resultSave = memberBmaRepo.setPassword(userName.trim(), password.trim());
                }
                resultData.setResult(resultSave);

                AppConfig appConfig = new AppConfig();
                String urlLogin = appConfig.value("front.link");
                resultData.setMessage(urlLogin);
            } else {
                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public ResultData<Boolean> changePassword(MemberFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();
        try {
            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {
                String password = filter.getPassword();
                if (memberRepo.hasDataByUserNamePassword(userName.trim(), password.trim())) {
                    String newPassword = filter.getNewPassword();
                    boolean resultSave = memberRepo.setPassword(userName.trim(), newPassword.trim());
                    resultData.setResult(resultSave);
                } else {
                    resultData.setResult(false);
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.member.hasDataUserNamePassword"));
                }
            } else {
                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultData<Boolean> authenMember(MemberFilterModel filter) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();
        try {
            String userName = filter.getUserName();
            if (memberRepo.hasDataByUserName(userName.trim())) {

                String password = filter.getPassword();
                if (memberRepo.hasDataByUserNamePassword(userName.trim(), password.trim())) {
                    AppConfig appConfig = new AppConfig();
                    String secretKey = appConfig.value("secretKey");
                    String encryptedString = AES256.encrypt(secretKey, userName.trim());

                    resultData.setResult(true);
                    resultData.setToken(encryptedString);

                } else {

                    resultData.setResult(false);
                    MessageBundleUtil message = new MessageBundleUtil();
                    resultData.setMessage(message.getMessage("message.member.authenNoSuccess"));

                }

            } else {

                resultData.setResult(false);
                MessageBundleUtil message = new MessageBundleUtil();
                resultData.setMessage(message.getMessage("message.member.hasDataUserName"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }
}
