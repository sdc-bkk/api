/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.MemberModel;
import model.data.ResultData;
import model.data.ResultPage;
import model.filter.MemberFilterModel;
import repository.mapper.MemberMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.MemberViewModel;

/**
 *
 * @author Sirichai
 */
public class MemberRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<List<MemberViewModel>> getListAll(ResultPage page, MemberFilterModel filter) throws SQLException {

        ResultData<List<MemberViewModel>> resultData = new ResultData<List<MemberViewModel>>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT * "
                    + " FROM MEMBER "
                    + " WHERE (? IS NULL OR IS_ACTIVE = ?) ";

            if (!AppUtil.isNullAndSpace(filter.getFullName())) {
                sql += " AND FULL_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getUserName())) {
                sql += " AND USER_NAME LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getIdcard())) {
                sql += " AND IDCARD LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                sql += " AND MOBILE LIKE ? ";
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                sql += " AND EMAIL LIKE ? ";
            }

            ps.setSql(sql);

            String orderBy = "";
            if (filter.getOrderBy() != null) {
                orderBy = getColumnDB(filter.getOrderBy());
            } else {
                orderBy = getColumnDB("fullName");
                filter.setSort("DESC");
            }
            String sort = filter.getSort() == null ? "ASC" : filter.getSort().toUpperCase();
            ps.setOrderBy(orderBy + " " + sort);

            if (page != null) {
                ps.setResultPage(page);
            }

            if (!AppUtil.isNull(filter.getActive())) {
                ps.setBoolean(i++, filter.getActive());
                ps.setBoolean(i++, filter.getActive());
            } else {
                ps.setNull(i++);
                ps.setNull(i++);
            }

            if (!AppUtil.isNullAndSpace(filter.getFullName())) {
                ps.setString(i++, "%" + filter.getFullName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getUserName())) {
                ps.setString(i++, "%" + filter.getUserName() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getIdcard())) {
                ps.setString(i++, "%" + filter.getIdcard() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getMobile())) {
                ps.setString(i++, "%" + filter.getMobile() + "%");
            }

            if (!AppUtil.isNullAndSpace(filter.getEmail())) {
                ps.setString(i++, "%" + filter.getEmail() + "%");
            }

            rs = ps.executeQuery();

            if (filter.getDropdown() != null && filter.getDropdown() == true) {
                List<MemberViewModel> dataList = new MemberMapper(rs).mapFullDrpList();
                resultData.setResult(dataList);
            } else {
                List<MemberViewModel> dataList = new MemberMapper(rs).mapFullList();
                resultData.setResult(dataList);
            }

            if (page != null) {
                resultData.setResultPage(ps.getResultPage());
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    private String getColumnDB(String orderBy) {

        String str = " MEMBER_ID ";

        if (orderBy != null) {

            if (orderBy.equalsIgnoreCase("userName")) {
                str = " USER_NAME ";
            } else if (orderBy.equalsIgnoreCase("fullName")) {
                str = " FULL_NAME ";
            } else if (orderBy.equalsIgnoreCase("idcard")) {
                str = " IDCARD ";
            } else if (orderBy.equalsIgnoreCase("mobile")) {
                str = " MOBILE ";
            } else if (orderBy.equalsIgnoreCase("email")) {
                str = " EMAIL ";
            } else if (orderBy.equalsIgnoreCase("activeDate")) {
                str = " ACTIVE_DATE ";
            } else if (orderBy.equalsIgnoreCase("updatedDate")) {
                str = " (CASE WHEN UPDATED_DATE IS NULL THEN CREATED_DATE ELSE UPDATED_DATE END) ";
            }

        }

        return str;

    }

    public boolean hasData(int id) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM MEMBER "
                    + " WHERE MEMBER_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public ResultData<MemberViewModel> getData(int id) throws SQLException {

        ResultData<MemberViewModel> resultData = new ResultData<MemberViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * "
                    + " FROM MEMBER "
                    + " WHERE MEMBER_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            MemberViewModel data = new MemberMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<MemberViewModel> getDataByUsername(String username) throws SQLException {

        ResultData<MemberViewModel> resultData = new ResultData<MemberViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * "
                    + " FROM MEMBER "
                    + " WHERE USER_NAME = ? ";

            ps.setSql(sql);

            ps.setString(1, username);

            rs = ps.executeQuery();

            MemberViewModel data = new MemberMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<MemberViewModel> getDataAndTaxNoByUsername(String username) throws SQLException {

        ResultData<MemberViewModel> resultData = new ResultData<MemberViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT distinct MEMBER.*, r.tax_no  FROM MEMBER  \n"
                    + "LEFT JOIN MEMBER_OF_RETAIL r ON MEMBER.USER_NAME = r.email and r.is_active=1\n"
                    + "WHERE USER_NAME = ? ";

            ps.setSql(sql);

            ps.setString(1, username);

            rs = ps.executeQuery();

            MemberViewModel data = new MemberMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean duplicate(MemberFilterModel filter) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM MEMBER "
                    + " WHERE (? = 0 OR MEMBER_ID <> ?) "
                    + " AND (USER_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setInt(i++, AppUtil.decryptId(filter.getMemberId()));
            ps.setInt(i++, AppUtil.decryptId(filter.getMemberId()));
            ps.setString(i++, filter.getUserName());

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean save(MemberModel member) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Member">
            if (member.getMemberId() == 0) {

                sql = "INSERT INTO MEMBER ( "
                        + "  USER_NAME "
                        + ", FULL_NAME "
                        + ", IDCARD "
                        + ", MOBILE "
                        + ", EMAIL "
                        + ", IS_ACTIVE "
                        + ", CREATED_DATE "
                        + ", CREATED_BY"
                        + ") VALUES(?,?, ?,?,?, ?,SYSDATE,?) ";

                //ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps = conn.prepareStatement(sql, new String[]{"MEMBER_ID"});

            } else {

                sql = "UPDATE MEMBER SET "
                        + "  USER_NAME = ? "
                        + ", FULL_NAME = ? "
                        + ", IDCARD = ? "
                        + ", MOBILE = ? "
                        + ", EMAIL = ? "
                        + ", IS_ACTIVE = ? "
                        + ", UPDATED_DATE = SYSDATE "
                        + ", UPDATED_BY = ? "
                        + " WHERE MEMBER_ID = ? ";

                ps = conn.prepareStatement(sql);

            }

            //Set Parameter
            ps.setString(i++, member.getUserName());
            ps.setString(i++, member.getFullName());

            ps.setString(i++, member.getIdcard());
            ps.setString(i++, member.getMobile());
            ps.setString(i++, member.getEmail());

//            ps.setBoolean(i++, member.isActive());
            ps.setBoolean(i++, true);

            if (member.getMemberId() == 0) {

                if (!AppUtil.isNullAndSpace(member.getUpdatedBy())) {
                    ps.setString(i++, member.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                member.setMemberId(id);

            } else {

                if (!AppUtil.isNullAndSpace(member.getUpdatedBy())) {
                    ps.setString(i++, member.getUpdatedBy());
                } else {
                    ps.setNull(i++, java.sql.Types.NVARCHAR);
                }

                ps.setInt(i++, member.getMemberId());

                result = ps.executeUpdate() != 0;

            }

            //</editor-fold>
            conn.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }

    public boolean duplicateUserName(String userName) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            int i = 1;

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM MEMBER "
                    + " WHERE (USER_NAME LIKE ?) AND (IS_ACTIVE = 1)  ";

            ps.setSql(sql);

            ps.setString(i++, userName);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public boolean register(String userName) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Delete Member">
            sql = " DELETE FROM MEMBER "
                    + " WHERE USER_NAME = ? ";

            i = 1;
            ps = conn.prepareStatement(sql);

            ps.setString(i++, userName.trim());

            result = ps.executeUpdate() != 0;

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="register Member">
            sql = "INSERT INTO MEMBER ( "
                    + "  USER_NAME "
                    + ", IS_ACTIVE "
                    + ", CREATED_DATE "
                    + ", CREATED_BY"
                    + ") VALUES(?,?,SYSDATE,?) ";

            ps = conn.prepareStatement(sql, new String[]{"MEMBER_ID"});

            //Set Parameter
            i = 1;
            ps.setString(i++, userName.trim());
            ps.setBoolean(i++, false);
            ps.setNull(i++, java.sql.Types.NVARCHAR);

            result = ps.executeUpdate() != 0;

            ResultSet rs = ps.getGeneratedKeys();
            int id = 0;
            while (rs.next()) {
                id = rs.getInt(1);
            }

            if (id <= 0) {
                throw new Exception();
            }

            //</editor-fold>
            conn.commit();
            result = true;

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }

    public boolean hasDataByUserName(String userName) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM MEMBER "
                    + " WHERE (USER_NAME LIKE ?) ";

            ps.setSql(sql);

            ps.setString(1, userName);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    public boolean setPassword(String userName, String password) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {

            conn.setAutoCommit(false);

            int i = 1;

            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="set password Member">
            sql = "UPDATE MEMBER SET "
                    + "  PASSWORD = ? "
                    + ", IS_ACTIVE = ? "
                    + ", ACTIVE_DATE = SYSDATE "
                    + ", UPDATED_DATE = SYSDATE "
                    + ", UPDATED_BY = ? "
                    + " WHERE (USER_NAME LIKE ?) ";

            ps = conn.prepareStatement(sql);

            //Set Parameter
            ps.setString(i++, AppUtil.encrypt(password.trim()));
            ps.setBoolean(i++, true);
            ps.setNull(i++, java.sql.Types.NVARCHAR);
            ps.setString(i++, userName.trim());

            boolean resultUpdate = ps.executeUpdate() != 0;

            if (resultUpdate == false) {
                throw new Exception();
            }

            //</editor-fold>
            conn.commit();
            result = true;

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            conn.rollback();

        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }

        }

        return result;
    }

    public boolean hasDataByUserNamePassword(String userName, String password) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM MEMBER "
                    + " WHERE (USER_NAME LIKE ?) AND (PASSWORD LIKE ?) ";

            ps.setSql(sql);

            ps.setString(1, userName.trim());
            ps.setString(2, AppUtil.encrypt(password.trim()));

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

}
