/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.MemberModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.MemberRetailViewModel;
import viewModel.MemberViewModel;

/**
 *
 * @author Sirichai
 */
public class MemberRetailMapper {

    private ResultSet rs = null;

    public MemberRetailMapper() {

    }

    public MemberRetailMapper(ResultSet rs) {
        this.rs = rs;
    }
    
    public List<MemberViewModel> mapFullList() {

        List<MemberViewModel> dataList = new ArrayList<MemberViewModel>();

        try {

            while (rs.next()) {

                MemberViewModel data = new MemberViewModel();
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));                

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public MemberRetailViewModel mapFull() {

        MemberRetailViewModel data = new MemberRetailViewModel();

        try {

            while (rs.next()) {

                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));                
                data.setCustomerType(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE"))); 

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public MemberModel mapFull(MemberViewModel dataView) {

        MemberModel data = new MemberModel();

        try {
            data.setMemberId(AppUtil.decryptId(dataView.getMemberId()));
            data.setUserName(dataView.getUserName());
            data.setFullName(dataView.getFullName());
            
            data.setIdcard(dataView.getIdcard());
            data.setMobile(dataView.getMobile());
            data.setEmail(dataView.getEmail());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
