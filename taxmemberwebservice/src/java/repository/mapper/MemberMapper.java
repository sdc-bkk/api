/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.MemberModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.MemberViewModel;

/**
 *
 * @author Sirichai
 */
public class MemberMapper {

    private ResultSet rs = null;

    public MemberMapper() {

    }

    public MemberMapper(ResultSet rs) {
        this.rs = rs;
    }

    public List<MemberViewModel> mapFullList() {

        List<MemberViewModel> dataList = new ArrayList<MemberViewModel>();

        try {

            while (rs.next()) {

                MemberViewModel data = new MemberViewModel();

                data.setMemberId(AppUtil.encryptId(rs.getInt("MEMBER_ID")));
                data.setUserName(AppUtil.checkNullData(rs.getString("USER_NAME")));
                data.setFullName(AppUtil.checkNullData(rs.getString("FULL_NAME")));
                
                data.setIdcard(AppUtil.checkNullData(rs.getString("IDCARD")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                
                if (rs.getDate("ACTIVE_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("ACTIVE_DATE")));
                }
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                    data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                    }
                }

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<MemberViewModel> mapFullDrpList() {

        List<MemberViewModel> dataList = new ArrayList<MemberViewModel>();

        try {

            while (rs.next()) {

                MemberViewModel data = new MemberViewModel();

                data.setMemberId(AppUtil.encryptId(rs.getInt("MEMBER_ID")));
                data.setUserName(rs.getString("USER_NAME"));
                data.setFullName(rs.getString("FULL_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    public MemberViewModel mapFull() {

        MemberViewModel data = new MemberViewModel();

        try {

            while (rs.next()) {

                data.setMemberId(AppUtil.encryptId(rs.getInt("MEMBER_ID")));
                data.setUserName(AppUtil.checkNullData(rs.getString("USER_NAME")));
                data.setFullName(AppUtil.checkNullData(rs.getString("FULL_NAME")));
                
                data.setIdcard(AppUtil.checkNullData(rs.getString("IDCARD")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("tax_no")));
                
                if (rs.getDate("ACTIVE_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("ACTIVE_DATE")));
                }
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                if (rs.getDate("CREATED_DATE") != null) {
                    data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                }

                if (rs.getDate("UPDATED_DATE") != null) {
                    //วันที่แก้ไขล่าสุด
                    data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                    data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                } else {
                    //วันที่สร้าง
                    data.setCreatedBy(rs.getString("CREATED_BY"));
                    if (rs.getDate("CREATED_DATE") != null) {
                        data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public MemberModel mapFull(MemberViewModel dataView) {

        MemberModel data = new MemberModel();

        try {
            data.setMemberId(AppUtil.decryptId(dataView.getMemberId()));
            data.setUserName(dataView.getUserName());
            data.setFullName(dataView.getFullName());
            
            data.setIdcard(dataView.getIdcard());
            data.setMobile(dataView.getMobile());
            data.setEmail(dataView.getEmail());
            
            data.setActive(dataView.getActive());

            if (!AppUtil.isNullAndSpace(dataView.getCreatedBy())) {
                data.setCreatedBy(dataView.getCreatedBy());
            }

            if (!AppUtil.isNullAndSpace(dataView.getUpdatedBy())) {
                data.setUpdatedBy(dataView.getUpdatedBy());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

/*
    private List<UserRoleModel> mapFullListUserRole(List<UserRoleViewModel> dataViewList) {

        List<UserRoleModel> dataList = new ArrayList<UserRoleModel>();

        try {

            for (UserRoleViewModel dataView : dataViewList) {

                UserRoleModel data = new UserRoleModel();

                data.setRoleId(AppUtil.decryptId(dataView.getRoleId()));

                dataList.add(data);
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }
    
    
    public List<UserRoleViewModel> mapFullListUserInRole() {

        List<UserRoleViewModel> dataList = new ArrayList<UserRoleViewModel>();

        try {

            while (rs.next()) {

                UserRoleViewModel data = new UserRoleViewModel();

                data.setRoleId(AppUtil.encryptId(rs.getInt("ROLE_ID")));
                data.setRoleName(rs.getString("ROLE_NAME"));
                
                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public UsersViewModel mapFullSSO() {

        UsersViewModel data = new UsersViewModel();

        try {

            while (rs.next()) {

                //data.setUserId(AppUtil.encryptId(rs.getInt("USER_TEMP_ID")));
                data.setUserCode(rs.getString("USER_CODE"));
                data.setUserName(rs.getString("USER_NAME"));
                data.setFullName(rs.getString("FULL_NAME"));
                data.setFirstName(rs.getString("FIRST_NAME"));
                data.setLastName(rs.getString("LAST_NAME"));
                
                data.setMobile(rs.getString("MOBILE"));
                data.setEmail(rs.getString("EMAIL"));
                data.setOffice(rs.getString("OFFICE"));
                data.setPartOf(rs.getString("PART_OF"));
                data.setDepartment(rs.getString("DEPARTMENT"));
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
*/
    
}
