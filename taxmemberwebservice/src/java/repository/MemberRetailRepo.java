/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.MemberModel;
import model.data.ResultData;
import repository.mapper.MemberMapper;
import repository.mapper.MemberRetailMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.MemberRetailViewModel;
import viewModel.MemberViewModel;

/**
 *
 * @author User
 */
public class MemberRetailRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public ResultData<MemberRetailViewModel> getData(String username) throws SQLException {

        ResultData<MemberRetailViewModel> resultData = new ResultData<MemberRetailViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT DISTINCT M.*, RETAIL.OWNER_NAME, RETAIL.CUSTOMER_TYPE FROM MEMBER_OF_RETAIL M\n"
                    + "LEFT JOIN RETAIL ON M.TAX_NO = RETAIL.TAX_NO\n"
                    + "WHERE M.EMAIL = ? ";

            ps.setSql(sql);

            ps.setString(1, username);

            rs = ps.executeQuery();

            MemberRetailViewModel data = new MemberRetailMapper(rs).mapFull();

            if (data != null && data.getTaxNo() != "") {
                //get data list
                sql = " SELECT DISTINCT M.* FROM MEMBER_OF_RETAIL M\n"
                        + " WHERE M.TAX_NO = ? ";

                ps.setSql(sql);

                ps.setString(1, data.getTaxNo());

                rs = ps.executeQuery();

                List<MemberViewModel> memberlist = new MemberRetailMapper(rs).mapFullList();

                data.setMemberList(memberlist);
            }

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public boolean save(MemberRetailViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);

            //update isActive = 0 ทั้งหมด
            String sql = "UPDATE MEMBER_OF_RETAIL SET "
                    + " IS_ACTIVE = 0 "
                    + " WHERE  TAX_NO = ? ";

            ps = conn.prepareStatement(sql);
            ps.setString(1, data.getTaxNo());
            result = ps.executeUpdate() != 0;

            //<editor-fold defaultstate="collapsed" desc="loop list of Id">
            for (MemberViewModel member : data.getMemberList()) {
                int i = 1;
                boolean checkHasData = hasDataByEmail(member.getEmail());

                if (!checkHasData) {
                    // ถ้ายังไม่มีให้ insert
                    String sqlInsert = "INSERT INTO MEMBER_OF_RETAIL ( "
                            + "  EMAIL "
                            + ", TAX_NO "
                            + ", CREATED_DATE "
                            + ", CREATED_BY "
                            + ", IS_ACTIVE "
                            + ") VALUES(?,?,SYSDATE,?,1) ";
                    i = 1;
                    ps = conn.prepareStatement(sqlInsert);
                    ps.setString(i++, member.getEmail());
                    ps.setString(i++, data.getTaxNo());
                    if (!AppUtil.isNullAndSpace(data.getCreatedBy())) {
                        ps.setString(i++, data.getCreatedBy());
                    } else {
                        ps.setNull(i++, java.sql.Types.NVARCHAR);
                    }
                    result = ps.executeUpdate() != 0;
                } else {

                    String sqlUpdate = "UPDATE MEMBER_OF_RETAIL SET "
                            + " IS_ACTIVE = 1 "
                            + " WHERE EMAIL = ? AND TAX_NO = ? ";

                    i = 1;
                    ps = conn.prepareStatement(sqlUpdate);
                    ps.setString(i++, member.getEmail());
                    ps.setString(i++, data.getTaxNo());
                    result = ps.executeUpdate() != 0;
                }

            }
            //</editor-fold>

            //update isActive = 0 ทั้งหมด
            String sqldelete = "DELETE FROM MEMBER_OF_RETAIL "
                    + " WHERE  TAX_NO = ? AND IS_ACTIVE = 0";

            ps = conn.prepareStatement(sqldelete);
            ps.setString(1, data.getTaxNo());
            result = ps.executeUpdate() != 0;

            conn.commit();
            result = true;

        } catch (Exception e) {
            result = false;

            conn.rollback();
        } finally {

            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return result;
    }

    public boolean hasDataByEmail(String email) throws SQLException {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT COUNT(*) AS COUNT_DATA "
                    + " FROM MEMBER_OF_RETAIL "
                    + " WHERE (EMAIL = ?) ";

            ps.setSql(sql);

            ps.setString(1, email);

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getInt("COUNT_DATA") > 0;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }
}
