/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import javax.json.bind.annotation.JsonbProperty;
/**
 *
 * @author User
 */
public class InvoiceViewModel {
    
    private String ref1;//รหัสอ้างอิง 1
    private String ref2;//รหัสอ้างอิง 2
    private String tax_no;//เลขประจำตัวผู้เสียภาษี 
    private String cus_name;//ชื่อผู้ชำระ
    private String cus_address1;//ที่อยู่ผู้ชำระบรรทัดที่ 1
    private String cus_address2;//ที่อยู่ผู้ชำระบรรทัดที่ 2
    private String due_date;//วันที่ครบกำหนดชำระ 
    private String detail1;//รายการรับที่ 1
    private String amount1;//จำนวนเงิน (บาท) 
    private String detail2; 
    private String amount2;  
    private String detail3; 
    private String amount3; 
    private String detail4; 
    private String amount4;
    private String detail5;
    private String amount5;
    private String total_amount;//จำนวนเงินรวมที่ต้องชำระ (บาท)
    private String add_pattern_percent;//ร้อยละที่ใช้คำนวณจำนวนเงินเพิ่ม
    private String add_due_month;//จำนวนเดือนที่ชำระเกินกำหนด
    private String payment_term;//งวดที่ชำระ
    private String tax_year;//ปี ภาษี พ.ศ.
    private String pay_tax_year;//ชำระปีที่ พ.ศ.
    private String ref_doc_date;//ลงวันที่หนังสือแจ้งการประเมิน/วันที่อ้างอิง
    private String ref_doc_no;//เลขที่หนังสือแจ้งการประเมิน/เอกสารอ้างอิง
    private String notice_rcv_date;//วันที่รับแจ้งการประเมิน
    private String contact_footer;//ข้อมูลติดต่อของหน่วยงาน
    private String doc_note;//หมายเหตุ
    
    
    
    
    
    //not used
    private String cusAddress;//ที่อยู่ผู้ชำระ
    private Double totalAmountDouble;//จำนวนเงินรวมที่ต้องชำระ (บาท)
    private String office;//หน่วยงาน
    private String partOfOffice;//ส่วนราชการ
    private String telephone;//เบอร์โทรศัพท์
    private String taxTypeCode;//รหัสภาษี
    private String taxTypeName;//ชื่อภาษี
    private String actionDate;//วันที่นำเข้าข้อมูล
    private String paymentStatus;//สถานะการชำระเงิน (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
    private String bmaBillNo;//เลขที่ใบแจ้งชำระของระบบ BMA
    private String barcode;
    private String qrcode;
    private String invoiceId;
    private int invoiceIdInt;
    private String transactionId;//รหัสเดียวกับ invoiceId
    private String billNo;//รหัสบัญชีธนาคารของกรุงเทพมหานคร

    public int getInvoiceIdInt() {
        return invoiceIdInt;
    }

    public void setInvoiceIdInt(int invoiceIdInt) {
        this.invoiceIdInt = invoiceIdInt;
    }

    public String getCusAddress() {
        return cusAddress;
    }

    public void setCusAddress(String cusAddress) {
        this.cusAddress = cusAddress;
    }
    
    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRef1() {
        return ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    public String getRef2() {
        return ref2;
    }

    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }
    
    public String getCusName() {
        return cus_name;
    }
    
    public void setCusName(String cusName) {
        this.cus_name = cusName;
    }
    
    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getDueDate() {
        return due_date;
    }

    public void setDueDate(String dueDate) {
        this.due_date = dueDate;
    }

    public String getDetail1() {
        return detail1;
    }

    public void setDetail1(String detail1) {
        this.detail1 = detail1;
    }

    public String getAmount1() {
        return amount1;
    }

    public void setAmount1(String amount1) {
        this.amount1 = amount1;
    }

    public String getDetail2() {
        return detail2;
    }

    public void setDetail2(String detail2) {
        this.detail2 = detail2;
    }

    public String getAmount2() {
        return amount2;
    }

    public void setAmount2(String amount2) {
        this.amount2 = amount2;
    }

    public String getDetail3() {
        return detail3;
    }

    public void setDetail3(String detail3) {
        this.detail3 = detail3;
    }

    public String getAmount3() {
        return amount3;
    }

    public void setAmount3(String amount3) {
        this.amount3 = amount3;
    }

    public String getDetail4() {
        return detail4;
    }

    public void setDetail4(String detail4) {
        this.detail4 = detail4;
    }

    public String getAmount4() {
        return amount4;
    }

    public void setAmount4(String amount4) {
        this.amount4 = amount4;
    }

    public String getDetail5() {
        return detail5;
    }

    public void setDetail5(String detail5) {
        this.detail5 = detail5;
    }

    public String getAmount5() {
        return amount5;
    }

    public void setAmount5(String amount5) {
        this.amount5 = amount5;
    }

    public String getTotalAmount() {
        return total_amount;
    }

    public void setTotalAmount(String totalAmount) {
        this.total_amount = totalAmount;
    }

    public Double getTotalAmountDouble() {
        return totalAmountDouble;
    }

    public void setTotalAmountDouble(Double totalAmountDouble) {
        this.totalAmountDouble = totalAmountDouble;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getPartOfOffice() {
        return partOfOffice;
    }

    public void setPartOfOffice(String partOfOffice) {
        this.partOfOffice = partOfOffice;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getTaxTypeName() {
        return taxTypeName;
    }

    public void setTaxTypeName(String taxTypeName) {
        this.taxTypeName = taxTypeName;
    }

    public String getTaxNo() {
        return tax_no;
    }

    public void setTaxNo(String taxNo) {
        this.tax_no = taxNo;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getBmaBillNo() {
        return bmaBillNo;
    }

    public void setBmaBillNo(String bmaBillNo) {
        this.bmaBillNo = bmaBillNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getCusAddress1() {
        return cus_address1;
    }

    public void setCusAddress1(String cusAddress1) {
        this.cus_address1 = cusAddress1;
    }

    public String getCusAddress2() {
        return cus_address2;
    }

    public void setCusAddress2(String cusAddress2) {
        this.cus_address2 = cusAddress2;
    }

    public String getAddPatternPercent() {
        return add_pattern_percent;
    }

    public void setAddPatternPercent(String addPatternPercent) {
        this.add_pattern_percent = addPatternPercent;
    }

    public String getAddDueMonth() {
        return add_due_month;
    }

    public void setAddDueMonth(String addDueMonth) {
        this.add_due_month = addDueMonth;
    }

    public String getPaymentTerm() {
        return payment_term;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.payment_term = paymentTerm;
    }

    public String getTaxYear() {
        return tax_year;
    }

    public void setTaxYear(String taxYear) {
        this.tax_year = taxYear;
    }

    public String getPayTaxYear() {
        return pay_tax_year;
    }

    public void setPayTaxYear(String payTaxYear) {
        this.pay_tax_year = payTaxYear;
    }

    public String getRefDocDate() {
        return ref_doc_date;
    }

    public void setRefDocDate(String refDocDate) {
        this.ref_doc_date = refDocDate;
    }

    public String getRefDocNo() {
        return ref_doc_no;
    }

    public void setRefDocNo(String refDocNo) {
        this.ref_doc_no = refDocNo;
    }

    public String getNoticeRCVDate() {
        return notice_rcv_date;
    }

    public void setNoticeRCVDate(String noticeRCVDate) {
        this.notice_rcv_date = noticeRCVDate;
    }

    public String getContactFooter() {
        return contact_footer;
    }

    public void setContactFooter(String contactFooter) {
        this.contact_footer = contactFooter;
    }

    public String getDocNote() {
        return doc_note;
    }

    public String getDoc_note() {
        return doc_note;
    }

    public void setDoc_note(String doc_note) {
        this.doc_note = doc_note;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getAdd_pattern_percent() {
        return add_pattern_percent;
    }

    public void setAdd_pattern_percent(String add_pattern_percent) {
        this.add_pattern_percent = add_pattern_percent;
    }

    public String getAdd_due_month() {
        return add_due_month;
    }

    public void setAdd_due_month(String add_due_month) {
        this.add_due_month = add_due_month;
    }

    public String getPayment_term() {
        return payment_term;
    }

    public void setPayment_term(String payment_term) {
        this.payment_term = payment_term;
    }

    public String getTax_year() {
        return tax_year;
    }

    public void setTax_year(String tax_year) {
        this.tax_year = tax_year;
    }

    public String getPay_tax_year() {
        return pay_tax_year;
    }

    public void setPay_tax_year(String pay_tax_year) {
        this.pay_tax_year = pay_tax_year;
    }

    public String getRef_doc_date() {
        return ref_doc_date;
    }

    public void setRef_doc_date(String ref_doc_date) {
        this.ref_doc_date = ref_doc_date;
    }

    public String getRef_doc_no() {
        return ref_doc_no;
    }

    public void setRef_doc_no(String ref_doc_no) {
        this.ref_doc_no = ref_doc_no;
    }

    public String getNotice_rcv_date() {
        return notice_rcv_date;
    }

    public void setNotice_rcv_date(String notice_rcv_date) {
        this.notice_rcv_date = notice_rcv_date;
    }

    public String getContact_footer() {
        return contact_footer;
    }

    public void setContact_footer(String contact_footer) {
        this.contact_footer = contact_footer;
    }

    public String getTax_no() {
        return tax_no;
    }

    public void setTax_no(String tax_no) {
        this.tax_no = tax_no;
    }

    public String getCus_address1() {
        return cus_address1;
    }

    public void setCus_address1(String cus_address1) {
        this.cus_address1 = cus_address1;
    }

    public String getCus_address2() {
        return cus_address2;
    }

    public void setCus_address2(String cus_address2) {
        this.cus_address2 = cus_address2;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    
}
