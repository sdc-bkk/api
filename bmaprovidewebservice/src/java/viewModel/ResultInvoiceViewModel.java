/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

/**
 *
 * @author User
 */
public class ResultInvoiceViewModel {
    private String transactionId;//รหัสเดียวกับ invoiceId
    private String bmaBillNo;//เลขที่ใบแจ้งชำระของระบบ BMA

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBmaBillNo() {
        return bmaBillNo;
    }

    public void setBmaBillNo(String bmaBillNo) {
        this.bmaBillNo = bmaBillNo;
    }
    
    
}
