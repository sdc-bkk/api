/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class InquiryRespModel {

    private String tranxId = "";
    private String bankRef = "";
    private int respCode = 999;
    private String respMsg = "";
    private double balance = 0.00;
    private String cusName = "";
    private String print1 = "";
    private String print2 = "";
    private String print3 = "";
    private String print4 = "";
    private String print5 = "";
    private String print6 = "";
    private String print7 = "";

    public String getTranxId() {
        return tranxId;
    }

    public void setTranxId(String tranxId) {
        this.tranxId = tranxId;
    }

    public String getBankRef() {
        return bankRef;
    }

    public void setBankRef(String bankRef) {
        this.bankRef = bankRef;
    }

    public int getRespCode() {
        return respCode;
    }

    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrint1() {
        return print1;
    }

    public void setPrint1(String print1) {
        this.print1 = print1;
    }

    public String getPrint2() {
        return print2;
    }

    public void setPrint2(String print2) {
        this.print2 = print2;
    }

    public String getPrint3() {
        return print3;
    }

    public void setPrint3(String print3) {
        this.print3 = print3;
    }

    public String getPrint4() {
        return print4;
    }

    public void setPrint4(String print4) {
        this.print4 = print4;
    }

    public String getPrint5() {
        return print5;
    }

    public void setPrint5(String print5) {
        this.print5 = print5;
    }

    public String getPrint6() {
        return print6;
    }

    public void setPrint6(String print6) {
        this.print6 = print6;
    }

    public String getPrint7() {
        return print7;
    }

    public void setPrint7(String print7) {
        this.print7 = print7;
    }
}
