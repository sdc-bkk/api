/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class DataPaymentReqModel {

    private String tran_type; //ระบุประเภทของ Message ว่าเป็น I (Inquiry) หรือ P (Payment) เท่านั้น**จะส่งค่าดังกล่าวกลับเฉพาะรายการที่ชำระเงินสำเร็จเท่านั้น
    private String site_name;//Your Web Site
    private String term_id;//ป็นค่าเดียวกับ Request Parameter
    private String term_seq;//เป็น running sequence ประจ ารายการ
    private String tran_amt;//จ านวนเงินที่ต้องช าระ
    private String post_date;//YYYYMMDD ex.20180524
    private String tran_time;//HHMMSSss ex.10012300
    private String result;//Server Result = E0200 คือท ารายการส าเร็จ หากเป็นค่าอื่นถือว่ารายการถูกปฏิเสธ
    private String approval_code;//ในกรณีที่ท ารายการ Financial ด้วยบัตรเครดิต จะมีการส่งค่า Approval Code กลับไปให้ ส าหรับรายการที่ตัดจากบัญชีธนาคาร field นี้จะมีค่าเป็น 000000
    private String ref1;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String ref2;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String ref3;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    
    private String urlServerToServer;
    private String flagServerToServer;
    private String servletPath;
    private String paramAction;

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getTerm_id() {
        return term_id;
    }

    public void setTerm_id(String term_id) {
        this.term_id = term_id;
    }

    public String getTerm_seq() {
        return term_seq;
    }

    public void setTerm_seq(String term_seq) {
        this.term_seq = term_seq;
    }

    public String getTran_amt() {
        return tran_amt;
    }

    public void setTran_amt(String tran_amt) {
        this.tran_amt = tran_amt;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getTran_time() {
        return tran_time;
    }

    public void setTran_time(String tran_time) {
        this.tran_time = tran_time;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getApproval_code() {
        return approval_code;
    }

    public void setApproval_code(String approval_code) {
        this.approval_code = approval_code;
    }

    public String getRef1() {
        return ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    public String getRef2() {
        return ref2;
    }

    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }

    public String getRef3() {
        return ref3;
    }

    public void setRef3(String ref3) {
        this.ref3 = ref3;
    }

    public String getUrlServerToServer() {
        return urlServerToServer;
    }

    public void setUrlServerToServer(String urlServerToServer) {
        this.urlServerToServer = urlServerToServer;
    }

    public String getFlagServerToServer() {
        return flagServerToServer;
    }

    public void setFlagServerToServer(String flagServerToServer) {
        this.flagServerToServer = flagServerToServer;
    }

    public String getServletPath() {
        return servletPath;
    }

    public void setServletPath(String servletPath) {
        this.servletPath = servletPath;
    }

    public String getParamAction() {
        return paramAction;
    }

    public void setParamAction(String paramAction) {
        this.paramAction = paramAction;
    }    
}
