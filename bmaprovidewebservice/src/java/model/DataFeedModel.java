/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class DataFeedModel {

    private String orderRef; //เลขที่อ้างอิงใบสั่งซื้อของร้านค้า
    private int prc;//สง่ กลับรหัสสถานะของธนาคารเจา้ของบัตร (ล าดับทหี่ นงึ่ )
    private int src;//สง่ กลับรหัสสถานะของธนาคารเจา้ของบัตร (ล าดับทสี่ อง)
    private int ord;//สง่ กลับรหัสสถานะของธนาคารเจา้ของบัตร (ล าดับทสี่ อง)
    private String holder;//ชอื่ เจา้ของบัญชชี าระเงน
    private int successcode;//0- ผ่าน , 1- ไม่ผ่าน , 2 – ยกเลิกโดยผู้ท ารายการ ,Others- ผิดพลาด
    private String orderRef1;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef2;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef3;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef4;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef5;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef6;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef7;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef8;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef9;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef10;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef11;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private String orderRef12;//เลขที่อ้างอิงเพิ่มเติมของร้านค้า
    private int payRef;//เลขที่อ้างอิงการช าระเงนิ ของ KTB FASTPAY
    private double amt;//สรุปยอดรายการ
    private String cur;//สกลุ เงนิ ทที่ ารายการ ตัวอย่างเชน่ “764” – THB “840” – USD
    private String remark;//ฟิลด์แสดงความคดิเห็นสำหรับเก็บข้อมูลเพิ่มเติม โดยจะไม่แสดงให้เห็นในหน้าการท ารายการ
    private String authId;//รหัสอนุมัต
    private String eci;//ค่า ECI (ส าหรับรา้นคา้ทรี่ องรับ 3D ได ้ 
    private String payerAuth;//สถานะการยนื ยันบคุ คลของผชู้ าระเงน
    private String sourceIp;//หมายเลขไอพี (IP address) ของผชู้ าระเงน
    private String ipCountry;//ประเทศทเี่ กดิ รายการซอื้ ขายสนิ คา้ (e.g. THA)
    private String cardNo;//หมายเลขผู้ถือบัตร แสดง 6 หลักแรกและ 4 หลักหลัง.
    private double surCharge;//ยอดเงนิ ทที่ าการรับช าระคา่ ธรรมเนยี มเพมิ่ เตมิ จากยอดรายการ
    private double totalAmt;//ยอดรวมรายการช าระเงนิ คา่ ธรรมเนยี ม รวมกับ ยอดรายการ
    private String payTime;//เวลาในการชำระเงินสำเร็จ
    private String securityKey;//เวลาในการชำระเงินสำเร็จ

    public String getOrderRef() {
        return orderRef;
    }

    public void setOrderRef(String orderRef) {
        this.orderRef = orderRef;
    }

    public int getPrc() {
        return prc;
    }

    public void setPrc(int prc) {
        this.prc = prc;
    }

    public int getSrc() {
        return src;
    }

    public void setSrc(int src) {
        this.src = src;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public int getSuccesscode() {
        return successcode;
    }

    public void setSuccesscode(int successcode) {
        this.successcode = successcode;
    }

    public String getOrderRef1() {
        return orderRef1;
    }

    public void setOrderRef1(String orderRef1) {
        this.orderRef1 = orderRef1;
    }

    public String getOrderRef2() {
        return orderRef2;
    }

    public void setOrderRef2(String orderRef2) {
        this.orderRef2 = orderRef2;
    }

    public String getOrderRef3() {
        return orderRef3;
    }

    public void setOrderRef3(String orderRef3) {
        this.orderRef3 = orderRef3;
    }

    public String getOrderRef4() {
        return orderRef4;
    }

    public void setOrderRef4(String orderRef4) {
        this.orderRef4 = orderRef4;
    }

    public String getOrderRef5() {
        return orderRef5;
    }

    public void setOrderRef5(String orderRef5) {
        this.orderRef5 = orderRef5;
    }

    public String getOrderRef6() {
        return orderRef6;
    }

    public void setOrderRef6(String orderRef6) {
        this.orderRef6 = orderRef6;
    }

    public int getPayRef() {
        return payRef;
    }

    public void setPayRef(int payRef) {
        this.payRef = payRef;
    }

    public double getAmt() {
        return amt;
    }

    public void setAmt(double amt) {
        this.amt = amt;
    }

    public String getCur() {
        return cur;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    public String getPayerAuth() {
        return payerAuth;
    }

    public void setPayerAuth(String payerAuth) {
        this.payerAuth = payerAuth;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public String getIpCountry() {
        return ipCountry;
    }

    public void setIpCountry(String ipCountry) {
        this.ipCountry = ipCountry;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public double getSurCharge() {
        return surCharge;
    }

    public void setSurCharge(double surCharge) {
        this.surCharge = surCharge;
    }

    public double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(double totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getOrderRef7() {
        return orderRef7;
    }

    public void setOrderRef7(String orderRef7) {
        this.orderRef7 = orderRef7;
    }

    public String getOrderRef8() {
        return orderRef8;
    }

    public void setOrderRef8(String orderRef8) {
        this.orderRef8 = orderRef8;
    }

    public String getOrderRef9() {
        return orderRef9;
    }

    public void setOrderRef9(String orderRef9) {
        this.orderRef9 = orderRef9;
    }

    public String getOrderRef10() {
        return orderRef10;
    }

    public void setOrderRef10(String orderRef10) {
        this.orderRef10 = orderRef10;
    }

    public String getOrderRef11() {
        return orderRef11;
    }

    public void setOrderRef11(String orderRef11) {
        this.orderRef11 = orderRef11;
    }

    public String getOrderRef12() {
        return orderRef12;
    }

    public void setOrderRef12(String orderRef12) {
        this.orderRef12 = orderRef12;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }
    
    
}
