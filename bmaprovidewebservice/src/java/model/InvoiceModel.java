/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class InvoiceModel {
    //for invoice dblink mis
    private String arivDept;//หน่วยงานเจ้าของรายรับ
    private String revTypeID;//ประเภทรายรับ
    private String arivYear;//ปี ภาษี พ.ศ.
    private String arivNo;//runnimg ของ ref1 8 หลักสุดท้าย    
    private String ref1;//เลขที่ใบแจ้งการชำระ (Ref1 18 digit) : ARIV_DEPT(4) + REV_TYPE_ID(4) + ARIV_YEAR(2) + ARIV_NO(8) = '500110186500000001'
    private String docStatus;//สถานะใบแจ้งการชำระ 0-Open ปกติ 1-Close ถูกอ้างอิง  9-Cancel ยกเลิก
    private String docDate;//วันที่ใบแจ้งการชำระ default sysdate 'DDMMYYYY'
    private String dueDate;//วันที่ครบกำหนดชำระ 
    private String ref2;//รหัสอ้างอิง 2
    private String taxNo;//เลขประจำตัวผู้เสียภาษี หรือก็คือ idTradeNo ของ mis
    private String cusName;//ชื่อผู้ชำระ คือ arName
    private String cusAddress1;//ที่อยู่ผู้ชำระ
    private String cusAddress2;//ที่อยู่ผู้ชำระ
    private double totalAmount;//จำนวนเงินรวมที่ต้องชำระ (บาท)
    private String detail1;//รายการรับที่ 1
    private double amount1;//จำนวนเงินภาษี, จำนวนเงินรายรับ taxAmt ก็คือ amount1
    private String detail2;
    private double amount2;//จำนวนเงินเบี้ยปรับ/ค่าเพิ่มค้างยื่นแบบ ADD_PATTERN_AMT ก็คือ amount2
    private String detail3;
    private double amount3;//จำนวนเงินเกินกำหนดชำระ/ค่าเพิ่มค้างชำระ ADD_DUE_AMT ก็คือ amount3
    private String detail4;
    private double amount4;
    private String detail5;
    private double amount5;
    private int addPatternPercent;//จำนวนเบี้ยปรับ ร้อยละ(%)
    private int addDueMonth;//จำนวนเงินเกินกำหนดชำระ
    private String taxYear;//ปี ภาษี พ.ศ.
    private String payTaxYear;//ชำระปีที่ พ.ศ.
    private String refDocNo;//เลขที่หนังสือแจ้งการประเมิน
    private String refDocDate;//ลงวันที่หนังสือแจ้งการประเมิน
    private String noticeRcvDate;//วันรับแจ้งการประเมิน
    private String paymentTerm;//งวดที่ชำระ
    private String docNote;//หมายเหตุ
    private String postFlag;//สถานะการผ่านรายการไประบบบัญชี (N/Y) default 'N'
    private String contactFooter;//ข้อมูลติดต่อของหน่วยงาน
    private String updateBy;//ผู้ที่แก้ไข
    
    
    
    //not used
    private int invoiceId;
    private int transactionId;//รหัสเดียวกับ invoiceId
    private String billNo;//รหัสบัญชีธนาคารของกรุงเทพมหานคร
    private String cusAddress;//ที่อยู่ผู้ชำระ
    private String office;//หน่วยงาน
    private String partOfOffice;//ส่วนราชการ
    private String telephone;//เบอร์โทรศัพท์
    private String taxTypeCode;//รหัสภาษี
    private String taxTypeName;//ชื่อภาษี
    private String actionDate;//วันที่นำเข้าข้อมูล
    private String paymentStatus;//สถานะการชำระเงิน (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค)
    private String bmaBillNo;//เลขที่ใบแจ้งชำระของระบบ BMA
    private String barcode;
    private String qrcode;

    public String getCusAddress() {
        return cusAddress;
    }

    public void setCusAddress(String cusAddress) {
        this.cusAddress = cusAddress;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRef1() {
        return ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    public String getRef2() {
        return ref2;
    }

    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getDetail1() {
        return detail1;
    }

    public void setDetail1(String detail1) {
        this.detail1 = detail1;
    }

    public double getAmount1() {
        return amount1;
    }

    public void setAmount1(double amount1) {
        this.amount1 = amount1;
    }

    public String getDetail2() {
        return detail2;
    }

    public void setDetail2(String detail2) {
        this.detail2 = detail2;
    }

    public double getAmount2() {
        return amount2;
    }

    public void setAmount2(double amount2) {
        this.amount2 = amount2;
    }

    public String getDetail3() {
        return detail3;
    }

    public void setDetail3(String detail3) {
        this.detail3 = detail3;
    }

    public double getAmount3() {
        return amount3;
    }

    public void setAmount3(double amount3) {
        this.amount3 = amount3;
    }

    public String getDetail4() {
        return detail4;
    }

    public void setDetail4(String detail4) {
        this.detail4 = detail4;
    }

    public double getAmount4() {
        return amount4;
    }

    public void setAmount4(double amount4) {
        this.amount4 = amount4;
    }

    public String getDetail5() {
        return detail5;
    }

    public void setDetail5(String detail5) {
        this.detail5 = detail5;
    }

    public double getAmount5() {
        return amount5;
    }

    public void setAmount5(double amount5) {
        this.amount5 = amount5;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getPartOfOffice() {
        return partOfOffice;
    }

    public void setPartOfOffice(String partOfOffice) {
        this.partOfOffice = partOfOffice;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getTaxTypeName() {
        return taxTypeName;
    }

    public void setTaxTypeName(String taxTypeName) {
        this.taxTypeName = taxTypeName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getBmaBillNo() {
        return bmaBillNo;
    }

    public void setBmaBillNo(String bmaBillNo) {
        this.bmaBillNo = bmaBillNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getArivDept() {
        return arivDept;
    }

    public void setArivDept(String arivDept) {
        this.arivDept = arivDept;
    }

    public String getRevTypeID() {
        return revTypeID;
    }

    public void setRevTypeID(String revTypeID) {
        this.revTypeID = revTypeID;
    }

    public String getArivYear() {
        return arivYear;
    }

    public void setArivYear(String arivYear) {
        this.arivYear = arivYear;
    }

    public String getArivNo() {
        return arivNo;
    }

    public void setArivNo(String arivNo) {
        this.arivNo = arivNo;
    }

    public String getDocStatus() {
        return docStatus;
    }

    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getCusAddress1() {
        return cusAddress1;
    }

    public void setCusAddress1(String cusAddress1) {
        this.cusAddress1 = cusAddress1;
    }

    public String getCusAddress2() {
        return cusAddress2;
    }

    public void setCusAddress2(String cusAddress2) {
        this.cusAddress2 = cusAddress2;
    }

    public int getAddPatternPercent() {
        return addPatternPercent;
    }

    public void setAddPatternPercent(int addPatternPercent) {
        this.addPatternPercent = addPatternPercent;
    }

    public int getAddDueMonth() {
        return addDueMonth;
    }

    public void setAddDueMonth(int addDueMonth) {
        this.addDueMonth = addDueMonth;
    }

    public String getTaxYear() {
        return taxYear;
    }

    public void setTaxYear(String taxYear) {
        this.taxYear = taxYear;
    }

    public String getPayTaxYear() {
        return payTaxYear;
    }

    public void setPayTaxYear(String payTaxYear) {
        this.payTaxYear = payTaxYear;
    }

    public String getRefDocNo() {
        return refDocNo;
    }

    public void setRefDocNo(String refDocNo) {
        this.refDocNo = refDocNo;
    }

    public String getRefDocDate() {
        return refDocDate;
    }

    public void setRefDocDate(String refDocDate) {
        this.refDocDate = refDocDate;
    }

    public String getNoticeRcvDate() {
        return noticeRcvDate;
    }

    public void setNoticeRcvDate(String noticeRcvDate) {
        this.noticeRcvDate = noticeRcvDate;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getDocNote() {
        return docNote;
    }

    public void setDocNote(String docNote) {
        this.docNote = docNote;
    }

    public String getPostFlag() {
        return postFlag;
    }

    public void setPostFlag(String postFlag) {
        this.postFlag = postFlag;
    }

    public String getContactFooter() {
        return contactFooter;
    }

    public void setContactFooter(String contactFooter) {
        this.contactFooter = contactFooter;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    
}
