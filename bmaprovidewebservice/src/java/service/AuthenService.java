/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import repository.AppConfigRepo;
import repository.ClentConfigRepo;
import utility.AES256;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;

public class AuthenService {

    private final ConnnectionDB connDB = new ConnnectionDB();
    AppConfigRepo appConfigRepo = new AppConfigRepo();
    
        
 private static String _userktb = "ktbpayment_uat";
 private static String _passktb = "ktb_bma_p@ssw0rd#64*_uat";

    //<editor-fold defaultstate="collapsed" desc="check permission directLink Service">
    public boolean hasKTBDirectlinkRight(String basicAuth) throws SQLException {
        boolean result = false;
//change for load test
//        String user = appConfigRepo.GetKeyValue("directlink_user");//"ktbpayment_uat";
//        String password = appConfigRepo.GetKeyValue("directlink_pass");//ktb_bma_p@ssw0rd#64*_uat";
        String user = _userktb;//"ktbpayment_uat";
        String password = _passktb;//ktb_bma_p@ssw0rd#64*_uat";
        String originalInput = user + ":" + password;
        //TO DO compare
        String encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());
        //TO DO substring
        if (encodedString.equals(basicAuth.split(" ")[1])) {
            result = true;
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="check permission client">
    public boolean checkClientRight(String clientId, String clientSecret, String grantType, String scope) {
        try {
            if (!"client_credentials".equals(grantType)) {
                return false;
            }
            ClentConfigRepo clentConfigRepo = new ClentConfigRepo();
            //TO DO scope
            String taxTypecode = clentConfigRepo.checkClientRight(clientId, clientSecret, scope);
            return !taxTypecode.isEmpty();
        } catch (Exception e) {
            return false;
        }
    }
    //</editor-fold>

}
