/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import enumeration.DirectLinkCodeEnum;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import model.ApprovalReqModel;
import model.ApprovalRespModel;
import model.ApproveReversalReqModel;
import model.ApproveReversalRespModel;
import model.InquiryReqModel;
import model.InquiryRespModel;
import model.PaymentReqModel;
import model.PaymentRespModel;
import model.ReversalReqModel;
import model.ReversalRespModel;
import repository.AppConfigRepo;
import repository.DirectLinkRepo;
import repository.InvoiceRepo;
import repository.PaymentRepo;
import utility.AppUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultPaymentViewModel;
import java.util.concurrent.ThreadLocalRandom;
import utility.ConnnectionDB;

/**
 *
 * @author DELL
 */
public class DirectLinkService {

    DirectLinkRepo directLinkRepo = new DirectLinkRepo();
    AppConfigRepo appConfigRepo = new AppConfigRepo();
    InvoiceRepo invoiceRepo = new InvoiceRepo();
    PaymentRepo paymentRepo = new PaymentRepo();

    public InquiryRespModel doInquiry(InquiryReqModel requestData) {

        InquiryRespModel data = new InquiryRespModel();

        try {
            //check case error
            //101-invalid user/pass
            if ((requestData.getUser() != null && !requestData.getUser().isEmpty())
                    && (requestData.getPassword() != null && !requestData.getPassword().isEmpty())) {
//                boolean hasRights = appConfigRepo.checkClientRight(requestData.getUser(), new String(Base64.getDecoder().decode(requestData.getPassword())));

                boolean hasRights = appConfigRepo.checkClientRight(requestData.getUser(), requestData.getPassword());
                if (hasRights == false) {
                    data.setRespCode(DirectLinkCodeEnum.InvalidUser.value());
                    data.setRespMsg(DirectLinkCodeEnum.InvalidUser.display());
                    return data;
                }
            }
            //102 Require field : ref1 
            if ((requestData.getBankRef() == null || requestData.getBankRef().isEmpty())
                    || (requestData.getRef1() == null || requestData.getRef1().isEmpty())
                    || (requestData.getRef2() == null || requestData.getRef2().isEmpty())) {
                data.setRespCode(DirectLinkCodeEnum.ReqIsNull.value());
                data.setRespMsg(DirectLinkCodeEnum.ReqIsNull.display());
                return data;
            }

            //106 Transaction number dup : check from inv_payment
            ResultPaymentViewModel paymentData = new ResultPaymentViewModel();
            paymentData = paymentRepo.getDataByRef(requestData.getRef1(), requestData.getRef2());
            if (!paymentData.getPaymentStatus().isEmpty()) {
                data.setRespCode(DirectLinkCodeEnum.TranNoDup.value());
                data.setRespMsg(DirectLinkCodeEnum.TranNoDup.display());
                return data;
            }

            //get data from invoice
            ResultData<InvoiceViewModel> invoiceResultData = new ResultData<>();
            InvoiceViewModel invoiceData = new InvoiceViewModel();
            invoiceResultData = invoiceRepo.getDataCanPayByRef(requestData.getRef1(), requestData.getRef2());

            //104 Invalid ref
            if (AppUtil.isNullAndSpace(invoiceResultData.getResult().getInvoiceId())) {
                data.setRespCode(DirectLinkCodeEnum.InvalidRef.value());
                data.setRespMsg(DirectLinkCodeEnum.InvalidRef.display());
                return data;
            } else {
                invoiceData = invoiceResultData.getResult();
            }

            //108 Invalid price
            if (requestData.getAmount() != null && requestData.getAmount() > 0) {
                if (!Objects.equals(requestData.getAmount(), invoiceData.getTotalAmountDouble())) {
                    data.setRespCode(DirectLinkCodeEnum.InvalidAmt.value());
                    data.setRespMsg(DirectLinkCodeEnum.InvalidAmt.display());
                    return data;
                }
            }

            //999 duedate
            String taxTypeCode = requestData.getRef1().substring(4, 8);
            if (!invoiceRepo.checkExistTaxType(taxTypeCode)) {
                data.setRespCode(DirectLinkCodeEnum.Other.value());
                data.setRespMsg("Not found taxtype code.");
                return data;
            }

            //gen tranno 
            String transactionPayNo = invoiceRepo.saveDataInvoiceTransTemp(invoiceData.getInvoiceId());

            //map to model            
            data.setTranxId(transactionPayNo);
            data.setBankRef(requestData.getBankRef());
            data.setRespCode(DirectLinkCodeEnum.Successful.value());
            data.setRespMsg(DirectLinkCodeEnum.Successful.display());

//            DecimalFormat decim = new DecimalFormat("0.00");
//            Double totalAmount = Double.parseDouble(decim.format(invoiceData.getTotalAmountDouble()));
            data.setBalance(invoiceData.getTotalAmountDouble());
            data.setCusName(invoiceData.getCusName());

        } catch (SQLException e) {
            data.setRespCode(DirectLinkCodeEnum.UnableProcess.value());
            data.setRespMsg(DirectLinkCodeEnum.UnableProcess.display());
        }

        return data;
    }

    public ApprovalRespModel doApprove(ApprovalReqModel requestData) {
        ApprovalRespModel data = new ApprovalRespModel();
        try {

            //check case error            
            //102 Require field : ดูแค่ req ฝั่งเรา
            if ((requestData.getBankRef() == null || requestData.getBankRef().isEmpty())
                    || (requestData.getRef1() == null || requestData.getRef1().isEmpty())
                    || (requestData.getRef2() == null || requestData.getRef2().isEmpty())) {
                data.setRespCode(DirectLinkCodeEnum.ReqIsNull.value());
                data.setRespMsg(DirectLinkCodeEnum.ReqIsNull.display());
                return data;
            }

            //999 duedate
            String duedate = requestData.getRef2().substring(0, 6);
            if (!checkDueDateCanPay(duedate)) {
                data.setRespCode(DirectLinkCodeEnum.Other.value());
                data.setRespMsg("Payment overdue");
                return data;
            }

            //get data from invoice join payment
            ResultData<InvoiceViewModel> invoiceResultData = new ResultData<>();
            InvoiceViewModel invoiceData = new InvoiceViewModel();
            invoiceResultData = invoiceRepo.getDataInvAndPay(requestData.getRef1(), requestData.getRef2());

            //104 Invalid ref
//            if (AppUtil.isNullAndSpace(invoiceResultData.getResult().getInvoiceId())) {            
            if (invoiceResultData.getResult().getInvoiceIdInt() == 0) {

                data.setRespCode(DirectLinkCodeEnum.InvalidRef.value());
                data.setRespMsg(DirectLinkCodeEnum.InvalidRef.display());
                return data;
            } else {
                invoiceData = invoiceResultData.getResult();
            }

            //108 Invalid price
            if (requestData.getAmount() != null && requestData.getAmount() > 0) {
                if (!Objects.equals(requestData.getAmount(), invoiceData.getTotalAmountDouble())) {
                    data.setRespCode(DirectLinkCodeEnum.InvalidAmt.value());
                    data.setRespMsg(DirectLinkCodeEnum.InvalidAmt.display());
                    return data;
                }
            }

            //106 Transaction number dup : check from inv_payment
            if (!"0".equals(invoiceData.getPaymentStatus())) {
                data.setRespCode(DirectLinkCodeEnum.TranNoDup.value());
                data.setRespMsg(DirectLinkCodeEnum.TranNoDup.display());
                return data;
            }

            //gen tranno               
            SimpleDateFormat formatter = new SimpleDateFormat("YYMMddHHmmss");
            Date date = new Date();
            int randomNum = ThreadLocalRandom.current().nextInt(1, 100000 + 1);
            String running = formatter.format(date) + randomNum;
            String transactionPayNo = running;//invoiceRepo.saveDataInvoiceTransTempRecode(requestData.getRef1(),requestData.getRef2());

            //map to model            
            data.setTranxId(transactionPayNo);
            data.setBankRef(requestData.getBankRef());
            data.setRespCode(DirectLinkCodeEnum.Successful.value());
            data.setRespMsg(DirectLinkCodeEnum.Successful.display());
            data.setBalance(invoiceData.getTotalAmountDouble());
            data.setCusName(invoiceData.getCusName());

        } catch (SQLException e) {
            data.setRespCode(DirectLinkCodeEnum.UnableProcess.value());
            data.setRespMsg(DirectLinkCodeEnum.UnableProcess.display());
        }

        return data;
    }

    public PaymentRespModel doPayment(PaymentReqModel requestData) {
        PaymentRespModel data = new PaymentRespModel();
        try {
            //check case error
            //102 Require field : ดูแค่ req ฝั่งเรา
            if ((requestData.getBankRef() == null || requestData.getBankRef().isEmpty())
                    || (requestData.getRef1() == null || requestData.getRef1().isEmpty())
                    || (requestData.getRef2() == null || requestData.getRef2().isEmpty())
                    || (requestData.getTranxId() == null || requestData.getTranxId().isEmpty())
                    || (requestData.getChannel() == null || requestData.getChannel().isEmpty())) {
                data.setRespCode(DirectLinkCodeEnum.ReqIsNull.value());
                data.setRespMsg(DirectLinkCodeEnum.ReqIsNull.display());
                return data;
            }

            //999 duedate
            String duedate = requestData.getRef2().substring(0, 6);
            if (!checkDueDateCanPay(duedate)) {
                data.setRespCode(DirectLinkCodeEnum.Other.value());
                data.setRespMsg("Payment overdue");
                return data;
            }

            //999 ref1 tax_type no have
//            String taxTypeCode = requestData.getRef1().substring(4, 8);
//            if (!invoiceRepo.checkExistTaxType(taxTypeCode)) {
//                data.setRespCode(DirectLinkCodeEnum.Other.value());
//                data.setRespMsg("Not found taxtype code.");
//                return data;
//            }
            //get data from invoice join payment
            ResultData<InvoiceViewModel> invoiceResultData = new ResultData<>();
            InvoiceViewModel invoiceData = new InvoiceViewModel();
            ConnnectionDB connDB = new ConnnectionDB();
            Connection conn = connDB.getConnection();
            try {
                invoiceResultData = invoiceRepo.getDataInvAndPay(requestData.getRef1(), requestData.getRef2(), conn);

                //104 Invalid ref
                if (invoiceResultData.getResult().getInvoiceIdInt() == 0) {
                    data.setRespCode(DirectLinkCodeEnum.InvalidRef.value());
                    data.setRespMsg(DirectLinkCodeEnum.InvalidRef.display());
                    return data;
                } else {
                    invoiceData = invoiceResultData.getResult();
                }

                //108 Invalid price
                if (requestData.getAmount() != null && requestData.getAmount() > 0) {
                    if (!Objects.equals(requestData.getAmount(), invoiceData.getTotalAmountDouble())) {
                        data.setRespCode(DirectLinkCodeEnum.InvalidAmt.value());
                        data.setRespMsg(DirectLinkCodeEnum.InvalidAmt.display());
                        return data;
                    }
                }

                //106 Transaction number dup : check from inv_payment
                if (!"0".equals(invoiceData.getPaymentStatus())) {
                    data.setRespCode(DirectLinkCodeEnum.TranNoDup.value());
                    data.setRespMsg(DirectLinkCodeEnum.TranNoDup.display());
                    return data;
                }

                //get tranno 
//                String transactionPayNo = requestData.getTranxId();//invoiceRepo.getTransactionNoTransTemp(invoiceData.getInvoiceId());

                //save to invoice_payment
                boolean result = invoiceRepo.saveDataFront(String.valueOf(invoiceResultData.getResult().getInvoiceIdInt()), requestData.getChannel(), requestData.getBankRef(), requestData.getTranxId(), conn);

                if (result) {
                    //map to model            
                    data.setTranxId(requestData.getTranxId());
                    data.setBankRef(requestData.getBankRef());
                    data.setRespCode(DirectLinkCodeEnum.Successful.value());
                    data.setRespMsg(DirectLinkCodeEnum.Successful.display());
                    data.setBalance(invoiceData.getTotalAmountDouble());
                    data.setCusName(invoiceData.getCusName());
                } else {
                    data.setRespCode(DirectLinkCodeEnum.UnableProcess.value());
                    data.setRespMsg(DirectLinkCodeEnum.UnableProcess.display());
                    return data;
                }
            } finally {
                conn.close();
            }

        } catch (Exception e) {
            data.setRespCode(DirectLinkCodeEnum.UnableProcess.value());
            data.setRespMsg(DirectLinkCodeEnum.UnableProcess.display());
        }

        return data;
    }

    public ReversalRespModel doReversal(ReversalReqModel filter) {
        ReversalRespModel data = new ReversalRespModel();

        try {
            data = directLinkRepo.doReversal(filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public ApproveReversalRespModel doApproveReversal(ApproveReversalReqModel filter) {
        ApproveReversalRespModel data = new ApproveReversalRespModel();

        try {
            data = directLinkRepo.doApproveReversal(filter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    //ตรวจสอบวันครบกำหนดชำระ ที่สามารถนำไปจ่ายเงินได้
    private boolean checkDueDateCanPay(String dueDateStr) {
        boolean canPay = false;
        try {
            Locale lc = new Locale("th", "TH");
            SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy", lc);

            Date dueDate = formatter.parse(dueDateStr);
            LocalDateTime localDateTime = dueDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            localDateTime = localDateTime.plusDays(1);
            Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
            dueDate = currentDatePlusOneDay;

            // Get current date;
            Date currentDate = formatter.parse(formatter.format(new Date()));

            if (currentDate.before(dueDate)) {
                //สามารถชำระได้
                canPay = true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return canPay;
    }
}
