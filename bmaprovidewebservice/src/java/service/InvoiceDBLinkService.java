/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import model.InvoiceModel;
import repository.InvoiceDBLinkRepo;
import repository.mapper.InvoiceDBLinkMapper;
import utility.MessageBundleUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;

/**
 *
 * @author User
 */
public class InvoiceDBLinkService {
    
    private final MessageBundleUtil message = new MessageBundleUtil();
    InvoiceDBLinkRepo repo = new InvoiceDBLinkRepo();
    
    //สำหรับรับข้อมูลค่าภาษี/ค่าธรรมเนียม/ค่าบริการจากระบบต่างๆ
    public ResultData<ResultInvoiceViewModel> saveInvoice(InvoiceViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        InvoiceModel dataSave = new InvoiceModel();

        try {
            //101 Require field : ref1 ,ref2,tax_no,cus_name,cus_address1,due_date,detail1,amount1,totalAmount
//            if ((requestData.getBankRef() == null || requestData.getBankRef().isEmpty())
//                    || (requestData.getRef1() == null || requestData.getRef1().isEmpty())
//                    || (requestData.getRef2() == null || requestData.getRef2().isEmpty())) {
//                data.setRespCode(DirectLinkCodeEnum.ReqIsNull.value());
//                data.setRespMsg(DirectLinkCodeEnum.ReqIsNull.display());
//                return data;
//            }

            //check format and legth
            
            //check duedate and format
            
            //check format double
            
            //check ref1/ref2 duplicate

            //map data                   
            InvoiceDBLinkMapper mapper = new InvoiceDBLinkMapper();
            dataSave = mapper.mapSaveData(data);

            //save data
            resultData = repo.saveInvoice(dataSave,true);
            
        } catch (SQLException e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

}
