/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.Date;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import utility.AppConfig;
import utility.JWTTokenUtil;
import utility.MessageBundleUtil;
import viewModel.ResultOAuthViewModel;

/**
 *
 * @author User
 */
public class OAuthService {

    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultOAuthViewModel generateToken(String grantType, String clientId, String clientSecret, String scope) {
        ResultOAuthViewModel resultData = new ResultOAuthViewModel();
        //check clientid and secret
        AuthenService authenClient = new AuthenService();
        Boolean hasPermission = authenClient.checkClientRight(clientId, clientSecret, grantType, scope);

        if (hasPermission) {
            AppConfig appconfig = new AppConfig();
            String secret = appconfig.value("secret_jwt");
            int expiresTime = appconfig.value("expireTime").isEmpty() ? Integer.valueOf(appconfig.value("expireTime")) : 60;//60 นาที
            String sysName = appconfig.value("sysName");

            //generate access_token
            JWTTokenUtil jwt = new JWTTokenUtil();
            resultData.setAccess_token(jwt.generateJWT(sysName, clientId, secret, expiresTime));
            resultData.setExpires_in(expiresTime);
        } else {
            throw new NotAuthorizedException(message.getMessage("message.notAuthorized"));
        }

        return resultData;
    }

    public void checkToken(String clientId, String token) {
        if (clientId != null || token != null) {

        } else {

        }
        throw new NotAuthorizedException(message.getMessage("message.notAuthorized"));
    }

    public String checkJWT(String token) {
        String secret = "123@abc";
        String claims = "";
        Algorithm algorithm = Algorithm.HMAC512(secret);

        try {
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("Simple Solution")
                    .acceptExpiresAt(60) // 60 seconds = 1 minute
                    .build();

            DecodedJWT decodedJWT = verifier.verify(token);

            System.out.println("Verify JWT token success.");
            System.out.println(decodedJWT.getClaims());

        } catch (JWTVerificationException ex) {
            System.out.println("Verify JWT token fail: " + ex.getMessage());
        }
        AppConfig appConfig = new AppConfig();
        claims = appConfig.value("secret_jwt");
        return claims;
    }
}
