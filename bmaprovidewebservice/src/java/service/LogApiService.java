/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import java.util.Objects;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import model.LogApiModel;
import repository.InvoiceRepo;
import repository.LogApiRepo;
import repository.PaymentRepo;
import repository.ClentConfigRepo;
import repository.mapper.InvoiceMapper;
import repository.mapper.PaymentMapper;
import utility.MessageBundleUtil;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultBasePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.ResultPaymentViewModel;
import viewModel.TaxTypeViewModel;

/**
 *
 * @author User
 */
public class LogApiService {

    LogApiRepo repo = new LogApiRepo();

    public void saveLogApi(String action,String apiName, String fromUser, String result, String refKey, String remark) {
        try {
            //MapData
            LogApiModel model = new LogApiModel();
            model.setAction(action);
            model.setApiName(apiName);
            model.setFromUser(apiName);
            model.setResult(result);
            model.setRefKey(refKey);
            model.setRemark(remark);
            repo.saveData(model);
        } catch (SQLException e) {

        }
    }

}
