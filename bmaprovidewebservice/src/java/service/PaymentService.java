/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.Objects;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import repository.InvoiceRepo;
import repository.PaymentRepo;
import repository.ClentConfigRepo;
import repository.mapper.InvoiceMapper;
import repository.mapper.PaymentMapper;
import utility.MessageBundleUtil;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultBasePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.ResultPaymentViewModel;
import viewModel.TaxTypeViewModel;

/**
 *
 * @author User
 */
public class PaymentService {

    PaymentRepo repo = new PaymentRepo();
    InvoiceRepo invoiceRepo = new InvoiceRepo();
    ClentConfigRepo taxTypeRepo = new ClentConfigRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<ResultPaymentViewModel> checkPayment(String bmaBillNo, String clientId, String clientSecret) {

        ResultData<ResultPaymentViewModel> resultData = new ResultData<ResultPaymentViewModel>();
        ResultPaymentViewModel data = null;

        try {
            //check client_id
            String taxTypecode = taxTypeRepo.checkClientRight(clientId, clientSecret, "");
            if (!taxTypecode.isEmpty()) { //ถ้าไม่ว่าง

                data = repo.getData(bmaBillNo);
                if ("".equals(data.getRef1())) {
                    resultData = invoiceRepo.getDataByBmaBillNo(bmaBillNo, data);
                    if ("".equals(resultData.getResult().getRef1())) {
                        resultData.setStatus("true");
                        resultData.setResult(data);
                        resultData.setResultMsg(message.getMessage("message.list.nodata"));
                    }
                } else {
                    resultData.setStatus("true");
                    resultData.setResult(data);
                    resultData.setResultMsg(message.getMessage("message.list.hasdata"));
                }

            } else {
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.noauthen"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.nodata"));
        }

        return resultData;
    }

    public ResultData<ResultBasePaymentViewModel> updatePayment(InvoicePaymentViewModel data, String clientId, String clientSecret) {

        ResultData resultData = new ResultData();
        InvoicePaymentModel dataSave = new InvoicePaymentModel();

        try {
            //check client_id
            String taxTypecode = taxTypeRepo.checkClientRight(clientId, clientSecret, "");
            if (!taxTypecode.isEmpty()) { //ถ้าไม่ว่าง
                PaymentMapper mapper = new PaymentMapper();
                dataSave = mapper.mapSaveData(data);
                resultData = repo.saveData(dataSave);

            } else {
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.noauthen"));
            }

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

}
