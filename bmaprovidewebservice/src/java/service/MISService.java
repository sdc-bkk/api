/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import repository.MISRepo;
import repository.TaxOilRepo;
import viewModel.InvoicePaymentViewModel;

/**
 *
 * @author User
 */
public class MISService {
    
    MISRepo repo = new MISRepo();
    TaxOilRepo taxOilRepo = new TaxOilRepo();
    
    public String paymentSuccess(InvoicePaymentViewModel data) {

        String result = "false";
        
        try {

            result = repo.updateInvoicePayment(data);
            
            int id = repo.getTransactionId(data.getRef1(),data.getRef2());
            data.setTransactionId(Integer.toString(id));
            
            result = taxOilRepo.updatePayment(data);

        } catch (Exception e) {
            result = e.getMessage();
        }

        return result;
    }

}
