/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import model.InvoiceModel;
import repository.AppConfigRepo;
import repository.InvoiceRepo;
import repository.ClentConfigRepo;
import repository.mapper.InvoiceMapper;
import utility.AppUtil;
import utility.MessageBundleUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.TaxTypeViewModel;

/**
 *
 * @author User
 */
public class InvoiceService {

    InvoiceRepo repo = new InvoiceRepo();
    AppConfigRepo appRepo = new AppConfigRepo();
    ClentConfigRepo clentConfigRepo = new ClentConfigRepo();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData<ResultInvoiceViewModel> sendExternalInvoice(InvoiceViewModel data, String clientId, String clientSecret) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        InvoiceModel dataSave = new InvoiceModel();

        try {
            //check client_id
            String taxTypecode = clentConfigRepo.checkClientRight(clientId, clientSecret, "1013");//"1013";
            if (!taxTypecode.isEmpty()) { //ถ้าไม่ว่าง
                //check taxtype from client_id
                ResultData<TaxTypeViewModel> taxTypeData = new ResultData<TaxTypeViewModel>();
                taxTypeData = clentConfigRepo.getDataByTaxTypeCode(taxTypecode);
//                resultData.setResultMsg(taxTypeData.getResult().getTaxTypeName());
                if (!taxTypeData.getResult().getTaxTypeCode().isEmpty()) {
                    data.setTaxTypeCode(taxTypecode);
//                    data.setTaxTypeCode(taxTypeData.getResult().getTaxTypeCode());
//                    data.setTaxTypeName(taxTypeData.getResult().getTaxTypeName());

                    //check data from BMA_BILL_NO ถ้ามีรายการแล้วให้ลบออกและสร้างใหม่                    
                    InvoiceMapper mapper = new InvoiceMapper();
                    dataSave = mapper.mapSaveData(data);

                    resultData = repo.sendExternalInvoice(dataSave);

                } else {
                    resultData.setStatus("false");
                    resultData.setResultMsg(message.getMessage("message.notaxtypecode"));
                }
            } else {
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.noauthen"));
            }

        } catch (SQLException e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

    public ResultData<InvoiceViewModel> getDataCanPayByRef(String ref1, String ref2) {
        ResultData<InvoiceViewModel> resultData = new ResultData<>();

        try {
            resultData = repo.getDataCanPayByRef(ref1, ref2);

        } catch (SQLException e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.list.error"));
        }

        return resultData;
    }

    public ResultData<ResultInvoiceViewModel> saveInvoice(InvoiceViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        InvoiceModel dataSave = new InvoiceModel();

        try {
            //check client_id
            String taxTypecode = "1013";//clentConfigRepo.checkClientRight(clientId, clientSecret,"");//"1013";
            if (!taxTypecode.isEmpty()) { //ถ้าไม่ว่าง
                //check taxtype from client_id
                ResultData<TaxTypeViewModel> taxTypeData = new ResultData<TaxTypeViewModel>();
                taxTypeData = clentConfigRepo.getDataByTaxTypeCode(taxTypecode);
//                resultData.setResultMsg(taxTypeData.getResult().getTaxTypeName());
                if (!taxTypeData.getResult().getTaxTypeCode().isEmpty()) {
                    data.setTaxTypeCode(taxTypecode);
//                    data.setTaxTypeCode(taxTypeData.getResult().getTaxTypeCode());
//                    data.setTaxTypeName(taxTypeData.getResult().getTaxTypeName());

                    //check data from BMA_BILL_NO ถ้ามีรายการแล้วให้ลบออกและสร้างใหม่                    
                    InvoiceMapper mapper = new InvoiceMapper();
                    dataSave = mapper.mapSaveData(data);

                    resultData = repo.sendExternalInvoice(dataSave);

                } else {
                    resultData.setStatus("false");
                    resultData.setResultMsg(message.getMessage("message.notaxtypecode"));
                }
            } else {
                resultData.setStatus("false");
                resultData.setResultMsg(message.getMessage("message.noauthen"));
            }

        } catch (SQLException e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return resultData;
    }

}
