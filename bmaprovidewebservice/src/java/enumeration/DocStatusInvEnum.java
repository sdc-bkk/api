/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumeration;

/**
 *
 * @author Prapaporn
 */
public enum DocStatusInvEnum {

    Open(0, "ปกติ"),
    Close(1,"ถูกอ้างอิง"),
    Cancel(9, "ยกเลิก");

    private final int value;

    private final String displayName;

    DocStatusInvEnum(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    public int value() {
        return value;
    }

    public String display() {
        return displayName;
    }
    
    public static String getDisplayName(int value) throws IllegalArgumentException {
        for (DocStatusInvEnum newsResourceType : DocStatusInvEnum.values()) {
            if (newsResourceType.value == value) {
                return newsResourceType.displayName;
            }
        }
        throw new IllegalArgumentException();
    }
}
