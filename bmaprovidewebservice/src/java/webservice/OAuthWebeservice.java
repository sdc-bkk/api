/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import service.OAuthService;
import viewModel.ResultOAuthViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("v2/auth")
public class OAuthWebeservice {

    @Context
    private UriInfo context;
    OAuthService service = new OAuthService();

    /**
     * Creates a new instance of OAuthWebeservice
     */
    public OAuthWebeservice() {
    }

    //create access_token
    @GET
    @Produces("Application/json;charset=utf8")
    @Path("token")
    public String getToken(@Context HttpServletRequest request) {
        String grantType = request.getHeader("grant_type");
        String clientId = request.getHeader("client_id");
        String clientSecret = request.getHeader("client_secret");
        String scope = request.getHeader("scope");

        ResultOAuthViewModel resultData = service.generateToken(grantType, clientId, clientSecret, scope);
        return new Gson().toJson(resultData);
    }

    @GET
    @Produces("Application/json;charset=utf8")
    @Path("check_token")
    public String saveData(@Context HttpServletRequest request) {

        String result = "";
        String clientId = request.getHeader("client_id");
        String clientSecret = request.getHeader("client_secret");
        String token = request.getHeader("token");

        result = service.checkJWT(token);

        return result;

    }

    public static <T> void handleErrorStatus(Response response) {
        final int status = response.getStatus();
        switch (status) {
//        case 400:
//            throw new BadRequestException(response);
            case 401:
                throw new NotAuthorizedException(response);
//        case 403:
//            throw new ForbiddenException(response);
//        case 404:
//            throw new NotFoundException(response);
//        case 405:
//            throw new NotAllowedException(response);
//        case 406:
//            throw new NotAcceptableException(response);
//        case 415:
//            throw new NotSupportedException(response);
//        case 500:
//            throw new InternalServerErrorException(response);
//        case 503:
//            throw new ServiceUnavailableException(response);
            default:
                break;
        }
//    if (status >= 400 && status < 500)
//        throw new ClientErrorException(response);
//    if (status >= 500)
//        throw new ServerErrorException(response);
//    throw new WebApplicationException(response);
    }

}
