/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.InvoiceDBLinkService;
import service.InvoiceService;
import service.OAuthService;
import utility.MessageBundleUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("v2/invoice")
public class InvoiceWebservice {

    @Context
    private UriInfo context;

    InvoiceDBLinkService service = new InvoiceDBLinkService();
    OAuthService oauthService = new OAuthService();
    private final MessageBundleUtil message = new MessageBundleUtil();

    /**
     * Creates a new instance of InvoiceWebservice
     */
    public InvoiceWebservice(@Context HttpServletRequest request) {        
        //check permission TODO
        String clientId = request.getHeader("client_id");
        String token = request.getHeader("token");
//        oauthService.checkToken(clientId,token);
    }

    //กรณีการเพิ่มหรือแก้ไขใบแจ้งหนี้ที่ยังไม่ได้ชำระ
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    public String saveData(@Context HttpServletRequest request, InvoiceViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        try {            
            //create invoice
            resultData.setStatus("tru");
            resultData.setErrorMsg("ok");
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
            resultData = service.saveInvoice(data);
            
            //savelog
        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }

}
