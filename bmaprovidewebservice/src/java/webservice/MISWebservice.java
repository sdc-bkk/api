/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import service.MISService;
import viewModel.InvoicePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("mis")
public class MISWebservice {

    @Context
    private UriInfo context;
    MISService service = new MISService();

    /**
     * Creates a new instance of MISWebservice
     */
    public MISWebservice() {
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("paymentOnlineSuccess")
    public String paymentOnlineSuccess(InvoicePaymentViewModel data) {

        String result = "false";
        try {

            result = service.paymentSuccess(data);

        } catch (Exception e) {
            result = e.getMessage();
        }

        return result;

    }

}
