/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import service.PaymentService;
import utility.MessageBundleUtil;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultBasePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultPaymentViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("payment")
public class PaymentWebservice {

    PaymentService service = new PaymentService();
    private final MessageBundleUtil message = new MessageBundleUtil();
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PaymentWebservice
     */
    public PaymentWebservice() {
    }

    @GET
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")

    @Path("checkPayment/{bmaBillNo}")
    public String checkPayment(@Context HttpServletRequest request,@PathParam("bmaBillNo") String bmaBillNo){
    

        ResultData<ResultPaymentViewModel> resultData = new ResultData<ResultPaymentViewModel>();
        try {
            String clientId = request.getHeader("client_id");
            String clientSecret = request.getHeader("client_secret");

            resultData = service.checkPayment(bmaBillNo, clientId, clientSecret);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);
    }
    
    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")

    @Path("updatePayment")
    public String updatePayment(@Context HttpServletRequest request, InvoicePaymentViewModel data) {
        
        ResultData<ResultBasePaymentViewModel> resultData = new ResultData<ResultBasePaymentViewModel>();
        try {
            String clientId = request.getHeader("client_id");
            String clientSecret = request.getHeader("client_secret");

            resultData = service.updatePayment(data, clientId, clientSecret);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);
    }
   
}
