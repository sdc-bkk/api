/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import enumeration.DirectLinkCodeEnum;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import model.ApprovalReqModel;
import model.ApprovalRespModel;
import model.DataFeedModel;
import model.DataPaymentReqModel;
import model.InquiryReqModel;
import model.InquiryRespModel;
import model.PaymentReqModel;
import model.PaymentRespModel;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import service.AuthenService;
import service.DirectLinkService;
import service.FastpayService;
import service.LogApiService;
import service.Page2PageService;
import utility.JsonUtils;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("ktb")
public class KtbWebservice {

    @Context
    private UriInfo context;
    DirectLinkService directLinkService = new DirectLinkService();
    AuthenService authenService = new AuthenService();
    LogApiService logApiService = new LogApiService();
    FastpayService fastpayService = new FastpayService();
    Page2PageService page2PageService = new Page2PageService();

    /**
     * Creates a new instance of KtbWebservice
     */
    public KtbWebservice() {
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("inquiry")
    public String inquiry(@Context HttpHeaders req, InquiryReqModel filter) {
        InquiryRespModel resultData = new InquiryRespModel();
        //สำหรับสอบถามข้อมูลของใบแจ้งหนี้ที่สามารถชำระได้
        try {
            List<String> authHeaders = req.getRequestHeader(HttpHeaders.AUTHORIZATION);
            logApiService.saveLogApi("Call API", "inquiry", authHeaders.get(0), "SUCCESS", "ref1:" + filter.getRef1(), new Gson().toJson(filter));
            if (authenService.hasKTBDirectlinkRight(authHeaders.get(0))) {
                resultData = directLinkService.doInquiry(filter);
                logApiService.saveLogApi("doInquiry", "inquiry", "", "SUCCESS", "ref1:" + filter.getRef1(), resultData.getRespMsg());
            } else {
                //กรณี user pass ไม่มีในระบบ
                Response res = Response.status(Response.Status.UNAUTHORIZED).entity("Unauthorized.").build();
                resultData.setRespCode(res.getStatus());
                resultData.setRespMsg(res.getEntity().toString());
                logApiService.saveLogApi("Authen by Basic Auth.", "inquiry", "", "SUCCESS", "ref1:" + filter.getRef1(), res.getEntity().toString());
            }

        } catch (SQLException e) {
            logApiService.saveLogApi("Exception", "inquiry", "", "FAIL", "ref1:" + filter.getRef1(), e.getMessage());
        }

        return new Gson().toJson(resultData);

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("approval")
    public String approval(@Context HttpHeaders req, ApprovalReqModel filter) {
        ApprovalRespModel resultData = new ApprovalRespModel();
        //สำหรับสอบถามข้อมูลของใบแจ้งหนี้ที่สามารถชำระได้
        try {
            List<String> authHeaders = req.getRequestHeader(HttpHeaders.AUTHORIZATION);
//            logApiService.saveLogApi("Call API", "approval", authHeaders.get(0), "SUCCESS", "ref1:" + filter.getRef1(), new Gson().toJson(filter));

            if (authenService.hasKTBDirectlinkRight(authHeaders.get(0))) {
                resultData = directLinkService.doApprove(filter);
                //logApiService.saveLogApi("doApprove", "approval", "", "SUCCESS", "ref1:" + filter.getRef1(), resultData.getRespMsg());
            } else {
                //101-invalid user/pass กรณี user pass ไม่มีในระบบ
                Response res = Response.status(Response.Status.UNAUTHORIZED).entity("Unauthorized.").build();
                resultData.setRespCode(DirectLinkCodeEnum.InvalidUser.value());
                resultData.setRespMsg(DirectLinkCodeEnum.InvalidUser.display());
                logApiService.saveLogApi("Authen by Basic Auth.", "approval", "", "SUCCESS", "ref1:" + filter.getRef1(), res.getEntity().toString());
            }
        } catch (Exception e) {
            logApiService.saveLogApi("Exception", "approval", "", "FAIL", "ref1:" + filter.getRef1(), e.getMessage());
        }

        //string map
        return new Gson().toJson(resultData);
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("approval2")
    public String approval2(@Context HttpHeaders req, ApprovalReqModel filter) {
        ApprovalRespModel resultData = new ApprovalRespModel();
        //สำหรับสอบถามข้อมูลของใบแจ้งหนี้ที่สามารถชำระได้
        try {
            List<String> authHeaders = req.getRequestHeader(HttpHeaders.AUTHORIZATION);
//            logApiService.saveLogApi("Call API", "approval", authHeaders.get(0), "SUCCESS", "ref1:" + filter.getRef1(), new Gson().toJson(filter));

            if (authenService.hasKTBDirectlinkRight(authHeaders.get(0))) {
                resultData = directLinkService.doApprove(filter);
                //logApiService.saveLogApi("doApprove", "approval", "", "SUCCESS", "ref1:" + filter.getRef1(), resultData.getRespMsg());
            } else {
                //101-invalid user/pass กรณี user pass ไม่มีในระบบ
                Response res = Response.status(Response.Status.UNAUTHORIZED).entity("Unauthorized.").build();
                resultData.setRespCode(DirectLinkCodeEnum.InvalidUser.value());
                resultData.setRespMsg(DirectLinkCodeEnum.InvalidUser.display());
                logApiService.saveLogApi("Authen by Basic Auth.", "approval", "", "SUCCESS", "ref1:" + filter.getRef1(), res.getEntity().toString());
            }
        } catch (Exception e) {
            logApiService.saveLogApi("Exception", "approval", "", "FAIL", "ref1:" + filter.getRef1(), e.getMessage());
        }

        return new Gson().toJson(resultData);
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("charge2")
    public String charge2(@Context HttpHeaders req, PaymentReqModel filter) {
        PaymentRespModel resultData = new PaymentRespModel();
        //สำหรับยืนยันการชำระเงิน
        try {
            List<String> authHeaders = req.getRequestHeader(HttpHeaders.AUTHORIZATION);
//            logApiService.saveLogApi("Call API", "payment", authHeaders.get(0), "SUCCESS", "ref1:" + filter.getRef1(), new Gson().toJson(filter));
            if (authenService.hasKTBDirectlinkRight(authHeaders.get(0))) {
                resultData = directLinkService.doPayment(filter);
//                logApiService.saveLogApi("doPayment", "payment", "", "SUCCESS", "ref1:" + filter.getRef1(), resultData.getRespMsg());
            } else {
                //กรณี user pass ไม่มีในระบบ
                Response res = Response.status(Response.Status.UNAUTHORIZED).entity("Unauthorized.").build();
                resultData.setRespCode(DirectLinkCodeEnum.InvalidUser.value());
                resultData.setRespMsg(DirectLinkCodeEnum.InvalidUser.display());
                logApiService.saveLogApi("Authen by Basic Auth.", "payment", "", "SUCCESS", "ref1:" + filter.getRef1(), res.getEntity().toString());
            }

        } catch (SQLException e) {
            logApiService.saveLogApi("Exception", "charge", "", "FAIL", "ref1:" + filter.getRef1(), e.getMessage());
        }

        return new Gson().toJson(resultData);

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("charge")
    public String charge(@Context HttpHeaders req, PaymentReqModel filter) {
        PaymentRespModel resultData = new PaymentRespModel();
        //สำหรับยืนยันการชำระเงิน
        try {
            List<String> authHeaders = req.getRequestHeader(HttpHeaders.AUTHORIZATION);
//            logApiService.saveLogApi("Call API", "payment", authHeaders.get(0), "SUCCESS", "ref1:" + filter.getRef1(), new Gson().toJson(filter));
            if (authenService.hasKTBDirectlinkRight(authHeaders.get(0))) {
                resultData = directLinkService.doPayment(filter);
//                logApiService.saveLogApi("doPayment", "payment", "", "SUCCESS", "ref1:" + filter.getRef1(), resultData.getRespMsg());
            } else {
                //กรณี user pass ไม่มีในระบบ
                Response res = Response.status(Response.Status.UNAUTHORIZED).entity("Unauthorized.").build();
                resultData.setRespCode(DirectLinkCodeEnum.InvalidUser.value());
                resultData.setRespMsg(DirectLinkCodeEnum.InvalidUser.display());
                logApiService.saveLogApi("Authen by Basic Auth.", "payment", "", "SUCCESS", "ref1:" + filter.getRef1(), res.getEntity().toString());
            }

        } catch (SQLException e) {
            logApiService.saveLogApi("Exception", "charge", "", "FAIL", "ref1:" + filter.getRef1(), e.getMessage());
        }

        return new Gson().toJson(resultData);

    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")
    @Path("datafeedjson")
    public String datafeedjson(@Context HttpHeaders req, DataFeedModel filter) {
        String result = "FAIL";
        //สำหรับการชำระเงินของ fastpay
//        try {
        logApiService.saveLogApi("Call API", "datafeed", "", "SUCCESS", "ref1:" + filter.getOrderRef(), new Gson().toJson(filter));

        result = fastpayService.datafeed(filter);
        logApiService.saveLogApi("Fastpay", "datafeed", "", "SUCCESS", "ref1:" + filter.getOrderRef(), new Gson().toJson(filter));

//        } catch (SQLException e) {
//            logApiService.saveLogApi("Exception", "datafeed", "", "FAIL", "ref1:" + filter.getOrderRef(), e.getMessage());
//        }
        return result;

    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Path("datafeed")
    public String datafeed(@Context HttpServletRequest request) throws IOException {
        String result = "FAIL";
        //สำหรับยืนยันการชำระเงินของ page2page
        DataFeedModel model = new DataFeedModel();
        try {
//        InputStream body = request.getInputStream();
            //Read from request
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = null;

            try {
                InputStream inputStream = request.getInputStream();

                if (inputStream != null) {
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                    char[] charBuffer = new char[128];
                    int bytesRead = -1;

                    while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                        stringBuilder.append(charBuffer, 0, bytesRead);
                    }
                    DataFeedModel data = new DataFeedModel();
                    Object JsonToObject = JsonUtils.JsonToObject(stringBuilder.toString(), (Class<Object>) (Object) DataFeedModel.class);
                    data = (DataFeedModel) JsonToObject;

                } else {
                    stringBuilder.append("");
                }
            } catch (IOException ex) {

            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException ex) {

                    }
                }
            }

            String body = stringBuilder.toString();

            model.setOrderRef(request.getParameter("orderRef"));
//            model.setTerm_id(request.getParameter("term_id"));
//            model.setTerm_seq(request.getParameter("term_seq"));
//            model.setTran_amt(request.getParameter("tran_amt"));
//            model.setPost_date(request.getParameter("post_date"));
//            model.setTran_time(request.getParameter("tran_time"));
//            model.setResult(request.getParameter("result"));
//            model.setApproval_code(request.getParameter("approval_code"));
//            model.setUrlServerToServer(request.getParameter("urlServerToServer"));
//            model.setFlagServerToServer(request.getParameter("flagServerToServer"));
//            model.setServletPath(request.getParameter("servletPath"));
//            model.setParamAction(request.getParameter("paramAction"));

            logApiService.saveLogApi("Call API", "datafeed", "", "SUCCESS", "ref1:" + model.getOrderRef(), new Gson().toJson(model));
            result = fastpayService.datafeed(model);

        } catch (Exception e) {
            logApiService.saveLogApi("Exception", "datafeed", "", "FAIL", "ref1:" + model.getOrderRef(), e.getMessage());
        } finally {

        }

        return result;

    }

    @POST
//    @Consumes("multipart/form-data")
    @Consumes("application/x-www-form-urlencoded")

    //@Produces("Application/json;charset=utf8")
    @Path("p2p/Payment")
    public String p2pPayment(@Context HttpServletRequest request) throws IOException {
        String result = "FAIL";
        //สำหรับยืนยันการชำระเงินของ page2page

        DataPaymentReqModel model = new DataPaymentReqModel();
        try {
//        InputStream body = request.getInputStream();
            //Read from request
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = null;

            try {
                InputStream inputStream = request.getInputStream();

                if (inputStream != null) {
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                    char[] charBuffer = new char[128];
                    int bytesRead = -1;

                    while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                        stringBuilder.append(charBuffer, 0, bytesRead);
                    }
//                    String output = "";
//                    String inLine;
//                    while ((inLine = bufferedReader.readLine()) != null) {
//                        output = output + inLine;
//                    }
                    DataPaymentReqModel data = new DataPaymentReqModel();
                    Object JsonToObject = JsonUtils.JsonToObject(stringBuilder.toString(), (Class<Object>) (Object) DataPaymentReqModel.class);
                    data = (DataPaymentReqModel) JsonToObject;

                } else {
                    stringBuilder.append("");
                }
            } catch (IOException ex) {

            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException ex) {

                    }
                }
            }

            String body = stringBuilder.toString();

            model.setSite_name(request.getParameter("site_name"));
            model.setTerm_id(request.getParameter("term_id"));
            model.setTerm_seq(request.getParameter("term_seq"));
            model.setTran_amt(request.getParameter("tran_amt"));
            model.setPost_date(request.getParameter("post_date"));
            model.setTran_time(request.getParameter("tran_time"));
            model.setResult(request.getParameter("result"));
            model.setApproval_code(request.getParameter("approval_code"));
            model.setUrlServerToServer(request.getParameter("urlServerToServer"));
            model.setFlagServerToServer(request.getParameter("flagServerToServer"));
            model.setServletPath(request.getParameter("servletPath"));
            model.setParamAction(request.getParameter("paramAction"));

            logApiService.saveLogApi("Call API", "p2pPayment", "", "SUCCESS", "ref1:" + model.getRef1(), new Gson().toJson(model));
            result = page2PageService.datafeed(model);

        } catch (Exception e) {
            logApiService.saveLogApi("Exception", "p2pPayment", "", "FAIL", "ref1:" + model.getRef1(), e.getMessage());
        } finally {

        }

        return result;

    }

}
