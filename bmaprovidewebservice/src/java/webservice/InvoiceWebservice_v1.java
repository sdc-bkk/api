/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import service.InvoiceService;
import utility.MessageBundleUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("invoice")
public class InvoiceWebservice_v1 {

    @Context
    private UriInfo context;

    InvoiceService service = new InvoiceService();
    private final MessageBundleUtil message = new MessageBundleUtil();

    /**
     * Creates a new instance of InvoiceWebservice
     */
    public InvoiceWebservice_v1() {
    }

    @POST
    @Consumes("Application/json;charset=utf8")
    @Produces("Application/json;charset=utf8")

    @Path("sendExternalInvoice")
    public String sendExternalInvoice(@Context HttpServletRequest request, InvoiceViewModel data) {

        ResultData<ResultInvoiceViewModel> resultData = new ResultData<ResultInvoiceViewModel>();
        try {
            String clientId = request.getHeader("client_id");
            String clientSecret = request.getHeader("client_secret");

            resultData = service.sendExternalInvoice(data, clientId, clientSecret);

        } catch (Exception e) {
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));
        }

        return new Gson().toJson(resultData);

    }

}
