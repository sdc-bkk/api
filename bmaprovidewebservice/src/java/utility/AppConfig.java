/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Prapaporn
 */
public class AppConfig {
    
    public String value(String key) {

        InputStream inputStream = null;
        Properties prop = new Properties();
        try {

            String propFileName = "/config/config.properties";

            inputStream = getClass().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            
        } finally {
            
            try {
                if(inputStream != null)
                    inputStream.close();
            } catch (IOException ex) {
                
            }
        }

        return prop.getProperty(key);
    }
    
}
