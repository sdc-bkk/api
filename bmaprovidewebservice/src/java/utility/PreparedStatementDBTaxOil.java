/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import viewModel.ResultPage;

/**
 *
 * @author Prapaporn
 */
public class PreparedStatementDBTaxOil {

    private Connection conn = null;
    private PreparedStatement ps = null;
    private Map<Integer, Object> dataMap = new HashMap<Integer, Object>();
    private String sql;
    private ResultPage resultPage = null;

    private boolean hasPage;
    private boolean hasOrderBy;
    private String orderBy = "(SELECT 1)";
    
    public PreparedStatementDBTaxOil(Connection conn) throws SQLException {
        this.conn = conn;
        this.hasPage = false;
        this.hasOrderBy = false;

    }

    private Map<Integer, Object> sortMap(Map<Integer, Object> dataMap) {
        Map<Integer, Object> sortMapData = new TreeMap<Integer, Object>(dataMap);
        return sortMapData;
    }

    public void setInt(int index, int value) {
        dataMap.put(index, value);
    }
    
    public void setLong(int index, long value) {
        dataMap.put(index, value);
    }

    public void setString(int index, String value) {
        value = value != null ? value.trim() : null;
        value = value != null ? value.equals("") ? null : value : null;
        dataMap.put(index, value);
    }

    public void setBoolean(int index, boolean value) {
        dataMap.put(index, value);
    }

    public void setDate(int index, Date value) {
        dataMap.put(index, value);
    }

    public void setTimestamp(int index, Timestamp value) {
        dataMap.put(index, value);
    }

    public void setFloat(int index, float value) {
        dataMap.put(index, value);
    }

    public void setNull(int index) {
        dataMap.put(index, null);
    }

    public void setOrderBy(String orderBy) {
        this.hasOrderBy = true;
        this.orderBy = orderBy;
    }

    public void setAutoCommit(boolean autoCommit) throws SQLException {
        conn.setAutoCommit(autoCommit);
    }

    public void setSql(String sql) throws SQLException {
        this.sql = sql;
        dataMap = new HashMap<Integer, Object>();
    }

    public void setResultPage(ResultPage resultPage) {
        this.resultPage = resultPage;
        this.hasPage = true;
    }

    public void commit() throws SQLException {
        conn.commit();
    }

    public void rollback() throws SQLException {
        conn.rollback();
    }

    public ResultSet executeQuery() throws SQLException, Exception {
        dataMap = sortMap(dataMap);

        ps = null;
        try {
            if (hasPage) {
                ps = conn.prepareStatement(sqlResultPage(sql, resultPage));
            } else {
                if (hasOrderBy) {
                    sql = setOrderByStr(sql);
                }

                ps = conn.prepareStatement(sql);
            }

            for (Map.Entry<Integer, Object> entry : dataMap.entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }

            return ps.executeQuery();
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean executeUpdate() throws SQLException, Exception {
        dataMap = sortMap(dataMap);
        ps = null;
        try {
            if (hasPage) {
                ps = conn.prepareStatement(sqlResultPage(sql, resultPage));
            } else {
                if (hasOrderBy) {
                    sql = setOrderByStr(sql);
                }

                ps = conn.prepareStatement(sql);
            }

            for (Map.Entry<Integer, Object> entry : dataMap.entrySet()) {
//                System.out.println("getKey : "+entry.getKey()+" : getValue : "+entry.getValue());
                ps.setObject(entry.getKey(), entry.getValue());
            }

            return ps.executeUpdate() != 0;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public ResultPage getResultPage() throws SQLException, Exception {
        dataMap = sortMap(dataMap);
        ps = null;
        try {
            String queryCount = String.format("SELECT COUNT(*) c1 FROM (%s) t1", sql);
            ps = conn.prepareStatement(queryCount);
            for (Map.Entry<Integer, Object> entry : dataMap.entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet resultSetTotal = ps.executeQuery();
            resultSetTotal.next();
            resultPage.setTotalItem(resultSetTotal.getInt("c1"));
            return resultPage;
        } catch (Exception e) {
            throw e;
        }
    }

    private String sqlResultPage(String sql, ResultPage paging) throws Exception {
        String result = "";
        try {
            result = String.format("SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY %s) rn, v1.* FROM (%s) v1) v2 WHERE rn > %d AND rn <= %d", orderBy, sql,
                    (paging.getPageNo() - 1) * paging.getItemPerPage(),
                    paging.getPageNo() * paging.getItemPerPage());
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    private String setOrderByStr(String sql) throws SQLException, Exception {
        try {
            String queryCount = String.format("%s ORDER BY %s", sql, orderBy);

            return queryCount;
        } catch (Exception e) {
            throw e;
        }
    }

    private void connectionClose(Connection connection) throws Exception {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void preparedStatementClose(PreparedStatement pstmt) throws Exception {
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void resultSetClose(ResultSet rs) throws SQLException {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public void closeConnection() {
        try {
            connectionClose(conn);
            preparedStatementClose(ps);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
