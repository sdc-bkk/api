/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.Date;

/**
 *
 * @author User
 */
public class JWTTokenUtil {

    public String generateJWT(String sysName, String clientId, String secret, int expiresTime) {
//        String secret = "123@abc";
        Algorithm algorithm = Algorithm.HMAC512(secret);
       long expireTime = (new Date().getTime()) + 30000; // 60000 milliseconds = 60 seconds = 1 minute
 //        long expireTime = (new Date().getTime()) + (expiresTime * 60000);
        Date expireDate = new Date(expireTime);

        String generatedToken = JWT.create()
                .withIssuer(sysName)
                .withClaim("client_id", clientId)
                .withExpiresAt(expireDate)
                .sign(algorithm);

        return generatedToken;
    }

    public String verifyJWT(String sysName, String secret, int expiresTime, String token) {
//        String secret = "123@abc";
        String result = "fail";
        Algorithm algorithm = Algorithm.HMAC512(secret);

        try {
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(sysName)
                    //                    .acceptExpiresAt(60) // 60 seconds = 1 minute
                    .acceptExpiresAt(expiresTime) // 60 seconds = 1 minute
                    .build();

            DecodedJWT decodedJWT = verifier.verify(token);

//            System.out.println("Verify JWT token success.");
//            System.out.println(decodedJWT.getClaims());
            result = "success";
        } catch (JWTVerificationException ex) {
            System.out.println("Verify JWT token fail: " + ex.getMessage());
            result = "fail";
        }
        return result;
    }
}
