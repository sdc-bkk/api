/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import repository.mapper.ResultPaymentMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.InvoicePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.ResultPaymentViewModel;

/**
 *
 * @author User
 */
public class PaymentRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public ResultData saveData(InvoicePaymentModel data) throws SQLException {

        ResultData resultData = new ResultData();
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        boolean result = false;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";
//insert select
            //<editor-fold defaultstate="collapsed" desc="insert into payment from invoice">
            sql = "INSERT INTO INVOICE_PAYMENT ( "
                    + "  BILL_NO "
                    + ", REF1 "
                    + ", REF2 "
                    + ", TAX_NO "
                    + ", CUS_NAME "
                    + ", DUE_DATE "
                    + ", DETAIL1 "
                    + ", AMOUNT1 "
                    + ", DETAIL2 "
                    + ", AMOUNT2 "
                    + ", DETAIL3 "
                    + ", AMOUNT3 "
                    + ", DETAIL4 "
                    + ", AMOUNT4 "
                    + ", DETAIL5 "
                    + ", AMOUNT5 "
                    + ", TOTAL_AMOUNT "
                    + ", OFFICE "
                    + ", PART_OF_OFFICE "
                    + ", TELEPHONE"
                    + ", TAX_TYPE_CODE "
                    + ", TAX_TYPE_NAME "
                    + ", INVOICE_DATE "
                    + ", TRANSACTION_ID "
                    + ", BMA_BILL_NO "
                    + ", BARCODE "
                    + ", QRCODE "
                    + ", PAYMENT_STATUS "
                    + ", PAYMENT_METHOD "
                    + ", PAYMENT_DATE "
                    + ", PAYMENT_TIME "
                    + ", PAYMENT_AMOUNT "
                    + ", RECEIPT_NO"
                    + ", CHEQUE_DATE "
                    + ", CHEQUE_NO "
                    + ", CHEQUE_BANK "
                    + ", CHEQUE_BRANCH "
                    + ") "
                    + " SELECT BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, DUE_DATE"
                    + ", DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5 "
                    + ", TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME "
                    + ", ACTION_DATE, INVOICE_ID, BMA_BILL_NO, BARCODE, QRCODE "
                    + ", ?, ?, ?, ?, ? "
                    + ", ?, ? , ?, ?, ? "
                    + " FROM INVOICE "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_NO = ? ";

            ps = conn.prepareStatement(sql);

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getPaymentStatus());
            ps.setString(i++, data.getPaymentMethod());
            ps.setDate(i++, AppUtil.toDateSql(data.getPaymentDate()));
            ps.setString(i++, data.getPaymentTime());
            ps.setDouble(i++, data.getAmount());

            ps.setString(i++, data.getReceiptNo());
            ps.setDate(i++, AppUtil.toDateSql(data.getChequeDate()));
            ps.setString(i++, data.getChequeNo());
            ps.setString(i++, data.getChequeBank());
            ps.setString(i++, data.getChequeBranch());

            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());
            ps.setString(i++, data.getTaxNo());

            result = ps.executeUpdate() != 0;

//            ResultSet rs = ps.getGeneratedKeys();
//            int id = 0;
//            while (rs.next()) {
//                id = rs.getInt(1);
//            }
//
//            if (id <= 0) {
//                throw new Exception();
//            }
//
//            data.setInvoiceId(id);
            //</editor-fold>
            conn.commit();

            resultData.setResult(true);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    public ResultPaymentViewModel getData(String bmaBillNo) throws SQLException {

        ResultData<ResultPaymentViewModel> resultData = new ResultData<ResultPaymentViewModel>();
        ResultPaymentViewModel data = null;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM INVOICE_PAYMENT "
                    + " WHERE BMA_BILL_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, bmaBillNo);

            rs = ps.executeQuery();

            data = new ResultPaymentMapper(rs).mapFull();

//            resultData.setResult(data);
//            resultData.setStatus("true");
        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return data;
//        return resultData;

    }
    
    public ResultPaymentViewModel getDataByRef(String ref1,String ref2) throws SQLException {

        ResultData<ResultPaymentViewModel> resultData = new ResultData<ResultPaymentViewModel>();
        ResultPaymentViewModel data = null;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM INVOICE_PAYMENT "
                    + " WHERE REF1 = ? AND  REF2 = ? ";

            ps.setSql(sql);
            ps.setString(1, ref1);
            ps.setString(2, ref2);

            rs = ps.executeQuery();

            data = new ResultPaymentMapper(rs).mapFull();
            
        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return data;
//        return resultData;

    }
}
