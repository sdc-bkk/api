/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.LogApiModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;

/**
 *
 * @author Sirichai
 */
public class LogApiRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public void saveData(LogApiModel data) throws SQLException {
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;

        try {
            conn.setAutoCommit(false);
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="insert">
            String sql = "INSERT INTO LOG_API ( "
                    + "  ACTION_DATE "
                    + ", ACTION "
                    + ", API_NAME "
                    + ", FROM_USER "
                    + ", RESULT "
                    + ", REF_KEY "
                    + ", REMARK "
                    + ") VALUES(SYSDATE,?,?,?,?,?,?) ";

            ps = conn.prepareStatement(sql);

            //Set Parameter
            ps.setString(i++, data.getAction());
            ps.setString(i++, data.getApiName());
            ps.setString(i++, data.getFromUser());
            ps.setString(i++, data.getResult());
            ps.setString(i++, data.getRefKey());
            ps.setString(i++, data.getRemark());

            boolean result = ps.executeUpdate() != 0;

            //</editor-fold>
            conn.commit();

        } catch (SQLException e) {
            conn.rollback();
        } finally {
            if (ps != null) {ps.close();}
            if (conn != null) {conn.close();}
        }
    }

}
