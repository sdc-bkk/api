/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import utility.AppUtil;
import utility.ConnnectionDBTaxOil;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.InvoicePaymentViewModel;

/**
 *
 * @author User
 */
public class TaxOilRepo {
    
    private final ConnnectionDBTaxOil connDB = new ConnnectionDBTaxOil();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public String updatePayment(InvoicePaymentViewModel data) throws SQLException {
        
        String result = "false";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="update INVOICE">
            sql = " UPDATE INVOICE SET "
                    + " PAYMENT_STATUS = ? "
                    + ",PAYMENT_METHOD = ? "
                    + ",PAYMENT_DATE = SYSDATE "
                    + ",PAYMENT_AMOUNT = ? "
                    + ",RECEIPT_NO = ? "
                    + ",RECEIPT_DATE = SYSDATE "
                    + ",CHEQUE_DATE = ? "
                    + ",CHEQUE_NO = ? "
                    + ",CHEQUE_BANK = ? "
                    + ",CHEQUE_BRANCH = ? "
                    + ",PAYMENT_TIME = ? "
                    + ",BMA_TRANSACTION_ID = ? "
                    + " WHERE REF1 = ? AND REF2 = ? ";

            ps.setSql(sql);

            ps.setString(i++, data.getPaymentStatus());
            ps.setString(i++, data.getPaymentMethod());
            ps.setString(i++, data.getAmount());
            ps.setString(i++, data.getReceiptNo());
            ps.setDate(i++, AppUtil.toDateSql(data.getChequeDate()));
            ps.setString(i++, data.getChequeNo());
            ps.setString(i++, data.getChequeBank());
            ps.setString(i++, data.getChequeBranch());
            ps.setString(i++, data.getPaymentTime());
            ps.setString(i++, data.getTransactionId());
            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());

            ps.executeUpdate();

            //</editor-fold>
            ps.commit();
            result = "true";

        } catch (Exception e) {
            result = e.getMessage();

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }
}
