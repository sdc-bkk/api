/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import repository.mapper.TaxTypeMapper;
import utility.AES256;
import utility.ConnnectionDB;
import utility.PreparedStatementDB;
import viewModel.ResultData;
import viewModel.TaxTypeViewModel;

/**
 *
 * @author User
 */
public class ClentConfigRepo {
    private final ConnnectionDB connDB = new ConnnectionDB();
    
    public String checkClientRight(String clientId, String clientSecret, String scope) throws SQLException {

        String taxTypecode = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get client_config ">
            String sql = " SELECT TAX_TYPE_CODE "
                    + " FROM CLIENT_CONFIG  "
                    + " WHERE CLIENT_ID = ? AND CLIENT_SECRET = ? AND TAX_TYPE_CODE = ? AND IS_ACTIVE = 1  ";

            ps.setSql(sql);

            ps.setString(1, clientId);
            ps.setString(2, AES256.decrypt(clientSecret));
            ps.setString(3, scope);//scope ให้เค้าส่งมาเป็นรหัสภาษี เพื่อเป็นการตรวจสอบ

            rs = ps.executeQuery();

            while (rs.next()) {
                taxTypecode = rs.getString("TAX_TYPE_CODE");
            }

            // </editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return taxTypecode;

    }
    
    public ResultData<TaxTypeViewModel> getDataByTaxTypeCode(String taxTypeCode) throws SQLException {

        ResultData<TaxTypeViewModel> resultData = new ResultData<TaxTypeViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM M_TAX_TYPE "
                       + " WHERE TAX_TYPE_CODE = ? AND IS_ACTIVE= 1 ";

            ps.setSql(sql);

            ps.setString(1, taxTypeCode);

            rs = ps.executeQuery();

            TaxTypeViewModel data = new TaxTypeMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }
}
