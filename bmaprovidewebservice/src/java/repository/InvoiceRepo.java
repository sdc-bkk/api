/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.PaymentStatusEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import model.InvoiceModel;
import repository.mapper.InvoiceMapper;
import repository.mapper.ResultPaymentMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;
import viewModel.ResultPaymentViewModel;

/**
 *
 * @author User
 */
public class InvoiceRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    //เพิ่มรายการใบแจ้งหนี้เข้าสู่ตาราง invoice
    public ResultData sendExternalInvoice(InvoiceModel data) throws SQLException {

        ResultData resultData = new ResultData();
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        boolean result = false;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";

            //<editor-fold defaultstate="collapsed" desc="Delete by id ">
            sql = " DELETE FROM INVOICE "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_TYPE_CODE = ? ";

            i = 1;
            ps = conn.prepareStatement(sql);

            ps.setString(1, data.getRef1());
            ps.setString(2, data.getRef2());
            ps.setString(3, data.getTaxTypeCode());

            ps.executeUpdate();
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="invoice">
            if (data.getInvoiceId() == 0) {

                sql = "INSERT INTO INVOICE ( "
                        + "  BILL_NO "
                        + ", REF1 "
                        + ", REF2 "
                        + ", TAX_NO "
                        + ", CUS_NAME "
                        + ", CUS_ADDRESS "
                        + ", DUE_DATE "
                        + ", DETAIL1 "
                        + ", AMOUNT1 "
                        + ", DETAIL2 "
                        + ", AMOUNT2 "
                        + ", DETAIL3 "
                        + ", AMOUNT3 "
                        + ", DETAIL4 "
                        + ", AMOUNT4 "
                        + ", DETAIL5 "
                        + ", AMOUNT5 "
                        + ", TOTAL_AMOUNT "
                        + ", OFFICE "
                        + ", PART_OF_OFFICE "
                        + ", TELEPHONE"
                        + ", TAX_TYPE_CODE "
                        + ", TAX_TYPE_NAME "
                        + ", ACTION_DATE "
                        + ", PAYMENT_STATUS "
                        + ", BARCODE "
                        + ", QRCODE "
                        + ", BMA_BILL_NO "
                        + ") VALUES(?,?,?,?,?,?,?  ,?,?,?,?,?,?,?,?,?,?,?  ,?,?,?  ,?,?,SYSDATE   ,0,?,?,?) ";

                ps = conn.prepareStatement(sql, new String[]{"INVOICE_ID"});

            } else {

                sql = "UPDATE INVOICE SET "
                        + "  BILL_NO = ? "
                        + ", REF1 = ? "
                        + ", REF2 = ? "
                        + ", TAX_NO = ? "
                        + ", CUS_NAME = ? "
                        + ", CUS_ADDRESS = ? "
                        + ", DUE_DATE = ?"
                        + ", DETAIL1 = ? "
                        + ", AMOUNT1 = ? "
                        + ", DETAIL2 = ? "
                        + ", AMOUNT2 = ? "
                        + ", DETAIL3 = ? "
                        + ", AMOUNT3 = ? "
                        + ", DETAIL4 = ? "
                        + ", AMOUNT4 = ? "
                        + ", DETAIL5 = ? "
                        + ", AMOUNT5 = ? "
                        + ", TOTAL_AMOUNT = ? "
                        + ", OFFICE = ? "
                        + ", PART_OF_OFFICE = ? "
                        + ", TELEPHONE = ?"
                        + ", TAX_TYPE_CODE = ? "
                        + ", TAX_TYPE_NAME = ? "
                        + ", ACTION_DATE = SYSDATE "
                        + ", BARCODE  = ?"
                        + ", QRCODE  = ?"
                        + " WHERE INVOICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter">
            ps.setString(i++, data.getBillNo());
            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());
            ps.setString(i++, data.getTaxNo());
            ps.setString(i++, data.getCusName());
            ps.setString(i++, data.getCusAddress());
            ps.setDate(i++, AppUtil.toDateSql(data.getDueDate()));

            ps.setString(i++, data.getDetail1());
            ps.setDouble(i++, data.getAmount1());
            ps.setString(i++, data.getDetail2());
            ps.setDouble(i++, data.getAmount2());
            ps.setString(i++, data.getDetail3());
            ps.setDouble(i++, data.getAmount3());
            ps.setString(i++, data.getDetail4());
            ps.setDouble(i++, data.getAmount4());
            ps.setString(i++, data.getDetail5());
            ps.setDouble(i++, data.getAmount5());
            ps.setDouble(i++, data.getTotalAmount());

            ps.setString(i++, data.getOffice());
            ps.setString(i++, data.getPartOfOffice());
            ps.setString(i++, data.getTelephone());

            ps.setString(i++, data.getTaxTypeCode());
            ps.setString(i++, data.getTaxTypeName());

            ps.setString(i++, data.getBarcode());
            ps.setString(i++, data.getQrcode());

            if (data.getInvoiceId() == 0) {
                ps.setString(i++, data.getBmaBillNo());

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setInvoiceId(id);
            } else {

                ps.setInt(i++, data.getInvoiceId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>

            conn.commit();
            dataOut.setBmaBillNo(data.getBmaBillNo());
            dataOut.setTransactionId(AppUtil.encryptId(data.getInvoiceId()));

            resultData.setResult(dataOut);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(dataOut);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

    //ลบรายการใบแจ้งหนี้
    public boolean deleteData(InvoiceViewModel data) throws SQLException {
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="Delete by id ">
            sql = " DELETE FROM INVOICE "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_TYPE_CODE = ? ";

            i = 1;
            ps.setSql(sql);

            ps.setString(1, data.getRef1());
            ps.setString(2, data.getRef2());
            ps.setString(3, data.getTaxTypeCode());

            result = ps.executeUpdate();
            //</editor-fold>

            ps.commit();

        } catch (Exception e) {

            e.printStackTrace();
            result = false;
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    //ดึงข้อมูล ref1 ref2 ของใบแจ้งหนี้ด้วยเลข BmaBillNo
    public ResultData<InvoiceViewModel> getDataFullByBmaBillNo(String bmaBillNo) throws SQLException {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT REF1,REF2 FROM INVOICE "
                    + " WHERE BMA_BILL_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, bmaBillNo);

            rs = ps.executeQuery();

            InvoiceViewModel data = new InvoiceMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public ResultData<ResultPaymentViewModel> getDataByBmaBillNo(String bmaBillNo, ResultPaymentViewModel data) throws SQLException {

        ResultData<ResultPaymentViewModel> resultData = new ResultData<ResultPaymentViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT REF1,REF2 FROM INVOICE "
                    + " WHERE BMA_BILL_NO = ? ";

            ps.setSql(sql);

            ps.setString(1, bmaBillNo);

            rs = ps.executeQuery();

            data = new InvoiceMapper(rs).mapInvoicePayment();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    //ดึงข้อมูล invoiceid ของใบแจ้งหนี้ด้วยเลข ref
    public int getInvoiceIdByRef(InvoiceViewModel data) throws SQLException {

        int invoiceId = 0;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get InvoiceId">
            String sql = " SELECT INVOICE_ID "
                    + " FROM INVOICE  "
                    + " WHERE REF1 = ? AND REF2 = ? AND TAX_TYPE_CODE = ? ";

            ps.setSql(sql);

            ps.setString(1, data.getRef1());
            ps.setString(2, data.getRef2());
            ps.setString(3, data.getTaxTypeCode());

            rs = ps.executeQuery();

            while (rs.next()) {
                invoiceId = rs.getInt("INVOICE_ID");
            }

            // </editor-fold>
        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return invoiceId;

    }

    //ดึงข้อมูลของใบแจ้งหนี้ที่สามารถชำระเงินได้ด้วยเลข ref
    public ResultData<InvoiceViewModel> getDataCanPayByRef(String ref1, String ref2) throws SQLException {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT * FROM INVOICE "
                    + " WHERE REF1 = ? AND  REF2 = ? "
                    + "AND IS_CANCEL <>1 AND PAYMENT_STATUS = 0 ";//ดึงรายการที่ยังไม่ยกเลิกและยังไม่ได้ชำระเงิน

            ps.setSql(sql);
            ps.setString(1, ref1);
            ps.setString(2, ref2);

            rs = ps.executeQuery();

            InvoiceViewModel data = new InvoiceMapper(rs).mapFull();

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    //ดึงข้อมูลของใบแจ้งหนี้ที่สามารถชำระเงินได้และการชำระเงิน
    public ResultData<InvoiceViewModel> getDataInvAndPay(String ref1, String ref2, Connection conn) throws SQLException {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();
//        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT I.INVOICE_ID, I.TOTAL_AMOUNT, I.CUS_NAME, I.PAYMENT_STATUS "
                    + " FROM INVOICE I "
                    //                    + " LEFT JOIN INVOICE_PAYMENT P ON I.REF1 = P.REF1 AND I.REF2 = P.REF2 "
                    + " WHERE I.REF1 = ? AND  I.REF2 = ? "
                    + "AND I.IS_CANCEL <>1 ";//ดึงรายการที่ยังไม่ยกเลิกและยังไม่ได้ชำระเงิน

            ps.setSql(sql);
            ps.setString(1, ref1);
            ps.setString(2, ref2);

            rs = ps.executeQuery();

            InvoiceViewModel data = new InvoiceViewModel();

            while (rs.next()) {
                data.setInvoiceIdInt(rs.getInt("INVOICE_ID"));
                data.setCusName(rs.getString("CUS_NAME"));
                data.setTotalAmountDouble(rs.getDouble("TOTAL_AMOUNT"));
                data.setPaymentStatus(rs.getString("PAYMENT_STATUS"));
            }

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            ps.resultSetClose(rs);
            ps.closePrepare();
            
        }

        return resultData;

    }

    //ดึงข้อมูลของใบแจ้งหนี้ที่สามารถชำระเงินได้และการชำระเงิน
    public ResultData<InvoiceViewModel> getDataInvAndPay(String ref1, String ref2) throws SQLException {

        ResultData<InvoiceViewModel> resultData = new ResultData<InvoiceViewModel>();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT I.INVOICE_ID, I.TOTAL_AMOUNT, I.CUS_NAME, I.PAYMENT_STATUS "
                    + " FROM INVOICE I "
                    //                    + " LEFT JOIN INVOICE_PAYMENT P ON I.REF1 = P.REF1 AND I.REF2 = P.REF2 "
                    + " WHERE I.REF1 = ? AND  I.REF2 = ? "
                    + "AND I.IS_CANCEL <>1 ";//ดึงรายการที่ยังไม่ยกเลิกและยังไม่ได้ชำระเงิน

            ps.setSql(sql);
            ps.setString(1, ref1);
            ps.setString(2, ref2);

            rs = ps.executeQuery();

            InvoiceViewModel data = new InvoiceViewModel();

            while (rs.next()) {
                data.setInvoiceIdInt(rs.getInt("INVOICE_ID"));
                data.setCusName(rs.getString("CUS_NAME"));
                data.setTotalAmountDouble(rs.getDouble("TOTAL_AMOUNT"));
                data.setPaymentStatus(rs.getString("PAYMENT_STATUS"));
            }

            resultData.setResult(data);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {
//TO DO close conn
            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    //generate transPayNo ของ invoiceid
    public String saveDataInvoiceTransTemp(String invoiceId) throws SQLException {
        String running = "";
        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="delete old data  ">
            sql = " DELETE FROM INVOICE_TRANS_TEMP "
                    + " WHERE INVOICE_ID = ? ";

            i = 1;
            ps.setSql(sql);
            ps.setString(1, AppUtil.decrypt(invoiceId));

            result = ps.executeUpdate();
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="insert from gen ">
//            running = AppUtil.getRunningDoc("ktbtransno", "");
            sql = " INSERT INTO INVOICE_TRANS_TEMP (INVOICE_ID, TRANSACTION_PAY_NO  "
                    + " ) VALUES(?,?) ";

            i = 1;
            ps.setSql(sql);

            ps.setString(1, AppUtil.decrypt(invoiceId));
            ps.setString(2, running);

            result = ps.executeUpdate();

            //</editor-fold>
            ps.commit();

        } catch (Exception e) {
            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return running;
    }
    //generate transPayNo ของ invoiceid

    public String saveDataInvoiceTransTempRecode(String ref1, String ref2) throws SQLException {
        String running = "";
        boolean result = false;
        Connection conn = connDB.getConnection();
//        PreparedStatementDB ps = new PreparedStatementDB(conn);
        PreparedStatement ps = null;
//        ResultSet rs = null;

        try {

            conn.setAutoCommit(false);
//            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="delete old data  ">
            sql = " DELETE FROM INVOICE_GEN_TRANS_TEMP "
                    + " WHERE REF1 = ? AND REF2= ? ";

            i = 1;
            ps = conn.prepareStatement(sql);
//            ps.setSql(sql);
            ps.setString(1, ref1);
            ps.setString(2, ref2);

            result = ps.executeUpdate() != 0;
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="insert from gen ">
//            running = AppUtil.getRunningDoc("ktbtransno", "");
            String sqlInsert = " INSERT INTO INVOICE_GEN_TRANS_TEMP (REF1 , REF2  "
                    + " ) VALUES(?,?) ";

            i = 1;
            ps = conn.prepareStatement(sqlInsert, new String[]{"TEMP_ID"});

            ps.setString(1, ref1);
            ps.setString(2, ref2);
            result = ps.executeUpdate() != 0;

            ResultSet rs = ps.getGeneratedKeys();
            int id = 0;
            while (rs.next()) {
                id = rs.getInt(1);
            }

            if (id <= 0) {
                throw new Exception();
            }
            running = Integer.toString(id);
            //</editor-fold>
            conn.commit();

        } catch (Exception e) {
            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return running;
    }

    //ดึงข้อมูล transPayNo ของ invoiceid
    public String getTransactionNoTransTemp(String invoiceId) throws SQLException {

        String transactionNo = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT TRANSACTION_PAY_NO FROM INVOICE_TRANS_TEMP "
                    + " WHERE INVOICE_ID = ? ";

            ps.setSql(sql);
            ps.setString(1, AppUtil.decrypt(invoiceId));

            rs = ps.executeQuery();

            while (rs.next()) {
                transactionNo = rs.getString("TRANSACTION_PAY_NO");
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return transactionNo;

    }

    //บันทึกข้อมูลการชำระเงินแล้วจากธนาคาร แต่ระบบจะขึ้นเป็นรอตรวจสอบรายการ เพราะต้องรอเช็คจากไฟล์ธนาคารในแต่ละวันอีกที    
    
    public boolean saveDataFront(String invoiceId, String paymentName, String username, String transactionPayNo, Connection conn) throws SQLException, Exception {

        boolean result = false;
//        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
//        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

//            conn.setAutoCommit(false);
            ps.setAutoCommit(false);
            int i = 1;
            String sql = "";

            if (invoiceId != null) {
                //update paymentStatus = 99 (รอตรวจสอบยอดเงิน) ที่ตาราง invoice
                //<editor-fold defaultstate="collapsed" desc="sql update INVOICE">
                i = 1;
                sql = " UPDATE INVOICE SET "
                        + "    PAYMENT_STATUS = ?, TRANSACTION_ID = ?, PAY_DATE = SYSDATE "
                        + " WHERE INVOICE_ID = ? ";

//                ps = conn.prepareStatement(sql);
                ps.setSql(sql);

                ps.setString(i++, String.valueOf(PaymentStatusEnum.Pending.value())); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค, 99 = รอตรวจสอบยอดเงิน)
                ps.setString(i++, transactionPayNo);
                ps.setString(i++, invoiceId);

                result = ps.executeUpdate();

                //</editor-fold>
            }

            ps.commit();
//            conn.commit();

        } catch (SQLException e) {
            ps.rollback();
            return result = false;
        } finally {

            ps.resultSetClose(rs);
            ps.closePrepare();
//            ps.closeConnection();
        }
        return result;
    }

    public boolean saveDataFront2(String invoiceId, String paymentName, String username, String transactionPayNo) throws SQLException, Exception {

        boolean result = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
//        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

//            conn.setAutoCommit(false);
            ps.setAutoCommit(false);
            int i = 1;
            String sql = "";

            if (invoiceId != null) {
                //update paymentStatus = 99 (รอตรวจสอบยอดเงิน) ที่ตาราง invoice
                //<editor-fold defaultstate="collapsed" desc="sql update INVOICE">
                i = 1;
                sql = " UPDATE INVOICE SET "
                        + "    PAYMENT_STATUS = ? "
                        + " WHERE INVOICE_ID = ? ";

//                ps = conn.prepareStatement(sql);
                ps.setSql(sql);

                ps.setString(i++, String.valueOf(PaymentStatusEnum.Pending.value())); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค, 99 = รอตรวจสอบยอดเงิน)
                ps.setString(i++, AppUtil.decrypt(invoiceId));

                result = ps.executeUpdate();

                //</editor-fold>
                //insert into invoice_payment จาก invoice โดยใส่ค่าพวก online เพิ่มมาด้วย
                //<editor-fold defaultstate="collapsed" desc="sql insert INVOICE_PAYMENT">
                i = 1;
                sql = " INSERT INTO INVOICE_PAYMENT ( "
                        + "  BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, CUS_ADDRESS, DUE_DATE, "
                        + "  DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5, "
                        + "  TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME, "
                        + "  INVOICE_DATE, TRANSACTION_ID, BARCODE, QRCODE, BMA_BILL_NO, PAYMENT_STATUS, "
                        + "  PAYMENT_METHOD, PAYMENT_DATE, PAYMENT_AMOUNT, RECEIPT_NO, "
                        + "  CHEQUE_DATE, CHEQUE_NO, CHEQUE_BANK, CHEQUE_BRANCH, IS_ONLINE_PAYMENT, "
                        + "  ONLINE_PAYMENT_METHOD, ONLINE_PAYMENT_DATE, ONLINE_PAYMENT_REF, ONLINE_PAYMENT_AMOUNT, ONLINE_PAYMENT_BY, "
                        + "  PAYMENT_TIME, CONFIRM_PAYMENT, INVOICE_NO ) "
                        + " SELECT BILL_NO, REF1, REF2, TAX_NO, CUS_NAME, CUS_ADDRESS, DUE_DATE, "
                        + "  DETAIL1, AMOUNT1, DETAIL2, AMOUNT2, DETAIL3, AMOUNT3, DETAIL4, AMOUNT4, DETAIL5, AMOUNT5, "
                        + "  TOTAL_AMOUNT, OFFICE, PART_OF_OFFICE, TELEPHONE, TAX_TYPE_CODE, TAX_TYPE_NAME, "
                        + "  ACTION_DATE, INVOICE_ID, BARCODE, QRCODE, BMA_BILL_NO, ?, "
                        + "  NULL, NULL, NULL, NULL, "
                        + "  NULL, NULL, NULL, NULL, ?, "
                        + "  ?, SYSDATE, ?, TOTAL_AMOUNT, ?, "
                        + "  NULL, ?, NULL "
                        + " FROM INVOICE "
                        + " WHERE INVOICE_ID = ? ";

//                ps = conn.prepareStatement(sql); //, new String[]{"PAYMENT_ID"}
                ps.setSql(sql);

                ps.setString(i++, String.valueOf(PaymentStatusEnum.Pending.value())); //PAYMENT_STATUS (0 = ยังไม่ชำระ, 1 = ชำระ, 2 = เช็คขัดข้อง, 3 = ชดใช้เช็ค, 99 = รอตรวจสอบยอดเงิน)
                ps.setString(i++, "1"); //IS_ONLINE_PAYMENT = 1
                ps.setString(i++, AppUtil.checkNullData(paymentName)); //ONLINE_PAYMENT_METHOD = m_payment.payment_name
                ps.setString(i++, transactionPayNo);

                //-------------- ของเดิม ที่ออกแบบไว้ -----------------
                // ('640817' || NVL(TAX_TYPE_CODE,'') || '000000000000') 
//                Locale lc = new Locale("th", "TH");
//                DateFormat df = new SimpleDateFormat("yyMMdd", lc); // Just the year, with 2 digits
//                String yymmdd = df.format(Calendar.getInstance(lc).getTime());
                //ps.setString(i++, yymmdd); //ONLINE_PAYMENT_REF 1 = หมายเลขอ้างอิงที่ออกโดยระบบ 
                ////เลขรันนิ่ง 12 หลัก (onlinepayment)
                //String onlinepaymentRunning = AppUtil.getRunningDoc("onlinepayment", "000000000000");
                ////format : yymmdd + tax_type 4 หลัก + เลขรันนิ่ง 12 หลัก (onlinepayment)
                //ps.setString(i++, onlinepaymentRunning); //ONLINE_PAYMENT_REF 2 = หมายเลขอ้างอิงที่ออกโดยระบบ                 
                //-------------- ของเดิม ที่ออกแบบไว้ -----------------
                ps.setString(i++, AppUtil.checkNullData(username)); //ONLINE_PAYMENT_BY
                ps.setString(i++, "0"); //CONFIRM_PAYMENT = false = 0 เพราะต้องรอตรวจสอบจากไฟล์ธนาคารอีกที

                ps.setString(i++, AppUtil.decrypt(invoiceId));;

//                result = ps.executeUpdate() != 0;
                result = ps.executeUpdate();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="delete INVOICE_TRANS_TEMP  ">
//                sql = " DELETE FROM INVOICE_TRANS_TEMP "
//                        + " WHERE INVOICE_ID = ? ";
//
//                i = 1;
////                ps = conn.prepareStatement(sql);
//            ps.setSql(sql);
//                ps.setString(1, AppUtil.decrypt(invoiceId));
//
////                result = ps.executeUpdate() != 0;
//                result = ps.executeUpdate() ;
                //</editor-fold>
            }

            ps.commit();
//            conn.commit();

        } catch (SQLException e) {
            ps.rollback();
            return result = false;
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        return result;
    }

    //ดึงข้อมูล channel name from value 
    public String getChannelNameBank(String channelValue) throws SQLException {

        String result = "";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT NAME FROM CHANNEL_VALUE_BANK "
                    + " WHERE VALUE = ? ";

            ps.setSql(sql);
            ps.setInt(1, Integer.parseInt(channelValue));

            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString("NAME");
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;

    }

    //ตรวจสอบว่า tax_type มีในระบบหรือไหม
    public boolean checkExistTaxType(String taxTypeCode) throws SQLException {

        boolean isFound = false;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT TAX_TYPE_NAME FROM M_TAX_TYPE "
                    + " WHERE TAX_TYPE_CODE = ? ";

            ps.setSql(sql);
            ps.setString(1, taxTypeCode);

            rs = ps.executeQuery();

            while (rs.next()) {
                isFound = true;
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return isFound;

    }
}
