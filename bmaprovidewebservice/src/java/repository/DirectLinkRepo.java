/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.ApprovalReqModel;
import model.ApprovalRespModel;
import model.ApproveReversalReqModel;
import model.ApproveReversalRespModel;
import model.InquiryReqModel;
import model.InquiryRespModel;
import model.PaymentReqModel;
import model.PaymentRespModel;
import model.ReversalReqModel;
import model.ReversalRespModel;
import repository.mapper.DirectlinkMapper;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;

/**
 *
 * @author DELL
 */
public class DirectLinkRepo {
    
    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();
    
    public InquiryRespModel doInquiry(InquiryReqModel filter) throws SQLException {
        
        InquiryRespModel resultData = new InquiryRespModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            ps.setAutoCommit(false);
            
            String sql = " SELECT * FROM INVOICE "
                    + " WHERE "
                    + "REF1 = ? AND "
                    + "REF2 = ? "
                    + "AND IS_CANCEL <>1 AND PAYMENT_STATUS = 0 ";//ดึงรายการที่ยังไม่ยกเลิกและยังไม่ได้ชำระเงิน
            
            ps.setSql(sql);
            
            ps.setString(1, filter.getRef1());
            ps.setString(2, filter.getRef2());
            
            rs = ps.executeQuery();
            resultData = new DirectlinkMapper(rs).mapFull();
            resultData.setBankRef(filter.getBankRef());
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        
        return resultData;
        
    }
    
    public ApprovalRespModel doApprove(ApprovalReqModel filter) throws SQLException {
        ApprovalRespModel resultData = new ApprovalRespModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            resultData.setTranxId("1111123456");
            resultData.setBankRef("K0000200009876");
            resultData.setRespCode(0);
            resultData.setRespMsg("Successful");
            resultData.setBalance(1000.00);
            resultData.setCusName("David John");
            resultData.setInfo("info");
            resultData.setPrint1("Print 1");
            resultData.setPrint2("Print2");
            resultData.setPrint3("Print3");
            resultData.setPrint4("Print4");
            resultData.setPrint5("Print5");
            resultData.setPrint6("Print6");
            resultData.setPrint7("Print7");
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        
        return resultData;
    }
    
    public PaymentRespModel doPayment(PaymentReqModel filter) throws SQLException {
        PaymentRespModel resultData = new PaymentRespModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            resultData.setTranxId("1111123456");
            resultData.setBankRef("K0000200009876");
            resultData.setRespCode(0);
            resultData.setRespMsg("Successful");
            resultData.setBalance(1000.00);
            resultData.setCusName("David John");
            resultData.setInfo("info");
            resultData.setPrint1("Print 1");
            resultData.setPrint2("Print2");
            resultData.setPrint3("Print3");
            resultData.setPrint4("Print4");
            resultData.setPrint5("Print5");
            resultData.setPrint6("Print6");
            resultData.setPrint7("Print7");
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        
        return resultData;//To change body of generated methods, choose Tools | Templates.
    }
    
    public ReversalRespModel doReversal(ReversalReqModel filter) throws SQLException {
        ReversalRespModel resultData = new ReversalRespModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            resultData.setTranxId("1111123456");
            resultData.setBankRef("K0000200009876");
            resultData.setRespCode(0);
            resultData.setRespMsg("Successful");
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        
        return resultData;
    }
    
    public ApproveReversalRespModel doApproveReversal(ApproveReversalReqModel filter) throws SQLException {
        ApproveReversalRespModel resultData = new ApproveReversalRespModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;
        
        try {
            resultData.setTranxId("1111123456");
            resultData.setBankRef("K0000200009876");
            resultData.setRespCode(0);
            resultData.setRespMsg("Successful");
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            
            ps.resultSetClose(rs);
            ps.closeConnection();
        }
        
        return resultData;
    }
    
}
