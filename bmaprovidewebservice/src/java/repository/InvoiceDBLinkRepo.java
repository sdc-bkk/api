/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.InvoiceModel;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;

/**
 *
 * @author User
 */
public class InvoiceDBLinkRepo {
    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();
    
     //เพิ่มรายการใบแจ้งหนี้เข้าสู่ตาราง invoice
    public ResultData saveInvoice(InvoiceModel data,boolean isCreate) throws SQLException {

        ResultData resultData = new ResultData();
        ResultInvoiceViewModel dataOut = new ResultInvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatement ps = null;
        boolean result = false;

        try {

            conn.setAutoCommit(false);
            int i = 1;
            String sql = "";
            String sql_inv = "";

            //<editor-fold defaultstate="collapsed" desc="save invoice - PTAR_ARIV_TRAN">
            if (isCreate) {

                sql = "INSERT INTO MIS_USER.PTAR_ARIV_TRAN ( "
                        + "  ARIV_DEPT "
                        + ", REV_TYPE_ID "
                        + ", ARIV_YEAR "
                        + ", ARIV_NO "
                        
                        + ", DOC_STATUS "
                        + ", DOC_DATE "
                        + ", DUE_DATE "
                        + ", REF2 "
                        + ", ID_TRADE_NO "
                        + ", AR_NAME "
                        + ", ADDRESS1 "
                        + ", ADDRESS2 "
                        
                        + ", RECEIPT_DETAIL1 "
                        + ", TAX_AMT "
                        + ", RECEIPT_DETAIL2 "
                        + ", ADD_PATTERN_AMT "
                        + ", RECEIPT_DETAIL3 "
                        + ", ADD_DUE_AMT "
                        + ", RECEIPT_DETAIL4 "
                        + ", RECEIPT_AMT4 "
                        + ", RECEIPT_DETAIL5 "
                        + ", RECEIPT_AMT5 "
                        + ", TOTAL_AMT "                        
                        
                        + ", ADD_PATTERN_RATE"
                        + ", ADD_DUE_MONTH "
                        + ", TAX_YEAR "
                        + ", REF_DOC_NO "
                        + ", REF_DOC_DATE "
                        + ", PAY_TAX_YEAR "
                        + ", NOTICE_RCV_DATE "
                        + ", PAYMENT_TERM "                        
                        + ", DOC_NOTE "
                        + ", CONTACT_FOOTER"
                        
                        + ", POST_FLAG "                        
                        + ", CREATE_BY "
                        + ", CREATE_DATE "
                        + ") VALUES(?,?,?,?  ,?,?,?,?,?,?,?,?  ,?,?,?,?,?,?,?,?,?,?,?  ,?,?,?,?,?,?,?,?,?,?   ,'N',?,SYSDATE) ";

                ps = conn.prepareStatement(sql);

            } else {

                sql = "UPDATE INVOICE SET "
                        + "  BILL_NO = ? "
                        + ", REF1 = ? "
                        + ", REF2 = ? "
                        + ", TAX_NO = ? "
                        + ", CUS_NAME = ? "
                        + ", CUS_ADDRESS = ? "
                        + ", DUE_DATE = ?"
                        + ", DETAIL1 = ? "
                        + ", AMOUNT1 = ? "
                        + ", DETAIL2 = ? "
                        + ", AMOUNT2 = ? "
                        + ", DETAIL3 = ? "
                        + ", AMOUNT3 = ? "
                        + ", DETAIL4 = ? "
                        + ", AMOUNT4 = ? "
                        + ", DETAIL5 = ? "
                        + ", AMOUNT5 = ? "
                        + ", TOTAL_AMOUNT = ? "
                        + ", OFFICE = ? "
                        + ", PART_OF_OFFICE = ? "
                        + ", TELEPHONE = ?"
                        + ", TAX_TYPE_CODE = ? "
                        + ", TAX_TYPE_NAME = ? "
                        + ", ACTION_DATE = SYSDATE "
                        + ", BARCODE  = ?"
                        + ", QRCODE  = ?"
                        + " WHERE INVOICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter - PTAR_ARIV_TRAN">
            ps.setString(i++, data.getArivDept());
            ps.setString(i++, data.getRevTypeID());
            ps.setString(i++, data.getArivYear());
            ps.setString(i++, data.getArivNo());
            
            ps.setString(i++, data.getDocStatus());
            ps.setDate(i++, AppUtil.toDateSql(data.getDocDate()));
            ps.setDate(i++, AppUtil.toDateSql(data.getDueDate()));
            ps.setString(i++, data.getRef2());
            ps.setString(i++, data.getTaxNo());
            ps.setString(i++, data.getCusName());
            ps.setString(i++, data.getCusAddress1());
            ps.setString(i++, data.getCusAddress2());

            ps.setString(i++, data.getDetail1());
            ps.setDouble(i++, data.getAmount1());
            ps.setString(i++, data.getDetail2());
            ps.setDouble(i++, data.getAmount2());
            ps.setString(i++, data.getDetail3());
            ps.setDouble(i++, data.getAmount3());
            ps.setString(i++, data.getDetail4());
            ps.setDouble(i++, data.getAmount4());
            ps.setString(i++, data.getDetail5());
            ps.setDouble(i++, data.getAmount5());
            ps.setDouble(i++, data.getTotalAmount());

            ps.setInt(i++, data.getAddPatternPercent());
            ps.setInt(i++, data.getAddDueMonth());
            ps.setString(i++, data.getTaxYear());
            ps.setString(i++, data.getRefDocNo());
            ps.setString(i++, data.getRefDocDate());
            ps.setString(i++, data.getPayTaxYear());
            ps.setString(i++, data.getNoticeRcvDate());
            ps.setString(i++, data.getPaymentTerm());
            ps.setString(i++, data.getDocNote());
            ps.setString(i++, data.getContactFooter());
            
            ps.setString(i++, data.getUpdateBy());

            result = ps.executeUpdate() != 0;
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="save invoice of payment - INV_PAYMENT">
            if (isCreate) {

                sql_inv = "INSERT INTO INV_PAYMENT ( "
                        + "  REF1 "
                        + ", REF2 "
                        + ", BMA_BILL_NO "
                        + ") VALUES(?,?,?) ";

                ps = conn.prepareStatement(sql_inv, new String[]{"INVOICE_ID"});

            } else {

                sql_inv = "UPDATE INVOICE SET "
                        + "  BILL_NO = ? "
                        + ", REF1 = ? "
                        + ", REF2 = ? "
                        + ", TAX_NO = ? "
                        + ", CUS_NAME = ? "
                        + ", CUS_ADDRESS = ? "
                        + ", DUE_DATE = ?"
                        + ", DETAIL1 = ? "
                        + ", AMOUNT1 = ? "
                        + ", DETAIL2 = ? "
                        + ", AMOUNT2 = ? "
                        + ", DETAIL3 = ? "
                        + ", AMOUNT3 = ? "
                        + ", DETAIL4 = ? "
                        + ", AMOUNT4 = ? "
                        + ", DETAIL5 = ? "
                        + ", AMOUNT5 = ? "
                        + ", TOTAL_AMOUNT = ? "
                        + ", OFFICE = ? "
                        + ", PART_OF_OFFICE = ? "
                        + ", TELEPHONE = ?"
                        + ", TAX_TYPE_CODE = ? "
                        + ", TAX_TYPE_NAME = ? "
                        + ", ACTION_DATE = SYSDATE "
                        + ", BARCODE  = ?"
                        + ", QRCODE  = ?"
                        + " WHERE INVOICE_ID = ? ";

                ps = conn.prepareStatement(sql);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="set parameter - INV_PAYMENT">
            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());
            
            if (isCreate) {
                ps.setString(i++, data.getBmaBillNo());

                result = ps.executeUpdate() != 0;

                ResultSet rs = ps.getGeneratedKeys();
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt(1);
                }

                if (id <= 0) {
                    throw new Exception();
                }

                data.setInvoiceId(id);
            } else {

                ps.setInt(i++, data.getInvoiceId());

                result = ps.executeUpdate() != 0;
            }
            //</editor-fold>
            
//            conn.commit();
            
            dataOut.setBmaBillNo(data.getBmaBillNo());
            dataOut.setTransactionId(AppUtil.encryptId(data.getInvoiceId()));

            resultData.setResult(dataOut);
            resultData.setStatus("true");
            resultData.setResultMsg(message.getMessage("message.save.success"));

        } catch (Exception e) {
            resultData.setResult(dataOut);
            resultData.setStatus("false");
            resultData.setErrorMsg(e.getMessage());
            resultData.setResultMsg(message.getMessage("message.save.notsuccess"));

            conn.rollback();
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (conn != null) {
                conn.close();
            }
        }

        return resultData;
    }

}
