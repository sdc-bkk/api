/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.InvoicePaymentViewModel;
import viewModel.ResultData;
import viewModel.ResultInvoiceViewModel;

/**
 *
 * @author User
 */
public class MISRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();
    private final MessageBundleUtil message = new MessageBundleUtil();

    public String updateInvoicePayment(InvoicePaymentViewModel data) throws SQLException {
        String result = "false";
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="update INVOICE cancel=1 ">
            sql = " UPDATE INVOICE_PAYMENT SET "
                    + " PAYMENT_STATUS = ? "
                    + ",PAYMENT_METHOD = ? "
                    + ",PAYMENT_DATE = SYSDATE "
                    + ",PAYMENT_AMOUNT = ? "
                    + ",RECEIPT_NO = ? "
                    + ",CHEQUE_DATE = ? "
                    + ",CHEQUE_NO = ? "
                    + ",CHEQUE_BANK = ? "
                    + ",CHEQUE_BRANCH = ? "
                    + ",PAYMENT_TIME = ? "
                    + ",CONFIRM_PAYMENT = 1 "
                    + " WHERE REF1 = ? AND REF2 = ? ";

            ps.setSql(sql);

            ps.setString(i++, data.getPaymentStatus());
            ps.setString(i++, data.getPaymentMethod());
            ps.setString(i++, data.getAmount());
            ps.setString(i++, data.getReceiptNo());
            ps.setDate(i++, AppUtil.toDateSql(data.getChequeDate()));
            ps.setString(i++, data.getChequeNo());
            ps.setString(i++, data.getChequeBank());
            ps.setString(i++, data.getChequeBranch());
            ps.setString(i++, data.getPaymentTime());
            ps.setString(i++, data.getRef1());
            ps.setString(i++, data.getRef2());

            ps.executeUpdate();

            //</editor-fold>
            ps.commit();
            result = "true";

        } catch (Exception e) {
            result = e.getMessage();

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return result;
    }

    public int getTransactionId(String ref1, String ref2) throws SQLException {

        int id = 0;
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            String sql = " SELECT TRANSACTION_ID FROM INVOICE_PAYMENT "
                    + " WHERE REF1 = ? AND REF2 = ? ";

            ps.setSql(sql);

            ps.setString(1, ref1);
            ps.setString(2, ref2);

            rs = ps.executeQuery();

            while (rs.next()) {
                id =rs.getInt("TRANSACTION_ID");
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return id;

    }
}
