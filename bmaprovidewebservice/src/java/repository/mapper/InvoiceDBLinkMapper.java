/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.DocStatusInvEnum;
import model.InvoiceModel;
import utility.AppUtil;
import viewModel.InvoiceViewModel;

/**
 *
 * @author User
 */
public class InvoiceDBLinkMapper {
    
    public InvoiceModel mapSaveData(InvoiceViewModel dataView) {

        InvoiceModel data = new InvoiceModel();

        try {
            
            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setBmaBillNo(AppUtil.getRunningDoc("bmabillno", ""));
            
            //split ref1 เพื่อ map ฟิลด์แยก
            data.setArivDept(AppUtil.checkNullData(dataView.getRef1()).substring(0, 4));
            data.setRevTypeID(AppUtil.checkNullData(dataView.getRef1()).substring(4, 4));
            data.setArivYear(AppUtil.checkNullData(dataView.getRef1()).substring(8, 2));
            data.setArivNo(AppUtil.checkNullData(dataView.getRef1()).substring(10, 8));
            
            data.setRef1(AppUtil.checkNullData(dataView.getRef1()));
            data.setRef2(AppUtil.checkNullData(dataView.getRef2()));
            data.setDocStatus(DocStatusInvEnum.Open.toString());
            data.setDueDate(dataView.getDue_date());
            
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
            data.setCusName(AppUtil.checkNullData(dataView.getCusName()));
            data.setCusAddress1(AppUtil.checkNullData(dataView.getCusAddress1()));
            data.setCusAddress2(AppUtil.checkNullData(dataView.getCusAddress2()));            

            data.setDetail1(AppUtil.checkNullData(dataView.getDetail1()));
            data.setAmount1(AppUtil.convertStringToDouble(dataView.getAmount1()));
            data.setDetail2(AppUtil.checkNullData(dataView.getDetail2()));
            data.setAmount2(AppUtil.convertStringToDouble(dataView.getAmount2()));
            data.setDetail3(AppUtil.checkNullData(dataView.getDetail3()));
            data.setAmount3(AppUtil.convertStringToDouble(dataView.getAmount3()));
            data.setDetail4(AppUtil.checkNullData(dataView.getDetail4()));
            data.setAmount4(AppUtil.convertStringToDouble(dataView.getAmount4()));
            data.setDetail5(AppUtil.checkNullData(dataView.getDetail5()));
            data.setAmount5(AppUtil.convertStringToDouble(dataView.getAmount5()));
            data.setTotalAmount(AppUtil.convertStringToDouble(dataView.getTotalAmount()));
            
            data.setAddPatternPercent(AppUtil.convertStringToInteger(dataView.getAddPatternPercent()));
            data.setAddDueMonth(AppUtil.convertStringToInteger(dataView.getAddDueMonth()));
            
            data.setTaxYear(AppUtil.checkNullData(dataView.getTaxYear()));
            data.setPayTaxYear(AppUtil.checkNullData(dataView.getPayTaxYear()));
            data.setRefDocNo(AppUtil.checkNullData(dataView.getRefDocNo()));
            data.setRefDocDate(AppUtil.checkNullData(dataView.getRefDocDate()));
            data.setNoticeRcvDate(AppUtil.checkNullData(dataView.getNoticeRCVDate()));
            data.setPaymentTerm(AppUtil.checkNullData(dataView.getPaymentTerm()));
            data.setDocNote(AppUtil.checkNullData(dataView.getDocNote()));
            data.setContactFooter(AppUtil.checkNullData(dataView.getContactFooter()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
