/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import model.InvoiceModel;
import model.InvoicePaymentModel;
import utility.AppUtil;
import viewModel.InvoicePaymentViewModel;
import viewModel.InvoiceViewModel;

/**
 *
 * @author User
 */
public class PaymentMapper {
    private ResultSet rs = null;

    public PaymentMapper() {

    }

    public PaymentMapper(ResultSet rs) {
        this.rs = rs;
    }


    public InvoicePaymentModel mapSaveData(InvoicePaymentViewModel dataView) {

        InvoicePaymentModel data = new InvoicePaymentModel();

        try {
            data.setPaymentStatus(AppUtil.checkNullData(dataView.getPaymentStatus()));
            data.setPaymentMethod(AppUtil.checkNullData(dataView.getPaymentMethod()));
            data.setPaymentDate(dataView.getPaymentDate());
            data.setPaymentTime(AppUtil.checkNullData(dataView.getPaymentTime()));
            data.setAmount(AppUtil.convertStringToDouble(dataView.getAmount()));
            data.setReceiptNo(AppUtil.checkNullData(dataView.getReceiptNo()));
            data.setChequeDate(dataView.getChequeDate());
            data.setChequeNo(AppUtil.checkNullData(dataView.getChequeNo()));
            data.setChequeBank(AppUtil.checkNullData(dataView.getChequeBank()));
            data.setChequeBranch(AppUtil.checkNullData(dataView.getChequeBranch()));
            
            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setBillNo(AppUtil.checkNullData(dataView.getBillNo()));
            data.setRef1(AppUtil.checkNullData(dataView.getRef1()));
            data.setRef2(AppUtil.checkNullData(dataView.getRef2()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
            data.setCusName(AppUtil.checkNullData(dataView.getCusName()));
            data.setDueDate(dataView.getDueDate());

            data.setDetail1(AppUtil.checkNullData(dataView.getDetail1()));
            data.setAmount1(AppUtil.convertStringToDouble(dataView.getAmount1()));
            data.setDetail2(AppUtil.checkNullData(dataView.getDetail2()));
            data.setAmount2(AppUtil.convertStringToDouble(dataView.getAmount2()));
            data.setDetail3(AppUtil.checkNullData(dataView.getDetail3()));
            data.setAmount3(AppUtil.convertStringToDouble(dataView.getAmount3()));
            data.setDetail4(AppUtil.checkNullData(dataView.getDetail4()));
            data.setAmount4(AppUtil.convertStringToDouble(dataView.getAmount4()));
            data.setDetail5(AppUtil.checkNullData(dataView.getDetail5()));
            data.setAmount5(AppUtil.convertStringToDouble(dataView.getAmount5()));
            data.setTotalAmount(AppUtil.convertStringToDouble(dataView.getTotalAmount()));

            data.setOffice(AppUtil.checkNullData(dataView.getOffice()));
            data.setPartOfOffice(AppUtil.checkNullData(dataView.getPartOfOffice()));
            data.setTelephone(AppUtil.checkNullData(dataView.getTelephone()));

            // </editor-fold>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
    
}
