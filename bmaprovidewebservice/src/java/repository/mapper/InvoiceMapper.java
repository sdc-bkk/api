/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.PaymentStatusEnum;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import model.InvoiceModel;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultPaymentViewModel;

/**
 *
 * @author User
 */
public class InvoiceMapper {

    private ResultSet rs = null;

    public InvoiceMapper() {

    }

    public InvoiceMapper(ResultSet rs) {
        this.rs = rs;
    }

    public InvoiceModel mapSaveData(InvoiceViewModel dataView) {

        InvoiceModel data = new InvoiceModel();

        try {
            data.setInvoiceId(AppUtil.decryptId(dataView.getInvoiceId()));

            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setBillNo(AppUtil.checkNullData(dataView.getBillNo()));
            data.setRef1(AppUtil.checkNullData(dataView.getRef1()));
            data.setRef2(AppUtil.checkNullData(dataView.getRef2()));
            data.setCusName(AppUtil.checkNullData(dataView.getCusName()));
            data.setCusAddress(AppUtil.checkNullData(dataView.getCusAddress()));
            data.setDueDate(dataView.getDueDate());

            data.setDetail1(AppUtil.checkNullData(dataView.getDetail1()));
            data.setAmount1(AppUtil.convertStringToDouble(dataView.getAmount1()));
            data.setDetail2(AppUtil.checkNullData(dataView.getDetail2()));
            data.setAmount2(AppUtil.convertStringToDouble(dataView.getAmount2()));
            data.setDetail3(AppUtil.checkNullData(dataView.getDetail3()));
            data.setAmount3(AppUtil.convertStringToDouble(dataView.getAmount3()));
            data.setDetail4(AppUtil.checkNullData(dataView.getDetail4()));
            data.setAmount4(AppUtil.convertStringToDouble(dataView.getAmount4()));
            data.setDetail5(AppUtil.checkNullData(dataView.getDetail5()));
            data.setAmount5(AppUtil.convertStringToDouble(dataView.getAmount5()));
            data.setTotalAmount(AppUtil.convertStringToDouble(dataView.getTotalAmount()));

            data.setOffice(AppUtil.checkNullData(dataView.getOffice()));
            data.setPartOfOffice(AppUtil.checkNullData(dataView.getPartOfOffice()));
            data.setTelephone(AppUtil.checkNullData(dataView.getTelephone()));

            // </editor-fold>
            data.setTaxTypeCode(AppUtil.checkNullData(dataView.getTaxTypeCode()));
            data.setTaxTypeName(AppUtil.checkNullData(dataView.getTaxTypeName()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
//            if (!dataView.getInvoiceId().isEmpty()) {
            //BmaBillNo มาจาก running
            data.setBmaBillNo(AppUtil.getRunningDoc("bmabillno", ""));
//            }
            //BARCODE มาจาก BillNo + Ref1 + REf2 + amount
            String totalAmt = AppUtil.numberFormatNoCommaAnd2Digit(dataView.getTotalAmount());
            String barcode = "|" + dataView.getBillNo() + " " + dataView.getRef1() + " " + dataView.getRef2() + " " + totalAmt.replace(".", "");

            data.setBarcode(barcode);
            data.setQrcode(barcode);//QRCODE มาจาก

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public InvoiceViewModel mapFull() {
        InvoiceViewModel data = new InvoiceViewModel();

        try {

            while (rs.next()) {
                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
                data.setBillNo(AppUtil.checkNullData(rs.getString("BILL_NO")));
                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
                data.setRef2(AppUtil.checkNullData(rs.getString("REF2")));
                data.setCusName(AppUtil.checkNullData(rs.getString("CUS_NAME")));
                data.setCusAddress(AppUtil.checkNullData(rs.getString("CUS_ADDRESS")));
                if (rs.getDate("DUE_DATE") != null) {
                    data.setDueDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
                } else {
                    data.setDueDate("");
                }
                data.setDetail1(AppUtil.checkNullData(rs.getString("DETAIL1")));
                data.setAmount1(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT1")));
                data.setDetail2(AppUtil.checkNullData(rs.getString("DETAIL2")));
                data.setAmount2(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT2")));
                data.setDetail3(AppUtil.checkNullData(rs.getString("DETAIL3")));
                data.setAmount3(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT3")));
                data.setDetail4(AppUtil.checkNullData(rs.getString("DETAIL4")));
                data.setAmount4(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT4")));
                data.setDetail5(AppUtil.checkNullData(rs.getString("DETAIL5")));
                data.setAmount5(AppUtil.convertDoubleToStringFormat(rs.getDouble("AMOUNT5")));

                data.setTotalAmount(AppUtil.convertDoubleToStringFormat(rs.getDouble("TOTAL_AMOUNT")));
                data.setTotalAmountDouble(rs.getDouble("TOTAL_AMOUNT"));
                data.setOffice(AppUtil.checkNullData(rs.getString("OFFICE")));
                data.setPartOfOffice(AppUtil.checkNullData(rs.getString("PART_OF_OFFICE")));
                data.setTelephone(AppUtil.checkNullData(rs.getString("TELEPHONE")));
                data.setTaxTypeCode(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
                data.setTaxTypeName(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));

                if (rs.getDate("ACTION_DATE") != null) {
                    data.setActionDate(DateUtils.toThai(rs.getDate("ACTION_DATE")));
                } else {
                    data.setActionDate("");
                }

                if (rs.getString("PAYMENT_STATUS") != null) {
                    data.setPaymentStatus(PaymentStatusEnum.getDisplayName(rs.getInt("PAYMENT_STATUS")));
                } else {
                    data.setPaymentStatus("");
                }

                data.setBmaBillNo(AppUtil.checkNullData(rs.getString("BMA_BILL_NO")));
                data.setBarcode(AppUtil.checkNullData(rs.getString("BARCODE")));
                data.setQrcode(AppUtil.checkNullData(rs.getString("QRCODE")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public ResultPaymentViewModel mapInvoicePayment() {

        ResultPaymentViewModel data = new ResultPaymentViewModel();

        try {

            while (rs.next()) {
                data.setRef1(rs.getString("REF1"));
                data.setRef2(rs.getString("REF2"));
                data.setPaymentStatus("0");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
