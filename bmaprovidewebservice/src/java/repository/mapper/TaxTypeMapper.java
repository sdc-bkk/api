/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import th.co.swf.jk.utilities.DateUtils;
import utility.AppUtil;
import viewModel.TaxTypeViewModel;

/**
 *
 * @author User
 */
public class TaxTypeMapper {
    
    private ResultSet rs = null;

    public TaxTypeMapper() {

    }

    public TaxTypeMapper(ResultSet rs) {
        this.rs = rs;
    }
    
    public TaxTypeViewModel mapFull() {

        TaxTypeViewModel data = new TaxTypeViewModel();

        try {

            while (rs.next()) {

                data.setTaxTypeId(AppUtil.encryptId(rs.getInt("TAX_TYPE_ID")));
                data.setTaxTypeName(rs.getString("TAX_TYPE_NAME"));
                data.setTaxTypeCode(rs.getString("TAX_TYPE_CODE"));


//                data.setActive(rs.getBoolean("IS_ACTIVE"));
//
//                data.setCreatedBy(rs.getString("CREATED_BY"));
//                if (rs.getDate("CREATED_DATE") != null) {
//                    data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
//                }
//
//                if (rs.getDate("UPDATED_DATE") != null) {
//                    //วันที่แก้ไขล่าสุด
//                    data.setUpdatedBy(rs.getString("UPDATED_BY"));
//                    data.setUpdatedDate(DateUtils.toThai(rs.getDate("UPDATED_DATE")));
//                } else {
//                    //วันที่สร้าง
//                    data.setCreatedBy(rs.getString("CREATED_BY"));
//                    if (rs.getDate("CREATED_DATE") != null) {
//                        data.setCreatedDate(DateUtils.toThai(rs.getDate("CREATED_DATE")));
//                    }
//                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
