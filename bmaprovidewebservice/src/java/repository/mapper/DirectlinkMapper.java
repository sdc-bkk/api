/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import model.InquiryRespModel;
import model.InvoiceModel;
import utility.AppUtil;
import viewModel.InvoiceViewModel;
import viewModel.ResultPaymentViewModel;

/**
 *
 * @author DELL
 */
public class DirectlinkMapper {
    private ResultSet rs = null;

    public DirectlinkMapper() {

    }

    public DirectlinkMapper(ResultSet rs) {
        this.rs = rs;
    }

    public InvoiceModel mapSaveData(InvoiceViewModel dataView) {

        InvoiceModel data = new InvoiceModel();

        try {
            data.setInvoiceId(AppUtil.decryptId(dataView.getInvoiceId()));

            // <editor-fold defaultstate="collapsed" desc=" data รับมาจาก request">
            data.setBillNo(AppUtil.checkNullData(dataView.getBillNo()));
            data.setRef1(AppUtil.checkNullData(dataView.getRef1()));
            data.setRef2(AppUtil.checkNullData(dataView.getRef2()));
            data.setCusName(AppUtil.checkNullData(dataView.getCusName()));
            data.setCusAddress(AppUtil.checkNullData(dataView.getCusAddress()));
            data.setDueDate(dataView.getDueDate());

            data.setDetail1(AppUtil.checkNullData(dataView.getDetail1()));
            data.setAmount1(AppUtil.convertStringToDouble(dataView.getAmount1()));
            data.setDetail2(AppUtil.checkNullData(dataView.getDetail2()));
            data.setAmount2(AppUtil.convertStringToDouble(dataView.getAmount2()));
            data.setDetail3(AppUtil.checkNullData(dataView.getDetail3()));
            data.setAmount3(AppUtil.convertStringToDouble(dataView.getAmount3()));
            data.setDetail4(AppUtil.checkNullData(dataView.getDetail4()));
            data.setAmount4(AppUtil.convertStringToDouble(dataView.getAmount4()));
            data.setDetail5(AppUtil.checkNullData(dataView.getDetail5()));
            data.setAmount5(AppUtil.convertStringToDouble(dataView.getAmount5()));
            data.setTotalAmount(AppUtil.convertStringToDouble(dataView.getTotalAmount()));

            data.setOffice(AppUtil.checkNullData(dataView.getOffice()));
            data.setPartOfOffice(AppUtil.checkNullData(dataView.getPartOfOffice()));
            data.setTelephone(AppUtil.checkNullData(dataView.getTelephone()));

            // </editor-fold>
            data.setTaxTypeCode(AppUtil.checkNullData(dataView.getTaxTypeCode()));
            data.setTaxTypeName(AppUtil.checkNullData(dataView.getTaxTypeName()));
            data.setTaxNo(AppUtil.checkNullData(dataView.getTaxNo()));
//            if (!dataView.getInvoiceId().isEmpty()) {
            //BmaBillNo มาจาก running
            data.setBmaBillNo(AppUtil.getRunningDoc("bmabillno", ""));
//            }
            //BARCODE มาจาก BillNo + Ref1 + REf2 + amount
            String totalAmt = AppUtil.numberFormatNoCommaAnd2Digit(dataView.getTotalAmount());
            String barcode = "|" + dataView.getBillNo() + " " + dataView.getRef1() + " " + dataView.getRef2() + " " + totalAmt.replace(".", "");
                
            data.setBarcode(barcode);            
            data.setQrcode(barcode);//QRCODE มาจาก

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public InquiryRespModel mapFull() {

        InquiryRespModel resultData = new InquiryRespModel(); 

        try {

            while (rs.next()) {
            resultData.setTranxId("1111123456");
            resultData.setBankRef("K0000200009876");
            resultData.setRespCode(0);
            resultData.setRespMsg("Successful");
            resultData.setBalance(1000.00);
            resultData.setCusName("David John");
            resultData.setPrint1("Print 1");
            resultData.setPrint2("Print2");
            resultData.setPrint3("Print3");
            resultData.setPrint4("Print4");
            resultData.setPrint5("Print5");
            resultData.setPrint6("Print6");
            resultData.setPrint7("Print7");
            
//                data.setInvoiceId(AppUtil.encryptId(rs.getInt("INVOICE_ID")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("BILL_NO")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("REF1")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("REF2")));
//                data.setRef2(AppUtil.checkNullData(rs.getString("CUS_NAME")));
//                if (rs.getDate("DUE_DATE") != null) {
//                    data.setPaymentDate(DateUtils.toThai(rs.getDate("DUE_DATE")));
//                }
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL1")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT1")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL2")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT2")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL3")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT3")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL4")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT4")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("DETAIL5")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("AMOUNT5")));
//                data.setRef1(AppUtil.convertStringToDouble(rs.getString("TOTAL_AMOUNT")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("OFFICE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("PART_OF_OFFICE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TELEPHONE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TAX_TYPE_CODE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TAX_TYPE_NAME")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("TAX_NO")));
//                if (rs.getDate("ACTION_DATE") != null) {
//                    data.setPaymentDate(DateUtils.toThai(rs.getDate("ACTION_DATE")));
//                }
//                data.setRef1(AppUtil.checkNullData(rs.getString("PAYMENT_STATUS")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("BMA_BILL_NO")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("BARCODE")));
//                data.setRef1(AppUtil.checkNullData(rs.getString("QRCODE")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;

    }

    public ResultPaymentViewModel mapInvoicePayment() {

        ResultPaymentViewModel data = new ResultPaymentViewModel();

        try {

            while (rs.next()) {
                data.setRef1(rs.getString("REF1"));
                data.setRef2(rs.getString("REF2"));
                data.setPaymentStatus("0");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
}
