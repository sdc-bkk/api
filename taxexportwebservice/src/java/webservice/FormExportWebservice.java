/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.BaseClassModel;
import model.ResultModel;
import org.json.simple.JSONObject;
import service.FormExportService;
import viewModel.FileDataViewModel;

/**
 * REST Web Service
 *
 * @author User
 */
@Path("formExport")
public class FormExportWebservice {

    @Context
    private UriInfo context;
    FormExportService service = new FormExportService();
    //<editor-fold defaultstate="collapsed" desc="ภน. 01">  

    @GET
    @Path("getTaxForm01PDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getTaxForm01PDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getTaxForm01(request, response, "pdf", id, null);
    }

    @GET
    @Path("getTaxForm01PDF2")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getTaxForm01PDF2(@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
        try {
            String jsonString = request.getHeader("filter") == null ? "" : request.getHeader("filter");
            Gson g = new Gson();

            BaseClassModel a = new ObjectMapper().readValue(jsonString, BaseClassModel.class);
            BaseClassModel p = g.fromJson(jsonString, BaseClassModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        service.getTaxForm01(request, response, "pdf", "aQfdyqoGPsnqvuErYHzpHw", null);
    }

    @GET
    @Path("getTaxForm01Base64/{id}")
    @Produces("Application/json;charset=utf8")
    public String getTaxForm01Base64(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        FileDataViewModel fileData = new FileDataViewModel();
        service.getTaxForm01(request, response, "base64", id, fileData);

        return new Gson().toJson(fileData);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ร.1">  
    @GET
    @Path("getTaxFormData01PDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getTaxFormData01PDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getTaxFormData01(request, response, "pdf", id, null);
    }

    @GET
    @Path("getTaxFormDataBase64/{id}")
    @Produces("Application/json;charset=utf8")
    public String getTaxFormDataBase64(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        FileDataViewModel fileData = new FileDataViewModel();
        service.getTaxFormData01(request, response, "base64", id, fileData);

        return new Gson().toJson(fileData);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ภน. 02">  
    @GET
    @Path("getTaxForm02PDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getTaxForm02PDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getTaxForm02(request, response, "pdf", id, null);
    }

    @GET
    @Path("getTaxForm02Base64/{id}")
    @Produces("Application/json;charset=utf8")
    public String getTaxForm02Base64(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        FileDataViewModel fileData = new FileDataViewModel();
        service.getTaxForm02(request, response, "base64", id, fileData);

        return new Gson().toJson(fileData);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ภน. 03">  
    @GET
    @Path("getTaxForm03PDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getTaxForm03PDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getTaxForm03(request, response, "pdf", id, null);
    }

    @GET
    @Path("getTaxForm03Base64/{id}")
    @Produces("Application/json;charset=utf8")
    public String getTaxForm03Base64(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        FileDataViewModel fileData = new FileDataViewModel();
        service.getTaxForm03(request, response, "base64", id, fileData);

        return new Gson().toJson(fileData);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ใบแจ้งชำระภาษี">  
    @GET
    @Path("getInvoicePDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getInvoicePDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        //service.getTaxForm03(request, response, "pdf", id, null);
        service.getInvoicePDF(request, response, "pdf", id, null);
    }

    @GET
    @Path("getInvoiceBase64/{id}")
    @Produces("Application/json;charset=utf8")
    public String getInvoiceBase64(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        FileDataViewModel fileData = new FileDataViewModel();
        service.getTaxForm03(request, response, "base64", id, fileData);

        return new Gson().toJson(fileData);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ใบเตือนยื่นแบบ">  
    @GET
    @Path("getWarnFormPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getWarnFormPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getWarnFormPDF(request, response, "pdf", id, null);
    }

    @GET
    @Path("getWarnFormPDFRead/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getWarnFormPDFRead(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.updateStatusReadMail(id);
        service.getWarnFormPDF(request, response, "pdf", id, null);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ใบเตือนค้างชำระ">  
    @GET
    @Path("getWarnPayPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getWarnPayPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getWarnPayPDF(request, response, "pdf", id, null);
    }

    @GET
    @Path("getWarnPayPDFRead/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getWarnPayPDFRead(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.updateStatusReadMail(id);
        service.getWarnPayPDF(request, response, "pdf", id, null);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="คำร้องขอรับเงินคืน">  
    @GET
    @Path("getTaxRefundFormPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getTaxRefundFormPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getTaxRefundFormPDF(request, response, "pdf", id, null);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ใบแจ้งรับเงินคืน">  
    @GET
    @Path("getTaxRefundPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getTaxRefundPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getTaxRefundPDF(request, response, "pdf", id, null);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ใบแจ้งชำระ">  
    @GET
    @Path("getInvPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getInvPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getInvoicePDF(request, response, "pdf", id, null);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ใบเสร็จรับเงิน">  
    @GET
    @Path("getReceiptPDF/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public void getReceiptPDF(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("id") String id) throws Exception {
        service.getReceiptPDF(request, response, "pdf", id, null);
    }
    //</editor-fold>
}
