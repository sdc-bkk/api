/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class BaseFileModel {
    
    private String fileName;
    private String pathFile;
    private String fileCate;//I = image ,F =file
    private String fileType;//I =image ,P = pdf , E = excel,W =word 

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getFileCate() {
        return fileCate;
    }

    public void setFileCate(String fileCate) {
        this.fileCate = fileCate;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

}
