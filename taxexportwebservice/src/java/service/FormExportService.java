/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ResultModel;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import repository.FormExportBKKRepo;
import repository.FormExportRepo;
import utility.AppConfig;
import utility.AppUtil;
import utility.DateUtil;
import viewModel.DrpDownViewModel;
import viewModel.FileDataViewModel;
import viewModel.InvoicePaymentModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;
import viewModel.TaxForm02ViewModel;
import viewModel.TaxForm03DetailViewModel;
import viewModel.TaxForm03SummaryViewModel;
import viewModel.TaxForm03ViewModel;
import viewModel.TaxFormRefundViewModel;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class FormExportService {

    FormExportRepo repo = new FormExportRepo();

    public void getTaxForm01(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();

        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        TaxForm01RetailViewModel data = new TaxForm01RetailViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataTaxform01(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="page 1">
        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        Map params = new HashMap();

//        params.put("OwnerName", "ชื่อผู้ประกอบการ"); // ชื่อผู้ประกอบการ
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ
        params.put("DocNo", data.getDocNo()); //เลขที่แบบ
        params.put("OwnerName", data.getOwnerName()); // ชื่อผู้ประกอบการ
        params.put("TaxNo", data.getTaxNo()); // เลขประจำตัวผู้เสียภาษี
        params.put("CustomerType1", "บุคคลธรรมดา".equals(data.getCustomerType().trim()) ? "/" : ""); //ประเภทบุคคลธรรมดา
        params.put("IdCard", data.getIdCard()); //เลขบัตรประชาชน
        params.put("IdCardAt", data.getIdCardAt()); // ออกให้ ณ ที่
        params.put("CustomerType2", "นิติบุคคล".equals(data.getCustomerType().trim()) ? "/" : ""); // ประเภทนิติบุคคล
        params.put("CorpNo", data.getCorpNo()); // เลขที่นิติบุคคล
        params.put("CorpDate", data.getCorpDate()); // เมื่อวันที่
        params.put("Address", data.getAddress()); //เลขที่
        params.put("Soi", data.getSoi()); //ซอย
        params.put("Road", data.getRoad()); // ถนน
        params.put("TambonName", data.getTambonName()); // ตำบล
        params.put("AmphurName", data.getAmphurName()); // อำเภอ
        params.put("ProvinceName", data.getProvinceName()); // จังหวัด
        params.put("Postcode", data.getPostcode()); // รหัสไปรษณีย์
        params.put("Mobile", data.getMobile()); // เบอร์โทร
        
        params.put("SignerName", data.getSignerName()); // เบอร์โทร

        params.put("NumFileHouse", AppUtil.checkNullData(data.getHouseFileNum())); // สำเนาทะเบียนบ้าน สำเนาบัตรประจำตัวประชาชน และสำเนาบัตรประจำตัวผู้เสียภาษีอากรของผู้แจ้งข้อมูล
        params.put("NumFileCer", AppUtil.checkNullData(data.getCerFileNum())); // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
        params.put("NumFileAgent", AppUtil.checkNullData(data.getAgentFileNum())); // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ

        int countStaion = 0;
        if (data.getStationList() != null && data.getStationList().size() > 0) {
            countStaion = data.getStationList().size();

            params.put("StationName", data.getStationList().get(0).getStationName()); //เลขที่
            String isOil = "";
            String isGas = "";
            for (DrpDownViewModel businessTypeData : data.getStationList().get(0).getBusinessTypeList()) {
                if (businessTypeData.getName().contains("น้ำมัน")) {
                    isOil = "/";
                }
                if (businessTypeData.getName().contains("ปิโตรเลียม")) {
                    isGas = "/";
                }
            }
            params.put("Oil", isOil); //น้ำมัน
            params.put("Gas", isGas); // ก๊าซ
            params.put("StationAddress", data.getStationList().get(0).getAddress()); //เลขที่
            params.put("StationMoo", data.getStationList().get(0).getMoo()); //หมู๋ที่
            params.put("StationSoi", data.getStationList().get(0).getSoi()); //ซอย
            params.put("StationRoad", data.getStationList().get(0).getRoad()); // ถนน
            params.put("StationTambonName", data.getStationList().get(0).getTambonName()); // ตำบล
            params.put("StationAmphurName", data.getStationList().get(0).getAmphurName()); // อำเภอ
            params.put("StationProvinceName", data.getStationList().get(0).getProvinceName()); // จังหวัด
            params.put("StationPostcode", data.getStationList().get(0).getPostcode()); // รหัสไปรษณีย์
            params.put("StationMobile", data.getStationList().get(0).getMobile()); // เบอร์โทร
        }

//        count file station
        Integer houseFileNumStation = 0;
        Integer mapFileNumStation = 0;
        for (TaxForm01StationViewModel stationData : data.getStationList()) {
            houseFileNumStation = houseFileNumStation + stationData.getHouseFileNum();
            mapFileNumStation = mapFileNumStation + stationData.getMapFileNum();
        }
        params.put("NumFileHouseStation", AppUtil.checkNullData(houseFileNumStation)); // สำเนาทะเบียนบ้านของสถานการค้าปลีก
        params.put("NumFileMap", AppUtil.checkNullData(mapFileNumStation)); // แผนที่ตั้งของสถานการค้าปลีก

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        AppConfig appConfig = new AppConfig();
        String pathReport = appConfig.value("export.taxform01_page1");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        jasperPrintList.add(jprint);
        //</editor-fold>  
        //</editor-fold>     

        if (countStaion > 1) {
            //<editor-fold defaultstate="collapsed" desc="page 2">
            //<editor-fold defaultstate="collapsed" desc="map parameter"> 
            Map params2 = new HashMap();
            int i = 0;
            //TO DO count > 7
            for (TaxForm01StationViewModel stationData : data.getStationList()) {
                if (i > 0) {
                    params2.put("StationName_" + i, stationData.getStationName()); //เลขที่
                    String isOil = "";
                    String isGas = "";
                    for (DrpDownViewModel businessTypeData : stationData.getBusinessTypeList()) {
                        if (businessTypeData.getName().contains("น้ำมัน")) {
                            isOil = "/";
                        }
                        if (businessTypeData.getName().contains("ปิโตรเลียม")) {
                            isGas = "/";
                        }
                    }
                    params2.put("Oil_" + i, isOil); //น้ำมัน
                    params2.put("Gas_" + i, isGas); // ก๊าซ
                    params2.put("StationAddress_" + i, stationData.getAddress()); //เลขที่
                    params2.put("StationMoo_" + i, stationData.getMoo()); //หมู่ที่
                    params2.put("StationSoi_" + i, stationData.getSoi()); //ซอย
                    params2.put("StationRoad_" + i, stationData.getRoad()); // ถนน
                    params2.put("StationTambonName_" + i, stationData.getTambonName()); // ตำบล
                    params2.put("StationAmphurName_" + i, stationData.getAmphurName()); // อำเภอ
                    params2.put("StationProvinceName_" + i, stationData.getProvinceName()); // จังหวัด
                    params2.put("StationPostcode_" + i, stationData.getPostcode()); // รหัสไปรษณีย์
                    params2.put("StationMobile_" + i, stationData.getMobile()); // เบอร์โทร
                }
                i++;
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="fill report">  
            InputStream resourceFile2 = null;
            String pathReport2 = appConfig.value("export.taxform01_page2");

            resourceFile2 = request.getServletContext().getResourceAsStream(pathReport2);

            JasperReport report2 = JasperCompileManager.compileReport(resourceFile2);
            JasperPrint jprint2 = JasperFillManager.fillReport(report2, params2, new JREmptyDataSource());

            jasperPrintList.add(jprint2);
            //</editor-fold>  
            //</editor-fold>  
        }
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxform01_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jasperPrintList, response, nameReport);
        }

        //</editor-fold>
    }

    public void getTaxFormData01(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        TaxForm01RetailViewModel data = new TaxForm01RetailViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataTaxform01(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo.bkk");
        Map params = new HashMap();

        params.put("OwnerName", "ชื่อผู้ประกอบการ"); // ชื่อผู้ประกอบการ
        params.put("OwnerName", data.getOwnerName()); // ชื่อผู้ประกอบการ
        params.put("AmphurName", data.getAmphurName()); // อำเภอ
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ
        params.put("Logo", pathLogo); //logo

        String isPass = "";
        String isNoPass = "";
        String numFile1 = "";
        String numFile2 = "";
        String numFile3 = "";
        String numFile4 = "";
        String numFile5 = "";

        if (data.getHouseFileNum() > 0) {
            numFile1 = "/";
        }
        if (data.getHouseFileNum() > 0) {
            numFile4 = "/";
        }
        if (data.getHouseFileNum() > 0) {
            numFile5 = "/";
        }

//        count file station
        Integer houseFileNumStation = 0;
        Integer mapFileNumStation = 0;
        for (TaxForm01StationViewModel stationData : data.getStationList()) {
            houseFileNumStation = houseFileNumStation + stationData.getHouseFileNum();
            mapFileNumStation = mapFileNumStation + stationData.getMapFileNum();
        }
        if (houseFileNumStation > 0) {
            numFile2 = "/";
        }
        if (mapFileNumStation > 0) {
            numFile3 = "/";
        }

        if (!"".equals(numFile1) && !"".equals(numFile2) && !"".equals(numFile3) && !"".equals(numFile4) && !"".equals(numFile5)) {
            isPass = "/";
            numFile1 = "";
            numFile2 = "";
            numFile3 = "";
            numFile4 = "";
            numFile5 = "";
        } else {
            isNoPass = "/";
        }

        params.put("Pass", isPass); // ก๊าซ
        params.put("NoPass", isNoPass); // ก๊าซ
        params.put("File1", numFile1); // ก๊าซ
        params.put("File2", numFile2); // ก๊าซ
        params.put("File3", numFile3); // ก๊าซ
        params.put("File4", numFile4); // ก๊าซ
        params.put("File5", numFile5); // ก๊าซ

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        String pathReport = appConfig.value("export.taxformdata01");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxformData01_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void getTaxForm02(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        TaxForm02ViewModel data = new TaxForm02ViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataTaxform02(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        Map params = new HashMap();

        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ
        params.put("DocNo", data.getDocNo()); //เลขที่แบบ

        params.put("OwnerNameOld", data.getOwnerNameOld()); // ชื่อผู้ประกอบการ
        params.put("TaxNoOld", data.getTaxNoOld()); // เลขประจำตัวผู้เสียภาษี

        params.put("StationNameOld", data.getStationNameOld()); //ชื่อสถานการค้าปลีก
        params.put("AddressStation", data.getAddressRetail()); //เลขที่
        params.put("SoiStation", data.getSoiStation()); //ซอย
        params.put("RoadStation", data.getRoadStation()); // ถนน
        params.put("TambonNameStation", data.getTambonNameStation()); // ตำบล
        params.put("AmphurNameStation", data.getAmphurNameStation()); // อำเภอ
        params.put("ProvinceNameStation", data.getProvinceNameStation()); // จังหวัด
        params.put("PostcodeStation", data.getPostcodeStation()); // รหัสไปรษณีย์
        params.put("MobileStation", data.getMobileStation()); // เบอร์โทร

        params.put("TaxRemain", AppUtil.convertDoubleToStringFormat(data.getTaxRemain())); // ภาษีค้างชำระตามบัญชีแนบ
        params.put("TaxOilGas", AppUtil.convertDoubleToStringFormat(data.getTaxOilGas())); //น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษี

        params.put("FileNumE", AppUtil.checkNullData(data.getFileNumE())); // จำนวนหน้า หนังสือรับรอง
        params.put("FileNumA", AppUtil.checkNullData(data.getFileNumA())); // จำนวนหน้า หนังสือมอบอำนาจ
        params.put("FileNumR", AppUtil.checkNullData(data.getFileNumR())); // จำนวนหน้า บัญชีรายการค้างชำระภาษี
        params.put("FileNumO", AppUtil.checkNullData(data.getFileNumO())); // จำนวนหน้า เอกสารอื่นๆ

//      ความประสงค์
        for (DrpDownViewModel childData : data.getBusinessStatusList()) {
            if ("A".equals(childData.getOther())) {
                //เลิกกิจการ
                params.put("TypeA", "/"); //
                String cancelDate = DateUtil.toFormatDateLongThai(data.getCancelDate());
                params.put("CancelDate", cancelDate); // จำนวนหน้า หนังสือรับรอง
                if (cancelDate != null) {
                    String[] parts = cancelDate.split(" ");
                    String day = parts[0]; // day
                    String month = parts[1]; // month
                    String year = parts[2]; // year
                    params.put("CancelDay", day);  
                    params.put("CancelMonth", month); 
                    params.put("CancelYear", year); 
                }
            } else {
                if ("B".equals(childData.getOther())) {
                    //ย้ายสถานที่ตั้ง
                    params.put("TypeB", "/"); //
                    params.put("AddressOld", data.getAddressOld()); // ที่ตั้งเดิม
                    params.put("FullAddress", data.getFullAddress()); // 
                    params.put("ChangeDate", DateUtil.toFormatDateLongThai(data.getChangeDate())); // 
                    params.put("Mobile", data.getMobile()); // 
                }
                if ("D".equals(childData.getOther())) {
                    //เปลี่ยนแปลงข้อมูลเจ้าของ
                    params.put("TypeC", "/"); //
                    params.put("TypeD", "/"); //
                    params.put("OwnerNameOldC", data.getOwnerNameOld()); // ที่ตั้งเดิม
                    params.put("OwnerName", data.getOwnerName()); // 
                    params.put("TaxNo", data.getTaxNo()); // 
                    params.put("Cause", data.getCause()); // 
                    params.put("AddressRetail", data.getAddressRetail()); // 
                    params.put("SoiRetail", data.getSoiRetail()); // 
                    params.put("RoadRetail", data.getRoadRetail()); //
                    params.put("TambonNameRetail", data.getTambonNameRetail()); // 
                    params.put("AmphurNameRetail", data.getAmphurNameRetail()); // 
                    params.put("ProvinceNameRetail", data.getProvinceNameRetail()); // 
                    params.put("PostcodeRetail", data.getPostcodeRetail()); //
                    params.put("MobileRetail", data.getMobileRetail()); //
                }
                if ("E".equals(childData.getOther())) {
                    //เปลี่ยนชื่อสถานค้าปลีก
                    params.put("TypeC", "/"); //
                    params.put("TypeE", "/"); //
                    params.put("StationNameOldC", data.getStationNameOld()); // 
                    params.put("StationName", data.getStationName()); // 
                }
                if ("F".equals(childData.getOther())) {
                    //อื่นๆ
                    params.put("TypeC", "/"); //
                    params.put("TypeF", "/"); //
                    params.put("Other", data.getOther()); // 
                }
            }
        }

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        AppConfig appConfig = new AppConfig();
        String pathReport = appConfig.value("export.taxform02");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

//        JasperDesign design = JRXmlLoader.load(resourceFile);
//            JasperReport reportFile =  JasperCompileManager.compileReport(design);
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxform02_" + formatter.format(new Date());

        //</editor-fold>     
        //<editor-fold defaultstate="collapsed" desc="export"> 
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void getTaxForm03(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        TaxForm03ViewModel data = new TaxForm03ViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataTaxform03(id);

        //get retail
        RetailViewModel retailData = new RetailViewModel();
        id = AppUtil.decryptId(data.getRefRetailId());
        retailData = repo.getDataRetail(id);
        data.setRefRetail(retailData);

        //get station
        RetailStationViewModel retailStationData = new RetailStationViewModel();
        id = AppUtil.decryptId(data.getRefStationId());
        retailStationData = repo.getDataStation(id);
        data.setRefStation(retailStationData);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="page 1">
        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        Map params = new HashMap();

        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ

        params.put("OwnerName", data.getRefRetail().getOwnerName()); // ชื่อผู้ประกอบการ
        params.put("TaxNo", data.getRefRetail().getTaxNo()); // เลขประจำตัวผู้เสียภาษี

        params.put("StationName", data.getRefStation().getStationName()); //ชื่อสถานการค้าปลีก
        params.put("StationAddress", data.getRefStation().getAddress()); //เลขที่
        params.put("StationMoo", data.getRefStation().getMoo()); //หมู่
        params.put("StationSoi", data.getRefStation().getSoi()); //ซอย
        params.put("StationRoad", data.getRefStation().getRoad()); // ถนน
        params.put("StationTambonName", data.getRefStation().getTambonName()); // ตำบล
        params.put("StationAmphurName", data.getRefStation().getAmphurName()); // อำเภอ
        params.put("StationProvinceName", data.getRefStation().getProvinceName()); // จังหวัด
        params.put("StationPostcode", data.getRefStation().getPostcode()); // รหัสไปรษณีย์
        params.put("StationMobile", data.getRefStation().getMobile()); // เบอร์โทร

        params.put("MonthName", data.getMonthName() + " " + data.getYearly()); // ประจำเดือน
        //1= ภายในกำหนด , 2 = เกินกำหนด ,3= เพิ่มภายในกำหนด,4= เพิ่มเกินกำหนด
        if ("1".equals(data.getStatusAcc()) || "3".equals(data.getStatusAcc())) {

            params.put("IsOk", "/"); // ภายในกำหนด
        }
        if ("3".equals(data.getStatusAcc()) || "4".equals(data.getStatusAcc())) {

            params.put("IsAdd", "/"); // ยื่นเพิ่มเติม
        }
        if ("2".equals(data.getStatusAcc()) || "4".equals(data.getStatusAcc())) {

            params.put("IsNotOk", "/"); // เกินกำหนด
        }
        params.put("AddTimes", AppUtil.checkNullData(data.getAddTimes())); // ครั้งที่

        int i = 1;
        double total = 0;
        for (TaxForm03SummaryViewModel sumData : data.getSummaryList()) {
            params.put("Seq_" + i, AppUtil.checkNullData(sumData.getSequence())); //น้ำมัน
            params.put("OilTypeName_" + i, sumData.getOilTypeName()); // ประเภท
            params.put("Qty_" + i, AppUtil.convertDoubleToStringFormat(sumData.getQty())); // ปริมาณ
            params.put("Rate_" + i, AppUtil.convertDoubleToStringFormat(sumData.getRate())); // อีตรา
            params.put("AmountB_" + i, AppUtil.checkNullData(sumData.getAmountBaht())); // บาท
            params.put("AmountS_" + i, AppUtil.toStringTwoDigitSatang(sumData.getAmountSatang())); // สตางค์
            total = total + sumData.getAmount();
            i++;
        }

        String doubleAsString = AppUtil.convertDoubleToStringFormat(total);
        int indexOfDecimal = doubleAsString.indexOf(".");
        if (total >= 1) {
            params.put("AmountB", AppUtil.checkNullData(doubleAsString.substring(0, indexOfDecimal))); // บาท
        } else {
            params.put("AmountB", "-"); // บาท
        }

//        String satang = String.valueOf(total);
        if (!doubleAsString.contains(".00")) {
            params.put("AmountS", AppUtil.checkNullData(doubleAsString.substring(doubleAsString.length() - 2, doubleAsString.length()))); // สตางค์
        } else {
            params.put("AmountS", "-"); // บาท
        }

        double extraMoney = 0;
        extraMoney = data.getExtraMoney();
        if (extraMoney > total) {
            extraMoney = total;
        }

        params.put("ExtraMoney", AppUtil.convertDoubleToStringFormat(extraMoney)); // ร้อยละเงินเพิ่ม       
        params.put("ExtraRate", AppUtil.convertDoubleToStringFormat(data.getExtraRate())); // ตั้งแต่เดือน       
        params.put("ExtraStartDate", DateUtil.toFormatDateLongThai(data.getExtraStartDate())); // ถึงเดือน        
        params.put("ExtraEndDate", DateUtil.toFormatDateLongThai(data.getExtraEndDate())); // เป็นเงิน

        double totalTax = 0;
        totalTax = total + extraMoney;
        String totalTaxStr = AppUtil.convertDoubleToStringFormat(totalTax);
        // data.getTaxTotal()
        params.put("TaxTotal", AppUtil.convertDoubleTwoDigit(totalTax)); // ยอดภาษีที่ต้องชำระ
        params.put("TaxTotalStr", totalTaxStr); // ยอดภาษีที่ต้องชำระ

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        AppConfig appConfig = new AppConfig();
        String pathReport = appConfig.value("export.taxform03_page1");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        jasperPrintList.add(jprint);

        //</editor-fold>     
        //</editor-fold>
        if (data.getDetailList() != null) {

            //<editor-fold defaultstate="collapsed" desc="page 2">
            //<editor-fold defaultstate="collapsed" desc="map parameter"> 
            Map params2 = new HashMap();
            params2.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ        
            params2.put("OwnerName", data.getRefRetail().getOwnerName()); // ชื่อผู้ประกอบการ
            params2.put("MonthName", data.getMonthName() + " " + data.getYearly()); // ประจำเดือน
            i = 1;
            //TO DO count > 7
            for (TaxForm03DetailViewModel detailData : data.getDetailList()) {
                params2.put("OilTypeName_" + i, detailData.getOilTypeName()); //น้ำมัน
                params2.put("Balance_" + i, AppUtil.convertDoubleToStringFormat(detailData.getBalance())); // ก๊าซ
                params2.put("InQty_" + i, AppUtil.convertDoubleToStringFormat(detailData.getInQty())); //เลขที่
                params2.put("TotalIn_" + i, AppUtil.convertDoubleToStringFormat(detailData.getBalance() + detailData.getInQty())); //ซอย
                params2.put("OutQty_" + i, AppUtil.convertDoubleToStringFormat(detailData.getOutQty())); // ถนน
                params2.put("TotalOut_" + i, AppUtil.convertDoubleToStringFormat(detailData.getOutQty())); // ถนน
//                params2.put("TotalOut_" + i, AppUtil.convertDoubleToStringFormat(detailData.getBalance() + detailData.getInQty() - detailData.getOutQty())); // ตำบล
                params2.put("Remain_" + i, AppUtil.convertDoubleToStringFormat(detailData.getRemain())); // อำเภอ
                i++;
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="fill report">  
            InputStream resourceFile2 = null;
            String pathReport2 = appConfig.value("export.taxform03_page2");

            resourceFile2 = request.getServletContext().getResourceAsStream(pathReport2);

            JasperReport report2 = JasperCompileManager.compileReport(resourceFile2);
            JasperPrint jprint2 = JasperFillManager.fillReport(report2, params2, new JREmptyDataSource());

            jasperPrintList.add(jprint2);
            //</editor-fold>  
            //</editor-fold>  
        }

        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxform03_" + formatter.format(new Date());

        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
//            ExportService.exportPDF(jprint, response, nameReport);
            ExportService.exportPDF(jasperPrintList, response, nameReport);
        }

        //</editor-fold>
    }

    public void getWarnFormPDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        WarnFormViewModel data = new WarnFormViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataWarnForm(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo");
        Map params = new HashMap();

        params.put("Logo", pathLogo); //logo
        params.put("OwnerName", data.getOwnerName()); // ชื่อผู้ประกอบการ
        params.put("StationName", data.getStationName()); // ชื่อผู้ประกอบการ
        params.put("DocNo1", data.getDocNo1()); // เลขที่1
        params.put("DocNo2", data.getDocNo2()); // เลขที่2
        params.put("OrgName", data.getOrgName().contains("สำนักงานเขต") == true ? data.getOrgName() : "สำนักงานเขต" + data.getOrgName()); // ชื่อหน่วยงาน
        params.put("OrgAddr", data.getOrgAddr()); // ที่ตั้ง
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //ลงวันที่
        params.put("MonthName", data.getMonthName() + " " + data.getYearly()); // ประจำปี/เดือน
        params.put("OrgOwnerName", data.getOrgOwnerName().contains("สำนักงานเขต") == true ? data.getOrgOwnerName() : "สำนักงานเขต" + data.getOrgOwnerName()); // หน่วยงานแจ้าของเรื่อง
        params.put("OrgOwnerTel", data.getOrgOwnerTel()); // โทรศัพท์ 
        params.put("OrgOwnerFax", data.getOrgOwnerFax()); // โทรสาร

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        String pathReport = appConfig.value("export.warnform");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "warnform_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void getWarnPayPDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        WarnFormViewModel data = new WarnFormViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataWarnForm(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo");
        Map params = new HashMap();

        params.put("Logo", pathLogo); //logo
        params.put("OwnerName", data.getOwnerName()); // ชื่อผู้ประกอบการ
        params.put("StationName", data.getStationName()); // ชื่อผู้ประกอบการ
        params.put("DocNo1", data.getDocNo1()); // อำเภอ
        params.put("DocNo2", data.getDocNo2()); // จังหวัด
        params.put("OrgName", data.getOfficeName().contains("สำนักงานเขต") == true ? data.getOfficeName() : "สำนักงานเขต" + data.getOfficeName()); // ชื่อหน่วยงาน
        params.put("OrgAddr", data.getOrgAddr()); // ชื่อผู้ประกอบการ
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ
        params.put("MonthName", data.getMonthName() + " " + data.getYearly()); // จังหวัด
        params.put("OrgOwnerName", data.getOrgOwnerName().contains("สำนักงานเขต") == true ? data.getOrgOwnerName() : "สำนักงานเขต" + data.getOrgOwnerName()); // หน่วยงานแจ้าของเรื่อง
        params.put("OrgOwnerTel", data.getOrgOwnerTel()); // จังหวัด
        params.put("OrgOwnerFax", data.getOrgOwnerFax()); // จังหวัด
        params.put("Amount", data.getAmount()); // จำนวนเงินแบบมีคอมมา
        params.put("AmountStr", data.getAmountStr()); // จำนวนเงิน

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        String pathReport = appConfig.value("export.warnpay");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "warnpay_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void getTaxRefundFormPDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        TaxFormRefundViewModel data = new TaxFormRefundViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataTaxRefund(id);

        RetailViewModel dataRetail = new RetailViewModel();
        if (data.getRefRetailId() != null) {
            dataRetail = repo.getDataRetail(AppUtil.decryptId(data.getRefRetailId()));
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        Map params = new HashMap();

        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ
        params.put("OrgName", data.getRefOfficeName()); // ชื่อหน่วยงาน
        params.put("OwnerName", data.getOwnerName()); // ชื่อผู้ประกอบการ

        params.put("Address", dataRetail.getAddress()); //เลขที่
        params.put("Moo", dataRetail.getMoo()); //ซอย
        params.put("Soi", dataRetail.getSoi()); //ซอย
        params.put("Road", dataRetail.getRoad()); // ถนน
        params.put("TambonName", dataRetail.getTambonName()); // ตำบล
        params.put("AmphurName", dataRetail.getAmphurName()); // อำเภอ
        params.put("ProvinceName", dataRetail.getProvinceName()); // จังหวัด
        params.put("Postcode", dataRetail.getPostcode()); // รหัสไปรษณีย์
        params.put("Mobile", dataRetail.getMobile()); // เบอร์โทร

        params.put("MonthName", data.getMonthName() + " " + data.getYearly()); // เดือนปี
        params.put("AmtStr", data.getAmountStr()); // จำนวนเงินแบบมีคอมมา
        params.put("Amt", AppUtil.convertDoubleTwoDigit(data.getAmount())); // ยอดภาษีที่ต้องชำระ

        params.put("ReceiptNo", data.getReceiptNo()); //เลขที่ใบเสร็จ
        params.put("ReceiptDate", DateUtil.toFormatDateLongThai(data.getReceiptDate())); //วันที่ใบเสร็จ

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        String pathReport = appConfig.value("export.taxrefundform");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxrefund_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void getTaxRefundPDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        TaxFormRefundViewModel data = new TaxFormRefundViewModel();
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataTaxRefund(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo");
        Map params = new HashMap();

        params.put("Logo", pathLogo); //logo
        params.put("OwnerName", data.getOwnerName()); // ชื่อผู้ประกอบการ
        params.put("StationName", data.getStationName()); // ชื่อผู้ประกอบการ
        params.put("DocNo1", data.getDocNo1()); // อำเภอ
        params.put("DocNo2", data.getDocNo2()); // จังหวัด
        params.put("OrgName", data.getRefOfficeName().contains("สำนักงานเขต") == true ? data.getRefOfficeName() : "สำนักงานเขต" + data.getRefOfficeName()); // ชื่อหน่วยงาน
        params.put("OrgAddr", ""); // ที่ตั้ง
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getDocDate())); //วันที่ยื่นแบบ
        params.put("MonthName", data.getMonthName() + " " + data.getYearly()); // จังหวัด
        params.put("OrgOwnerName", data.getRefOfficeName().contains("สำนักงานเขต") == true ? data.getRefOfficeName() : "สำนักงานเขต" + data.getRefOfficeName()); // ชื่อหน่วยงาน
        params.put("OrgOwnerTel", data.getOrgOwnerTel()); // จังหวัด
        params.put("OrgOwnerFax", data.getOrgOwnerFax()); // จังหวัด
        params.put("Place", data.getPlace()); // สถานที่
        params.put("AmtStr", data.getAmountStr()); // จำนวนเงินแบบมีคอมมา
        params.put("Amt", AppUtil.convertDoubleTwoDigit(data.getAmount())); // ยอดภาษีที่ต้องชำระ

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        String pathReport = appConfig.value("export.taxrefund");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxrefund_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void getInvoicePDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        InvoiceViewModel data = new InvoiceViewModel();
        //       int id = Integer.parseInt(idStr);
        int id = AppUtil.decryptId(idStr);
        data = repo.getDataInvoice(id);
        
        //TO DO : get data
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo.bkk");
        String bankList = appConfig.value("banklist");
        Map params = new HashMap();

        params.put("Logo", pathLogo); //logo
        params.put("BankList", bankList); //bankList
        params.put("OwnerName", data.getCusName()); // ชื่อผู้ประกอบการ
        params.put("CusAddress", data.getCusAddress()); // ที่อยู่ผู้ประกอบการ
        params.put("BillNo", data.getBillNo()); // ชื่อผู้ประกอบการ
        params.put("Ref1", data.getRef1()); // อำเภอ
        params.put("Ref2", data.getRef2()); // อำเภอ
        params.put("DueDate", DateUtil.toFormatDateLongThai(data.getDueDate())); // จังหวัด
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getActionDate())); // วันที่
        params.put("OrgName", data.getOffice()); // จังหวัด

        params.put("Order1", "1"); // จังหวัด
        params.put("Detail1", data.getDetail1()); // จังหวัด
        params.put("Amount1", data.getAmount1()); // จังหวัด

        params.put("Order2", !"".equals(data.getDetail2()) ? "2" : ""); // จังหวัด
        params.put("Detail2", data.getDetail2()); // จังหวัด
        params.put("Amount2", !"0.00".equals(data.getAmount2()) ? data.getAmount2() : ""); // จังหวัด

        params.put("Order3", !"".equals(data.getDetail3()) ? "3" : ""); // จังหวัด
        params.put("Detail3", data.getDetail3()); // จังหวัด
        params.put("Amount3", !"0.00".equals(data.getAmount3()) ? data.getAmount3() : ""); // จังหวัด

        params.put("Order4", !"".equals(data.getDetail4()) ? "4" : ""); // จังหวัด
        params.put("Detail4", data.getDetail4()); // จังหวัด
        params.put("Amount4", !"0.00".equals(data.getAmount4()) ? data.getAmount4() : ""); // จังหวัด

        params.put("Order5", !"".equals(data.getDetail5()) ? "5" : ""); // จังหวัด
        params.put("Detail5", data.getDetail5()); // จังหวัด
        params.put("Amount5", !"0.00".equals(data.getAmount5()) ? data.getAmount5() : ""); // จังหวัด

        params.put("Amt", data.getAmount()); // จังหวัด
        params.put("TotalAmount", data.getTotalAmount()); // จังหวัด

        params.put("Barcode", data.getBarcode()); // จังหวัด        
        params.put("QRCode", data.getQrcode()); // จังหวัด

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        String pathReport = appConfig.value("export.invoice");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxrefund_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }

    public void get01(HttpServletRequest request, HttpServletResponse response, String fileType, String id) throws Exception {

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        Map params = new HashMap();

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile = null;

        AppConfig appConfig = new AppConfig();
        String pathReport = appConfig.value("export.taxformdata01");

        resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

//        JasperDesign design = JRXmlLoader.load(resourceFile);
//            JasperReport reportFile =  JasperCompileManager.compileReport(design);
        //</editor-fold>   
        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        Map params2 = new HashMap();

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        InputStream resourceFile2 = null;

        AppConfig appConfig2 = new AppConfig();
        String pathReport2 = appConfig2.value("export.taxform02");

        resourceFile2 = request.getServletContext().getResourceAsStream(pathReport2);

        JasperReport report2 = JasperCompileManager.compileReport(resourceFile2);
        JasperPrint jprint2 = JasperFillManager.fillReport(report2, params2, new JREmptyDataSource());

//        JasperDesign design = JRXmlLoader.load(resourceFile);
//            JasperReport reportFile =  JasperCompileManager.compileReport(design);
        //</editor-fold>     
        List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
        jasperPrintList.add(jprint);
        jasperPrintList.add(jprint2);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        //Add the list as a Parameter
        exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
        //this will make a bookmark in the exported PDF for each of the reports
//  exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.exportReport();

//            JasperExportManager.exportReportToPdfStream(jprint, baos);
        String header = "inline;filename=\"555.pdf\"";
        header = new String(header.getBytes("UTF8"), "ISO8859-1");
        response.setHeader("Content-disposition", header);
        response.setContentType("application/pdf;charset=UTF-8");

        response.getOutputStream().write(baos.toByteArray());
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }

    public ResultData<Boolean> updateStatusReadMail(String id) {
        ResultData<Boolean> resultData = new ResultData<Boolean>();

        try {

            resultData = repo.updateStatus(id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public void getReceiptPDF(HttpServletRequest request, HttpServletResponse response, String fileType, String idStr, FileDataViewModel fileData) throws Exception {

        AppConfig appConfig = new AppConfig();
        //<editor-fold defaultstate="collapsed" desc="get data from repo"> 
        //      int id = Integer.parseInt(idStr);
        int id = AppUtil.decryptId(idStr);
        InvoicePaymentModel data = repo.getDataReceipt(id);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="map parameter"> 
        String pathLogo = appConfig.value("logo.bkk");
        Map params = new HashMap();

        params.put("Logo", pathLogo); //logo
        params.put("ReceiptNo", data.getReceiptNo()); // ชื่อผู้ประกอบการ
        params.put("BillNo", data.getBillNo()); // ชื่อผู้ประกอบการ
        params.put("OwnerName", data.getCusName()); // ชื่อผู้ประกอบการ
        params.put("CusAddress", data.getCusAddress()==null?"":data.getCusAddress()); // ที่อยู่ผู้ประกอบการ
        params.put("DocDate", DateUtil.toFormatDateLongThai(data.getPaymentDate())); // อำเภอ
        params.put("OrgName", data.getOffice().contains("สำนักงานเขต") == true ? data.getOffice() : "สำนักงานเขต" + data.getOffice()); // ชื่อหน่วยงาน
        params.put("OrgAddress", data.getOfficeAddress()==null?"-":data.getOfficeAddress()); // ที่อยู่เขต
        
        params.put("Order1", "1"); // จังหวัด
        params.put("Detail1", data.getDetail1()); // จังหวัด
        params.put("Amount1", data.getAmount1()); // จังหวัด

        params.put("Order2", !"".equals(data.getDetail2()) ? "2" : ""); // จังหวัด
        params.put("Detail2", data.getDetail2()); // จังหวัด
        params.put("Amount2", !"0.00".equals(data.getAmount2()) ? data.getAmount2() : ""); // จังหวัด

        params.put("Order3", !"".equals(data.getDetail3()) ? "3" : ""); // จังหวัด
        params.put("Detail3", data.getDetail3()); // จังหวัด
        params.put("Amount3", !"0.00".equals(data.getAmount3()) ? data.getAmount3() : ""); // จังหวัด

        params.put("Order4", !"".equals(data.getDetail4()) ? "4" : ""); // จังหวัด
        params.put("Detail4", data.getDetail4()); // จังหวัด
        params.put("Amount4", !"0.00".equals(data.getAmount4()) ? data.getAmount4() : ""); // จังหวัด

        params.put("Order5", !"".equals(data.getDetail5()) ? "5" : ""); // จังหวัด
        params.put("Detail5", data.getDetail5()); // จังหวัด
        params.put("Amount5", !"0.00".equals(data.getAmount5()) ? data.getAmount5() : ""); // จังหวัด

        params.put("Amt", data.getAmount()); // จังหวัด
        params.put("TotalAmount", data.getTotalAmount()); // จังหวัด

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="fill report">  
        String pathReport = appConfig.value("export.receipt");

        InputStream resourceFile = request.getServletContext().getResourceAsStream(pathReport);

        JasperReport report = JasperCompileManager.compileReport(resourceFile);
        JasperPrint jprint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());

        //</editor-fold>  
        //<editor-fold defaultstate="collapsed" desc="export"> 
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        String nameReport = "taxrefund_" + formatter.format(new Date());
        if (fileType.equalsIgnoreCase("WORD")) {
            ExportService.exportWord(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("EXCEL")) {
            ExportService.exportExcel(jprint, response, nameReport);
        } else if (fileType.equalsIgnoreCase("base64")) {
            ExportService.exportBase64(jprint, response, nameReport, fileData);
        } else {//PDF
            ExportService.exportPDF(jprint, response, nameReport);
        }

        //</editor-fold>
    }
}
