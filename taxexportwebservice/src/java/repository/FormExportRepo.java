/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import enumeration.AttachFileCateEnum;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import repository.mapper.InvoiceMapper;
import repository.mapper.InvoicePaymentMapper;
import repository.mapper.RetailMapper;
import repository.mapper.TaxForm01Mapper;
import repository.mapper.TaxForm02Mapper;
import repository.mapper.TaxForm03Mapper;
import repository.mapper.TaxFormRefundMapper;
import repository.mapper.WarnFormMapper;
import utility.AppUtil;
import utility.ConnnectionDB;
import utility.MessageBundleUtil;
import utility.PreparedStatementDB;
import viewModel.DrpDownViewModel;
import viewModel.InvoicePaymentModel;
import viewModel.InvoiceViewModel;
import viewModel.ResultData;
import viewModel.RetailStationViewModel;
import viewModel.RetailViewModel;
import viewModel.TaxForm01RetailFileViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationViewModel;
import viewModel.TaxForm02ViewModel;
import viewModel.TaxForm03DetailViewModel;
import viewModel.TaxForm03SummaryViewModel;
import viewModel.TaxForm03ViewModel;
import viewModel.TaxFormRefundViewModel;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class FormExportRepo {

    private final ConnnectionDB connDB = new ConnnectionDB();

    public TaxForm01RetailViewModel getDataTaxform01(int id) throws SQLException {

        TaxForm01RetailViewModel data = new TaxForm01RetailViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT TAX_FORM01_RETAIL.* ,M_TAMBON.TAMBON_THAI,M_TAMBON.DISTRICT_THAI,M_TAMBON.PROVINCE_THAI "
                    + " FROM TAX_FORM01_RETAIL  LEFT JOIN M_TAMBON ON TAX_FORM01_RETAIL.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + " WHERE TAX_FORM01_RETAIL_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            data = new TaxForm01Mapper(rs).mapDataRetail();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get stationList ">
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION  "
                    + " WHERE TAX_FORM01_RETAIL_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<TaxForm01StationViewModel> stationList = new ArrayList<TaxForm01StationViewModel>();

            while (rs.next()) {
                TaxForm01StationViewModel dataStation = new TaxForm01StationViewModel();
                int stationId = rs.getInt("TAX_FORM01_STATION_ID");
                dataStation = _getDataStationTaxform01(stationId, conn);
                stationList.add(dataStation);
            }
//            stationList = new TaxForm01Mapper(rs).mapListStation();
            data.setStationList(stationList);
            // </editor-fold>

        } catch (Exception e) {
            return null;
        } finally {
            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }

    public TaxForm01StationViewModel _getDataStationTaxform01(int id, Connection conn) throws SQLException {

        TaxForm01StationViewModel resultData = new TaxForm01StationViewModel();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION  "
                    + " WHERE TAX_FORM01_STATION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            resultData = new TaxForm01Mapper(rs).mapDataStation();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail business type ">
            sql = " SELECT * "
                    + " FROM TAX_FORM01_STATION_BUSINESS_TYPE  "
                    + " WHERE TAX_FORM01_STATION_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataBusiness = new TaxForm01Mapper(rs).mapDataStationBusiness();
            resultData.setBusinessTypeList(dataBusiness);
            // </editor-fold>

        } catch (Exception e) {

        } finally {

        }

        return resultData;

    }

    public TaxForm02ViewModel getDataTaxform02(int id) throws SQLException {

        TaxForm02ViewModel resultData = new TaxForm02ViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc="get TaxForm02 ">
            String sql = " SELECT TAX_FORM02.*,M_BUSINESS_STATUS.BUSINESS_STATUS_CODE "
                    + " FROM TAX_FORM02 LEFT JOIN M_BUSINESS_STATUS ON M_BUSINESS_STATUS.BUSINESS_STATUS_ID = TAX_FORM02.BUSINESS_STATUS_ID "
                    + " WHERE TAX_FORM02_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            resultData = new TaxForm02Mapper(rs).mapData();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get TaxForm02 Business Status ">
            // ความประสงค์
            sql = " SELECT * "
                    + " FROM TAX_FORM02_BUSINESS_STATUS  "
                    + " WHERE TAX_FORM02_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataBusiness = new TaxForm02Mapper(rs).mapDataBusinessStatus();
            resultData.setBusinessStatusList(dataBusiness);
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;

    }

    public TaxForm03ViewModel getDataTaxform03(int id) throws SQLException {

        TaxForm03ViewModel data = new TaxForm03ViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get taxform03 ">
            String sql = " SELECT * "
                    + " FROM TAX_FORM03  "
                    + " WHERE TAX_FORM03_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            data = new TaxForm03Mapper(rs).mapData();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get summaryList ">
            sql = " SELECT * "
                    + " FROM TAX_FORM03_SUMMARY  "
                    + " WHERE TAX_FORM03_ID = ?  ORDER BY SEQUENCE ASC ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<TaxForm03SummaryViewModel> dataSum = new TaxForm03Mapper(rs).mapDataSummary();
            data.setSummaryList(dataSum);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get detailList ">
            sql = " SELECT * "
                    + " FROM TAX_FORM03_DETAIL  "
                    + " WHERE TAX_FORM03_ID = ? ORDER BY SEQUENCE ASC  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<TaxForm03DetailViewModel> dataDetail = new TaxForm03Mapper(rs).mapDataDetail();
            data.setDetailList(dataDetail);
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }

    public RetailViewModel getDataRetail(int id) throws SQLException {

        RetailViewModel data = new RetailViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT RETAIL.*,M_TAMBON.TAMBON_THAI,M_TAMBON.DISTRICT_THAI,M_TAMBON.PROVINCE_THAI "
                    + " FROM RETAIL  LEFT JOIN M_TAMBON ON RETAIL.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + " WHERE (RETAIL.IS_ACTIVE = 1)  AND (RETAIL.RETAIL_ID = ?)";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new RetailMapper(rs).mapDataRetail();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }

    public RetailStationViewModel getDataStation(int id) throws SQLException {

        RetailStationViewModel data = new RetailStationViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT RETAIL_STATION.*,M_TAMBON.TAMBON_THAI,M_TAMBON.DISTRICT_THAI,M_TAMBON.PROVINCE_THAI  "
                    + " FROM RETAIL_STATION LEFT JOIN M_TAMBON ON RETAIL_STATION.TAMBON_ID = M_TAMBON.TAMBON_ID "
                    + " WHERE RETAIL_STATION_ID = ? ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            data = new RetailMapper(rs).mapDataStation();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc=" get retail business type ">
            sql = " SELECT * "
                    + " FROM RETAIL_STATION_BUSINESS_TYPE  "
                    + " WHERE RETAIL_STATION_ID = ?  ";

            ps.setSql(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            List<DrpDownViewModel> dataBusiness = new RetailMapper(rs).mapDataStationBusiness();
            data.setBusinessTypeList(dataBusiness);
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }

    public WarnFormViewModel getDataWarnForm(int id) throws SQLException {

        WarnFormViewModel data = new WarnFormViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT RETAIL.OWNER_NAME,S.STATION_NAME,OFFICE.OFFICE_NAME,OFFICE.MOBILE , OFFICE.FAX ,F.* FROM WARN_FORM F\n"
                    + "LEFT JOIN RETAIL_STATION S ON S.RETAIL_STATION_ID = F.RETAIL_STATION_ID\n"
                    + "LEFT JOIN RETAIL ON S.RETAIL_ID = RETAIL.RETAIL_ID\n"
                    + "LEFT JOIN OFFICE ON OFFICE.AMPHUR_ID = S.REF_OFFICE\n"
                    + "WHERE WARN_FORM_ID =? ";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new WarnFormMapper(rs).mapData();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }
    
    public TaxFormRefundViewModel getDataTaxRefund(int id) throws SQLException {

        TaxFormRefundViewModel data = new TaxFormRefundViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT RETAIL.OWNER_NAME,S.STATION_NAME,OFFICE.MOBILE , OFFICE.FAX , F.* \n"
                    + "FROM TAX_FORM_REFUND F\n"
                    + "LEFT JOIN RETAIL ON F.REF_RETAIL_ID = RETAIL.RETAIL_ID\n"
                    + "LEFT JOIN RETAIL_STATION S ON S.RETAIL_STATION_ID = F.REF_STATION_ID\n"
                    + "LEFT JOIN OFFICE ON OFFICE.AMPHUR_ID = F.REF_OFFICE"
                    + " WHERE F.TAX_FORM_REFUND_ID =?  ";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new TaxFormRefundMapper(rs).mapData();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }

    public ResultData updateStatus(String id) throws SQLException {
        boolean result = false;
        ResultData resultData = new ResultData();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            String sql = "";
            int i = 1;

            //<editor-fold defaultstate="collapsed" desc="update ">
            sql = " UPDATE WARN_FORM  SET STATUS = ? ,STATUS_NAME = ? , SEND_MAIL_DATE = SYSDATE "
                    + " WHERE WARN_FORM_ID = ? ";
            i = 1;
            ps.setSql(sql);

            ps.setInt(i++, 2);
            ps.setString(i++, "ตอบรับ");
            ps.setInt(i++, AppUtil.decryptId(id));

            result = ps.executeUpdate();

            //</editor-fold>
            ps.commit();

            resultData.setResult(true);
            resultData.setStatus("true");

        } catch (Exception e) {

            resultData.setResult(false);
            resultData.setStatus("false");
            resultData.setErrorMessage(e.getMessage());

            ps.rollback();
        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return resultData;
    }
    
    public InvoiceViewModel getDataInvoice(int id) throws SQLException {

        InvoiceViewModel data = new InvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get invoice ">
            String sql = " SELECT * \n"
                    + "FROM INVOICE \n"
                    + " WHERE INVOICE_ID =?  ";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new InvoiceMapper(rs).mapData();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }
    
    public InvoicePaymentModel getDataReceipt(int id) throws SQLException {

        InvoicePaymentModel data = new InvoicePaymentModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT INVOICE.*,OFFICE.ADDRESS \n"
                    + "FROM INVOICE \n"
                    + " LEFT JOIN OFFICE ON INVOICE.OFFICE ='สำนักงานเขต' || OFFICE.OFFICE_NAME "
                    + " WHERE INVOICE_ID =?  ";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new InvoicePaymentMapper(rs).mapData();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }
}
