/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import utility.AppUtil;
import viewModel.WarnFormViewModel;

/**
 *
 * @author User
 */
public class WarnFormMapper {

    private ResultSet rs = null;

    public WarnFormMapper() {

    }

    public WarnFormMapper(ResultSet rs) {
        this.rs = rs;
    }


    public WarnFormViewModel mapData() {

        WarnFormViewModel data = new  WarnFormViewModel();
        try {

            while (rs.next()) {
                data.setWarnFormId(AppUtil.encryptId(rs.getInt("WARN_FORM_ID")));
                data.setStationId(AppUtil.encryptId(rs.getInt("RETAIL_STATION_ID")));

                data.setYearly(AppUtil.checkNullData(rs.getString("YEARLY")));
                data.setMonthly(AppUtil.checkNullData(rs.getInt("MONTHLY")));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo1(AppUtil.checkNullData(rs.getString("DOC_NO1")));
                data.setDocNo2(AppUtil.checkNullData(rs.getString("DOC_NO2")));
                
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                
                data.setOrgName(AppUtil.checkNullData(rs.getString("ORG_NAME")));
                data.setOrgAddr(AppUtil.checkNullData(rs.getString("ORG_ADDR")));
                data.setOrgOwnerName(AppUtil.checkNullData(rs.getString("ORG_OWNER_NAME")));
                data.setOrgOwnerTel(AppUtil.checkNullData(rs.getString("ORG_OWNER_TEL")));
                data.setOrgOwnerFax(AppUtil.checkNullData(rs.getString("ORG_OWNER_FAX")));
                
                data.setOfficeName(AppUtil.checkNullData(rs.getString("OFFICE_NAME")));
                data.setAmount(AppUtil.checkNullData(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT"))));
                data.setAmountStr(AppUtil.convertDoubleTwoDigit(rs.getDouble("AMOUNT")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }
}
