/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utility.AppUtil;
import viewModel.TaxForm03DetailViewModel;
import viewModel.TaxForm03SummaryViewModel;
import viewModel.TaxForm03ViewModel;

/**
 *
 * @author User
 */
public class TaxForm03Mapper {

    private ResultSet rs = null;

    public TaxForm03Mapper() {

    }

    public TaxForm03Mapper(ResultSet rs) {
        this.rs = rs;
    }

    public TaxForm03ViewModel mapData() {

        TaxForm03ViewModel data = new TaxForm03ViewModel();

        try {

            while (rs.next()) {
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));

                data.setYearly(rs.getInt("YEARLY"));
                data.setMonthly(rs.getInt("MONTHLY"));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }
                data.setStatusAcc(AppUtil.checkNullData(rs.getString("STATUS_ACC")));
                data.setStatusAccDesc(AppUtil.checkNullData(rs.getString("STATUS_ACC_DESC")));

                data.setAddTimes(rs.getInt("ADD_TIMES"));
                data.setSequence(rs.getInt("SEQUENCE"));
                data.setExtraMoney(rs.getDouble("EXTRA_MONEY"));
                data.setExtraRate(rs.getDouble("EXTRA_RATE"));
                data.setExtraStartDate(AppUtil.checkNullData(rs.getDate("EXTRA_START_DATE")));
                data.setExtraEndDate(AppUtil.checkNullData(rs.getDate("EXTRA_END_DATE")));
                data.setTaxTotal(rs.getDouble("TAX_TOTAL"));

                data.setActive(rs.getBoolean("IS_ACTIVE"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

    public List<TaxForm03SummaryViewModel> mapDataSummary() {

        List<TaxForm03SummaryViewModel> dataList = new ArrayList<TaxForm03SummaryViewModel>();

        try {

            while (rs.next()) {
                TaxForm03SummaryViewModel data = new TaxForm03SummaryViewModel();
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setSequence(rs.getInt("SEQUENCE"));
                data.setOilTypeId(AppUtil.encryptId(rs.getInt("OIL_TYPE_ID")));
                data.setOilTypeName(AppUtil.checkNullData(rs.getString("OIL_TYPE_NAME")));
                data.setQty(rs.getDouble("QTY"));
                data.setRate(rs.getDouble("RATE"));
                data.setAmount(rs.getDouble("AMOUNT"));
                data.setAmountBaht(rs.getInt("AMOUNT_BAHT"));
                data.setAmountSatang(rs.getInt("AMOUNT_SATANG"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

    public List<TaxForm03DetailViewModel> mapDataDetail() {

        List<TaxForm03DetailViewModel> dataList = new ArrayList<TaxForm03DetailViewModel>();

        try {

            while (rs.next()) {
                TaxForm03DetailViewModel data = new TaxForm03DetailViewModel();
                data.setTaxForm03Id(AppUtil.encryptId(rs.getInt("TAX_FORM03_ID")));
                data.setSequence(rs.getInt("SEQUENCE"));
                data.setOilTypeId(AppUtil.encryptId(rs.getInt("OIL_TYPE_ID")));
                data.setOilTypeName(AppUtil.checkNullData(rs.getString("OIL_TYPE_NAME")));
                data.setBalance(rs.getDouble("BALANCE"));
                data.setInQty(rs.getDouble("IN_QTY"));
                data.setOutQty(rs.getDouble("OUT_QTY"));
                data.setRemain(rs.getDouble("REMAIN"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

}
