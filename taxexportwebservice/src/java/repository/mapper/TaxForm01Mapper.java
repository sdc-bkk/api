/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.AttachFileCateEnum;
import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utility.AppConfig;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.TaxForm01RetailFileViewModel;
import viewModel.TaxForm01RetailViewModel;
import viewModel.TaxForm01StationFileViewModel;
import viewModel.TaxForm01StationViewModel;

/**
 *
 * @author User
 */
public class TaxForm01Mapper {
    
    private ResultSet rs = null;
    
    public TaxForm01Mapper() {
        
    }
    
    public TaxForm01Mapper(ResultSet rs) {
        this.rs = rs;
    }
    
    public TaxForm01RetailViewModel mapDataRetail() {
        
        TaxForm01RetailViewModel data = new TaxForm01RetailViewModel();
        
        try {
            
            while (rs.next()) {
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setMainStatus(AppUtil.checkNullData(rs.getString("MAIN_STATUS")));
                if (rs.getString("MAIN_STATUS") != null) {
                    data.setMainStatusName(StatusEnum.getDisplayNameTH(rs.getInt("MAIN_STATUS")));
                }
                
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO")));
                data.setCustomerType(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE")));
                data.setIdCard(AppUtil.checkNullData(rs.getString("ID_CARD")));
                data.setIdCardAt(AppUtil.checkNullData(rs.getString("ID_CARD_AT")));
                data.setCorpNo(AppUtil.checkNullData(rs.getString("CORP_NO")));
                data.setCorpDate(AppUtil.checkNullData(rs.getDate("CORP_DATE")));
                
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setMoo(AppUtil.checkNullData(rs.getString("MOO")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_THAI")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("DISTRICT_THAI")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_THAI")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));
                
                data.setSignerName(AppUtil.checkNullData(rs.getString("SIGNER_NAME")));
                
                String fullAddress ="";
                fullAddress = data.getAddress() + " ซอย" + data.getSoi()+ " ถนน"+ data.getRoad()+ " ตำบล"+ data.getTambonName()+ " อำเภอ"+ data.getAmphurName()+ " จังหวัด"+ data.getProvinceName();
                data.setFullAddress(fullAddress);
                
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                
                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setCerFileNum(rs.getInt("CER_FILE_NUM"));
                data.setAgentFileNum(rs.getInt("AGENT_FILE_NUM"));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));
                
                data.setFirstRecord(rs.getBoolean("FIRST_RECORD"));
                
                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                
                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
        
    }
       
    public TaxForm01StationViewModel mapDataStation() {
        
        TaxForm01StationViewModel data = new TaxForm01StationViewModel();
        try {
            
            while (rs.next()) {
                data.setTaxForm01StationId(AppUtil.encryptId(rs.getInt("TAX_FORM01_STATION_ID")));
                data.setTaxForm01RetailId(AppUtil.encryptId(rs.getInt("TAX_FORM01_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                
                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
//                data.setStatusName(AppUtil.checkNullData(rs.getString("STATUS_NAME")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }
                
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setMoo(AppUtil.checkNullData(rs.getString("MOO")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));
                
                String fullAddress ="";
                fullAddress = data.getAddress() + " ซอย" + data.getSoi()+ " ถนน"+ data.getRoad()+ " ตำบล"+ data.getTambonName()+ " อำเภอ"+ data.getAmphurName()+ " จังหวัด"+ data.getProvinceName();
                data.setFullAddress(fullAddress);
                
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setEmail(AppUtil.checkNullData(rs.getString("EMAIL")));
                
                data.setHouseFileNum(rs.getInt("HOUSE_FILE_NUM"));
                data.setMapFileNum(rs.getInt("MAP_FILE_NUM"));
                
                data.setActive(rs.getBoolean("IS_ACTIVE"));
                
                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));
                
                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));
                
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
        
    }
    
    public List<DrpDownViewModel> mapDataStationBusiness() {
        
        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();
        
        try {
            
            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(AppUtil.encryptId(rs.getInt("BUSINESS_TYPE_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("BUSINESS_TYPE_NAME")));
                
                dataList.add(data);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return dataList;
        
    }
    
       
}
