/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import enumeration.StatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utility.AppUtil;
import viewModel.DrpDownViewModel;
import viewModel.TaxForm02ViewModel;

/**
 *
 * @author User
 */
public class TaxForm02Mapper {

    private ResultSet rs = null;

    public TaxForm02Mapper() {

    }

    public TaxForm02Mapper(ResultSet rs) {
        this.rs = rs;
    }

    
    public TaxForm02ViewModel mapData() {

        TaxForm02ViewModel data = new TaxForm02ViewModel();

        try {

            while (rs.next()) {
                data.setTaxForm02Id(AppUtil.encryptId(rs.getInt("TAX_FORM02_ID")));
//                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
//                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                if (rs.getInt("REF_RETAIL_ID") > 0) {
                    data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                } else {
                    data.setRefRetailId("");
                }
                if (rs.getInt("REF_STATION_ID") > 0) {
                    data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));
                } else {
                    data.setRefStationId("");
                }
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                if (rs.getString("STATUS") != null) {
                    data.setStatusName(StatusEnum.getDisplayNameTH(rs.getInt("STATUS")));
                }

                //data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                if (rs.getInt("BUSINESS_STATUS_ID") > 0) {
                    data.setBusinessStatusId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                    data.setBusinessStatusCode(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_CODE")));

                } else {
                    data.setBusinessStatusId("");
                    data.setBusinessStatusCode("");
                }
                data.setBusinessStatusName(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_NAME")));
                data.setCancelDate(AppUtil.checkNullData(rs.getDate("CANCEL_DATE")));

                data.setChangeDate(AppUtil.checkNullData(rs.getDate("CHANGE_DATE")));
                data.setAddressOld(AppUtil.checkNullData(rs.getString("ADDRESS_OLD")));
                data.setHouseNo(AppUtil.checkNullData(rs.getString("HOUSE_NO")));
                data.setAddress(AppUtil.checkNullData(rs.getString("ADDRESS_NEW")));
                data.setSoi(AppUtil.checkNullData(rs.getString("SOI")));
                data.setRoad(AppUtil.checkNullData(rs.getString("ROAD")));
                data.setTambonId(AppUtil.checkNullData(rs.getString("TAMBON_ID")));
                data.setTambonName(AppUtil.checkNullData(rs.getString("TAMBON_NAME")));
                data.setAmphurId(AppUtil.checkNullData(rs.getString("AMPHUR_ID")));
                data.setAmphurName(AppUtil.checkNullData(rs.getString("AMPHUR_NAME")));
                data.setProvinceId(AppUtil.checkNullData(rs.getString("PROVINCE_ID")));
                data.setProvinceName(AppUtil.checkNullData(rs.getString("PROVINCE_NAME")));
                data.setPostcode(AppUtil.checkNullData(rs.getString("POSTCODE")));
                data.setMobileOld(AppUtil.checkNullData(rs.getString("MOBILE_OLD")));
                data.setMobile(AppUtil.checkNullData(rs.getString("MOBILE_NEW")));
                data.setOwnerNameOld(AppUtil.checkNullData(rs.getString("OWNER_NAME_OLD")));
                
                String fullAddress ="";
                fullAddress = data.getAddress() + " ตรอก/ซอย " + data.getSoi()+ " ถ."+ data.getRoad()+ " แขวง"+ data.getTambonName()+ " เขต"+ data.getAmphurName()+ " "+ data.getProvinceName();
                data.setFullAddress(fullAddress);

                data.setStationCode(AppUtil.checkNullData(rs.getString("STATION_CODE")));
                data.setStationNameOld(AppUtil.checkNullData(rs.getString("STATION_NAME_OLD")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME_NEW")));

                data.setOwnerNameOld(AppUtil.checkNullData(rs.getString("OWNER_NAME_OLD")));
                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME_NEW")));
                data.setTaxNoOld(AppUtil.checkNullData(rs.getString("TAX_NO_OLD")));
                data.setTaxNo(AppUtil.checkNullData(rs.getString("TAX_NO_NEW")));
                data.setCustomerTypeOld(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE_OLD")));
                data.setCustomerType(AppUtil.checkNullData(rs.getString("CUSTOMER_TYPE_NEW")));
                data.setIdCardOld(AppUtil.checkNullData(rs.getString("ID_CARD_OLD")));
                data.setIdCard(AppUtil.checkNullData(rs.getString("ID_CARD_NEW")));
                data.setIdCardAtOld(AppUtil.checkNullData(rs.getString("ID_CARD_AT_OLD")));
                data.setIdCardAt(AppUtil.checkNullData(rs.getString("ID_CARD_AT_NEW")));
                data.setCorpNoOld(AppUtil.checkNullData(rs.getString("CORP_NO_OLD")));
                data.setCorpNo(AppUtil.checkNullData(rs.getString("CORP_NO_NEW")));
                data.setCorpDateOld(AppUtil.checkNullData(rs.getDate("CORP_DATE_OLD")));
                data.setCorpDate(AppUtil.checkNullData(rs.getDate("CORP_DATE_NEW")));

                data.setAddressRetail(AppUtil.checkNullData(rs.getString("ADDRESS_RETAIL")));
                data.setSoiRetail(AppUtil.checkNullData(rs.getString("SOI_RETAIL")));
                data.setRoadRetail(AppUtil.checkNullData(rs.getString("ROAD_RETAIL")));
                data.setTambonIdRetail(AppUtil.checkNullData(rs.getString("TAMBON_ID_RETAIL")));
                data.setTambonNameRetail(AppUtil.checkNullData(rs.getString("TAMBON_NAME_RETAIL")));
                data.setAmphurIdRetail(AppUtil.checkNullData(rs.getString("AMPHUR_ID_RETAIL")));
                data.setAmphurNameRetail(AppUtil.checkNullData(rs.getString("AMPHUR_NAME_RETAIL")));
                data.setProvinceIdRetail(AppUtil.checkNullData(rs.getString("PROVINCE_ID_RETAIL")));
                data.setProvinceNameRetail(AppUtil.checkNullData(rs.getString("PROVINCE_NAME_RETAIL")));
                data.setPostcodeRetail(AppUtil.checkNullData(rs.getString("POSTCODE_RETAIL")));
                data.setMobileRetail(AppUtil.checkNullData(rs.getString("MOBILE_RETAIL")));
                data.setCause(AppUtil.checkNullData(rs.getString("CAUSE")));
                
                data.setAddressStation(AppUtil.checkNullData(rs.getString("ADDRESS_STATION")));
                data.setSoiStation(AppUtil.checkNullData(rs.getString("SOI_STATION")));
                data.setRoadStation(AppUtil.checkNullData(rs.getString("ROAD_STATION")));
                data.setTambonIdStation(AppUtil.checkNullData(rs.getString("TAMBON_ID_STATION")));
                data.setTambonNameStation(AppUtil.checkNullData(rs.getString("TAMBON_NAME_STATION")));
                data.setAmphurIdStation(AppUtil.checkNullData(rs.getString("AMPHUR_ID_STATION")));
                data.setAmphurNameStation(AppUtil.checkNullData(rs.getString("AMPHUR_NAME_STATION")));
                data.setProvinceIdStation(AppUtil.checkNullData(rs.getString("PROVINCE_ID_STATION")));
                data.setProvinceNameStation(AppUtil.checkNullData(rs.getString("PROVINCE_NAME_STATION")));
                data.setPostcodeStation(AppUtil.checkNullData(rs.getString("POSTCODE_STATION")));
                data.setMobileStation(AppUtil.checkNullData(rs.getString("MOBILE_STATION")));
                
                data.setOther(AppUtil.checkNullData(rs.getString("OTHER")));
                data.setTaxRemain(rs.getDouble("TAX_REMAIN"));
                data.setTaxOilGas(rs.getDouble("TAX_OIL_GAS"));

                data.setFileNumE(rs.getInt("FILE_NUM_E"));
                data.setFileNumA(rs.getInt("FILE_NUM_A"));
                data.setFileNumR(rs.getInt("FILE_NUM_R"));
                data.setFileNumO(rs.getInt("FILE_NUM_O"));

                data.setCreatedBy(AppUtil.checkNullData(rs.getString("CREATED_BY")));
                data.setCreatedDate(AppUtil.checkNullData(rs.getDate("CREATED_DATE")));

                data.setUpdatedBy(AppUtil.checkNullData(rs.getString("UPDATED_BY")));
                data.setUpdatedDate(AppUtil.checkNullData(rs.getDate("UPDATED_DATE")));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }
    
    public List<DrpDownViewModel> mapDataBusinessStatus() {

        List<DrpDownViewModel> dataList = new ArrayList<DrpDownViewModel>();

        try {

            while (rs.next()) {
                DrpDownViewModel data = new DrpDownViewModel();
                data.setId(AppUtil.encryptId(rs.getInt("BUSINESS_STATUS_ID")));
                data.setName(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_NAME")));
                data.setCode(AppUtil.checkNullData(rs.getString("BUSINESS_STATUS_CODE")));
                data.setOther(AppUtil.checkNullData(rs.getString("TEMPLATE_CODE")));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataList;

    }

}
