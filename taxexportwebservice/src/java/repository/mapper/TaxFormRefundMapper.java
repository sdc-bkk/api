/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.mapper;

import java.sql.ResultSet;
import utility.AppUtil;
import viewModel.TaxFormRefundViewModel;

/**
 *
 * @author User
 */
public class TaxFormRefundMapper {

    private ResultSet rs = null;

    public TaxFormRefundMapper() {

    }

    public TaxFormRefundMapper(ResultSet rs) {
        this.rs = rs;
    }

    public TaxFormRefundViewModel mapData() {

        TaxFormRefundViewModel data = new TaxFormRefundViewModel();

        try {

            while (rs.next()) {

                data.setTaxFormRefundId(AppUtil.encryptId(rs.getInt("TAX_FORM_REFUND_ID")));
                data.setRefRetailId(AppUtil.encryptId(rs.getInt("REF_RETAIL_ID")));
                data.setRefStationId(AppUtil.encryptId(rs.getInt("REF_STATION_ID")));

                data.setDocNo(AppUtil.checkNullData(rs.getString("DOC_NO")));
                data.setDocDate(AppUtil.checkNullData(rs.getDate("DOC_DATE")));
                data.setAmount(rs.getDouble("AMOUNT"));
                data.setAmountStr(AppUtil.checkNullData(AppUtil.numberFormatCommaTwoDigit(rs.getString("AMOUNT"))));

                data.setOwnerName(AppUtil.checkNullData(rs.getString("OWNER_NAME")));
                data.setStationName(AppUtil.checkNullData(rs.getString("STATION_NAME")));
                
                data.setYearly(AppUtil.checkNullData(rs.getString("YEARLY")));
                data.setMonthly(AppUtil.checkNullData(rs.getInt("MONTHLY")));
                data.setMonthName(AppUtil.convertMonthToThMonth(rs.getInt("MONTHLY")));

                data.setStatus(AppUtil.checkNullData(rs.getString("STATUS")));
                data.setRejectRemark(AppUtil.checkNullData(rs.getString("REJECT_REMARK")));
                data.setDocNo1(AppUtil.checkNullData(rs.getString("DOC_NO1")));
                data.setDocNo2(AppUtil.checkNullData(rs.getString("DOC_NO2")));
                data.setPlace(AppUtil.checkNullData(rs.getString("PLACE")));
                data.setDocRefundDate(AppUtil.checkNullData(rs.getDate("DOC_REFUND_DATE")));
                data.setRefOffice(AppUtil.checkNullData(rs.getString("REF_OFFICE")));
                data.setRefOfficeName(AppUtil.checkNullData(rs.getString("REF_OFFICE_NAME")));
                data.setRefOfficeCode(AppUtil.checkNullData(rs.getString("REF_OFFICE_CODE")));
                data.setReceiptNo(AppUtil.checkNullData(rs.getString("RECEIPT_NO")));
                data.setReceiptDate(AppUtil.checkNullData(rs.getDate("RECEIPT_DATE")));
                
                data.setOrgOwnerTel(AppUtil.checkNullData(rs.getString("MOBILE")));
                data.setOrgOwnerFax(AppUtil.checkNullData(rs.getString("FAX")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;

    }

}
