/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import repository.mapper.InvoiceMapper;
import repository.mapper.TaxFormRefundMapper;
import service.ExportService;
import utility.AppConfig;
import utility.AppUtil;
import utility.ConnnectionDB_BKK;
import utility.DateUtil;
import utility.PreparedStatementDB;
import viewModel.FileDataViewModel;
import viewModel.InvoiceViewModel;
import viewModel.TaxFormRefundViewModel;

/**
 *
 * @author User
 */
public class FormExportBKKRepo {
    
    private final ConnnectionDB_BKK connDB = new ConnnectionDB_BKK();
    
    public InvoiceViewModel getDataInvoice(int id) throws SQLException {

        InvoiceViewModel data = new InvoiceViewModel();
        Connection conn = connDB.getConnection();
        PreparedStatementDB ps = new PreparedStatementDB(conn);
        ResultSet rs = null;

        try {

            ps.setAutoCommit(false);
            int i = 1;

            // <editor-fold defaultstate="collapsed" desc=" get retail ">
            String sql = " SELECT * \n"
                    + "FROM INVOICE \n"
                    + " WHERE INVOICE_ID =?  ";

            ps.setSql(sql);

            ps.setInt(i++, id);

            rs = ps.executeQuery();

            data = new InvoiceMapper(rs).mapData();
            // </editor-fold>

        } catch (Exception e) {

        } finally {

            ps.resultSetClose(rs);
            ps.closeConnection();
        }

        return data;

    }
    
}
