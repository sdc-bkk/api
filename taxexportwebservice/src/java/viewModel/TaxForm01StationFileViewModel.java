/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import model.*;

/**
 *
 * @author User
 */
public class TaxForm01StationFileViewModel extends BaseFileModel {
    private String taxForm01StationId;

    public String getTaxForm01StationId() {
        return taxForm01StationId;
    }

    public void setTaxForm01StationId(String taxForm01StationId) {
        this.taxForm01StationId = taxForm01StationId;
    }
    
}
