/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.*;

/**
 *
 * @author User
 */
public class RetailViewModel extends BaseClassModel{
    private String retailId;
    private String mainStatus;//สถานะ (0="สร้าง",1="รอตรวจสอบ",2="ไม่ผ่าน" ,3, ="อนุมัติ"); 
    private String mainStatusName;
    private String ownerName;
    private String taxNo;
    private String customerType;
    private String idCard;
    private String idCardAt;
    private String corpNo;//เลขทำเบียนนิติ
    private String corpDate;//เมื่อวันที่
    private String houseNo;//เลขประจำบ้าน
    private String address;
    private String moo;
    private String soi;
    private String road;
    private String tambonId;
    private String amphurId;
    private String provinceId;
    private String postcode;
    private String tambonName;
    private String amphurName;
    private String provinceName;
    
    private String fullAddress;
    private String mobile;
    private String email;
    private String signerName;
    
    private int houseFileNum;
    private int cerFileNum;
    private int agentFileNum;
    
    private List<TaxForm01RetailFileViewModel> houseFileList; //สำเนาทะเบียนบ้าน
    private List<TaxForm01RetailFileViewModel> cerFileList;//สำเนารับรอง
    private List<TaxForm01RetailFileViewModel> agentFileList;//หนังสือมอบอำนาจ

    public String getSignerName() {
        return signerName;
    }

    public void setSignerName(String signerName) {
        this.signerName = signerName;
    }

    public String getTambonName() {
        return tambonName;
    }

    public void setTambonName(String tambonName) {
        this.tambonName = tambonName;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getRetailId() {
        return retailId;
    }

    public void setRetailId(String retailId) {
        this.retailId = retailId;
    }

    public String getMainStatus() {
        return mainStatus;
    }

    public void setMainStatus(String mainStatus) {
        this.mainStatus = mainStatus;
    }

    public String getMainStatusName() {
        return mainStatusName;
    }

    public void setMainStatusName(String mainStatusName) {
        this.mainStatusName = mainStatusName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIdCardAt() {
        return idCardAt;
    }

    public void setIdCardAt(String idCardAt) {
        this.idCardAt = idCardAt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCorpNo() {
        return corpNo;
    }

    public void setCorpNo(String corpNo) {
        this.corpNo = corpNo;
    }

    public String getCorpDate() {
        return corpDate;
    }

    public void setCorpDate(String corpDate) {
        this.corpDate = corpDate;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public int getHouseFileNum() {
        return houseFileNum;
    }

    public void setHouseFileNum(int houseFileNum) {
        this.houseFileNum = houseFileNum;
    }

    public int getCerFileNum() {
        return cerFileNum;
    }

    public void setCerFileNum(int cerFileNum) {
        this.cerFileNum = cerFileNum;
    }

    public int getAgentFileNum() {
        return agentFileNum;
    }

    public void setAgentFileNum(int agentFileNum) {
        this.agentFileNum = agentFileNum;
    }

    public List<TaxForm01RetailFileViewModel> getHouseFileList() {
        return houseFileList;
    }

    public void setHouseFileList(List<TaxForm01RetailFileViewModel> houseFileList) {
        this.houseFileList = houseFileList;
    }

    public List<TaxForm01RetailFileViewModel> getCerFileList() {
        return cerFileList;
    }

    public void setCerFileList(List<TaxForm01RetailFileViewModel> cerFileList) {
        this.cerFileList = cerFileList;
    }

    public List<TaxForm01RetailFileViewModel> getAgentFileList() {
        return agentFileList;
    }

    public void setAgentFileList(List<TaxForm01RetailFileViewModel> agentFileList) {
        this.agentFileList = agentFileList;
    }
    
}
