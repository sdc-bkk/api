/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewModel;

import java.util.List;
import model.*;

/**
 *
 * @author User
 */
public class RetailStationViewModel  extends BaseClassModel{
    private String retailStationId;
    private String retailId;
    private String stationCode;
    private String stationName;
    private String branchName;
    private List<DrpDownViewModel> businessTypeList;
    private String houseNo;//เลขประจำบ้าน
    private String address;
    private String moo;
    private String soi;
    private String road;
    private String tambonId;
    private String tambonName;
    private String amphurId;
    private String amphurName;
    private String provinceId;
    private String provinceName;
    private String postcode;
    private String fullAddress;
    
    private String mobile;
    private String email;
    private String status;
    private String statusName;
    
    private int houseFileNum;
    private int mapFileNum;

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }        

    public String getRetailStationId() {
        return retailStationId;
    }

    public void setRetailStationId(String retailStationId) {
        this.retailStationId = retailStationId;
    }

    public String getRetailId() {
        return retailId;
    }

    public void setRetailId(String retailId) {
        this.retailId = retailId;
    }

    public String getStationCode() {
        return stationCode;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTambonId() {
        return tambonId;
    }

    public void setTambonId(String tambonId) {
        this.tambonId = tambonId;
    }

    public String getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(String amphurId) {
        this.amphurId = amphurId;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<DrpDownViewModel> getBusinessTypeList() {
        return businessTypeList;
    }

    public void setBusinessTypeList(List<DrpDownViewModel> businessTypeList) {
        this.businessTypeList = businessTypeList;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public int getHouseFileNum() {
        return houseFileNum;
    }

    public void setHouseFileNum(int houseFileNum) {
        this.houseFileNum = houseFileNum;
    }

    public int getMapFileNum() {
        return mapFileNum;
    }

    public void setMapFileNum(int mapFileNum) {
        this.mapFileNum = mapFileNum;
    }


    public String getTambonName() {
        return tambonName;
    }

    public void setTambonName(String tambonName) {
        this.tambonName = tambonName;
    }

    public String getAmphurName() {
        return amphurName;
    }

    public void setAmphurName(String amphurName) {
        this.amphurName = amphurName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    
}
